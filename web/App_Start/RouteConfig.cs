﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{param}",
                defaults: new { controller = "Acceso", action = "Login", param = UrlParameter.Optional }
            );

            routes.MapRoute("default-action", "{controller}/{actionName}/{id}", new { action = "Index" });

        }
    }
}