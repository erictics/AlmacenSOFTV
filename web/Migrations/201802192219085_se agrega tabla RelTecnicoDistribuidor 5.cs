namespace web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregatablaRelTecnicoDistribuidor5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RelTecnicoDistribuidor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntidadClave = c.Guid(nullable: false),
                        DistribuidorClave = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RelTecnicoDistribuidor");
        }
    }
}
