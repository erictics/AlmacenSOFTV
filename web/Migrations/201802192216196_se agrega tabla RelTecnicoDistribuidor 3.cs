namespace web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregatablaRelTecnicoDistribuidor3 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.RelTecnicoDistribuidor");
            AlterColumn("dbo.RelTecnicoDistribuidor", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.RelTecnicoDistribuidor", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.RelTecnicoDistribuidor");
            AlterColumn("dbo.RelTecnicoDistribuidor", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.RelTecnicoDistribuidor", "Id");
        }
    }
}
