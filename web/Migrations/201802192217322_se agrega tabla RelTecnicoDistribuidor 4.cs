namespace web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregatablaRelTecnicoDistribuidor4 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.RelTecnicoDistribuidor");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RelTecnicoDistribuidor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntidadClave = c.Guid(nullable: false),
                        DistribuidorClave = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
