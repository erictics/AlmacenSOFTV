// <auto-generated />
namespace web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class seagregatablaRelTecnicoDistribuidor5 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(seagregatablaRelTecnicoDistribuidor5));
        
        string IMigrationMetadata.Id
        {
            get { return "201802192219085_se agrega tabla RelTecnicoDistribuidor 5"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
