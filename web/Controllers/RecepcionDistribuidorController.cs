﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using web.Wrappers;
using Web.Controllers;

namespace web.Controllers
{
    public class RecepcionDistribuidorController : Controller
    {
        private class ProcesoInvDet{    
            public Guid         ArticuloClave   { get; set; }
            public List<Guid>   Inventario      { get; set; }
        }

        private ArticuloProcesoWrapper PedidoToarticuloProcesoWrapper(ProcesoArticulo u, Guid pgRecepcion)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            ArticuloProcesoWrapper ap = new ArticuloProcesoWrapper();
            ap.Clave = u.ProcesoClave + "|" + u.ArticuloClave;
            ap.ArticuloClave = u.ArticuloClave.ToString();
            ap.Nombre = u.Articulo.Nombre;
            ap.Cantidad = u.Cantidad.Value;         
            ap.Surtida = 0;        
            List<Proceso> recepciones = (from s in dc.Proceso
                                         where s.ProcesoClaveRespuesta == u.Proceso.ProcesoClave
                                         && s.catTipoProcesoClave == 4
                                         && s.catEstatusClave != 6
                                         select s).ToList();
            int cantidad = 0;
            foreach (Proceso r in recepciones)
            {
                foreach (ProcesoArticulo pa in r.ProcesoArticulo)
                {
                    if (pa.ArticuloClave == u.ArticuloClave)
                    {
                        cantidad += pa.Cantidad.Value;
                    }
                }
            }
            ap.Surtida = cantidad;
            
            ap.catSeriesClaves = new List<catSerieWrapper>();
            foreach (web.Models.SerieTipoArticulo sta in u.Articulo.catTipoArticulo.SerieTipoArticulo)
            {
                catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                ap.catSeriesClaves.Add(csw);
            }
            ap.TieneSeries = u.Articulo.catTipoArticulo.SerieTipoArticulo.Count();
            //ap.Seriales = new List<SerialesWrapper>();
            ap.Existencia = (from c in dc.Inventario
                           where c.ArticuloClave == u.ArticuloClave
                           && c.EntidadClave == u.Proceso.EntidadClaveSolicitante
                           && c.Cantidad > 0 && c.Estatus == 1
                           select c.Cantidad.Value).ToList().Sum();
            int vRecepcion = 0;
            if (pgRecepcion != Guid.Empty)
            {
                vRecepcion = (from pir in dc.ProcesoInventario
                              join par in dc.ProcesoArticulo on pir.ProcesoClave equals par.ProcesoClave
                              where pir.ProcesoClave == pgRecepcion
                              && par.ArticuloClave == u.ArticuloClave
                              && pir.Inventario.ArticuloClave == par.ArticuloClave
                              select pir.Cantidad).ToList().Sum().GetValueOrDefault();
            }

            if (ap.Surtida == 0)
            {
                try
                {
                    ap.Surtida = u.CantidadEntregada.Value;
                    ap.Surtiendo = 0;
                }
                catch
                {
                    ap.Surtida = 0;
                    ap.Surtiendo = 0;
                }
            }

            if (pgRecepcion != Guid.Empty)
            {
                ap.Surtiendo = vRecepcion;
            }
            return ap;
        }
        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            return rw;
        }

        public InventarioWrapper Inventario2Wrapper(Inventario i, bool inicial, bool pRecepcion, bool pEditable)
        {
            InventarioWrapper iw = new InventarioWrapper();
            iw.Clave = i.InventarioClave.ToString();
            iw.ArticuloClave = i.ArticuloClave.ToString();
            iw.AlmacenClave = i.EntidadClave.ToString();
            iw.Seriales = new List<SerialesWrapper>();
            List<Serie> series = i.Serie.ToList();
            SerialesWrapper sw = new SerialesWrapper();
            sw.InventarioClave = i.InventarioClave.ToString();
            sw.ArticuloClave = i.ArticuloClave.ToString();
            sw.EstatusTexto = (pRecepcion) ? "1" : "0";           
            sw.EstatusClave = (pEditable) ? "1" : "0";
            sw.datos = new List<SerialesDatosWrapper>();
            foreach (Serie s in series)
            {
                SerialesDatosWrapper sdw = new SerialesDatosWrapper();
                sdw.Clave = s.catSerieClave.ToString();
                sdw.Valor = s.Valor.ToString();
                sw.datos.Add(sdw);
            }
            iw.Seriales.Add(sw);
            if (inicial)
            {
                iw.catSeriesClaves = new List<catSerieWrapper>();
                List<web.Models.SerieTipoArticulo> tiposarticulo = i.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();
                foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
                {
                    catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                    iw.catSeriesClaves.Add(csw);
                }
            }
            else
            {
                iw.catSeriesClaves = null;
            }
            return iw;
        }

        private PedidoWrapper PedidoToWrapper(Proceso u)
        {
            PedidoDetalle pd = u.PedidoDetalle;
            PedidoWrapper rw = new PedidoWrapper();
            rw.NumeroPedido = u.NumeroPedido.ToString();
            rw.Clave = u.ProcesoClave.ToString();
            if (u.catProveedor != null)
            {
                catProveedor pro = u.catProveedor;
                rw.Origen = pro.Nombre;
            }
            else
            {
                rw.Origen = "";
            }

            Entidad alm = u.Entidad;
            rw.Destino = alm.Nombre;
            catEstatus estado = u.catEstatus;
            rw.Estatus = estado.Nombre;
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");            
            rw.Total = (u.ProcesoInventario.Count(
                    x =>
                        (x.Inventario.Articulo.catTipoArticulo.SerieTipoArticulo.Count > 0 && (x.Inventario.Cantidad == 0 || x.Inventario.Estatus != 1))
                        ||
                        (x.Inventario.Articulo.catTipoArticulo.SerieTipoArticulo.Count == 0 && x.Inventario.Cantidad < x.Cantidad)
                        ) > 0) ? "1" : "0";
            return rw;
        }

       
        private int elementos = 15;
        [Compress]
        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();            
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();
            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();
            
            List<Entidad> almacenes = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                almacenes = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                     && ta.catDistribuidorClave == null
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {
                almacenes = (from ta in dc.Entidad join b in dc.AmbitoAlmacen on ta.EntidadClave equals b.EntidadClaveAlmacen
                             where ta.catEntidadTipoClave == 1
                             && b.UsuarioClave==u.UsuarioClave
                                     && ta.catDistribuidorClave == u.DistribuidorClave
                             orderby ta.Nombre
                             select ta).ToList();
            }

            List<Entidad> tecnicos = null;

            if (u.DistribuidorClave == null)
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == null
                            orderby ta.Nombre
                            select ta).ToList();
            }
            else
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == u.DistribuidorClave
                            orderby ta.Nombre
                            select ta).ToList();
            }

            List<catTransportista> transportistas = (from t in dc.catTransportista
                                                         where t.Activo
                                                         orderby t.Descripcion
                                                         select t).ToList();

            List<catTipoSalida> tipossalida = (from ta in dc.catTipoSalida
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();

            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;

            ViewData["almacenes"] = almacenes;
            ViewData["tecnicos"] = tecnicos;
            ViewData["tipossalida"] = tipossalida;
            ViewData["transportistas"] = transportistas;

            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");

            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        [Compress]
        public ActionResult Articulos(string data)
        {
            PedidoWrapper rw = new PedidoWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);

            Proceso r = null;
            Guid vgPedido = Guid.Parse(jObject["pedido"].ToString()),
                vgRecepcion = Guid.Empty;

            try
            {
                vgRecepcion = Guid.Parse(jObject["recepcion"].ToString());
            }
            catch { }

            r = (from ro in dc.Proceso
                 where ro.ProcesoClave == vgPedido
                 select ro).SingleOrDefault();

            //List<ProcesoArticulo> articulos = r.ProcesoArticulo.ToList();
            List<ArticuloProcesoWrapper> apws = new List<ArticuloProcesoWrapper>();
            //foreach (ProcesoArticulo a in articulos.Where(x => x.Cantidad > 0))
            foreach (ProcesoArticulo a in
                r.ProcesoArticulo
                    .Where(x => x.Cantidad > 0)
                    //.Where(x =>
                    //    dc.ProcesoArticulo
                    //        .Where(y => y.ProcesoClave == vgRecepcion)
                    //        .Select(y => y.ArticuloClave)
                    //        .Contains(x.ArticuloClave) || vgRecepcion == Guid.Empty
                    //)
                )
            {
                ArticuloProcesoWrapper apw = PedidoToarticuloProcesoWrapper(a, vgRecepcion);
                apws.Add(apw);
            }
            return Json(apws, JsonRequestBehavior.AllowGet);
        }

        [Compress]
        public ActionResult Articulo(string data)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            Guid articulo = Guid.Parse(jObject["Articulo"].ToString()),
                proceso = Guid.Parse(jObject["Proceso"].ToString()),
                recepcion = Guid.Empty;

            try
            {
                recepcion = Guid.Parse(jObject["Recepcion"].ToString());
            }
            catch { }

            ModeloAlmacen dc = new ModeloAlmacen();

            // Listado del Pedido Original
            List<Inventario> rls = (from i in dc.Inventario
                                        join pi in dc.ProcesoInventario on i.InventarioClave equals pi.InventarioClave
                                        where i.ArticuloClave == articulo
                                        && pi.ProcesoClave == proceso
                                        select i).ToList();

            List<Inventario> rls_rec = (from i in dc.Inventario
                                            join pi in dc.ProcesoInventario on i.InventarioClave equals pi.InventarioClave
                                            join p in dc.Proceso on pi.ProcesoClave equals p.ProcesoClave
                                            join a in dc.Articulo on i.ArticuloClave equals a.ArticuloClave
                                            join sta in dc.SerieTipoArticulo on a.catTipoArticuloClave equals sta.catTipoArticuloClave
                                            where i.ArticuloClave == articulo
                                            && p.ProcesoClaveRespuesta == proceso
                                            && (recepcion == Guid.Empty || p.ProcesoClave != recepcion)
                                            && p.catEstatusClave != 6
                                            select i).ToList();

            List<InventarioWrapper> liw = new List<InventarioWrapper>();
             List<InventarioWrapper> liw2 = new List<InventarioWrapper>();
            bool inicial = true;
            bool bRecepcion = false;
            bool bEditable = true;
            foreach (Inventario i in rls.Where(os => !rls_rec.Any(nr => os.InventarioClave.Equals(nr.InventarioClave))))
            {
                if (recepcion != Guid.Empty)
                {
                    bRecepcion = (from pi in dc.ProcesoInventario
                                  where pi.ProcesoClave == recepcion
                                  && pi.InventarioClave == i.InventarioClave
                                  select pi).Count() > 0;

                    // Validamos que no haya sido reasignado el Equipo
                    // o que tengamos menos inventario del asignado originalmente en caso de ser
                    // producto de tipo material
                    bEditable = (from pi in dc.ProcesoInventario
                                 where pi.ProcesoClave == proceso
                                 && pi.InventarioClave == i.InventarioClave
                                 //&& pi.Proceso.EntidadClaveSolicitante == pi.Inventario.EntidadClave
                                 && ((pi.Inventario.Cantidad > 0 && pi.Inventario.Estatus == 1)
                                 || pi.Inventario.Estatus == 2)
                                 select pi).Count() > 0;
                }

                InventarioWrapper iw = Inventario2Wrapper(i, inicial, bRecepcion, bEditable);
                liw.Add(iw);

            }
            
            try
            {
                
                liw.OrderBy(o => o.Seriales.First().datos.First().Valor);
                return Json(liw.Where(p=>p.catSeriesClaves.Count >0).OrderBy(o => o.Seriales.First().datos.First().Valor), JsonRequestBehavior.AllowGet);
            }
            catch {

                return Json(liw, JsonRequestBehavior.AllowGet);
            
            }

            
        }

        [Compress]
        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());
            Guid pedido = Guid.Empty;
            try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }            
            int skip = (pag - 1) * elementos;
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Proceso> rls = (from use in dc.Proceso
                                     where use.catTipoProcesoClave == 4
                                     && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                                     orderby use.Fecha descending
                                     select use).Skip(skip).Take(elementos).ToList();

            List<PedidoWrapper> rw = new List<PedidoWrapper>();

            foreach (Proceso p in rls)
            {
                PedidoWrapper w = PedidoToWrapper(p);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            if (Request["s"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["s"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso pro = (from p in dc.Proceso
                               where p.ProcesoClave == clave
                               select p).SingleOrDefault();

            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();

            Entidad e = pro.Entidad;
            Usuario u = pro.Usuario;

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Salida #" + pro.NumeroPedido + "</h2><br>");
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<font size=2>");
            sb.Append("<b>Enviar a</b><br>");
            sb.Append(e.Nombre + "<br>");
            sb.Append("<br><br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Cantidad Recibida");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" colspan=""3"">");
            sb.Append("Concepto");
            sb.Append("</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            
            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                if (pa.Cantidad > 0)
                {
                    Articulo a = pa.Articulo;
                    
                    sb.Append("<tr>");
                    sb.Append(@"<td align=""right"">");
                    sb.Append(pa.Cantidad);
                    sb.Append("</td>");
                    sb.Append(@"<td colspan=""3"">");
                    sb.Append(a.Nombre);
                    sb.Append("<br>");
                    List<Serie> seriesart = new List<Serie>();
                    foreach (ProcesoInventario pi in procesoinventario)
                    {
                        Inventario i = pi.Inventario;
                        if (i.ArticuloClave == a.ArticuloClave)
                        {
                            List<Serie> series = i.Serie.ToList();
                            int conta = 0;
                            foreach (Serie s in series)
                            {
                                seriesart.Add(s);
                               // sb.Append(s.Valor);
                                if (conta > 0 && conta + 1 < series.Count)
                                {
                                 //   sb.Append("-");
                                }
                                conta++;
                            }
                            if (conta > 0)
                            {
                              //  sb.Append("<br>");
                            }
                        }
                    }

                    int conta1 = 0;
                    foreach (Serie s in seriesart.OrderBy(i => i.Valor))
                    {

                        sb.Append(s.Valor);

                        if (conta1 > -1 && conta1 + 1 < seriesart.Count)
                        {
                            sb.Append(" - ");
                        }

                        conta1++;
                    }

                    if (conta1 > 0)
                    {
                        sb.Append("<br>");
                    }


                }
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");
            
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "_recepciondist.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "RecepcionD_" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "_recepciondist.pdf" + "' style='position: relative; height: 90%; width: 90%;' ></iframe>";
                    return View();
                }
            }
        }

        public ActionResult Guarda(string data)
        {
            try
            {
                JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                Guid vgProcesoSal = Guid.Parse(jObject["ProcesoSal"].ToString()),
                    idProceso = Guid.Empty;

                try
                {
                    idProceso = Guid.Parse(jObject["Recepcion"].ToString());
                }
                catch { }

                ModeloAlmacen dc = new ModeloAlmacen();
                Proceso r = new Proceso();
                Proceso vProcesoSal = new Proceso();
                vProcesoSal = (from ro in dc.Proceso
                     where ro.ProcesoClave == vgProcesoSal
                     select ro).SingleOrDefault();

                if (idProceso != Guid.Empty)
                {
                    r = (from ro in dc.Proceso
                         where ro.ProcesoClave == idProceso
                         select ro).SingleOrDefault();
                }
                else
                {
                    r.ProcesoClave = Guid.NewGuid();
                    r.ProcesoClaveRespuesta = vgProcesoSal;
                    r.catEstatusClave = 4;
                    r.catTipoProcesoClave = 4;
                    r.EntidadClaveSolicitante = vProcesoSal.EntidadClaveSolicitante;
                    r.EntidadClaveAfectada = vProcesoSal.EntidadClaveAfectada;
                }
                
                r.Fecha = DateTime.Now;

                try
                {
                    Usuario usu = (Usuario)Session["u"];
                    r.UsuarioClave = usu.UsuarioClave;
                }
                catch { }

                try
                {
                    r.Observaciones = jObject["Observaciones"].ToString();
                }
                catch { }

                if (idProceso == Guid.Empty)
                {
                    dc.Proceso.Add(r);
                }
                dc.SaveChanges();

                // Reiniciamos los datos de la Recepcion
                if (idProceso != Guid.Empty)
                {
                    ReversaPro(idProceso, Guid.Parse(r.ProcesoClaveRespuesta.ToString()), true);
                }

                Guid inventarioclave = new Guid();
                int saliendo = 0;

                foreach (var item in ((JArray)jObject["Inventario"]).Children())
                {
                    ProcesoArticulo procesoarticulo = new ProcesoArticulo();
                    procesoarticulo.ProcesoClave = r.ProcesoClave;
                    var itemProperties = item.Children<JProperty>();
                    var articuloclave = itemProperties.FirstOrDefault(x => x.Name == "ArticuloClave");
                    procesoarticulo.ArticuloClave = Guid.Parse(articuloclave.Value.ToString());

                    var tieneseries = itemProperties.FirstOrDefault(x => x.Name == "tieneseries").Value;

                    if (Convert.ToInt32(tieneseries.ToString()) > 0)
                    {
                        procesoarticulo.Cantidad = Convert.ToInt32(item["Inventario"].Children().Count());
                        foreach (var inventarioitem in item["Inventario"].Children())
                        {
                            string inventario = inventarioitem.ToString();
                            inventarioclave = Guid.Parse(inventario);
                            Inventario inv = (from i in dc.Inventario
                                                  where i.InventarioClave == inventarioclave
                                                  select i).SingleOrDefault();

                            if (inv != null)
                            {
                                if (!inv.EntidadClave.Equals(r.EntidadClaveSolicitante))
                                {
                                    inv.EntidadClave = r.EntidadClaveSolicitante;
                                    inv.Cantidad = 1;
                                    inv.Standby = 0;
                                    inv.Estatus = 1;
                                    inv.EstatusInv = 7;
                                }
                            }

                            ProcesoInventario pi = new ProcesoInventario();

                            pi.InventarioClave = inv.InventarioClave;
                            pi.ProcesoClave = r.ProcesoClave;
                            pi.Cantidad = 1;
                            pi.Fecha = DateTime.Now;
                            dc.ProcesoInventario.Add(pi);

                            saliendo++;
                        }
                    }
                    else
                    {
                        saliendo = Int32.Parse(itemProperties.FirstOrDefault(x => x.Name == "Saliendo").Value.ToString());
                        procesoarticulo.Cantidad = saliendo;

                        // Afectamos Inventario Almacen Original
                        Inventario inv = (from i in dc.Inventario
                                              where i.ArticuloClave == procesoarticulo.ArticuloClave
                                              && i.EntidadClave == r.EntidadClaveAfectada && i.EstatusInv==7
                                              select i).SingleOrDefault();
                        if (inv != null)
                        {
                            inv.Standby -= saliendo;
                        }

                        // Afectamos Inventario Almacen Solicitante
                        inv = (from i in dc.Inventario
                               where i.ArticuloClave == procesoarticulo.ArticuloClave
                               && i.EntidadClave == r.EntidadClaveSolicitante && i.EstatusInv==7
                               select i).SingleOrDefault();
                        if (inv != null)
                        {
                            inv.Cantidad += saliendo;
                            inv.EstatusInv = 7;
                        }
                        else
                        {
                            inv = new Inventario();
                            inv.InventarioClave = Guid.NewGuid();
                            inv.ArticuloClave = procesoarticulo.ArticuloClave;
                            inv.EntidadClave = r.EntidadClaveSolicitante;
                            inv.Cantidad = saliendo;
                            inv.Standby = 0;
                            inv.Estatus = 1;
                            inv.EstatusInv = 7;

                            dc.Inventario.Add(inv);
                        }

                        ProcesoInventario pi = new ProcesoInventario();
                        pi.InventarioClave = inv.InventarioClave;
                        pi.ProcesoClave = r.ProcesoClave;
                        pi.Cantidad = saliendo;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);
                    }

                    dc.ProcesoArticulo.Add(procesoarticulo);

                    // Afectamos ProcesoArticulo Salida
                    ProcesoArticulo pa = (from x in dc.ProcesoArticulo
                                              where x.ProcesoClave == r.ProcesoClaveRespuesta
                                              && x.ArticuloClave == procesoarticulo.ArticuloClave
                                              select x).SingleOrDefault();

                    if(pa != null)
                    {
                        if(pa.CantidadEntregada == null)
                        {
                            pa.CantidadEntregada = saliendo;
                        }
                        else
                        {
                            pa.CantidadEntregada += saliendo;
                        }
                    }

                    saliendo = 0;
                }

                dc.SaveChanges();
                
                // Actualizamos Estatus de Proceso Salida
                try
                {
                    //int arts_pend
                    var vRes = (from x in dc.ProcesoArticulo
                                where x.ProcesoClave == r.ProcesoClaveRespuesta
                                //&& x.Cantidad > 0 && (x.Cantidad > x.CantidadEntregada
                                //|| x.CantidadEntregada == null)
                                group x by x.ProcesoClave into pro
                                select new
                                {
                                    Tot = pro.Count(),
                                    Pend = pro.Count(p => p.Cantidad > 0 && (p.Cantidad > p.CantidadEntregada || p.CantidadEntregada == null)),
                                    SRec = pro.Count(p => p.CantidadEntregada == 0 || p.CantidadEntregada == null)
                                }
                                     ).SingleOrDefault();

                    Proceso pedido = (from p in dc.Proceso
                                          where p.ProcesoClave == r.ProcesoClaveRespuesta
                                          select p).SingleOrDefault();
                    if (pedido != null)
                    {
                        pedido.catEstatusClave = ((vRes.Pend == 0) ? 4 : ((vRes.Tot != vRes.SRec) ? 3 : 1));
                    }
                    dc.SaveChanges();
                }
                catch { }

                return Content(r.ProcesoClave.ToString());
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Cancela(string data)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);

            Guid idProceso = Guid.Parse(jObject["proceso"].ToString());

            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                Proceso dP = (from d in dc.Proceso
                              where d.ProcesoClave == idProceso
                              select d).SingleOrDefault();

                if (dP != null)
                {
                    dP.Fecha = DateTime.Now;
                    dP.catEstatusClave = 6;
                    ReversaPro(idProceso, dP.ProcesoClaveRespuesta.GetValueOrDefault(), false);
                    dc.SaveChanges();

                    try
                    {
                        var vRes = (from x in dc.ProcesoArticulo
                                    where x.ProcesoClave == dP.ProcesoClaveRespuesta
                                    //&& x.Cantidad > 0 && (x.Cantidad > x.CantidadEntregada
                                    //|| x.CantidadEntregada == null)
                                    group x by x.ProcesoClave into pro
                                    select new
                                    {
                                        Tot = pro.Count(),
                                        Pend = pro.Count(p => p.Cantidad > 0 && (p.Cantidad > p.CantidadEntregada || p.CantidadEntregada == null)),
                                        SRec = pro.Count(p => p.CantidadEntregada == 0 || p.CantidadEntregada == null)
                                    }
                                         ).SingleOrDefault();

                        Proceso pedido = (from p in dc.Proceso
                                              where p.ProcesoClave == dP.ProcesoClaveRespuesta
                                              select p).SingleOrDefault();
                        if (pedido != null)
                        {
                            pedido.catEstatusClave = ((vRes.Pend == 0) ? 4 : ((vRes.Tot != vRes.SRec) ? 3 : 1));
                        }
                        dc.SaveChanges();
                    }
                    catch { }

                    return Content("1");
                }
            }
            catch
            {
                return Content("0");
            }

            return Content("0");
        }

        private List<ProcesoInvDet> GetDetallePro(Guid pProcesoClave)
        {
            List<ProcesoInvDet> Articulos = new List<ProcesoInvDet>();

            ModeloAlmacen dc = new ModeloAlmacen();

            var vArticulos = (from pa in dc.ProcesoArticulo
                              where pa.ProcesoClave == pProcesoClave
                              select pa.ArticuloClave).ToList();

            ProcesoInvDet Articulo;

            foreach (Guid vArticulo in vArticulos)
            {
                Articulo = new ProcesoInvDet();

                Articulo.ArticuloClave = vArticulo;

                //se selecciona la cantidad de articulos que estan en el inventario  100 articulos
                Articulo.Inventario = (from i in dc.Inventario
                                       join pi in dc.ProcesoInventario on i.InventarioClave equals pi.InventarioClave
                                       where pi.ProcesoClave == pProcesoClave
                                       && i.ArticuloClave == vArticulo
                                       select i.InventarioClave).ToList();

                Articulos.Add(Articulo);
            }

            return Articulos;
        }

        private void ReversaPro(Guid pIdProcesoRec, Guid pIdProcesoSal, bool pbFull)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            ProcesoArticulo paR, paS;
            int vCantidad = 0;
            bool HasSerie = false;

            // Regresamos Inventario a su estatus original
            //se regresa clave del articulo y su inventario original
            foreach (ProcesoInvDet vArtDet in GetDetallePro(pIdProcesoRec))
            {
                // Obtenemos los datos de Proceso
                paR = (from pa1 in dc.ProcesoArticulo
                      where pa1.ProcesoClave == pIdProcesoRec
                      && pa1.ArticuloClave == vArtDet.ArticuloClave
                      select pa1).SingleOrDefault();

                HasSerie = paR.Articulo.catTipoArticulo.SerieTipoArticulo.Count > 0;

                vCantidad = 0;
                //por cada articulo en inventario
                foreach (Guid vInvD in vArtDet.Inventario)
                {
                    Inventario invR = (from i in dc.Inventario
                                          where i.InventarioClave == vInvD
                                          select i).SingleOrDefault();

                    if (!HasSerie)
                    {
                        // Tenemos que ir por el inventario actual, si es menor a la cantidad recibida
                        // solo podemos descontar hasta el inventario actual, si no, se descuenta todo lo recibido
                        vCantidad = int.Parse(((invR.Cantidad < paR.Cantidad) ? invR.Cantidad : paR.Cantidad).ToString());
                    }
                        // si hay series
                    else
                    {

                        vCantidad++;
                    }
                    //si existe en el inventario
                    if (invR != null)
                    {
                        if (HasSerie)
                        {
                            if (invR.Cantidad > 0 && invR.Estatus == 1)
                            {
                                invR.Cantidad = 0;
                                invR.Standby = 1;
                                invR.Estatus = 2;
                                invR.EntidadClave = Guid.Parse("00000000-0000-0000-0000-000000000001");
                            }
                        }
                        else
                        {
                            invR.Cantidad -= vCantidad;

                            // Buscamos el Inventario del Almacen Central
                            // Para reintegrar al campo StandBy lo reversado
                            Inventario invS = (from piS in dc.ProcesoInventario
                                                   where piS.ProcesoClave == pIdProcesoSal
                                                   && piS.Inventario.ArticuloClave == vArtDet.ArticuloClave
                                                   select piS.Inventario).SingleOrDefault();

                            // Al Standby del Almacen Central se regresa la cantidad completa de la recepción
                            // puesto que hacemos obligatorio que haga una recepcion minima en caso de haber
                            // tenido diferencia entre el inventario del almacen y lo recibido en el proceso
                            // originalmente
                            invS.Standby += paR.Cantidad;
                        }
                    }
                }

                // Reiniciamos contadores de CantidadEntregada en el Proceso de Salida
                paS = (from pa1 in dc.ProcesoArticulo
                       where pa1.ProcesoClave == pIdProcesoSal
                       && pa1.ArticuloClave == vArtDet.ArticuloClave
                       select pa1).SingleOrDefault();

                paS.CantidadEntregada -= vCantidad;
            }

            // Eliminamos los registros Originales del Proceso
            if (pbFull)
            {
                dc.ProcesoArticulo.RemoveRange(dc.ProcesoArticulo.Where(paD => paD.ProcesoClave == pIdProcesoRec));
                dc.ProcesoInventario.RemoveRange(dc.ProcesoInventario.Where(piD => piD.ProcesoClave == pIdProcesoRec));
            }

            dc.SaveChanges();
        }
    }
}
                                                                                        