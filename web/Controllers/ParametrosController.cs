﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class ParametrosController : Controller
    {

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {

            web.Models.ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            int pags = (from ro in dc.Rol
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div - 1).ToString());
            }
            else
            {
                return Content((div).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<Configuracion> rls = (from use in dc.Configuracion
                                           where use.GrupoConfiguracionClave == pag
                                           orderby use.Parametro
                                           select use).ToList();

            List<ParametroWrapper> rw = new List<ParametroWrapper>();

            foreach (Configuracion u in rls)
            {
                ParametroWrapper w = ParametroToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Obten(string data)
        {
            ParametroWrapper rw = new ParametroWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            string parametro = data;

            Configuracion r = (from ro in dc.Configuracion
                                   where ro.Parametro == parametro
                                   select ro).SingleOrDefault();

            if (r != null)
            {
                rw = ParametroToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    string uid = del_id;

                    Configuracion r = (from ro in dc.Configuracion
                                           where ro.Parametro == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult GuardaColor(string edcolor_id, string edcolor_valor)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                Configuracion r = new Configuracion();

                if (edcolor_id != String.Empty)
                {
                    string parametro = edcolor_id;

                    r = (from ro in dc.Configuracion
                         where ro.Parametro == parametro
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Valor = edcolor_valor;

                if (edcolor_id == String.Empty)
                {
                    dc.Configuracion.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        public ActionResult GuardaLogo(string edlogo_id, string edlogo_valor)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                Configuracion r = new Configuracion();

                if (edlogo_id != String.Empty)
                {
                    string parametro = edlogo_id;

                    r = (from ro in dc.Configuracion
                         where ro.Parametro == parametro
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Valor = edlogo_valor;

                if (edlogo_id == String.Empty)
                {
                    dc.Configuracion.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }

        public ActionResult Guarda(string ed_id, string ed_valor)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                Configuracion r = new Configuracion();

                if (ed_id != String.Empty)
                {
                    string parametro = ed_id;

                    r = (from ro in dc.Configuracion
                         where ro.Parametro == parametro
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Valor = ed_valor;

                if (ed_id == String.Empty)
                {
                    dc.Configuracion.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }

        private ParametroWrapper ParametroToWrapper(Configuracion u)
        {
            ParametroWrapper pw = new ParametroWrapper();
            pw.Descripcion = u.Nombre;
            pw.Nombre = u.Parametro;
            pw.Valor = u.Valor;
            return pw;
        }


    }
}
