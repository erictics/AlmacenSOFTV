﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class SerieController : Controller
    {
        //
        // GET: /Series/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
           Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
           ModeloAlmacen dc = new ModeloAlmacen();

            int pags = (from ro in dc.catSerie
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div - 1).ToString());

            }
            else
            {
                return Content((div).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

           ModeloAlmacen dc = new ModeloAlmacen();

            List<catSerie> rls = (from use in dc.catSerie
                                      orderby use.Nombre
                                      select use).Skip(skip).Take(elementos).ToList();

            List<catSerieWrapper> rw = new List<catSerieWrapper>();

            foreach (catSerie u in rls)
            {
                catSerieWrapper w = SerieToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Obten(string data)
        {
            catSerieWrapper rw = new catSerieWrapper();

           ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

           catSerie r = (from ro in dc.catSerie
                              where ro.catSerieClave == uid
                              select ro).SingleOrDefault();

            if (r != null)
            {
                rw = SerieToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
               ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                   catSerie r = (from ro in dc.catSerie
                                      where ro.catSerieClave == uid
                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
               ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                   catSerie r = (from ro in dc.catSerie
                                      where ro.catSerieClave == uid
                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_hexadecimal, string ed_minima, string ed_maxima, string ed_activo)
        {

            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();
               catSerie r = new catSerie();
                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);
                    r = (from ro in dc.catSerie
                         where ro.catSerieClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {}

                r.Nombre = ed_nombre;

                if (ed_hexadecimal == "on")
                {
                    r.Hexadecimal = true;
                }
                else
                {
                    r.Hexadecimal = false;
                }

                r.Min = Convert.ToInt32(ed_minima);
                r.Max = Convert.ToInt32(ed_maxima);
                r.Activo = ed_activo == null ? false : ed_activo.Length > 0;

                if (ed_id == String.Empty)
                {
                    dc.catSerie.Add(r);
                }
                if (ed_id != String.Empty)
                {
                    int existe = 0;
                    List<Articulo> articulos_serie = dc.Articulo.Where(t=>t.catTipoArticulo.catSerieClaveInterface==r.catSerieClave).ToList();
                    articulos_serie.ForEach(s=> {
                        if(dc.Inventario.Any(m => m.ArticuloClave == s.ArticuloClave)){
                            existe = existe + 1;
                        }
                    });

                    if (existe > 0)
                    {
                       
                        return Content("-1");
                    }

                }

                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }

        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }
    }
}
