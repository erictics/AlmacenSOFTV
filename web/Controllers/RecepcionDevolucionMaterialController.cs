﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class RecepcionDevolucionMaterialController : Controller
    {
        //
        // GET: /RecepcionDevolucionMaterial/

        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> dis = dc.catDistribuidor.Where(x => x.Activo == true).ToList();
            ViewData["distribuidores"] = dis;
            return View();
        }

        public class Devolucion
        {
            public long pedido { get; set; }
            public string fecha { get; set; }
            public string almacen { get; set; }

            public int estatus { get; set; }
            public Guid proceso { get; set; }

            public string Recepcion { get; set; }

        }

        public class lista_dev
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Devolucion> data { get; set; }
        }
        public ActionResult ListaDevoluciones(string data,string almacen,int orden, int draw, int start, int length)
        {
            lista_dev dataTableData = new lista_dev();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = Obtenerdevoluciones(almacen,orden).Count;
            dataTableData.data = Obtenerdevoluciones(almacen,orden).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Devolucion> Obtenerdevoluciones(string almacen, int orden)
        {
            ModeloAlmacen dc = new ModeloAlmacen();             
            List<Devolucion> lista = new List<Devolucion>();
            List<Proceso> dev;
            try
            {
                Guid alm=Guid.Parse(almacen);
                 dev = dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catEstatusClave != 6 && o.EntidadClaveAfectada==alm).ToList();
            }
            catch
            {
                 dev = dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catEstatusClave != 6).ToList();
            }

            if (orden > 0)
            {
                dev = dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catEstatusClave != 6 && o.NumeroPedido==orden).ToList();

            }


           
            foreach (var a in dev)
            {
                Devolucion d = new Devolucion();
                Entidad ent = dc.Entidad.Where(x=>x.EntidadClave==a.EntidadClaveAfectada).First();
                d.almacen = ent.Nombre;
                d.pedido = a.NumeroPedido;
                d.estatus = a.catEstatusClave.Value;
                d.fecha = a.Fecha.ToString();
                d.proceso = a.ProcesoClave;
                try
                {
                    d.Recepcion = dc.Proceso.Where(p => p.ProcesoClaveRespuesta == a.ProcesoClave).Select(y => y.ProcesoClave).First().ToString();
                }
                catch
                {
                    d.Recepcion = "";
                }
                lista.Add(d);

            }
            return lista.OrderByDescending(x=>x.pedido).ToList();
        }



        public class detalledev
        {
            public Guid ClaveArticulo { get; set; }
            public int Cantidad { get; set; }
            public bool tieneseries { get; set; }

            public string articulo { get; set; }

        }

        public ActionResult Detalledevolucion(Guid devolucion)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<detalledev> lista = new List<detalledev>();
            Proceso proceso = dc.Proceso.Where(o => o.ProcesoClave == devolucion && o.catTipoProcesoClave == 13).First();
            List<ProcesoArticulo> pa = dc.ProcesoArticulo.Where(x => x.ProcesoClave == proceso.ProcesoClave).ToList();
            foreach (var a in pa)
            {
                detalledev d = new detalledev();
                d.Cantidad = a.Cantidad.Value;
                d.ClaveArticulo = a.ArticuloClave;
                d.articulo = a.Articulo.Nombre;
                d.tieneseries = tieneSeries(a.ArticuloClave);
                lista.Add(d);
            }
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public class DetSerie
        {
            public string valor { get; set; }
            public string estatus { get; set; }

            public int clv_estatus { get; set; }
        }
        public ActionResult detalleSeries(Guid devolucion,Guid articulo)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<DetSerie> lista = new List<DetSerie>();
            List<ProcesoInventario> pi = dc.ProcesoInventario.Where(x => x.ProcesoClave == devolucion && x.Inventario.ArticuloClave==articulo).ToList();
            foreach (var a in pi)
            {

                DetSerie s = new DetSerie();
                s.valor = a.Inventario.Serie.Select(x => x.Valor).First();
                s.clv_estatus = a.Inventario.EstatusInv.Value;
                string estatus = dc.catEstatus.Where(x => x.catEstatusClave == a.Inventario.EstatusInv.Value).Select(o => o.Nombre).First().ToString();
                s.estatus = estatus;
                lista.Add(s);

            }
            return Json(lista.OrderBy(o=>o.valor), JsonRequestBehavior.AllowGet);

        }


        public bool tieneSeries(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public ActionResult guarda(Guid devolucion)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Proceso devo = dc.Proceso.Where(x => x.ProcesoClave == devolucion).First();

                    if(devo.catEstatusClave==6){
                        dbContextTransaction.Rollback(); 
                        return Content("-2");   
                    }


                    devo.catEstatusClave = 4;

                    Proceso p = new Proceso();
                    p.catEstatusClave = 4;
                    p.catTipoProcesoClave = 14;
                    p.ProcesoClave = Guid.NewGuid();
                    p.ProcesoClaveRespuesta = devo.ProcesoClave;
                    p.EntidadClaveSolicitante = devo.EntidadClaveAfectada;
                    p.EntidadClaveAfectada = devo.EntidadClaveSolicitante;
                    p.Fecha = DateTime.Now;
                    Usuario u = (Usuario)Session["u"];
                    p.UsuarioClave = u.UsuarioClave;
                    dc.Proceso.Add(p);


                    List<ProcesoArticulo> pad = dc.ProcesoArticulo.Where(x => x.ProcesoClave == devo.ProcesoClave).ToList();
                    List<ProcesoInventario> pid = dc.ProcesoInventario.Where(x => x.ProcesoClave == devo.ProcesoClave).ToList();
                    foreach (var a in pad)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = a.ArticuloClave;
                        pa.ProcesoClave = p.ProcesoClave;
                        pa.Cantidad = a.Cantidad;
                        dc.ProcesoArticulo.Add(pa);
                    }
                    foreach (var b in pid)
                    {
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = b.InventarioClave;
                        pi.Cantidad = b.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);
                        Guid alm = Guid.Parse("00000000-0000-0000-0000-000000000001");
                        Inventario inv = dc.Inventario.Where(x => x.InventarioClave == b.InventarioClave).First();
                       

                        if (tieneSeries(b.Inventario.ArticuloClave.Value))
                        {
                            if(inv.Standby==0 && inv.Cantidad==1 && inv.EntidadClave==alm){
                                dbContextTransaction.Rollback(); 
                                return Content("-1");
                            }
                            inv.Cantidad = 1;
                            inv.EntidadClave = alm;
                            inv.Standby = 0;
                            inv.Estatus = 1;
                            inv.EntidadClave = Guid.Parse("00000000-0000-0000-0000-000000000001");
                            inv.Estatus = 1;
                        }
                        else
                        {
                          try {
                                Inventario invalm = dc.Inventario.Where(x => x.EntidadClave == alm && x.ArticuloClave == b.Inventario.ArticuloClave && x.EstatusInv == 9).First();
                                //rebajando del standby 
                               
                                invalm.Cantidad = invalm.Cantidad.Value + b.Cantidad;
                            }
                            catch
                            {
                                Inventario i = new Inventario();
                                i.InventarioClave = Guid.NewGuid();
                                i.ArticuloClave = b.Inventario.ArticuloClave;
                                i.EntidadClave = alm;
                                i.Cantidad = b.Cantidad;
                                i.Standby=0;
                                i.Estatus = 1;
                                i.EstatusInv = 9;
                                dc.Inventario.Add(i);

                                pi.InventarioClave = i.InventarioClave;
                                //rebajando del standby 
                                
                               

                            }
                          inv.Standby = inv.Standby.Value - b.Cantidad;
                            
                        }

                        
                        dc.SaveChanges();
                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);

                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }
        }

        public class alm
        {
            public Guid Clave { get; set; }
            public string Nombre { get; set; }
        }
        public ActionResult ObtenAlamacenes(Guid id)
        {
             ModeloAlmacen dc = new ModeloAlmacen();
             List<alm> almacenes = dc.Entidad.Where(x => x.catDistribuidorClave == id && x.catEntidadTipoClave==1).Select(y => new alm { Clave=y.EntidadClave,Nombre=y.Nombre }).ToList();
             return Json(almacenes.OrderBy(x=>x.Nombre),JsonRequestBehavior.AllowGet);
        }




    }
}
