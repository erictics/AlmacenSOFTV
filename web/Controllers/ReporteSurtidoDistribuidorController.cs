﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;

namespace web.Controllers
{
    public class ReporteSurtidoDistribuidorController : Controller
    {
        //
        // GET: /ReporteSurtidoDistribuidor/
        ModeloAlmacen db = new ModeloAlmacen();
        public ActionResult Index()
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);
            List<catDistribuidor> distribuidores = null;
            if (u.DistribuidorClave == null)
            {
                distribuidores = dc.catDistribuidor.Where(x => x.Activo == true).ToList();
            }
            else
            {
                distribuidores = dc.catDistribuidor.Where(x => x.Activo == true && x.catDistribuidorClave == u.DistribuidorClave).ToList();
            }

            List<catClasificacionArticulo> clasif = db.catClasificacionArticulo.Where(x => x.Activo == true).ToList();
            ViewData["Distribuidores"] = distribuidores;
            ViewData["Clasificacion"] = clasif;


            return View();
        }


        public ActionResult OntenerTipoArticulo(int id)
        {
           var lista= db.catTipoArticulo.Where(x => x.catClasificacionArticuloClave == id).Select(o=>new{o.catTipoArticuloClave,o.Descripcion}).ToList();
           return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult obtenerArticulos(int id)
        {
            var lista = db.Articulo.Where(o => o.catTipoArticuloClave == id).Select(o => new { o.ArticuloClave, o.Nombre}).ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);

            
        }


        public class salidas
        {
            public string distribuidor { get; set; }
            public long OrdenCompra { get; set; }
            public string status { get; set; }
            public string almacen { get; set; }
            public string fecha { get; set; }
            public string salida { get; set; }
            public int cantidad { get; set; }
            public int cant_faltante { get; set; }
            public string articulo { get; set; }
        }


        public ActionResult excelDetalle()
        {

            try
            {
                using (ExcelPackage pck = new ExcelPackage())
                {
                    if (Request["clasificacion"] == null)
                    {
                        return Content("");
                    }

                    ModeloAlmacen dc = new ModeloAlmacen();
                    Guid dis = Guid.Empty;

                    if (Request["distribuidor"] != "-1")
                    {
                        dis = Guid.Parse(Request["distribuidor"]);
                    }
                    else
                    {
                        dis = Guid.Empty;
                    }

                    int cla = -1;
                    try
                    {
                        cla = Convert.ToInt32(Request["clasificacion"]);
                    }
                    catch
                    {
                        cla = -1;
                    }

                    int tip = -1;
                    try
                    {
                        tip = Convert.ToInt32(Request["tipo"]);
                    }
                    catch
                    {
                        tip = -1;
                    }
                    Guid art = Guid.Empty;
                    try
                    {
                        art = Guid.Parse(Request["articulo"]);
                    }
                    catch
                    {
                        art = Guid.Empty;
                    }

                    string clasifi = dc.catClasificacionArticulo.Where(x => x.catClasificacionArticuloClave == cla).Select(x => x.Descripcion).First();
                    string tipo = "", articulo = "";
                    try
                    {
                        tipo = dc.catTipoArticulo.Where(x => x.catTipoArticuloClave == tip).Select(x => x.Descripcion).First();
                        articulo = dc.Articulo.Where(x => x.ArticuloClave == art).Select(x => x.Nombre).First();
                    }
                    catch
                    {
                        tipo = "Todos los tipos de articulo";
                        articulo = "todos los articulos";
                    }

                    string Distribuidor="";
                    try
            {
                catDistribuidor distribuidor = (from d in dc.catDistribuidor
                                                    where d.catDistribuidorClave == dis
                                                    select d).SingleOrDefault();
                        Distribuidor=distribuidor.Nombre;
              
            }
            catch
            {
               Distribuidor="Todos";
            } 


                    string fechaInicio = Convert.ToString(Request["fechainicio"]);
                    string fechaIn = fechaInicio + " 00:01:00:AM";

                    string fechaFin = Convert.ToString(Request["fechaFin"]);
                    string fechaFn = fechaFin + " 11:59:00:PM";

                    DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
                    DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);

                   
                        #region encabezado
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Detalle Surtido");
                        using (ExcelRange rng = ws.Cells["C1:F1"])
                        {
                            rng.Style.Font.Bold = true;
                            //rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                            //rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(182, 221, 232));  //Set color to dark blue
                            //rng.Style.Font.Color.SetColor(Color.Black);
                            rng.Merge = true;


                        }

                        ws.Cells["C1"].Value = "DETALLE ORDENES DE COMPRA";
                        ws.Cells["A2"].Value = "CLASIFICACION";
                        ws.Cells["B2"].Value = clasifi;
                        ws.Cells["C2"].Value = "DISTRIBUIDOR";
                        ws.Cells["D2"].Value = Distribuidor;
                        ws.Cells["A3"].Value = "TIPO DE ARTICULO";
                        ws.Cells["B3"].Value = tipo;
                        ws.Cells["A3"].Value = "PERIODO";
                        ws.Cells["B3"].Value = "Peridodo del: " + fechaInicio + " al: " + fechaFin;
                        ws.Cells["A4"].Value = "ARTICULO";
                        ws.Cells["A5"].Value = articulo;

                        ws.Cells["A6"].Value = "DISTRIBUIDOR";
                        ws.Cells["B6"].Value = "# PEDIDO";
                        ws.Cells["C6"].Value = "STATUS";
                        ws.Cells["D6"].Value = "ALMACEN";
                        ws.Cells["E6"].Value = "FECHA";
                        ws.Cells["F6"].Value = "#SALIDA";
                        ws.Cells["G6"].Value = "CANTIDAD";
                        ws.Cells["H6"].Value = "ARTICULO";


                        
                        #endregion

                        #region Contenido


                        ws.Column(1).Width = 30;
                        ws.Column(2).Width = 10;
                        ws.Column(3).Width = 20;
                        ws.Column(4).Width = 30;
                        ws.Column(5).Width = 30;
                        ws.Column(6).Width = 10;
                        ws.Column(7).Width = 10;
                        ws.Column(8).Width = 30;



                        List<salidas> sal = new List<salidas>();
                        

                        if (dis == Guid.Empty)
                        {
                            sal = (from a in dc.Proceso
                                   join b in dc.Entidad on a.EntidadClaveSolicitante equals b.EntidadClave
                                   join c in dc.ProcesoArticulo on a.ProcesoClave equals c.ProcesoClave
                                   join d in dc.Articulo on c.ArticuloClave equals d.ArticuloClave
                                   join e in dc.Proceso on a.ProcesoClave equals e.ProcesoClave
                                   where a.catTipoProcesoClave == 3 && a.catEstatusClave != 6 && b.catEntidadTipoClave == 1
                                   && c.Cantidad > 0 && d.ArticuloClave == art
                                   && (a.Fecha >= fi && a.Fecha <= fn)
                                   select new salidas
                                   {
                                       distribuidor = b.catDistribuidor.Nombre.ToString(),
                                       OrdenCompra = a.Proceso2.NumeroPedido,
                                       status = a.catEstatus.Nombre,
                                       almacen = b.Nombre,
                                       fecha = a.Fecha.ToString(),
                                       salida = a.NumeroPedido.ToString(),
                                       cantidad = c.Cantidad.Value,
                                       articulo = d.Nombre
                                   }
                                      ).ToList();


                            if (tip == -1)
                            {
                                sal = (from a in dc.Proceso
                                       join b in dc.Entidad on a.EntidadClaveSolicitante equals b.EntidadClave
                                       join c in dc.ProcesoArticulo on a.ProcesoClave equals c.ProcesoClave
                                       join d in dc.Articulo on c.ArticuloClave equals d.ArticuloClave
                                       join e in dc.Proceso on a.ProcesoClave equals e.ProcesoClave
                                       where a.catTipoProcesoClave == 3 && a.catEstatusClave != 6 && b.catEntidadTipoClave == 1
                                       && c.Cantidad > 0
                                       && (a.Fecha >= fi && a.Fecha <= fn)
                                       select new salidas { distribuidor = b.catDistribuidor.Nombre.ToString(), OrdenCompra = a.Proceso2.NumeroPedido, status = a.catEstatus.Nombre, almacen = b.Nombre, fecha = a.Fecha.ToString(), salida = a.NumeroPedido.ToString(), cantidad = c.Cantidad.Value, articulo = d.Nombre }
                                      ).ToList();

                            }


                        }
                        else
                        {
                            sal = (from a in dc.Proceso
                                   join b in dc.Entidad on a.EntidadClaveSolicitante equals b.EntidadClave
                                   join c in dc.ProcesoArticulo on a.ProcesoClave equals c.ProcesoClave
                                   join d in dc.Articulo on c.ArticuloClave equals d.ArticuloClave
                                   join e in dc.Proceso on a.ProcesoClave equals e.ProcesoClave
                                   where a.catTipoProcesoClave == 3 && a.catEstatusClave != 6 && b.catEntidadTipoClave == 1
                                   && c.Cantidad > 0 && b.catDistribuidorClave == dis && d.ArticuloClave == art
                                   && (a.Fecha >= fi && a.Fecha <= fn)
                                   select new salidas { distribuidor = b.catDistribuidor.Nombre.ToString(), OrdenCompra = a.Proceso2.NumeroPedido, status = a.catEstatus.Nombre, almacen = b.Nombre, fecha = a.Fecha.ToString(), salida = a.NumeroPedido.ToString(), cantidad = c.Cantidad.Value, articulo = d.Nombre }
                                      ).ToList();
                            if (tip == -1)
                            {

                                sal = (from a in dc.Proceso
                                       join b in dc.Entidad on a.EntidadClaveSolicitante equals b.EntidadClave
                                       join c in dc.ProcesoArticulo on a.ProcesoClave equals c.ProcesoClave
                                       join d in dc.Articulo on c.ArticuloClave equals d.ArticuloClave
                                       join e in dc.Proceso on a.ProcesoClave equals e.ProcesoClave
                                       where a.catTipoProcesoClave == 3 && a.catEstatusClave != 6 && b.catEntidadTipoClave == 1
                                       && c.Cantidad > 0 && b.catDistribuidorClave == dis
                                       && (a.Fecha >= fi && a.Fecha <= fn)
                                       select new salidas { distribuidor = b.catDistribuidor.Nombre.ToString(), OrdenCompra = a.Proceso2.NumeroPedido, status = a.catEstatus.Nombre, almacen = b.Nombre, fecha = a.Fecha.ToString(), salida = a.NumeroPedido.ToString(), cantidad = c.Cantidad.Value, articulo = d.Nombre }
                                          ).ToList();
                            }
                        }


                        ws.Cells["A7"].LoadFromCollection(sal);

                        #endregion
                           
                           

                    

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=ReporteDetallePedidos.xlsx");
                    Response.BinaryWrite(pck.GetAsByteArray());
                }
            }
            catch { }



            return null;

        }

        public ActionResult Detalle()
        {
            if (Request["clasificacion"] == null)
            {
                return Content("");
            }

            ModeloAlmacen dc = new ModeloAlmacen();
            Guid dis = Guid.Empty;

            if (Request["distribuidor"] != "-1")
            {
                dis = Guid.Parse(Request["distribuidor"]);
            }
            else
            {
                dis = Guid.Empty;
            }
            
            int cla = -1;
            try
            {
                cla = Convert.ToInt32(Request["clasificacion"]);
            }
            catch
            {
                cla = -1;
            }

            int tip = -1;
            try
            {
                tip = Convert.ToInt32(Request["tipo"]);
            }
            catch
            {
                tip = -1;
            }
            Guid art = Guid.Empty;
            try
            {
                art = Guid.Parse(Request["articulo"]);
            }
            catch
            {
                art = Guid.Empty;
            }

            string clasifi = dc.catClasificacionArticulo.Where(x=>x.catClasificacionArticuloClave==cla).Select(x=>x.Descripcion).First();
             string tipo="",articulo="";
            try{
                 tipo = dc.catTipoArticulo.Where(x => x.catTipoArticuloClave == tip).Select(x => x.Descripcion).First();
                  articulo = dc.Articulo.Where(x => x.ArticuloClave == art).Select(x => x.Nombre).First();
            }
           catch{
               tipo="Todos los tipos de articulo";
               articulo="todos los articulos";
           }
            


            int pdf = Convert.ToInt32(Request["pdf"]);
            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaIn = fechaInicio + " 00:01:00:AM";
            
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            string fechaFn = fechaFin + " 11:59:00:PM";
            
            DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");

            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h3>Detalle de ordenes de compra</h3><br>");
            sb.Append("<b>Peridodo del: " + fechaInicio + " al: " + fechaFin + "</b><br>");
            sb.Append("<b>Clasificación: " + clasifi + " </b><br>");
            sb.Append("<b>Tipo de artículo: " + tipo + " </b><br>");
            sb.Append("<b>Artículo: " + articulo + "</b>");
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            try
            {
                catDistribuidor distribuidor = (from d in dc.catDistribuidor
                                                    where d.catDistribuidorClave == dis
                                                    select d).SingleOrDefault();
                sb.Append("<b>Distribuidor: </b>&nbsp;");
                sb.Append("<b>" + distribuidor.Nombre+"</b><br>");
            }
            catch
            {
                sb.Append("<b>Distribuidor: </b>&nbsp;");
                sb.Append("<b>Todos los distribuidores</b><br>");
            }            
            List<salidas> sal = new List<salidas>();
            sb.Append("<br>");

            sal = (from a in dc.Proceso
                   join b in dc.Entidad on a.EntidadClaveSolicitante equals b.EntidadClave
                   join c in dc.ProcesoArticulo on a.ProcesoClave equals c.ProcesoClave
                   join d in dc.Articulo on c.ArticuloClave equals d.ArticuloClave
                   join e in dc.Proceso on a.ProcesoClave equals e.ProcesoClave
                   where a.catTipoProcesoClave == 1 && a.catEstatusClave != 6 && b.catEntidadTipoClave == 1
                    && (art==Guid.Empty|| d.ArticuloClave == art)
                   && (dis==Guid.Empty||  a.catDistribuidorClave==dis)
                   && (a.Fecha >= fi && a.Fecha <= fn)
                   select new salidas
                   {
                       distribuidor = b.catDistribuidor.Nombre.ToString(),
                       OrdenCompra = a.NumeroPedido,
                       status = a.catEstatus.Nombre,
                       almacen = b.Nombre,
                       fecha = a.Fecha.ToString(),
                       salida = a.NumeroPedido.ToString(),
                       cantidad = c.Cantidad.Value,
                       articulo = d.Nombre, cant_faltante=c.CantidadEntregada.Value
                       
                        
                   }
                          ).ToList();

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Distribuidor</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Almacén</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>#Orden</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Status</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Fecha</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Artículo</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Cantidad</i></b></td>");
            sb.Append(@"<td style=""font-size:8.5px;text-decoration: underline;"" align=""center""><b><i>Pendientes</i></b></td>");
            sb.Append("</tr>");
           
           int cant = 0;
            int cant_pend = 0;
            foreach (var a in sal.OrderBy(x=>x.OrdenCompra).ThenBy(o=>o.OrdenCompra)){
                cant = cant + a.cantidad;
                cant_pend = cant_pend + a.cant_faltante;
                sb.Append("<tr>");
                sb.Append(@"<td style=""font-size:7px;"" align=""center"">" + a.distribuidor + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + a.almacen + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + a.OrdenCompra + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + a.status + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + string.Format("{0:dd-MM-yyyy}", a.fecha.ToString()) + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + a.articulo + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + a.cantidad + "</td>");
                sb.Append(@"<td style=""font-size:8px;"" align=""center"">" + (a.cantidad - a.cant_faltante) + "</td>");
                sb.Append("</tr>");

                
            }

            sb.Append("</table>");

           // sb.Append("<b>Cantidad total pedida: </b>"+cant);
            //sb.Append("<b>Cantidad total pendiente: </b>" + cant_pend);

            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "movimientostecnico.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);


                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "MovimientosTecnico.pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "movimientostecnico.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
                    return View();
                }
            }
        }



    }
}
