﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using web.Models;

using web.Utils;
using Web.Controllers;

namespace web.Controllers
{
    public class DevolucionSACController : Controller
    {

        ModeloAlmacen dc = new ModeloAlmacen();
        //
        // GET: /DevolucionSAC/
        [SessionExpireFilterAttribute]

        public ActionResult Index()
        {
            Usuario u = ((Usuario)Session["u"]);
            
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();
            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();


            List<Entidad> alm = (from a in dc.AmbitoAlmacen join b in dc.Entidad on a.EntidadClaveAlmacen equals b.EntidadClave where a.UsuarioClave == u.UsuarioClave select b).ToList();
            List<Entidad> subalm = new List<Entidad>();
            Guid almCuartentena = Guid.Parse(WebConfigurationManager.AppSettings["almacenprincipal"].ToString());
            List<catTransportista> transp = dc.catTransportista.Where(x => x.Activo == true).ToList();
            if (u.DistribuidorClave != null)
            {
                subalm = (from a in dc.Entidad where a.EntidadClave== almCuartentena select a).ToList();


            }
            ViewData["subalmacenes"] = subalm;
            ViewData["almacenes"] = alm;
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tipo_articulos"] = tiposarticulo;
            //
            List<catEntidadTipo> tipoalmacen = dc.catEntidadTipo.Where(x => x.catEntidadTipoClave != 2).ToList();
            ViewData["RolUsuario"] = u.RolClave;
            ViewData["tipoalmacen"] = tipoalmacen;
            ViewData["transportistas"] = transp;
            //         
            return View();
        }

        public ActionResult ListaDevoluciones(string data, string almacen, int orden, int draw, int start, int length)
        {
            Guid alm;
            try
            { alm = Guid.Parse(almacen);}
            catch
            {
                alm = Guid.Empty;
            }
            lista_dev dataTableData = new lista_dev();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = Obtenerdevoluciones(ref recordsFiltered, alm, orden,start,length).OrderByDescending(x => x.pedido).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public List<Devolucion> Obtenerdevoluciones(ref int recordFiltered, Guid almacen, int orden, int start, int length)
        {
            
            List<Devolucion> dev;
            List<Devolucion> aux;
            Usuario u = (Usuario)Session["u"];          

                dev = (from a in dc.Proceso
                       join b in dc.AmbitoAlmacen on a.EntidadClaveSolicitante equals b.EntidadClaveAlmacen
                       where b.UsuarioClave == u.UsuarioClave && a.catTipoProcesoClave == 5 && (orden == 0|| (a.NumeroPedido == orden))
                       && (almacen == Guid.Empty || (almacen == a.EntidadClaveSolicitante))
                       select new Devolucion { pedido = a.NumeroPedido, almacenDestino=a.EntidadClaveAfectada.Value.ToString(), almacen = a.EntidadClaveSolicitante.Value.ToString(), estatus = a.catEstatusClave.Value, fecha = a.Fecha.Value, proceso=a.ProcesoClave }
                     ).OrderByDescending(o => o.pedido).ToList();
                aux = dev;
                recordFiltered = aux.Count;
            List<Devolucion> lista = new List<Devolucion>();
            foreach (var a in dev.Skip(start).Take(length).ToList())
            {
                Devolucion i = new Devolucion();
                i.proceso = a.proceso;
                i.pedido = a.pedido;
                i.fecha = a.fecha;
                i.fechaCorta = a.fecha.ToShortDateString();
                i.estatus = a.estatus;
                Guid ar = Guid.Parse(a.almacen);
                Guid ad = Guid.Parse(a.almacenDestino);
                Entidad al = dc.Entidad.Where(x => x.EntidadClave == ar).First();
                Entidad destino = dc.Entidad.Where(x => x.EntidadClave == ad).First();
                i.almacen = al.Nombre;
                i.almacenDestino = destino.Nombre;
                int env = 0;
                try
                {
                    List<ProcesoEnvio> pe = dc.ProcesoEnvio.Where(x => x.ProcesoClave == a.proceso).ToList();
                    if (pe.Count > 0)
                    {
                        env = 1;
                    }
                }
                catch { }
                i.tiene_envio = env;
                lista.Add(i);
            }
            return lista;

        }
        public ActionResult getInventario(Guid id, Guid almacen)
        {
           
            int cantidad = 0;
            Inv i = new Inv();
            try
            {
                i.Cantidad = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == 9).Sum(o => o.Cantidad.Value);
                i.InventarioClave = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == 9).Select(x => x.InventarioClave).First();
                i.tieneseries = tieneSeries(id);
            }
            catch
            {
                cantidad = 0;
            }
            return Json(i, JsonRequestBehavior.AllowGet);

        }
        public bool tieneSeries(Guid artclave)
        {
           
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public ActionResult ValidaSerie(string serie, Guid articulo, Guid almacen)
        {
           
            try
            {
                var s = (from a in dc.Serie
                         where a.Valor == serie && a.Inventario.ArticuloClave == articulo && a.Inventario.EntidadClave == almacen
                         where a.Inventario.EstatusInv == 9 && a.Inventario.Standby.Value == 0 && a.Inventario.Cantidad == 1
                         select new { a.InventarioClave, a.catSerieClave, a.Valor, a.Inventario.ArticuloClave, a.Inventario.Articulo.Nombre }).First();
                return Json(s, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult aparatos(Guid articulo, Guid plaza, string Articulos, string idDev, int draw, int start, int length)
        {

            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series> series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(plaza, articulo, series, idDev).Count;
            dataTableData.data = ObtenerAparatos(plaza, articulo, series, idDev).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public List<Aparatos> ObtenerAparatos(Guid plaza, Guid articulo, List<series> listaseries, string idDev)
        {
            
            bool dev;
            Guid Devolucion;
            try
            {
                dev = true;
                Devolucion = Guid.Parse(idDev);

            }
            catch
            {
                dev = false;
                Devolucion = Guid.Empty;
            }
            List<Aparatos> lista = (from a in dc.Serie where a.Inventario.EntidadClave == plaza && a.Inventario.ArticuloClave == articulo && a.Inventario.EstatusInv == 9 && a.Inventario.Cantidad.Value == 1 && a.Inventario.Standby.Value == 0 select new Aparatos { InventarioClave = a.InventarioClave, valor = a.Valor, Plaza = a.Inventario.EntidadClave.ToString(), ArticuloClave = a.Inventario.ArticuloClave.Value, Nombre = a.Inventario.Articulo.Nombre, estatus = a.Inventario.EstatusInv.Value }).OrderBy(x => x.valor).ThenBy(x => x.valor).ToList();

            if (dev)
            {

                List<Aparatos> aparatosdev = (from a in dc.ProcesoInventario
                                              where a.ProcesoClave == Devolucion && a.Inventario.Standby == 1 && a.Inventario.Estatus == 2 && a.Inventario.ArticuloClave == articulo
                                              select new Aparatos { InventarioClave = a.Inventario.InventarioClave, ArticuloClave = a.Inventario.ArticuloClave.Value, estatus = a.Inventario.EstatusInv.Value, valor = a.Inventario.Serie.Select(o => o.Valor).FirstOrDefault(), Nombre = a.Inventario.Articulo.Nombre, Plaza = a.Inventario.EntidadClave.Value.ToString() }
                                             ).ToList();
                lista.AddRange(aparatosdev);

            }
            foreach (var a in lista)
            {
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus = Int32.Parse(s.status);
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }

        public ActionResult GetTipoArticulo(int id)
        {
           
            var tipos = dc.catTipoArticulo.Where(x => x.catClasificacionArticuloClave == id && x.Activo == true ).Select(o => new { o.catTipoArticuloClave, o.Descripcion }).OrderBy(p=>p.Descripcion).ToList();
            return Json(tipos, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetArticulos(int id, Guid almacen)
        {
           
            var articulos = dc.Articulo.Where(o => o.Activo == true && o.catTipoArticuloClave == id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList();
            return Json(articulos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetalleDevolucion(Guid iddevolucion)
        {
            
            Proceso devolucion;
            DetalleDevo objretornar = new DetalleDevo();
            try
            {
                devolucion = dc.Proceso.Where(x => x.ProcesoClave == iddevolucion).First();
                objretornar.almacen = devolucion.EntidadClaveSolicitante.Value;
                objretornar.motivo = devolucion.Observaciones;
                objretornar.subalmacen = devolucion.EntidadClaveAfectada.Value;
               
            }
            catch
            {
                return Content("");
            }

            List<ProcesoInventario> pi = devolucion.ProcesoInventario.ToList();
            List<series> articulos = new List<series>();

            foreach (var a in pi)
            {
                series t = new series();
                t.ArticuloClave = a.Inventario.ArticuloClave.Value;
                t.status = a.Inventario.EstatusInv.Value.ToString();
                t.InventarioClave = a.InventarioClave;
                t.Nombre = a.Inventario.Articulo.Nombre;

                if (a.Inventario.Articulo.catTipoArticulo.catSerieClaveInterface == null)
                {
                    t.Cantidad = a.Cantidad.Value;
                    t.Valor = null;
                }
                else
                {
                    t.Cantidad = 1;
                    t.Valor = dc.Serie.Where(x => x.InventarioClave == a.Inventario.InventarioClave).Select(o => o.Valor).First();
                }

                articulos.Add(t);
            }
            objretornar.articulos = articulos;

            return Json(objretornar, JsonRequestBehavior.AllowGet);


        }

        public int cuantosdevolucion(Guid idDev, Guid idArticulo)
        {
           
            int cantidad = 0;
            try
            {
                Proceso devolucion = dc.Proceso.Where(x => x.ProcesoClave == idDev).First();
                cantidad = devolucion.ProcesoArticulo.Where(x => x.ArticuloClave == idArticulo).Select(o => o.Cantidad.Value).First();
            }
            catch { }

            return cantidad;
        }


        public ActionResult GuardaDevolucion(Guid almacen, string motivo, string articulos)
        {
            
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                   Guid almCuartentena = Guid.Parse(WebConfigurationManager.AppSettings["almacenprincipal"].ToString());
                    Entidad subalm = dc.Entidad.Where(o => o.EntidadClave == almCuartentena).First();
                    Entidad alm = dc.Entidad.Where(o => o.EntidadClave == almacen).First();
                    catDistribuidor distribuidor = dc.catDistribuidor.Where(x => x.catDistribuidorClave == alm.catDistribuidorClave).First();

                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    Usuario u = (Usuario)Session["u"];
                    Proceso p = new Proceso();
                    p.ProcesoClave = Guid.NewGuid();
                    p.catEstatusClave = 1;
                    p.catTipoProcesoClave = 5;
                    p.EntidadClaveSolicitante = alm.EntidadClave;
                    p.EntidadClaveAfectada =subalm.EntidadClave;
                    p.UsuarioClave = u.UsuarioClave;
                    p.Fecha = DateTime.Now;
                    p.Observaciones = motivo;
                    p.catDistribuidorClave = distribuidor.catDistribuidorClave;
                    dc.Proceso.Add(p);


                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g
                                     select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach (var a in result)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = p.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                    }

                    foreach (var Articulo in series)
                    {

                        Inventario Inv = new Inventario();
                        try
                        {
                            Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == almacen && o.EstatusInv == 9).First();
                        }
                        catch { }
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            if (Inv.Standby == 1 && Inv.Cantidad.Value == 0)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-1");
                            }
                            else if (Inv.EntidadClave != alm.EntidadClave)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-1");
                            }
                            else
                            {
                                Inv.Standby = 1;
                                Inv.Cantidad = 0;
                                Inv.Estatus = 2;
                                

                                
                            }


                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-2");
                            }
                            else
                            {
                                Inv.Standby = Inv.Standby.Value + Articulo.Cantidad;
                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;


                                

                            }


                        }
                        //dc.SaveChanges();
                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }


        }
        public bool Espagare(Guid artclave)
        {
            
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.EsPagare.Value == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult EditaDevolucion(string motivo, string articulos, Guid devolucion)
        {
            
            Proceso Devolucion;
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Entidad alm;
                    catDistribuidor distribuidor;
                    try
                    {
                        Devolucion = dc.Proceso.Where(x => x.ProcesoClave == devolucion).First();
                        alm = dc.Entidad.Where(o => o.EntidadClave == Devolucion.EntidadClaveSolicitante).First();
                        distribuidor = dc.catDistribuidor.Where(x => x.catDistribuidorClave == alm.catDistribuidorClave).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -1;
                        e1.Mensaje = "No existe la devolución o existe un error de escritura en almacén solicitante";
                        return Json(e1, JsonRequestBehavior.AllowGet);
                    }
                    if (Devolucion.catEstatusClave == 4)
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -7;
                        e1.Mensaje = "La orden no se puede editar,ya fue recepcionada por el almacén central";
                        return Json(e1, JsonRequestBehavior.AllowGet);
                    }

                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    
                    #region Eliminando devolucion  pasada
                    try
                    {
                        List<ProcesoArticulo> pa = Devolucion.ProcesoArticulo.ToList();
                        dc.ProcesoArticulo.RemoveRange(pa);

                        List<ProcesoInventario> pi = Devolucion.ProcesoInventario.ToList();
                        foreach (var p in pi)
                        {
                            if (tieneSeries(p.Inventario.ArticuloClave.Value))
                            {
                                p.Inventario.Cantidad = 1;
                                p.Inventario.Standby = 0;
                                p.Inventario.Estatus = 1;
                               
                               

                            }
                            else
                            {
                                Inventario Inv = dc.Inventario.Where(o => o.InventarioClave == p.Inventario.InventarioClave && o.ArticuloClave == p.Inventario.ArticuloClave && o.EntidadClave == alm.EntidadClave && o.EstatusInv == 9).First();

                                Inv.Cantidad = Inv.Cantidad + p.Cantidad;
                                Inv.Standby = Inv.Standby - p.Cantidad;
                                

                            }

                            dc.SaveChanges();

                        }
                        dc.SaveChanges();

                        dc.ProcesoInventario.RemoveRange(pi);

                        dc.SaveChanges();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -2;
                        e1.Mensaje = "No se pudo eliminar detalles de devolución pasada";
                        return Json(e1, JsonRequestBehavior.AllowGet);
                    }
                    #endregion
                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g
                                     select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };
                    foreach (var a in result)
                    {
                        ProcesoArticulo newpa = new ProcesoArticulo();
                        newpa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        newpa.Cantidad = a.Cantidad;
                        newpa.ProcesoClave = Devolucion.ProcesoClave;
                        dc.ProcesoArticulo.Add(newpa);
                    }
                    foreach (var Articulo in series)
                    {
                        Inventario Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == alm.EntidadClave && o.EstatusInv == 9).First();

                        ProcesoInventario pinew = new ProcesoInventario();
                        pinew.ProcesoClave = Devolucion.ProcesoClave;
                        pinew.InventarioClave = Articulo.InventarioClave;
                        pinew.Cantidad = Articulo.Cantidad;
                        pinew.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pinew);

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            if (Inv.Standby == 1 && Inv.Cantidad.Value == 0)
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -3;
                                e1.Mensaje = "El artículo ya se encuentra en devolución";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }
                            else if (Inv.EntidadClave != alm.EntidadClave)
                            {
                                dbContextTransaction.Rollback();
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -4;
                                e1.Mensaje = "El artículo no se encuentra en el almacén solicitado";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                Inv.Standby = 1;
                                Inv.Cantidad = 0;
                                Inv.Estatus = 2;
                                

                            }
                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -5;
                                e1.Mensaje = "La cantidad en almacén es menor a la solicitada por la orden";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                Inv.Standby = Inv.Standby.Value + Articulo.Cantidad;
                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;


                                

                            }


                        }
                        //dc.SaveChanges();
                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(Devolucion.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);




                }
                catch
                {

                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -6;
                    e1.Mensaje = "El sistema evitó la generación de la devolución ya que se encontraron errores en ella";
                    return Json(e1, JsonRequestBehavior.AllowGet);

                }

            }





        }

        public ActionResult Detalle()
        {
           


            if (Request["d"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["d"]);
            int pdf = Convert.ToInt32(Request["pdf"]);
            Proceso pro = (from p in dc.Proceso
                           where p.ProcesoClave == clave
                           select p).SingleOrDefault();
            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();
            Entidad eS = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveSolicitante
                          select e).SingleOrDefault();
            Entidad eA = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveAfectada
                          select e).SingleOrDefault();
            Usuario u = pro.Usuario;
            catEstatus vEst = (from e in dc.catEstatus
                               where e.catEstatusClave == pro.catEstatusClave
                               select e).SingleOrDefault();

            StringBuilder sb = new StringBuilder();
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h3><b>SALIDA DE MATERIAL</b></h3>");            
            if (pro.catTipoProcesoClave == 5)
            {
                sb.Append("<h3><b>Devolución SAC al Almacén Central #" + pro.NumeroPedido + "</b></h3>");
            }
            if (pro.catTipoProcesoClave == 6)
            {
                sb.Append("<h3>Recepción de material por devolución #" + pro.NumeroPedido + "</b></h3>");
            }
            if (pro.catTipoProcesoClave == 7)
            {
                sb.Append("<h3><b>Habilitación de equipos #" + pro.NumeroPedido + "</b></h3>");
            }
            if (pro.catTipoProcesoClave == 8)
            {
                sb.Append("<h3><b>Baja de Equipos #" + pro.NumeroPedido + "</b></h3>");
            }
            if (pro.catTipoProcesoClave == 9)
            {
                sb.Append("<h3><b>Orden de Garantía #" + pro.NumeroPedido + "</b></h3>");
            }
            if (pro.catTipoProcesoClave == 18)
            {
                sb.Append("<h3><b>Recepción de material por Garantía #" + pro.NumeroPedido + "</b></h3>");
            }

            sb.Append("<b>Fecha: </b>"+DateTime.Today.ToShortDateString());

            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<font size=2>");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append("<td>");
            if (pro.catTipoProcesoClave == 5)
            {


                sb.Append("<b>Enviar a</b><br>");
                sb.Append(eA.Nombre + "<br>");
                sb.Append("<b>Enviado de</b><br>");
                sb.Append(eS.Nombre + "<br>");
            }           
            sb.Append("<b>Tipo de Proceso</b><br>");
            sb.Append(pro.catTipoProceso.Nombre + "<br>");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>Estatus Proceso </b><br>");
            sb.Append(vEst.Nombre + "<br>");

            if (pro.catTipoProcesoClave == 9)
            {
                sb.Append("<b>Proveedor</b><br>");
                sb.Append(pro.catProveedor.Nombre+"<br>");
            }

            if (pro.catTipoProcesoClave == 5)
            {
                List<ProcesoEnvio> pe = dc.ProcesoEnvio.Where(o => o.ProcesoClave == pro.ProcesoClave).ToList();
                if (pe.Count > 0)
                {
                    sb.Append("<b>Datos de envio: </b><br>");
                    sb.Append("<b>Transportista</b><br>");
                    sb.Append(pro.ProcesoEnvio.catTransportista.Descripcion);
                    sb.Append("<br>");
                    sb.Append("<b>Guia</b><br>");
                    sb.Append(pro.ProcesoEnvio.GuiaEnvio);
                }


            }
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");



            if (pro.catTipoProcesoClave == 5)
            {
                sb.Append("<b>Motivo </b><br>");
                sb.Append(pro.Observaciones + "<br>");
            }
            sb.Append("<br><br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th>Cantidad</th>");
            sb.Append(@"<th colspan=""3"">Concepto</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                if (pa.Cantidad > 0)
                {
                    Articulo a = pa.Articulo;

                    sb.Append("<tr>");
                    sb.Append(@"<td align=""right"">");
                    sb.Append(pa.Cantidad);
                    sb.Append("</td>");
                    sb.Append(@"<td colspan=""3"">");
                    sb.Append(a.Nombre);
                    sb.Append("<br>");

                    foreach (ProcesoInventario pi in procesoinventario)
                    {
                        Inventario i = pi.Inventario;
                        if (i.ArticuloClave == a.ArticuloClave)
                        {
                            List<Serie> series = i.Serie.ToList();
                            int conta = 0;
                            var innerCount = 0;
                            foreach (Serie s in series)
                            {
                                if (innerCount > 0)
                                {
                                    sb.Append("-");
                                }
                                string val = "";
                                if (s.Inventario.EstatusInv == 7)
                                {
                                    val = "Buen Estado";
                                }
                                else if (s.Inventario.EstatusInv == 9)
                                {
                                    val = "Revisión";
                                }
                                else if (s.Inventario.EstatusInv == 10)
                                {
                                    val = "Dañado";
                                }
                                else
                                {
                                    val = "Baja";
                                }
                                sb.Append(s.Valor + "->" + val);
                                innerCount++;
                            }

                            conta++;


                            if (conta > 0)
                            {
                                sb.Append("<br>");
                            }
                        }
                    }
                }
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");

            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "_dev-almcentral.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Dev-AlmCentral_" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "_dev-almcentral.pdf" + "' style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }
        }

        public ActionResult detalleEnvio(Guid proceso)
        {

           
            ProcesoEnvio p = dc.ProcesoEnvio.Where(x => x.ProcesoClave == proceso).First();

            envio e = new envio();

            e.fecha = p.FechaEnvio.ToString("dd/MM/yyyy");
            e.guia = p.GuiaEnvio.ToString();
            e.tran = p.catTranspClave;
            return Json(e, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Cancela(Guid proceso)
        {
            
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Proceso devolucion;
                    try
                    {
                        devolucion = dc.Proceso.Where(x => x.ProcesoClave == proceso).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-1");
                    }
                    if (devolucion.catEstatusClave.Value == 6)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-2");
                    }
                    if (devolucion.catEstatusClave == 4)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-3");
                    }

                    devolucion.catEstatusClave = 6;

                    List<ProcesoInventario> pi = dc.ProcesoInventario.Where(x => x.ProcesoClave == proceso).ToList();
                    foreach (var Articulo in pi)
                    {

                        if (tieneSeries(Articulo.Inventario.ArticuloClave.Value))
                        {
                            Articulo.Inventario.Standby = 0;
                            Articulo.Inventario.Cantidad = 1;
                            Articulo.Inventario.Estatus = 1;
                            Articulo.Inventario.EstatusInv = 7;

                            
                        }
                        else
                        {
                            Articulo.Inventario.Standby = Articulo.Inventario.Standby - Articulo.Cantidad;
                            Articulo.Inventario.Cantidad = Articulo.Inventario.Cantidad + Articulo.Cantidad;                           
                        }


                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(devolucion.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }

            }


        }

        #region classHelpers



        public class envio
        {
            public int tran { get; set; }
            public string guia { get; set; }
            public string fecha { get; set; }
        }
        public class Devolucion
        {
            public long pedido { get; set; }
            public DateTime fecha { get; set; }
            public string fechaCorta { get; set; }
            public string almacen { get; set; }
            public string almacenDestino { get; set; }
            public int estatus { get; set; }
            public Guid proceso { get; set; }

            public int tiene_envio { get; set; }

        }

        public class DetalleDevo
        {
            public Guid almacen { get; set; }
            public Guid subalmacen { get; set; }
            public string motivo { get; set; }
            public List<series> articulos { get; set; }

        }

        public class lista_dev
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Devolucion> data { get; set; }
        }

        public ActionResult Envio(Guid proceso, int trans, string guia, string fecha)
        {
            try
            {
               

                if (dc.ProcesoEnvio.Any(x => x.ProcesoClave == proceso))
                {
                    ProcesoEnvio p = dc.ProcesoEnvio.Where(o => o.ProcesoClave == proceso).First();
                    p.catTranspClave = trans;
                    p.GuiaEnvio = guia;
                    p.FechaEnvio = DateTime.Parse(fecha);

                    dc.SaveChanges();
                    return Content("1");
                }
                else
                {

                    ProcesoEnvio pe = new ProcesoEnvio();
                    pe.ProcesoClave = proceso;
                    pe.catTranspClave = trans;
                    pe.GuiaEnvio = guia;
                    pe.FechaEnvio = DateTime.Parse(fecha);
                    dc.ProcesoEnvio.Add(pe);
                    dc.SaveChanges();
                    return Content("1");
                }
            }
            catch
            {
                return Content("0");
            }


        }

        #endregion

    }
}
