﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class UnidadController : Controller
    {
        //
        // GET: /Unidad/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            int pags = (from ro in dc.catUnidad
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div - 1).ToString());
            }
            else
            {
                return Content((div).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<catUnidad> rls = (from use in dc.catUnidad
                                                      orderby use.Nombre
                                                      select use).Skip(skip).Take(elementos).ToList();

            List<catUnidadWrapper> rw = new List<catUnidadWrapper>();

            foreach (catUnidad u in rls)
            {
                catUnidadWrapper w = UnidadToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Obten(string data)
        {
            catUnidadWrapper rw = new catUnidadWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catUnidad r = (from ro in dc.catUnidad
                                              where ro.catUnidadClave == uid
                                              select ro).SingleOrDefault();

            if (r != null)
            {
                rw = UnidadToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catUnidad r = (from ro in dc.catUnidad
                                                      where ro.catUnidadClave == uid
                                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catUnidad r = (from ro in dc.catUnidad
                                       where ro.catUnidadClave == uid
                                       select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_activo)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catUnidad unidad = (from cu in dc.catUnidad
                                        where cu.Nombre == ed_nombre
                                        select cu).SingleOrDefault();

               


                catUnidad r = new catUnidad();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catUnidad
                         where ro.catUnidadClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Nombre = ed_nombre;
                r.Activo = ed_activo == null ? false : ed_activo.Length > 0;

                if (ed_id == String.Empty)
                {
                    dc.catUnidad.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }






        private catUnidadWrapper UnidadToWrapper(catUnidad u)
        {
            catUnidadWrapper rw = new catUnidadWrapper();
            rw.Clave = u.catUnidadClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }
    }
}
