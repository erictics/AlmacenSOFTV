﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class BitacoraTecnicoController : Controller
    {
        //
        // GET: /Articulo/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();


            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where  ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();

//ta.SerieTipoArticulo.Count==0 &&

            List<Entidad> almacenes = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                almacenes = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                     && ta.catDistribuidorClave == null
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {

                List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                                  where a.UsuarioClave == u.UsuarioClave
                                                  select a).ToList();
                almacenes = new List<Entidad>();


                foreach (AmbitoAlmacen a in ambito)
                {
                    Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        almacenes.Add(e);
                    }
                }
            }


            List<Entidad> tecnicos = null;

            if (u.DistribuidorClave == null)
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == null
                            orderby ta.Nombre
                            select ta).ToList();
            }
            else
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == u.DistribuidorClave
                            orderby ta.Nombre
                            select ta).ToList();
            }



            List<catTipoSalida> tipossalida = (from ta in dc.catTipoSalida
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();




            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;

            ViewData["almacenes"] = almacenes;
            ViewData["tecnicos"] = tecnicos;
            ViewData["tipossalida"] = tipossalida;

            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");

            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;



            int cuantos = (from use in dc.Proceso
                           where
                           use.catDistribuidorClave != null
                           && use.catTipoProcesoClave == 1
                           orderby use.Fecha descending
                           select use).Count();
            ViewData["cuantos"] = cuantos;


            List<Proceso> salidaspendientes = (from use in dc.Proceso
                                                   where use.catDistribuidorClave != null && use.catTipoProcesoClave == 1
                                                   orderby use.Fecha descending
                                                   select use).ToList();

            ViewData["salidaspendientes"] = salidaspendientes;
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        int elementos = 15;
        

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<SalidaMaterialWrapper> data { get; set; }
        }


        public ActionResult ObtenListaBitacora(string data, int draw, int start, int length)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            string contrato;
            try
            {
               contrato = jObject["pag"].ToString();
            }
            catch
            {
                 contrato = "";
            }
            
               int  vnEstatus = 0;
            Guid pedido = Guid.Empty,
                vgAlmacen = Guid.Empty;            try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }

            try
            {
                vgAlmacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch { }

            try
            {
                vnEstatus = int.Parse(jObject["estatus"].ToString());
            }
            catch { }

            Guid tecnico = Guid.Empty;

            try
            {
                tecnico = Guid.Parse(jObject["tecnico"].ToString());
            }
            catch { }


            Guid plaza = Guid.Empty;
            try
            {
                plaza = Guid.Parse(jObject["plaza"].ToString());
            }
            catch { }

            string tipobitacora = String.Empty;
            try
            {
                tipobitacora = jObject["tipobitacora"].ToString();
            }
            catch { }


            int tipoentidad = 2;

            try
            {
                tipoentidad = Convert.ToInt32(jObject["tipoentidad"].ToString());
            }
            catch
            {

            }


            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData2(ref recordsFiltered, start, length, pedido, vgAlmacen, vnEstatus, tecnico, plaza, tipobitacora, tipoentidad,contrato);
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);


           
        }

        private List<SalidaMaterialWrapper> FilterData2(ref int recordFiltered, int start, int length, Guid pedido, Guid vgAlmacen, int vnEstatus, Guid tecnico, Guid plaza, string tipobitacora, int tipoentidad,string contrato)
        {
             ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = (Usuario)Session["u"];
            List<BitacoraSofTV> rls = null;
            List<BitacoraSofTV> rls2 = null;
            if (u.DistribuidorClave == null)
            {
                rls = (from use in dc.BitacoraSofTV
                       where
                       (tecnico == Guid.Empty || tecnico == use.EntidadClaveTecnico)
                       && use.catDistribuidorClave == null
                       && (use.TipoServicio != "A" && use.TipoServicio != "C")
                       && (plaza == Guid.Empty || use.EntidadClaveAlmacen == plaza)
                       && (tipobitacora == "" || use.TipoServicio == tipobitacora )
                       orderby use.Fecha descending
                       select use).ToList();             

                recordFiltered = rls.Count();
            }
            else
            {

                if (contrato != "")
                {
                    rls = (from use in dc.BitacoraSofTV where use.Contrato == contrato select use).ToList();
                    
                }
                else
                {
                    rls = (from use in dc.BitacoraSofTV
                           where
                           (tecnico == Guid.Empty || tecnico == use.EntidadClaveTecnico)
                           && use.catDistribuidorClave == u.DistribuidorClave
                           && (use.TipoServicio != "A" && use.TipoServicio != "C")
                           && (plaza == Guid.Empty || use.EntidadClaveAlmacen == plaza)
                           && (tipobitacora == "" || use.TipoServicio == tipobitacora )
                           orderby use.Fecha descending
                           select use).ToList();
                    recordFiltered = rls.Count();
                }              
               
            }
            List<SalidaMaterialWrapper> rw = new List<SalidaMaterialWrapper>();

            foreach (BitacoraSofTV p in rls.Skip(start).Take(length).ToList())
            {
                SalidaMaterialWrapper w = SalidaMaterialBitacoraToWrapper(p, false);
                rw.Add(w);
            }

            return rw;


        }

        

        private SalidaMaterialWrapper SalidaMaterialBitacoraToWrapper(BitacoraSofTV u, bool arts)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            SalidaMaterialWrapper rw = new SalidaMaterialWrapper();
            rw.Clave = u.BitacoraSofTVClave.ToString();

            rw.EntidadClaveSolicitante = null;
            rw.EntidadClaveAfectada = u.EntidadClaveTecnico.Value.ToString();

            rw.NumeroPedido = u.BitacoraSofTVClave.ToString();
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");

            try
            {
                rw.BitacoraLetra = u.TipoServicio.ToString();
                rw.BitacoraNumero = u.NumeroOrden.ToString();

                if (rw.BitacoraNumero == "")
                {
                    rw.BitacoraNumero = u.Contrato;
                }

                rw.Plaza = u.EntidadClaveAlmacen.ToString();
            }
            catch
            {
            }
          Entidad tec = (from ea in dc.Entidad
                           where ea.EntidadClave == u.EntidadClaveTecnico
                           select ea).SingleOrDefault();

            if (tec != null)
            {
                rw.Destino = tec.Nombre;
            }

            return rw;
        }
        

        public ActionResult DetalleSofTV()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            if (Request["s"] == null) {
                return Content("");
            }

            int clave = Convert.ToInt32(Request["s"]);
            int pdf = Convert.ToInt32(Request["pdf"]);
            BitacoraSofTV pro = (from p in dc.BitacoraSofTV
                               where p.BitacoraSofTVClave == clave
                               select p).SingleOrDefault();

            StringBuilder sb = new StringBuilder();
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");


            sb.Append(@"<td align=""center"">");
            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h3><b>BITÁCORA DE MATERIAL</b></h3>");
            sb.Append("<h3<b>Bitacora SofTV: #</b>" + pro.BitacoraSofTVClave + "</h3>");
           

            Entidad almacen = (from a in dc.Entidad
                               where a.EntidadClave == pro.EntidadClaveAlmacen
                               select a).SingleOrDefault();
            sb.Append(@"<b  style=""font-size:10px;"">Almacén:</b><span  style=""font-size:10px;"">" + almacen.Nombre + "</span> <br>");

            Entidad tecnico = (from a in dc.Entidad
                               where a.EntidadClave == pro.EntidadClaveTecnico
                               select a).SingleOrDefault();

            sb.Append(@"<b  style=""font-size:10px;"">Técnico:</b><span>" + tecnico.Nombre + "</span><br>");
          
            sb.Append("<br>");
            sb.Append("<b>Tipo de bitátora: </b>");
            switch (pro.TipoServicio)
            {
                case "O":
                    sb.Append("Orden de Trabajo<br> ");
                    break;
                case "Q":
                    sb.Append("Queja<br>");
                    break;
                case "M":
                    sb.Append("Mantenimiento<br>");
                    break;
                case "C":
                    sb.Append("Bitácora Cancelada Manualmente<br>");
                    break;
                case "A":
                    sb.Append("Bitácora Manual Almacén<br>");
                    break;
            }
            sb.Append("<b>Número: </b>");
            if (pro.NumeroOrden == null)
            {
                sb.Append(pro.NumeroOrden + "<br>");
            }
            else
            {
                sb.Append(pro.Contrato + "<br>");
            }
            sb.Append(@"<b style=""font-size:10px;"">Fecha: </b><span style=""font-size:10px;"">" + pro.Fecha.Value.ToShortDateString() + "</span><br>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append(@"</table>");
            

            sb.Append("<br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead><tr>");
            sb.Append(@"<th style=""font-size:10px;""><b>Cantidad</b></th>");
            sb.Append(@"<th  style=""font-size:10px;"" colspan=""3""><b>Concepto</b></th>");
            sb.Append(@"<th style=""font-size:10px;"" ><b>Unidad</b></th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            List<BitacoraSofTVDetalle> arts = pro.BitacoraSofTVDetalle.ToList().OrderBy(o=>o.Inventario.Articulo.catTipoArticulo.Descripcion).ToList();
            sb.Append("<tbody>");

            foreach (BitacoraSofTVDetalle pa in arts)
            {
                Inventario inv = pa.Inventario;
                    Articulo a = inv.Articulo;
                sb.Append("<tr>");
                sb.Append(@"<td style=""font-size:10px;"">" + pa.Cantidad + "</td>");
                sb.Append(@"<td style=""font-size:10px;"" colspan=""3"">" + a.Nombre + "</td>");
                sb.Append(@"<td style=""font-size:10px;"" >" + a.catUnidad.Nombre + "</td>");
                sb.Append("</tr>");
                List<Serie> series = inv.Serie.ToList();
                sb.Append(@"<tr colspan=5>");
                sb.Append(@"<td style=""font-size:8px;"">");
                if (a.catTipoArticulo.SerieTipoArticulo.Count() > 0)
                {                   
                    sb.Append("<b>Series:</b><br>");
                    series.ForEach(se =>
                    {
                        sb.Append("<span>" + se.Valor + "-</span>");
                    });
                }
                sb.Append("</td>");
                sb.Append("</tr>");

            }            
            sb.Append("</tbody>");
            sb.Append("</table>");
            if (pdf == 1){
                sb.Append("<br><br>");
            }
            sb.Append("</font>");
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "pedido.pdf";

                elpdf.HTMLToPdfBitacora(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "BitacotaSofTV" + pro.BitacoraSofTVClave.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "pedido.pdf" + "' style='position: relative; height: 90%; width: 90%;' frameborder=0></iframe>";
                    return View();
                }
            }



        }


    }
}
