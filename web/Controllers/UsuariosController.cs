﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class UsuariosController : Controller
    {
        //
        // GET: /Usuarios/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Rol> roles = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                roles = (from r in dc.Rol
                         where r.Activo == true
                         orderby r.Nombre
                         select r).ToList();
            }
            else {
                roles = (from r in dc.Rol
                         where r.Activo == true
                            && r.RolClave != 0
                         orderby r.Nombre
                         select r).ToList();            
            }

            ViewData["roles"] = roles;


            List<catDistribuidor> distribuidores = null;

            if (u.DistribuidorClave == null)
            {
                distribuidores = (from d in dc.catDistribuidor
                                  where d.Activo == true
                                  orderby d.Nombre
                                  select d).ToList();
            }
            else
            {
                distribuidores = (from d in dc.catDistribuidor
                                  where d.Activo == true && d.catDistribuidorClave == u.DistribuidorClave
                                  orderby d.Nombre
                                  select d).ToList();
            }
            ViewData["distribuidores"] = distribuidores;
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        int elementos = 15;


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<UsuarioWrapper> data { get; set; }
        }

        public ActionResult ObtenUsuarios(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ObtenLista(data).OrderBy(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ObtenLista(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }





        public List<UsuarioWrapper> ObtenLista(string data)
        {

            Usuario u = ((Usuario)Session["u"]);

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());
            int rol = Convert.ToInt32(jObject["rol"].ToString());
            Guid distribuidor = Guid.Empty;

            distribuidor = Guid.Parse(jObject["distribuidor"].ToString());

            string keyword = jObject["keyword"].ToString().ToLower();

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<Usuario> us = null;

            if (u.DistribuidorClave == null)
            {
                us = (from use in dc.Usuario
                      where (rol == -1 || use.RolClave == rol)
                         && (keyword == String.Empty || (use.Nombre + " " + use.CorreoElectronico).ToLower().Contains(keyword))
                         && ((distribuidor == Guid.Empty && use.DistribuidorClave == null) || (distribuidor == use.DistribuidorClave))

                      orderby use.Nombre
                      select use).Skip(skip).Take(elementos).ToList();
            }
            else
            {
                us = (from use in dc.Usuario
                      where (rol == -1 || use.RolClave == rol)
                         && use.DistribuidorClave == u.DistribuidorClave
                         && (keyword == String.Empty || (use.Nombre + " " + use.CorreoElectronico).ToLower().Contains(keyword))
                         && ((distribuidor == use.DistribuidorClave))

                      orderby use.Nombre
                      select use).Skip(skip).Take(elementos).ToList();
            }



            List<UsuarioWrapper> uw = new List<UsuarioWrapper>();

            foreach (Usuario ui in us)
            {
                UsuarioWrapper w = UsuarioToWrapper(ui);
                uw.Add(w);
            }

            return uw;
        }

      

        public ActionResult Obten(string data)
        {
            UsuarioWrapper uw = new UsuarioWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid uid = Guid.Parse(data);

            Usuario u = (from us in dc.Usuario
                 where us.UsuarioClave == uid
                 select us).SingleOrDefault();

            if (u != null)
            {
                uw = UsuarioToWrapper(u);
                return Json(uw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    Usuario r = (from ro in dc.Usuario
                                 where ro.UsuarioClave == uid
                                 select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Almacenes()
        {

            ModeloAlmacen dc = new ModeloAlmacen();

//usuarioalmacen_id

            Guid usuarioclave = Guid.Parse(Request["usuarioalmacen_id"]);

            List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                              where a.UsuarioClave == usuarioclave
                                              select a).ToList();

            foreach (AmbitoAlmacen a in ambito)
            {
                dc.AmbitoAlmacen.Remove(a);
            }
            dc.SaveChanges();



            Guid distribuidorguid = Guid.Parse(Request["distribuidoralmacen_id"]);

            List<Entidad> rl = (from ro in dc.Entidad
                                    where ro.catDistribuidorClave == distribuidorguid
                                    
                                    select ro).ToList();

            List<AlmacenWrapper> rwl = new List<AlmacenWrapper>();

            foreach (Entidad r in rl)
            {

                if (Request["almacen" + r.EntidadClave] == "on")
                {
                    AmbitoAlmacen aa = new AmbitoAlmacen();
                    aa.AmbitoAlmacenClave = Guid.NewGuid();
                    aa.UsuarioClave = usuarioclave;
                    aa.EntidadClaveAlmacen = r.EntidadClave;

                    dc.AmbitoAlmacen.Add(aa);
                }
            }
            dc.SaveChanges();


            return Content("0");
        }

        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_email, string ed_rol, string ed_distribuidor, string edfirma_valor, string ed_estatus)
        {

            

            ModeloAlmacen dc = new ModeloAlmacen();

            List<Usuario> usu = (from a in dc.Usuario where a.CorreoElectronico == ed_email.ToLower() select a).ToList();
            if (usu.Count() > 0 && ed_id == String.Empty)
            {

               return Content("-2");
            }
            else
            {





                try
                {



                    Usuario u = new Usuario();

                    if (ed_id != String.Empty)
                    {
                        Guid uid = Guid.Parse(ed_id);

                        u = (from us in dc.Usuario
                             where us.UsuarioClave == uid
                             select us).SingleOrDefault();
                    }
                    else
                    {
                        u.UsuarioClave = Guid.NewGuid();
                        u.Activo = true;
                        u.Password = "123";
                    }


                    u.Nombre = ed_nombre;
                    u.CorreoElectronico = ed_email;
                    u.RolClave = Convert.ToInt32(ed_rol);

                    u.Firma = edfirma_valor;

                    if (ed_distribuidor != String.Empty && ed_distribuidor != "-1")
                    {
                        u.DistribuidorClave = Guid.Parse(ed_distribuidor);
                    }
                    else
                    {
                        u.DistribuidorClave = null;
                    }

                    if (ed_estatus == "0")
                    {
                        u.Activo = false;
                    }
                    else
                    {
                        u.Activo = true;
                    }


                   


                    if (ed_id == String.Empty)
                    {
                        dc.Usuario.Add(u);
                    }
                    dc.SaveChanges();



                    Guid usuarioclave = u.UsuarioClave;

                    List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                                      where a.UsuarioClave == usuarioclave
                                                      select a).ToList();

                    foreach (AmbitoAlmacen a in ambito)
                    {
                        dc.AmbitoAlmacen.Remove(a);
                    }
                    dc.SaveChanges();




                    Guid? distribuidorguid = null;
                    distribuidorguid = u.DistribuidorClave;

                    List<Entidad> rl = (from ro in dc.Entidad
                                            where ro.catDistribuidorClave == distribuidorguid

                                            select ro).ToList();

                    List<AlmacenWrapper> rwl = new List<AlmacenWrapper>();

                    foreach (Entidad r in rl)
                    {

                        if (Request["almacen" + r.EntidadClave] == "on")
                        {
                            AmbitoAlmacen aa = new AmbitoAlmacen();
                            aa.AmbitoAlmacenClave = Guid.NewGuid();
                            aa.UsuarioClave = usuarioclave;
                            aa.EntidadClaveAlmacen = r.EntidadClave;

                            dc.AmbitoAlmacen.Add(aa);
                        }
                    }
                    dc.SaveChanges();
                    return Content("1");
                }
                catch
                {
                    return Content("0");
                }
            }
        }

        private UsuarioWrapper UsuarioToWrapper(Usuario u){
            UsuarioWrapper uw = new UsuarioWrapper();
            uw.Id = u.UsuarioClave.ToString();
            uw.Nombre = u.Nombre;
            uw.Rol = u.Rol.Nombre;
            uw.RolId = u.RolClave.ToString();
            uw.Firma = u.Firma;

            if (u.catDistribuidor != null)
            {
                uw.Sucursal = u.catDistribuidor.Nombre;
                uw.SucursalClave = u.DistribuidorClave.ToString();
            }
            else
            {
                uw.Sucursal = "--";
                uw.SucursalClave = "-1";
            }
            uw.Correo = u.CorreoElectronico;
            uw.Asignaciones = String.Empty;
            uw.Activo = u.Activo;
            return uw;
        }

        public ActionResult queusuariosoy()
        {
            Usuario u = ((Usuario)Session["u"]);

            try
            {
                if (u.DistribuidorClave.HasValue)
                {
                    return Json("AD", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("ADM", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }

        }



    }
}
