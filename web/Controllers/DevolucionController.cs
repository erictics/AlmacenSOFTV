﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using web.Utils;
using web.Wrappers;
using Web.Controllers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;


using System.Globalization;

using web.Models;

namespace web.Controllers
{
    public class DevolucionController : Controller
    {
        ModeloAlmacen dc = new ModeloAlmacen();

        public ActionResult getInventario(Guid id, Guid almacen, int status)
        {
            Inv i = new Inv();
            try
            {
                i.Cantidad = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == status).Sum(o => o.Cantidad.Value);
                i.InventarioClave = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == status).Select(x => x.InventarioClave).First();
                i.tieneseries = tieneSeries(id);
            }
            catch
            {
            }
            return Json(i, JsonRequestBehavior.AllowGet);

        }

        public bool tieneSeries(Guid artclave)
        {
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult aparatos(Guid articulo, Guid plaza, string Articulos, string idDev, int status, int draw, int start, int length)
        {

            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series> series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(plaza, articulo, series, idDev, status).Count;
            dataTableData.data = ObtenerAparatos(plaza, articulo, series, idDev, status).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Aparatos> ObtenerAparatos(Guid plaza, Guid articulo, List<series> listaseries, string idDev, int status)
        {
            bool dev;
            Guid Devolucion;
            try
            {
                dev = true;
                Devolucion = Guid.Parse(idDev);
            }
            catch
            {
                dev = false;
                Devolucion = Guid.Empty;
            }
            List<Aparatos> lista = (from a in dc.Serie
                                    where a.Inventario.EntidadClave == plaza && a.Inventario.ArticuloClave == articulo && a.Inventario.EstatusInv == status && a.Inventario.Cantidad.Value == 1 && a.Inventario.Standby.Value == 0
                                    select new Aparatos { InventarioClave = a.InventarioClave, valor = a.Valor, Plaza = a.Inventario.EntidadClave.ToString(), ArticuloClave = a.Inventario.ArticuloClave.Value, Nombre = a.Inventario.Articulo.Nombre, estatus = a.Inventario.EstatusInv.Value }).OrderBy(x => x.valor).ThenBy(x => x.valor).ToList();

            if (dev)
            {

                List<Aparatos> aparatosdev = (from a in dc.ProcesoInventario
                                              where a.ProcesoClave == Devolucion
                                              && a.Inventario.Standby == 1
                                              && (a.Inventario.Estatus == 3 || a.Inventario.Estatus == 2)
                                              && a.Inventario.ArticuloClave == articulo
                                              select new
                                              Aparatos
                                              {
                                                  InventarioClave = a.Inventario.InventarioClave,
                                                  ArticuloClave = a.Inventario.ArticuloClave.Value,
                                                  estatus = a.Inventario.EstatusInv.Value,
                                                  valor = a.Inventario.Serie.Select(o => o.Valor).FirstOrDefault(),
                                                  Nombre = a.Inventario.Articulo.Nombre,
                                                  Plaza = a.Inventario.EntidadClave.Value.ToString()
                                              }
                                             ).ToList();
                lista.AddRange(aparatosdev);

            }




            foreach (var a in lista)
            {
                a.txtstatus = dc.catEstatus.Where(i => i.catEstatusClave == a.estatus).Select(y => y.Nombre).First();
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus = Int32.Parse(s.status);
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }

        public ActionResult ValidaSerie(string serie, Guid articulo, Guid almacen, int status)
        {

            try
            {
                var s = (from a in dc.Serie
                         where a.Valor == serie && a.Inventario.ArticuloClave == articulo && a.Inventario.EntidadClave == almacen
                         where a.Inventario.EstatusInv == status && a.Inventario.Standby.Value == 0 && a.Inventario.Cantidad == 1
                         select new { a.InventarioClave, a.catSerieClave, a.Valor, a.Inventario.ArticuloClave, a.Inventario.Articulo.Nombre }).First();
                return Json(s, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Content("0");
            }
        }
        public ActionResult GetTipoArticulo(int id)
        {

            var tipos = dc.catTipoArticulo.Where(x => x.catClasificacionArticuloClave == id && x.Activo == true).Select(o => new { o.catTipoArticuloClave, o.Descripcion }).ToList();
            return Json(tipos, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetArticulos(int id, Guid almacen)
        {

            //ArticuloController ac = new ArticuloController();
            //var articulos = ac.GetArticulosByServicio(almacen, id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList();



            var articulos = dc.Articulo.Where(o => o.Activo == true && o.catTipoArticuloClave == id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList();
            return Json(articulos, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DetalleArticulo(Guid idarticulo)
        {
            DetalleArticulo da = new DetalleArticulo();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == idarticulo).First();
            da.idArticulo = art.ArticuloClave;
            da.idTipoArticulo = art.catTipoArticuloClave;
            da.idClasificacion = art.catTipoArticulo.catClasificacionArticuloClave;
            return Json(da, JsonRequestBehavior.AllowGet);
        }

        public class DetalleDevo
        {
            public Guid almacen { get; set; }
            public string motivo { get; set; }
            public long orden { get; set; }
            public List<series> articulos { get; set; }

        }

        public ActionResult DetalleDevolucion(Guid iddevolucion)
        {

            Proceso devolucion;
            DetalleDevo objretornar = new DetalleDevo();
            try
            {
                devolucion = dc.Proceso.Where(x => x.ProcesoClave == iddevolucion).First();
                if (devolucion.catTipoProcesoClave == 9)
                {
                    objretornar.almacen = devolucion.EntidadClaveSolicitante.Value;
                }
                else if (devolucion.catTipoProcesoClave == 5)
                {
                    objretornar.almacen = devolucion.EntidadClaveSolicitante.Value;
                }
                else
                {
                    objretornar.almacen = devolucion.EntidadClaveAfectada.Value;
                }
                objretornar.orden = devolucion.NumeroPedido;
                objretornar.motivo = devolucion.Observaciones;

            }
            catch
            {

                return Content("");
            }

            List<ProcesoInventario> pi = devolucion.ProcesoInventario.ToList();
            List<series> articulos = new List<series>();

            foreach (var a in pi)
            {
                series t = new series();
                t.ArticuloClave = a.Inventario.ArticuloClave.Value;
                t.status = a.Inventario.EstatusInv.Value.ToString();
                t.InventarioClave = a.InventarioClave;
                t.Nombre = a.Inventario.Articulo.Nombre;

                if (a.Inventario.Articulo.catTipoArticulo.catSerieClaveInterface == null)
                {
                    t.Cantidad = a.Cantidad.Value;
                    t.Valor = null;
                    t.tieneserie = false;
                }
                else
                {
                    t.Cantidad = 1;
                    t.Valor = dc.Serie.Where(x => x.InventarioClave == a.Inventario.InventarioClave).Select(o => o.Valor).First();
                    t.tieneserie = true;
                }

                articulos.Add(t);
            }
            objretornar.articulos = articulos;

            return Json(objretornar, JsonRequestBehavior.AllowGet);


        }




        public ActionResult detalleEnvio(Guid proceso)
        {


            ProcesoEnvio p = dc.ProcesoEnvio.Where(x => x.ProcesoClave == proceso).First();

            envio e = new envio();

            e.fecha = p.FechaEnvio.ToString("dd/MM/yyyy");
            e.guia = p.GuiaEnvio.ToString();
            e.tran = p.catTranspClave;
            return Json(e, JsonRequestBehavior.AllowGet);
        }

        public class envio
        {
            public int tran { get; set; }
            public string guia { get; set; }
            public string fecha { get; set; }
        }


        public ActionResult Detalle()
        {



            if (Request["d"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["d"]);
            int pdf = Convert.ToInt32(Request["pdf"]);
            Proceso pro = (from p in dc.Proceso
                           where p.ProcesoClave == clave
                           select p).SingleOrDefault();
            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();
            Entidad eS = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveSolicitante
                          select e).SingleOrDefault();
            Entidad eA = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveAfectada
                          select e).SingleOrDefault();
            Usuario u = pro.Usuario;
            catEstatus vEst = (from e in dc.catEstatus
                               where e.catEstatusClave == pro.catEstatusClave
                               select e).SingleOrDefault();

            StringBuilder sb = new StringBuilder();
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h3>" + empresa + "</h3>");
            if (pro.catTipoProcesoClave == 5)
            {
                sb.Append("<h3>Devolución SAC al Almacén Central #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 6)
            {
                sb.Append("<h3>Recepción de material por devolución #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 7)
            {
                sb.Append("<h3>Habilitación de equipos #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 8)
            {
                sb.Append("<h3>Baja de Equipos #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 9)
            {
                sb.Append("<h3>Orden de Garantía #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 18)
            {
                sb.Append("<h3>Recepción de material por Garantía #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 13)
            {
                sb.Append("<h3>Devolución de material #" + pro.NumeroPedido + "</h3><br>");
            }



            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=100 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<font size=2>");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append("<td>");
            if (pro.catTipoProcesoClave == 5 || pro.catTipoProcesoClave == 13)
            {


                sb.Append("<b>Enviar a</b><br>");
                sb.Append(eA.Nombre + "<br>");
                sb.Append("<b>Enviado de</b><br>");
                sb.Append(eS.Nombre + "<br>");
            }
            else
            {
                sb.Append("<b>Fecha</b><br>");
                sb.Append(pro.Fecha + "<br>");
            }
            sb.Append("<b>Tipo de Proceso</b><br>");
            sb.Append(pro.catTipoProceso.Nombre + "<br>");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>Estatus Proceso </b><br>");
            sb.Append(vEst.Nombre + "<br>");

            if (pro.catTipoProcesoClave == 9)
            {
                sb.Append("<b>Proveedor</b><br>");
                sb.Append(pro.catProveedor.Nombre + "<br>");
            }

            if (pro.catTipoProcesoClave == 5 || pro.catTipoProcesoClave == 13)
            {
                List<ProcesoEnvio> pe = dc.ProcesoEnvio.Where(o => o.ProcesoClave == pro.ProcesoClave).ToList();
                if (pe.Count > 0)
                {
                    sb.Append("<b>Datos de envio: </b><br>");
                    sb.Append("<b>Transportista</b><br>");
                    sb.Append(pro.ProcesoEnvio.catTransportista.Descripcion);
                    sb.Append("<br>");
                    sb.Append("<b>Guia</b><br>");
                    sb.Append(pro.ProcesoEnvio.GuiaEnvio);
                }


            }
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");



            if (pro.catTipoProcesoClave == 5)
            {
                sb.Append("<b>Motivo </b><br>");
                sb.Append(pro.Observaciones + "<br>");
            }
            sb.Append("<br><br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Cantidad");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" colspan=""3"">");
            sb.Append("Concepto");
            sb.Append("</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                if (pa.Cantidad > 0)
                {
                    Articulo a = pa.Articulo;

                    sb.Append("<tr>");
                    sb.Append(@"<td align=""center"">");
                    sb.Append(pa.Cantidad);
                    sb.Append("</td>");
                    sb.Append(@"<td colspan=""3"">");
                    sb.Append(a.Nombre);
                    sb.Append("<br>");

                    foreach (ProcesoInventario pi in procesoinventario)
                    {
                        Inventario i = pi.Inventario;
                        if (i.ArticuloClave == a.ArticuloClave)
                        {
                            List<Serie> series = i.Serie.ToList();
                            int conta = 0;
                            var innerCount = 0;
                            foreach (Serie s in series)
                            {
                                if (innerCount > 0)
                                {
                                    sb.Append("-");
                                }
                                string val = "";
                                if (s.Inventario.EstatusInv == 7)
                                {
                                    val = "Buen Estado";
                                }
                                else if (s.Inventario.EstatusInv == 8)
                                {
                                    val = "Instalado";
                                }
                                else if (s.Inventario.EstatusInv == 9)
                                {
                                    val = "Revisión";
                                }
                                else if (s.Inventario.EstatusInv == 10)
                                {
                                    val = "Dañado";
                                }
                                else if (s.Inventario.EstatusInv == 21)
                                {
                                    val = "Usado";
                                }
                                else if (s.Inventario.EstatusInv == 20)
                                {
                                    val = "Robado";
                                }
                                else if (s.Inventario.EstatusInv == 14)
                                {
                                    val = "Baja por garantía";
                                }
                                else if (s.Inventario.EstatusInv == 13)
                                {
                                    val = "Reparación";
                                }
                                else
                                {
                                    val = "Baja";
                                }
                                sb.Append(s.Valor + "->" + val);
                                innerCount++;
                            }

                            conta++;


                            if (conta > 0)
                            {
                                sb.Append("<br>");
                            }
                        }
                    }
                }
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");

            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "_dev-almcentral.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Dev-AlmCentral_" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "_dev-almcentral.pdf" + "' style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }
        }





    }
}

