﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class ProveedorController : Controller
    {
        //
        // GET: /Proveedor/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

                int elementos = 15;


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catProveedorWrapper> data { get; set; }
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = listaProveedores(data).OrderByDescending(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = listaProveedores(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet); 
                      
        }



        public List<catProveedorWrapper> listaProveedores(string data)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            List<catProveedor> rls = (from use in dc.catProveedor
                                          orderby use.Nombre
                                          select use).ToList();

            List<catProveedorWrapper> rw = new List<catProveedorWrapper>();

            foreach (catProveedor u in rls)
            {
                catProveedorWrapper w = ProveedorToWrapper(u);
                rw.Add(w);
            }
            return rw;


        }

        public ActionResult Obten(string data)
        {
            catProveedorWrapper rw = new catProveedorWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid uid = Guid.Parse(data);

            catProveedor r = (from ro in dc.catProveedor
                                  where ro.catProveedorClave == uid
                                  select ro).SingleOrDefault();

            if (r != null)
            {
                rw = ProveedorToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    catProveedor r = (from ro in dc.catProveedor
                                          where ro.catProveedorClave == uid
                                 select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    Guid uid = Guid.Parse(activa_id);

                    catProveedor r = (from ro in dc.catProveedor
                                          where ro.catProveedorClave == uid
                                          select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_rfc, string ed_CalleNumero, string ed_Colonia, string ed_CodigoPostal, string ed_Ciudad, string ed_nombrecontacto, string ed_emailcontacto, string ed_lada1contacto, string ed_telefono1contacto, string ed_lada2contacto, string ed_telefono2contacto, string ed_activo)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();    // Error causado por el int , se va a cambiar a Guid

                catProveedor r = new catProveedor();

                if (ed_id != String.Empty)
                {
                    Guid uid = Guid.Parse(ed_id);

                    r = (from ro in dc.catProveedor
                         where ro.catProveedorClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {
                    r.catProveedorClave = Guid.NewGuid();
                }

                r.RFC = ed_rfc;
                r.Nombre = ed_nombre;

                r.NombreContacto = ed_nombrecontacto;
                r.Email = ed_emailcontacto;
                r.Lada1 = ed_lada1contacto;
                r.Telefono1 = ed_telefono1contacto;
                r.Lada2 = ed_lada2contacto;
                r.Telefono2 = ed_telefono2contacto;
                r.Activo = ed_activo == null ? false : ed_activo.Length > 0;

                if (ed_id == String.Empty)
                {
                    dc.catProveedor.Add(r);
                }
                dc.SaveChanges();

                if (r.DomicilioClave == null)
                {
                    Domicilio d = new Domicilio();
                    d.DomicilioClave = Guid.NewGuid();
                    d.CalleNumero = ed_CalleNumero;
                    d.Colonia = ed_Colonia;
                    d.CodigoPostal = ed_CodigoPostal;
                    d.Ciudad = ed_Ciudad;
                    d.Pais = "México";

                    dc.Domicilio.Add(d);
                    dc.SaveChanges();

                    r.DomicilioClave = d.DomicilioClave;
                    
                }
                else
                {
                    Domicilio d = (from dom in dc.Domicilio
                                       where dom.DomicilioClave == r.DomicilioClave
                                       select dom).SingleOrDefault();
                    d.CalleNumero = ed_CalleNumero;
                    d.Colonia = ed_Colonia;
                    d.CodigoPostal = ed_CodigoPostal;
                    d.Ciudad = ed_Ciudad;

                    r.DomicilioClave = d.DomicilioClave;
                }





                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        private catProveedorWrapper ProveedorToWrapper(catProveedor u)
        {
            catProveedorWrapper rw = new catProveedorWrapper();
            rw.Clave = u.catProveedorClave.ToString();
            rw.Nombre = u.Nombre;

            rw.RFC = u.RFC;

            rw.NombreContacto = u.NombreContacto;
            rw.EmailContacto = u.Email;
            rw.Lada1Contacto = u.Lada1;
            rw.Telefono1Contacto = u.Telefono1;
            rw.Lada2Contacto = u.Lada2;
            rw.Telefono2Contacto = u.Telefono2;

            if (u.Domicilio != null)
            {
                Domicilio dom = u.Domicilio;
                rw.CalleNumero = dom.CalleNumero;
                rw.Colonia = dom.Colonia;
                rw.CodigoPostal = dom.CodigoPostal;
                rw.Ciudad = dom.Ciudad;
            }

            rw.Activo = Convert.ToInt32(u.Activo).ToString();

            return rw;
        }

  
    }
}
