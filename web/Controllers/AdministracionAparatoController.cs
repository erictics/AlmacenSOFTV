﻿
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using Web.Controllers;

namespace web.Controllers
{
    public class AdministracionAparatoController : Controller
    {
        
        ModeloAlmacen dc = new ModeloAlmacen();

        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {           
            Usuario u = ((Usuario)Session["u"]);
            List<Entidad> entidades = null;
            entidades = (from a in dc.AmbitoAlmacen join b in dc.Entidad on a.EntidadClaveAlmacen equals b.EntidadClave where a.UsuarioClave == u.UsuarioClave select b).ToList();
             ViewData["almacenes"] = entidades;
            return View();
            
        }

        public ActionResult lista(int tipo, int orden, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ObtenProcesos(tipo, orden).OrderByDescending(x => x.numeropedido).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ObtenProcesos(tipo, orden).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Proceso_> ObtenProcesos(int tipo,int orden)
        {
            List<Proceso_> lista = (from a in dc.Proceso
                                    where
               (tipo == 0 || a.catTipoProcesoClave == tipo)&&
                (orden == 0 || a.NumeroPedido == orden)
                                    select new Proceso_
                                    {
                                        numeropedido = a.NumeroPedido,
                                        fecha = a.Fecha.Value,
                                        procesoclave = a.ProcesoClave,
                                        usuario = a.Usuario.Nombre,
                                        tipoproceso = a.catTipoProcesoClave.Value,
                                         nombreproceso=a.catTipoProceso.Nombre
                                                                           
                                    }
                                  ).ToList();
            lista.ForEach(p => { p.fechacorta = p.fecha.ToShortDateString(); });
            return lista;
        }


        public class Proceso_
        {
            public Guid procesoclave { get; set; }
            public DateTime fecha { get; set; }
            public string fechacorta { get; set; }
            public string usuario { get; set; }
            public string articulo { get; set; }
            public string tipoarticulo { get; set; }
            public int tipoproceso { get; set; }
            public string nombreproceso { get; set;}
            public long numeropedido { get; set; }

        }

        public class Aparato
        {
            public string serie { get; set; }
            public string idInventario { get; set; }
            public string Nombre { get; set; }
            public string Status { get; set; }
            public string Ubicacion { get; set; }

        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Proceso_> data { get; set; }
        }


        public ActionResult DetalleAparato(string serie)
        {
         
           
              
            Usuario u = (Usuario)Session["u"];
            Guid idinventario = Guid.Empty;
            try
            {
                idinventario = dc.Serie.Where(o => o.Valor == serie).Select(s => s.InventarioClave).First();
            }
            catch
            { }
            if (idinventario == Guid.Empty)
            {
                return Content("-1");
            }
            else
            {
                Aparato aparato = new Aparato();
                Inventario inventario = dc.Inventario.Where(o => o.InventarioClave == idinventario).First();
                aparato.idInventario = inventario.IdInventario.ToString();
                aparato.Nombre = inventario.Articulo.Nombre;
                aparato.serie = dc.Serie.Where(p => p.InventarioClave == inventario.InventarioClave).Select(o => o.Valor).First();
                aparato.Status = dc.catEstatus.Where(o => o.catEstatusClave == inventario.EstatusInv.Value).Select(o => o.Nombre).First();
                if (inventario.Entidad != null)
                {
                    aparato.Ubicacion = inventario.Entidad.Nombre;
                }
                else
                {
                    try
                    {
                        aparato.Ubicacion = "";
                        if (inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.Contrato != String.Empty)
                        {
                            aparato.Ubicacion += "Contrato:" + inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.Contrato + ", ";
                        }

                        if (inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.NumeroOrden != null)
                        {
                            aparato.Ubicacion += " Orden: " + inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.NumeroOrden.Value.ToString();
                        }
                    }
                    catch
                    {
                    }

                }

                return Json(aparato, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenArticulos(Guid idalmacen)
        {
           
            List<Articulo> articulos = dc.Articulo.Where(o => o.Activo == true).ToList();
            List<seriealm> art = new List<seriealm>();
            articulos.ForEach(x => {

                List<Inventario> inv = dc.Inventario.Where(o => o.EntidadClave == idalmacen && o.ArticuloClave == x.ArticuloClave && o.Articulo.catTipoArticulo.catSerieClaveInterface != null).ToList();
                if (inv.Count > 0)
                {
                    seriealm sm = new seriealm();
                    sm.Aparato = x.Nombre;
                    sm.Mac = x.ArticuloClave.ToString();
                    art.Add(sm);
                }
            });
            return Json(art.OrderBy(s => s.Aparato), JsonRequestBehavior.AllowGet);
        }



        public class seriealm
        {
            public string almacen { get; set; }
            public string Aparato { get; set; }
            public string Letra { get; set; }
            public string Serie { get; set; }
            public string Mac { get; set; }
            public int clvstatus { get; set; }
            public string Status { get; set; }
        }

        public ActionResult Exportar()
        {
            Guid Articulo = Guid.Empty;
            Guid Almacen = Guid.Empty;
            int status = 7;
            try
            {
                Articulo = Guid.Parse(Request["articulo"].ToString());
                Almacen = Guid.Parse(Request["almacen"].ToString());
                status = Int32.Parse(Request["status"].ToString());
                ExportarExcel(Articulo, Almacen, status);
                return null;
            }
            catch
            {
                return null;
            }
        }

        public void ExportarExcel(Guid articulo, Guid almacen, int status)
        {           
            List<seriealm> lista = dc.Inventario.Where(o => o.EntidadClave == almacen && o.EstatusInv == status
                && o.Standby == 0 &&
                o.Cantidad == 1 && o.ArticuloClave == articulo && o.Articulo.catTipoArticulo.catSerieClaveInterface != null).Select(p => new seriealm { almacen = p.Entidad.Nombre, Aparato = p.Articulo.Nombre, Letra = p.Articulo.catTipoArticulo.Letra, Mac = p.IdInventario.ToString(), Serie = "", clvstatus = p.EstatusInv.Value }).ToList();
            lista.ForEach(s => {
                long id = long.Parse(s.Mac);
                s.Serie = dc.Serie.Where(a => a.Inventario.IdInventario == id).Select(i => i.Valor).First();
                s.Status = dc.catEstatus.Where(p => p.catEstatusClave == s.clvstatus).Select(y => y.Nombre).First();
            });
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Series");
                ws.Cells["A1"].LoadFromCollection(lista, true);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Inventario.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());

            }

        }


        public class Historial
        {
            public string TipoProceso { get; set; }
            public int ProcesoClave { get; set; }
            public string Orden { get; set; }
            public DateTime Fecha { get; set; }
            public string Fecha2 { get; set; }
            public string Hora { get; set; }
            public string Usuario { get; set; }
            public Guid? EntidadSolicitante { get; set; }
            public Guid? EntidadAfectada { get; set; }
            public string Observaciones { get; set; }

        }

        public ActionResult HistorialAparato(int idInventario)
        {
           
            try
            {
                Inventario inv = dc.Inventario.Where(o => o.IdInventario == idInventario).First();
                List<Historial> lista = new List<Historial>();
                List<Historial> historial = new List<Historial>();
                List<Historial> contratos = new List<Historial>();
                historial = (from a in dc.Proceso join b in dc.ProcesoInventario on a.ProcesoClave equals b.ProcesoClave where b.InventarioClave == inv.InventarioClave select new Historial { TipoProceso = a.catTipoProceso.Nombre, Orden = a.NumeroPedido.ToString(), Fecha = a.Fecha.Value, Usuario = a.Usuario1.Nombre, EntidadAfectada = a.EntidadClaveAfectada.HasValue ? a.EntidadClaveAfectada.Value : Guid.Empty, EntidadSolicitante = a.EntidadClaveSolicitante.HasValue ? a.EntidadClaveSolicitante : null, ProcesoClave = a.catTipoProcesoClave.Value }).ToList();

                contratos = (from c in dc.BitacoraSofTV
                             join d in dc.BitacoraSofTVDetalle on c.BitacoraSofTVClave equals d.BitacoraSofTVClave
                             where d.InventarioClave == inv.InventarioClave
                             select new Historial { Orden = c.Contrato + "|" + c.NumeroOrden, Fecha = c.Fecha.Value, Usuario = c.NombreUsuario, TipoProceso = "Bitácora SAC" }).ToList();
                historial.AddRange(contratos);
                lista = historial.OrderBy(o => o.Fecha).ToList();

                lista.ForEach(x =>
                {
                    Entidad e;
                    Entidad e2;

                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.Hora = x.Fecha.ToShortTimeString();
                    x.Observaciones = "Sin Observaciones";
                    if (x.ProcesoClave == 1)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            x.Observaciones = "De " + e.Nombre;
                        }
                        catch { }

                    }
                    if (x.ProcesoClave == 3)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "De " + e2.Nombre + " A " + e.Nombre;
                        }
                        catch { }

                    }
                    if (x.ProcesoClave == 4)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();

                            x.Observaciones = "En " + e.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 5)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "De " + e.Nombre + " A " + e2.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 5)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "De " + e.Nombre + " A " + e2.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 6)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            if (e.catDistribuidorClave == null)
                            {
                                x.Observaciones = "En: " + e.Nombre;
                            }
                            else
                            {
                                x.Observaciones = "En:" + e2.Nombre;
                            }



                        }
                        catch { }
                    }
                    if (x.ProcesoClave == 7)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            x.Observaciones = "En " + e.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 8)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            x.Observaciones = "En " + e.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 9)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            x.Observaciones = "En " + e.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 9)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            x.Observaciones = "En " + e.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 11)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "En " + e.Nombre;
                        }
                        catch { }
                    }
                    if (x.ProcesoClave == 12)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "De " + e.Nombre + " A " + e2.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 13)
                    {
                        try
                        {
                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "De " + e2.Nombre + " A " + e.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 14)
                    {
                        try
                        {

                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "En " + e2.Nombre;
                        }
                        catch { }


                    }
                    if (x.ProcesoClave == 16)
                    {
                        try
                        {

                            e = dc.Entidad.Where(y => y.EntidadClave == x.EntidadSolicitante).First();
                            e2 = dc.Entidad.Where(y => y.EntidadClave == x.EntidadAfectada).First();
                            x.Observaciones = "De " + e2.Nombre + " A " + e.Nombre;
                        }
                        catch { }


                    }





                });


                return Json(lista.OrderByDescending(g=>g.Fecha), JsonRequestBehavior.AllowGet);



            }
            catch
            {
                return Content("0");
            }


        }



        public ActionResult CambioSerie(string serieError, string serieCorrecta)
        {

            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                Serie serieE;
                try
                {
                    try
                    {
                        serieE = dc.Serie.Where(s => s.Valor == serieError).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-1");

                    }
                    

                

                if (serieE.Inventario.EstatusInv == 8)
                {
                    dbContextTransaction.Rollback();
                    return Content("-6");
                }


                Usuario u = ((Usuario)Session["u"]);
                Proceso proceso = new Proceso();
                proceso.ProcesoClave = Guid.NewGuid();
                proceso.catTipoProcesoClave = 19;
                proceso.catEstatusClave = 4;
                proceso.Fecha = DateTime.Now;
                proceso.UsuarioClave = u.UsuarioClave;
                proceso.Observaciones = "cambio de serie de " + serieError + "    a    " + serieCorrecta;
                dc.Proceso.Add(proceso);
                dc.SaveChanges();

                ProcesoArticulo pa = new ProcesoArticulo();
                pa.ProcesoClave = proceso.ProcesoClave;
                pa.ArticuloClave = serieE.Inventario.ArticuloClave.Value;
                pa.Cantidad = 1;
                dc.ProcesoArticulo.Add(pa);

                ProcesoInventario pi = new ProcesoInventario();
                pi.ProcesoClave = proceso.ProcesoClave;
                pi.InventarioClave = serieE.InventarioClave;
                pi.Fecha = DateTime.Now;
                pi.Cantidad = 1;
                dc.ProcesoInventario.Add(pi);


                int max = serieE.catSerie.Max;
                int min = serieE.catSerie.Min;

                if (serieError == serieCorrecta || serieError.ToUpper() == serieCorrecta.ToUpper())
                {
                    dbContextTransaction.Rollback();
                    return Content("-3");
                }

                List<Serie> sl = dc.Serie.Where(p => p.Valor == serieCorrecta).ToList();
                if (sl.Count > 0)
                {
                    dbContextTransaction.Rollback();
                    return Content("-4");
                }
                //if (serieCorrecta.Length >= min && serieCorrecta.Length <= max)
                //{
                    serieE.Valor = serieCorrecta;
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(proceso.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    dbContextTransaction.Rollback();
                //    return Content("-2");
                //}

                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("-1");
                }
            }
        }

        public ActionResult CambioEstatus(string serie, int status)
        {
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Usuario u = ((Usuario)Session["u"]);
                    Serie s;
                    try
                    {
                        s = dc.Serie.Where(y => y.Valor == serie).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-2");
                    }

                    
                    string statusant = dc.catEstatus.Where(i => i.catEstatusClave == s.Inventario.EstatusInv).Select(i => i.Nombre).First();
                    string statusnue = dc.catEstatus.Where(i => i.catEstatusClave == status).Select(i => i.Nombre).First();
                    Proceso proceso = new Proceso();
                    proceso.ProcesoClave = Guid.NewGuid();
                    proceso.catTipoProcesoClave = 20;
                    proceso.catEstatusClave = 4;
                    proceso.EntidadClaveAfectada = s.Inventario.EntidadClave;
                    proceso.EntidadClaveSolicitante = s.Inventario.EntidadClave;
                    proceso.Fecha = DateTime.Now;
                    proceso.UsuarioClave = u.UsuarioClave;
                    proceso.Observaciones = "cambio de status de " + statusant + "  a " + statusnue;
                    dc.Proceso.Add(proceso);
                    dc.SaveChanges();

                    ProcesoArticulo pa = new ProcesoArticulo();
                    pa.ProcesoClave = proceso.ProcesoClave;
                    pa.ArticuloClave = s.Inventario.ArticuloClave.Value;
                    pa.Cantidad = 1;
                    dc.ProcesoArticulo.Add(pa);
                    dc.SaveChanges();

                    ProcesoInventario pi = new ProcesoInventario();
                    pi.ProcesoClave = proceso.ProcesoClave;
                    pi.InventarioClave = s.InventarioClave;
                    pi.Fecha = DateTime.Now;
                    pi.Cantidad = 1;
                    dc.ProcesoInventario.Add(pi);
                    dc.SaveChanges();

                    

                        if (s.Inventario.Cantidad == 0 && s.Inventario.Standby == 1)
                        {
                            dbContextTransaction.Rollback();
                            return Content("-6");
                        }
                        else if (s.Inventario.EstatusInv == 11)
                        {
                            dbContextTransaction.Rollback();
                            return Content("-7");
                        }
                        else if (s.Inventario.EstatusInv == 8)
                        {
                            dbContextTransaction.Rollback();
                            return Content("-8");

                        }
                        else
                        {
                            s.Inventario.EstatusInv = status;
                            //s.Inventario.EntidadClave = almacen_cuarentena;
                            dc.SaveChanges();
                            dbContextTransaction.Commit();
                            return Json(proceso.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                        }

                    

                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");
                }
            }

        }


        public ActionResult Detalle()
        {



            if (Request["d"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["d"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso pro = (from p in dc.Proceso
                           where p.ProcesoClave == clave
                           select p).SingleOrDefault();

            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();

            Entidad eS = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveSolicitante
                          select e).SingleOrDefault();
            Entidad eA = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveAfectada
                          select e).SingleOrDefault();
            Usuario u = pro.Usuario;

            catEstatus vEst = (from e in dc.catEstatus
                               where e.catEstatusClave == pro.catEstatusClave
                               select e).SingleOrDefault();

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h3>" + empresa + "</h3>");
            sb.Append("<h3>"+pro.catTipoProceso.Nombre+" #" + pro.NumeroPedido + "</h3><br>");
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<font size=2>");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("<b>Usuario que ejecutó</b><br>");
            sb.Append(pro.Usuario1.Nombre + "<br>");
            sb.Append("<b>Fecha de ejecución</b><br>");
            sb.Append(pro.Fecha + "<br>");
            sb.Append("<b>Observaciones</b><br>");
            sb.Append(pro.Observaciones + "<br>");         
            sb.Append("<b>Tipo de Proceso</b><br>");
            sb.Append(pro.catTipoProceso.Nombre + "<br>");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>Estatus Proceso </b><br>");
            sb.Append(vEst.Nombre + "<br>");          
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<br>");
            sb.Append("<br>");
            sb.Append("<br>");
            List <ProcesoInventario> pa = pro.ProcesoInventario.ToList();

            pa.ForEach(x => {
                sb.Append("<b>Clasificación artículo: </b>" + x.Inventario.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion);
                sb.Append("<br>");
                sb.Append("<b>Tipo artículo:  </b>" + x.Inventario.Articulo.catTipoArticulo.Descripcion);
                sb.Append("<br>");
                sb.Append("<b>Artículo: </b>" + x.Inventario.Articulo.Nombre);
                sb.Append("<br>");

                try
                {
                    Serie s = dc.Serie.Where(j => j.InventarioClave == x.InventarioClave).First();
                    catEstatus ss = dc.catEstatus.Where(m=>m.catEstatusClave==x.Inventario.EstatusInv).First();
                    sb.Append("<b>serie: </b>" + s.Valor);
                    sb.Append("<br>");
                    sb.Append("<b>Estatus actual: </b>" + ss.Nombre);
                }
                catch
                {
                    
                }                
                
            });
            
            

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");

            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "_dev-almcentral.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Dev-AlmCentral_" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "_dev-almcentral.pdf" + "' style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }
        }









    }
}
