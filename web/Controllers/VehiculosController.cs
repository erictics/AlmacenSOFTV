﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Controllers;

namespace web.Controllers
{
    public class VehiculosController : Controller
    {
        //
        // GET: /Vehiculos/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor>dis= dc.catDistribuidor.Where(x => x.Activo == true).ToList();
            ViewData["distribuidores"] = dis;
            return View();
        }



        public class Vehiculo
        {
            public int  idVehiculo { get; set; }
            public string Conductor {get; set;}
            public string serie {get; set;}
            public string placas {get; set;}
            public string poliza {get; set;}
            public string  DistribuidorClave { get; set; }
            public string Distribuidor { get; set; }
            public string plazaEncierro { get; set; }
            public bool Asegurado { get; set; }
            public bool Activo { get; set; }
            public string color { get; set; }
            public string modelo { get; set; }
            public string marca { get; set; }
            public string Especificaciones { get; set; }
        }



        public ActionResult GuardaVehiculo(Vehiculo obj)
        {
            ModeloAlmacen dc = new ModeloAlmacen();              
                try{

                    vehiculo v = new vehiculo();
                    v.Activo = obj.Activo;
                    v.Asegurado = obj.Asegurado;
                    v.conductorTitular = obj.Conductor;
                    v.DistribuidorClave = obj.DistribuidorClave;
                    v.IdVehiculo = obj.idVehiculo;
                    v.Placas = obj.placas;
                    v.PlazaEncierro = obj.plazaEncierro;
                    v.Poliza = obj.poliza;
                    v.Serie = obj.serie;
                    dc.vehiculo.Add(v);
                    dc.SaveChanges();

                    DetalleVehiculo dv = new DetalleVehiculo();
                    dv.color = obj.color;
                    dv.Especificaciones = obj.Especificaciones;
                    dv.IdVehiculo = v.IdVehiculo;
                    dv.Marca = obj.marca;
                    dv.Modelo = obj.modelo;
                    dc.DetalleVehiculo.Add(dv);
                    dc.SaveChanges();

                    
                    return Content("1");
                }
                catch
                {
                    return Content("0");
                }
        }


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Vehiculo> data { get; set; }
        }

        public ActionResult ObtenLista(string distribuidor, string placas, string seguro, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = lista(ref recordsFiltered, start, length, distribuidor, placas, seguro);
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);

        }

        public List<Vehiculo> lista(ref int recordsFiltered, int start, int length, string distribuidor, string placas, string seguro)
        {
            List<Vehiculo> listav = new List<Vehiculo>();
            ModeloAlmacen dc = new ModeloAlmacen();
            listav =(from a in dc.vehiculo
                     join b in dc.DetalleVehiculo on a.IdVehiculo equals b.IdVehiculo                    
                     select new Vehiculo { Activo=a.Activo.Value, Asegurado=a.Asegurado.Value, color=b.color, Conductor=a.conductorTitular, Especificaciones=b.Especificaciones, idVehiculo=a.IdVehiculo,
                      marca=b.Marca,modelo=b.Modelo, placas=a.Placas,poliza=a.Poliza, serie=a.Serie, DistribuidorClave=a.DistribuidorClave}).ToList();
            listav.ForEach(y => {
                try{
                    Guid id = Guid.Parse(y.DistribuidorClave);
                    y.Distribuidor = dc.catDistribuidor.Where(p=>p.catDistribuidorClave==id).Select(u=>u.Nombre).First();
                }catch{

                }
            
            });

            recordsFiltered = listav.Count;
            return listav.Skip(start).Take(length).ToList();
        }


        public ActionResult DetalleVehiculo(int id)
        {
             ModeloAlmacen dc = new ModeloAlmacen();   
            vehiculo vehiculo=dc.vehiculo.Where(p=>p.IdVehiculo==id).First();
            DetalleVehiculo detalle=dc.DetalleVehiculo.Where(p=>p.IdVehiculo==id).First();
               
            Vehiculo v = new Vehiculo();
            v.Activo=vehiculo.Activo.Value;
            v.Asegurado=vehiculo.Asegurado.Value;
            v.color=detalle.color;
            v.Conductor = vehiculo.conductorTitular;
            Guid idDist = Guid.Parse(vehiculo.DistribuidorClave);
            v.Distribuidor = dc.catDistribuidor.Where(s => s.catDistribuidorClave == idDist).Select(y => y.Nombre).First();
            v.DistribuidorClave = vehiculo.DistribuidorClave;
            v.Especificaciones = detalle.Especificaciones;
            v.idVehiculo = vehiculo.IdVehiculo;
            v.marca = detalle.Marca;
            v.modelo = detalle.Modelo;
            v.placas = vehiculo.Placas;
            Guid plaza=Guid.Parse(vehiculo.PlazaEncierro);
            v.plazaEncierro = dc.Entidad.Where(p => p.EntidadClave == plaza).Select(f=>f.Nombre).First();
            v.poliza = vehiculo.Poliza;
            v.serie = vehiculo.Serie;            
            
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditaVehiculo(Vehiculo obj)
        {
            ModeloAlmacen dc = new ModeloAlmacen();              
                try{

                    vehiculo v = dc.vehiculo.Where(t=>t.IdVehiculo==obj.idVehiculo).First();
                    v.Activo = obj.Activo;
                    v.Asegurado = obj.Asegurado;
                    v.conductorTitular = obj.Conductor;
                    v.DistribuidorClave = obj.DistribuidorClave;
                    v.IdVehiculo = obj.idVehiculo;
                    v.Placas = obj.placas;
                    v.PlazaEncierro = obj.plazaEncierro;
                    v.Poliza = obj.poliza;
                    v.Serie = obj.serie;
                    
                    dc.SaveChanges();
                    DetalleVehiculo dv = new DetalleVehiculo();
                    dv.color = obj.color;
                    dv.Especificaciones = obj.Especificaciones;
                    dv.IdVehiculo = v.IdVehiculo;
                    dv.Marca = obj.marca;
                    dv.Modelo = obj.modelo;
                    
                    dc.SaveChanges();                    
                    return Content("1");
                }
                catch
                {
                    return Content("0");
                }
           
        }

        public class plaza
        {
            public Guid id { get; set; }
            public string nombre { get; set; }
        }

        public ActionResult ObtenerPlaza(Guid id)
        {
           
            ModeloAlmacen dc = new ModeloAlmacen();
            List<plaza> lista  = dc.Entidad.Where(x => x.catDistribuidorClave == id && x.Activo == true && x.catEntidadTipoClave == 1).Select(x => new plaza { id=x.EntidadClave,nombre=x.Nombre}).ToList();

            return Json(lista,JsonRequestBehavior.AllowGet);

        }



    }
}
