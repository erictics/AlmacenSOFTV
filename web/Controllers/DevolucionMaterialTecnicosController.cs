﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.BL;
using web.Models;
using web.Utils;
using web.Wrappers;
using Web.Controllers;

namespace web.Controllers
{
    public class DevolucionMaterialTecnicosController : Controller
    {
        //
        // GET: /DevolucionMaterialTecnicos/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();
            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();
            List<Entidad> almacenes = null;
            Usuario u = ((Usuario)Session["u"]);
            if (u.DistribuidorClave == null)
            {
                almacenes = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                     && ta.catDistribuidorClave == null
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {
                List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                                  where a.UsuarioClave == u.UsuarioClave
                                                  select a).ToList();
                almacenes = new List<Entidad>();
                foreach (AmbitoAlmacen a in ambito)
                {
                    Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        almacenes.Add(e);
                    }
                }
            }
            List<Entidad> tecnicos = null;
            List<Entidad> tecnicosgen = null;
            if (u.DistribuidorClave == null)
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == null
                            orderby ta.Nombre
                            select ta).ToList();
            }
            else
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == u.DistribuidorClave
                            orderby ta.Nombre
                            select ta).ToList();

                tecnicosgen = (from a in dc.Entidad
                              join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                              where b.DistribuidorClave == u.DistribuidorClave
                              select a).ToList();

                tecnicos.AddRange(tecnicosgen);

            }
            List<catTipoSalida> tipossalida = (from ta in dc.catTipoSalida
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["almacenes"] = almacenes;
            ViewData["tecnicos"] = tecnicos;
            ViewData["tipossalida"] = tipossalida;
            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");
            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;
            int cuantos = (from use in dc.Proceso
                           where
                           use.catDistribuidorClave != null
                           && use.catTipoProcesoClave == 1
                           orderby use.Fecha descending
                           select use).Count();
            ViewData["cuantos"] = cuantos;
            List<Proceso> salidaspendientes = (from use in dc.Proceso
                                                   where use.catDistribuidorClave != null && use.catTipoProcesoClave == 1
                                                   orderby use.Fecha descending
                                                   select use).ToList();
            ViewData["salidaspendientes"] = salidaspendientes;
            ViewData["RolUsuario"] = u.RolClave;
            return View();            
        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<SalidaMaterialWrapper> data { get; set; }
        }

        public class DataTableData2
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Aparatos> data { get; set; }
        }

        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString()),
                vnEstatus = 0;
            Guid pedido = Guid.Empty,
                vgAlmacen = Guid.Empty;
            try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }

            try
            {
                vgAlmacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch { }

            try
            {
                vnEstatus = int.Parse(jObject["estatus"].ToString());
            }
            catch { }

            Guid tecnico = Guid.Empty;

            try
            {
                tecnico = Guid.Parse(jObject["tecnico"].ToString());
            }
            catch { }


            int tipoentidad = 2;

            try
            {
                tipoentidad = Convert.ToInt32(jObject["tipoentidad"].ToString());
            }
            catch
            { }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData(ref recordsFiltered, start, length, pedido, vgAlmacen, vnEstatus, tecnico, tipoentidad);
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        private List<SalidaMaterialWrapper> FilterData(ref int recordFiltered, int start, int length, Guid pedido, Guid vgAlmacen, int vnEstatus, Guid tecnico, int tipoentidad)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = (Usuario)Session["u"];
            List<Proceso> rls = null;
            List<Proceso> rls2 = null;
            if (u.DistribuidorClave == null)
            {
                rls = (from use in dc.Proceso
                       where use.catTipoProcesoClave == 12
                       && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                       && (tecnico == Guid.Empty || tecnico == use.EntidadClaveAfectada)
                       && (vgAlmacen == Guid.Empty || vgAlmacen == use.EntidadClaveSolicitante)
                       && use.Entidad.catEntidadTipoClave == tipoentidad
                       && use.catDistribuidorClave == null
                       orderby use.NumeroPedido descending
                       select use).ToList();              
            }
            else
            {
                rls = (from use in dc.Proceso
                       where use.catTipoProcesoClave == 12
                       && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                       && (tecnico == Guid.Empty || tecnico == use.EntidadClaveAfectada)
                       && (vgAlmacen == Guid.Empty || vgAlmacen == use.EntidadClaveSolicitante)
                       && use.catDistribuidorClave == u.DistribuidorClave
                       && (vnEstatus == 0 || use.catEstatusClave == vnEstatus)

                       orderby use.NumeroPedido descending
                       select use).ToList();


                recordFiltered = rls.Count();

            }
            List<SalidaMaterialWrapper> rw = new List<SalidaMaterialWrapper>();

            foreach (Proceso p in rls.Skip(start).Take(length).ToList())
            {
                SalidaMaterialWrapper w = SalidaMaterialToWrapper(p, false);
                rw.Add(w);
            }

            return rw;

        }

        private SalidaMaterialWrapper SalidaMaterialToWrapper(Proceso u, bool arts)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            PedidoDetalle pd = u.PedidoDetalle;

            SalidaMaterialWrapper rw = new SalidaMaterialWrapper();
            rw.NumeroPedido = u.NumeroPedido.ToString();
            rw.ProcesoClave = u.ProcesoClave.ToString();
            rw.Clave = u.ProcesoClave.ToString();
            rw.EntidadClaveSolicitante = u.EntidadClaveSolicitante.Value.ToString();
            rw.EntidadClaveAfectada = u.EntidadClaveAfectada.Value.ToString();
            try
            {
                rw.BitacoraLetra = u.BitacoraLetra.ToString();
                rw.BitacoraNumero = u.BitacoraNumero.ToString();
                rw.Plaza = u.EntidadPlaza.ToString();
            }
            catch
            {
            }            
            if (u.catTipoSalidaClave != null)
            {
                rw.TipoSalida = u.catTipoSalidaClave.ToString();
            }
            if (u.catProveedor != null)
            {
                catProveedor pro = u.catProveedor;
                rw.Origen = pro.Nombre;
            }
            else
            {
                rw.Origen = "";
            }

            if (u.ProcesoClaveRespuesta != null)
            {
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            else
            {
                rw.Pedido = String.Empty;
            }

            Entidad tec = (from ea in dc.Entidad
                               where ea.EntidadClave == u.EntidadClaveAfectada
                               select ea).SingleOrDefault();
            if (tec != null)
            {
                rw.Destino = tec.Nombre;
            }
            catEstatus estado = u.catEstatus;
            rw.Estatus = estado.Nombre;
            rw.EsCancelable = "1";
            foreach (ProcesoInventario pro in u.ProcesoInventario)
            {
                Inventario inv = pro.Inventario;
                if (inv.Serie.Count > 0 && (inv.EntidadClave != u.EntidadClaveSolicitante || inv.Cantidad > 0))
                {
                    rw.EsCancelable = "0";
                }
            }
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");          
            try
            {
                rw.Total_USD = (from p in dc.ProcesoEnvio
                                where p.ProcesoClave == u.ProcesoClave
                                select p).Count().ToString();
            }
            catch
            {
                rw.Total_USD = "0";
            }

            try
            {
                rw.Total = pd.Total.Value.ToString("C");
            }
            catch
            {
                rw.Total = String.Empty;
            }

            // Usamos esta variable para validar si ya existen los datos de envio capturados
            rw.DiasCredito = ((from pe in dc.ProcesoEnvio
                               where pe.ProcesoClave == u.ProcesoClave
                               select pe).Count() > 0) ? "1" : "0";

            // Si la variable arts es verdadera, entonces regresamos además la lista de productos de la salida
            // Principalmente para Ediciones
            if (arts)
            {
                List<ProcesoArticulo> articulos = u.ProcesoArticulo.ToList();

                List<ArticuloSalidaMaterialUI> apws = new List<ArticuloSalidaMaterialUI>();

                foreach (ProcesoArticulo pa in articulos)
                {
                    Articulo art = pa.Articulo;

                    ArticuloSalidaMaterialUI arui = new ArticuloSalidaMaterialUI();

                    arui.Clasificacion = art.catTipoArticulo.catClasificacionArticuloClave.ToString();
                    arui.TipoMaterial = art.catTipoArticulo.catTipoArticuloClave.ToString();

                    arui.ArticuloClave = art.ArticuloClave.ToString();
                    arui.ArticuloTexto = art.Nombre;
                    arui.Cantidad = pa.Cantidad.ToString();

                    if (pa.CantidadEntregada != null)
                    {
                        arui.Cantidad = pa.CantidadEntregada.ToString();
                    }
                    else
                    {
                        pa.Cantidad = 0;
                    }

                    int cuantos = (from c in dc.Inventario
                                   where c.ArticuloClave == art.ArticuloClave
                                   && c.EntidadClave == u.EntidadClaveAfectada
                                   select c.Cantidad.Value).ToList().Sum();

                    arui.Existencias = cuantos.ToString();


                    List<ProcesoInventario> proinv = u.ProcesoInventario.ToList().Where(o => o.Inventario.ArticuloClave == pa.ArticuloClave).ToList();

                    List<string> inventario = new List<string>();

                    arui.EsCancelable = "1";
                    foreach (ProcesoInventario pi in proinv)
                    {
                        inventario.Add(pi.InventarioClave.ToString());

                        Inventario inv = pi.Inventario;

                        if (inv.Serie.Count > 0 && (inv.EntidadClave != u.EntidadClaveSolicitante || inv.Cantidad > 0))
                        {
                            arui.EsCancelable = "0";
                        }

                    }

                    arui.Inventario = inventario;

                    apws.Add(arui);
                }

                rw.Articulos = apws;

            }

            return rw;
        }

        public ActionResult aparatos(Guid articulo, Guid tecnico, string Articulos, int draw, int start, int length)
        {

            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series> series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));
            DataTableData2 dataTableData = new DataTableData2();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(tecnico, articulo, series).Count;
            dataTableData.data = ObtenerAparatos(tecnico, articulo, series).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Aparatos> ObtenerAparatos(Guid tecnico, Guid articulo, List<series> listaseries)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Aparatos> lista = (from a in dc.Serie where a.Inventario.EntidadClave == tecnico && a.Inventario.ArticuloClave == articulo && a.Inventario.EstatusInv == 7 && a.Inventario.Cantidad.Value == 1 && a.Inventario.Standby.Value == 0 select new Aparatos { InventarioClave = a.InventarioClave, valor = a.Valor, Plaza = a.Inventario.EntidadClave.ToString(), ArticuloClave = a.Inventario.ArticuloClave.Value, Nombre = a.Inventario.Articulo.Nombre, estatus = a.Inventario.EstatusInv.Value }).OrderBy(x => x.valor).ThenBy(x => x.valor).ToList();

            foreach (var a in lista)
            {
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus = 7;
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }


        public ActionResult getInventario(Guid id, Guid tecnico)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            int cantidad = 0;
            Inv i = new Inv();
            try
            {
                i.Cantidad = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == tecnico && o.EstatusInv == 7).Sum(o => o.Cantidad.Value);
                i.InventarioClave = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == tecnico && o.EstatusInv == 7).Select(x => x.InventarioClave).First();
                i.tieneseries = tieneSeries(id);
            }
            catch
            {
                cantidad = 0;
            }
            return Json(i, JsonRequestBehavior.AllowGet);

        }


        public bool tieneSeries(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public ActionResult ValidaSerie(string serie, Guid articulo, Guid tecnico)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            try
            {
                var s = (from a in dc.Serie where a.Valor == serie && a.Inventario.ArticuloClave == articulo && a.Inventario.EntidadClave == tecnico && a.Inventario.EstatusInv==7 && a.Inventario.Standby==0 && a.Inventario.Cantidad==1 select new { a.InventarioClave, a.catSerieClave, a.Valor, a.Inventario.ArticuloClave, a.Inventario.Articulo.Nombre }).First();
                return Json(s, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guardar(Guid idAlmacen, Guid idTecnico, string articulos,string obs)
        {
             ModeloAlmacen dc = new ModeloAlmacen();
             using (var dbContextTransaction = dc.Database.BeginTransaction())
             {
                 try
                 {
                     Entidad Almacen;
                     Entidad Tecnico;
                     try
                     {
                         Almacen = dc.Entidad.Where(x => x.EntidadClave == idAlmacen).First();
                     }
                     catch
                     {
                         dbContextTransaction.Rollback();
                         error e1 = new error();
                         e1.idError = -1;
                         e1.Mensaje = " No se encontró el almacén en el sistema";
                         return Json(e1, JsonRequestBehavior.AllowGet);

                     }

                     try
                     {
                         Tecnico = dc.Entidad.Where(x => x.EntidadClave == idTecnico).First();
                     }
                     catch
                     {
                         dbContextTransaction.Rollback();
                         error e1 = new error();
                         e1.idError = -2;
                         e1.Mensaje = " No se encontró el técnico en el sistema";
                         return Json(e1, JsonRequestBehavior.AllowGet);

                     }

                     List<series> series;
                     try
                     {
                         var myArray = new Newtonsoft.Json.Linq.JArray();
                         series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                     }
                     catch
                     {
                         dbContextTransaction.Rollback();
                         error e1 = new error();
                         e1.idError = -3;
                         e1.Mensaje = " Ocurrió un error de lectura en la devolución";
                         return Json(e1, JsonRequestBehavior.AllowGet);

                     }

                     Usuario u = (Usuario)Session["u"];
                     Proceso p = new Proceso();
                     p.ProcesoClave = Guid.NewGuid();
                     p.catEstatusClave = 4;
                     p.catTipoProcesoClave = 12;
                     p.EntidadClaveSolicitante = Almacen.EntidadClave;
                     p.EntidadClaveAfectada = Tecnico.EntidadClave;
                     p.UsuarioClave = u.UsuarioClave;
                     p.Fecha = DateTime.Now;
                     p.Observaciones = obs;
                     p.catDistribuidorClave = Almacen.catDistribuidorClave;
                     dc.Proceso.Add(p);


                     var result = from x in series
                                  group x by new { x.ArticuloClave }
                                      into g
                                      select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                     foreach (var a in result)
                     {
                         ProcesoArticulo pa = new ProcesoArticulo();
                         pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                         pa.Cantidad = a.Cantidad;
                         pa.ProcesoClave = p.ProcesoClave;
                         dc.ProcesoArticulo.Add(pa);
                     }

                     foreach (var Articulo in series)
                     {

                         ProcesoInventario pi = new ProcesoInventario();
                         pi.ProcesoClave = p.ProcesoClave;
                         pi.InventarioClave = Articulo.InventarioClave;
                         pi.Cantidad = Articulo.Cantidad;
                         pi.Fecha = DateTime.Now;
                         dc.ProcesoInventario.Add(pi);


                         Inventario InvTecnico;
                         try
                         {
                             InvTecnico = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == Tecnico.EntidadClave && o.EstatusInv == 7).First();

                         }
                         catch
                         {
                             dbContextTransaction.Rollback();
                             error e1 = new error();
                             e1.idError = -4;
                             e1.Mensaje = " El aparato " + Articulo.Valor + " No se encuentra en el saldo del técnico";
                             return Json(e1, JsonRequestBehavior.AllowGet);

                         }

                         if (tieneSeries(Articulo.ArticuloClave))
                         {
                             if (InvTecnico.Standby == 1 && InvTecnico.Cantidad.Value == 0)
                             {
                                 dbContextTransaction.Rollback();
                                 error e1 = new error();
                                 e1.idError = -5;
                                 e1.Mensaje = " El aparato " + Articulo.Valor + " No se encuentra en el saldo del técnico ,se encuentra en standby ";
                                 return Json(e1, JsonRequestBehavior.AllowGet);
                             }

                             InvTecnico.EntidadClave = Almacen.EntidadClave;                             
                            Devolucion(InvTecnico, p.EntidadClaveSolicitante.Value,p.EntidadClaveAfectada.Value);
                             if (InvTecnico.Articulo.catTipoArticuloClave == 15)
                             {
                                 try
                                 {
                                    string Mac = InvTecnico.Serie.Select(o => o.Valor).SingleOrDefault();
                                    dc.Database.ExecuteSqlCommand("exec SP_DevolucionTecnicos_Por_Mac @mac",new SqlParameter("@mac", Mac));                                   
                                 }
                                 catch
                                 {}                              

                             }
                             else
                             {                      
                               


                             }





                         }
                         else
                         {

                             if (InvTecnico.Cantidad < Articulo.Cantidad)
                             {
                                 dbContextTransaction.Rollback();
                                 error e1 = new error();
                                 e1.idError = -6;
                                 e1.Mensaje = " El saldo del artículo: " + Articulo.Nombre + "Es menor a la cantidad solicitada";
                                 return Json(e1, JsonRequestBehavior.AllowGet);

                             }


                             InvTecnico.Cantidad = InvTecnico.Cantidad - Articulo.Cantidad;

                             List<Inventario> invfinal_l = (from i in dc.Inventario
                                                                where i.ArticuloClave == Articulo.ArticuloClave
                                                                && i.EntidadClave == p.EntidadClaveSolicitante
                                                                && i.Estatus == 1 && i.EstatusInv == 7
                                                                select i).ToList();

                             Inventario invfinal = null;

                             if (invfinal_l.Count > 0)
                             {
                                 invfinal = invfinal_l[0];
                             }

                             if (invfinal != null)
                             {
                                 invfinal.Cantidad += Articulo.Cantidad;
                                 invfinal.Standby = 0;
                                 invfinal.Estatus = 1;
                             }





                         }




                     }

                     dc.SaveChanges();
                     dbContextTransaction.Commit();
                     return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                 }
                 catch
                 {
                     dbContextTransaction.Rollback();
                     error e1 = new error();
                     e1.idError = -7;
                     e1.Mensaje = " No se pudo realizar la devolución, contacte al administrador";
                     return Json(e1, JsonRequestBehavior.AllowGet);

                 }
             }
        }
        public void Devolucion(web.Models.Inventario inventario, Guid claveEntidadAfectada, Guid claveEntidadSolicitante)
        {
            web.Models.ModeloAlmacen dc = new web.Models.ModeloAlmacen();
            web.Models.Newsoftv.NewSoftvModel ns = new web.Models.Newsoftv.NewSoftvModel();
            web.Models.Articulo art = inventario.Articulo;
            web.Models.catTipoArticulo tipo = art.catTipoArticulo;
            string mac = "";
            try
            {
                mac = inventario.Serie.Select(x => x.Valor).First();
            }
            catch
            {
                mac = dc.Serie.Where(e => e.InventarioClave == inventario.InventarioClave).Select(o => o.Valor).First().ToString();
            }
            web.Models.Entidad plaza = (from e in dc.Entidad
                                        where e.EntidadClave == claveEntidadAfectada
                                        select e).SingleOrDefault();
            web.Models.Entidad tecnico = (from t in dc.Entidad
                                          where t.EntidadClave == claveEntidadSolicitante
                                          select t).SingleOrDefault();
            try
            {
                web.Models.Entidad ent = (from a in dc.Entidad where a.EntidadClave == inventario.EntidadClave select a).FirstOrDefault();

                try
                {

                    ns.Database.ExecuteSqlCommand("exec SP_DevolucionCablemodems @IdDistribuidor,@IdPlaza,@IdTecnico,@IdAparato,@Mac,@TipoAparato,@Marca",
                        new SqlParameter("@IdDistribuidor", Convert.ToInt32(ent.catDistribuidor.IdDistribuidor)),
                         new SqlParameter("@IdPlaza", Convert.ToInt32(plaza.IdEntidad)),
                         new SqlParameter("@IdTecnico", Convert.ToInt32(tecnico.IdEntidad)),
                         new SqlParameter("@IdAparato", Convert.ToInt32(inventario.IdInventario)),
                         new SqlParameter("@Mac", mac),
                         new SqlParameter("@TipoAparato", art.catTipoArticulo.Letra),
                         new SqlParameter("@Marca", art.Nombre)
                        );
                }
                catch
                {

                }
            }
            catch (Exception ex)
            {}

        }



        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            if (Request["s"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["s"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso pro = (from p in dc.Proceso
                               where p.ProcesoClave == clave
                               select p).SingleOrDefault();

            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();


            Entidad e = pro.Entidad;
            Usuario u = pro.Usuario;

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");

            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h3><b>DEVOLUCION DE MATERIAL</b></h3>");
            sb.Append("<h3<b>Salida: #</b>" + pro.NumeroPedido + "</h3>");
            
            sb.Append(@"<b  style=""font-size:10px;"">Almacén:</b><span  style=""font-size:10px;"">" + e.Nombre+"</span> <br>");
            Entidad tecnico = (from d in dc.Entidad
                               where d.EntidadClave == pro.EntidadClaveAfectada
                               select d).SingleOrDefault();
            sb.Append(@"<b  style=""font-size:10px;"">Técnico:</b><span>" + tecnico.Nombre + "</span><br>");
            sb.Append(@"<b style=""font-size:10px;"">Fecha: </b><span style=""font-size:10px;"">" + pro.Fecha.Value.ToShortDateString() + "</span><br>");
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<font size=2>");
             sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead><tr>");
            sb.Append(@"<th style=""font-size:10px;""><b>Cantidad</b></th>");
            sb.Append(@"<th  style=""font-size:10px;"" colspan=""3""><b>Concepto</b></th>");
            sb.Append(@"<th style=""font-size:10px;"" ><b>Unidad</b></th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                if (pa.Cantidad > 0)
                {

                    Articulo a = pa.Articulo;
                    sb.Append("<tr>");
                    sb.Append(@"<td style=""font-size:10px;"">" + pa.Cantidad + "</td>");
                    sb.Append(@"<td style=""font-size:10px;"" colspan=""3"">" + a.Nombre + "</td>");
                    sb.Append(@"<td style=""font-size:10px;"" >" + a.catUnidad.Nombre + "</td>");
                    sb.Append("</tr>");

                    sb.Append(@"<tr colspan=5>");
                    sb.Append(@"<td style=""font-size:8px;"">");
                    if (a.catTipoArticulo.SerieTipoArticulo.Count() > 0)
                    {

                        List<Serie> series = (from aa in dc.ProcesoInventario
                                              join bb in dc.Serie on aa.InventarioClave equals bb.InventarioClave
                                              join cc in dc.Inventario on bb.InventarioClave equals cc.InventarioClave
                                              where cc.ArticuloClave == a.ArticuloClave && aa.ProcesoClave == pro.ProcesoClave
                                              select bb
                                             ).ToList();
                        sb.Append("<b>Series:</b><br>");
                        series.ForEach(se =>
                        {
                            sb.Append("<span>" + se.Valor + " - </span>");
                        });
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("<br>");
            sb.Append("<b>Observaciones:</b><br>");
            sb.Append(pro.Observaciones);
            //sb.Append("</td></tr></table>");


            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }


            sb.Append("</font>");


            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "pedido.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Pedido" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "pedido.pdf" + "' style='position: relative; height: 90%; width: 90%;'frameborder=0></iframe>";
                    return View();
                }
            }



        }


    }
}
