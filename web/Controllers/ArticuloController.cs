﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Wrappers;
using web.Controllers;
using System.Data.SqlClient;
using System.Xml.Linq;
using web.Models;
using System.Web.Configuration;

namespace Web.Controllers
{
    public class ArticuloController : Controller
    {
        //
        // GET: /Articulo/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();


            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                              where ta.Activo
                                                              orderby ta.Descripcion
                                                              select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();


            List<catProveedor> proveedores = (from ta in dc.catProveedor
                                              where ta.Activo
                                              orderby ta.Nombre
                                              select ta).ToList();


            List<catTipoMoneda> tiposmoneda = (from ta in dc.catTipoMoneda
                                               where ta.Activo
                                               orderby ta.Descripcion
                                               select ta).ToList();

            List<catUnidad> unidades = (from ta in dc.catUnidad
                                        where ta.Activo
                                        orderby ta.Nombre
                                        select ta).ToList();

            Guid gentidad = Guid.Parse("00000000-0000-0000-0000-000000000001");

            List<catUbicacion> ubicaciones = (from ta in dc.catUbicacion
                                              where ta.Activo && ta.EntidadClave == gentidad
                                              orderby ta.Nombre
                                              select ta).ToList();
            AlmacenController ac = new AlmacenController();
            List<CatTipoServicios> servicios = dc.CatTipoServicios.ToList();


            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["proveedores"] = proveedores;
            ViewData["tiposmoneda"] = tiposmoneda;
            ViewData["ubicaciones"] = ubicaciones;
            ViewData["unidades"] = unidades;
            ViewData["servicios"] = servicios;

            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPorTipo2(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);

            int tipo = -1;
            int cla = -1;
            int pagare = -1;
            bool soypedido = false;
            try
            {
                tipo = Convert.ToInt32(jObject["tipo"].ToString());
            }
            catch
            {
            }

            try
            {
                soypedido = bool.Parse(jObject["Pedido"].ToString());
            }
            catch
            {
            }



            try
            {
                cla = Convert.ToInt32(jObject["cla"].ToString());
            }
            catch
            {
            }

            try
            {
                pagare = Convert.ToInt32(jObject["pagare"].ToString());
            }
            catch
            {
            }

            ModeloAlmacen dc = new ModeloAlmacen();


            List<Articulo> rls = new List<Articulo>();

            try
            {
                Usuario u = ((Usuario)Session["u"]);

                if (soypedido == true && u.DistribuidorClave.HasValue)
                {
                    Guid alm = Guid.Parse(jObject["Almacen"].ToString());
                    int servicio = Int32.Parse(jObject["Servicio"].ToString());

                    rls = GetArticulosByServicio(alm, tipo, servicio);


                }
                else
                {

                    int pag = Convert.ToInt32(jObject["pag"].ToString());
                    int skip = (pag - 1) * elementos;


                    rls = (from use in dc.Articulo
                           where
                           (use.catTipoArticulo.catClasificacionArticuloClave == cla || cla == -1)
                           && (use.catTipoArticuloClave == tipo || tipo == -1)
                           && (pagare == -1 || (pagare == 1 && use.catTipoArticulo.EsPagare.Value) || (pagare == 0 && (!use.catTipoArticulo.EsPagare.HasValue || !use.catTipoArticulo.EsPagare.Value)))
                           orderby use.Nombre
                           select use).Skip(skip).Take(elementos).ToList();


                }


            }
            catch
            {
                rls = (from use in dc.Articulo
                       where
                       (use.catTipoArticulo.catClasificacionArticuloClave == cla || cla == -1)
                       && (use.catTipoArticulo.catTipoArticuloClave == tipo || tipo == -1)
                       && (pagare == -1 || (pagare == 1 && use.catTipoArticulo.EsPagare.Value) || (pagare == 0 && (!use.catTipoArticulo.EsPagare.HasValue || !use.catTipoArticulo.EsPagare.Value)))
                       orderby use.Nombre
                       select use).ToList();
            }

            List<ArticuloWrapper> rw = new List<ArticuloWrapper>();


            bool conserie = false;

            if (Request["cs"] != null && Request["cs"].ToString() == "1")
            {
                conserie = true;
            }


            var almacen = Guid.Empty;
            try
            {
                almacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch
            {
            }


            if (rls.Count > 0)
            {
                foreach (Articulo u in rls)
                {
                    ArticuloWrapper w = ArticuloToWrapper(u, conserie);


                    if (almacen != Guid.Empty)
                    {

                        List<Inventario> invs = (from i in dc.Inventario
                                                 where
                                                 i.EntidadClave == almacen
                                                 && i.ArticuloClave == u.ArticuloClave
                                                 && i.Estatus.HasValue && i.Estatus.Value == 1
                                                 && i.EstatusInv == 7
                                                 select i).ToList();
                        if (invs.Count() == 0)
                        {
                            w.Existencias = "0";
                        }
                        else
                        {
                            int conta = 0;
                            foreach (Inventario inv in invs)
                            {
                                conta += inv.Cantidad.Value;
                            }
                            w.Existencias = conta.ToString();
                        }
                    }
                    else
                    {
                        w.Existencias = "0";
                    }

                    rw.Add(w);
                }

            }
            else
            {

                return Json(rw, JsonRequestBehavior.AllowGet);

            }




            return Json(rw, JsonRequestBehavior.AllowGet);
        }




        public ActionResult ObtenPorTipo(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaArticulos(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaArticulos(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaArticulos(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaArticulos(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<ArticuloWrapper> data { get; set; }
        }

        public List<ArticuloWrapper> ListaArticulos(string data)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);

            int tipo = -1;
            int cla = -1;
            int pagare = -1;
            bool soypedido = false;
            try
            {
                tipo = Convert.ToInt32(jObject["tipo"].ToString());
            }
            catch
            {
            }

            try
            {
                soypedido = bool.Parse(jObject["Pedido"].ToString());
            }
            catch
            {
            }



            try
            {
                cla = Convert.ToInt32(jObject["cla"].ToString());
            }
            catch
            {
            }

            try
            {
                pagare = Convert.ToInt32(jObject["pagare"].ToString());
            }
            catch
            {
            }

            ModeloAlmacen dc = new ModeloAlmacen();


            List<Articulo> rls = new List<Articulo>();

            try
            {
                Usuario u = ((Usuario)Session["u"]);

                if (soypedido == true && u.DistribuidorClave.HasValue)
                {
                    Guid alm = Guid.Parse(jObject["Almacen"].ToString());
                    int servicio = Int32.Parse(jObject["Servicio"].ToString());

                    rls = GetArticulosByServicio(alm, tipo, servicio);


                }
                else
                {

                    int pag = Convert.ToInt32(jObject["pag"].ToString());
                    int skip = (pag - 1) * elementos;


                    rls = (from use in dc.Articulo
                           where
                           (use.catTipoArticulo.catClasificacionArticuloClave == cla || cla == -1)
                           && (use.catTipoArticuloClave == tipo || tipo == -1)
                           && (pagare == -1 || (pagare == 1 && use.catTipoArticulo.EsPagare.Value) || (pagare == 0 && (!use.catTipoArticulo.EsPagare.HasValue || !use.catTipoArticulo.EsPagare.Value)))
                           orderby use.Nombre
                           select use).Skip(skip).Take(elementos).ToList();


                }


            }
            catch
            {
                rls = (from use in dc.Articulo
                       where
                       (use.catTipoArticulo.catClasificacionArticuloClave == cla || cla == -1)
                       && (use.catTipoArticulo.catTipoArticuloClave == tipo || tipo == -1)
                       && (pagare == -1 || (pagare == 1 && use.catTipoArticulo.EsPagare.Value) || (pagare == 0 && (!use.catTipoArticulo.EsPagare.HasValue || !use.catTipoArticulo.EsPagare.Value)))
                       orderby use.Nombre
                       select use).ToList();
            }

            List<ArticuloWrapper> rw = new List<ArticuloWrapper>();


            bool conserie = false;

            if (Request["cs"] != null && Request["cs"].ToString() == "1")
            {
                conserie = true;
            }


            var almacen = Guid.Empty;
            try
            {
                almacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch
            {
            }


            if (rls.Count > 0)
            {
                foreach (Articulo u in rls)
                {
                    ArticuloWrapper w = ArticuloToWrapper(u, conserie);


                    if (almacen != Guid.Empty)
                    {

                        List<Inventario> invs = (from i in dc.Inventario
                                                 where
                                                 i.EntidadClave == almacen
                                                 && i.ArticuloClave == u.ArticuloClave
                                                 && i.Estatus.HasValue && i.Estatus.Value == 1
                                                 && i.EstatusInv == 7
                                                 select i).ToList();
                        if (invs.Count() == 0)
                        {
                            w.Existencias = "0";
                        }
                        else
                        {
                            int conta = 0;
                            foreach (Inventario inv in invs)
                            {
                                conta += inv.Cantidad.Value;
                            }
                            w.Existencias = conta.ToString();
                        }
                    }
                    else
                    {
                        w.Existencias = "0";
                    }

                    rw.Add(w);
                }

            }
            else
            {
                return rw;

            }
            return rw;
        }


        public ActionResult Obten(string data)
        {
            ArticuloWrapper rw = new ArticuloWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid uid = Guid.Parse(data);

            Articulo r = (from ro in dc.Articulo
                          where ro.ArticuloClave == uid
                          select ro).SingleOrDefault();

            if (r != null)
            {
                rw = ArticuloToWrapper(r, false);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    Articulo r = (from ro in dc.Articulo
                                  where ro.ArticuloClave == uid
                                  select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    Guid uid = Guid.Parse(activa_id);

                    Articulo r = (from ro in dc.Articulo
                                  where ro.ArticuloClave == uid
                                  select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_tipomaterial, string ed_clasificacionmaterial, string ed_tope, string ed_unidad, string ed_ubicacion, string ed_proveedor1, string ed_precio, string ed_moneda, string ed_precio1, string ed_moneda1, string ed_proveedor2, string ed_precio2, string ed_moneda2, string ed_proveedor3, string ed_precio3, string ed_moneda3, string ed_activa, string ed_servicio, string parte)
        {
            string serv = "";
            try
            {
                serv = Request.Params["servicio"].ToString();
            }
            catch
            {
                return Content("-3");
            }
            string[] Servicios = serv.Split(',');
            XElement element = new XElement("root", Servicios.Select(x => new XElement("servicio", x)));
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo r = new Articulo();
            if (ed_id != String.Empty)
            {
                Guid uid = Guid.Parse(ed_id);

                r = (from ro in dc.Articulo
                     where ro.ArticuloClave == uid
                     select ro).SingleOrDefault();
            }
            else
            {
                r.ArticuloClave = Guid.NewGuid();
            }

            r.Nombre = ed_nombre;
            r.Activo = (ed_activa == "on") ? true : false;
            r.catTipoArticuloClave = Convert.ToInt32(ed_tipomaterial);
            r.TopeMinimo = Convert.ToInt32(ed_tope);
            r.catUnidadClave = Convert.ToInt32(ed_unidad);
            r.Porcentaje = 0;
            r.PrecioPagare = Convert.ToDecimal(ed_precio);
           // r.parte = parte;
            if (ed_moneda == null)
            {
                ed_moneda = "4";
            }
            r.catTipoMonedaClave = Convert.ToInt32(ed_moneda);
            //Convert.ToInt32(ed_moneda);
            r.Existencias = 0;

            if (ed_ubicacion != String.Empty)
            {
                r.catUbicacionClave = Convert.ToInt32(ed_ubicacion);
            }


            if (ed_id == String.Empty)
            {
                dc.Articulo.Add(r);
            }
            try
            {
                dc.SaveChanges();
            }
            catch
            {

            }
            List<ArticuloProveedor> artpro = (from ap in dc.ArticuloProveedor
                                              where ap.ArticuloClave == r.ArticuloClave
                                              select ap).ToList();

            foreach (ArticuloProveedor ap in artpro)
            {
                dc.ArticuloProveedor.Remove(ap);
            }
            dc.SaveChanges();


            if (ed_proveedor1 != null && ed_proveedor1 != String.Empty)
            {
                ArticuloProveedor ap1 = new ArticuloProveedor();
                ap1.ArticuloClave = r.ArticuloClave;
                ap1.catProveedorClave = Guid.Parse(ed_proveedor1);
                ap1.Precio = Convert.ToDecimal(ed_precio1);
                ap1.catTipoMonedaClave = Convert.ToInt32(ed_moneda1);
                ap1.Indice = 1;
                ap1.Activo = true;

                dc.ArticuloProveedor.Add(ap1);
            }

            if (ed_proveedor2 != null && ed_proveedor2 != String.Empty)
            {
                ArticuloProveedor ap2 = new ArticuloProveedor();
                ap2.ArticuloClave = r.ArticuloClave;
                ap2.catProveedorClave = Guid.Parse(ed_proveedor2);
                ap2.Precio = Convert.ToDecimal(ed_precio2);
                ap2.catTipoMonedaClave = Convert.ToInt32(ed_moneda2);
                ap2.Indice = 2;
                ap2.Activo = true;

                dc.ArticuloProveedor.Add(ap2);
            }

            if (ed_proveedor3 != null && ed_proveedor3 != String.Empty)
            {
                ArticuloProveedor ap3 = new ArticuloProveedor();
                ap3.ArticuloClave = r.ArticuloClave;
                ap3.catProveedorClave = Guid.Parse(ed_proveedor3);
                ap3.Precio = Convert.ToDecimal(ed_precio3);
                ap3.catTipoMonedaClave = Convert.ToInt32(ed_moneda3);
                ap3.Indice = 3;
                ap3.Activo = true;

                dc.ArticuloProveedor.Add(ap3);
            } try
            {
                dc.Database.ExecuteSqlCommand("exec Asigna_servicio_articulo @xml,@idArticulo",
                    new SqlParameter("@xml", element.ToString()),
                     new SqlParameter("@idArticulo", r.IdArticulo)
                    );


            }
            catch
            {

                return Content("-2");

            }





            dc.SaveChanges();
            return Content("1");


        }





        private ArticuloWrapper ArticuloToWrapper(Articulo u, bool conseries)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            ArticuloWrapper rw = new ArticuloWrapper();
            rw.Clave = u.ArticuloClave.ToString();
            rw.Descripcion = u.Nombre;
            rw.Existencias = u.Existencias.ToString();
            rw.TopeMinimo = u.TopeMinimo.ToString();
            rw.catUnidadClave = u.catUnidadClave.ToString();
            rw.Porcentaje = u.Porcentaje.ToString();
            rw.catUnidadClave = u.catUnidadClave.ToString();
            rw.catTipoArticuloClave = u.catTipoArticuloClave.ToString();
            rw.EsPagare = Convert.ToInt32(u.catTipoArticulo.EsPagare).ToString();
            rw.PrecioPagare = "0";
           // rw.parte = u.parte;
            try
            {
                rw.PrecioPagare = u.PrecioPagare.Value.ToString("C").Replace("$", "");
            }
            catch
            { }

            rw.catMonedaPagareClave = u.catTipoMonedaClave.ToString();
            rw.Pagare = "0";
            if (conseries)
            {
                rw.catSeriesClaves = new List<catSerieWrapper>();
                List<web.Models.SerieTipoArticulo> tiposarticulo = u.catTipoArticulo.SerieTipoArticulo.ToList();
                foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
                {
                    catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                    rw.catSeriesClaves.Add(csw);
                }
            }

            rw.tieneseries = u.catTipoArticulo.SerieTipoArticulo.Count().ToString();


            try
            {
                catTipoArticulo t = u.catTipoArticulo;
                rw.TipoArticulo = t.Descripcion;
                rw.Clasificacion = t.catClasificacionArticulo.Descripcion;
            }
            catch { }


            try
            {
                rw.catClasificacionMaterialClave = u.catTipoArticulo.catClasificacionArticuloClave.ToString();
            }
            catch { }



            List<ArticuloProveedor> articulosproveedor = (from ap in dc.ArticuloProveedor
                                                          where ap.ArticuloClave == u.ArticuloClave
                                                          orderby ap.Indice
                                                          select ap).ToList();

            try
            {
                rw.proveedor1 = articulosproveedor[0].catProveedorClave.ToString();
                rw.precio1 = articulosproveedor[0].Precio.ToString();
                rw.moneda1 = articulosproveedor[0].catTipoMonedaClave.ToString();
            }
            catch { }

            try
            {
                rw.proveedor2 = articulosproveedor[1].catProveedorClave.ToString();
                rw.precio2 = articulosproveedor[1].Precio.ToString();
                rw.moneda2 = articulosproveedor[1].catTipoMonedaClave.ToString();
            }
            catch { }


            try
            {
                rw.proveedor3 = articulosproveedor[2].catProveedorClave.ToString();
                rw.precio3 = articulosproveedor[2].Precio.ToString();
                rw.moneda3 = articulosproveedor[2].catTipoMonedaClave.ToString();
            }
            catch { }

            rw.catUbicacionClave = u.catUbicacionClave.ToString();
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            rw.Servicios = (from a in dc.Tbl_articulo_servicio join b in dc.CatTipoServicios on a.IdServicio equals b.IdServicio where a.IdArticulo == u.IdArticulo select b.Descripcion).ToList();
            rw.idServicios=(from a in dc.Tbl_articulo_servicio join b in dc.CatTipoServicios on a.IdServicio equals b.IdServicio where a.IdArticulo == u.IdArticulo select b.IdServicio.ToString()).ToList();
            return rw;
        }


        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }
        public string a()
        {
            return "response";
        }



        public List<Articulo> GetArticulosByServicio(Guid almacen, int tipo, int idser)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            List<Articulo> lista = new List<Articulo>();
            int id = dc.Entidad.Where(x => x.EntidadClave == almacen).Select(x => x.idEntidadSofTV).First();
            Connection c = new Connection();
            SqlConnection conexionSQL = new SqlConnection(c.GetConnectionString());
            try
            {

                conexionSQL.Open();
                SqlCommand comandoSql = new SqlCommand("exec MuestraArticulosAsigAlmacen  @idalmacen,@idtipo,@servicio");
                comandoSql.Parameters.AddWithValue("@idalmacen", id);
                comandoSql.Parameters.AddWithValue("@idtipo", tipo);
                comandoSql.Parameters.AddWithValue("@servicio", idser);
                comandoSql.Connection = conexionSQL;
                SqlDataReader reader = comandoSql.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Guid art = Guid.Parse(reader[0].ToString());
                        Articulo a = dc.Articulo.Where(x => x.ArticuloClave == art).FirstOrDefault();

                        lista.Add(a);
                    }
                }
                conexionSQL.Close();
            }
            catch(Exception e)
            {
                conexionSQL.Close();
            }
            return lista;
        }


        public ActionResult GetArticuloByTipo (int ? id)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Articulo> arti = dc.Articulo.Where(y => y.catTipoArticuloClave == id && y.Activo == true).ToList();
            return Json(arti.Select(x=> new { x.ArticuloClave,x.Nombre }), JsonRequestBehavior.AllowGet);

        }



    }
}
