﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class TipoSalidaController : Controller
    {
        //
        // GET: /TipoSalida/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;



        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catTipoSalidaWrapper> data { get; set; }
        }




        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = listaSalidas(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = listaSalidas(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);

                      
        }

        public List<catTipoSalidaWrapper> listaSalidas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catTipoSalida> rls = (from use in dc.catTipoSalida
                                           orderby use.Descripcion
                                           select use).ToList();

            List<catTipoSalidaWrapper> rw = new List<catTipoSalidaWrapper>();
            foreach (catTipoSalida u in rls)
            {
                catTipoSalidaWrapper w = TipoSalidaToWrapper(u);
                rw.Add(w);
            }

            return rw;

        }




        public ActionResult Obten(string data)
        {
            catTipoSalidaWrapper rw = new catTipoSalidaWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catTipoSalida r = (from ro in dc.catTipoSalida
                                   where ro.catTipoSalidaClave == uid
                                              select ro).SingleOrDefault();

            if (r != null)
            {
                rw = TipoSalidaToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catTipoSalida r = (from ro in dc.catTipoSalida
                                           where ro.catTipoSalidaClave == uid
                                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catTipoSalida r = (from ro in dc.catTipoSalida
                                           where ro.catTipoSalidaClave == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_activa)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catTipoSalida r = new catTipoSalida();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catTipoSalida
                         where ro.catTipoSalidaClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Descripcion = ed_nombre;
                r.Activo = (ed_activa == "on") ? true : false;

                if (ed_id == String.Empty)
                {
                    dc.catTipoSalida.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        private catTipoSalidaWrapper TipoSalidaToWrapper(catTipoSalida u)
        {
            catTipoSalidaWrapper rw = new catTipoSalidaWrapper();
            rw.Clave = u.catTipoSalidaClave.ToString();
            rw.Descripcion = u.Descripcion;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }

    }
}
