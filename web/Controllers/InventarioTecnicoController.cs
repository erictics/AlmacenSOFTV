﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class InventarioTecnicoController : Controller
    {

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<TecnicoW> data { get; set; }
        }

        public class TecnicoW
        {
            public Guid IdEntidad { get; set; }
            public string Nombre { get; set; }
            public string almacen { get; set; }
        }

        public ActionResult ObtenTecnicos(Guid distribuidor, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaTecnicos(distribuidor).OrderBy(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaTecnicos(distribuidor).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<TecnicoW> ListaTecnicos(Guid distribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<TecnicoW> lista = dc.Entidad.Where(s => s.catDistribuidorClave == distribuidor && s.catEntidadTipoClave == 2)
                .Select(o => new TecnicoW { IdEntidad = o.EntidadClave, Nombre = o.Nombre }).ToList();

            List<TecnicoW> tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == distribuidor select new TecnicoW { Nombre= a.Nombre, IdEntidad= a.EntidadClave }).ToList();
            lista.AddRange(tecgen);

            return lista;
        }

        public class StatusInv
        {

            public string Nombre { get; set; }
            public int enAlmacen { get; set; }

        }


        public ActionResult GetInventario(Guid tecnico)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Articulo> articulos = dc.Articulo.ToList();
            List<StatusInv> lista = new List<StatusInv>();

            foreach (var b in articulos)
            {
                StatusInv obj = new StatusInv();
                obj.Nombre = b.Nombre;
                int? activos = (from a in dc.Inventario
                                where a.ArticuloClave == b.ArticuloClave
                                && a.EntidadClave == tecnico
                                && a.EstatusInv == 7
                                && a.Estatus.HasValue                               
                                && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                select a).Sum(a => a.Cantidad);

                int cuantos_activos = 0;
                try
                {
                    cuantos_activos = activos.Value;
                }
                catch
                {}

                obj.enAlmacen = cuantos_activos;
                if (cuantos_activos > 0)
                {
                    lista.Add(obj);
                }
                
              }
            return Json(lista, JsonRequestBehavior.AllowGet);

        }
    }
}
