﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class TecnicoController : Controller
    {
        //
        // GET: /Tecnico/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where 
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();            
            }


            ViewData["distribuidor"] = distribuidor;


            List<catPuestoTecnico> puestostecnico = (from c in dc.catPuestoTecnico
                                                         where c.Activo
                                                      orderby c.Descripcion
                                                      select c).ToList();
            ViewData["puestostecnico"] = puestostecnico;


            
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        int elementos = 15;



        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<TecnicoWrapper> data { get; set; }
        }

        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaTecnicos(data).OrderByDescending(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaTecnicos(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);

        }



        public List<TecnicoWrapper> ListaTecnicos(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            Guid dis = Guid.Empty;
            if (jObject["dis"].ToString() != "-1")
            {
                dis = Guid.Parse(jObject["dis"].ToString());
            }            ModeloAlmacen dc = new ModeloAlmacen();

            List<Entidad> rls = (from use in dc.Entidad
                                             where use.catEntidadTipoClave == 2
                                                && (use.catDistribuidorClave == dis || (dis == Guid.Empty && use.catDistribuidorClave == null))
                                             orderby use.Nombre
                                             select use).ToList();

            List<TecnicoWrapper> rw = new List<TecnicoWrapper>();

            foreach (Entidad u in rls)
            {
                TecnicoWrapper w = TecnicoToWrapper(u);
                rw.Add(w);
            }

            return rw;
        }

        public ActionResult Obten(string data)
        {
            TecnicoWrapper rw = new TecnicoWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid uid = Guid.Parse(data);
            Entidad r = (from ro in dc.Entidad
                             where ro.EntidadClave == uid
                             select ro).SingleOrDefault();

            if (r != null)
            {
                rw = TecnicoToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    Entidad r = (from ro in dc.Entidad
                                     where ro.EntidadClave == uid
                                             select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    Guid uid = Guid.Parse(activa_id);

                    Entidad r = (from ro in dc.Entidad
                                     where ro.EntidadClave == uid
                                     select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public string QuitAccents(string inputString)
        {
            Regex a = new Regex("[á|à|ä|â]", RegexOptions.Compiled);
            Regex e = new Regex("[é|è|ë|ê]", RegexOptions.Compiled);
            Regex i = new Regex("[í|ì|ï|î]", RegexOptions.Compiled);
            Regex o = new Regex("[ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex u = new Regex("[ú|ù|ü|û]", RegexOptions.Compiled);
            Regex n = new Regex("[ñ|Ñ]", RegexOptions.Compiled);
            inputString = a.Replace(inputString, "a");
            inputString = e.Replace(inputString, "e");
            inputString = i.Replace(inputString, "i");
            inputString = o.Replace(inputString, "o");
            inputString = u.Replace(inputString, "u");
            inputString = n.Replace(inputString, "n");
            return inputString;
        }

        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_distr, string ed_puesto, string ed_activa)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            if (ed_id == String.Empty){


                 List<Entidad> tecnicos = (from d in dc.Entidad where d.catEntidadTipoClave == 2 select d).ToList();
                    foreach (var a in tecnicos)
                    {

                        if (QuitAccents(ed_nombre).ToUpper() == QuitAccents(a.Nombre).ToUpper())
                        {
                            return Json("-1", JsonRequestBehavior.AllowGet);
                        }

                    }



            }// si se edita tecnico
            else{

                Guid tec = Guid.Empty;
            try
            {
                tec = Guid.Parse(ed_id);
            }
            catch
            {

            }
            string nombre_anterior=(from ent in dc.Entidad where ent.EntidadClave==tec select ent.Nombre).FirstOrDefault();
            if (QuitAccents(ed_nombre).ToUpper() == QuitAccents(nombre_anterior).ToUpper())
            {

            }
            else
            {
                List<Entidad> tecnicos = (from d in dc.Entidad where d.catEntidadTipoClave == 2 select d).ToList();
                foreach (var a in tecnicos)
                {

                    if (QuitAccents(ed_nombre).ToUpper() == QuitAccents(a.Nombre).ToUpper())
                    {
                        return Json("-1", JsonRequestBehavior.AllowGet);
                    }

                }

            }

            }


                                    


            try
            {

                Entidad r = new Entidad();

                if (ed_id != String.Empty)
                {
                    Guid uid = Guid.Parse(ed_id);

                    r = (from ro in dc.Entidad
                         where ro.EntidadClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {
                    r.EntidadClave = Guid.NewGuid();
                }

                r.catEntidadTipoClave = 2;

                if (ed_distr != "-1")
                {
                    r.catDistribuidorClave = Guid.Parse(ed_distr);
                }
                else
                {
                    r.catDistribuidorClave = null;
                }
                r.Nombre = ed_nombre;
                r.Activo = (ed_activa == "on") ? true : false;

                if (ed_id == String.Empty)
                {
                    dc.Entidad.Add(r);
                }
                dc.SaveChanges();


                PuestoTecnico pu = (from p in dc.PuestoTecnico
                                        where p.EntidadClave == r.EntidadClave
                                        select p).SingleOrDefault();

                bool nuevo = false;
                if (pu == null)
                {
                    pu = new PuestoTecnico();
                    nuevo = true;
                }

                pu.EntidadClave = r.EntidadClave;
                pu.catPuestosTecnicosClave = Convert.ToInt32(ed_puesto);
                pu.Activo = true;

                if (nuevo)
                {
                    dc.PuestoTecnico.Add(pu);
                }

                dc.SaveChanges();



                Guid usuarioclave = r.EntidadClave;

                List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                                  where a.EntidadClaveTecnico == usuarioclave
                                                  select a).ToList();

                foreach (AmbitoAlmacen a in ambito)
                {
                    dc.AmbitoAlmacen.Remove(a);
                }
                dc.SaveChanges();



                Guid? distribuidorguid = r.catDistribuidorClave;

                List<Entidad> rl = (from ro in dc.Entidad
                                        where ro.catDistribuidorClave == distribuidorguid
                                        select ro).ToList();

                List<AlmacenWrapper> rwl = new List<AlmacenWrapper>();

                foreach (Entidad rr in rl)
                {

                    if (Request["almacen" + rr.EntidadClave] == "on")
                    {
                        AmbitoAlmacen aa = new AmbitoAlmacen();
                        aa.AmbitoAlmacenClave = Guid.NewGuid();
                        aa.EntidadClaveTecnico = usuarioclave;
                        aa.EntidadClaveAlmacen = rr.EntidadClave;

                        dc.AmbitoAlmacen.Add(aa);
                    }
                }
                dc.SaveChanges();



                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }
        }

        public ActionResult ObtenArbol(string data)
        {
            int tipo = 1;

            if (Request["t"] != null)
            {
                tipo = Convert.ToInt32(Request["t"]);
            }

            return Content(Arbol(tipo));
        }

        private string Arbol(int tipo)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            StringBuilder sb = new StringBuilder();

            List<Entidad> r = null;

            Usuario u = ((Usuario)Session["u"]);

            r = (from e in dc.Entidad
                 where e.EntidadPadreClave == null
                     && e.catEntidadTipoClave == 2
                 orderby e.Nombre
                 select e).ToList();


            int conta = 0;
            foreach (Entidad e in r)
            {
                if (u.DistribuidorClave == null)
                {
                    sb.Append(Nodo(conta++, Guid.Empty, e.Nombre, e.EntidadClave, e.catDistribuidor, tipo, 1, e.Activo.Value));
                }
                else
                {
                    if (u.DistribuidorClave == e.EntidadClave || e.catDistribuidorClave == null)
                    {
                        sb.Append(Nodo(conta++, Guid.Empty, e.Nombre, e.EntidadClave, e.catDistribuidor, tipo, 1, e.Activo.Value));
                    }
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contador"></param>
        /// <param name="padre"></param>
        /// <param name="nombre"></param>
        /// <param name="clave"></param>
        /// <param name="distribuidor"></param>
        /// <param name="tipo">Tipo=1 --> Almacen; Tipo=2 --> Ubicación</param>
        /// <param name="soy">Soy=1 --> Almacen; Soy2=2 ---> Distribuidor </param>
        /// <returns></returns>
        private string Nodo(int contador, Guid padre, string nombre, Guid clave, catDistribuidor distribuidor, int tipo, int soy, bool activo)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            StringBuilder sb = new StringBuilder();

            string spadre = String.Empty;
            if (padre != Guid.Empty)
            {
                spadre = "treegrid-parent-" + padre;
            }


            if (soy == 2)
            {
                sb.Append("<tr class='treegrid-" + clave + " treegrid-parent-" + Guid.Parse("00000000-0000-0000-0000-000000000001") + "' rel='" + clave + "' data-tipo='2'>");
            }
            else
            {
                if (spadre == "treegrid-parent-00000000-0000-0000-0000-000000000001")
                {
                    sb.Append("<tr class='treegrid-" + clave + " treegrid-parent-" + distribuidor.catDistribuidorClave + "' rel='" + clave + "' data-txt='" + nombre + "'>");
                }
                else
                {
                    sb.Append("<tr class='treegrid-" + clave + " " + spadre + "' rel='" + clave + "' data-txt='" + nombre + "'>");
                }
            }

            if (soy == 1)
            {
                sb.Append("<td>" + nombre + "</td>");
            }
            else
            {
                sb.Append("<td><b>" + nombre + "</b></td>");
            }


            /*
            if (distribuidor == null)
            {
                sb.Append("<td>--</td>");
            }
            else
            {
                sb.Append("<td>"+ distribuidor.Nombre +"</td>");            
            }
            */

            if (tipo == 3)
            {
                sb.Append("<td>");
                if (soy == 1)
                {
                    sb.Append("<button type='button' class='btn btn-default btn-xs invent' rel='" + clave + "'>&nbsp;Inventario</button>");
                }
                sb.Append("</td>");
            }


            if (tipo == 2)
            {
                sb.Append("<td>");
                if (soy == 1)
                {
                    sb.Append("<button type='button' class='btn btn-default btn-xs ubi' rel='" + clave + "'>&nbsp;Ubicaciones</button>");
                }
                sb.Append("</td>");
            }


            if (tipo == 1)
            {
                if (soy == 2)
                {
                    clave = Guid.Parse("00000000-0000-0000-0000-000000000001");
                }


                sb.Append("<td>");

                if (padre != Guid.Empty && soy == 1)
                {

                    if (activo)
                    {
                        sb.Append("<img src='/Content/assets/images/activo.png'>");
                    }
                    else
                    {
                        sb.Append("<img src='/Content/assets/images/inactivo.png'>");
                    }
                }


                sb.Append("</td>");

                sb.Append("<td>");

                if (padre != Guid.Empty)
                {
                    sb.Append("<button type='button' class='btn btn-info btn-xs add' rel='" + clave + "' data-distr='" + distribuidor.catDistribuidorClave + "'>&nbsp;Agregar</button>");
                    if (soy == 1)
                    {
                        sb.Append("<button type='button' class='btn btn-default btn-xs edit' rel='" + clave + "' data-distr='" + distribuidor.catDistribuidorClave + "'>&nbsp;Editar</button>");
                    }
                }
                sb.Append("</td>");
            }


            sb.Append("</tr>");


            if (padre != Guid.Empty)
            {

                if (soy == 2)
                {
                    clave = Guid.Parse("00000000-0000-0000-0000-000000000001");
                }



                List<Entidad> r = null;


                Usuario u = ((Usuario)Session["u"]);

                if (u.DistribuidorClave == null)
                {
                    r = (from e in dc.Entidad
                         where e.EntidadPadreClave == clave
                             && e.catDistribuidorClave == distribuidor.catDistribuidorClave
                             && e.catEntidadTipoClave == 2
                         orderby e.Nombre
                         select e).ToList();
                }
                else
                {
                    r = (from e in dc.Entidad
                         where e.EntidadPadreClave == clave
                             && e.catDistribuidorClave == distribuidor.catDistribuidorClave
                             && (e.catDistribuidorClave == u.DistribuidorClave)
                             && e.catEntidadTipoClave == 2
                         orderby e.Nombre
                         select e).ToList();
                }


                int conta = 0;
                foreach (Entidad e in r)
                {
                    sb.Append(Nodo(conta++, clave, e.Nombre, e.EntidadClave, e.catDistribuidor, tipo, 1, e.Activo.Value));
                }
            }
            else
            {
                Usuario u = ((Usuario)Session["u"]);

                List<catDistribuidor> d = null;

                if (u.DistribuidorClave == null)
                {
                    d = (from di in dc.catDistribuidor
                         where di.Activo == true
                         orderby di.Nombre
                         select di).ToList();
                }
                else
                {
                    d = (from di in dc.catDistribuidor
                         where di.catDistribuidorClave == u.DistribuidorClave
                                && di.Activo == true
                         orderby di.Nombre
                         select di).ToList();
                }


                int conta = 0;
                foreach (catDistribuidor di in d)
                {
                    sb.Append(Nodo(conta++, di.catDistribuidorClave, di.Nombre, di.catDistribuidorClave, di, tipo, 2, di.Activo));
                }

            }


            return sb.ToString();
        }

        private AlmacenWrapper AlmacenToWrapper(Entidad e)
        {
            AlmacenWrapper aw = new AlmacenWrapper();
            aw.Clave = e.EntidadClave.ToString();
            aw.Nombre = e.Nombre;
            aw.catEntidadTipoClave = e.catEntidadTipoClave.ToString();
            aw.EntidadPadreClave = e.EntidadPadreClave.ToString();
            aw.Activo = Convert.ToInt32(e.Activo).ToString();
            return aw;
        }



        public ActionResult Almacenes()
        {

            ModeloAlmacen dc = new ModeloAlmacen();

            //usuarioalmacen_id

            Guid usuarioclave = Guid.Parse(Request["tecnicoalmacen_id"]);

            List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                              where a.EntidadClaveTecnico == usuarioclave
                                              select a).ToList();

            foreach (AmbitoAlmacen a in ambito)
            {
                dc.AmbitoAlmacen.Remove(a);
            }
            dc.SaveChanges();



            Guid distribuidorguid = Guid.Parse(Request["distribuidoralmacen_id"]);

            List<Entidad> rl = (from ro in dc.Entidad
                                    where ro.catDistribuidorClave == distribuidorguid
                                    select ro).ToList();

            List<AlmacenWrapper> rwl = new List<AlmacenWrapper>();

            foreach (Entidad r in rl)
            {

                if (Request["almacen" + r.EntidadClave] == "on")
                {
                    AmbitoAlmacen aa = new AmbitoAlmacen();
                    aa.AmbitoAlmacenClave = Guid.NewGuid();
                    aa.EntidadClaveTecnico = usuarioclave;
                    aa.EntidadClaveAlmacen = r.EntidadClave;

                    dc.AmbitoAlmacen.Add(aa);
                }
            }
            dc.SaveChanges();


            return Content("0");
        }

        [SessionExpireFilterAttribute]
        public ActionResult Inventario()
        {

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid al = Guid.Empty;

            try
            {
                al = Guid.Parse(Request["a"]);
            }
            catch (Exception ex)
            { }

            Entidad almacen = (from a in dc.Entidad
                                   where a.EntidadClave == al
                                   select a).SingleOrDefault();

            ViewData["almacen"] = almacen;

            return View();

        }




        private TecnicoWrapper TecnicoToWrapper(Entidad e)
        {
            TecnicoWrapper aw = new TecnicoWrapper();
            aw.Clave = e.EntidadClave.ToString();
            aw.Nombre = e.Nombre;
            aw.catEntidadTipoClave = e.catEntidadTipoClave.ToString();
            aw.EntidadPadreClave = e.EntidadPadreClave.ToString();

            try
            {
                PuestoTecnico pt = e.PuestoTecnico;
                aw.PuestoTecnicos = pt.catPuestoTecnico.Descripcion;
                aw.catPuestosTecnicosClave = pt.catPuestosTecnicosClave.ToString();
            }
            catch
            {
                aw.PuestoTecnicos = String.Empty;
                aw.catPuestosTecnicosClave = String.Empty;
            }

            aw.catDistribuidorClave = e.catDistribuidorClave.ToString();

            aw.Activo = Convert.ToInt32(e.Activo).ToString();

            return aw;
        }

    }
}
