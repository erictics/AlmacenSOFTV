﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class FallaController : Controller
    {
        //
        // GET: /Unidad/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catFallaWrapper> data { get; set; }
        }




        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ObtenListaFalla(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ObtenListaFalla(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<catFallaWrapper> ObtenListaFalla(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<catFalla> rls = (from use in dc.catFalla
                                             orderby use.Descripcion
                                             select use).ToList();

            List<catFallaWrapper> rw = new List<catFallaWrapper>();

            foreach (catFalla u in rls)
            {
                catFallaWrapper w = FallaToWrapper(u);
                rw.Add(w);
            }

            return rw;
        }


        public ActionResult Obten(string data)
        {
            catFallaWrapper rw = new catFallaWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catFalla r = (from ro in dc.catFalla
                              where ro.catFallaClave == uid
                                     select ro).SingleOrDefault();

            if (r != null)
            {
                rw = FallaToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catFalla r = (from ro in dc.catFalla
                                             where ro.catFallaClave == uid
                                             select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catFalla r = (from ro in dc.catFalla
                                      where ro.catFallaClave == uid
                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_activa)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catFalla r = new catFalla();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catFalla
                         where ro.catFallaClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Descripcion = ed_nombre;
                r.Activo = (ed_activa == "on") ? true : false;

                if (ed_id == String.Empty)
                {
                    dc.catFalla.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }






        private catFallaWrapper FallaToWrapper(catFalla u)
        {
            catFallaWrapper rw = new catFallaWrapper();
            rw.Clave = u.catFallaClave.ToString();
            rw.Descripcion = u.Descripcion;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }
    }
}
