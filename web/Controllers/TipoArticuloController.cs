﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class TipoArticuloController : Controller
    {
        //
        // GET: /TipoArticulo/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catClasificacionArticulo> clasificacion = (from c in dc.catClasificacionArticulo
                                                                where c.Activo
                                                                orderby c.Descripcion
                                                                select c).ToList();
            ViewData["clasificacion"] = clasificacion;


            List<catSerie> series = (from s in dc.catSerie
                                         where s.Activo
                                         orderby s.Nombre
                                         select s).ToList();
            ViewData["series"] = series;
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catTipoArticuloWrapper> data { get; set; }
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaTipos(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaTipos(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);

            
        }



        public List<catTipoArticuloWrapper> ListaTipos(string data)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);            

            int pag = Convert.ToInt32(jObject["pag"].ToString());
            int cla = Convert.ToInt32(jObject["cla"].ToString());           

            ModeloAlmacen dc = new ModeloAlmacen();
            List<catTipoArticulo> rls = (from use in dc.catTipoArticulo
                                             where (use.catClasificacionArticuloClave == cla || cla == -1)
                                             orderby use.Descripcion
                                             select use).ToList();
            List<catTipoArticuloWrapper> rw = new List<catTipoArticuloWrapper>();

            foreach (catTipoArticulo u in rls)
            {
                catTipoArticuloWrapper w = TipoArticuloToWrapper(u);
                rw.Add(w);
            }

            return rw;
        }



        public ActionResult Obten(string data)
        {
            catTipoArticuloWrapper rw = new catTipoArticuloWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catTipoArticulo r = (from ro in dc.catTipoArticulo
                                     where ro.catTipoArticuloClave == uid
                                     select ro).SingleOrDefault();

            if (r != null)
            {
                rw = TipoArticuloToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catTipoArticulo r = (from ro in dc.catTipoArticulo
                                             where ro.catTipoArticuloClave == uid
                                             select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catTipoArticulo r = (from ro in dc.catTipoArticulo
                                             where ro.catTipoArticuloClave == uid
                                             select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_clasificacion, string ed_activa, string ed_pagare, string ed_devolucionmatriz, string ed_letra)
        {

            try
            {
                



                ModeloAlmacen dc = new ModeloAlmacen();//manejador

                catTipoArticulo r = new catTipoArticulo();//entidad

                if (ed_id != String.Empty)//si no esta vacio 
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catTipoArticulo
                         where ro.catTipoArticuloClave == uid//se busca si ya esta en la base
                         select ro).SingleOrDefault();

                    List<Guid> lista = dc.Articulo.Where(p => p.catTipoArticuloClave == r.catTipoArticuloClave).Select(x=>x.ArticuloClave).ToList();
                    foreach (var a in lista)
                    {

                        List<Inventario> listainv = dc.Inventario.Where(x => x.ArticuloClave == a).ToList();
                        if (listainv.Count > 0)
                        {
                            return Content("2");
                        }

                    }


                    
                   
                    if (ed_activa != String.Empty && ed_activa != null)
                    {
                        r.Activo = true;
                    }
                    else
                    {
                        r.Activo = false;
                    }
                }
                else
                {
                    r.Activo = true;
                }

                if (ed_devolucionmatriz != String.Empty && ed_devolucionmatriz != null)
                {
                    r.DevMatriz = true;
                }
                else
                {
                    r.DevMatriz = false;
                }

                if (ed_pagare != String.Empty && ed_pagare != null)
                {
                    r.EsPagare = true;
                }
                else
                {
                    r.EsPagare = false;
                }


                r.catClasificacionArticuloClave = Convert.ToInt32(ed_clasificacion);
                r.Descripcion = ed_nombre;


                if (Request["serieinterface"] != null)
                {
                    r.catSerieClaveInterface = Convert.ToInt32(Request["serieinterface"]);
                }
                r.Letra = ed_letra;
                

                if (ed_id == String.Empty)
                {
                    dc.catTipoArticulo.Add(r);//agrega a bd
                }
                dc.SaveChanges();//cambia registro


                List<web.Models.SerieTipoArticulo> seriestiposarticulo = (from st in dc.SerieTipoArticulo
                                                                   where st.catTipoArticuloClave == r.catTipoArticuloClave
                                                                   select st).ToList();

                foreach (web.Models.SerieTipoArticulo sta in seriestiposarticulo)
                {
                    dc.SerieTipoArticulo.Remove(sta);
                }
                dc.SaveChanges();


                List<catSerie> series = (from st in dc.catSerie
                                             select st).ToList();

                foreach (catSerie s in series)
                {
                    if (Request["serie" + s.catSerieClave] == "on")
                    {
                        web.Models.SerieTipoArticulo sta = new web.Models.SerieTipoArticulo();
                        sta.catTipoArticuloClave = r.catTipoArticuloClave;
                        sta.catSerieClave = s.catSerieClave;
                        sta.Activo = true;
                        dc.SerieTipoArticulo.Add(sta);
                    }
                }

                dc.SaveChanges();




                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }
        }

        private catTipoArticuloWrapper TipoArticuloToWrapper(catTipoArticulo u)
        {
            catTipoArticuloWrapper rw = new catTipoArticuloWrapper();
            rw.Clave = u.catTipoArticuloClave.ToString();
            rw.Descripcion = u.Descripcion;

            List<web.Models.SerieTipoArticulo> series = u.SerieTipoArticulo.ToList();

            rw.Series = new List<string>();

            foreach (web.Models.SerieTipoArticulo s in series)
            {
                rw.Series.Add(s.catSerieClave.ToString());
            }

            rw.EsPagare = Convert.ToInt32(u.EsPagare).ToString();
            rw.DevMatriz = Convert.ToInt32(u.DevMatriz).ToString();

            if (u.Letra != null)
            {
                rw.Letra = u.Letra.ToString();
            }
            else
            {
                rw.Letra = String.Empty;
            }

            if (u.catSerieClaveInterface != null)
            {
                rw.SerieInterface = u.catSerieClaveInterface.ToString();
            }
            else
            {
                rw.SerieInterface = String.Empty;
            }
            rw.Activo = Convert.ToInt32(u.Activo).ToString();

            return rw;
        }
    }
}
