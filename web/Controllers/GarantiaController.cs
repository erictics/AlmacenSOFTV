﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

using Web.Controllers;

namespace web.Controllers
{
    public class GarantiaController : Controller
    {
        //
        // GET: /Garantia/

        ModeloAlmacen dc = new ModeloAlmacen();
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            Usuario u = ((Usuario)Session["u"]);
            //ist<Entidad> alm = dc.Entidad.Where(x => x.catDistribuidorClave == u.DistribuidorClave && x.catEntidadTipoClave == 1).ToList();
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();
            List<Articulo> articulos = dc.Articulo.Where(x => x.Activo == true).ToList();
            List<catTransportista> transp = dc.catTransportista.Where(x => x.Activo == true).ToList();
            List<catProveedor> proveedores = dc.catProveedor.Where(s => s.Activo).ToList();

            List<Entidad> almacenes = null;
            if (u.DistribuidorClave == null)
            {
                almacenes = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                     && ta.catDistribuidorClave == null
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {

                List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                              where a.UsuarioClave == u.UsuarioClave
                                              select a).ToList();
                almacenes = new List<Entidad>();


                foreach (AmbitoAlmacen a in ambito)
                {
                    Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        almacenes.Add(e);
                    }
                }
            }

            // ViewData["almacenes"] = alm;
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tipo_articulos"] = tiposarticulo;
            ViewData["transportistas"] = transp;
            ViewData["proveedores"] = proveedores;
            ViewData["almacenes"] = almacenes;
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Aparatos> data { get; set; }
        }

        public class Devolucion
        {
            public long pedido { get; set; }
            public DateTime fecha { get; set; }
            public String fechacorta { get; set; }
            public string almacen { get; set; }
            public int estatus { get; set; }
            public String txtestatus { get; set; }
            public Guid proceso { get; set; }
            public string proveedor { get; set; }
            public int tiene_envio { get; set; }
            public Guid recepcion_proveedor { get; set; }

        }

        public class lista_dev
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Devolucion> data { get; set; }
        }


        public ActionResult ListaDevoluciones(string data, string almacen, int orden, int draw, int start, int length)
        {
            lista_dev dataTableData = new lista_dev();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            Guid proveedor;
            try
            {
                proveedor = Guid.Parse(almacen);
            }
            catch
            {
                proveedor = Guid.Empty;
            }

            dataTableData.data = Obtenerdevoluciones(ref recordsFiltered, proveedor, orden, start, length).OrderByDescending(x => x.pedido).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Devolucion> Obtenerdevoluciones(ref int recordFiltered, Guid proveedor, int orden, int start, int length)
        {

            List<Devolucion> lista = (from a in dc.Proceso
                                      where a.catTipoProcesoClave == 9 &&
                                     (a.catProveedorClave == proveedor || proveedor == Guid.Empty) &&
                                     (a.NumeroPedido == orden || orden == 0)
                                      select new Devolucion
                                      {
                                          almacen = a.Entidad.Nombre,
                                          txtestatus = a.catEstatus.Nombre,
                                          fecha = a.Fecha.Value,
                                          estatus = a.catEstatusClave.Value,
                                          pedido = a.NumeroPedido,
                                          proceso = a.ProcesoClave,
                                          proveedor = a.catProveedor.Nombre
                                      }).ToList();
            recordFiltered = lista.Count;
            lista.Skip(start).Take(length).ToList().ForEach(u =>
            {

                u.fechacorta = u.fecha.ToShortDateString();
                if (u.estatus == 4)
                {
                    try
                    {
                        u.recepcion_proveedor = dc.Proceso.Where(i => i.ProcesoClaveRespuesta == u.proceso)
                        .Select(i => i.ProcesoClave).First();
                        int env = 0;
                        try
                        {
                            List<ProcesoEnvio> pe = dc.ProcesoEnvio.Where(x => x.ProcesoClave == u.proceso).ToList();
                            if (pe.Count > 0)
                            {
                                env = 1;
                            }
                        }
                        catch { }

                        u.tiene_envio = env;
                    }
                    catch { }


                }
            });
            return lista;
        }


        public ActionResult GuardaDevolucion(Guid almacen, string motivo, string articulos, Guid proveedor, int status)
        {

            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Entidad alm = dc.Entidad.Where(o => o.EntidadClave == almacen).First();
                    catProveedor prov = dc.catProveedor.Where(un => un.catProveedorClave == proveedor).First();

                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    Usuario u = (Usuario)Session["u"];
                    Proceso p = new Proceso();

                    try
                    {
                        p.ProcesoClave = Guid.NewGuid();
                        p.catEstatusClave = 1;
                        p.catTipoProcesoClave = 9;
                        p.EntidadClaveSolicitante = alm.EntidadClave;
                        p.EntidadClaveAfectada = null;
                        p.UsuarioClave = u.UsuarioClave;
                        p.Fecha = DateTime.Now;
                        p.Observaciones = motivo;
                        p.catProveedorClave = prov.catProveedorClave;
                        p.BitacoraNumero = status.ToString();
                        dc.Proceso.Add(p);
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-3");
                    }



                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g
                                 select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach (var a in result)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = p.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                    }

                    foreach (var Articulo in series)
                    {

                        Inventario Inv = new Inventario();
                        try
                        {
                            Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave
                            && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == almacen
                            && o.EstatusInv == status).First();
                        }
                        catch { }
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            if (Inv.Standby == 1 && Inv.Cantidad.Value == 0)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-1");
                            }
                            else if (Inv.EntidadClave != alm.EntidadClave)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-1");
                            }
                            else
                            {
                                Inv.Standby = 1;
                                Inv.Cantidad = 0;
                                Inv.Estatus = 3;
                                Inv.EstatusInv = 13;
                            }


                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-2");
                            }
                            else
                            {

                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;
                                try
                                {
                                    Inventario inv_accesorios = dc.Inventario
                                        .Where(x => x.EntidadClave == alm.EntidadClave
                                        && x.ArticuloClave == Articulo.ArticuloClave
                                        && x.EstatusInv == 13).First();
                                    inv_accesorios.Cantidad = inv_accesorios.Cantidad + Articulo.Cantidad;


                                }
                                catch
                                {
                                    Inventario i = new Inventario();
                                    i.InventarioClave = Guid.NewGuid();
                                    i.ArticuloClave = Articulo.ArticuloClave;
                                    i.EntidadClave = almacen;
                                    i.Cantidad = Articulo.Cantidad;
                                    i.Standby = 0;
                                    i.Estatus = 1;
                                    i.EstatusInv = 13;
                                    dc.Inventario.Add(i);

                                }

                            }


                        }
                        //dc.SaveChanges();
                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }


        }





        public ActionResult Recepcion(string devolucion, string articulos)
        {
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    Usuario u = (Usuario)Session["u"];
                    Guid dev = Guid.Parse(devolucion);
                    Proceso p = dc.Proceso.Where(o => o.ProcesoClave == dev && o.catTipoProcesoClave == 9
                             && (o.catEstatusClave != 4 || o.catEstatusClave != 6)).First();
                    Proceso recepcion = new Proceso();
                    try
                    {

                        recepcion.ProcesoClave = Guid.NewGuid();
                        recepcion.catEstatusClave = 4;
                        recepcion.catTipoProcesoClave = 18;
                        recepcion.EntidadClaveAfectada = p.EntidadClaveSolicitante;
                        recepcion.UsuarioClave = u.UsuarioClave;
                        recepcion.Fecha = DateTime.Now;
                        recepcion.ProcesoClaveRespuesta = p.ProcesoClave;
                        recepcion.catProveedorClave = p.catProveedorClave.Value;
                        //p.Observaciones = ;
                        // p.catDistribuidorClave = distribuidor.catDistribuidorClave;
                        dc.Proceso.Add(recepcion);
                        dc.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        //(1)
                        dbContextTransaction.Rollback();
                        return Content("-1");
                    }

                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                    into g
                                 select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach (var a in result)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = recepcion.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                        dc.SaveChanges();
                    }

                    foreach (var Articulo in series)
                    {

                        Inventario Inv = new Inventario();
                        try
                        {
                            Inv = dc.Inventario.Where(o =>
                             o.ArticuloClave == Articulo.ArticuloClave
                            && o.EntidadClave == p.EntidadClaveSolicitante && o.EstatusInv == 13).First();
                        }
                        catch
                        {
                            dbContextTransaction.Rollback();
                            return Content("-2");

                        }
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = recepcion.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);
                        dc.SaveChanges();

                        if (tieneSeries(Articulo.ArticuloClave))
                        {

                            if (Articulo.Reemplazada == true)
                            {

                                if (dc.Serie.Any(o => o.Valor.ToUpper() == Articulo.Reemplazo.ToUpper()) == true)
                                {
                                    dbContextTransaction.Rollback();
                                    return Content("-3");

                                }

                                Inventario i = new Inventario();
                                i.InventarioClave = Guid.NewGuid();
                                i.ArticuloClave = Articulo.ArticuloClave;
                                i.EntidadClave = p.EntidadClaveSolicitante;
                                i.Cantidad = Articulo.Cantidad;
                                i.Standby = 0;
                                i.Estatus = 1;
                                i.EstatusInv = 9;
                                dc.Inventario.Add(i);
                                dc.SaveChanges();

                                ProcesoInventario pi2 = new ProcesoInventario();
                                pi2.ProcesoClave = recepcion.ProcesoClave;
                                pi2.InventarioClave = i.InventarioClave;
                                pi2.Cantidad = Articulo.Cantidad;
                                pi2.Fecha = DateTime.Now;
                                dc.ProcesoInventario.Add(pi2);
                                dc.SaveChanges();

                                Serie s = new Serie();
                                s.catSerieClave = 2;
                                s.Valor = Articulo.Reemplazo;
                                s.InventarioClave = i.InventarioClave;
                                dc.Serie.Add(s);
                                dc.SaveChanges();

                                Inv.Standby = 0;
                                Inv.Cantidad = 1;
                                Inv.Estatus = 1;
                                Inv.EstatusInv = 14;
                                dc.SaveChanges();
                            }
                            else
                            {
                                Inv.Standby = 0;
                                Inv.Cantidad = 1;
                                Inv.Estatus = 1;
                                Inv.EstatusInv = 9;
                                dc.SaveChanges();
                            }
                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-4");
                            }
                            else
                            {
                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;
                                try
                                {
                                    Inventario inv_accesorios = dc.Inventario.Where(x =>
                                  x.ArticuloClave == Articulo.ArticuloClave
                                  && x.EntidadClave == p.EntidadClaveSolicitante
                                  && x.EstatusInv == 9
                                ).First();

                                    inv_accesorios.Cantidad = inv_accesorios.Cantidad + Articulo.Cantidad;
                                    dc.SaveChanges();
                                }
                                catch
                                {

                                    Inventario i = new Inventario();
                                    i.InventarioClave = Guid.NewGuid();
                                    i.ArticuloClave = Articulo.ArticuloClave;
                                    i.EntidadClave = p.EntidadClaveSolicitante;
                                    i.Cantidad = Articulo.Cantidad;
                                    i.Standby = 0;
                                    i.Estatus = 1;
                                    i.EstatusInv = 9;
                                    dc.Inventario.Add(i);
                                    dc.SaveChanges();
                                }
                            }
                        }
                    }

                    p.catEstatusClave = 4;
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(recepcion.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Content("-5");
                }

            }

        }









        public ActionResult CancelaDevolucion(Guid devolucion)
        {
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {

                    Proceso p = dc.Proceso.Where(x => x.ProcesoClave == devolucion && x.catTipoProcesoClave == 9 && (x.catEstatusClave != 6 || x.catEstatusClave != 4)).First();
                    Usuario u = (Usuario)Session["u"];
                    p.catEstatusClave = 6;
                    p.Observaciones = "cancelado por " + u.Nombre + ",Fecha: " + DateTime.Now;
                    int statusant = Int32.Parse(p.BitacoraNumero);
                    List<ProcesoInventario> pro = dc.ProcesoInventario.Where(un => un.ProcesoClave == p.ProcesoClave).ToList();
                    pro.ForEach(c =>
                    {
                        if (tieneSeries(c.Inventario.Articulo.ArticuloClave) == true)
                        {
                            c.Inventario.EstatusInv = statusant;
                        }
                        else
                        {
                            Inventario Invaccesorios = dc.Inventario.Where(x => x.ArticuloClave == c.Inventario.ArticuloClave
                            && x.EntidadClave == p.EntidadClaveSolicitante && x.EstatusInv == statusant).First();

                            Inventario Invaccesorios_reparacion = dc.Inventario.Where(x => x.ArticuloClave == c.Inventario.ArticuloClave
                            && x.EntidadClave == p.EntidadClaveSolicitante && x.EstatusInv == 13).First();


                            Invaccesorios.Cantidad = Invaccesorios.Cantidad.Value + c.Cantidad.Value;
                            Invaccesorios_reparacion.Cantidad = Invaccesorios_reparacion.Cantidad.Value - c.Cantidad.Value;
                            dc.SaveChanges();
                        }
                    });

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave, JsonRequestBehavior.AllowGet);

                }
                catch
                {

                    dbContextTransaction.Dispose();
                    return Content("-1");
                }
            }
        }





        public bool tieneSeries(Guid artclave)
        {

            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }







    }
}
