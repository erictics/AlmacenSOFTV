﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;
using Wrappers;

namespace Web.Controllers
{
    public class UbicacionController : Controller
    {
        //
        // GET: /Ubicacion/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {

            ViewData["pantalla"] = "ubicaciones";
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            Guid al = Guid.Empty;
            
            try
            {
                al = Guid.Parse(Request["a"]);
            }
            catch(Exception ex)
            {}

            Entidad almacen = (from a in dc.Entidad
                                   where a.EntidadClave == al 
                                   select a).SingleOrDefault();

            ViewData["almacen"] = almacen;

            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();


            Guid almacen = Guid.Empty;
            try
            {
                almacen = Guid.Parse(Request["a"]);
            }
            catch { }

            int pags = (from ro in dc.catUbicacion
                        where ro.EntidadClave == almacen
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div).ToString());
            }
            else
            {
                return Content((div+1).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid almacen = Guid.Empty;
            try
            {
                almacen = Guid.Parse(Request["a"]);
            }
            catch { }

            List<catUbicacion> rls = (from use in dc.catUbicacion
                                          where use.EntidadClave == almacen
                                           orderby use.Nombre
                                           select use).Skip(skip).Take(elementos).ToList();

            List<catUbicacionWrapper> rw = new List<catUbicacionWrapper>();

            foreach (catUbicacion u in rls)
            {
                catUbicacionWrapper w = UbicacionToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Obten(string data)
        {
            catUbicacionWrapper rw = new catUbicacionWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catUbicacion r = (from ro in dc.catUbicacion
                                   where ro.catUbicacionClave == uid
                                   select ro).SingleOrDefault();

            if (r != null)
            {
                rw = UbicacionToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catUbicacion r = (from ro in dc.catUbicacion
                                           where ro.catUbicacionClave == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catUbicacion r = (from ro in dc.catUbicacion
                                          where ro.catUbicacionClave == uid
                                          select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_almacen, string ed_activa)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catUbicacion r = new catUbicacion();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catUbicacion
                         where ro.catUbicacionClave == uid
                         select ro).SingleOrDefault();

                    if (ed_activa != String.Empty && ed_activa != null)
                    {
                        r.Activo = true;
                    }
                    else
                    {
                        r.Activo = false;
                    }

                }
                else
                {
                    r.EntidadClave = Guid.Parse(ed_almacen);
                    r.Activo = true;
                }

                
                r.Nombre = ed_nombre;                

                if (ed_id == String.Empty)
                {
                    dc.catUbicacion.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        private catUbicacionWrapper UbicacionToWrapper(catUbicacion u)
        {
            catUbicacionWrapper rw = new catUbicacionWrapper();
            rw.Clave = u.catUbicacionClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }
    }
}
