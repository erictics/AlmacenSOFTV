﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class PuestoTecnicoController : Controller
    {
        //
        // GET: /Unidad/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catClasificacionArticulo> clasificacion = (from c in dc.catClasificacionArticulo
                                                                where c.Activo
                                                                orderby c.Descripcion
                                                                select c).ToList();
            ViewData["clasificacion"] = clasificacion;
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;



        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catPuestoTecnicoWrapper> data { get; set; }
        }

        public List<catPuestoTecnicoWrapper> ObtenPuestos(string data)
        {            

            ModeloAlmacen dc = new ModeloAlmacen();
            List<catPuestoTecnico> rls = (from use in dc.catPuestoTecnico
                                              orderby use.Descripcion
                                              select use).ToList();

            List<catPuestoTecnicoWrapper> rw = new List<catPuestoTecnicoWrapper>();
            foreach (catPuestoTecnico u in rls)
            {
                catPuestoTecnicoWrapper w = PuestoTecnicoToWrapper(u);
                rw.Add(w);
            }
            return rw;

        }



        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ObtenPuestos(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ObtenPuestos(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);            
        }


        public ActionResult Obten(string data)
        {
            catPuestoTecnicoWrapper rw = new catPuestoTecnicoWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catPuestoTecnico r = (from ro in dc.catPuestoTecnico
                                      where ro.catPuestosTecnicosClave == uid
                                   select ro).SingleOrDefault();

            if (r != null)
            {
                rw = PuestoTecnicoToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catPuestoTecnico r = (from ro in dc.catPuestoTecnico
                                              where ro.catPuestosTecnicosClave == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catPuestoTecnico r = (from ro in dc.catPuestoTecnico
                                              where ro.catPuestosTecnicosClave == uid
                                              select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catPuestoTecnico r = new catPuestoTecnico();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catPuestoTecnico
                         where ro.catPuestosTecnicosClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Descripcion = ed_nombre;
                r.Activo = (Request["ed_activa"] == "on") ? true : false;

                if (ed_id == String.Empty)
                {
                    dc.catPuestoTecnico.Add(r);
                }
                dc.SaveChanges();


                // Se guarda la relación con Clasificación Artículo
                List<PuestoTecnicoClasificacionArticulo> seriestiposarticulo = (from st in dc.PuestoTecnicoClasificacionArticulo
                                                                   where st.catPuestosTecnicosClave == r.catPuestosTecnicosClave
                                                                   select st).ToList();

                foreach (PuestoTecnicoClasificacionArticulo sta in seriestiposarticulo)
                {
                    dc.PuestoTecnicoClasificacionArticulo.Remove(sta);
                }
                dc.SaveChanges();


                List<catClasificacionArticulo> clasificaciones = (from st in dc.catClasificacionArticulo
                                             select st).ToList();

                foreach (catClasificacionArticulo s in clasificaciones)
                {
                    if (Request["cla" + s.catClasificacionArticuloClave] == "on")
                    {
                        PuestoTecnicoClasificacionArticulo sta = new PuestoTecnicoClasificacionArticulo();
                        sta.catPuestosTecnicosClave = r.catPuestosTecnicosClave;
                        sta.catClasificacionArticuloClave = s.catClasificacionArticuloClave;
                        sta.Activo = true;
                        dc.PuestoTecnicoClasificacionArticulo.Add(sta);
                    }
                }

                dc.SaveChanges();



                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }
        
        private catPuestoTecnicoWrapper PuestoTecnicoToWrapper(catPuestoTecnico u)
        {
            catPuestoTecnicoWrapper rw = new catPuestoTecnicoWrapper();
            rw.Clave = u.catPuestosTecnicosClave.ToString();
            rw.Descripcion = u.Descripcion;

            List<PuestoTecnicoClasificacionArticulo> clasif = u.PuestoTecnicoClasificacionArticulo.ToList();

            rw.ClasificacionesArticulo = new List<string>();

            foreach (PuestoTecnicoClasificacionArticulo cla in clasif)
            {
                rw.ClasificacionesArticulo.Add(cla.catClasificacionArticuloClave.ToString());
            }

            rw.Activo = Convert.ToInt32(u.Activo).ToString();

            return rw;
        }
    }
}
