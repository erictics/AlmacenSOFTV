﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class TransportistaController : Controller
    {
        //
        // GET: /Transportista/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catTransportistaWrapper> data { get; set; }
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = listaSalidas(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = listaSalidas(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public List<catTransportistaWrapper> listaSalidas(string data)
        {
             ModeloAlmacen dc = new ModeloAlmacen();

            List<catTransportista> rls = (from use in dc.catTransportista
                                              orderby use.Descripcion
                                              select use).ToList();

            List<catTransportistaWrapper> rw = new List<catTransportistaWrapper>();

            foreach (catTransportista u in rls)
            {
                catTransportistaWrapper w = TransportistaToWrapper(u);
                rw.Add(w);
            }

            return rw;
        }

        public ActionResult Obten(string data)
        {
            catTransportistaWrapper rw = new catTransportistaWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catTransportista r = (from ro in dc.catTransportista
                                   where ro.catTranspClave == uid
                                              select ro).SingleOrDefault();

            if (r != null)
            {
                rw = TransportistaToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catTransportista r = (from ro in dc.catTransportista
                                           where ro.catTranspClave == uid
                                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catTransportista r = (from ro in dc.catTransportista
                                              where ro.catTranspClave == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre,  string ed_url, string ed_activa)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catTransportista r = new catTransportista();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catTransportista
                         where ro.catTranspClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Descripcion = ed_nombre;
                r.URLSeguimiento = ed_url;
                r.Activo = (ed_activa == "on") ? true : false;

                if (ed_id == String.Empty)
                {
                    dc.catTransportista.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        private catTransportistaWrapper TransportistaToWrapper(catTransportista u)
        {
            catTransportistaWrapper rw = new catTransportistaWrapper();
            rw.Clave = u.catTranspClave.ToString();
            rw.Descripcion = u.Descripcion;
            rw.URLSeguimiento = u.URLSeguimiento;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }

    }
}
