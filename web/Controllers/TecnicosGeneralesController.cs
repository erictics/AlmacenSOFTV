﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace web.Controllers
{
    public class TecnicosGeneralesController : Controller
    {
        //
        // GET: /TecnicosGenerales/

        ModeloAlmacen dc = new ModeloAlmacen();



        public ActionResult Index()
        {

            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);          
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            ViewData["distribuidor"] = distribuidor;
            List<catPuestoTecnico> puestostecnico = (from c in dc.catPuestoTecnico
                                                     where c.Activo
                                                     orderby c.Descripcion
                                                     select c).ToList();
            ViewData["puestostecnico"] = puestostecnico;



            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaTecnicos(data).OrderByDescending(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaTecnicos(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);


        }


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<TecnicoWrapper> data { get; set; }
        }


        public List<TecnicoWrapper> ListaTecnicos(string data)
        {

            
            ModeloAlmacen dc = new ModeloAlmacen();

            List<Entidad> rls = (from use in dc.Entidad
                                 where use.catEntidadTipoClave == 2
                                    && use.catDistribuidorClave ==  null
                                 orderby use.Nombre
                                 select use).ToList();

            List<TecnicoWrapper> rw = new List<TecnicoWrapper>();

            foreach (Entidad u in rls)
            {
                TecnicoWrapper w = TecnicoToWrapper(u);
                rw.Add(w);
            }

            return rw;
        }

        private TecnicoWrapper TecnicoToWrapper(Entidad e)
        {
            TecnicoWrapper aw = new TecnicoWrapper();
            aw.Clave = e.EntidadClave.ToString();
            aw.Nombre = e.Nombre;
            aw.catEntidadTipoClave = e.catEntidadTipoClave.ToString();
            //aw.EntidadPadreClave = e.EntidadPadreClave.ToString();

            try
            {
                PuestoTecnico pt = e.PuestoTecnico;
                aw.PuestoTecnicos = pt.catPuestoTecnico.Descripcion;
                aw.catPuestosTecnicosClave = pt.catPuestosTecnicosClave.ToString();
            }
            catch
            {
                aw.PuestoTecnicos = String.Empty;
                aw.catPuestosTecnicosClave = String.Empty;
            }

           // aw.catDistribuidorClave = e.catDistribuidorClave.ToString();

            aw.Activo = Convert.ToInt32(e.Activo).ToString();

            return aw;
        }

        public ActionResult detalleTecnico(Guid id)
        {            
        Entidad e = dc.Entidad.Where(u => u.EntidadClave == id).First();
        TecnicoWrapper tec = TecnicoToWrapper(e);
        return Json(tec, JsonRequestBehavior.AllowGet);           
        }

        public class distribuidoresTecnicoWrapper
        {
            public long idDistribuidor { get; set; }
            public string nombre { get; set; }
        }


        public ActionResult obtieneDistribuidoresTecnico(Guid id)
        {
            List<distribuidoresTecnicoWrapper> lista_dis = new List<distribuidoresTecnicoWrapper>();
            Entidad e = dc.Entidad.Where(u => u.EntidadClave == id).First();
            List<RelTecnicoDistribuidor> lista = dc.RelTecnicoDistribuidor.Where(x=>x.EntidadClave==e.EntidadClave).ToList();
            lista.ForEach(y => {
                distribuidoresTecnicoWrapper dw = dc.catDistribuidor.Where(x=>x.catDistribuidorClave==y.DistribuidorClave)
                .Select(x=>new distribuidoresTecnicoWrapper { idDistribuidor=x.IdDistribuidor, nombre=x.Nombre } ).First();
                lista_dis.Add(dw);
            });

            return Json(lista_dis, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditarTecnico(Guid idEntidad,string nombre, bool activo, int puesto, string distribuidores)
        {
            try
            {
                Entidad e = dc.Entidad.Where(x => x.EntidadClave == idEntidad).First();
                e.Activo = activo;
                e.catDistribuidorClave = null;
                e.catEntidadTipoClave = 2;
                e.Nombre = nombre;               
              //  PuestoTecnico pt = dc.PuestoTecnico.Where(x => x.catPuestosTecnicosClave == puesto).First();
                //e.PuestoTecnico = pt;              
                dc.SaveChanges();


                PuestoTecnico pu = (from p in dc.PuestoTecnico
                                    where p.EntidadClave == e.EntidadClave
                                    select p).SingleOrDefault();                

                pu.EntidadClave = e.EntidadClave;
                pu.catPuestosTecnicosClave = Convert.ToInt32(puesto);
                pu.Activo = true;
                dc.SaveChanges();


                List<RelTecnicoDistribuidor> relaciones = dc.RelTecnicoDistribuidor.Where(x => x.EntidadClave == e.EntidadClave).ToList();
                dc.RelTecnicoDistribuidor.RemoveRange(relaciones);
                dc.SaveChanges();

                List<string> _arrdist = distribuidores.Split('|').ToList();
                _arrdist.ForEach(x => {
                    if (x != "")
                    {
                        int id = Int32.Parse(x.ToString());
                        RelTecnicoDistribuidor rt = new RelTecnicoDistribuidor();
                        rt.EntidadClave = e.EntidadClave;
                        Guid guidDis = dc.catDistribuidor.Where(u => u.IdDistribuidor == id).Select(u => u.catDistribuidorClave).First();
                        rt.DistribuidorClave = guidDis;
                        dc.RelTecnicoDistribuidor.Add(rt);
                    }

                });
                dc.SaveChanges();


                return Json("-1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GuardaTecnico(string nombre,bool activo,int puesto,string distribuidores)
        {
            try
            {
                Entidad e = new Entidad();
                e.Activo = activo;
                e.catDistribuidorClave = null;
                e.catEntidadTipoClave = 2;
                e.Nombre = nombre;
                e.EntidadClave = Guid.NewGuid();
                //PuestoTecnico pt = dc.PuestoTecnico.Where(x=>x.catPuestosTecnicosClave==puesto).First();
                //e.PuestoTecnico = pt;

                PuestoTecnico pu = (from p in dc.PuestoTecnico
                                    where p.EntidadClave == e.EntidadClave
                                    select p).SingleOrDefault();

                bool nuevo = false;
                if (pu == null)
                {
                    pu = new PuestoTecnico();
                    nuevo = true;
                }

                pu.EntidadClave = e.EntidadClave;
                pu.catPuestosTecnicosClave = Convert.ToInt32(puesto);
                pu.Activo = true;

                if (nuevo)
                {
                    dc.PuestoTecnico.Add(pu);
                }

                dc.Entidad.Add(e);
                dc.SaveChanges();

                List<string> _arrdist = distribuidores.Split('|').ToList();

                _arrdist.ForEach(x=> {
                    if(x != "")
                    {
                       int id= Int32.Parse(x.ToString());
                        RelTecnicoDistribuidor rt = new RelTecnicoDistribuidor();
                        rt.EntidadClave = e.EntidadClave;
                        Guid guidDis = dc.catDistribuidor.Where(u => u.IdDistribuidor == id).Select(u => u.catDistribuidorClave).First();
                        rt.DistribuidorClave = guidDis;
                        dc.RelTecnicoDistribuidor.Add(rt);
                    }
                  
                });
                dc.SaveChanges();
                
                
                return Json("-1",JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

    }
}
