﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class SaldoArticuloController : Controller
    {

        [SessionExpireFilterAttribute]
        public ActionResult SaldoArticulo()
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();


            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;

            Guid uguid = Guid.Parse(Request["u"]);

            Entidad e = (from en in dc.Entidad
                             where en.EntidadClave == uguid
                             select en).SingleOrDefault();

            ViewData["e"] = e;

            return View();
        }

        public void getexcel(string clasi, string tipo, string almace)
        {

            if (clasi == null || clasi=="-1" ||clasi == null || clasi == "-1")
            {
                ViewBag.error = "por favor selecciona ";
               
            }
            else { 
            DataTable dt = new DataTable();
            List<InventarioTecnicoWrapper> lis = new List<InventarioTecnicoWrapper>();
           lis= listasaldo( clasi,  tipo,  almace);
           ModeloAlmacen dc = new ModeloAlmacen();
            dt = ConvertToDatatable(lis);
            Guid alm = Guid.Parse(almace);
            List<Entidad> almacen = (from p in dc.Entidad
                                   where p.EntidadClave == alm
                                   select p).ToList();
            int t =int.Parse(tipo);
            string tip = (from a in dc.catTipoArticulo where a.catTipoArticuloClave==t select a.Descripcion).First().ToString();
            foreach (Entidad a in almacen)
            {
               
                DumpExcel(dt, a.Nombre.ToString(), tip);
            }
            }
           
         

        }



        private void DumpExcel(DataTable tbl,string alm,string tipo)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("saldo de articulos");

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A4"].LoadFromDataTable(tbl, true);

                //Format the header for column 1-3



                using (ExcelRange rng = ws.Cells["A4:D4"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);


                }
                //estilo a datos de plantilla
                using (ExcelRange rng = ws.Cells["A1:D3"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);


                }
                using (ExcelRange rng = ws.Cells["A1:D1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.Red);  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);


                }



                ws.Column(1).Width = 30;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 30;
                ws.Column(4).Width = 30;
               


                
                ws.Cells["A1"].Value = "ALMACEN STARTV";
                
                ws.Cells["A3"].Value = "ALMACEN";
                ws.Cells["B3"].Value = alm;
                ws.Cells["C3"].Value = "TIPO DE ARTICULO ";
                ws.Cells["D3"].Value = tipo;
                ws.Cells["B1"].Value = " SALDO DE ARTICULOS";




                //Example how to Format Column 1 as numeric 
                using (ExcelRange col = ws.Cells[5, 1, 5 + tbl.Rows.Count, 1])
                {
                    //col.Style.Numberformat.Format = "#,##0.00";
                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                }




                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=saldoarticulos.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }


        static DataTable ConvertToDatatable(List<InventarioTecnicoWrapper> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("CLASIFICACION");
            dt.Columns.Add("TIPO");
            dt.Columns.Add("ARTICULO");            
            dt.Columns.Add("CANTIDAD");
          
            foreach (var item in list)
            {
                var row = dt.NewRow();

                row["CLASIFICACION"] = item.CATEGORIA.ToString();
                row["TIPO"] = item.TIPO;
                row["ARTICULO"] = item.ARTICULO;
                row["CANTIDAD"] = item.CANTIDAD;
               

                dt.Rows.Add(row);
            }

            return dt;
        }





        public List<InventarioTecnicoWrapper> listasaldo(string clasi, string tipo, string almace)
        {
            List<InventarioTecnicoWrapper> Saldo = new List<InventarioTecnicoWrapper>();

           

            ModeloAlmacen dc = new ModeloAlmacen();


            Guid clave = Guid.Parse(almace);
            int cla = -1;
            if (clasi != "")
            {
                cla = Convert.ToInt32(clasi);
            }

            int tip = -1;
            if (tipo != "")
            {
                tip = Convert.ToInt32(tipo);
            }


            Entidad almacen = (from p in dc.Entidad
                                   where p.EntidadClave == clave
                                   select p).SingleOrDefault();




            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            List<catClasificacionArticulo> clas = (from c in dc.catClasificacionArticulo
                                                       where (cla == -1 || c.catClasificacionArticuloClave == cla)
                                                       orderby c.Descripcion
                                                       select c).ToList();

            foreach (catClasificacionArticulo cl in clas)
            {
               
                
                List<catTipoArticulo> tipos = (from tt in cl.catTipoArticulo
                                                   where tip == -1 || tt.catTipoArticuloClave == tip
                                                   orderby tt.Descripcion
                                                   select tt).ToList();

                foreach (catTipoArticulo tp in tipos)
                {

                   

                    List<Articulo> art = (from a in tp.Articulo
                                              orderby a.Nombre
                                              select a).ToList();

                    foreach (Articulo a in art)
                    {
                        InventarioTecnicoWrapper saldoobj = new InventarioTecnicoWrapper();
                        saldoobj.CATEGORIA = cl.Descripcion;
                        saldoobj.TIPO = tp.Descripcion;
                        saldoobj.ARTICULO = a.Nombre;

                        int invs = (from i in dc.Inventario
                                    where i.ArticuloClave == a.ArticuloClave
                                    && i.EntidadClave == clave
                                    && i.Estatus == 1
                                    && i.EstatusInv == 7
                                    select i).ToList().Sum(o => o.Cantidad.Value);

                        saldoobj.CANTIDAD = invs.ToString();
                        Saldo.Add(saldoobj);
                    }
                }



            }
            return Saldo;

        }

        [SessionExpireFilterAttribute]
        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            if (Request["almacen"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["almacen"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            int cla = -1;
            if (Request["cla"] != "")
            {
                cla = Convert.ToInt32(Request["cla"]);
            }

            int tip = -1;
            if (Request["tip"] != "")
            {
                tip = Convert.ToInt32(Request["tip"]);
            }


            Entidad almacen = (from p in dc.Entidad
                                   where p.EntidadClave == clave
                                   select p).SingleOrDefault();


            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append("<font size=2>");


            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");

            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Saldo Artículos " + almacen.Nombre + "</h3><br>");

            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");



            sb.Append("<br><br>");



            sb.Append("<b>Artículos</b><br><br>");
            //sb.Append(@"<table cellpadding=""1""  cellspacing=""0"" bgcolor=""#EFEFEF""><tr></td>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"" class=""table table-hover-color"">");

            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" colspan=""3"">");
            sb.Append("Descripción");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Existencias");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Unidad");
            sb.Append("</th>");

            sb.Append("</thead>");

            List<catClasificacionArticulo> clas = (from c in dc.catClasificacionArticulo
                                                       where (cla == -1 || c.catClasificacionArticuloClave == cla)
                                                       orderby c.Descripcion
                                                       select c).ToList();



            sb.Append("<tbody>");

            foreach (catClasificacionArticulo cl in clas)
            {
                sb.Append("<tr>");
                sb.Append(@"<td align=""left"" colspan=""6""><b>");
                sb.Append(cl.Descripcion);
                sb.Append("</b></td>");
                sb.Append("</tr>");



                List<catTipoArticulo> tipos = (from tt in cl.catTipoArticulo
                                                       where tip ==-1 || tt.catTipoArticuloClave == tip
                                                       orderby tt.Descripcion
                                                       select tt).ToList();



                foreach (catTipoArticulo tp in tipos)
                {

                    sb.Append("<tr>");
                    sb.Append(@"<td align=""left"" colspan=""6""><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    sb.Append(tp.Descripcion);
                    sb.Append("</b></td>");
                    sb.Append("</tr>");


                    List<Articulo> art = (from a in tp.Articulo
                                        orderby a.Nombre
                                        select a).ToList();

                    foreach (Articulo a in art)
                    {

                        int invs = (from i in dc.Inventario
                                    where i.ArticuloClave == a.ArticuloClave
                                    && i.EntidadClave == clave
                                    && i.Estatus == 1
                                    && i.EstatusInv == 7
                                    select i).ToList().Sum(o => o.Cantidad.Value);

                        //if (invs < a.TopeMinimo.Value)
                        //{

                            sb.Append("<tr>");
                            sb.Append(@"<td colspan=""3"">");
                            sb.Append(a.Nombre);
                            sb.Append("</td>");
                            sb.Append(@"<td align=""right"">");
                            sb.Append(invs);
                            sb.Append("</td>");
                            sb.Append(@"<td align=""left"">");
                            sb.Append(a.catUnidad.Nombre);
                            sb.Append("</td>");


                            sb.Append("</tr>");
                        //}
                    }
                }


            }
            //sb.Append("</table>");
            //sb.Append("</td></tr></table>");
            sb.Append("</tbody>");


            if (pdf == 1 || pdf == 2)
            {
                sb.Append("<br>");
            }



            sb.Append("</table>");





            sb.Append("</font>");


            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "saldoarticulo.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);


                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "SaldoArticulo" + clave.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "saldoarticulo.pdf" + "' width=100% height=380 frameborder=0></iframe>";
                    return View();
                }
            }



        }


    }
}
