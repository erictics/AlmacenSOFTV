﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace web.Controllers
{
    public class Connection
    {

        public string GetConnectionString()
        {
            string AlmacenUser = WebConfigurationManager.AppSettings["AlmacenUser"].ToString();
            string AlmacenSource = WebConfigurationManager.AppSettings["AlmacenSource"].ToString();
            string AlmacenBase = WebConfigurationManager.AppSettings["AlmacenBase"].ToString();
            string AlmacenPassword = WebConfigurationManager.AppSettings["AlmacenPassword"].ToString();
            string con = "Data Source="+ AlmacenSource + ";Initial Catalog="+ AlmacenBase + ";User ID="+ AlmacenUser + ";Password="+ AlmacenPassword + "";
            return con;
        }
        
    }
}