﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace Web.Controllers
{
    public class ConfiguracionController : Controller
    {
        //
        // GET: /Configuracion/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

    }
}
