﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace Web.Controllers
{
    public class PermisosController : Controller
    {
        //
        // GET: /Permisos/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Rol> roles = (from r in dc.Rol
                                   where r.Activo == true
                                   orderby r.Nombre
                                   select r).ToList();

            ViewData["roles"] = roles;

            List<Modulo> modulos = (from m in dc.Modulo
                                    where m.Activo==true
                                        orderby m.ModuloClave
                                        select m).ToList();

            ViewData["modulos"] = modulos;



            return View();
        }

        public ActionResult Obten()
        {
            int rol = Convert.ToInt32(Request["rol"]);

            ModeloAlmacen dc = new ModeloAlmacen();

            List<Permiso> pe = (from p in dc.Permiso
                                    where p.RolClave == rol
                                    select p).ToList();

            List<String> cadena = new List<string>();

            foreach(Permiso p in pe)
            {
                cadena.Add(p.ModuloClave + "|" + Convert.ToInt32(p.Consulta) + "|" + Convert.ToInt32(p.Agrega) + "|" + Convert.ToInt32(p.Edita) + "|" + Convert.ToInt32(p.Elimina) + "|" + Convert.ToInt32(p.Ejecuta));
            }


            return Json(cadena,JsonRequestBehavior.AllowGet);
        }

        public ActionResult Guarda()
        {
            int rol = Convert.ToInt32(Request["rol"]);
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();
                List<Modulo> modulos = (from m in dc.Modulo
                                            orderby m.ModuloClave
                                            select m).ToList();
                foreach (Modulo m in modulos)
                {
                    Permiso pe = (from p in dc.Permiso
                                      where p.RolClave == rol
                                      && p.ModuloClave == m.ModuloClave
                                      select p).SingleOrDefault();
                    bool nuevo = false;
                    if (pe == null)
                    {
                        nuevo = true;
                        pe = new Permiso();
                    }
                    pe.RolClave = rol;
                    pe.ModuloClave = m.ModuloClave;
                    pe.Consulta = false;
                    try
                    {
                        if (Request["co_" + m.ModuloClave] == "on")
                        {
                            pe.Consulta = true;
                        }
                    }
                    catch
                    {
                    }
                    pe.Agrega = false;
                    try
                    {
                        if (Request["ag_" + m.ModuloClave] == "on")
                        {
                            pe.Agrega = true;
                        }
                    }
                    catch
                    {
                    }
                    pe.Edita = false;
                    try
                    {
                        if (Request["ed_" + m.ModuloClave] == "on")
                        {
                            pe.Edita = true;
                        }
                    }
                    catch
                    {
                    }
                    pe.Elimina = false;
                    try
                    {
                        if (Request["el_" + m.ModuloClave] == "on")
                        {
                            pe.Elimina = true;
                        }
                    }
                    catch
                    {
                    }
                    pe.Ejecuta = false;
                    try
                    {
                        if (Request["ej_" + m.ModuloClave] == "on")
                        {
                            pe.Ejecuta = true;
                        }
                    }
                    catch
                    {
                    }
                    if (nuevo)
                    {
                        dc.Permiso.Add(pe);
                    }
                    dc.SaveChanges();
                }
                return Content("1");
            }
            catch { }
            return Content("0");
        }

    }
}
