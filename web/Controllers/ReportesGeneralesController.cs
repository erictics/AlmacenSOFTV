﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using web.Utils;
using web.Controllers;
using Web.Controllers;
using System.Data.SqlClient;
using OfficeOpenXml;
using web.Models;

namespace web.Controllers
{
    public class ReportesGeneralesController : Controller
    {
        //
        // GET: /ReporteRecepciones/


        ModeloAlmacen dc = new ModeloAlmacen();



        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {


            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();

                catDistribuidor cd = new catDistribuidor();
                cd.catDistribuidorClave = Guid.Empty;
                cd.Nombre = "SUBALMACENES";
                cd.IdDistribuidor = -1;

                catDistribuidor cd1 = new catDistribuidor();
                cd1.catDistribuidorClave = Guid.Empty;
                cd1.Nombre = "ALMACENES PRINCIPALES";
                cd1.IdDistribuidor = 0;


                distribuidor.Add(cd);
                distribuidor.Add(cd1);
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }

            List<catClasificacionArticulo> cla = dc.catClasificacionArticulo.ToList();
            List<catProveedor> proveedores = dc.catProveedor.ToList();
            ViewData["distribuidor"] = distribuidor;
            ViewData["proveedores"] = proveedores;
            ViewData["clasificaciones"] = cla;
            return View();
        }


        public class recepciones
        {

            public long pedido { get; set; }
            public long recep { get; set; }

            public string usuario { get; set; }

            public DateTime fecha { get; set; }

            public string probvee { get; set; }

            public string articulo { get; set; }

            public Guid clvarticulo { get; set; }

            public string tipo { get; set; }

            public int cantidad { get; set; }

            public string serie { get; set; }

            public decimal precio { get; set; }

            public decimal total { get; set; }
        }



        public class ArticulosRec
        {

            public Guid ArticuloClave { get; set; }
            public string Nombre { get; set; }

            public int cantidad { get; set; }
        }



        public ActionResult RecepcionesExcel()
        {

            string almacenes = "";
            try
            {
                almacenes = Request["Almacenes"];
            }
            catch
            { }



            int reporte = 1;
            try
            {
                reporte = Int32.Parse(Request["reporte"].ToString());
            }
            catch
            { }


            int recepcion = 1;
            try
            {
                recepcion = Int32.Parse(Request["recepcion"].ToString());
            }
            catch
            { }


            Guid proveedor = Guid.Empty;
            try
            {
                proveedor = Guid.Parse(Request["proveedor"].ToString());
            }
            catch
            { }

            string distribuidor = "";
            try
            {
                distribuidor = Request["Distribuidor"].ToString();
            }
            catch
            { }


            int servicio = 0;
            try
            {
                servicio = Int32.Parse(Request["servicio"].ToString());
            }
            catch
            { }

            int tipo = 0;
            try
            {
                tipo = Int32.Parse(Request["TipoArticulo"].ToString());
            }
            catch
            { }

            Guid articulo = Guid.Empty;
            try
            {
                articulo = Guid.Parse(Request["Articulo"].ToString());
            }
            catch
            { }

            int proceso = 0;
            try
            {
                proceso = Int32.Parse(Request["proceso"].ToString());
            }
            catch
            { }

            string[] t = almacenes.ToString().Split(',');
            List<int> guidst = new List<int>();
            foreach (string tt in t)
            {
                try
                {
                    guidst.Add(Int32.Parse(tt));
                }
                catch { }
            }

            List<Entidad> Listaalmacenes = new List<Entidad>();

            guidst.ForEach(s =>
            {
                try
                {
                    Entidad e = dc.Entidad.Where(u => u.idEntidadSofTV == s && u.catEntidadTipoClave == 1).First();
                    Listaalmacenes.Add(e);
                }
                catch
                { }
            });
            string txtservicio = "";
            Connection c = new Connection();
            try
            {
                SqlConnection conexionSQL = new SqlConnection(c.GetConnectionString());
                try
                {
                    conexionSQL.Open();
                }
                catch
                {

                }
                SqlCommand comandoSql = new SqlCommand("select * from  CatTipoServicios where IdServicio=@servicio");
                comandoSql.Connection = conexionSQL;
                comandoSql.Parameters.AddWithValue("@servicio", servicio);
                SqlDataReader reader = comandoSql.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        txtservicio = reader[1].ToString();

                    }
                }
                reader.Close();
                conexionSQL.Close();



            }
            catch { }
            string txtclasificacion = "";
            try
            {
                txtclasificacion = Request["clasificacion"].ToString();
            }
            catch { }

            string txttipo = "";

            try
            {

                txttipo = dc.catTipoArticulo.Where(y => y.catTipoArticuloClave == tipo).Select(d => d.Descripcion).First();

            }
            catch
            {
                txttipo = "Todos los tipos";

            }
            string txtarticulo = "";
            try
            {

                txtarticulo = dc.Articulo.Where(y => y.ArticuloClave == articulo).Select(d => d.Nombre).First();

            }
            catch
            {
                txtarticulo = "Todos los artículos";

            }

            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaIn = fechaInicio + " 00:01:00:AM";
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            string fechaFn = fechaFin + " 11:59:00:PM";
            DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            String empresa = Config.Dame("EMPRESA");

            string orden = "";
            if (recepcion == 1)
            {
                orden = "Recepción Con orden de compra";
            }
            if (recepcion == 2)
            {
                orden = "Recepción sin orden de compra";
            }

            ImportaRecepcionesExcel(Listaalmacenes, recepcion, reporte, proveedor, servicio, tipo, articulo, fi, fn, txtarticulo, txttipo, txtservicio, orden);
            return null;
        }

        public void ImportaRecepcionesExcel(List<Entidad> Listaalmacenes, int recepcion, int reporte, Guid proveedor, int servicio, int tipo, Guid articulo, DateTime fi, DateTime fn, string txtarticulo, string txttipo, string txtservicio, string orden)
        {


            using (ExcelPackage pck = new ExcelPackage())
            {

                Connection c = new Connection();
                Guid almacen_central = Guid.Parse("00000000-0000-0000-0000-000000000001");
                int id = 0;
                Listaalmacenes.ForEach(x =>
                {
                    id = id + 1;
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(id + "" + x.Nombre
                       );
                    List<recepciones> lista = new List<recepciones>();

                    if (x.EntidadClave == almacen_central)
                    {
                        SqlConnection conexionSQL = new SqlConnection(c.GetConnectionString());
                        try
                        {
                            conexionSQL.Open();
                        }
                        catch
                        {

                        }
                        SqlCommand comandoSql = new SqlCommand("exec ObtenRecepciones @soyprincipal,@almacenclave,@tiporecepcion,@tiporeporte,@proveedor,@tiposerv,@tipoart,@articulo,@fechainicio,@fechafin");
                        comandoSql.CommandTimeout = 0;
                        comandoSql.Connection = conexionSQL;
                        comandoSql.Parameters.AddWithValue("@soyprincipal", true);
                        comandoSql.Parameters.AddWithValue("@almacenclave", almacen_central);
                        comandoSql.Parameters.AddWithValue("@tiporecepcion", recepcion);
                        comandoSql.Parameters.AddWithValue("@tiporeporte", reporte);
                        comandoSql.Parameters.AddWithValue("@proveedor", proveedor);
                        comandoSql.Parameters.AddWithValue("@tiposerv", servicio);
                        comandoSql.Parameters.AddWithValue("@tipoart", tipo);
                        comandoSql.Parameters.AddWithValue("@articulo", articulo);
                        comandoSql.Parameters.AddWithValue("@fechainicio", fi);
                        comandoSql.Parameters.AddWithValue("@fechafin", fn);
                        SqlDataReader reader = comandoSql.ExecuteReader();
                        try
                        {

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    recepciones r = new recepciones();
                                    r.pedido = long.Parse(reader[0].ToString());
                                    r.recep = long.Parse(reader[1].ToString());
                                    r.fecha = DateTime.Parse(reader[2].ToString());
                                    r.probvee = reader[3].ToString();
                                    r.usuario = reader[4].ToString();
                                    r.articulo = reader[5].ToString();
                                    r.cantidad = Int32.Parse(reader[6].ToString());
                                    r.tipo = reader[7].ToString();
                                    r.clvarticulo = Guid.Parse(reader[8].ToString());
                                    try
                                    {
                                        r.serie = reader[9].ToString();
                                    }
                                    catch { }
                                    lista.Add(r);

                                }
                            }
                            reader.Close();
                            conexionSQL.Close();

                        }
                        catch
                        {
                            reader.Close();
                            conexionSQL.Close();

                        }






                    }
                    else
                    {
                        SqlConnection conexionSQL = new SqlConnection(dc.Database.Connection.ConnectionString);

                        try
                        {
                            conexionSQL.Open();
                        }
                        catch
                        {

                        }
                        SqlCommand comandoSql = new SqlCommand("exec ObtenRecepciones @soyprincipal,@almacenclave,@tiporecepcion,@tiporeporte,@proveedor,@tiposerv,@tipoart,@articulo,@fechainicio,@fechafin");
                        comandoSql.CommandTimeout = 0;
                        comandoSql.Connection = conexionSQL;
                        comandoSql.Parameters.AddWithValue("@soyprincipal", false);
                        comandoSql.Parameters.AddWithValue("@almacenclave", x.EntidadClave);
                        comandoSql.Parameters.AddWithValue("@tiporecepcion", 1);
                        comandoSql.Parameters.AddWithValue("@tiporeporte", reporte);
                        comandoSql.Parameters.AddWithValue("@proveedor", proveedor);
                        comandoSql.Parameters.AddWithValue("@tiposerv", servicio);
                        comandoSql.Parameters.AddWithValue("@tipoart", tipo);
                        comandoSql.Parameters.AddWithValue("@articulo", articulo);
                        comandoSql.Parameters.AddWithValue("@fechainicio", fi);
                        comandoSql.Parameters.AddWithValue("@fechafin", fn);
                        SqlDataReader reader = comandoSql.ExecuteReader();
                        try
                        {

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    recepciones r = new recepciones();
                                    r.pedido = long.Parse(reader[0].ToString());
                                    r.recep = long.Parse(reader[1].ToString());
                                    r.fecha = DateTime.Parse(reader[2].ToString());
                                    r.probvee = reader[3].ToString();
                                    r.usuario = reader[4].ToString();
                                    r.articulo = reader[5].ToString();
                                    r.cantidad = Int32.Parse(reader[6].ToString());
                                    r.tipo = reader[7].ToString();
                                    r.clvarticulo = Guid.Parse(reader[8].ToString());
                                    try
                                    {
                                        r.serie = reader[9].ToString();
                                    }
                                    catch { }
                                    lista.Add(r);

                                }
                            }
                            reader.Close();
                            conexionSQL.Close();

                        }
                        catch
                        {
                            reader.Close();
                            conexionSQL.Close();

                        }
                    }

                    ws.Column(1).Width = 20;
                    ws.Column(2).Width = 20;
                    ws.Column(3).Width = 20;
                    ws.Column(4).Width = 20;
                    ws.Column(5).Width = 20;
                    ws.Column(6).Width = 15;
                    ws.Column(7).Width = 15;
                    ws.Column(8).Width = 15;
                    ws.Column(9).Width = 15;


                    if (lista.Count > 0)
                    {
                        string empresa = Config.Dame("EMPRESA");
                        ws.Cells["A1"].Value = empresa;
                        ws.Cells["A2"].Value = "TIPO ARTÍCULO: " + txttipo;
                        ws.Cells["A3"].Value = "ARTÍCULO: " + txtarticulo;
                        ws.Cells["B1"].Value = "RECEPCIÓN DE MATERIAL";
                        ws.Cells["B3"].Value = orden;
                        ws.Cells["B3"].Value = "SERVICIO: " + txtservicio;

                        ws.Cells["A4"].Value = "No.PEDIDO";
                        ws.Cells["B4"].Value = "No.RECEPCION";
                        ws.Cells["C4"].Value = "USUARIO";
                        ws.Cells["D4"].Value = "FECHA";
                        ws.Cells["E4"].Value = "PROVEEDOR";
                        ws.Cells["F4"].Value = "TIPO ARTICULO";
                        ws.Cells["G4"].Value = "ARTICULO";
                        ws.Cells["H4"].Value = "CANTIDAD";
                        ws.Cells["I4"].Value = "SERIE";

                        ws.Cells["A5"].LoadFromCollection(lista.Select(o => new { o.pedido, o.recep, o.usuario, FECHA = o.fecha.ToShortDateString(), o.probvee, o.tipo, o.articulo, o.cantidad, o.serie }), false);

                    }








                });

                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Recepcion de material.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());

            }
        }

        public class Traspaso
        {
            public string Numero { get; set; }
            public DateTime Fecha { get; set; }
            public string Servicio { get; set; }
            public string Destino { get; set; }
            public string clasificacion { get; set; }
            public string tipo { get; set; }
            public string articulo { get; set; }
            public Guid articulo_clave { get; set; }
            public int cantidad { get; set; }
        }



        public ActionResult TraspasoPDF()
        {
            int almacen = -1;
            try
            {
                almacen = Int32.Parse(Request["Almacen"].ToString());
            }
            catch
            { }

            Guid articulo = Guid.Empty;
            try
            {
                articulo = Guid.Parse(Request["Articulo"].ToString());
            }
            catch
            { }
            int servicio = 0;
            try
            {
                servicio = Int32.Parse(Request["servicio"].ToString());
            }
            catch
            { }
            int TipoArticulo = 0;
            try
            {
                TipoArticulo = Int32.Parse(Request["TipoArticulo"].ToString());
            }
            catch
            { }
            int tiporeporte = 0;
            try
            {
                tiporeporte = Int32.Parse(Request["tiporeporte"].ToString());
            }
            catch
            { }
            Connection c = new Connection();
            StringBuilder sb = new StringBuilder();
            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaIn = fechaInicio + " 00:01:00:AM";
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            string fechaFn = fechaFin + " 11:59:00:PM";
            DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            string ser = dc.CatTipoServicios.Where(p => p.IdServicio == servicio).Select(o => o.Descripcion).First();
            string art = "Todos los artículos";
            try
            {
                art = dc.Articulo.Where(u => u.ArticuloClave == articulo).Select(o => o.Nombre).First();
            }
            catch
            { }
            string tip = "Todos los tipos de artículo";
            try
            {
                tip = dc.catTipoArticulo.Where(u => u.catTipoArticuloClave == TipoArticulo).Select(o => o.Descripcion).First();
            }
            catch
            { }

            string alm = "Todos los Almacenes";
            string dis = "";
            try
            {
                if (almacen == 0)
                {

                }
                else
                {
                    alm = dc.Entidad.Where(p => p.idEntidadSofTV == almacen).Select(y => y.Nombre).First();
                    dis = dc.Entidad.Where(p => p.idEntidadSofTV == almacen).Select(u => u.catDistribuidor.Nombre).First();
                }

            }
            catch
            { }


            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");

            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h3><b>TRASPASO DE MATERIAL</b></h3>");
            if (tiporeporte == 1)
            {
                sb.Append("<b>MATERIAL ENVIADO</b><br>");

            }
            else
            {
                sb.Append("<h4><b>MATERIAL RECIBIDO</b></h4>");
            }
            sb.Append("<b>Tipo Artículo:</b>" + tip + "<br>");
            sb.Append("<b>Artículo:</b>" + art + "<br>");
           // sb.Append("<h4><b>servicio:</b>" + ser + "</h4>");
            if (almacen != 0)
            {
                sb.Append("<b>Distribuidor:</b>" + dis +"<br>" );
            }
            sb.Append("<b>Almacén:</b>" + alm + "<br>");
            sb.Append("<b>Periodo: </b> " + fi.ToShortDateString() + " al " + fn.ToShortDateString() + "<br>");
            sb.Append("<b>Fecha: </b> " + DateTime.Today.ToShortDateString() + "<br>");
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("</r><br><br>");




            SqlConnection conexionSQL = new SqlConnection(c.GetConnectionString());
            try
            {
                conexionSQL.Open();
            }
            catch
            {

            }
            List<Traspaso> lista = new List<Traspaso>();
            SqlCommand comandoSql = new SqlCommand("exec ObtenTraspasos @tiporeporte,@almacen,@tipo,@articulo,@servicio,@fechainicio,@fechafin");
            comandoSql.CommandTimeout = 0;
            comandoSql.Connection = conexionSQL;
            comandoSql.Parameters.AddWithValue("@tiporeporte", tiporeporte);
            comandoSql.Parameters.AddWithValue("@almacen", almacen);
            comandoSql.Parameters.AddWithValue("@tipo", TipoArticulo);
            comandoSql.Parameters.AddWithValue("@articulo", articulo);
            comandoSql.Parameters.AddWithValue("@servicio", servicio);
            comandoSql.Parameters.AddWithValue("@fechainicio", fi);
            comandoSql.Parameters.AddWithValue("@fechafin", fn);
            SqlDataReader reader = comandoSql.ExecuteReader();
            try
            {

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        long NumeroPedido = long.Parse(reader[0].ToString());
                        Proceso p = dc.Proceso.Where(i => i.NumeroPedido == NumeroPedido).First();

                        sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"">");
                        sb.Append(@"<tr border=1 style=""font-size:8px"">");
                        sb.Append(@"<td border=1 colspan=2>");
                        sb.Append(@"<b style=""font-size:8px"">Traspaso: </b> " + p.NumeroPedido + "<br>");
                        sb.Append(@"<b style=""font-size:8px"">Fecha: </b>" + p.Fecha.Value.ToShortDateString() + "<br>");
                        sb.Append(@"</td>");

                        sb.Append(@"<td border=1 colspan=2 style=""border-left-style:dotted !important;"">");
                        sb.Append(@"<b style=""font-size:8px"">Destino: </b>" + p.Entidad.Nombre + "<br>");
                        //sb.Append(@"<b style=""font-size:8px"">Elaboró: </b>" + p.Usuario.Nombre + "<br>");
                        sb.Append(@"</td>");
                        sb.Append("</tr>");


                        sb.Append("<tr border=0>");
                        sb.Append(@"<td style=""font-size:7px""><b>Descripción: </b></td>");
                        sb.Append(@"<td style=""font-size:7px""><b>Cantidad Entregada: </b></td>");
                        sb.Append(@"<td style=""font-size:7px""><b>Unidad: </b></td>");
                        sb.Append(@"<td style=""font-size:7px""><b>Serie: </b> </td>");
                        sb.Append("</tr>");


                        p.ProcesoInventario.ToList().ForEach(d => {

                            sb.Append("<tr border=0>");
                            sb.Append(@"<td style=""font-size:7px"">" + d.Inventario.Articulo.Nombre + "</td>");
                            sb.Append(@"<td align=""center"" style=""font-size:7px"">" + d.Cantidad + "</td>");
                            sb.Append(@"<td style=""font-size:7px"">" + d.Inventario.Articulo.catUnidad.Nombre + "</td>");
                            try
                            {
                                Serie s = dc.Serie.Where(xa => xa.InventarioClave == d.InventarioClave).First();
                                sb.Append(@"<td style=""font-size:7px"">" + s.Valor + "</td>");
                            }
                            catch (Exception ex)
                            {
                                sb.Append(@"<td style=""font-size:7px""></td>");
                            }
                            sb.Append("</tr>");

                        });

                        sb.Append(@"</table>");

                        sb.Append("<br>");
                        sb.Append("<br>");
                    }
                }
                reader.Close();
                conexionSQL.Close();

            }
            catch
            {
                reader.Close();
                conexionSQL.Close();
            }
            
            PDF elpdf = new PDF();
            Guid g = Guid.NewGuid();
            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "Traspaso.pdf";
            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
            ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "Traspaso.pdf" + "' scrolling='no' width='100%' style='height: 100vh;'></iframe>";

            return View();

        }

        public ActionResult Detalle()
        {
            Guid almacen = Guid.Empty;
            try
            {
                almacen = Guid.Parse(Request["almacen"].ToString());
            }
            catch { }
            Guid proveedor = Guid.Empty;
            try
            {
                proveedor = Guid.Parse(Request["proveedor"].ToString());
            }
            catch
            { }
            string distribuidor = "";
            try
            {
                distribuidor = Request["Distribuidor"].ToString();
            }
            catch
            { }

            int tipo = 0;
            try
            {
                tipo = Int32.Parse(Request["TipoArticulo"].ToString());
            }
            catch
            { }
            Guid articulo = Guid.Empty;
            try
            {
                articulo = Guid.Parse(Request["Articulo"].ToString());
            }
            catch
            { }

            int clasificacion = 0;
            try
            {
                clasificacion = int.Parse(Request["clasificacion"].ToString());
            }
            catch
            { }
           
            Connection c = new Connection();
            StringBuilder sb = new StringBuilder();
            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaIn = fechaInicio + " 00:01:00:AM";
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            string fechaFn = fechaFin + " 11:59:00:PM";
            DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h4><b>LISTADO DE RECEPCION DE MATERIAL</b></h4>");
            sb.Append("<b>Periodo: </b> " + fi.ToShortDateString() + " al " + fn.ToShortDateString() + " <br>");
           
            List<ProcesoArticulo> pa = new List<ProcesoArticulo>();
            try
            {
                catProveedor prov = dc.catProveedor.Where(s => s.catProveedorClave == proveedor).First();
                sb.Append("<b>Proveedor: </b>" + prov.Nombre + "<br>");

            }
            catch
            {
                sb.Append("<b>Proveedor: </b>Todos los proveedores<br>");

            }
            sb.Append("<b>Fecha: </b> " + DateTime.Now.ToShortDateString() + " <br>");
            sb.Append("</td>");

            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=100 align=right><br>");
            //sb.Append(@"</td>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<br>");

            List<Entidad> almacenes = (from a in dc.Entidad where almacen == Guid.Empty || almacen == a.EntidadClave select a).ToList();
            foreach(var alma in almacenes)
            {
                sb.Append(@"<b>Almacén: </b><span>" + alma.Nombre + "</span><br>");
                sb.Append(@"__________________________________________________________<br>");
                List<catClasificacionArticulo> listClas = (from a in dc.catClasificacionArticulo where clasificacion == 0 || clasificacion == a.catClasificacionArticuloClave select a).ToList();
                foreach(var clas in listClas)
                {
                    

                    List<catTipoArticulo> listtipos = (from a in dc.catTipoArticulo where a.catClasificacionArticuloClave == clas.catClasificacionArticuloClave select a).ToList();

                    foreach(var tip in listtipos)
                    {
                       

                        List<recepciones> lista = new List<recepciones>();
                        SqlConnection conexionSQL = new SqlConnection(c.GetConnectionString());                       
                        conexionSQL.Open();                        
                        SqlCommand comandoSql = new SqlCommand("exec ObtenRecepciones @soyprincipal,@almacenclave,@tiporecepcion,@tiporeporte,@proveedor,@tiposerv,@tipoart,@articulo,@fechainicio,@fechafin");
                        comandoSql.CommandTimeout = 0;
                        comandoSql.Connection = conexionSQL;
                        comandoSql.Parameters.AddWithValue("@soyprincipal", false);
                        comandoSql.Parameters.AddWithValue("@almacenclave", alma.EntidadClave);
                        comandoSql.Parameters.AddWithValue("@tiporecepcion", 1);
                        comandoSql.Parameters.AddWithValue("@tiporeporte", 1);
                        comandoSql.Parameters.AddWithValue("@proveedor", proveedor);
                        comandoSql.Parameters.AddWithValue("@tiposerv", 0);
                        comandoSql.Parameters.AddWithValue("@tipoart", tip.catTipoArticuloClave);
                        comandoSql.Parameters.AddWithValue("@articulo", articulo);
                        comandoSql.Parameters.AddWithValue("@fechainicio", fi);
                        comandoSql.Parameters.AddWithValue("@fechafin", fn);
                        SqlDataReader reader = comandoSql.ExecuteReader();
                        try
                        {

                            if (reader.HasRows)
                            {
                                sb.Append(@"<b style=""font-size:8px;"">Clasificación : </b><span style=""font-size:8px;"">" + clas.Descripcion + "</span><br>");
                                sb.Append(@"<b style=""font-size:8px;"">Tipo de Artículo : </b><span style=""font-size:8px;"">" + tip.Descripcion + "</span><br>");

                                sb.Append("<table border=0>");
                                sb.Append("<tr>");
                                sb.Append(@"<td style=""font-size:7px;""><b>Fecha</b></td>");
                                sb.Append(@"<td style=""font-size:7px""><b>Folio</b></td>");
                                sb.Append(@"<td colspan=2 style=""font-size:7px""><b>Proveedor</b></td>");
                                sb.Append(@"<td colspan=2 style=""font-size:7px""><b>Nombre Artículo</b></td>");
                                sb.Append(@"<td style=""font-size:7px""><b>Cant.Surt</b></td>");
                                sb.Append(@"<td style=""font-size:7px""><b>Unidad</b></td>");
                                sb.Append(@"<td style=""font-size:7px;text-align:right;""><b>Prec Unit.</b></td>");
                                sb.Append(@"<td style=""font-size:7px;text-align:right;""><b>Totales</b></td>");
                                sb.Append(@"<td colspan=2 style=""font-size:7px""><b>Usuario</b></td>");
                                sb.Append("</tr>");


                                decimal total = 0;
                                while (reader.Read())
                                {
                                    recepciones r = new recepciones();
                                    r.pedido = long.Parse(reader[0].ToString());
                                    r.recep = long.Parse(reader[1].ToString());
                                    r.fecha = DateTime.Parse(reader[2].ToString());
                                    r.probvee = reader[3].ToString();
                                    r.usuario = reader[4].ToString();
                                    r.articulo = reader[5].ToString();
                                    r.cantidad = Int32.Parse(reader[6].ToString());
                                    r.tipo = reader[7].ToString();
                                    r.clvarticulo = Guid.Parse(reader[8].ToString());
                                    r.precio = decimal.Parse(reader[9].ToString());
                                    r.total = decimal.Parse(reader[10].ToString());
                                    try
                                    {
                                        r.serie = reader[9].ToString();
                                    }
                                    catch { }

                                    Articulo ar = dc.Articulo.Where(gg => gg.ArticuloClave == r.clvarticulo).First();
                                    sb.Append("<tr>");
                                    sb.Append(@"<td style=""font-size:7px"" >" + r.fecha.ToShortDateString() + "</td>");
                                    sb.Append(@"<td style=""font-size:7px"">" + r.recep + "</td>");
                                    sb.Append(@"<td colspan=2 style=""font-size:5px"" >" + r.probvee + "</td>");
                                    sb.Append(@"<td colspan=2 style=""font-size:6px"" >" + r.articulo + "</td>");
                                    sb.Append(@"<td style=""font-size:7px"" >" + r.cantidad + "</td>");
                                    sb.Append(@"<td style=""font-size:7px"" >" + ar.catUnidad.Nombre + "</td>");
                                    sb.Append(@"<td style=""font-size:7px;text-align:right;"" >" + r.precio.ToString("C") + "</td>");
                                    sb.Append(@"<td style=""font-size:7px;text-align:right;"" >" + r.total.ToString("C") + "</td>");
                                    sb.Append(@"<td colspan=2 style=""font-size:5px"">" + r.usuario + "</td>");
                                    sb.Append("</tr>");

                                    total += r.total;
                                }

                                sb.Append("<tr>");
                                sb.Append(@"<td colspan=10 style=""font-size:7px; text-align:right;"" ><b>Total: </b>"+total.ToString("C") + "</td>");
                                sb.Append(@"<td colspan=2></td>");
                                sb.Append("</tr>");

                                sb.Append("</table>");
                            }else
                            {
                               // sb.Append("<b>Sin Movimientos</b>");
                            }
                            reader.Close();
                            conexionSQL.Close();
                        }
                        catch
                        {
                            reader.Close();
                            conexionSQL.Close();
                        }

                       
                    }
                }

            }            
         
            PDF elpdf = new PDF();
            Guid g = Guid.NewGuid();
            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "DesgloseRecepciones.pdf";
            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
            ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "DesgloseRecepciones.pdf" + "' scrolling='no' width='100%' style='height: 100vh;'></iframe>";
            return View();
        }




        PedidoController pc = new PedidoController();


        public string RecepcionesPorPedido(Proceso pedido)
        {

            List<Proceso> Recepcion = new List<Proceso>();
            Entidad x = pedido.Entidad;
            StringBuilder sb = new StringBuilder();
            if (x.catEntidadTipoClave == 3)
            {
                Recepcion = (from a in dc.Proceso where a.ProcesoClaveRespuesta == pedido.ProcesoClave && a.catTipoProcesoClave == 2 && a.catEstatusClave != 6 && a.EntidadClaveSolicitante == x.EntidadClave orderby a.NumeroPedido select a).ToList();
            }
            else
            {
                List<Proceso> lista_salidas = dc.Proceso.Where(xs => xs.ProcesoClaveRespuesta == pedido.ProcesoClave && xs.catTipoProcesoClave == 3 && xs.catEstatusClave != 6).ToList();
                lista_salidas.ForEach(pp =>
                {
                    List<Proceso> recepciones_ = dc.Proceso.Where(xy => xy.catTipoProcesoClave == 4 && xy.ProcesoClaveRespuesta == pp.ProcesoClave && xy.catEstatusClave != 6).ToList();
                    Recepcion.AddRange(recepciones_);

                });

            }
            List<Proceso> pedidosGenerados = dc.Proceso.Where(s => s.catTipoProcesoClave == 15 && s.ProcesoClaveRespuesta == pedido.ProcesoClave && s.catEstatusClave != 6).ToList();
            if (pedidosGenerados.Count > 0)
            {

                pedidosGenerados.ForEach(c =>
                {
                    List<Proceso> lista_salidas = dc.Proceso.Where(xs => xs.ProcesoClaveRespuesta == c.ProcesoClave && xs.catTipoProcesoClave == 3 && xs.catEstatusClave != 6).ToList();
                    lista_salidas.ForEach(p =>
                    {
                        List<Proceso> recepciones_por_asignacion = dc.Proceso.Where(xy => xy.catTipoProcesoClave == 4 && xy.ProcesoClaveRespuesta == p.ProcesoClave && xy.catEstatusClave != 6).ToList();
                        Recepcion.AddRange(recepciones_por_asignacion);

                    });
                });

            }


            if (Recepcion.Count > 0)
            {

                if (pedido.Entidad.catEntidadTipoClave == 3)
                {
                    sb.Append("<table cellspacing=0>");
                    sb.Append(@"<tr><td colspan=6 align=center style=""font-size:8px""  bgcolor=""bgcolor=""#9E9E9E""""><center>Desglose de recepciones</center></td></tr>");
                    sb.Append("<tr>");

                    sb.Append(@"<td style=""font-size:6px"" color=""#FFFFFF"" bgcolor=""#9E9E9E"" > #Recepción</td>");
                    sb.Append(@"<td style=""font-size:6px"" color=""#FFFFFF"" bgcolor=""#9E9E9E"" >ARTICULO</td>");
                    sb.Append(@"<td style=""font-size:6px"" color=""#FFFFFF"" bgcolor=""#9E9E9E"" >CANTIDAD</td>");
                    sb.Append(@"<td style=""font-size:6px"" color=""#FFFFFF"" bgcolor=""#9E9E9E"" >PRECIO UNITARIO</td>");
                    sb.Append(@"<td style=""font-size:6px"" color=""#FFFFFF"" bgcolor=""#9E9E9E"" >TOTAL MX</td>");
                    sb.Append(@"<td style=""font-size:6px"" color=""#FFFFFF"" bgcolor=""#9E9E9E"" >TOTAL USD</td>");
                    sb.Append("</tr>");
                    Recepcion.ForEach(o =>
                    {
                        decimal total_mx = decimal.Parse("0.00");
                        decimal total_usd = decimal.Parse("0.00");
                        o.ProcesoArticulo.ToList().ForEach(s =>
                        {
                            sb.Append("<tr>");

                            sb.Append(@"<td style=""font-size:6px"">" + o.NumeroPedido + "</td>");
                            sb.Append(@"<td style=""font-size:6px"">" + s.Articulo.Nombre + "</td>");
                            sb.Append(@"<td style=""font-size:6px"">" + s.CantidadEntregada + "</td>");
                            sb.Append(@"<td style=""font-size:6px"">$" + s.PrecioUnitario + "</td>");
                            if (s.catTipoMonedaClave == 4)
                            {

                                if (x.catEntidadTipoClave == 3)
                                {
                                    total_mx = total_mx + decimal.Parse((s.CantidadEntregada.Value * s.PrecioUnitario.Value).ToString());
                                    sb.Append(@"<td style=""font-size:6px"">$" + s.CantidadEntregada * s.PrecioUnitario + "</td>");
                                }
                                else
                                {
                                    total_mx = total_mx + decimal.Parse((s.Cantidad.Value * s.PrecioUnitario.Value).ToString());
                                    sb.Append(@"<td style=""font-size:6px"">$" + s.Cantidad * s.PrecioUnitario + "</td>");
                                }
                                sb.Append(@"<td align=right style=""font-size:6px"">$" + 0.00 + "</td>");
                            }
                            else
                            {


                                sb.Append(@"<td align=right style=""font-size:6px"">$" + 0.00 + "</td>");
                                if (x.catEntidadTipoClave == 3)
                                {
                                    sb.Append(@"<td align=right style=""font-size:6px"">$" + (s.CantidadEntregada * s.PrecioUnitario) + "</td>");
                                    total_usd = total_usd + decimal.Parse((s.CantidadEntregada.Value * s.PrecioUnitario.Value).ToString());
                                }
                                else
                                {
                                    sb.Append(@"<td align=right style=""font-size:6px"">$" + (s.Cantidad * s.PrecioUnitario) + "</td>");
                                    try
                                    {
                                        total_usd = total_usd + decimal.Parse((s.Cantidad.Value * s.PrecioUnitario.Value).ToString());
                                    }
                                    catch
                                    {

                                    }

                                }

                            }

                            sb.Append("</tr>");

                        });
                        decimal IVA = decimal.Parse("0.16");
                        sb.Append("<tr>");
                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" colspan=4  >Subtotal</td>");

                        sb.Append(@"<td bgcolor=""#d0d1d2""   style=""font-size:6px"" align=right>$" + total_mx + "</td>");
                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" align=right>$" + total_usd + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" colspan=4  >IVA</td>");

                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" align=right>$" + total_mx * IVA + "</td>");
                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" align=right>$" + total_usd * IVA + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" colspan=4  >Total</td>");

                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" align=right>$" + (total_mx + (total_mx * IVA)) + "</td>");
                        sb.Append(@"<td bgcolor=""#d0d1d2""  style=""font-size:6px"" align=right>$" + (total_usd + (total_usd * IVA)) + "</td>");
                        sb.Append("</tr>");



                        //var Resumen=( from a in Recepcion join b in dc.ProcesoArticulo on a.ProcesoClave equals b.ProcesoClave join c in dc.Inventario on b.InventarioClave equals c.InventarioClave
                        //   group by  into g)

                    });
                    sb.Append("</table>");
                }
                else
                {
                    sb.Append("<table cellspacing=0>");
                    sb.Append(@"<tr><td colspan=5 align=center style=""font-size:8px""  bgcolor=""bgcolor=""#9E9E9E""""><center>Desglose de recepciones</center></td></tr>");
                    sb.Append("<tr>");
                    sb.Append(@"<td style=""font-size:6px"" bgcolor=""#9E9E9E"" > #RECEPCION</td>");
                    sb.Append(@"<td style=""font-size:6px"" bgcolor=""#9E9E9E"" > FECHA</td>");
                    sb.Append(@"<td style=""font-size:6px"" bgcolor=""#9E9E9E"" > RECIBIO</td>");
                    sb.Append(@"<td style=""font-size:6px""  bgcolor=""#9E9E9E"" >ARTICULO</td>");
                    sb.Append(@"<td style=""font-size:6px""  bgcolor=""#9E9E9E"" >CANTIDAD</td>");
                    sb.Append(@"</tr>");
                    Recepcion.ForEach(o =>
                    {
                        o.ProcesoArticulo.ToList().ForEach(y =>
                        {
                            sb.Append("<tr>");
                            sb.Append(@"<td style=""font-size:6px"">#" + o.NumeroPedido + "</td>");
                            sb.Append(@"<td style=""font-size:6px"">" + o.Fecha + "</td>");
                            try
                            {
                                sb.Append(@"<td style=""font-size:6px"">" + o.Usuario1.Nombre + "</td>");
                            }
                            catch
                            {
                                sb.Append(@"<td style=""font-size:6px"">No asignado</td>");
                            }

                            sb.Append(@"<td style=""font-size:6px"">" + y.Articulo.Nombre + "</td>");
                            sb.Append(@"<td style=""font-size:6px"">" + y.Cantidad.Value + "</td>");
                            sb.Append(@"</tr>");

                        });

                    });
                    sb.Append("</table>");
                }

            }


            return sb.ToString();
        }


        public ActionResult ReingresoAparatos()
        {

            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();

                catDistribuidor cd = new catDistribuidor();
                cd.catDistribuidorClave = Guid.Empty;
                cd.Nombre = "SUBALMACENES";
                cd.IdDistribuidor = -1;

                catDistribuidor cd1 = new catDistribuidor();
                cd1.catDistribuidorClave = Guid.Empty;
                cd1.Nombre = "ALMACENES PRINCIPALES";
                cd1.IdDistribuidor = 0;


                distribuidor.Add(cd);
                distribuidor.Add(cd1);
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }

            List<catClasificacionArticulo> cla = dc.catClasificacionArticulo.ToList();
            ViewData["distribuidor"] = distribuidor;
            ViewData["clasificaciones"] = cla;
            return View();
        }

        public ActionResult TraspasoMaterial()
        {
            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
                catDistribuidor cd1 = new catDistribuidor();
                cd1.catDistribuidorClave = Guid.Empty;
                cd1.Nombre = "ALMACEN PRINCIPAL";
                cd1.IdDistribuidor = 0;
                distribuidor.Add(cd1);
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }


            List<catClasificacionArticulo> cla = dc.catClasificacionArticulo.ToList();
            List<CatTipoServicios> servicios = dc.CatTipoServicios.ToList();
            ViewData["distribuidor"] = distribuidor;
            ViewData["clasificaciones"] = cla;
            ViewData["servicios"] = servicios;
            return View();
        }


        public ActionResult RecepcionMaterial()
        {

            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
                catDistribuidor cd1 = new catDistribuidor();
                cd1.catDistribuidorClave = Guid.Empty;
                cd1.Nombre = "ALMACEN PRINCIPAL";
                cd1.IdDistribuidor = 0;
                distribuidor.Add(cd1);
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }

            List<catClasificacionArticulo> cla = dc.catClasificacionArticulo.ToList();
            List<catProveedor> proveedores = dc.catProveedor.ToList();
            List<CatTipoServicios> servicios = dc.CatTipoServicios.ToList();
            ViewData["distribuidor"] = distribuidor;
            ViewData["proveedores"] = proveedores;
            ViewData["clasificaciones"] = cla;
            ViewData["servicios"] = servicios;


            return View();
        }
        [SessionExpireFilterAttribute]
        public ActionResult ReporteBitacora()
        {
            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();


            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }

            ViewData["distribuidor"] = distribuidor;
            return View();
        }

        public ActionResult ReporteBitacoraDetalle()

        {
            Guid distribuidor = Guid.Empty;
            catDistribuidor distribuidor_ = null;
            try
            {
                distribuidor = Guid.Parse(Request["distribuidor"].ToString());
                distribuidor_ = dc.catDistribuidor.Where(x => x.catDistribuidorClave == distribuidor).First();

            }
            catch { }

            int almacen = 0;
            Entidad almacen_ = null;
            try
            {
                almacen = int.Parse(Request["almacen"].ToString());
                almacen_ = dc.Entidad.Where(x => x.idEntidadSofTV == almacen).First();

            }
            catch { }
            Guid tecnico = Guid.Empty;
            Entidad tecnico_ = null;
            try
            {
                tecnico = Guid.Parse(Request["tecnico"].ToString());
                almacen_ = dc.Entidad.Where(x => x.EntidadClave == tecnico).First();
            }
            catch { }

            StringBuilder sb = new StringBuilder();
            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaIn = fechaInicio + " 00:01:00:AM";
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            string fechaFn = fechaFin + " 11:59:00:PM";
            DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h4><b>BITACORAS DE TECNICOS</b></h4>");
            try
            {
                sb.Append("<b>Almacen:</b><span>" + almacen_.Nombre + "</span><br>");
            }
            catch (Exception ex)
            {
                sb.Append("<b>Almacen:</b><span> Todos los almacenes</span><br>");
            }

            try
            {
                sb.Append("<b>Técnico:</b><span> " + tecnico_.Nombre + "</span><br>");
            }
            catch (Exception ex)
            {
                sb.Append("<b>Técnico:</b><span> Todos los técnicos</span><br>");
            }

            sb.Append("<b>Periodo: </b><span> " + fi.ToShortDateString() + "</span><b> al</b><span> " + fn.ToShortDateString() + "</span><br>");

            sb.Append("<b>Fecha: " + DateTime.Now.ToShortDateString() + "</b><br>");


            
            
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<br>");

            List<BitacoraSofTV> bitacoras = (from a in dc.BitacoraSofTV
                                             where (almacen == 0 || a.EntidadClaveAlmacen == almacen_.EntidadClave)
                                             && (tecnico == Guid.Empty || a.EntidadClaveTecnico == tecnico)
                                             && a.Fecha >= fi.Date
                                             && a.Fecha <= fn.Date
                                             select a).ToList();

            foreach(var bit in bitacoras.OrderBy(x=>x.BitacoraSofTVClave).ToList())
            {
                sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"">");
                sb.Append(@"<tr border=1 style=""font-size:8px"">");
                sb.Append(@"<td border=1 colspan=2>");
                sb.Append(@"<b style=""font-size:8px"">Bitácora: </b> " + bit.BitacoraSofTVClave + "<br>");
                sb.Append(@"<b style=""font-size:8px"">Fecha: </b>" + bit.Fecha.Value.ToShortDateString() + "<br>");
                sb.Append(@"<b style=""font-size:8px"">Referencia: </b>ORDEN: " + bit.NumeroOrden);
                sb.Append(@"</td>");

                sb.Append(@"<td border=1 colspan=2 style=""border-left-style:dotted !important;"">");
                sb.Append(@"<b style=""font-size:8px"">Técnico: </b>" + bit.Entidad.Nombre + "<br>");
                sb.Append(@"<b style=""font-size:8px"">Almacén: </b>" + bit.Entidad1.Nombre + "<br>");
                sb.Append(@"<b style=""font-size:8px"">Efectuo Orden: </b>" + bit.NombreUsuario);
                sb.Append(@"</td>");               
                sb.Append("</tr>");


                sb.Append("<tr border=0>");               
                sb.Append(@"<td style=""font-size:7px""><b>Descripción: </b></td>");
                sb.Append(@"<td style=""font-size:7px""><b>Cantidad Entregada: </b></td>");
                sb.Append(@"<td style=""font-size:7px""><b>Unidad: </b></td>");
                sb.Append(@"<td style=""font-size:7px""><b>Serie: </b> </td>");
                sb.Append("</tr>");

                bit.BitacoraSofTVDetalle.ToList().ForEach(d=> {

                    sb.Append("<tr border=0>");
                    sb.Append(@"<td style=""font-size:7px"">" + d.Inventario.Articulo.Nombre+"</td>");
                    sb.Append(@"<td style=""font-size:7px"">" + d.Cantidad+"</td>");
                    sb.Append(@"<td style=""font-size:7px"">" + d.Inventario.Articulo.catTipoArticulo.Descripcion+"</td>");
                   try
                    {
                        Serie s = dc.Serie.Where(xa => xa.InventarioClave == d.InventarioClave).First();
                        sb.Append(@"<td style=""font-size:7px"">" + s.Valor + "</td>");
                    }
                    catch (Exception ex)
                    {
                        sb.Append(@"<td style=""font-size:7px""></td>");
                    }
                    sb.Append("</tr>");

                });

                sb.Append("</table>");
                sb.Append("<br>");
                sb.Append("<br>");
            }
            

            //sb.Append("<table>");
            //sb.Append("<thead>");
            //sb.Append("<tr>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF""># BITACORA</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">NO.ORDEN</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">FECHA</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">USUARIO</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">ALMACEN</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">TECNICO</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">ARTICULO</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">CANTIDAD</td>");
            //sb.Append(@"<td  style=""font-size:6px"" bgcolor=""#424242"" color=""#FFFFFF"">SERIE</td>");
            //sb.Append("</tr>");
            //sb.Append("</thead>");

            //bitacoras.ForEach(x =>
            //{

            //    x.BitacoraSofTVDetalle.ToList().ForEach(h =>
            //    {
            //        sb.Append("<tr>");
            //        sb.Append(@"<td style=""font-size:5px"" >" + x.BitacoraSofTVClave + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"">" + x.NumeroOrden + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"">" + x.Fecha.Value.ToShortDateString() + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"" >" + x.NombreUsuario + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"" >" + x.Entidad.Nombre + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"">" + x.Entidad1.Nombre + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"">" + h.Inventario.Articulo.Nombre + "</td>");
            //        sb.Append(@"<td style=""font-size:5px"">" + h.Inventario.Cantidad + "</td>");
            //        try
            //        {
            //            Serie s = dc.Serie.Where(xa => xa.InventarioClave == h.InventarioClave).First();
            //            sb.Append(@"<td style=""font-size:5px"">" + s.Valor + "</td>");
            //        }
            //        catch (Exception ex)
            //        {
            //            sb.Append(@"<td style=""font-size:5px""></td>");
            //        }

            //        sb.Append(@"</tr>");

            //    });

            //});

            //sb.Append("</tbody>");
            //sb.Append("</table>");

            PDF elpdf = new PDF();
            Guid g = Guid.NewGuid();
            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "bitacora.pdf";
            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
            ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "bitacora.pdf" + "' scrolling='no' width='100%' style='height: 100vh;'></iframe>";

            return View();
        }

    }
}
