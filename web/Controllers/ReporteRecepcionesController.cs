﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;

namespace web.Controllers
{
    public class ReporteRecepcionesController : Controller
    {
        //
        // GET: /ReporteRecepciones/

        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Articulo> articulos= dc.Articulo.Where(x=>x.Activo==true).ToList();
            List<catProveedor> proveedores = dc.catProveedor.Where(x => x.Activo == true).ToList();
            List<Usuario> usuarios = dc.Usuario.Where(x => x.Activo == true && x.RolClave==6).ToList();
            ViewData["articulos"] = articulos;
            ViewData["proveedores"] = proveedores;
            ViewData["usuarios"] = usuarios;
            return View();
        }


        public class recepciones{

            public long pedido{get; set;}
            public long recep{get; set;}

            public Guid usuario{get; set;}

            public string fecha{get; set;}

            public string probvee{get; set;}

            public string articulo{get; set;}

            public int cantidad{get; set;}
        }




     



        public ActionResult Detalle()
        {

            ModeloAlmacen dc = new ModeloAlmacen();


            Guid articulo = Guid.Empty;
            Articulo art= new Articulo();
            Usuario usu=new Usuario();
            try
            {

                articulo = Guid.Parse(Request["articulo"]);
                art = dc.Articulo.Where(x => x.ArticuloClave == articulo).First();
            }
            catch
            {
               
                articulo = Guid.Empty;
            }

           


            Guid usuario = Guid.Empty;
            try
            {
                usuario = Guid.Parse(Request["usuario"]);
                usu=dc.Usuario.Where(x=>x.UsuarioClave==usuario).First();
            }
            catch
            {
                usuario = Guid.Empty;
            }

             bool con_orden = false;
            try
            {
                con_orden = bool.Parse(Request["orden"]);
                
            }
            catch
            {
                con_orden = false;
            }


            

            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaIn = fechaInicio + " 00:01:00:AM";

            string fechaFin = Convert.ToString(Request["fechaFin"]);
            string fechaFn = fechaFin + " 11:59:00:PM";

            DateTime fi = DateTime.ParseExact(fechaIn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);
            DateTime fn = DateTime.ParseExact(fechaFn, "dd/MM/yyyy hh:mm:ss:tt", CultureInfo.InvariantCulture);



            int pdf = Convert.ToInt32(Request["pdf"]);

            StringBuilder sb = new StringBuilder();

            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Desglose recepciones de almacén central: </h3><br>");
            sb.Append("<b>Artículo: </b><br>");
            if(art.Nombre!=null){
                sb.Append(art.Nombre); 
            }
            else
            {
                sb.Append("Todos los artículos"); 
            }
            sb.Append("<br>");          
            sb.Append("<b>Usuario: </b><br>");
            if (usu.Nombre != null)
            {
                sb.Append(usu.Nombre);
            }
            else
            {
                sb.Append("Todos los usuarios");
            }
            sb.Append("<br>");
            sb.Append("<b>Periodo: </b><br>");
            sb.Append(fi +" al "+fn);
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<br>");

            sb.Append(@"<table><thead><tr><td bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF""> # PEDIDO </td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF""># RECEPCION</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">USUARIO</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">FECHA</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">PROVEEDOR</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">ARTICULO</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">CANTIDAD</td></tr></thead>");




            List<recepciones> lista = (from a in dc.Proceso
                         join b in dc.ProcesoArticulo on a.ProcesoClave equals b.ProcesoClave
                         join c in dc.Articulo on b.ArticuloClave equals c.ArticuloClave
                                       where a.catTipoProcesoClave == 2 && a.catEstatusClave != 6 && b.CantidadEntregada > 0 && a.ProcesoClaveRespuesta != null && (b.ArticuloClave == articulo || b.ArticuloClave == Guid.Empty) && a.Fecha >= fi && a.Fecha <= fn
                                       select new recepciones { pedido= a.Proceso2.NumeroPedido, recep= a.NumeroPedido, usuario= a.UsuarioClave.Value, fecha= a.Fecha.ToString(), probvee=a.Proceso2.catDistribuidor.Nombre, articulo= b.Articulo.Nombre, cantidad= b.CantidadEntregada.Value }
                           ).ToList();

            int cantidad = 0;
            
            foreach (var a in lista.OrderBy(x=>x.pedido))
            {
                string nusuario = dc.Usuario.Where(x=>x.UsuarioClave==a.usuario).Select(x=>x.Nombre).First().ToString();
                cantidad = cantidad + a.cantidad;
                 string fecha = string.Format("{0:dd-MM-yyyy}", a.fecha);
                sb.Append("<tr>");
                sb.Append(@"<td style=""font-size:6px;"">" + a.pedido + "</td>");
                sb.Append(@"<td style=""font-size:6px;"">" + a.recep + "</td>");
                sb.Append(@"<td style=""font-size:6px;"">" + nusuario + "</td>");
                sb.Append(@"<td style=""font-size:6px;"">" + fecha + "</td>");
                sb.Append(@"<td style=""font-size:6px;"">" + a.probvee + "</td>");                
                sb.Append(@"<td style=""font-size:6px;"">" + a.articulo + "</td>");
                sb.Append(@"<td style=""font-size:6px;"">" + a.cantidad + "</td>");
             
                sb.Append("</tr>");
            }
            sb.Append("</tbody></table>");


            sb.Append("Cantidad de recepciones: "+lista.Count+" Total recepcionada: "+cantidad);


            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "DesgloseRecepciones.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);


                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "DesgloseRecepciones.pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "DesgloseRecepciones.pdf" + "' width=100% height=450 frameborder=0></iframe>";
                    return View();
                }
            }


        }

    }
}
