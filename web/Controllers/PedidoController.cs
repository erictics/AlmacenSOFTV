﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using LinqToExcel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using web.Controllers;
using web.Models;
using web.Models.Newsoftv;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class PedidoController : Controller
    {
        // 
        // GET: /Articulo/

        [Compress]
        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();


            Usuario u = ((Usuario)Session["u"]);


            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();


            List<catProveedor> proveedores = (from ta in dc.catProveedor
                                                  where ta.Activo
                                                  orderby ta.Nombre
                                                  select ta).ToList();


            List<catTipoMoneda> tiposmoneda = (from ta in dc.catTipoMoneda
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();

            List<catUnidad> unidades = (from ta in dc.catUnidad
                                            where ta.Activo
                                            orderby ta.Nombre
                                            select ta).ToList();

            List<catUbicacion> ubicaciones = (from ta in dc.catUbicacion
                                                  where ta.Activo
                                                  orderby ta.Nombre
                                                  select ta).ToList();


            List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                              where a.UsuarioClave == u.UsuarioClave
                                              select a).ToList();

            List<Entidad> entidades = null;

            if (u.DistribuidorClave == null)
            {

                entidades = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                 && ta.catDistribuidorClave == null
                                 && ta.Activo.Value
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {
                entidades = new List<Entidad>();

                foreach (AmbitoAlmacen a in ambito)
                {
                    Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        entidades.Add(e);
                    }
                }

                /*
                entidades = (from ta in dc.Entidad
                             where 
                                 ta.catDistribuidorClave == u.DistribuidorClave
                                 && ta.catEntidadTipoClave == 1
                                 && ta.Activo.Value
                             orderby ta.Nombre
                             select ta).ToList();       
                 */
            }

            List<Usuario> usuarios = (from us in dc.Usuario
                                          join p in dc.Permiso on us.RolClave equals p.RolClave
                                          where us.DistribuidorClave == null
                                          && p.ModuloClave == 27 && p.Ejecuta == true
                                          orderby us.Nombre
                                          select us).ToList();

            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["proveedores"] = proveedores;
            ViewData["tiposmoneda"] = tiposmoneda;
            ViewData["ubicaciones"] = ubicaciones;
            ViewData["unidades"] = unidades;

            ViewData["almacenes"] = entidades;
            ViewData["usuarios"] = usuarios;

            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");

            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        

        [Compress]
        [SessionExpireFilterAttribute]       

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<PedidoWrapper> data { get; set; }
        }

        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);           
            int pag = Convert.ToInt32(jObject["pag"].ToString());           
            Guid autorizador = Guid.Empty;        
            if (jObject["autoriza"] != null)
           {
                autorizador = ((Usuario)Session["u"]).UsuarioClave;
          }
            Guid proveedor = Guid.Empty;
             if (jObject["proveedor"] != null && jObject["proveedor"].ToString() != "-1")
             { 
                  proveedor = Guid.Parse(jObject["proveedor"].ToString());
              }

            Guid distribuidor = Guid.Empty;
            bool pedidodistribuidor = false;
            if (jObject["distribuidor"] != null && jObject["distribuidor"] != null && jObject["distribuidor"].ToString() != "-1")
            {
                distribuidor = Guid.Parse(jObject["distribuidor"].ToString());
                pedidodistribuidor = true;
            }            
            else if (jObject["distribuidor"] != null && jObject["distribuidor"].ToString() == "-1")
            {
                pedidodistribuidor = true;
            }           
            int estatus = -1;
            if (jObject["estatus"] != null)
            {
                estatus = Convert.ToInt32(jObject["estatus"].ToString());
            }
            int pedido = -1;
            if (jObject["pedido"] != null && jObject["pedido"].ToString() != String.Empty)
            {
                pedido = Convert.ToInt32(jObject["pedido"].ToString());
            }           
            int esPagare = -1;
            if (jObject["esPagare"] != null && jObject["esPagare"].ToString() != String.Empty)
            {
                esPagare = Convert.ToInt32(jObject["esPagare"].ToString());
            }            
            int pagadas = -1;
            try
            {
                if (jObject["pagadas"] != null && jObject["pagadas"].ToString() == "1")
                {
                    pagadas = 1;
                }

            }
            catch {
            }


            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData(ref recordsFiltered, start, length, pedidodistribuidor, autorizador, estatus, proveedor, distribuidor, esPagare, pedido, pagadas);
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        private List<PedidoWrapper> FilterData(ref int recordFiltered, int start, int length, bool pedidodistribuidor, Guid autorizador, int estatus, Guid proveedor, Guid distribuidor, int esPagare, int pedido, int pagadas)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);
            List<Proceso> rls = null;          
            if (u.DistribuidorClave == null)
            {
                rls = (from use in dc.Proceso 
                       where                         
                          ((use.catDistribuidorClave == null && !pedidodistribuidor) || ((use.catDistribuidorClave != null && pedidodistribuidor)))
                          && use.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus < -1 && use.catEstatusClave != -estatus) || (estatus == use.catEstatusClave))
                          && (proveedor == Guid.Empty || (proveedor == use.catProveedorClave))
                          && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                          && (esPagare == -1 || (use.PedidoDetalle != null && use.PedidoDetalle.EsPagare.HasValue && ((esPagare == 1 && use.PedidoDetalle.EsPagare.Value) || (esPagare == 0 && !use.PedidoDetalle.EsPagare.Value))))
                          && (pedido == -1 || (use.NumeroPedido == pedido))
                          && (pagadas == -1 || (pagadas == 1 && use.catEstatusClave != 12))
                       orderby use.NumeroPedido descending
                       select use).ToList();                
            }              
            else
            {
                rls = (from use in dc.Proceso
                       join b in dc.AmbitoAlmacen on use.EntidadClaveSolicitante equals b.EntidadClaveAlmacen
                       where
                         b.UsuarioClave == u.UsuarioClave &&
                          use.catDistribuidorClave == u.DistribuidorClave
                          && use.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus < -1 && use.catEstatusClave != -estatus) || (estatus == use.catEstatusClave))
                          && (pedido == -1 || (use.NumeroPedido == pedido))
                          && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                          && (esPagare == -1 || (use.PedidoDetalle != null && use.PedidoDetalle.EsPagare.HasValue && ((esPagare == 1 && use.PedidoDetalle.EsPagare.Value) || (esPagare == 0 && !use.PedidoDetalle.EsPagare.Value))))
                          && (pagadas == -1 || (pagadas == 1 && use.catEstatusClave != 12))
                       orderby use.Fecha descending
                       select use).ToList();              
            }            
            List<PedidoWrapper> rw = new List<PedidoWrapper>();
            recordFiltered = rls.Count;
            foreach (Proceso p in rls.Skip(start).Take(length).ToList())
            {
                if (p.PedidoDetalle != null)
                {
                    PedidoWrapper w = PedidoToWrapper(p);
                    rw.Add(w);
                }
            }           
            return rw;
        }

        public ActionResult ObtenUI(string data)
       {            
            PedidoWrapperUI rw = new PedidoWrapperUI();         
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid uid = Guid.Parse(data);          
            Proceso r = (from ro in dc.Proceso
                             where ro.ProcesoClave == uid
                             select ro).SingleOrDefault();
            if (r != null)
            {               
                rw = PedidoToUI(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }        

        public ActionResult Obten(string data)
        {
            PedidoWrapper rw = new PedidoWrapper();
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid uid = Guid.Parse(data);
            Proceso r = (from ro in dc.Proceso
                             where ro.ProcesoClave == uid
                             select ro).SingleOrDefault();
            if (r != null)
            {
                rw = PedidoToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Articulos(string data)
        {
            PedidoWrapper rw = new PedidoWrapper();
            ModeloAlmacen dc = new ModeloAlmacen();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            Proceso r = null;
            int recepcion = -1;
            try
            {
                recepcion = Convert.ToInt32(jObject["recepcion"].ToString());
            }
            catch { }

            Guid vgRecepcion = Guid.Empty;
            try
            {
                vgRecepcion = Guid.Parse(jObject["recepcion"].ToString());
            }
            catch { }

            Guid vgSalida = Guid.Empty;
            try
            {
                vgSalida = Guid.Parse(jObject["salida"].ToString());
            }
            catch { }
            int pedido = 0;
            Guid pedidoguid = Guid.Empty;
            try
            {
                try
                {
                    pedido = Convert.ToInt32(jObject["pedido"].ToString());

                    r = (from ro in dc.Proceso
                         where ro.NumeroPedido == pedido
                         select ro).SingleOrDefault();
                }
                catch
                {
                    pedidoguid = Guid.Parse(jObject["pedido"].ToString());

                    r = (from ro in dc.Proceso
                         where ro.ProcesoClave == pedidoguid
                         select ro).SingleOrDefault();
                }
            }
            catch
            {

            }            
            if (r != null)
            {
                if (r.catEstatusClave == 1 && r.catTipoProcesoClave != 3)
                {
                    return Json("-2: No autorizada", JsonRequestBehavior.AllowGet);
                }
                List<ProcesoArticulo> articulos = r.ProcesoArticulo.ToList();
                List<ArticuloProcesoWrapper> apws = new List<ArticuloProcesoWrapper>();
                foreach (ProcesoArticulo a in articulos)
                {
                    ArticuloProcesoWrapper apw = null;
                    if (vgRecepcion != Guid.Empty)
                    {
                        apw = PedidoToarticuloProcesoWrapper(a, vgRecepcion);
                    }
                    else if (vgSalida != Guid.Empty)
                    {
                        apw = PedidoToarticuloProcesoWrapper(a, vgSalida);
                    }
                    else
                    {
                        apw = PedidoToarticuloProcesoWrapper(a, Guid.Empty);
                    }
                    apws.Add(apw);
                }
                return Json(apws, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("-3", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Cancela(string cancela_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();
                if (cancela_id != String.Empty)
                {
                    Guid uid = Guid.Parse(cancela_id);
                    Proceso r = (from ro in dc.Proceso
                                     where ro.ProcesoClave == uid
                                     select ro).SingleOrDefault();

                    if (r != null)
                    {
                        if (r.PedidoDetalle.EsPagare.HasValue && r.PedidoDetalle.EsPagare.Value)
                        {
                            Usuario us = (Usuario)Session["u"];
                            List<ProcesoArticulo> procesosarticulos = r.ProcesoArticulo.ToList();                      

                        }

                        r.catEstatusClave = 6;
                        dc.SaveChanges();
                        return Content("1");
                    }
                }
                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();
                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    Articulo r = (from ro in dc.Articulo
                                      where ro.ArticuloClave == uid
                                      select ro).SingleOrDefault();
                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guarda(string data)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            try
            {
                //lista de proceso articulo es una lista aun vacia
                List<ProcesoArticulo> anteriores_arts = new List<ProcesoArticulo>();
                List<ProcesoArticulo> anteriores_arts2 = new List<ProcesoArticulo>();
                bool Agrega_nuevo_articulo = false;
                bool bandera = false;

                JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                // se asigna ed_id como un string vacio
                string ed_id = String.Empty;

                //si el objeto si trae clave de proceso es por que se quiere volver a guardar
                //pero con valores ya editados
                if (jObject["ProcesoClave"] != null)
                {
                    ed_id = jObject["ProcesoClave"].ToString();
                }

                //entidades del modelo

                Proceso r = new Proceso();//modelo de proceso

                //si el objeto tiene clave de proceso
                if (ed_id != String.Empty)
                {
                    Guid uid = Guid.Parse(ed_id);

                    //se busca en proceso el objeto que tenga la misma clave de proceso
                    r = (from ro in dc.Proceso
                         where ro.ProcesoClave == uid
                         select ro).SingleOrDefault();


                    //selecciona articulos que son de ese proceso y los agrega a la lista de losart  de proceso articulo
                    List<ProcesoArticulo> losarts = r.ProcesoArticulo.ToList();

                    //iguala la lista 
                    anteriores_arts = losarts.ToList();

                    anteriores_arts2 = anteriores_arts.ToList();
                    //recorre proceso articulo y elimina los articulos que son de ese proceso
                    foreach (ProcesoArticulo pa in losarts)
                    {
                        dc.ProcesoArticulo.Remove(pa);//remueve el articulo de la base
                    }

                    List<PedidoGastos> losgastos = r.PedidoGastos.ToList();
                    foreach (PedidoGastos pg in losgastos)
                    {
                        dc.PedidoGastos.Remove(pg);//remueve gastos de ese pedido
                    }
                }

                //si no tiene clave de proceso se le asigna uno
                else
                {
                    r.ProcesoClave = Guid.NewGuid();
                }
                //r={ProcesoClave:00000000-0000-0000-0000-000000000000,catTipoProcesoClave=1}
                //se asigna clave de proceso a proceso
                r.catTipoProcesoClave = 1;

                // signa propiedad 
                r.EntidadClaveSolicitante = Guid.Parse(jObject["EntidadClaveSolicitante"].ToString());


                //si catproveedorClave esta nulo o es -1
                if (jObject["catProveedorClave"].ToString() == "-1" || jObject["catProveedorClave"].ToString() == "")
                {

                    //se asigna 
                    r.EntidadClaveAfectada = Guid.Parse("00000000-0000-0000-0000-000000000001");
                    //se asigna
                    r.catProveedorClave = null;

                    //si es pagare es 1 estatus sera por autorizar 
                    if (jObject["EsPagare"].ToString() == "1")
                    {
                        r.catEstatusClave = 2;
                    }
                    //si no es pagare el estatus sera  por pagar 
                    else
                    {
                        r.catEstatusClave = 12;
                    }
                }

                //si hay clave de proveedor 
                else
                {
                    r.EntidadClaveAfectada = null;
                    r.catProveedorClave = Guid.Parse(jObject["catProveedorClave"].ToString());
                    r.catEstatusClave = 1;
                }

                try
                {
                    //se asigna a proceso
                    r.UsuarioClaveAutorizacion = Guid.Parse(jObject["UsuarioClaveAutorizacion"].ToString());
                }
                catch
                {

                }


                try
                {
                    //se asigna a usuario 
                    Usuario usu = (Usuario)Session["u"];
                    r.UsuarioClave = usu.UsuarioClave;
                }
                catch
                {

                }

                //se asigna 
                r.Fecha = DateTime.Now;
                r.Observaciones = jObject["Observaciones"].ToString();

                Guid? distribuidor = ((Usuario)Session["u"]).DistribuidorClave;
                r.catDistribuidorClave = distribuidor;

                if (ed_id == String.Empty)
                {
                    dc.Proceso.Add(r);// se agrega a la bd

                }
                dc.SaveChanges();//se guardan cambios

                int cuantos_nuevos = ((JArray)jObject["Articulos"]).Children().Count();

                //******************************************agregar pedido***************************************
                //se recorre el objeto articulo
                foreach (var item in ((JArray)jObject["Articulos"]).Children())
                {
                    //se crea objeto de proceso articulo 
                    ProcesoArticulo procesoarticulo = new ProcesoArticulo();
                    //se asigna  procesoclave
                    procesoarticulo.ProcesoClave = r.ProcesoClave;



                    var itemProperties = item.Children<JProperty>();
                    // se obtienen todos los campos del objeto articulos  y se asigna procesoarticulo.ArticuloClave
                    var articuloclave = itemProperties.FirstOrDefault(x => x.Name == "ArticuloClave");
                    procesoarticulo.ArticuloClave = Guid.Parse(articuloclave.Value.ToString());

                    // se obtienen todos los campos del objeto articulos  y se asigna procesoarticulo.ArticuloClave
                    var cantidad = itemProperties.FirstOrDefault(x => x.Name == "Cantidad");
                    procesoarticulo.Cantidad = Convert.ToInt32(cantidad.Value.ToString());

                    // se obtienen todos los campos del objeto articulos  y se asigna procesoarticulo.ArticuloClave
                    var cantidadentregada = itemProperties.FirstOrDefault(x => x.Name == "CantidadEntregada");
                    procesoarticulo.CantidadEntregada = Convert.ToInt32(cantidadentregada.Value.ToString());

                    // se obtienen todos los campos del objeto articulos  y se asigna procesoarticulo.ArticuloClave
                    var preciounitario = itemProperties.FirstOrDefault(x => x.Name == "PrecioUnitario");
                    procesoarticulo.PrecioUnitario = Convert.ToDecimal(preciounitario.Value.ToString());

                    try
                    {
                        var tipomoneda = itemProperties.FirstOrDefault(x => x.Name == "catTipoMonedaClave");
                        procesoarticulo.catTipoMonedaClave = Convert.ToInt32(tipomoneda.Value.ToString());
                    }
                    catch
                    {
                        procesoarticulo.catTipoMonedaClave = 1;
                    }

                    //se agrega a la bd el nuevo proceso
                    dc.ProcesoArticulo.Add(procesoarticulo);





                    try
                    {
                        //******************* si es pagare ********************************

                        if (jObject["catProveedorClave"].ToString() == "-1" || jObject["catProveedorClave"].ToString() == "")
                        {
                            if (jObject["EsPagare"].ToString() == "1")
                            {
                                Pagare p = (from pa in dc.Pagare
                                            where pa.ArticuloClave == procesoarticulo.ArticuloClave
                                            && pa.catDistribuidorClave == distribuidor
                                            select pa).SingleOrDefault();
                                if (p != null)
                                {
                                    if (anteriores_arts.Count > 0)
                                    {
                                        bool esta = anteriores_arts.Any(o => o.ArticuloClave == procesoarticulo.ArticuloClave);
                                        ProcesoArticulo elart = anteriores_arts.SingleOrDefault(o => o.ArticuloClave == procesoarticulo.ArticuloClave);
                                        if (cuantos_nuevos > anteriores_arts.Count)
                                        {


                                            if (esta == true)
                                            {
                                                //si la cantidad se edito
                                                if (procesoarticulo.Cantidad.Value > elart.Cantidad.Value)
                                                {
                                                    p.Utilizadas = p.Utilizadas - elart.Cantidad.Value;
                                                    p.Utilizadas = p.Utilizadas + procesoarticulo.Cantidad.Value;
                                                    p.Disponibles = p.Asignadas - p.Utilizadas;
                                                }
                                                if (procesoarticulo.Cantidad.Value == elart.Cantidad.Value)
                                                {

                                                }
                                                if (procesoarticulo.Cantidad.Value < elart.Cantidad.Value)
                                                {
                                                    int diferencia = elart.Cantidad.Value - procesoarticulo.Cantidad.Value;
                                                    p.Utilizadas = p.Utilizadas - diferencia;
                                                    p.Disponibles = p.Asignadas - p.Utilizadas;
                                                }




                                            }
                                            if (esta == false)
                                            {
                                                Agrega_nuevo_articulo = true;
                                                p.Utilizadas += procesoarticulo.Cantidad;
                                                p.Disponibles = p.Asignadas - p.Utilizadas;
                                            }


                                        }
                                        if (cuantos_nuevos == anteriores_arts.Count)
                                        {

                                            //se valida si es la misma cantidad pero diferente articulo
                                            if (esta == true)
                                            {



                                                if (procesoarticulo.Cantidad.Value > elart.Cantidad.Value)
                                                {

                                                    p.Utilizadas = p.Utilizadas - elart.Cantidad.Value;
                                                    p.Utilizadas = p.Utilizadas + procesoarticulo.Cantidad.Value;
                                                    p.Disponibles = p.Asignadas - p.Utilizadas;
                                                }
                                                if (procesoarticulo.Cantidad.Value == elart.Cantidad.Value)
                                                {

                                                }
                                                if (procesoarticulo.Cantidad.Value < elart.Cantidad.Value)
                                                {
                                                    int diferencia = elart.Cantidad.Value - procesoarticulo.Cantidad.Value;
                                                    p.Utilizadas = p.Utilizadas - diferencia;
                                                    p.Disponibles = p.Asignadas - p.Utilizadas;
                                                }

                                                anteriores_arts2.Remove(elart);


                                            }
                                            else
                                            {
                                                bandera = true;

                                                p.Utilizadas += procesoarticulo.Cantidad;
                                                p.Disponibles = p.Asignadas - p.Utilizadas;


                                            }



                                        }
                                        if (cuantos_nuevos < anteriores_arts.Count)
                                        {
                                            //bool esta = anteriores_arts.Any(o => o.ArticuloClave == procesoarticulo.ArticuloClave);                                       
                                            if (procesoarticulo.Cantidad.Value > elart.Cantidad.Value)
                                            {
                                                int diferencia = procesoarticulo.Cantidad.Value - elart.Cantidad.Value;
                                                p.Utilizadas = p.Utilizadas + diferencia;
                                                p.Disponibles = p.Asignadas - p.Utilizadas;
                                            }
                                            if (procesoarticulo.Cantidad.Value == elart.Cantidad.Value)
                                            {

                                            }
                                            if (procesoarticulo.Cantidad.Value < elart.Cantidad.Value)
                                            {
                                                int diferencia = elart.Cantidad.Value - procesoarticulo.Cantidad.Value;
                                                p.Utilizadas = p.Utilizadas - diferencia;
                                                p.Disponibles = p.Asignadas - p.Utilizadas;
                                            }

                                            anteriores_arts2.Remove(elart);



                                        }



                                        //if (anteriores_arts.Count > 0)
                                        //{
                                        //    ProcesoArticulo elart = anteriores_arts.SingleOrDefault(o => o.ArticuloClave == procesoarticulo.ArticuloClave);

                                        //    if (elart == null)
                                        //    {
                                        //        p.Utilizadas += procesoarticulo.Cantidad;
                                        //        p.Disponibles = p.Asignadas - p.Utilizadas;

                                        //    }
                                        //    p.Utilizadas += procesoarticulo.Cantidad - elart.Cantidad;
                                        //    p.Disponibles = p.Asignadas - p.Utilizadas;
                                        //}
                                        //else
                                        //{
                                        //    //p.Asignadas -= procesoarticulo.Cantidad;
                                        //    p.Utilizadas += procesoarticulo.Cantidad;
                                        //    p.Disponibles = p.Asignadas - p.Utilizadas;
                                        //}
                                    }

                                    else
                                    {
                                        p.Utilizadas += procesoarticulo.Cantidad;
                                        p.Disponibles = p.Asignadas - p.Utilizadas;

                                    }
                                }
                            }
                        }
                    }
                    catch
                    {

                    }


                }
                dc.SaveChanges();
                // se guardan cambios de procesoarticulo 
                if (anteriores_arts.Count > 0)
                {
                    if (Agrega_nuevo_articulo == true)
                    {

                    }

                    else if (cuantos_nuevos == anteriores_arts.Count)
                    {

                        if (bandera == true)
                        {
                            if (anteriores_arts2.Count > 0)
                            {
                                foreach (var a in anteriores_arts2)
                                {
                                    Pagare p = (from pa in dc.Pagare
                                                where pa.ArticuloClave == a.ArticuloClave
                                                && pa.catDistribuidorClave == distribuidor
                                                select pa).SingleOrDefault();
                                    if (p != null)
                                    {
                                        p.Utilizadas = p.Utilizadas - a.Cantidad;
                                        p.Disponibles = p.Asignadas - p.Utilizadas;

                                    }

                                }
                            }
                        }

                    }

                    else
                    {
                        if (anteriores_arts2.Count > 0)
                        {
                            foreach (var a in anteriores_arts2)
                            {
                                Pagare p = (from pa in dc.Pagare
                                            where pa.ArticuloClave == a.ArticuloClave
                                            && pa.catDistribuidorClave == distribuidor
                                            select pa).SingleOrDefault();
                                if (p != null)
                                {
                                    p.Utilizadas = p.Utilizadas - a.Cantidad;
                                    p.Disponibles = p.Asignadas - p.Utilizadas;

                                }

                            }
                        }


                    }

                }



                //***********************************GUARDA PEDIDO DETALLE*************************************************************
                //********************************************************************************************************************
                PedidoDetalle detalle = null;

                detalle = (from de in dc.PedidoDetalle
                           where de.ProcesoClave == r.ProcesoClave
                           select de).SingleOrDefault();


                if (ed_id != String.Empty)
                {
                }
                else
                {
                    detalle = new PedidoDetalle();
                    detalle.ProcesoClave = r.ProcesoClave;
                }

                detalle.IVA = Convert.ToDecimal(jObject["Iva"].ToString());
                detalle.Descuento = 0;


                //if (jObject["catProveedorClave"].ToString() == "-1" || jObject["catProveedorClave"].ToString() == "")
                //{
                if (jObject["EsPagare"] != null && jObject["EsPagare"].ToString() == "1")
                {
                    detalle.EsPagare = true;
                }
                else
                {
                    detalle.EsPagare = false;
                }
                //}


                try
                {
                    detalle.Descuento = Convert.ToDecimal(jObject["Descuento"].ToString());
                }
                catch
                { }


                detalle.TipoCambio = 0;

                try
                {
                    detalle.TipoCambio = Convert.ToDecimal(jObject["TipoCambio"].ToString());
                }
                catch
                {
                }

                detalle.DiasCredito = 0;
                try
                {
                    detalle.DiasCredito = Convert.ToInt32(jObject["DiasCredito"].ToString());
                }
                catch
                { }

                detalle.Subtotal_Total = Convert.ToDecimal(jObject["ToSubtotal"].ToString());
                detalle.Descuento_Total = Convert.ToDecimal(jObject["ToDescuento"].ToString());
                detalle.IVA_Total = Convert.ToDecimal(jObject["ToIVA"].ToString());
                detalle.Total = Convert.ToDecimal(jObject["Total"].ToString());

                detalle.Subtotal_Total_USD = Convert.ToDecimal(jObject["ToSubtotal_usd"].ToString());
                detalle.Descuento_Total_USD = Convert.ToDecimal(jObject["ToDescuento_usd"].ToString());
                detalle.IVA_Total_USD = Convert.ToDecimal(jObject["ToIVA_usd"].ToString());
                detalle.Total_USD = Convert.ToDecimal(jObject["Total_usd"].ToString());


                if (ed_id != String.Empty)
                {
                }

                // se guarda en la base de datos detallepedido 
                else
                {
                    dc.PedidoDetalle.Add(detalle);
                }

                dc.SaveChanges();


                //guardar en pedido gastos 
                foreach (var item in ((JArray)jObject["OtrosGastos"]).Children())
                {
                    try
                    {
                        PedidoGastos pedidogasto = new PedidoGastos();
                        pedidogasto.ProcesoClavePedido = r.ProcesoClave;
                        pedidogasto.GastoClave = Guid.NewGuid();

                        var itemProperties = item.Children<JProperty>();
                        var concepto = itemProperties.FirstOrDefault(x => x.Name == "Concepto");
                        pedidogasto.Concepto = concepto.Value.ToString();

                        var costo = itemProperties.FirstOrDefault(x => x.Name == "Precio");
                        pedidogasto.Costo = Convert.ToDecimal(costo.Value.ToString());

                        var moneda = itemProperties.FirstOrDefault(x => x.Name == "Moneda");
                        pedidogasto.catTipoMonedaClave = Convert.ToInt32(moneda.Value.ToString());

                        dc.PedidoGastos.Add(pedidogasto);
                    }
                    catch
                    {
                    }
                }

                //try
                //{
                //    int num = Int32.Parse(r.NumeroPedido.ToString());
                //    int Idser = Int32.Parse(jObject["Servicio"].ToString());

                //    if (Idser == 2)//si es internet
                //    {
                //        SetRel_Proceso_servicio(num, Idser);
                //    }

                //}
                //catch
                //{

                //}



                dc.SaveChanges();// se guarda en otros gastos 

                try
                {
                    if (jObject["catProveedorClave"].ToString() != "")
                    {
                        if (ed_id != String.Empty)
                        {

                            Usuario u = (from a in dc.Usuario where a.UsuarioClave == r.UsuarioClaveAutorizacion.Value select a).First();
                            string email = u.CorreoElectronico;
                          //  EnviaCorreo(r.ProcesoClave, "Se ha editado una orden de compra desde almacén central", email);
                        }
                        else
                        {
                            Usuario u = (from a in dc.Usuario where a.UsuarioClave == r.UsuarioClaveAutorizacion.Value select a).First();
                            string email = u.CorreoElectronico;
                        //    EnviaCorreo(r.ProcesoClave, "Se generó una nueva orden de compra desde almacén central", email);
                        }
                    }
                    else
                    {
                        if (r.PedidoDetalle.EsPagare == true)
                        {
                            Entidad almacen = (from a in dc.Entidad where a.EntidadClave == r.EntidadClaveSolicitante select a).First();
                          //  EnviaCorreo(r.ProcesoClave, "Se generó una nueva orden de compra PAGARE  desde " + almacen.Nombre, "todos");
                        }
                        else
                        {
                            Entidad almacen = (from a in dc.Entidad where a.EntidadClave == r.EntidadClaveSolicitante select a).First();
                           // EnviaCorreo(r.ProcesoClave, "Se generó una nueva orden de compra VENTA desde " + almacen.Nombre, "todos");
                        }


                    }
                }
                catch { }


                return Content(r.ProcesoClave.ToString());
            }
            catch (Exception ex)
            {
                return Content("0");
            }
        }

        private ArticuloProcesoWrapper PedidoToarticuloProcesoWrapper(ProcesoArticulo u, Guid pProcesoP)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            ArticuloProcesoWrapper ap = new ArticuloProcesoWrapper();
            ap.Clave = u.ProcesoClave + "|" + u.ArticuloClave;
            ap.ArticuloClave = u.ArticuloClave.ToString();
            ap.Nombre = u.Articulo.Nombre;
            ap.Cantidad = (u.Cantidad == null) ? 0 : u.Cantidad.Value;
            ap.PrecioUnitario = (u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value;
            ap.PrecioUnitarioTexto = ((u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value).ToString("C");
            Proceso Pedido = u.Proceso;
            ap.Surtida = 0;
            if (Pedido.catTipoProcesoClave == 1)
            {

                List<Proceso> salidas = (from s in dc.Proceso
                                             where s.ProcesoClaveRespuesta == Pedido.ProcesoClave
                                             && s.catTipoProcesoClave == 3 && s.catEstatusClave != 6
                                             select s).ToList();
                int cantidad = 0;
                foreach (Proceso s in salidas)
                {
                    List<ProcesoArticulo> proarts = s.ProcesoArticulo.ToList();

                    foreach (ProcesoArticulo pa in proarts)
                    {
                        if (pa.ArticuloClave == u.ArticuloClave)
                        {
                            cantidad += pa.Cantidad.Value;
                        }
                    }

                }

                ap.Surtida = cantidad;
            }
            ap.catSeriesClaves = new List<catSerieWrapper>();
            List<web.Models.SerieTipoArticulo> tiposarticulo = u.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();
            foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
            {
                catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                ap.catSeriesClaves.Add(csw);
            }
            ap.TieneSeries = tiposarticulo.Count();
            ap.Seriales = new List<SerialesWrapper>();
            int cuantos = (from c in dc.Inventario
                           where c.ArticuloClave == u.ArticuloClave
                           && c.EntidadClave == Pedido.EntidadClaveAfectada
                           && c.EstatusInv == 7
                           select c.Cantidad.Value).ToList().Sum();
            ap.Existencia = cuantos;
            int standby = (from c in dc.Inventario
                           where c.ArticuloClave == u.ArticuloClave
                           && c.EntidadClave == Pedido.EntidadClaveAfectada
                           && c.EstatusInv == 7
                           select c.Standby.Value).ToList().Sum();

            ap.Standby = standby;
            if (ap.Surtida == 0)
            {
                try
                {
                    ap.Surtida = u.CantidadEntregada.Value;
                    ap.Surtiendo = 0;
                }
                catch
                {
                    ap.Surtida = 0;
                    ap.Surtiendo = 0;
                }
            }
            List<ProcesoInventario> proinv = (from i in dc.ProcesoInventario
                                                  where i.ProcesoClave == pProcesoP
                                                  && i.Inventario.ArticuloClave == u.ArticuloClave
                                                  select i).ToList();
            foreach (ProcesoInventario pro in proinv)
            {
                if (ap.Surtida == 0)
                {
                    ap.Surtida = pro.Cantidad.Value;
                }
                ap.Surtiendo += pro.Cantidad.Value;
                Inventario inventario = pro.Inventario;
                List<Serie> series = inventario.Serie.ToList();
                SerialesWrapper sw = new SerialesWrapper();
                sw.RecepcionClave = pro.ProcesoClave.ToString();
                sw.InventarioClave = inventario.InventarioClave.ToString();
                sw.ArticuloClave = inventario.ArticuloClave.ToString();
                sw.EstatusTexto = "Buen Estado";
                sw.EstatusClave = inventario.Estatus.ToString();
                if (inventario.Cantidad > 0)
                {
                    sw.Activo = 1;
                }
                else
                {
                    sw.Activo = 0;
                }
                sw.datos = new List<SerialesDatosWrapper>();
                foreach (Serie s in series)
                {
                    SerialesDatosWrapper sdw = new SerialesDatosWrapper();
                    sdw.Clave = s.catSerieClave.ToString();
                    sdw.Valor = s.Valor.ToString();
                    sw.datos.Add(sdw);
                }
                ap.Seriales.Add(sw);
            }
            return ap;
        }



        List<catEstatus> _estatus = null;
        private string DameEstatus(int estatusId)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            if (_estatus == null)
            {
                _estatus = (from e in dc.catEstatus
                            orderby e.catEstatusClave
                            select e).ToList();
            }

            catEstatus est = _estatus.Where(o => o.catEstatusClave == estatusId).SingleOrDefault();
            if (est != null)
            {
                return est.Nombre;
            }
            else
            {
                return String.Empty;
            }
        }

        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            return rw;
        }    
        private PedidoWrapperUI PedidoToUI(Proceso u)
        {
           
            PedidoWrapperUI pwui = new PedidoWrapperUI();
            pwui.ProcesoClave = u.ProcesoClave.ToString();
            pwui.catProveedorClave = u.catProveedorClave.ToString();
            pwui.EntidadClaveSolicitante = u.EntidadClaveSolicitante.ToString();
            pwui.UsuarioClaveAutorizacion = u.UsuarioClaveAutorizacion.ToString();
            pwui.Observaciones = u.Observaciones;           
            PedidoDetalle pd = u.PedidoDetalle;            
            pwui.TipoCambio = pd.TipoCambio.Value.ToString();
            pwui.Iva = pd.IVA.Value.ToString();
            pwui.Descuento = pd.Descuento.Value.ToString();
            pwui.DiasCredito = pd.DiasCredito.Value.ToString();
            pwui.ToSubtotal = pd.Subtotal_Total.Value.ToString();
            pwui.ToDescuento = pd.Descuento_Total.Value.ToString();
            pwui.ToIVA = pd.IVA_Total.Value.ToString();
            pwui.Total = pd.Total.Value.ToString();
            pwui.ToSubtotal_usd = pd.Subtotal_Total_USD.Value.ToString();
            pwui.ToDescuento_usd = pd.Descuento_Total_USD.Value.ToString();
            pwui.ToIVA_usd = pd.IVA_Total_USD.Value.ToString();
            pwui.Total_usd = pd.Total_USD.Value.ToString();            
            if (u.PedidoDetalle.EsPagare.HasValue && u.PedidoDetalle.EsPagare.Value)
            {
                pwui.EsPagare = "1";
            }
            else
            {
                pwui.EsPagare = "0";
            }
            pwui.OtrosGastos = new List<OtroGastoUI>();
            List<PedidoGastos> otrosgastos = u.PedidoGastos.ToList();
            foreach (PedidoGastos pg in otrosgastos)
            {
                OtroGastoUI ogui = new OtroGastoUI();
                ogui.Concepto = pg.Concepto;
                ogui.Moneda = pg.catTipoMonedaClave.ToString();
                ogui.Precio = pg.Costo.ToString();
                pwui.OtrosGastos.Add(ogui);
            }
            pwui.Articulos = new List<ArticuloUI>();
            List<ProcesoArticulo> proarts = u.ProcesoArticulo.ToList();
            foreach (ProcesoArticulo pa in proarts)
            {
                Articulo art = pa.Articulo;
                ArticuloUI arui = new ArticuloUI();
                arui.ArticuloClasificacionClave = art.catTipoArticulo.catClasificacionArticuloClave.ToString();
                arui.ArticuloTipoClave = art.catTipoArticulo.catTipoArticuloClave.ToString();
                arui.ArticuloClave = art.ArticuloClave.ToString();
                arui.ArticuloTexto = art.Nombre;
                arui.Cantidad = pa.Cantidad.ToString();
                if (pa.CantidadEntregada != null)
                {
                    arui.CantidadEntregada = pa.CantidadEntregada.ToString();
                }
                else
                {
                    pa.CantidadEntregada = 0;
                }
                arui.PrecioUnitario = pa.PrecioUnitario.ToString();
                arui.catTipoMonedaClave = pa.catTipoMonedaClave.ToString();
                arui.MonedaTexto = pa.catTipoMoneda.Descripcion;

                pwui.Articulos.Add(arui);
            }
            return pwui;
        }
        
       
        private PedidoWrapper PedidoToWrapper(Proceso u)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            PedidoDetalle pd = u.PedidoDetalle;
            PedidoWrapper rw = new PedidoWrapper();
            rw.NumeroPedido = u.NumeroPedido.ToString();
            try
            {
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            catch
            {
                rw.Pedido = u.NumeroPedido.ToString();
            }

            rw.Clave = u.ProcesoClave.ToString();
            rw.EntidadClaveAfectada = u.EntidadClaveAfectada.ToString();
            rw.EntidadClaveSolicitante = u.EntidadClaveSolicitante.ToString();
            if (u.PedidoDetalle.EsPagare.HasValue && u.PedidoDetalle.EsPagare.Value)
            {
                rw.EsPagare = "1";
            }
            else
            {
                rw.EsPagare = "0";
            }
            rw.TieneSalidas = "0";            
            int salidas = (from s in dc.Proceso
                           where s.ProcesoClaveRespuesta == u.ProcesoClave
                           && s.catTipoProcesoClave == 3
                           && s.catEstatusClave != 6
                           select s).Count();
            rw.TieneSalidas = salidas.ToString();
            try
            {
                catProveedor pro = u.catProveedor;
                rw.Origen = pro.Nombre;
            }
            catch
            {
                rw.Origen = "";
            }
            try
            {
                rw.Distribuidor = u.catDistribuidor.Nombre;
            }
            catch
            {
                rw.Distribuidor = "";
            }
            try
            {
                Entidad alm = u.Entidad;
                rw.Destino = alm.Nombre;
            }
            catch
            {
                rw.Destino = "";
            }
            catEstatus estado = u.catEstatus;
            rw.Estatus = estado.Nombre;
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");
            rw.Total = pd.Total.Value.ToString("C");
            rw.Total_USD = pd.Total_USD.Value.ToString("C");
            rw.FacturacionClave = u.catEstatusFacturacionClave.ToString();
            int numped = Int32.Parse(rw.NumeroPedido.ToString());
            if (u.catEstatusFacturacionClave != null)
            {
                rw.Facturacion = DameEstatus(u.catEstatusFacturacionClave.Value);
            }
            else
            {
                rw.Facturacion = String.Empty;
            }
            return rw;
        }
       
        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();           
            if (Request["p"] == null)
            {
                return Content("");
            }
            Guid clave = Guid.Parse(Request["p"]);

            int pdf = Convert.ToInt32(Request["pdf"]);            
            List<catTipoMoneda> monedas = (from m in dc.catTipoMoneda
                                               orderby m.Descripcion
                                               select m).ToList();          
            Proceso pro = (from p in dc.Proceso
                               where p.ProcesoClave == clave
                               select p).SingleOrDefault();
            Entidad e = pro.Entidad; 
            Usuario u = pro.Usuario; 
            Usuario elaboro = pro.Usuario1;
            StringBuilder sb = new StringBuilder();
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append(@"<table cellpadding=""2"" style=""width: 100%; border: 1px solid black;"" cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append(@"<h2><b>" + empresa + "</b></h2><br>");          
                int numped=Int32.Parse(pro.NumeroPedido.ToString());
                sb.Append("<h3>Orden de Compra: #" + pro.NumeroPedido + "</h3>");
                string servicio = "";
                try
                {
                    servicio = (from a in dc.Tbl_proceso_Internet
                                join b in dc.CatTipoServicios on a.idServicio equals b.IdServicio
                                where a.NumeroPedido == pro.NumeroPedido
                                select b.Descripcion).First();
                }
                catch
                { }             
                //sb.Append("<h3><b>Material de "+ servicio + "</b></h3><br>");           
                sb.Append("<h4>" + pro.catEstatus.Nombre + "</h4><br>");
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</tr>");
            sb.Append("<tr><td></td><tr>");
            sb.Append("</table>");          
            sb.Append(@"<table class='red' border='1' bordercolor='red' cellpadding=0 cellspacing=0><tr><td>&nbsp;</td></tr></table>");          
            sb.Append("<br>");           
            NewSoftvModel ns = new NewSoftvModel();            
            if (pro.catDistribuidor != null)
            {               
                catDistribuidor datos_distribuidor = (from a in dc.catDistribuidor where a.catDistribuidorClave == pro.catDistribuidorClave select a).FirstOrDefault();

                Entidad almacen = (from j in dc.Entidad where j.EntidadClave == pro.EntidadClaveSolicitante select j).First();
               // companias comp = (from b in ns.companias where b.id_compania == almacen.idEntidadSofTV select b).First();
              //  int clave_plaza = (from f in dc.Entidad where f.EntidadClave == pro.EntidadClaveSolicitante select f.idEntidadSofTV).FirstOrDefault();
              //  companias compania = (from r in ns.companias where r.id_compania == clave_plaza select r).FirstOrDefault();
                AlmacenDireccionPlaza direccion_plaza = new AlmacenDireccionPlaza();
                try
                {
                   // direccion_plaza = ns.AlmacenDireccionPlaza.Where(x => x.idcompania == compania.id_compania).First();

                }
                catch
                { }
                         
               
                sb.Append(@"<table  style=""""  border=""0"" width=""400"" cellspacing=""0"" cellpadding=""0"">");
                sb.Append("<tr >");
                sb.Append(@"<td width=""50"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Distribuidor:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""300"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + datos_distribuidor.Nombre.ToString() + "</h5>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append(@"<td width=""50"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Direccion Fiscal:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""200"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + datos_distribuidor.Domicilio.CalleNumero + " COLONIA:" + datos_distribuidor.Domicilio.Colonia + "," + datos_distribuidor.Domicilio.Ciudad + ", C.P" + datos_distribuidor.Domicilio.CodigoPostal + "");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append(@"<td width=""50"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Almacén:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""200"">");
               //sb.Append(@"<h5 style=""font-size:9px;"">" + compania.razon_social + "</h5>");
                sb.Append(@"<h5 style=""font-size:9px;""></h5>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append(@"<td width=""40"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Responsable de almacén:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""200"">");
               //sb.Append(@"<h5 style=""font-size:9px;"">" + compania.contacto.ToString() + "");
                sb.Append(@"<h5 style=""font-size:9px;"">");
                sb.Append("</td>");
                sb.Append("</tr>");


                sb.Append("<tr>");
                sb.Append(@"<td width=""50"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Direccion de almacén:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""200"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + direccion_plaza.calle + " " + direccion_plaza.numero_exterior + " COLONIA " + direccion_plaza.colonia + "," + direccion_plaza.localidad + "," + direccion_plaza.estado + ". CP" + direccion_plaza.codigo_postal + "</h5>");

                sb.Append("</td>");
                sb.Append("</tr>");

                if (pro.catProveedor != null)
                {
                    sb.Append("<tr>");
                    sb.Append(@"<td width=""50"">");
                    sb.Append(@"<h5 style=""font-size:9px;""><b>Proveedor:</b></h5>");
                    sb.Append("</td>");
                    sb.Append(@"<td width=""200"">");
                    sb.Append(pro.catProveedor.Nombre);

                    sb.Append("</td>");
                    sb.Append("</tr>");


                }
              

                if (elaboro != null)
                {
                    sb.Append("<tr>");
                    sb.Append(@"<td width=""50"">");
                    sb.Append(@"<h5 style=""font-size:9px;""><b>Elaborado por:</b></h5>");
                    sb.Append("</td>");
                    sb.Append(@"<td width=""200"">");
                    sb.Append(@"<h5 style=""font-size:9px;"">" + elaboro.Nombre + "</h5>");

                    sb.Append("</td>");
                    sb.Append("</tr>");


                }
                else
                { }
              
                if (u != null)
                {
                    sb.Append("<b>Autorizó</b>&nbsp;");
                    sb.Append(u.Nombre);
                    sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                }
                else
                {}
                sb.Append("</table>");              
            }                
            else
            {


                GeneralPrincipal gen = (from s in ns.GeneralPrincipal select s).FirstOrDefault();

                sb.Append(@"<table style=""""  border=""0"" width=""400"" cellspacing=""0"" cellpadding=""0"">");
                sb.Append("<tr>");
                sb.Append(@"<td width=""100"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Proveedor :</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""300"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + pro.catProveedor.Nombre + "</h5>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr >");
                sb.Append("<tr >");
                sb.Append(@"<td width=""100"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Contacto :</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""300"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + pro.catProveedor.NombreContacto + "</h5>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr >");
                sb.Append(@"<td width=""100"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Dirección del proveedor:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""300"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + pro.catProveedor.Domicilio.CalleNumero + " " + pro.catProveedor.Domicilio.Colonia + " " + pro.catProveedor.Domicilio.Ciudad + "</h5>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append(@"<td width=""100"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Almacén:</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""300"">");
                sb.Append(@"<h5 style=""font-size:9px;"">" + gen.nombre + "</h5><br>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append(@"<td width=""100"">");
                sb.Append(@"<h5 style=""font-size:9px;""><b>Dirección de almacen</b></h5>");
                sb.Append("</td>");
                sb.Append(@"<td width=""300"">");              
                sb.Append(@"<h5 style=""font-size:9px;"">" + gen.direccion + " " + gen.numExt + "," + gen.colonia + " " + gen.ciudad + " " + gen.estado + " " + gen.CP + "</h5>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
            }

            PedidoDetalle pedido = pro.PedidoDetalle;
            sb.Append(@"<b style=""font-size:8px;"">Artículos:</b><br><br>");
            //sb.Append(@"<table cellpadding=""1""  cellspacing=""0"" bgcolor=""#EFEFEF""><tr></td>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" style=""font-size:9px;"" align=""center"">");
            sb.Append("Cantidad");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" style=""font-size:9px;"" colspan=""3"">");
            sb.Append("Concepto");
            sb.Append("</th>");
            if (pro.PedidoDetalle.EsPagare.Value == false)
            {

                sb.Append(@"<th bgcolor=""#424242"" style=""font-size:9px;"" color=""#FFFFFF"" align=""center"">");
                sb.Append("Precio Unitario");
                sb.Append("</th>");
                sb.Append(@"<th bgcolor=""#424242""  style=""font-size:9px;"" color=""#FFFFFF"" align=""center"">");
                sb.Append("Importe");
                sb.Append("</th>");
                sb.Append(@"<th bgcolor=""#424242"" style=""font-size:9px;"" color=""#FFFFFF"" align=""center"">");
                sb.Append("Importe USD");
                sb.Append("</th>");
                sb.Append("</tr>");
            }
            sb.Append("</thead>");
            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();
            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                Articulo a = pa.Articulo;
                sb.Append("<tr>");
                sb.Append(@"<td align=""center"" style=""font-size:9px;"" align=""center"">");
                sb.Append(pa.Cantidad);
                sb.Append("</td>");
                sb.Append(@"<td colspan=""3"" style=""font-size:9px;"">");
                sb.Append(a.Nombre);
                sb.Append("</td>");
                if (pro.PedidoDetalle.EsPagare.Value == false)
                {
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"" align=""center"">");
                    sb.Append(pa.PrecioUnitario.Value.ToString("C"));
                    sb.Append("</td>");
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"" align=""center"">");

                    if (pa.catTipoMonedaClave == 4)
                    {
                        decimal importe = pa.PrecioUnitario.Value * pa.Cantidad.Value;
                        sb.Append("$" + importe.ToString());
                    }
                    else
                    {
                        sb.Append(Convert.ToDecimal(0).ToString("C"));
                    }


                    sb.Append("</td>");


                    if (pedido.EsPagare.HasValue && !pedido.EsPagare.Value)
                    {
                        sb.Append(@"<td align=""right"" style=""font-size:9px;"" align=""center"">");


                        catTipoMoneda m = (from mo in monedas
                                               where mo.catTipoMonedaClave == pa.catTipoMonedaClave
                                               select mo).SingleOrDefault();

                        if (pa.catTipoMonedaClave == 5)
                        {
                            decimal importeusd = pa.PrecioUnitario.Value * pa.Cantidad.Value;
                            sb.Append(importeusd.ToString("C"));
                        }
                        else
                        {
                            sb.Append(Convert.ToDecimal(0).ToString("C"));
                        }
                        sb.Append("</td>");
                    }
                    sb.Append("</tr>");
                }
            }

            sb.Append("</table>");
            //sb.Append("</td></tr></table>");
            sb.Append("</tbody>");

            if (pdf == 1 || pdf == 2)
            {
                sb.Append("<br>");
            }

            sb.Append(@"<b style=""font-size:9px;"">Otros Gastos: </b><br>");
            List<PedidoGastos> gtos = pro.PedidoGastos.ToList();
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");

            foreach (PedidoGastos g in gtos)
            {

                sb.Append("<tr>");
                sb.Append(@"<td>");
                sb.Append("<br>");
                sb.Append("</td>");
                sb.Append(@"<td colspan=""4"" style=""font-size:9px;"">");
                sb.Append(g.Concepto);
                sb.Append("</td>");

                if (pro.PedidoDetalle.EsPagare.Value == false)
                {
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"">");

                    if (g.catTipoMonedaClave == 4)
                    {

                        decimal importe = g.Costo.Value;
                        sb.Append(importe.ToString("C"));
                    }
                    else
                    {
                        sb.Append(Convert.ToDecimal(0).ToString("C"));
                    }

                    sb.Append("</td>");

                    sb.Append(@"<td align=""right"" style=""font-size:9px;"">");

                    catTipoMoneda m = (from mo in monedas
                                           where mo.catTipoMonedaClave == g.catTipoMonedaClave
                                           select mo).SingleOrDefault();


                    if (g.catTipoMonedaClave == 5)
                    {

                        decimal importe = g.Costo.Value;
                        sb.Append(importe.ToString("C"));
                    }
                    else
                    {
                        sb.Append(Convert.ToDecimal(0).ToString("C"));
                    }

                    sb.Append("</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");


            if (pro.PedidoDetalle.EsPagare.Value == false)
            {

                sb.Append(@"<b style=""font-size:9px;"">Total</b><br>");
                sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"">");
                sb.Append(@"<b style=""font-size:9px;"">Subtotal</b>");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                sb.Append(pedido.Subtotal_Total.Value.ToString("C"));
                sb.Append("</td>");

                if (pedido.EsPagare.HasValue && !pedido.EsPagare.Value)
                {
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                    sb.Append(pedido.Subtotal_Total_USD.Value.ToString("C"));
                    sb.Append("</td>");
                }
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"" style=""font-size:9px;"">");
                sb.Append("<b>Descuento</b>");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"" style=""font-size:9px;"" >");
                sb.Append(pedido.Descuento_Total.Value.ToString("C"));
                sb.Append("</td>");

                if (pedido.EsPagare.HasValue && !pedido.EsPagare.Value)
                {
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                    sb.Append(pedido.Descuento_Total_USD.Value.ToString("C"));
                    sb.Append("</td>");
                }
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"" style=""font-size:9px;"">");
                sb.Append("<b>IVA</b>");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                sb.Append(pedido.IVA_Total.Value.ToString("C"));
                sb.Append("</td>");

                if (pedido.EsPagare.HasValue && !pedido.EsPagare.Value)
                {
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                    sb.Append(pedido.IVA_Total_USD.Value.ToString("C"));
                    sb.Append("</td>");
                }

                sb.Append("</tr>");


                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"" style=""font-size:9px;"">");
                sb.Append("<b>Total<b>");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                sb.Append(pedido.Total.Value.ToString("C"));
                sb.Append("</td>");

                if (pedido.EsPagare.HasValue && !pedido.EsPagare.Value)
                {
                    sb.Append(@"<td align=""right"" style=""font-size:9px;"">");
                    sb.Append(pedido.Total_USD.Value.ToString("C"));
                    sb.Append("</td>");
                }
                sb.Append("</tr>");

                sb.Append("</table>");

            }
            sb.Append(@"<h5 style=""font-size:9px;""><b>Autorización: </b></h5><br><br>");
            sb.Append(@"<table border=0 ><tr><td><h5 style=""font-size:9px;""><b></b></h5></td><td><h5 style=""font-size:9px;""><b></b></h5></td><td><h5 style=""font-size:9px;""><b></b></h5></td><td><h5 style=""font-size:9px;""><b></b></h5></td></tr></table>");
            sb.Append(@"<table border=0 ><tr><td><center><h5 style=""font-size:9px;"">Director comercial</h5></center></td><td><center><h5 style=""font-size:9px;"">Directora financiera</h5></center></td><td><center><h5 style=""font-size:9px;"">Director administrativo</h5></center></td><td><center><h5 style=""font-size:9px;"">Director general</h5></td></center></tr></table>");
            sb.Append("<br>");
            sb.Append(@"<h5 style=""font-size:9px;""><b>Comentarios: </b></h5><br>");
            sb.Append(@"<h5 style=""font-size:9px;"">" + pro.Observaciones + "</h5>");



            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "pedido.pdf";
                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);
                
                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Pedido" + pro.NumeroPedido.ToString() + ".pdf");
                }               
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "pedido.pdf" + "'  style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }
        }
        
        

        public ActionResult PedidosAutorizador(string data, int draw, int start, int length)
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int estatus = -1;
            if (jObject["estatus"] != null)
            {
                estatus = Convert.ToInt32(jObject["estatus"].ToString());
            }
            int pedido = -1;
            if (jObject["pedido"] != null && jObject["pedido"].ToString() != String.Empty)
            {
                pedido = Convert.ToInt32(jObject["pedido"].ToString());
            }
            Guid proveedor = Guid.Empty;
            if (jObject["proveedor"] != null && jObject["proveedor"].ToString() != "-1")
            {
                proveedor = Guid.Parse(jObject["proveedor"].ToString());
            }

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FiltraAutorizador(start, length, estatus, proveedor, pedido).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = dataTableData.data.Count;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);


        }

        public List<PedidoWrapper> FiltraAutorizador(int start, int length, int estatus, Guid proveedor, int pedido)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);

            List<Proceso> listap = (from a in dc.Proceso
                                  
                                    where a.catTipoProcesoClave == 1 && (a.catEstatusClave == 4 || a.catEstatusClave == 1 || a.catEstatusClave == 2) && a.UsuarioClaveAutorizacion == u.UsuarioClave
                                        && (pedido == -1 || (a.NumeroPedido == pedido)) && (estatus == -1 || (estatus < -1 && a.catEstatusClave != -estatus) || (estatus == a.catEstatusClave)) && (proveedor == Guid.Empty || (proveedor == a.catProveedorClave))
                                    orderby a.NumeroPedido descending
                                    select a).ToList();
            List<PedidoWrapper> rw = new List<PedidoWrapper>();

            foreach (Proceso p in listap)
            {
                if (p.PedidoDetalle != null)
                {
                    PedidoWrapper w = PedidoToWrapper(p);
                    rw.Add(w);
                }
            }
            return rw;
        }






    }
}
