﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using web.Utils;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Web.Controllers;
using web.Models;

namespace web.Controllers
{
    public class ReporteSaldoTecnicoController : Controller
    {
        //
        // GET: /ReporteSaldoTecnico/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);
            List<Entidad> lista_tecnicos = (from a in dc.Entidad
                                            join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveTecnico
                                            where  a.catDistribuidorClave ==u.DistribuidorClave   && a.catEntidadTipoClave == 2
                                            select a).ToList();


            var tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == u.DistribuidorClave
                          select a).ToList();

            lista_tecnicos.AddRange(tecgen);
            ViewData["tecnicos"] = lista_tecnicos.OrderBy(p=>p.Nombre).ToList();
            return View();
        }



        public ActionResult getTipoArticulo()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var lista = dc.catTipoArticulo.Where(x => x.Activo == true && x.catSerieClaveInterface!=null).Select(o => new { o.Descripcion, o.catTipoArticuloClave }).ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getArticulos(int id_categoria)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var lista = dc.Articulo.Where(x => x.catTipoArticuloClave == id_categoria).Select(o => new { o.Nombre, o.ArticuloClave }).ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detalle()
        {
            int pdf = Convert.ToInt32(Request["pdf"]);


            Guid tecnico = Guid.Empty;
            try
            {
                tecnico = new Guid(Request["tecnico"].ToString());
            }
            catch
            {}

            Guid articulo = Guid.Empty;
            try
            {
                articulo = new Guid(Request["articulo"].ToString());
            }
            catch
            { }
            ModeloAlmacen dc = new ModeloAlmacen();
            List<reporte_lista> lista = new List<reporte_lista>();            
            StringBuilder sb = new StringBuilder();
            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h4><b>APARATOS DE TÉCNICO</b></h4>");
            // sb.Append("<b>Periodo del:</b> " + fechaInicio + "<b> al: </b> " + fechaFin + "<br>");
            if (articulo == Guid.Empty)
            {
                sb.Append("<b>Artículos:</b>Todos los artículos <br>");
            }
            else
            {
                Articulo art = dc.Articulo.Where(o => o.ArticuloClave == articulo).First();
                sb.Append("<b>Artículos:</b>" + art.Nombre + " <br>");
            }
            sb.Append("<b>Fecha: " + DateTime.Now.ToShortDateString() + "</b><br>");


            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=90 height=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            List<Articulo> articuloslista = (from aa in dc.Articulo where articulo == Guid.Empty 
                                             || articulo == aa.ArticuloClave orderby aa.Nombre
                                             select aa).ToList();

            List<Entidad> tecnicoslista = (from aa in dc.Entidad
                                             where tecnico == Guid.Empty
                                              || tecnico == aa.EntidadClave
                                             orderby aa.Nombre
                                             select aa).ToList();
            tecnicoslista.ForEach(tec  => {

                List<Serie> seriest = (from s in dc.Serie
                                      where s.Inventario.EntidadClave == tec.EntidadClave                                      
                                      select s).ToList();
                if(seriest.Count > 0)
                {
                    //sb.Append("<br>");
                    sb.Append(@"<b style=""font-size:9px"" >Técnico: </b><span style=""font-size:8px"">" + tec.Nombre+"</span>");
                    sb.Append("<br>");
                   // sb.Append("_____________________________________");
                    sb.Append("<br>");

                    articuloslista.ForEach(x=> {
                 
                    List<Serie> series = (from s in dc.Serie
                                          where s.Inventario.EntidadClave == tec.EntidadClave
                                          && s.Inventario.ArticuloClave == x.ArticuloClave
                                          select s).ToList();

                    if(series.Count > 0)
                    {
                        

                        sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                        sb.Append("<tr>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Clasificación:</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Tipo de Artículo:</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Artículo:</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"" >Serie</td>");
                        sb.Append("</tr>");

                        series.ForEach(se =>
                        {
                            sb.Append("<tr>");
                            sb.Append(@"<td style=""font-size:8px;"">" + se.Inventario.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion+"</td>");
                            sb.Append(@"<td style=""font-size:8px;"">" + se.Inventario.Articulo.catTipoArticulo.Descripcion+"</td>");
                            sb.Append(@"<td style=""font-size:8px;"">" + se.Inventario.Articulo.Nombre+"</td>");
                            sb.Append(@"<td style=""font-size:8px;"">" + se.Valor+"</td>");
                            sb.Append("</tr>");

                        });

                            sb.Append("<tr>");
                            sb.Append(@"<td colspan=3></td>");                            
                            sb.Append(@"<td style=""font-size:8px;""> <b>Total:</b> " + series.Count + "</td>");
                            sb.Append("</tr>");

                            sb.Append(@"</table>");
                    }
                    

                });
                }
                
            });

            


            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + ".pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);


                if (pdf == 1)
                {
                    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + ".pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";

                    return View();
                }
                else
                {
                    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + ".pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";

                    return View();
                }
            }
        }


        //public ActionResult getAlamcen(System.Guid id_distribuidor)
        //{
        //    ModeloAlmacen dc = new ModeloAlmacen();
        //    var lista = dc.Entidad.Where(x => x.catDistribuidorClave == id_distribuidor && x.catEntidadTipoClave == 1).Select(o => new { o.EntidadClave, o.Nombre }).ToList();
        //    return Json(lista.OrderBy(x => x.Nombre), JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult getTecnicos(System.Guid id_almacen)
        //{
        //    ModeloAlmacen dc = new ModeloAlmacen();
        //    var lista = (from a in dc.Entidad join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveTecnico where b.EntidadClaveAlmacen == id_almacen select new { a.Nombre, a.EntidadClave }).ToList();

        //    Entidad almacen = dc.Entidad.Where(x => x.EntidadClave == id_almacen).First();
        //    var tecgen = (from a in dc.Entidad
        //                  join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
        //                  where b.DistribuidorClave == almacen.catDistribuidorClave
        //                  select new { a.Nombre, a.EntidadClave }).ToList();

        //    lista.AddRange(tecgen);

        //    return Json(lista.OrderBy(x => x.Nombre), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult getTecnicos2(int id_almacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Entidad almacen = dc.Entidad.Where(x => x.idEntidadSofTV == id_almacen).First();
            var lista = (from a in dc.Entidad join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveTecnico where b.EntidadClaveAlmacen == almacen.EntidadClave select new { a.Nombre, a.EntidadClave }).ToList();


            var tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == almacen.catDistribuidorClave
                          select new { a.Nombre, a.EntidadClave }).ToList();

            lista.AddRange(tecgen);

            return Json(lista.OrderBy(x => x.Nombre), JsonRequestBehavior.AllowGet);
        }






        public class obj_tecnicos{
             public Guid id_tecnico { get; set; }
         }
         public class reporte_lista
         {
             public Guid InventarioClave { get; set; }
             public string Nombre { get; set; }
             public int? Cantidad { get; set; }
         }
    }
}
