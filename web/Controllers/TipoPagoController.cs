﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Controllers;

namespace web.Controllers
{
    public class TipoPagoController : Controller
    {

        ModeloAlmacen dc = new ModeloAlmacen();
        //
        // GET: /CatTipoPago/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }





        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<CatTipoPago> data { get; set; }
        }

        public ActionResult ObtenLista(int id, int draw, int start, int length)
        {
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = dc.CatTipoPago.ToList();
            dataTableData.recordsFiltered = ListaTipoPago().Count;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public List<CatTipoPago> ListaTipoPago()
        {
            return dc.CatTipoPago.ToList();
        }

        public ActionResult AgregaTipoPago(CatTipoPago obj)
        {
            try
            {
                CatTipoPago t = new CatTipoPago();
                t.Idpago = obj.Idpago;
                t.Descripcion = obj.Descripcion;
                t.Cuenta = obj.Cuenta;
                t.Activo = obj.Activo;
                dc.CatTipoPago.Add(t);
                dc.SaveChanges();
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Content("0");
            }


        }

        public ActionResult ObtenTipoPago(int id)
        {
            CatTipoPago t = new CatTipoPago();
            t = dc.CatTipoPago.Where(t1 => t1.Idpago == id).First();
            return Json(t, JsonRequestBehavior.AllowGet);

        }


        public ActionResult EditaTipoPago(CatTipoPago obj)
        {
            try
            {
                CatTipoPago t = new CatTipoPago();
                t = dc.CatTipoPago.Where(t1 => t1.Idpago == obj.Idpago).First();
                t.Descripcion = obj.Descripcion;
                t.Cuenta = obj.Cuenta;
                t.Activo = obj.Activo;
                dc.SaveChanges();

                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Content("0");
            }


        }



    }
}
