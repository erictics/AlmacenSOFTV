﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using Web.Controllers;

namespace web.Controllers
{
    public class ReporteVehiculosController : Controller
    {
        //
        // GET: /ReporteVehiculos/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> dis = dc.catDistribuidor.Where(x => x.Activo == true).ToList();
            ViewData["distribuidores"] = dis;
            return View();
        }

        public ActionResult Estatica()
        {
            return View();
        }

        public ActionResult Detalle()
        {

            ModeloAlmacen dc = new ModeloAlmacen();


            Guid distribuidor = Guid.Empty;
            try
            {
                distribuidor = Guid.Parse(Request["distribuidor"]);
            }
            catch
            {
                distribuidor = Guid.Empty;
            }
            string NOMBRE = "";
            try
            {
                NOMBRE = dc.catDistribuidor.Where(x => x.catDistribuidorClave == distribuidor).Select(a => a.Nombre).First();
            }
            catch { }


            int pdf = Convert.ToInt32(Request["pdf"]);

            StringBuilder sb = new StringBuilder();

            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Reporte de vehiculos</h3><br>");
            sb.Append("<h3>Distribuidor</h3><br>");
            sb.Append(NOMBRE);
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<br><br>");

            sb.Append(@"<table><thead><tr><td bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"">ID VEHICULO</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">CONDUCTOR TITULAR</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">SERIE</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">PLACAS</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">POLIZA</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">PLAZA DE ENCIERRO</td><td style=""font-size:8px;""bgcolor=""#424242"" color=""#FFFFFF"">TIENE SEGURO</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">MODELO</td><td style=""font-size:8px;"" bgcolor=""#424242"" color=""#FFFFFF"">MARCA</td></tr></thead>");

          
            SqlCommand comandoSql ;
            SqlConnection conexionSQL = new SqlConnection(dc.Database.Connection.ConnectionString);
            try
            {
                conexionSQL.Open();
            }
            catch
            {

            }

            if (distribuidor == Guid.Empty) {
                comandoSql = new SqlCommand("select * from vehiculo x1 join DetalleVehiculo x2  on x1.IdVehiculo=x2.IdVehiculo");
            }
            else
            {
                comandoSql = new SqlCommand("select * from vehiculo x1 join DetalleVehiculo x2  on x1.IdVehiculo=x2.IdVehiculo WHERE x1.DistribuidorClave='" + distribuidor.ToString() + "'");
            }
                       
            comandoSql.Connection = conexionSQL;
            SqlDataReader reader = comandoSql.ExecuteReader();

            sb.Append("<tbody>");
            string distrib;
            if (reader.HasRows)
            {

                while (reader.Read())
                {

                    sb.Append("<tr>");
                    sb.Append(@"<td style=""font-size:6px;"">" + Int32.Parse(reader[0].ToString()) + "</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + reader[1].ToString() + "</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + reader[2].ToString() + "</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + reader[3].ToString() + "</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + reader[4].ToString() + "</td>");

                    try
                    {
                        Guid pla = Guid.Parse(reader[6].ToString());
                        Entidad e = dc.Entidad.Where(x=>x.EntidadClave==pla).First();

                        sb.Append(@"<td style=""font-size:6px;"">" +e.Nombre+ "</td>");
                    }
                    catch
                    {
                        sb.Append(@"<td style=""font-size:6px;"">SIN PLAZA</td>");
                    }
                    

                    if (reader[7].ToString() == "True")
                    {
                        sb.Append(@"<td style=""font-size:6px;"">SI</td>");
                    }
                    else
                    {
                        sb.Append(@"<td style=""font-size:6px;"">NO</td>");
                    }
                    
                    sb.Append(@"<td style=""font-size:6px;"">" + reader[10].ToString() + "</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + reader[11].ToString() + "</td>");
                    

                    sb.Append("</tr>");
                }
            }
           
            sb.Append("</tbody></table>");
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "movimientostecnico.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);


                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "ReporteVehiculos.pdf");
                }
                else
                {
                    return File(rutaarchivo, "application/pdf", "ReporteVehiculos.pdf");

                //    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "movimientostecnico.pdf" + "' style='width:100%; height:700;' frameborder=0></iframe>";
                //    return View();
                }
            }


        }

    }
}
