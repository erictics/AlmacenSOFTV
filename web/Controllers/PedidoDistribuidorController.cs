﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace Web.Controllers
{
    public class PedidoDistribuidorController : Controller
    {
        //
        // GET: /PedidoDistribuidor/
        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {

            Usuario u = ((Usuario)Session["u"]);
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidores = (from d in dc.catDistribuidor
                                                        orderby d.Nombre
                                                        select d).ToList();
            List<catTransportista> Transportistas = (from t in dc.catTransportista
                                                     where t.Activo
                                                     orderby t.Descripcion
                                                     select t).ToList();
            
            ViewData["distribuidores"] = distribuidores;
            ViewData["transportistas"] = Transportistas;
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

    }
}
