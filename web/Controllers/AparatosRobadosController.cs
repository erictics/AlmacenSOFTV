﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;

namespace web.Controllers
{
    public class AparatosRobadosController : Controller
    {
        //
        // GET: /AparatosRobados/

        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);
            List<catDistribuidor> distribuidores = null;
            if (u.DistribuidorClave == null)
            {
                distribuidores = dc.catDistribuidor.Where(x => x.Activo == true).ToList();
            }
            else
            {
                distribuidores = dc.catDistribuidor.Where(x => x.Activo == true && x.catDistribuidorClave==u.DistribuidorClave).ToList();
            }
            
            List<catClasificacionArticulo> clasificacion = dc.catClasificacionArticulo.Where(o => o.Activo == true).ToList();

            ViewData["distribuidores"]=distribuidores;
            ViewData["clasificacion"] = clasificacion;

            return View();
        }


        public ActionResult Reporte()
        {
             ModeloAlmacen dc = new ModeloAlmacen();

            Guid distribuidor = Guid.Empty;
            Guid almacen = Guid.Empty;
            Guid articulo = Guid.Empty;
            int clasificacion = 0;
            int tipo = 0;
            int pdf = 0;
            try
            {
               distribuidor=Guid.Parse(Request["distribuidor"]);
            }
            catch
            {}
            
            try
            {
                almacen = Guid.Parse(Request["almacen"]);
            }
            catch
            {}

            try
            {
                articulo = Guid.Parse(Request["arti"]);
            }
            catch
            { }

            try
            {
                clasificacion = Int32.Parse(Request["clasificacion"]);   
            }
            catch
            {}
            try
            {
                tipo = Int32.Parse(Request["tipo"]);
            }
            catch
            {}
           
            try
            {
                pdf = Int32.Parse(Request["pdf"]);
            }
            catch
            { }

            string sd="Todos los distribuidores";
            string sa="Todos los artículos";
            string sal="Todos los almacenes";
            string scla="Todas las clasificaciones";
            string stip="Todos los tipos";

            try{
                sd=dc.catDistribuidor.Where(o=>o.Activo==true && o.catDistribuidorClave==distribuidor).Select(o=>o.Nombre).First().ToString();
            }catch{

            }
            
            try{
                sa=dc.Articulo.Where(o=>o.Activo==true && o.ArticuloClave==articulo).Select(o=>o.Nombre).First().ToString();
            }catch{

            }
            try{
                sal=dc.Entidad.Where(o=>o.Activo==true && o.EntidadClave==almacen).Select(o=>o.Nombre).First().ToString();
            }catch{

            }

            try{
                scla=dc.catClasificacionArticulo.Where(o=>o.Activo==true && o.catClasificacionArticuloClave==clasificacion).Select(o=>o.Descripcion).First().ToString();
            }catch{

            }

            try{
                stip=dc.catTipoArticulo.Where(o=>o.Activo==true && o.catTipoArticuloClave==tipo).Select(o=>o.Descripcion).First().ToString();
            }catch{

            }

          


            PDF elpdf = new PDF();
            Guid g = Guid.NewGuid();
            StringBuilder sb = new StringBuilder();

           



            if (pdf == 1)
            {
                string empresa = Config.Dame("EMPRESA");
                string logo = Config.Dame("LOGO_ENTRADA");

                sb.Append(@"<table cellpadding=""2"" style=""width: 100%; border: 1px solid black;"" cellspacing=""0"" border=0 width=""100%"">");
                sb.Append("<tr>");
                sb.Append(@"<td align=""left"">");

                sb.Append(@"<h3 style=""""><b>" + empresa + "</b></h3>");
                sb.Append("<h4>Reporte aparatos con reporte de robo</h4><br>");
                sb.Append(DateTime.Now.ToString());
                sb.Append("<h4>Distribuidor: "+sd+"</h4>");                
                sb.Append("<h4>Almacén: "+sal+" </h4>");               
                sb.Append("<h4>Clasificación: "+scla+"</h4>");               
                sb.Append("<h4>Tipo: "+ stip + "</h4>");              
                sb.Append("<h4>Artículo: "+ sa + "</h4>");                
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 align=right><br>");
                sb.Append("</tr>");
                //sb.Append("<tr><td></td><tr>");
                sb.Append("</table>");

                

                if (distribuidor==Guid.Empty){

                    List<catDistribuidor> distrib = dc.catDistribuidor.Where(o => o.Activo == true).ToList();

                    foreach (var a in distrib)
                    {
                        if (distritieneApaRob(a.catDistribuidorClave))
                        {
                            sb.Append("<table>");
                            sb.Append(@"<tr><td colspan=5 bgcolor=""#424242"" color=""#FFFFFF"" align=center>" + a.Nombre + "</td></tr>");
                            sb.Append(@"<tr><td colspan=5 bgcolor=""#424242"" color=""#FFFFFF""></td></tr>");

                            List<Entidad> alm = dc.Entidad.Where(x => x.catDistribuidorClave == a.catDistribuidorClave && x.catEntidadTipoClave == 1).ToList();

                            foreach (var b in alm)
                            {

                                if (tieneaparatosrobados(b.EntidadClave))
                                {


                                    sb.Append(@"<tr><td colspan=5  align=center bgcolor=""#d1d3d4"" ><center>" + b.Nombre + "</center></td></tr>");

                                    if (tipo == 0)
                                    {
                                        var art = dc.Inventario.Where(x => x.EntidadClave == b.EntidadClave && x.EstatusInv == 20 && x.Articulo.catTipoArticulo.catSerieClaveInterface != null).Select(s => new aparatos { articulo = s.Articulo.Nombre, clasificacion = s.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = s.Articulo.catTipoArticulo.Descripcion, ubicacion = s.Entidad.Nombre, serie = s.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();

                                        foreach (var s in art)
                                        {
                                            sb.Append("<tr><td>" + s.ubicacion + "</td><td>" + s.clasificacion + "</td><td>" + s.tipo + "</td><td>" + s.articulo + "</td><td>" + s.serie + "</td></tr>");
                                        }

                                    }
                                    else
                                    {

                                        var art = dc.Inventario.Where(x => x.EntidadClave == b.EntidadClave && x.EstatusInv == 20 && x.Articulo.catTipoArticulo.catSerieClaveInterface != null && x.Articulo.catTipoArticuloClave == tipo
                                            && x.ArticuloClave == articulo
                                            ).Select(s => new aparatos { articulo = s.Articulo.Nombre, clasificacion = s.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = s.Articulo.catTipoArticulo.Descripcion, ubicacion = s.Entidad.Nombre, serie = s.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();

                                        foreach (var s in art)
                                        {
                                            sb.Append("<tr><td>" + s.ubicacion + "</td><td>" + s.clasificacion + "</td><td>" + s.tipo + "</td><td>" + s.articulo + "</td><td>" + s.serie + "</td></tr>");
                                        }


                                    }

                                }
                                else
                                {

                                }

                            }


                            sb.Append("</table>");
                        }
                       

                    }
                }
                else
                {
                    catDistribuidor distrib = dc.catDistribuidor.Where(o => o.Activo == true && o.catDistribuidorClave==distribuidor).First();
                    
                    sb.Append("<table>");
                    sb.Append(@"<tr><td colspan=5 bgcolor=""#424242"" color=""#FFFFFF"" align=center><center>" + distrib.Nombre + "</center></td></tr>");
                    sb.Append(@"<tr><td colspan=5 bgcolor=""#424242"" color=""#FFFFFF"" ></td></tr>");


                    if (almacen == Guid.Empty)
                    {
                        List<Entidad> alm = dc.Entidad.Where(x => x.catDistribuidorClave == distrib.catDistribuidorClave && x.catEntidadTipoClave == 1).ToList();

                        foreach (var b in alm)
                        {
                            if (tieneaparatosrobados(b.EntidadClave))
                            {

                                sb.Append(@"<tr><td colspan=5  bgcolor=""#d1d3d4"" align=center >" + b.Nombre + "</td></tr>");
                                sb.Append("<tr><td><b>Ubicación</b></td><td><b>Clasificación</b></td><td><b>Tipo de artículo</b></td><td><b>Artículo</b></td><td><b>Serie</b></td></tr>");
                                if (tipo == 0)
                                {
                                    var art = dc.Inventario.Where(x => x.EntidadClave == b.EntidadClave && x.EstatusInv == 20 && x.Articulo.catTipoArticulo.catSerieClaveInterface != null).Select(s => new aparatos { articulo = s.Articulo.Nombre, clasificacion = s.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = s.Articulo.catTipoArticulo.Descripcion, ubicacion = s.Entidad.Nombre, serie = s.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();

                                    foreach (var s in art)
                                    {
                                        sb.Append("<tr><td>" + s.ubicacion + "</td><td>" + s.clasificacion + "</td><td>" + s.tipo + "</td><td>" + s.articulo + "</td><td>" + s.serie + "</td></tr>");
                                    }

                                }
                                else
                                {

                                    var art = dc.Inventario.Where(x => x.EntidadClave == b.EntidadClave && x.EstatusInv == 20 && x.Articulo.catTipoArticulo.catSerieClaveInterface != null && x.Articulo.catTipoArticuloClave == tipo
                                        && x.ArticuloClave == articulo
                                        ).Select(s => new aparatos { articulo = s.Articulo.Nombre, clasificacion = s.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = s.Articulo.catTipoArticulo.Descripcion, ubicacion = s.Entidad.Nombre, serie = s.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();

                                    foreach (var s in art)
                                    {
                                        sb.Append("<tr><td>" + s.ubicacion + "</td><td>" + s.clasificacion + "</td><td>" + s.tipo + "</td><td>" + s.articulo + "</td><td>" + s.serie + "</td></tr>");
                                    }

                                }
                            }
                            else
                            {

                            }
                        }
                    }
                    else
                    {
                        Entidad alm = dc.Entidad.Where(x => x.EntidadClave == almacen && x.catEntidadTipoClave == 1).First();
                        sb.Append(@"<tr><td colspan=5  bgcolor=""#d1d3d4"" align=center ><center>" + alm.Nombre + "</center></td></tr>");
                        sb.Append("<tr><td><b>Ubicación</b></td><td><b>Clasificación</b></td><td><b>Tipo de artículo</b></td><td><b>Artículo</b></td><td><b>Serie</b></td></tr>");
                        if (tipo == 0)
                        {
                            var art = dc.Inventario.Where(x => x.EntidadClave == alm.EntidadClave && x.EstatusInv == 20 && x.Articulo.catTipoArticulo.catSerieClaveInterface != null).Select(s => new aparatos { articulo = s.Articulo.Nombre, clasificacion = s.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = s.Articulo.catTipoArticulo.Descripcion, ubicacion = s.Entidad.Nombre, serie = s.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();
                            foreach (var s in art)
                            {
                                sb.Append("<tr><td>" + s.ubicacion + "</td><td>" + s.clasificacion + "</td><td>" + s.tipo + "</td><td>" + s.articulo + "</td><td>" + s.serie + "</td></tr>");
                            }

                        }
                        else
                        {

                            var art = dc.Inventario.Where(x => x.EntidadClave == alm.EntidadClave && x.EstatusInv == 20 && x.Articulo.catTipoArticulo.catSerieClaveInterface != null && x.Articulo.catTipoArticuloClave == tipo
                                && x.ArticuloClave == articulo
                                ).Select(s => new aparatos { articulo = s.Articulo.Nombre, clasificacion = s.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = s.Articulo.catTipoArticulo.Descripcion, ubicacion = s.Entidad.Nombre, serie = s.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();
                            foreach (var s in art)
                            {
                                sb.Append("<tr><td>" + s.ubicacion + "</td><td>" + s.clasificacion + "</td><td>" + s.tipo + "</td><td>" + s.articulo + "</td><td>" + s.serie + "</td></tr>");
                            }
                        }




                    }

                    sb.Append("</table>");
                    
                    }


                }
                              
              

            
            else
            {

                


                Guid dis = dc.catDistribuidor.Where(x => x.Activo == true).Select(o => o.catDistribuidorClave).First();
                string ndis = dc.catDistribuidor.Where(x => x.Activo == true).Select(o => o.Nombre).First();
                List<Entidad> almacenes = dc.Entidad.Where(x => x.catDistribuidorClave == dis && x.catEntidadTipoClave == 1).ToList();


                string empresa = Config.Dame("EMPRESA");
                string logo = Config.Dame("LOGO_ENTRADA");

                sb.Append(@"<table cellpadding=""2"" style=""width: 100%; border: 1px solid black;"" cellspacing=""0"" border=0 width=""100%"">");
                sb.Append("<tr>");
                sb.Append(@"<td align=""left"">");

                sb.Append(@"<h2 style=""""><b>" + empresa + "</b></h2>");
                sb.Append("<h3><b>Reporte aparatos con reporte de robo</b></h3><br>");
                sb.Append(DateTime.Now.ToString());
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append("<img src='_RUTA_/media/" + logo + "' width=120 align=right><br>");
                sb.Append("</tr>");
                //sb.Append("<tr><td></td><tr>");
                sb.Append("</table>");

                sb.Append("<b>Distribuidor:</b><br>");
                sb.Append(ndis + "<br>");
                sb.Append("<b>Almacén:</b><br>");
                sb.Append("Todos los almacénes" + "<br>");
                sb.Append("<b>Clasificación:</b><br>");
                sb.Append("Todas las clasificaciones" + "<br>");
                sb.Append("<b>Tipo:</b><br>");
                sb.Append("Todos los tipos" + "<br>");
                sb.Append("<b>Artículo:</b><br>");
                sb.Append("Todos los artículos" + "<br>");
                sb.Append("<br>");








                sb.Append("<table>");
                sb.Append(@"<tr><td colspan=5 bgcolor=""#424242"" align=center color=""#ffffff ""><center>" + ndis + "</center></td></tr>");

                foreach(var alm in almacenes){

                    if (tieneaparatosrobados(alm.EntidadClave))
                    {
                    var articulos = dc.Inventario.Where(x => x.EstatusInv == 20 && x.EntidadClave==alm.EntidadClave ).Select(o => new aparatos { ubicacion = o.Entidad.Nombre, clasificacion = o.Articulo.catTipoArticulo.catClasificacionArticulo.Descripcion, tipo = o.Articulo.catTipoArticulo.Descripcion, articulo = o.Articulo.Nombre, serie = o.Serie.Select(x => x.Valor).FirstOrDefault() }).ToList();


                    
                    sb.Append(@"<tr><td colspan=5 bgcolor=""#d1d3d4"" align=center color=""#000000""><center>" + alm.Nombre + "</center></td></tr>");
                    sb.Append("<tr><td><b>Ubicación</b></td><td><b>Clasificación</b></td><td><b>Tipo de artículo</b></td><td><b>Artículo</b></td><td><b>Serie</b></td></tr>");
                    foreach (var a in articulos)
                    {
                        sb.Append("<tr><td>" + a.ubicacion + "</td><td>" + a.clasificacion + "</td><td>" + a.tipo + "</td><td>" + a.articulo + "</td><td>" + a.serie + "</td></tr>");
                    }
                                  
                    }

                }             



                sb.Append("</table>");

            }
            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "reporte.pdf";
            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);

            ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "reporte.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
            return View();              
        }




        public bool tieneaparatosrobados(Guid idalmacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            if (dc.Inventario.Any(x => x.EntidadClave == idalmacen && x.EstatusInv == 20))
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }



        public bool distritieneApaRob(Guid distribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var lista = dc.Entidad.Where(x => x.catDistribuidorClave == distribuidor && x.catEntidadTipoClave == 1).Select(x=>x.EntidadClave).ToList();

            var products = dc.Inventario
             .Where(p => lista
           .Contains(p.EntidadClave.Value) && p.EstatusInv==20)
            .ToList();
            return (products.Count > 0) ? true : false;
           
        }



        class aparatos
        {
            public string ubicacion { get; set; }
            public string clasificacion { get; set; }
            public string tipo { get; set; }
            public string articulo { get; set; }

            public string serie { get; set; }
        }
    }
}
