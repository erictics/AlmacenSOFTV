﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{

    public class CompressAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            var encodingsAccepted = filterContext.HttpContext.Request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(encodingsAccepted)) return;

            encodingsAccepted = encodingsAccepted.ToLowerInvariant();
            var response = filterContext.HttpContext.Response;

            if (encodingsAccepted.Contains("deflate"))
            {
                response.AppendHeader("Content-encoding", "deflate");
                response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
            }
            else if (encodingsAccepted.Contains("gzip"))
            {
                response.AppendHeader("Content-encoding", "gzip");
                response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
            }
        }
    }


    public class SessionExpireFilterAttribute : ActionFilterAttribute
    {


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            ctx.Session.Timeout = 720;
            if (ctx.Session["u"] == null)
            {
                //var ASPCookie = ctx.Request.Cookies["Cookie"];
                //ASPCookie.Expires = DateTime.Now.AddDays(15);
               // ctx.Response.SetCookie(ASPCookie);
                ctx.Response.Redirect("~/Acceso/Login");
            }
            
            if (ctx.Session != null)
            {

                if (ctx.Session.IsNewSession)
                {
                    
                    string sessionCookie = ctx.Request.Headers["Cookie"];

                    //sessionCookie.Expires = DateTime.Now.AddDays(15);
                    if ((null != sessionCookie) && (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0))
                    {

                        ctx.Response.Redirect("~/Acceso/Login");
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

    }

    public class AllowCrossDomain : ActionFilterAttribute
    {
        public const string AllDomains = "*";

        private readonly string[] _allowMethods;
        private string _allowOrigin;

        public AllowCrossDomain()
            : this(null, null)
        {

        }

        public AllowCrossDomain(string allowOrigin, params string[] allowMethods)
        {
            _allowMethods = allowMethods;
            _allowOrigin = allowOrigin;

            if (string.IsNullOrWhiteSpace(_allowOrigin))
            {
                _allowOrigin = AllDomains;
            }
            if (_allowMethods == null || _allowMethods.Length == 0)
            {
                _allowMethods = new[] { "GET" };
            }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", _allowOrigin);
            filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", string.Join(", ", _allowMethods));
            filterContext.HttpContext.Response.Headers.Add("Access-Control-Max-Age", "86400");
        }
    }

}