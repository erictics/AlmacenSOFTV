﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Controllers;
using web.Models;

using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class SalidaMaterialController : Controller
    {
        //
        // GET: /Articulo/
        ModeloAlmacen dc = new ModeloAlmacen();



        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                              where ta.Activo
                                                              orderby ta.Descripcion
                                                              select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();


            List<Entidad> almacenes = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                almacenes = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                     && ta.catDistribuidorClave == null
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {

                List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                              where a.UsuarioClave == u.UsuarioClave
                                              select a).ToList();
                almacenes = new List<Entidad>();


                foreach (AmbitoAlmacen a in ambito)
                {
                    Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        almacenes.Add(e);
                    }
                }
            }


            List<Entidad> tecnicos = null;

            if (u.DistribuidorClave == null)
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == null
                            orderby ta.Nombre
                            select ta).ToList();
            }
            else
            {
                tecnicos = (from ta in dc.Entidad
                            where ta.catEntidadTipoClave == 2
                                   && ta.catDistribuidorClave == u.DistribuidorClave
                            orderby ta.Nombre
                            select ta).ToList();
            }



            List<catTipoSalida> tipossalida = (from ta in dc.catTipoSalida
                                               where ta.Activo
                                               orderby ta.Descripcion
                                               select ta).ToList();




            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;

            ViewData["almacenes"] = almacenes;
            ViewData["tecnicos"] = tecnicos;
            ViewData["tipossalida"] = tipossalida;

            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");

            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;



            int cuantos = (from use in dc.Proceso
                           where
                           use.catDistribuidorClave != null
                           && use.catTipoProcesoClave == 1
                           orderby use.Fecha descending
                           select use).Count();
            ViewData["cuantos"] = cuantos;


            List<Proceso> salidaspendientes = (from use in dc.Proceso
                                               where use.catDistribuidorClave != null && use.catTipoProcesoClave == 1
                                               orderby use.Fecha descending
                                               select use).ToList();

            ViewData["salidaspendientes"] = salidaspendientes;
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        int elementos = 30;

        public ActionResult ObtenPaginas(string data)
        {


            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int vnEstatus = 0;

            Guid pedido = Guid.Empty,
                vgAlmacen = Guid.Empty;

            try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }

            try
            {
                vgAlmacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch { }

            try
            {
                vnEstatus = int.Parse(jObject["estatus"].ToString());
            }
            catch { }

            Guid tecnico;
            try
            {
                tecnico = Guid.Parse(jObject["tecnico"].ToString());
            }
            catch
            {
                tecnico = Guid.Empty;
            }

            int tipoentidad = 2;

            try
            {
                tipoentidad = Convert.ToInt32(jObject["tipoentidad"].ToString());
            }
            catch
            {

            }

            Usuario u = (Usuario)Session["u"];


            int pags = -1;

            if (u.DistribuidorClave == null)
            {
                pags = (from use in dc.Proceso
                        where use.catTipoProcesoClave == 3
                        && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                        && (tecnico == Guid.Empty || tecnico == use.EntidadClaveSolicitante)
                        && use.Entidad.catEntidadTipoClave == tipoentidad
                        && use.Entidad.catDistribuidorClave == null
                        orderby use.NumeroPedido descending
                        select use).Count();
            }
            else
            {
                pags = (from use in dc.Proceso
                        where use.catTipoProcesoClave == 3
                        && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                        && (tecnico == Guid.Empty || tecnico == use.EntidadClaveSolicitante)
                        && (vgAlmacen == Guid.Empty || vgAlmacen == use.EntidadClaveAfectada)
                        && use.Entidad.catEntidadTipoClave == tipoentidad
                        && use.Entidad.catDistribuidorClave == u.DistribuidorClave
                        && (vnEstatus == 0 || use.catEstatusClave == vnEstatus)
                        orderby use.NumeroPedido descending
                        select use).Count();
            }

            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div).ToString());
            }
            else
            {
                return Content((div + 1).ToString());
            }
        }



        /// <summary>
        ///  SE OBTIENE LA LISTA DE SALIDAS A TECNICO PARA MOSTRARLAS EN EL GRID
        /// </summary>
        /// <param name="data">
        /// SE RECIBE EL NUMERO DE PAGINAS Y EL CODIGO DEL TECNICO 
        /// </param>
        /// <returns>
        /// List<SalidaMaterialWrapper> rw
        /// </returns>
        public ActionResult ObtenLista2(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString()),
                vnEstatus = 0;

            Guid pedido = Guid.Empty,
                vgAlmacen = Guid.Empty;

            try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }

            try
            {
                vgAlmacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch { }

            try
            {
                vnEstatus = int.Parse(jObject["estatus"].ToString());
            }
            catch { }

            Guid tecnico = Guid.Empty;

            try
            {
                tecnico = Guid.Parse(jObject["tecnico"].ToString());
            }
            catch { }


            int tipoentidad = 2;

            try
            {
                tipoentidad = Convert.ToInt32(jObject["tipoentidad"].ToString());
            }
            catch
            {

            }


            int skip = (pag - 1) * elementos;



            // SE OBTIENE EL USUARIO
            Usuario u = (Usuario)Session["u"];


            List<Proceso> rls = null;

            // SI NO ES UN DISTRIBUIDOR 
            if (u.DistribuidorClave == null)
            {
                // SELECCIONAR TODOS LOS PROCESOS DONDE  catTipoProcesoClave = SALIDA
                //Y PEDIDO SEA IGUAL use.ProcesoClaveRespuesta
                // Y ENTIDAD SEA IGUAL A LA MANDADA
                // Y DISTRIBUIDOR SEA IGUAL A NULO
                rls = (from use in dc.Proceso
                       where use.catTipoProcesoClave == 3
                       && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                       && (tecnico == Guid.Empty || tecnico == use.EntidadClaveSolicitante)
                       && use.Entidad.catEntidadTipoClave == tipoentidad
                       && use.catDistribuidorClave == null
                       orderby use.NumeroPedido descending
                       select use).ToList();
            }
            else
            {
                // SI ES DISTRIBUIDOR 
                //
                rls = (from use in dc.Proceso
                       where use.catTipoProcesoClave == 3
                       && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                       && (tecnico == Guid.Empty || tecnico == use.EntidadClaveSolicitante)
                       && (vgAlmacen == Guid.Empty || vgAlmacen == use.EntidadClaveSolicitante)
                       && use.Entidad.catEntidadTipoClave == tipoentidad
                       && use.Entidad.catDistribuidorClave == u.DistribuidorClave
                       && (vnEstatus == 0 || use.catEstatusClave == vnEstatus)

                       orderby use.NumeroPedido descending
                       select use).ToList();
            }
            List<SalidaMaterialWrapper> rw = new List<SalidaMaterialWrapper>();

            foreach (Proceso p in rls)
            {

                SalidaMaterialWrapper w = SalidaMaterialToWrapper(p, false);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<SalidaMaterialWrapper> data { get; set; }
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString()),
               vnEstatus = 0;

            Guid pedido = Guid.Empty,
               vgAlmacen = Guid.Empty;

            int buscar = 0;
            try
            {
                buscar = Int32.Parse(jObject["buscar"].ToString());
            }
            catch { }
            try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }

            try
            {
                vgAlmacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch { }

            try
            {
                vnEstatus = int.Parse(jObject["estatus"].ToString());
            }
            catch { }

            Guid tecnico = Guid.Empty;

            try
            {
                tecnico = Guid.Parse(jObject["tecnico"].ToString());
            }
            catch { }


            int tipoentidad = 2;

            try
            {
                tipoentidad = Convert.ToInt32(jObject["tipoentidad"].ToString());
            }
            catch
            {

            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData(ref recordsFiltered, start, length, pedido, vgAlmacen, vnEstatus, tecnico, tipoentidad, buscar);
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);


        }


        private List<SalidaMaterialWrapper> FilterData(ref int recordFiltered, int start, int length, Guid pedido, Guid vgAlmacen, int vnEstatus, Guid tecnico, int tipoentidad, int buscar)
        {


            // SE OBTIENE EL USUARIO
            Usuario u = (Usuario)Session["u"];
            List<Proceso> rls = null;
            List<Proceso> rls1 = null;
            List<Proceso> rls2 = null;
            if (buscar > 0)
            {
                rls = (from use in dc.Proceso
                       where use.catTipoProcesoClave == 3 &&
                      use.NumeroPedido == buscar
                       orderby use.NumeroPedido descending
                       select use).ToList();
            }
            else
            {
                if (u.DistribuidorClave == null)
                {

                    rls = (from use in dc.Proceso
                           where use.catTipoProcesoClave == 3
                          && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                          && (tecnico == Guid.Empty || tecnico == use.EntidadClaveSolicitante)
                           && use.Entidad.catEntidadTipoClave == tipoentidad
                          && use.catDistribuidorClave == null
                          && use.catTipoSalidaClave > 0
                           orderby use.NumeroPedido descending
                           select use).ToList();
                    rls1 = rls;
                    recordFiltered = rls1.Count;
                }
                else
                {
                    rls = (from use in dc.Proceso
                           where use.catTipoProcesoClave == 3
                           && (pedido == Guid.Empty || use.ProcesoClaveRespuesta == pedido)
                           && (tecnico == Guid.Empty || tecnico == use.EntidadClaveSolicitante)
                           && (vgAlmacen == Guid.Empty || vgAlmacen == use.EntidadClaveSolicitante)
                           && use.Entidad.catEntidadTipoClave == tipoentidad
                           && use.Entidad.catDistribuidorClave == u.DistribuidorClave
                           && (vnEstatus == 0 || use.catEstatusClave == vnEstatus)
                           orderby use.NumeroPedido descending
                           select use).ToList();

                    rls2 = (from a_ in dc.Proceso
                            join b_ in dc.RelTecnicoDistribuidor on a_.EntidadClaveSolicitante equals b_.EntidadClave
                            where (pedido == Guid.Empty || a_.ProcesoClaveRespuesta == pedido)
                               && (tecnico == Guid.Empty || tecnico == a_.EntidadClaveSolicitante)
                           && b_.DistribuidorClave == u.DistribuidorClave
                            select a_).ToList();

                    rls.AddRange(rls2);
                    rls1 = rls;
                    recordFiltered = rls1.Count;
                }
            }

            List<SalidaMaterialWrapper> rw = new List<SalidaMaterialWrapper>();

            foreach (Proceso p in rls.OrderByDescending(x => x.NumeroPedido).Skip(start).Take(length).ToList())
            {

                SalidaMaterialWrapper w = SalidaMaterialToWrapper(p, false);
                rw.Add(w);
            }

            return rw;

        }








        public ActionResult Obten(string data)
        {
            SalidaMaterialWrapper rw = new SalidaMaterialWrapper();



            Guid uid = Guid.Parse(data);

            Proceso r = (from ro in dc.Proceso
                         where ro.ProcesoClave == uid
                         select ro).SingleOrDefault();

            if (r != null)
            {
                rw = SalidaMaterialToWrapper(r, true);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Articulos(string data)
        {
            PedidoWrapper rw = new PedidoWrapper();



            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pedido = Convert.ToInt32(jObject["pedido"].ToString());

            Proceso r = (from ro in dc.Proceso
                         where ro.NumeroPedido == pedido
                         select ro).SingleOrDefault();

            if (r != null)
            {
                List<ProcesoArticulo> articulos = r.ProcesoArticulo.ToList();

                List<ArticuloProcesoWrapper> apws = new List<ArticuloProcesoWrapper>();

                foreach (ProcesoArticulo a in articulos)
                {
                    ArticuloProcesoWrapper apw = PedidoToarticuloProcesoWrapper(a);
                    apws.Add(apw);
                }


                return Json(apws, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {


                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    Articulo r = (from ro in dc.Articulo
                                  where ro.ArticuloClave == uid
                                  select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {


                if (activa_id != String.Empty)
                {
                    Guid uid = Guid.Parse(activa_id);

                    Articulo r = (from ro in dc.Articulo
                                  where ro.ArticuloClave == uid
                                  select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Recepcion(string data)
        {
            try
            {


                string salida_id = Request["salida_id"].ToString();
                string acepta_rechaza = Request["acepta_rechaza"].ToString();

                Guid salida_guid = Guid.Parse(salida_id);

                Guid salidaguid = Guid.Parse(salida_id);


                Proceso salida = (from i in dc.Proceso
                                  where i.ProcesoClave == salida_guid
                                  select i).SingleOrDefault();

                if (salida != null)
                {

                    if (acepta_rechaza == "0")
                    {
                        List<ProcesoInventario> proin = salida.ProcesoInventario.ToList();

                        foreach (ProcesoInventario pi in proin)
                        {
                            Inventario i = pi.Inventario;
                            i.EntidadClave = salida.EntidadClaveAfectada;
                        }


                        salida.catEstatusClave = 5;
                    }
                    else if (acepta_rechaza == "1")
                    {
                        List<ProcesoInventario> proin = salida.ProcesoInventario.ToList();
                        List<ProcesoArticulo> salidaproducto = salida.ProcesoArticulo.ToList();


                        foreach (ProcesoInventario pi in proin)
                        {
                            Inventario i = pi.Inventario;

                            Inventario ifinal = (from ifi in dc.Inventario
                                                 where ifi.EntidadClave == salida.EntidadClaveSolicitante
                                                 && ifi.ArticuloClave == i.ArticuloClave
                                                 select ifi).SingleOrDefault();

                            if (ifinal == null)
                            {
                                ifinal = new Inventario();
                                ifinal.InventarioClave = Guid.NewGuid();
                                ifinal.ArticuloClave = i.ArticuloClave;
                                ifinal.EntidadClave = salida.EntidadClaveSolicitante;
                                ifinal.Cantidad = pi.Cantidad;
                                ifinal.Standby = 0;
                                ifinal.Estatus = 1;
                                ifinal.EstatusInv = 7;
                                dc.Inventario.Add(ifinal);
                            }
                            else
                            {
                                ifinal.ArticuloClave = i.ArticuloClave;
                                ifinal.EntidadClave = salida.EntidadClaveSolicitante;
                                ifinal.Cantidad += pi.Cantidad;
                                ifinal.Standby = 0;
                                ifinal.Estatus = 1;
                            }

                            i.Standby -= ifinal.Cantidad;

                            dc.SaveChanges();
                        }

                        salida.catEstatusClave = 4;

                    }

                }

                dc.SaveChanges();


                return Content("1");

            }
            catch
            {
                return Content("0");

            }
        }



        public ActionResult CancelaTraspaso(string cancelar_id)
        {

            try
            {



                Guid uid = Guid.Parse(cancelar_id);

                Proceso salida = (from ro in dc.Proceso
                                  where ro.ProcesoClave == uid
                                  && ro.catEstatusClave != 6
                                  select ro).SingleOrDefault();

                if (salida != null)
                {

                    Proceso Pedido = salida.Proceso2;
                    if (Pedido != null)
                    {

                        int intsalida = (from ro in dc.Proceso
                                         where ro.ProcesoClaveRespuesta == Pedido.ProcesoClave
                                                && ro.catTipoProcesoClave == 3
                                                && ro.catEstatusClave != 6
                                         select ro).Count();

                        if (intsalida != 1)
                        {
                            Pedido.catEstatusClave = 3;
                        }
                        else
                        {
                            Pedido.catEstatusClave = 2;
                        }


                        if (Pedido != null)
                        {

                            List<ProcesoArticulo> proarts = Pedido.ProcesoArticulo.ToList();

                            List<ProcesoInventario> proinvs = (from i in dc.ProcesoInventario
                                                               where i.ProcesoClave == salida.ProcesoClave
                                                               select i).ToList();

                            foreach (ProcesoInventario proinv in proinvs)
                            {
                                Inventario inv = proinv.Inventario;
                                ProcesoArticulo proart = proarts.SingleOrDefault(o => o.ProcesoClave == Pedido.ProcesoClave && o.ArticuloClave == inv.ArticuloClave);

                                //proart.CantidadEntregada -= proinv.Cantidad;
                                //inv.Cantidad -= proinv.Cantidad;

                                if (inv.Cantidad == 0)
                                {

                                }

                            }
                        }
                    }


                    salida.catEstatusClave = 6;

                    List<ProcesoInventario> invpro = (from pi in dc.ProcesoInventario
                                                      where pi.ProcesoClave == uid
                                                      select pi).ToList();

                    foreach (ProcesoInventario pi in invpro)
                    {
                        //pi.sur


                        Inventario inv_almacen = pi.Inventario;
                        if (inv_almacen.Standby < pi.Cantidad)
                        {
                            return Content("-1");
                        }

                        inv_almacen.Cantidad += pi.Cantidad;
                        inv_almacen.Standby -= pi.Cantidad;

                        if (inv_almacen.Standby < 0)
                        {
                            return Content("-1");
                        }

                        inv_almacen.Estatus = 1;

                    }

                    dc.ProcesoInventario.RemoveRange(invpro);


                }

                dc.SaveChanges();


                return Content("1");
            }
            catch
            {

                return Content("0");
            }
        }


        public ActionResult Cancela(string cancelar_id)
        {

            try
            {



                Guid uid = Guid.Parse(cancelar_id);

                Proceso r = (from ro in dc.Proceso
                             where ro.ProcesoClave == uid
                             && ro.catEstatusClave != 6
                             select ro).SingleOrDefault();

                if (r != null)
                {

                    r.catEstatusClave = 6;

                    List<ProcesoInventario> invpro = (from pi in dc.ProcesoInventario
                                                      where pi.ProcesoClave == uid
                                                      select pi).ToList();

                    foreach (ProcesoInventario pi in invpro)
                    {
                        Inventario inv_tecnico = pi.Inventario;


                        /// Los que tienen Seriales
                        if (inv_tecnico.Serie.Count > 0)
                        {
                            inv_tecnico.EntidadClave = r.EntidadClaveAfectada;
                        }
                        else//Los que no tienen seriales
                        {
                            inv_tecnico.Cantidad -= pi.Cantidad;

                            if (inv_tecnico.Cantidad == 0)
                            {
                                inv_tecnico.Estatus = 0;
                            }
                            dc.SaveChanges();

                            List<Inventario> invs_almacen = (from ia in dc.Inventario
                                                             where ia.ArticuloClave == inv_tecnico.ArticuloClave
                                                             && ia.EntidadClave == r.EntidadClaveAfectada
                                                             && ia.EstatusInv == 7
                                                             select ia).ToList();
                            Inventario inv_almacen = new Inventario();
                            if (invs_almacen.Count != 1)
                            {
                                inv_almacen = null;
                            }
                            else
                            {
                                inv_almacen = invs_almacen[0];
                            }

                            if (inv_almacen == null)
                            {
                                inv_almacen = new Inventario();
                                inv_almacen.InventarioClave = Guid.NewGuid();
                                inv_almacen.ArticuloClave = inv_tecnico.ArticuloClave;
                                inv_almacen.EntidadClave = r.EntidadClaveAfectada;
                                inv_almacen.Cantidad = pi.Cantidad;
                                inv_almacen.Standby = 0;
                                inv_almacen.Estatus = 1;
                                inv_almacen.EstatusInv = 7;
                                dc.Inventario.Add(inv_almacen);
                            }
                            else
                            {
                                inv_almacen.Cantidad += pi.Cantidad;
                                inv_almacen.Estatus = 1;
                            }
                        }

                        dc.SaveChanges();

                    }

                }

                dc.SaveChanges();


                return Content("1");
            }
            catch
            {

                return Content("0");
            }
        }

        public class en_pedidos
        {
            public Guid articulo { get; set; }
            public int cantidad { get; set; }
        }

        public class surtidos
        {
            public Guid articulo { get; set; }
            public int cantidad { get; set; }
        }

        public ActionResult Guarda(string data)
        {
            //try
            //{

            Usuario u = (Usuario)Session["u"];
            bool soysalidadistribuidor = false;
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            string ed_id = String.Empty;

            if (jObject["ed_id"] != null)
            {
                ed_id = jObject["ed_id"].ToString();
            }


            Proceso r = new Proceso();

            if (ed_id != String.Empty)
            {
                Guid uid = Guid.Parse(ed_id);

                r = (from ro in dc.Proceso
                     where ro.ProcesoClave == uid
                     select ro).SingleOrDefault();
            }
            else
            {
                r.ProcesoClave = Guid.NewGuid();
            }


            try
            {
                if (jObject["Pedido"] != null)
                {
                    r.ProcesoClaveRespuesta = Guid.Parse(jObject["Pedido"].ToString());
                    soysalidadistribuidor = true;
                    var qsc = from pa in dc.ProcesoArticulo
                              join p in dc.Proceso on pa.ProcesoClave equals p.ProcesoClave
                              where p.ProcesoClaveRespuesta == r.ProcesoClaveRespuesta
                              && p.catEstatusClave != 6
                              group pa by pa.ArticuloClave into g
                              select new
                              {
                                  ArticuloClave = g.Key,
                                  Cantidad = g.Sum(pa => pa.Cantidad)
                              };

                    int arts_pend = (from pa in dc.ProcesoArticulo
                                     join pan in qsc on pa.ArticuloClave equals pan.ArticuloClave into g
                                     from x in g.DefaultIfEmpty()
                                     where pa.ProcesoClave == r.ProcesoClaveRespuesta
                                     && pa.Cantidad != x.Cantidad
                                     select pa).ToList().Count();

                    if (arts_pend == 0)
                    {

                        return Content("-10");
                    }

                    //   /*añadi esto para validar que no se excedan mas articulos en salida  de la cantidad total pedida*/
                    // List<en_pedidos>pedidos=new List<en_pedidos>();
                    // List<surtidos> surtidos=new List<surtidos>();
                    //Proceso proces = (from a in dc.Proceso where a.ProcesoClave == r.ProcesoClaveRespuesta select a).FirstOrDefault();

                    // foreach(var p in proces.ProcesoArticulo){
                    //     en_pedidos i=new en_pedidos();
                    //    i.cantidad= p.Cantidad.Value;
                    //     i.articulo=p.ArticuloClave;
                    //     pedidos.Add(i);

                    // }



                    //List<Proceso> salidas=(from b in dc.Proceso where b.ProcesoClaveRespuesta==proces.ProcesoClave select b).ToList();
                    //foreach (var s in salidas)
                    //{
                    //    foreach (var ss in s.ProcesoArticulo)
                    //    {
                    //        surtidos d=new surtidos();
                    //        d.articulo = ss.ArticuloClave;
                    //        d.cantidad = ss.Cantidad.Value;
                    //        surtidos.Add(d);

                    //    }
                    //}




                    //List<en_pedidos> result = (from x in pedidos
                    //             group x by new { x.articulo, x.cantidad }
                    //                 into g
                    //                 select new en_pedidos{ articulo=g.Key.articulo, cantidad = g.Sum(o => o.cantidad) }).ToList();

                    //List<surtidos> results = (from x in surtidos
                    //                           group x by new { x.articulo, x.cantidad }
                    //                               into g
                    //                               select new surtidos { articulo = g.Key.articulo, cantidad = g.Sum(o => o.cantidad) }).ToList();

                    //var firstNotSecond = result.Except(results).ToList();

                    /**/

                }

                r.catEstatusClave = 1;
                r.catTipoProcesoClave = 3;
                r.EntidadClaveSolicitante = Guid.Parse(jObject["Tecnico"].ToString());
                r.EntidadClaveAfectada = Guid.Parse(jObject["Almacen"].ToString());
            }
            catch
            {
            }


            try
            {
                r.catTipoSalidaClave = Convert.ToInt32(jObject["TipoSalida"].ToString());
            }
            catch
            {
            }
            r.UsuarioClave = ((Usuario)Session["u"]).UsuarioClave;

            r.Fecha = DateTime.Now;

            try
            {
                r.Observaciones = jObject["Observaciones"].ToString();
            }
            catch { }

            if (ed_id == String.Empty)
            {
                dc.Proceso.Add(r);
            }
            // dc.SaveChanges();

            bool distribuidor = true;

            Entidad entidad = (from e in dc.Entidad
                               where e.EntidadClave == r.EntidadClaveSolicitante
                               select e).SingleOrDefault();

            if (entidad != null)
            {
                if (entidad.catEntidadTipoClave == 2)
                {
                    distribuidor = false;
                    r.catEstatusClave = 4;
                }
            }

            #region CANCELA_SALIDA
            //CANCELAMOS LA SALIDA Y HACEMOS DE NUEVO
            if (ed_id != String.Empty)
            {
                //SI ES DE DISTRIBUIDOR
                if (distribuidor)
                {
                    Proceso Pedido = r.Proceso2;
                    if (Pedido != null)
                    {   //SI ES UN PEDIDO, EL PEDIDO LO REGRESAMOS AL ESTATUs PARCIAL
                        Pedido.catEstatusClave = 3;
                    }

                    //SI SE CANCELA UN TRASPASO, SOLO SE MUEVEN DEL STANDBY AL ALMACEN
                    List<ProcesoInventario> invpro = (from pi in dc.ProcesoInventario
                                                      where pi.ProcesoClave == r.ProcesoClave
                                                      select pi).ToList();
                    foreach (ProcesoInventario pi in invpro)
                    {
                        Inventario inv_almacen = pi.Inventario;
                        if (inv_almacen.Standby < pi.Cantidad)
                        {
                            return Content("-1");
                        }
                        inv_almacen.Cantidad += pi.Cantidad;
                        inv_almacen.Standby -= pi.Cantidad;
                        if (inv_almacen.Standby < 0)
                        {
                            return Content("-1");
                        }
                        inv_almacen.Estatus = 1;
                    }

                    dc.ProcesoInventario.RemoveRange(invpro);
                }
                else  ///EDITAR SALIDA A TECNICO
                {
                    List<ProcesoInventario> invpro = (from pi in dc.ProcesoInventario
                                                      where pi.ProcesoClave == r.ProcesoClave
                                                      select pi).ToList();

                    foreach (ProcesoInventario pi in invpro)
                    {
                        Inventario inv_tecnico = pi.Inventario;

                        /// Los que tienen Seriales: SOLO SE REGRESAN A SU ENTIDAD ANTERIOR
                        if (inv_tecnico.Serie.Count > 0)
                        {
                            inv_tecnico.EntidadClave = r.EntidadClaveAfectada;
                        }
                        else//Los que no tienen seriales
                        {
                            inv_tecnico.Cantidad -= pi.Cantidad;
                            if (inv_tecnico.Cantidad == 0)
                            {
                                inv_tecnico.Estatus = 0;
                            }
                            //dc.SaveChanges();

                            List<Inventario> invs_almacen = (from ia in dc.Inventario
                                                             where ia.ArticuloClave == inv_tecnico.ArticuloClave
                                                             && ia.EntidadClave == r.EntidadClaveAfectada
                                                             && ia.EstatusInv == 7
                                                             select ia).ToList();
                            Inventario inv_almacen = new Inventario();
                            if (invs_almacen.Count != 1)
                            {
                                inv_almacen = null;
                            }
                            else
                            {
                                inv_almacen = invs_almacen[0];
                            }

                            if (inv_almacen == null)
                            {
                                inv_almacen = new Inventario();
                                inv_almacen.InventarioClave = Guid.NewGuid();
                                inv_almacen.ArticuloClave = inv_tecnico.ArticuloClave;
                                inv_almacen.EntidadClave = r.EntidadClaveAfectada;
                                inv_almacen.Cantidad = pi.Cantidad;
                                inv_almacen.Standby = 0;
                                inv_almacen.Estatus = 1;
                                inv_almacen.EstatusInv = 7;
                                dc.Inventario.Add(inv_almacen);
                            }
                            else
                            {
                                inv_almacen.Cantidad += pi.Cantidad;
                                inv_almacen.Estatus = 1;
                            }
                        }

                        //dc.SaveChanges();

                    }

                }


                //LIMPIAMOS EL PROCESO SUS ARTS Y SU INV
                List<ProcesoArticulo> proart2 = r.ProcesoArticulo.ToList();
                dc.ProcesoArticulo.RemoveRange(proart2);
                List<ProcesoInventario> proint = r.ProcesoInventario.ToList();
                dc.ProcesoInventario.RemoveRange(proint);
            }
            #endregion
            // dc.SaveChanges();



            //COMENZAMOS LA INSERCIÓN DE NUEVO
            foreach (var item in ((JArray)jObject["Inventario"]).Children())
            {


                ProcesoArticulo procesoarticulo = new ProcesoArticulo();
                procesoarticulo.ProcesoClave = r.ProcesoClave;
                var itemProperties = item.Children<JProperty>();
                var articuloclave = itemProperties.FirstOrDefault(x => x.Name == "ArticuloClave");
                procesoarticulo.ArticuloClave = Guid.Parse(articuloclave.Value.ToString());
                int salidas1 = 0;
                int catpedida = 0;
                if (articuloclave.Value != null)
                {

                    if (soysalidadistribuidor)
                    {
                        try
                        {


                            List<Proceso> salidas = (from b in dc.Proceso where b.ProcesoClaveRespuesta == r.ProcesoClaveRespuesta select b).ToList();
                            catpedida = (from d in dc.ProcesoArticulo where d.ProcesoClave == r.ProcesoClaveRespuesta && d.ArticuloClave == procesoarticulo.ArticuloClave select d.Cantidad.Value).FirstOrDefault();

                            foreach (var s in salidas)
                            {
                                foreach (var ss in s.ProcesoArticulo.Where(o => o.ArticuloClave == procesoarticulo.ArticuloClave))
                                {
                                    salidas1 += ss.Cantidad.Value;

                                }
                            }

                            if (salidas1 == catpedida)
                            {
                                return Content("-20");
                            }

                        }
                        catch { }
                    }

                    bool aprueba = false;
                    procesoarticulo.Cantidad = Convert.ToInt32(item["Inventario"].Children().Count());
                    int total = procesoarticulo.Cantidad.Value + salidas1;
                    if (catpedida >= total)
                    {
                        aprueba = true;
                    }

                    dc.ProcesoArticulo.Add(procesoarticulo);

                    var tieneseries = 0;

                    Guid guidart = Guid.Parse(articuloclave.Value.ToString());
                    Articulo articulo = (from ar in dc.Articulo
                                         where ar.ArticuloClave == guidart
                                         select ar).SingleOrDefault();

                    tieneseries = articulo.catTipoArticulo.SerieTipoArticulo.Count();



                    //PRODUCTOS CON SERIAL
                    if (Convert.ToInt32(tieneseries.ToString()) > 0)
                    {
                        foreach (var inventarioitem in item["Inventario"].Children())
                        {
                            string inventario = inventarioitem.ToString();
                            Guid inventarioclave = Guid.Parse(inventario);
                            Inventario inv = (from i in dc.Inventario
                                              where i.InventarioClave == inventarioclave
                                              select i).SingleOrDefault();
                            if (inv != null)
                            {
                                //SALIDA A DISTRIBUIDOR PARA ARTS CON SERIAL
                                if (distribuidor)
                                {
                                    if (aprueba)
                                    {




                                        if (inv.Standby > 0 && inv.Cantidad == 0)
                                        {


                                            procesoarticulo.Cantidad = procesoarticulo.Cantidad - 1;

                                        }
                                        else
                                        {
                                            inv.Cantidad = 0;
                                            inv.Standby = 1;
                                            inv.Estatus = 2;
                                            ProcesoInventario pi = (from proi in dc.ProcesoInventario
                                                                    where proi.InventarioClave == inv.InventarioClave
                                                                    && proi.ProcesoClave == r.ProcesoClave
                                                                    select proi).SingleOrDefault();
                                            if (pi != null)
                                            {
                                                pi.InventarioClave = inv.InventarioClave;
                                                pi.ProcesoClave = r.ProcesoClave;
                                                pi.Cantidad = 1;
                                                pi.Fecha = DateTime.Now;
                                            }
                                            else
                                            {
                                                pi = new ProcesoInventario();
                                                pi.InventarioClave = inv.InventarioClave;
                                                pi.ProcesoClave = r.ProcesoClave;
                                                pi.Cantidad = 1;
                                                pi.Fecha = DateTime.Now;
                                                dc.ProcesoInventario.Add(pi);

                                            }

                                        }

                                    }
                                    else
                                    {

                                        return Content("-30");

                                    }

                                }
                                else
                                {
                                    //SALIDA A TECNICO PARA ARTS CON SERIAL

                                    if (r.EntidadClaveAfectada == inv.EntidadClave)
                                    {
                                        inv.EntidadClave = r.EntidadClaveSolicitante;

                                        ProcesoInventario pi = new ProcesoInventario();

                                        pi.InventarioClave = inv.InventarioClave;
                                        pi.ProcesoClave = r.ProcesoClave;
                                        pi.Cantidad = 1;
                                        pi.Fecha = DateTime.Now;

                                        dc.ProcesoInventario.Add(pi);

                                        //dc.SaveChanges();




                                        /*INTERFACE HACIA SOFTV*/

                                        long idInventario = inv.IdInventario;


                                        Articulo art = inv.Articulo;

                                        catTipoArticulo tipo = art.catTipoArticulo;

                                        string serie = "";

                                        try
                                        {
                                            serie = inv.Serie.Select(x => x.Valor).First();
                                        }
                                        catch
                                        {
                                            serie = dc.Serie.Where(x => x.InventarioClave == inv.InventarioClave).Select(o => o.Valor).First().ToString();
                                        }

                                        Entidad plaza = (from e in dc.Entidad
                                                         where e.EntidadClave == r.EntidadClaveAfectada
                                                         select e).SingleOrDefault();


                                        Entidad tecnico = (from t in dc.Entidad
                                                           where t.EntidadClave == r.EntidadClaveSolicitante
                                                           select t).SingleOrDefault();
                                        try
                                        {

                                            //dsoft.SP_AltaCablemodems(Convert.ToInt32(inv.Entidad.catDistribuidor.IdDistribuidor), Convert.ToInt32(plaza.IdEntidad), Convert.ToInt32(tecnico.IdEntidad), Convert.ToInt32(inv.IdInventario), serie, art.catTipoArticulo.Letra, art.Nombre);
                                        }
                                        catch (Exception ex)
                                        {

                                        }

                                    }
                                    else
                                    {

                                        procesoarticulo.Cantidad = procesoarticulo.Cantidad - 1;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        int saliendo = 0;

                        try
                        {
                            saliendo = Int32.Parse(itemProperties.FirstOrDefault(x => x.Name == "Saliendo").Value.ToString());
                        }
                        catch
                        {
                            saliendo = Int32.Parse(itemProperties.FirstOrDefault(x => x.Name == "Cantidad").Value.ToString());
                        }

                        List<Inventario> inv_l = (from i in dc.Inventario
                                                  where i.ArticuloClave == procesoarticulo.ArticuloClave
                                                  && i.EntidadClave == r.EntidadClaveAfectada
                                                  && i.Estatus == 1 && i.EstatusInv == 7
                                                  select i).ToList();

                        Inventario inv = null;

                        if (inv_l.Count > 0)
                        {
                            inv = inv_l[0];
                        }

                        if (inv != null)
                        {
                            if (distribuidor)
                            {

                                /*se agrego esto*/


                                int totala = saliendo + salidas1;
                                if (catpedida >= totala)
                                {
                                    aprueba = true;
                                }
                                else
                                {
                                    aprueba = false;
                                }
                                /**/

                                if (aprueba)
                                {
                                    inv.Cantidad -= saliendo;
                                    inv.Standby += saliendo;

                                    ProcesoInventario pi = (from proi in dc.ProcesoInventario
                                                            where proi.InventarioClave == inv.InventarioClave
                                                            && proi.ProcesoClave == r.ProcesoClave
                                                            select proi).SingleOrDefault();

                                    if (pi != null)
                                    {
                                        pi.InventarioClave = inv.InventarioClave;
                                        pi.ProcesoClave = r.ProcesoClave;
                                        pi.Cantidad = saliendo;
                                        pi.Fecha = DateTime.Now;
                                    }
                                    else
                                    {
                                        pi = new ProcesoInventario();
                                        pi.InventarioClave = inv.InventarioClave;
                                        pi.ProcesoClave = r.ProcesoClave;
                                        pi.Cantidad = saliendo;
                                        pi.Fecha = DateTime.Now;

                                        dc.ProcesoInventario.Add(pi);
                                    }
                                }
                                else
                                {
                                    return Content("-40");
                                }



                            }
                            else
                            {
                                //Salida a técnico para articulos SIN SERIAL
                                if (inv.Cantidad.Value < saliendo)
                                {
                                    return Content("-50");
                                }

                                inv.Cantidad -= saliendo;

                                List<Inventario> invfinal_l = (from i in dc.Inventario
                                                               where i.ArticuloClave == procesoarticulo.ArticuloClave
                                                               && i.EntidadClave == r.EntidadClaveSolicitante
                                                               && i.Estatus == 1 && i.EstatusInv == 7
                                                               select i).ToList();

                                Inventario invfinal = null;

                                if (invfinal_l.Count > 0)
                                {
                                    invfinal = invfinal_l[0];
                                }

                                if (invfinal != null)
                                {
                                    invfinal.Cantidad += saliendo;
                                    invfinal.Standby = 0;
                                    invfinal.Estatus = 1;
                                }
                                else
                                {
                                    invfinal = new Inventario();
                                    invfinal.InventarioClave = Guid.NewGuid();
                                    invfinal.ArticuloClave = procesoarticulo.ArticuloClave;
                                    invfinal.EntidadClave = r.EntidadClaveSolicitante;
                                    invfinal.Cantidad = saliendo;
                                    invfinal.Standby = 0;
                                    invfinal.Estatus = 1;
                                    invfinal.EstatusInv = 7;
                                    dc.Inventario.Add(invfinal);
                                }




                                ProcesoInventario pi = new ProcesoInventario();

                                pi.InventarioClave = invfinal.InventarioClave;
                                pi.ProcesoClave = r.ProcesoClave;
                                pi.Cantidad = saliendo;
                                pi.Fecha = DateTime.Now;

                                dc.ProcesoInventario.Add(pi);

                                // dc.SaveChanges();
                            }
                        }
                        procesoarticulo.Cantidad = saliendo;
                    }
                }
            }

            dc.SaveChanges();


            try
            {
                var qsc = from pa in dc.ProcesoArticulo
                          join p in dc.Proceso on pa.ProcesoClave equals p.ProcesoClave
                          where p.ProcesoClaveRespuesta == r.ProcesoClaveRespuesta
                          && p.catEstatusClave != 6
                          group pa by pa.ArticuloClave into g
                          select new
                          {
                              ArticuloClave = g.Key,
                              Cantidad = g.Sum(pa => pa.Cantidad)
                          };

                int arts_pend = (from pa in dc.ProcesoArticulo
                                 join pan in qsc on pa.ArticuloClave equals pan.ArticuloClave into g
                                 from x in g.DefaultIfEmpty()
                                 where pa.ProcesoClave == r.ProcesoClaveRespuesta
                                 && pa.Cantidad != x.Cantidad
                                 select pa).ToList().Count();

                Proceso pedido = (from p in dc.Proceso
                                  where p.ProcesoClave == r.ProcesoClaveRespuesta
                                  select p).SingleOrDefault();

                if (pedido != null)
                {
                    pedido.catEstatusClave = (arts_pend == 0) ? 4 : 3;
                }

                dc.SaveChanges();
            }
            catch { }


            return Content(r.ProcesoClave.ToString());
            //return Json("{pedido:'" + r.ProcesoClave.ToString() + "'}", JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception ex)
            //{
            //    return Content("0");
            //}
        }

        private ArticuloProcesoWrapper PedidoToarticuloProcesoWrapper(ProcesoArticulo u)
        {
            ArticuloProcesoWrapper ap = new ArticuloProcesoWrapper();
            ap.Clave = u.ProcesoClave + "|" + u.ArticuloClave;
            ap.Nombre = u.Articulo.Nombre;
            ap.Cantidad = u.Cantidad.Value;

            ap.catSeriesClaves = new List<catSerieWrapper>();

            List<web.Models.SerieTipoArticulo> tiposarticulo = u.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();

            foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
            {
                catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                ap.catSeriesClaves.Add(csw);
            }


            try
            {
                ap.Surtida = u.CantidadEntregada.Value;
            }
            catch
            {
                ap.Surtida = 0;
            }

            return ap;
        }

        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }




        /// <summary>
        /// SE EMPAQUETA  EL PROCESO Y SE REGRESA COMO UNA LISTA
        /// </summary>
        /// <param name="u"></param>
        /// <param name="arts"></param>
        /// <returns></returns>

        private SalidaMaterialWrapper SalidaMaterialToWrapper(Proceso u, bool arts)
        {


            PedidoDetalle pd = u.PedidoDetalle;

            SalidaMaterialWrapper rw = new SalidaMaterialWrapper();
            rw.NumeroPedido = u.NumeroPedido.ToString();
            rw.ProcesoClave = u.ProcesoClave.ToString();
            rw.Clave = u.ProcesoClave.ToString();

            rw.EntidadClaveSolicitante = u.EntidadClaveSolicitante.Value.ToString();
            rw.EntidadClaveAfectada = u.EntidadClaveAfectada.Value.ToString();


            if (u.catTipoSalidaClave != null)
            {
                rw.TipoSalida = u.catTipoSalidaClave.ToString();
            }

            if (u.catProveedor != null)
            {
                catProveedor pro = u.catProveedor;
                rw.Origen = pro.Nombre;
            }
            else
            {
                rw.Origen = "";
            }

            if (u.ProcesoClaveRespuesta != null)
            {
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            else
            {
                rw.Pedido = String.Empty;
            }

            Entidad alm = u.Entidad;
            rw.Destino = alm.Nombre;

            catEstatus estado = u.catEstatus;
            rw.Estatus = estado.Nombre;

            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");

            // Utilizamos esta variable para almacenar estatus de Recepcion de Devolución
            // Si ya fue creado un proceso de Recepción (catTipoProceso = 6), ya no pueden
            // Hacer mas recepciones a la devolución.
            try
            {
                rw.Total_USD = (from p in dc.ProcesoEnvio
                                where p.ProcesoClave == u.ProcesoClave
                                select p).Count().ToString();
            }
            catch
            {
                rw.Total_USD = "0";
            }

            try
            {
                rw.Total = pd.Total.Value.ToString("C");
            }
            catch
            {
                rw.Total = String.Empty;
            }

            // Usamos esta variable para validar si ya existen los datos de envio capturados
            rw.DiasCredito = ((from pe in dc.ProcesoEnvio
                               where pe.ProcesoClave == u.ProcesoClave
                               select pe).Count() > 0) ? "1" : "0";

            // Si la variable arts es verdadera, entonces regresamos además la lista de productos de la salida
            // Principalmente para Ediciones
            if (arts)
            {
                List<ProcesoArticulo> articulos = u.ProcesoArticulo.ToList();

                List<ArticuloSalidaMaterialUI> apws = new List<ArticuloSalidaMaterialUI>();

                foreach (ProcesoArticulo pa in articulos)
                {
                    Articulo art = pa.Articulo;

                    ArticuloSalidaMaterialUI arui = new ArticuloSalidaMaterialUI();

                    arui.Clasificacion = art.catTipoArticulo.catClasificacionArticuloClave.ToString();
                    arui.TipoMaterial = art.catTipoArticulo.catTipoArticuloClave.ToString();

                    arui.ArticuloClave = art.ArticuloClave.ToString();
                    arui.ArticuloTexto = art.Nombre;
                    arui.Cantidad = pa.Cantidad.ToString();

                    if (pa.CantidadEntregada != null)
                    {
                        arui.Cantidad = pa.CantidadEntregada.ToString();
                    }
                    else
                    {
                        pa.Cantidad = 0;
                    }

                    int cuantos = (from c in dc.Inventario
                                   where c.ArticuloClave == art.ArticuloClave
                                   && c.EntidadClave == u.EntidadClaveAfectada
                                   select c.Cantidad.Value).ToList().Sum();


                    int standby = (from c in dc.Inventario
                                   where c.ArticuloClave == art.ArticuloClave
                                   && c.EntidadClave == u.EntidadClaveAfectada
                                   select c.Standby.Value).ToList().Sum();
                    arui.Existencias = cuantos.ToString();
                    //arui.Standby = standby.ToString();

                    List<ProcesoInventario> proinv = u.ProcesoInventario.ToList().Where(o => o.Inventario.ArticuloClave == pa.ArticuloClave).ToList();

                    List<string> inventario = new List<string>();

                    foreach (ProcesoInventario pi in proinv)
                    {
                        inventario.Add(pi.InventarioClave.ToString());
                    }

                    arui.Inventario = inventario;

                    apws.Add(arui);
                }

                rw.Articulos = apws;

            }

            return rw;
        }










        private ArticuloProcesoWrapper PedidoToarticuloProcesoWrapper(ProcesoArticulo u, Guid pProcesoP)
        {



            ArticuloProcesoWrapper ap = new ArticuloProcesoWrapper();
            ap.Clave = u.ProcesoClave + "|" + u.ArticuloClave;

            Articulo art = u.Articulo;

            ap.ClasificacionClave = art.catTipoArticulo.catClasificacionArticuloClave.ToString();
            ap.TipoArticuloClave = art.catTipoArticuloClave.ToString();
            ap.ArticuloClave = u.ArticuloClave.ToString();
            ap.Nombre = art.Nombre;
            ap.Cantidad = (u.Cantidad == null) ? 0 : u.Cantidad.Value;
            ap.PrecioUnitario = (u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value;
            ap.PrecioUnitarioTexto = ((u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value).ToString("C");


            try
            {
                ap.MonedaTexto = u.catTipoMoneda.Descripcion;
                ap.MonedaClave = u.catTipoMonedaClave.ToString();
            }
            catch
            {

            }

            Proceso Pedido = u.Proceso;
            ap.Surtida = 0;
            if (Pedido.catTipoProcesoClave == 1)
            {



                List<Proceso> salidas = (from s in dc.Proceso
                                         where s.ProcesoClaveRespuesta == Pedido.ProcesoClave
                                         && s.catTipoProcesoClave == 3 && s.catEstatusClave != 6
                                         select s).ToList();
                int cantidad = 0;
                foreach (Proceso s in salidas)
                {
                    List<ProcesoArticulo> proarts = s.ProcesoArticulo.ToList();

                    foreach (ProcesoArticulo pa in proarts)
                    {
                        if (pa.ArticuloClave == u.ArticuloClave)
                        {
                            cantidad += pa.Cantidad.Value;
                        }
                    }

                }

                ap.Surtida = cantidad;
            }



            ap.catSeriesClaves = new List<catSerieWrapper>();

            List<web.Models.SerieTipoArticulo> tiposarticulo = u.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();

            foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
            {
                catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                ap.catSeriesClaves.Add(csw);
            }

            ap.TieneSeries = tiposarticulo.Count();

            ap.Seriales = new List<SerialesWrapper>();




            int cuantos = (from c in dc.Inventario
                           where c.ArticuloClave == u.ArticuloClave
                           && c.EntidadClave == Pedido.EntidadClaveAfectada
                           select c.Cantidad.Value).ToList().Sum();


            ap.Existencia = cuantos;



            if (ap.Surtida == 0)
            {
                try
                {
                    ap.Surtida = u.CantidadEntregada.Value;
                    ap.Surtiendo = 0;
                }
                catch
                {
                    ap.Surtida = 0;
                    ap.Surtiendo = 0;
                }
            }



            List<ProcesoInventario> proinv = (from i in dc.ProcesoInventario
                                              where i.Pedido == u.ProcesoClave || i.ProcesoClave == u.ProcesoClave
                                              && i.Inventario.ArticuloClave == u.ArticuloClave
                                              select i).ToList();

            foreach (ProcesoInventario pro in proinv)
            {

                ap.Surtida -= pro.Cantidad.Value;
                ap.Surtiendo += pro.Cantidad.Value;

                Inventario inventario = pro.Inventario;

                List<Serie> series = inventario.Serie.ToList();

                SerialesWrapper sw = new SerialesWrapper();
                sw.RecepcionClave = pro.ProcesoClave.ToString();
                sw.InventarioClave = inventario.InventarioClave.ToString();
                sw.ArticuloClave = inventario.ArticuloClave.ToString();
                sw.EstatusTexto = "Buen Estado";
                sw.EstatusClave = inventario.Estatus.ToString();
                sw.Activo = 1;

                sw.datos = new List<SerialesDatosWrapper>();

                foreach (Serie s in series)
                {
                    SerialesDatosWrapper sdw = new SerialesDatosWrapper();
                    sdw.Clave = s.catSerieClave.ToString();
                    sdw.Valor = s.Valor.ToString();
                    sw.datos.Add(sdw);
                }

                ap.Seriales.Add(sw);
            }




            return ap;
        }

        public string getclaveInventario(string serie)
        {

            string inv = (from s in dc.Serie where s.Valor == serie select s.InventarioClave).FirstOrDefault().ToString();
            return inv;
        }

        public ActionResult Detalle()
        {

            if (Request["s"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["s"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso pro = (from p in dc.Proceso
                           where p.ProcesoClave == clave
                           select p).SingleOrDefault();

            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();


            Entidad e = pro.Entidad;
            Usuario u = pro.Usuario;

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");

            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h3><b>SALIDA DE MATERIAL</b></h3>");          
            sb.Append("<h3<b>Salida: #</b>" + pro.NumeroPedido + "</h3>");
            try
            {
                sb.Append(@"<b style=""font-size:10px;"">Tipo Salida:</b><span style=""font-size:10px;"">" + pro.catTipoSalida.Descripcion + "</span><br>");
            }catch { }
           
            sb.Append(@"<b style=""font-size:10px;"">Fecha: </b><span style=""font-size:10px;"">" + pro.Fecha.Value.ToShortDateString() + "</span><br>");


            if (pro.catEstatusClave == 6)
            {
                sb.Append("<FONT COLOR='#FF0000'>CANCELADA</FONT><br>");
            }

            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<font size=3>");
            sb.Append("<b>Enviar a: " + e.Nombre + "</b><br>");
            sb.Append("</font>");
            sb.Append("<font size=2>");
            sb.Append("<br>");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th style=""font-size:10px;""><b>Cantidad</b></th>");
            sb.Append(@"<th  style=""font-size:10px;"" colspan=""3""><b>Concepto</b></th>");
            sb.Append(@"<th style=""font-size:10px;"" ><b>Unidad</b></th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();
            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                List<Serie> seriesart = new List<Serie>();
                if (pa.Cantidad > 0)
                {
                    Articulo a = pa.Articulo;
                    sb.Append("<tr>");
                    sb.Append(@"<td style=""font-size:10px;"">" + pa.Cantidad + "</td>");
                    sb.Append(@"<td style=""font-size:10px;"" colspan=""3"">" + a.Nombre + "</td>");
                    sb.Append(@"<td style=""font-size:10px;"" >" + a.catUnidad.Nombre + "</td>");
                    sb.Append("</tr>");

                    sb.Append(@"<tr colspan=5>");
                    sb.Append(@"<td style=""font-size:8px;"">");
                    if (a.catTipoArticulo.SerieTipoArticulo.Count() > 0)
                    {

                        List<Serie> series = (from aa in dc.ProcesoInventario
                                              join bb in dc.Serie on aa.InventarioClave equals bb.InventarioClave
                                              join cc in dc.Inventario on bb.InventarioClave equals cc.InventarioClave
                                              where cc.ArticuloClave == a.ArticuloClave && aa.ProcesoClave == pro.ProcesoClave
                                              select bb
                                             ).ToList();
                        sb.Append("<b>Series:</b><br>");
                        series.ForEach(se =>
                        {
                            sb.Append("<span> " + se.Valor + " - </span>");
                        });
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</tbody>");
            sb.Append("</table>");
            //sb.Append("</td></tr></table>");
            sb.Append("<br>");
            sb.Append("<font size=2>");
            sb.Append("<b>Observaciones: </b><br>" + pro.Observaciones);
            sb.Append("</font>");

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "pedido.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Pedido" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "pedido.pdf" + "' style='position: relative; height: 90%; width: 90%;' ></iframe>";
                    return View();
                }
            }

        }


    }
}
