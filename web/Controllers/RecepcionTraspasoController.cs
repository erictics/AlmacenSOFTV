﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using Web.Controllers;

namespace web.Controllers
{
    public class RecepcionTraspasoController : Controller
    {
        //
        // GET: /RecepcionTraspaso/
         [SessionExpireFilterAttribute]
        public ActionResult Index()
        {

            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]);
            List<Entidad> entidades = null;
            entidades = (from a in dc.AmbitoAlmacen join b in dc.Entidad on a.EntidadClaveAlmacen equals b.EntidadClave where a.UsuarioClave == u.UsuarioClave select b).ToList();
            ViewData["almacenes"] = entidades;

            //
            List<catEntidadTipo> tipoalmacen = dc.catEntidadTipo.Where(x => x.catEntidadTipoClave != 2).ToList();
            ViewData["RolUsuario"] = u.RolClave;
            ViewData["tipoalmacen"] = tipoalmacen;
            //
            return View();
        }


        public class traspaso
        {
            public long pedido { get; set; }
            public string fecha { get; set; }
            public string almacen { get; set; }
            public string almacen2 { get; set; }
            public int estatus { get; set; }
            public Guid proceso { get; set; }
            public string Recepcion { get; set; }
            public List<Recepciones> Recepciones { get; set; }

        }


        public class series
        {
            public Guid ArticuloClave { get; set; }
            public Guid InventarioClave { get; set; }
            public string Nombre { get; set; }
            public string Statustxt { get; set; }
            public string Valor { get; set; }
            public string catSerieClave { get; set; }
            public string status { get; set; }
            public int Cantidad { get; set; }

        }

        public class lista_tras
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<traspaso> data { get; set; }
        }
        public ActionResult ListaTraspasos(string data, string almacen, int orden, int draw, int start, int length)
        {
            lista_tras dataTableData = new lista_tras();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerTraspasos(almacen, orden).Count;
            dataTableData.data = ObtenerTraspasos(almacen, orden).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<traspaso> ObtenerTraspasos(string almacen, int orden)
        {
            Usuario u = (Usuario)Session["u"];
            ModeloAlmacen dc = new ModeloAlmacen();
            List<traspaso> lista = new List<traspaso>();
            List<Proceso> dev;
            Guid alm;
            try
            {
                alm = Guid.Parse(almacen);
            }
            catch
            {
                alm = Guid.Empty;
            }

            dev = (from a in dc.Proceso
                   join b in dc.AmbitoAlmacen on a.EntidadClaveAfectada equals b.EntidadClaveAlmacen
                   where b.UsuarioClave == u.UsuarioClave && a.catEstatusClave != 6
                   && (alm == Guid.Empty || (a.EntidadClaveAfectada == alm))
                   && (orden == 0 || (a.NumeroPedido == orden))
                   && a.catTipoProcesoClave == 16
                   select a).ToList();


            foreach (var a in dev)
            {
                traspaso d = new traspaso();
                Entidad ent = dc.Entidad.Where(x => x.EntidadClave == a.EntidadClaveAfectada).First();
                Entidad ent2 = dc.Entidad.Where(x => x.EntidadClave == a.EntidadClaveSolicitante).First();
                d.almacen = ent.Nombre;
                d.almacen2 = ent2.Nombre;
                d.pedido = a.NumeroPedido;
                d.estatus = a.catEstatusClave.Value;
                d.fecha = a.Fecha.ToString();
                d.proceso = a.ProcesoClave;
                d.Recepciones = dc.Proceso.Where(o => o.ProcesoClaveRespuesta == a.ProcesoClave).Select(i => new Recepciones { Clave = i.ProcesoClave, NumeroPedido = i.NumeroPedido, Fecha = i.Fecha.Value }).ToList();
                try
                {
                    d.Recepcion = dc.Proceso.Where(p => p.ProcesoClaveRespuesta == a.ProcesoClave).Select(y => y.ProcesoClave).First().ToString();
                }
                catch
                {
                    d.Recepcion = "";
                }
                lista.Add(d);

            }
            return lista.OrderByDescending(x => x.pedido).ToList();
        }

        public class DetTraspasoRec
        {
            public long NumeroPedido { get; set; }
            public string Fecha { get; set; }
            public string Usuario { get; set; }

            public Guid Traspaso { get; set; }

            public List<DetArticulo> DetArticulo { get; set; }

        }

        public class DetArticulo
        {

            public string Articulo { get; set; }
            public int cantidad { get; set; }
        }



        public ActionResult DetalleAvenzadoRecepcion(Guid idtraspaso)
        {
            List<DetTraspasoRec> listarec = new List<DetTraspasoRec>();
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Proceso> lista = dc.Proceso.Where(x => x.ProcesoClaveRespuesta == idtraspaso).ToList();
            foreach (var a in lista)
            {
                DetTraspasoRec dt = new DetTraspasoRec();
                dt.NumeroPedido = a.NumeroPedido;
                dt.Traspaso = a.ProcesoClave;
                dt.Usuario = dc.Usuario.Where(x => x.UsuarioClave == a.UsuarioClave).Select(o => o.Nombre).First();
                dt.Fecha = a.Fecha.ToString();
                dt.DetArticulo = a.ProcesoArticulo.Select(x => new DetArticulo { Articulo = x.Articulo.Nombre, cantidad = x.Cantidad.Value }).ToList();
                listarec.Add(dt);
            }
            return Json(listarec, JsonRequestBehavior.AllowGet);
        }


        public class Recepciones
        {
            public long NumeroPedido { get; set; }
            public DateTime Fecha { get; set; }

            public Guid Clave { get; set; }
        }

        public class DetalleTrasp
        {
            public string Nombre { get; set; }
            public int Cantidad { get; set; }
            public Guid ArticuloClave { get; set; }
            public Guid ProcesoClave { get; set; }
            public long NumeroPedido { get; set; }
            public int? catSerieClaveInterface { get; set; }
            public int pendiente { get; set; }



        }

        public ActionResult DetalleTraspaso(Guid idTraspaso)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Proceso traspaso = dc.Proceso.Where(x => x.ProcesoClave == idTraspaso).First();
            List<ProcesoArticulo> articulos = traspaso.ProcesoArticulo.ToList();
            List<DetalleTrasp> ldt = new List<DetalleTrasp>();
            foreach (var a in articulos)
            {
                DetalleTrasp dt = new DetalleTrasp();
                dt.ArticuloClave = a.ArticuloClave;
                dt.Cantidad = a.Cantidad.Value;
                dt.catSerieClaveInterface = a.Articulo.catTipoArticulo.catSerieClaveInterface;
                dt.Nombre = a.Articulo.Nombre;
                dt.NumeroPedido = a.Proceso.NumeroPedido;
                dt.ProcesoClave = a.ProcesoClave;


                int cantidad = 0;
                try
                {
                    cantidad = (from ad in dc.Proceso
                                join b in dc.ProcesoArticulo on ad.ProcesoClave equals b.ProcesoClave
                                where ad.catEstatusClave != 6 && ad.ProcesoClaveRespuesta == traspaso.ProcesoClave && b.ArticuloClave == a.ArticuloClave
                                select b.Cantidad).Sum(o => o.Value);
                }
                catch
                {

                }
                dt.pendiente = a.Cantidad.Value - cantidad;
                ldt.Add(dt);
            }

            return Json(ldt, JsonRequestBehavior.AllowGet);
        }

        public ActionResult aparatos(Guid idtraspaso, Guid idarticulo, string Articulos, int draw, int start, int length)
        {

            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series> series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(idtraspaso, idarticulo, series).Count;
            dataTableData.data = ObtenerAparatos(idtraspaso, idarticulo, series).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Aparatos> ObtenerAparatos(Guid idtraspaso, Guid idarticulo, List<series> listaseries)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Aparatos> lista = (from a in dc.Proceso
                                    join b in dc.ProcesoInventario on a.ProcesoClave equals b.ProcesoClave
                                    join c in dc.Inventario on b.InventarioClave equals c.InventarioClave
                                    join d in dc.Serie on c.InventarioClave equals d.InventarioClave
                                    where a.ProcesoClave == idtraspaso && c.ArticuloClave == idarticulo
                                    && c.Cantidad.Value == 0 && c.Standby.Value == 1
                                    select new Aparatos
                                    {
                                        ArticuloClave = c.ArticuloClave.Value,
                                        estatus = c.EstatusInv.Value,
                                        InventarioClave = c.InventarioClave,
                                        Nombre = c.Articulo.Nombre,
                                        valor = d.Valor,
                                        Plaza = c.EntidadClave.Value.ToString()

                                    }
                                      ).ToList();
            foreach (var a in lista)
            {
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus = Int32.Parse(s.status);
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }


        public ActionResult Guarda(Guid id, string articulos)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Proceso traspaso;
                    try
                    {
                        traspaso = dc.Proceso.Where(x => x.ProcesoClave == id).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-2");
                    }


                    if (traspaso.catEstatusClave == 4 || traspaso.catEstatusClave == 6)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-1");
                    }


                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    Usuario u = (Usuario)Session["u"];
                    Proceso p = new Proceso();
                    p.ProcesoClave = Guid.NewGuid();
                    p.catEstatusClave = 4;
                    p.catTipoProcesoClave = 17;
                    p.EntidadClaveSolicitante = traspaso.EntidadClaveAfectada;
                    p.EntidadClaveAfectada = traspaso.EntidadClaveSolicitante;
                    p.Fecha = DateTime.Now;
                    p.UsuarioClave = u.UsuarioClave;
                    p.ProcesoClaveRespuesta = traspaso.ProcesoClave;
                    dc.Proceso.Add(p);


                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g
                                     select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach (var a in result)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = p.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                    }


                    foreach (var Articulo in series)
                    {


                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            Inventario Invent = new Inventario();
                            try
                            {
                                Invent = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == traspaso.EntidadClaveSolicitante && o.EstatusInv == 7 && o.Estatus == 4).First();
                            }
                            catch
                            {
                                dbContextTransaction.Rollback();
                                return Content("-4");
                            }
                            ProcesoInventario pi = new ProcesoInventario();
                            pi.ProcesoClave = p.ProcesoClave;
                            pi.InventarioClave = Articulo.InventarioClave;
                            pi.Cantidad = Articulo.Cantidad;
                            pi.Fecha = DateTime.Now;
                            dc.ProcesoInventario.Add(pi);

                            Invent.EntidadClave = traspaso.EntidadClaveAfectada;
                            Invent.Standby = 0;
                            Invent.Cantidad = 1;
                            Invent.Estatus = 1;
                        }
                        else
                        {
                            Inventario AlmOriginal = new Inventario();
                            Inventario invalm = new Inventario();
                            try
                            {
                                AlmOriginal = dc.Inventario.Where(x => x.EntidadClave == traspaso.EntidadClaveSolicitante && x.ArticuloClave == Articulo.ArticuloClave && x.EstatusInv == 7).First();
                            }
                            catch
                            {
                                dbContextTransaction.Rollback();
                                return Content("-5");

                            }


                            try
                            {
                                invalm = dc.Inventario.Where(x => x.EntidadClave == p.EntidadClaveSolicitante && x.ArticuloClave == Articulo.ArticuloClave && x.EstatusInv == 7).First();

                                ProcesoInventario pi = new ProcesoInventario();
                                pi.ProcesoClave = p.ProcesoClave;
                                pi.InventarioClave = invalm.InventarioClave;
                                pi.Cantidad = Articulo.Cantidad;
                                pi.Fecha = DateTime.Now;
                                dc.ProcesoInventario.Add(pi);

                            }
                            catch
                            {

                                Inventario i = new Inventario();
                                i.InventarioClave = Guid.NewGuid();
                                i.ArticuloClave = Articulo.ArticuloClave;
                                i.EntidadClave = p.EntidadClaveSolicitante;
                                i.Cantidad = 0;
                                i.Standby = 0;
                                i.Estatus = 1;
                                i.EstatusInv = 7;
                                dc.Inventario.Add(i);
                                invalm = i;

                                ProcesoInventario pi = new ProcesoInventario();
                                pi.ProcesoClave = p.ProcesoClave;
                                pi.InventarioClave = i.InventarioClave;
                                pi.Cantidad = Articulo.Cantidad;
                                pi.Fecha = DateTime.Now;
                                dc.ProcesoInventario.Add(pi);
                            }
                            invalm.Cantidad = invalm.Cantidad.Value + Articulo.Cantidad;
                            AlmOriginal.Standby = AlmOriginal.Standby - Articulo.Cantidad;

                        }

                    }
                    dc.SaveChanges();


                    Proceso pr = dc.Proceso.Where(o => o.ProcesoClave == traspaso.ProcesoClave).First();
                    List<ProcesoArticulo> articulosTraspaso = pr.ProcesoArticulo.ToList();
                    int cant = 0;

                    foreach (var articulo in articulosTraspaso)
                    {

                        int cantidad = 0;
                        try
                        {
                            cantidad = (from a in dc.Proceso
                                        join b in dc.ProcesoArticulo on a.ProcesoClave equals b.ProcesoClave
                                        where a.catEstatusClave != 6 && a.ProcesoClaveRespuesta == traspaso.ProcesoClave && b.ArticuloClave == articulo.ArticuloClave
                                        select b.Cantidad).Sum(o => o.Value);
                        }
                        catch
                        {
                        }
                        if (articulo.Cantidad != cantidad)
                        {
                            cant = cant + 1;
                        }
                    }
                    if (cant > 0)
                    {
                        traspaso.catEstatusClave = 3;
                    }
                    else
                    {
                        traspaso.catEstatusClave = 4;
                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);

                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");
                }
            }
        }




        public bool tieneSeries(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();


            if (Request["d"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["d"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso pro = (from p in dc.Proceso
                           where p.ProcesoClave == clave
                           select p).SingleOrDefault();

            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();

            Entidad eS = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveSolicitante
                          select e).SingleOrDefault();
            Entidad eA = (from e in dc.Entidad
                          where e.EntidadClave == pro.EntidadClaveAfectada
                          select e).SingleOrDefault();
            Usuario u = pro.Usuario;

            catEstatus vEst = (from e in dc.catEstatus
                               where e.catEstatusClave == pro.catEstatusClave
                               select e).SingleOrDefault();

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Recepción de traspaso de material: #" + pro.NumeroPedido + "</h3><br>");



            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<font size=2>");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("<b>Fecha: " + pro.Fecha + "</b><br>");
            sb.Append("<b>Tipo de Proceso: " + pro.catTipoProceso.Nombre + "</b><br>");
            sb.Append("<b>Estatus Proceso: " + vEst.Nombre + "</b><br>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");

            if (pro.catTipoProcesoClave == 13)
            {
                sb.Append("<b>Motivo: " + pro.Observaciones + "</b><br>");
            }
            sb.Append("<br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th style=""font-size:10px;text-decoration: underline;"" align=""center""><b>Cantidad</b></th>");
            sb.Append(@"<th style=""font-size:10px;text-decoration: underline;"" align=""center"" colspan=""3""><b>Concepto</b></th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                if (pa.Cantidad > 0)
                {
                    Articulo a = pa.Articulo;

                    sb.Append("<tr>");
                    sb.Append(@"<td style=""font-size:9px;"" align=""center""><b>" + pa.Cantidad + "</b></td>");
                    sb.Append(@"<td style=""font-size:9px;"" align=""center"" colspan=""3""><b>" + a.Nombre + "</b></td>");
                    sb.Append("<br>");

                    foreach (ProcesoInventario pi in procesoinventario)
                    {
                        Inventario i = pi.Inventario;
                        if (i.ArticuloClave == a.ArticuloClave)
                        {
                            List<Serie> series = i.Serie.ToList();
                            int conta = 0;
                            var innerCount = 0;
                            foreach (Serie s in series)
                            {
                                if (innerCount > 0)
                                {
                                    sb.Append("-");
                                }
                                string val = "";
                                if (s.Inventario.EstatusInv == 7)
                                {
                                    val = "Buen Estado";
                                }
                                else if (s.Inventario.EstatusInv == 9)
                                {
                                    val = "Revisión";
                                }
                                else if (s.Inventario.EstatusInv == 10)
                                {
                                    val = "Dañado";
                                }
                                else
                                {
                                    val = "Baja";
                                }
                                sb.Append(s.Valor + "->" + val);
                                innerCount++;
                            }

                            conta++;


                            if (conta > 0)
                            {
                                sb.Append("<br>");
                            }
                        }
                    }
                }
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");

            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "_recepcion.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Dev-AlmCentral_" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "_recepcion.pdf" + "' style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }
        }



    }
}
