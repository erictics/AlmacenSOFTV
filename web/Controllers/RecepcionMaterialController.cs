﻿
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using web.Controllers;
using web.Models;
using web.Models.Newsoftv;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class RecepcionMaterialController : Controller
    {
        
        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();           
            Usuario u = (Usuario)Session["u"];
            Guid? clavedistribuidor = null;
            catDistribuidor d = null;          
            if (u.DistribuidorClave != null)
            {
                d = u.catDistribuidor;
                clavedistribuidor = d.catDistribuidorClave;
            }

          

            List<Entidad> almacenes = (from e in dc.Entidad
                                           where (e.catDistribuidorClave == clavedistribuidor)
                                           && e.Activo.Value && e.catEntidadTipoClave == 1
                                           select e).ToList();

            ViewData["almacenes"] = almacenes;         
            List<catProveedor> proveedores = (from ta in dc.catProveedor
                                                  where ta.Activo
                                                  orderby ta.Nombre
                                                  select ta).ToList();

            ViewData["proveedores"] = proveedores;            
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();           
            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();
            List<catTipoMoneda> tiposmoneda = (from ta in dc.catTipoMoneda
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();            
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["tiposmoneda"] = tiposmoneda;
            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");         
            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;
        
        public ActionResult Pedidos(string data)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());
            Guid proveedor = Guid.Empty;
            try
            {
                proveedor = Guid.Parse(jObject["proveedor"].ToString());
            }
            catch
            {
            }
            string vActivos = string.Empty;
            try
            {
                vActivos = jObject["act"].ToString();
            }
            catch {
                vActivos = "0";
            }

            int skip = (pag - 1) * elementos;
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = ((Usuario)Session["u"]); 
            List<Proceso> rls = null;           
            if (u.DistribuidorClave == null)
            {                          

                rls = (from use in dc.Proceso
                       where use.catTipoProcesoClave == 1
                       && (vActivos == "1" && (use.catEstatusClave == 2 || use.catEstatusClave == 3)
                       || vActivos == "0")
                       && (use.catProveedorClave == proveedor || proveedor == Guid.Empty)
                       && use.catDistribuidorClave == null
                       orderby use.Fecha descending
                       select use).ToList();
            }
            
            else
            {               
                rls = (from use in dc.Proceso
                       where
                       use.catDistribuidorClave == u.DistribuidorClave
                       && use.catTipoProcesoClave == 1
                       && (vActivos == "1" && (use.catEstatusClave == 2 || use.catEstatusClave == 3)
                       || vActivos == "0")
                       && (use.catProveedorClave == proveedor || proveedor == Guid.Empty)
                       orderby use.Fecha descending
                       select use).ToList();
            }          
            List<RecepcionWrapper> rw = new List<RecepcionWrapper>();
            foreach (Proceso p in rls)
            {              
                RecepcionWrapper w = RecepcionToWrapper(p,false);
                rw.Add(w);
            }
            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            int pags = 0;
            Usuario u = ((Usuario)Session["u"]);
            if (u.DistribuidorClave == null)
            {
                pags = (from ro in dc.Proceso
                        where ro.catTipoProcesoClave == 2
                        select ro).Count();
            }
            else
            {
                pags = (from ro in dc.Proceso
                        where
                        ro.catDistribuidorClave == u.DistribuidorClave
                        && ro.catTipoProcesoClave == 2
                        select ro).Count();
            }
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div).ToString());
            }
            else
            {
                return Content((div + 1).ToString());
            }
        }


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<RecepcionWrapper> data { get; set; }
        }
        
        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            string tiporecepcion = jObject["tiporecepcion"].ToString();
            string buscaproveedor = jObject["buscaproveedor"].ToString();
            string abuscar = jObject["abuscar"].ToString();
            Guid gproveedor = Guid.Empty;
            try
            {
                gproveedor = Guid.Parse(buscaproveedor);
            }
            catch
            {

            }
            int iabuscar = 0;            
            try
            {
                iabuscar = Convert.ToInt32(abuscar);
            }
            catch
            {
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData(ref recordsFiltered, start, length, tiporecepcion, gproveedor, iabuscar);
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        private List<RecepcionWrapper> FilterData(ref int recordFiltered, int  start, int  length, string tiporecepcion, Guid gproveedor, int iabuscar)
        {
            ModeloAlmacen dc = new ModeloAlmacen();          
            Usuario u = ((Usuario)Session["u"]);
            List<Proceso> rls = null;
            if (tiporecepcion == "SP")
            {
                rls = (from us in dc.Proceso join b in dc.AmbitoAlmacen on us.EntidadClaveSolicitante
                       equals b.EntidadClaveAlmacen
                       where us.catTipoProcesoClave == 2 &&
                       us.ProcesoClaveRespuesta == null
                       && b.UsuarioClave==u.UsuarioClave
                       && (iabuscar == 0 || us.NumeroPedido == iabuscar)
                       orderby us.NumeroPedido descending
                       select us).ToList();
            }
            else if (tiporecepcion == "CP")
            {
                rls = (from ua in dc.Proceso
                       join b in dc.AmbitoAlmacen on ua.EntidadClaveSolicitante equals b.EntidadClaveAlmacen
                       where ua.catTipoProcesoClave == 2 &&
                       ua.ProcesoClaveRespuesta != null
                       && b.UsuarioClave == u.UsuarioClave
                       && (iabuscar==0||ua.NumeroPedido== iabuscar)
                       orderby ua.NumeroPedido descending
                       select ua).Skip(start).Take(length).ToList();
            }else
            {
                rls = (from ua in dc.Proceso
                       join b in dc.AmbitoAlmacen on ua.EntidadClaveSolicitante equals b.EntidadClaveAlmacen
                       where ua.catTipoProcesoClave == 2                        
                       && b.UsuarioClave == u.UsuarioClave                       
                       orderby ua.NumeroPedido descending
                       select ua).Skip(start).Take(length).ToList();
            }
            recordFiltered = rls.Count;
            List<RecepcionWrapper> rw = new List<RecepcionWrapper>();
            foreach (Proceso p in rls.Skip(start).Take(length).ToList())
            {
                RecepcionWrapper w = RecepcionToWrapperLISTA(p, true);
                rw.Add(w);
            }
            return rw;
        }

        public ActionResult Obten(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            Guid recepcionid = Guid.Empty;
            recepcionid = Guid.Parse(jObject["recepcion"].ToString());

            Proceso recepcion = (from r in dc.Proceso
                                     where r.ProcesoClave == recepcionid
                                     select r).SingleOrDefault();
            RecepcionWrapper rw = new RecepcionWrapper();
            if (recepcion != null)
            {
                rw = RecepcionToWrapper(recepcion, true);
            }
            return Json(rw, JsonRequestBehavior.AllowGet);

        }

        private RecepcionWrapper RecepcionToWrapperLISTA(Proceso u, bool arts)
        {
           
            ModeloAlmacen dc = new ModeloAlmacen();        
            RecepcionWrapper rw = new RecepcionWrapper();           
            rw.NumeroPedido = u.NumeroPedido.ToString();           
            rw.Clave = u.ProcesoClave.ToString();            
            rw.AlmacenClave = u.EntidadClaveSolicitante.ToString();          
            rw.ProveedorClave = u.catProveedorClave.ToString();
            PedidoDetalle pd = (from pde in dc.PedidoDetalle
                                    where pde.ProcesoClave == u.ProcesoClave
                                    select pde).SingleOrDefault();
            if (pd != null)
            {               
                rw.TipoCambio = pd.TipoCambio.Value.ToString("N2");            
                rw.Iva = pd.IVA.Value.ToString("N2");              
                rw.Descuento = pd.Descuento.Value.ToString("N2");                
                rw.DiasCredito = pd.DiasCredito.Value.ToString("N0");
            }
            if (u.catProveedor != null)
            {
                rw.Proveedor = u.catProveedor.Nombre;
            }

            if (u.ProcesoClaveRespuesta != null)
            {                
                rw.ClavePedido = u.ProcesoClaveRespuesta.ToString();               
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            else
            {
                rw.ClavePedido = String.Empty;
            }

            catProveedor pro = u.catProveedor;
            if (pro != null)
            {               
                rw.Origen = pro.Nombre;
            }
            else
            {
                rw.Origen = "";
            }

            Entidad alm = u.Entidad;
            rw.Destino = alm.Nombre;

            catEstatus estado = u.catEstatus;         
            rw.Estatus = estado.Nombre;          
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");
            return rw;
        }

        private RecepcionWrapper RecepcionToWrapper(Proceso u, bool arts)
        {
            ModeloAlmacen dc = new ModeloAlmacen();           
            RecepcionWrapper rw = new RecepcionWrapper();            
            rw.NumeroPedido = u.NumeroPedido.ToString();           
            rw.Clave = u.ProcesoClave.ToString();           
            rw.AlmacenClave = u.EntidadClaveSolicitante.ToString();           
            rw.ProveedorClave = u.catProveedorClave.ToString();
            PedidoDetalle pd = (from pde in dc.PedidoDetalle
                                        where pde.ProcesoClave == u.ProcesoClave
                                        select pde).SingleOrDefault();
            if(pd!=null)
            {               
                rw.TipoCambio = pd.TipoCambio.Value.ToString("N2");               
                rw.Iva = pd.IVA.Value.ToString("N2");              
                rw.Descuento = pd.Descuento.Value.ToString("N2");             
                rw.DiasCredito = pd.DiasCredito.Value.ToString("N0");
            }
            if (u.catProveedor != null)
            {
               rw.Proveedor = u.catProveedor.Nombre;
            }

            if (u.ProcesoClaveRespuesta != null)
            {
                rw.ClavePedido = u.ProcesoClaveRespuesta.ToString();               
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            else
            {               
                rw.ClavePedido = String.Empty;
            }
            catProveedor pro = u.catProveedor;
            if (pro != null)
            {               
                rw.Origen = pro.Nombre;
            }
            else
            {
                rw.Origen = "";
            }

            Entidad alm = u.Entidad;
            rw.Destino = alm.Nombre;
            catEstatus estado = u.catEstatus;           
            rw.Estatus = estado.Nombre;          
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");
            if (arts)
            {               
                List<ProcesoArticulo> articulos = u.ProcesoArticulo.ToList();
                List<ArticuloProcesoWrapper> apws = new List<ArticuloProcesoWrapper>();
                foreach (ProcesoArticulo a in articulos)
                {
                    ArticuloProcesoWrapper apw = PedidoToarticuloProcesoWrapper(a, u.ProcesoClave);
                    apws.Add(apw);
                }
                rw.Articulos = apws;

            }          
            return rw;
        }

        public ActionResult Cancela(string data)
        {
           ModeloAlmacen dc = new ModeloAlmacen();
           using (var dbContextTransaction = dc.Database.BeginTransaction())
           {
               try
               {
                   JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                   string ed_id = String.Empty;
                   if (jObject["ed_id"] != null)
                   {
                       ed_id = jObject["ed_id"].ToString();
                   }                   
                   Guid uid = Guid.Parse(ed_id);
                   Proceso pedido;
                   Proceso recepcion = dc.Proceso.Where(x => x.ProcesoClave == uid).First();
                   if (recepcion.catEstatusClave==6)
                   {
                       dbContextTransaction.Rollback(); 
                       return Content("100");
                   }
                   List<ProcesoInventario> arts = dc.ProcesoInventario.Where(r => r.ProcesoClave == recepcion.ProcesoClave).ToList();
                   foreach (ProcesoInventario a in arts)
                   {                      
                       try
                       {
                           pedido = dc.Proceso.Where(x => x.ProcesoClave == recepcion.ProcesoClaveRespuesta).First();
                           ProcesoArticulo p=pedido.ProcesoArticulo.Where(x=>x.ArticuloClave==a.Inventario.ArticuloClave).First();
                           p.CantidadEntregada = p.CantidadEntregada - a.Cantidad;
                       }
                       catch { }
                       if (a.Inventario.Articulo.catTipoArticulo.catSerieClaveInterface==null)
                       {
                           Inventario inventarioAP = dc.Inventario.Where(o => o.EntidadClave == recepcion.EntidadClaveSolicitante && o.ArticuloClave == a.Inventario.ArticuloClave && o.EstatusInv == 7).First();
                           if (a.Cantidad > inventarioAP.Cantidad)
                           {
                               dbContextTransaction.Rollback();      
                               return Content("200A");
                           }
                           else
                           {
                               ProcesoArticulo pa = dc.ProcesoArticulo.Where(p => p.ArticuloClave == a.Inventario.ArticuloClave && p.ProcesoClave == a.ProcesoClave).First();
                               dc.ProcesoInventario.Remove(a);                               
                               inventarioAP.Cantidad = inventarioAP.Cantidad - a.Cantidad;
                           }
                       }
                       else
                       {
                           if (a.Inventario.EntidadClave == recepcion.EntidadClaveSolicitante && a.Inventario.Standby == 0 && a.Inventario.EstatusInv == 7)
                           {
                               Serie serie = dc.Serie.Where(i => i.InventarioClave == a.InventarioClave).First();
                               Inventario inventario = dc.Inventario.Where(m => m.InventarioClave == a.InventarioClave).First();
                               ProcesoArticulo procesoart = dc.ProcesoArticulo.Where(u => u.ProcesoClave == a.ProcesoClave && u.ArticuloClave == a.Inventario.ArticuloClave).First();
                               dc.Serie.Remove(serie);
                               dc.Inventario.Remove(inventario);
                               dc.ProcesoInventario.Remove(a);                     

                           }
                           else
                           {
                               dbContextTransaction.Rollback();      
                               return Content("200B");

                           }
                       }                      

                   }
                   Usuario u1 = (Usuario)Session["u"];
                   recepcion.catEstatusClave = 6;
                   recepcion.Observaciones = "Fecha de cancelación " + DateTime.Now + " Usuario que canceló esta recepción " + u1.Nombre;
                   dc.SaveChanges();

                   dbContextTransaction.Commit();
                   return Content("ok");
               }
               catch
               {
                   dbContextTransaction.Rollback();                   
                   return Content("0");
               }
           }

        }


        public bool TieneArticulos(string data)
        {
            int cant = 0;
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            foreach (var ritem in ((JArray)jObject["Articulos"]).Children())
            {
                var itemProperties = ritem.Children<JProperty>();
                if (Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "TieneSeries").Value.ToString().Replace("s", "")) == 0)
                {
                    try
                    {
                        cant += Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").ToString());
                    }
                    catch
                    {
                       cant+= Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").Value.ToString().Replace("s", ""));
                    }
                  
                }
                else
                {
                    JArray Seriales = (JArray)itemProperties.FirstOrDefault(x => x.Name == "Series").Value;
                    List<SeriesRecepcion> seriesRec2 = Seriales.ToObject<List<SeriesRecepcion>>();
                    cant = seriesRec2.Count;

                }
            }
            if (cant > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public ActionResult Guarda(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            bool existe_serie = false;
            try { 
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            foreach (var ritem in ((JArray)jObject["Articulos"]).Children())
            {
                var itemProperties = ritem.Children<JProperty>();
                if (Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "TieneSeries").Value.ToString().Replace("s", "")) == 0)
                { }
                else
                {
                    JArray Seriales = (JArray)itemProperties.FirstOrDefault(x => x.Name == "Series").Value;
                    List<SeriesRecepcion> seriesRec2 = Seriales.ToObject<List<SeriesRecepcion>>();
                    List<string> TodasSeries = seriesRec2.Select(x=>x.datos[0].valor).ToList();
                    var filteredDatabase = dc.Serie.Where(a => TodasSeries.Contains(a.Valor)).ToList();
                    if (filteredDatabase.Count>0)
                    {
                        return Content("300");
                    }
                }
              }
            }
            catch
            {
                return Content("0");
            }
            using (var dbContextTransaction = new TransactionScope())
            {
                try
                {
                    #region SACANDO INFROMACION DE LA RECEPCION
                    Usuario u = (Usuario)Session["u"];
                    JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                    string ed_id = String.Empty;

                    if (jObject["ed_id"] != null)
                    {
                        ed_id = jObject["ed_id"].ToString();
                    }
                    string ed_almacen = String.Empty;
                    if (jObject["Almacen"] != null)
                    {
                        ed_almacen = jObject["Almacen"].ToString();
                    }

                    string ed_proveedor = String.Empty;
                    if (jObject["Proveedor"] != null)
                    {
                        ed_proveedor = jObject["Proveedor"].ToString();
                    }

                    string tipocambiosinpedido = String.Empty;
                    if (jObject["TipoCambio"] != null)
                    {
                        tipocambiosinpedido = jObject["TipoCambio"].ToString();
                    }

                    string ivasinpedido = String.Empty;
                    if (jObject["Iva"] != null)
                    {
                        ivasinpedido = jObject["Iva"].ToString();
                    }

                    string descuentosinpedido = String.Empty;
                    if (jObject["Descuento"] != null)
                    {
                        descuentosinpedido = jObject["Descuento"].ToString();
                    }

                    string diascreditosinpedido = String.Empty;
                    if (jObject["DiasCredito"] != null)
                    {
                        diascreditosinpedido = jObject["DiasCredito"].ToString();
                    }

                    string Observaciones = "";
                    if (jObject["Observaciones"] != null)
                    {
                        Observaciones = jObject["Observaciones"].ToString();
                    }

                    Guid ed_pedido = Guid.Empty;
                    if (jObject["ProcesoClaveRespuesta"] != null && jObject["ProcesoClaveRespuesta"].ToString() != "")
                    {
                        try
                        {
                            ed_pedido = Guid.Parse(jObject["ProcesoClaveRespuesta"].ToString());
                        }
                        catch
                        {

                        }
                    }
                    #endregion

                    #region GUARDANDO PROCESO

                    Proceso r = new Proceso();
                    r.ProcesoClave = Guid.NewGuid();
                    r.catEstatusClave = 1; // por autorizar
                    r.catTipoProcesoClave = 2;//por ser recepcion
                    r.EntidadClaveSolicitante = Guid.Parse(ed_almacen);
                    r.Observaciones = Observaciones;
                    Proceso Pedido = (from p in dc.Proceso
                                          where p.ProcesoClave == ed_pedido
                                          select p).SingleOrDefault();
                    Guid pedidoclave = Guid.Empty;
                    if (Pedido != null)
                    {
                        pedidoclave = Pedido.ProcesoClave;
                    }
                    if (ed_pedido != Guid.Empty)
                    {
                        r.ProcesoClaveRespuesta = ed_pedido;
                    }
                    r.UsuarioClave = u.UsuarioClave;
                    r.Fecha = DateTime.Now;
                    dc.Proceso.Add(r);
                    #endregion


                    #region SI LA RECEPCION NO TIENE PEDIDO
                    if (Pedido == null)
                    {

                        r.catProveedorClave = Guid.Parse(ed_proveedor);
                        PedidoDetalle detalle = null;
                        detalle = (from d in dc.PedidoDetalle
                                   where d.ProcesoClave == r.ProcesoClave
                                   select d).SingleOrDefault();
                        // se agrega registro a pedidodetalle
                        if (detalle == null)
                        {
                            detalle = new PedidoDetalle();
                            detalle.ProcesoClave = r.ProcesoClave;
                            detalle.IVA = Convert.ToDecimal(ivasinpedido);
                            detalle.TipoCambio = Convert.ToDecimal(tipocambiosinpedido);
                            detalle.Descuento = Convert.ToDecimal(descuentosinpedido);
                            detalle.DiasCredito = Convert.ToInt32(diascreditosinpedido);
                            dc.PedidoDetalle.Add(detalle);
                        }
                        else
                        {
                            detalle.ProcesoClave = r.ProcesoClave;
                            detalle.IVA = Convert.ToDecimal(ivasinpedido);
                            detalle.TipoCambio = Convert.ToDecimal(tipocambiosinpedido);
                            detalle.Descuento = Convert.ToDecimal(descuentosinpedido);
                            detalle.DiasCredito = Convert.ToInt32(diascreditosinpedido);
                        }


                    }
                    #endregion


                    #region procesoArticulo

                    int quedanTotal = 0;
                    foreach (var ritem in ((JArray)jObject["Articulos"]).Children())
                    {
                        var itemProperties = ritem.Children<JProperty>();
                        Guid artclave = Guid.Parse(itemProperties.FirstOrDefault(x => x.Name == "ArticuloClave").Value.ToString());

                        ProcesoArticulo pa = (from p in dc.ProcesoArticulo
                                                  where p.ProcesoClave == pedidoclave && p.ArticuloClave == artclave
                                                  select p).SingleOrDefault();

                        int surtiendo = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").Value.ToString().Replace("s", ""));

                        if (surtiendo > 0)
                        {
                            decimal preciounitario = Convert.ToDecimal(itemProperties.FirstOrDefault(x => x.Name == "PrecioUnitario").Value.ToString().Replace("s", ""));

                            int anterior = 0;
                            int tipomoneda = 0;
                            if (pa != null)
                            {
                                if (pa.CantidadEntregada != null)
                                {
                                    anterior = pa.CantidadEntregada.Value;
                                }

                                //se valida si la suma de recepciones anteriores mas la actual no sobrepasa la cantidad pedida
                                if (pa.Cantidad >= surtiendo + anterior)
                                {
                                    pa.CantidadEntregada = surtiendo + anterior;
                                    int cantidad_solicitada = 0;
                                    int cantidad_entregada = 0;
                                    if (pa.Cantidad != null)
                                    {
                                        cantidad_solicitada = pa.Cantidad.Value;
                                    }
                                    if (pa.CantidadEntregada != null)
                                    {
                                        cantidad_entregada = pa.CantidadEntregada.Value;
                                    }
                                    quedanTotal += cantidad_solicitada - cantidad_entregada;
                                    tipomoneda = pa.catTipoMonedaClave.Value;
                                }
                                else
                                {
                                    dbContextTransaction.Dispose();
                                    return Content("200");
                                }

                            }
                            else
                            {
                                tipomoneda = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "catTipoMonedaClave").Value.ToString().Replace("s", ""));
                            }

                            ProcesoArticulo parm = (from p in dc.ProcesoArticulo
                                                        where p.ProcesoClave == r.ProcesoClave
                                                        && p.ArticuloClave == artclave
                                                        select p).SingleOrDefault();
                            if (parm == null)
                            {
                                parm = new ProcesoArticulo();
                                parm.ProcesoClave = r.ProcesoClave;
                                parm.ArticuloClave = artclave;
                                parm.CantidadEntregada = Math.Abs(surtiendo);
                                parm.PrecioUnitario = preciounitario;
                                parm.catTipoMonedaClave = tipomoneda;
                                dc.ProcesoArticulo.Add(parm);
                            }
                            else
                            {
                                parm.ProcesoClave = r.ProcesoClave;
                                parm.ArticuloClave = artclave;
                                parm.CantidadEntregada = Math.Abs(surtiendo);
                                parm.PrecioUnitario = preciounitario;
                                parm.catTipoMonedaClave = tipomoneda;
                            }

                            dc.SaveChanges();





                    #endregion

                            if (Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "TieneSeries").Value.ToString().Replace("s", "")) == 0)
                            {
                                //Articulo NO tiene Series

                                Guid g_almacen = Guid.Parse(ed_almacen);
                                Inventario inv = (from i in dc.Inventario
                                                      where i.ArticuloClave == artclave && i.EntidadClave == g_almacen && i.EstatusInv == 7
                                                      select i).SingleOrDefault();
                                if (inv == null)
                                {
                                    inv = new Inventario();
                                    inv.InventarioClave = Guid.NewGuid();
                                    inv.ArticuloClave = Guid.Parse(itemProperties.FirstOrDefault(x => x.Name == "ArticuloClave").Value.ToString());
                                    inv.EntidadClave = Guid.Parse(ed_almacen);
                                    inv.Cantidad = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").Value.ToString());
                                    inv.Estatus = 1;
                                    inv.Standby = 0;
                                    inv.EstatusInv = 7;
                                    dc.Inventario.Add(inv);
                                }
                                else
                                {
                                    inv.Cantidad += Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").Value.ToString());
                                    inv.EstatusInv = 7;
                                }



                                ProcesoInventario pi = (from p in dc.ProcesoInventario
                                                            where p.InventarioClave == inv.InventarioClave && p.ProcesoClave == r.ProcesoClave
                                                            select p).SingleOrDefault();

                                if (pi == null)
                                {
                                    pi = new ProcesoInventario();

                                    pi.InventarioClave = inv.InventarioClave;
                                    pi.ProcesoClave = r.ProcesoClave;

                                    if (r.ProcesoClaveRespuesta != null)
                                    {
                                        pi.Pedido = r.ProcesoClaveRespuesta.Value;
                                    }
                                    pi.Cantidad = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").Value.ToString());
                                    pi.Fecha = DateTime.Now;

                                    dc.ProcesoInventario.Add(pi);
                                }
                                else
                                {

                                    pi.InventarioClave = inv.InventarioClave;
                                    pi.ProcesoClave = r.ProcesoClave;

                                    if (r.ProcesoClaveRespuesta != null)
                                    {
                                        pi.Pedido = r.ProcesoClaveRespuesta.Value;
                                    }
                                    pi.Cantidad = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == "Surtiendo").Value.ToString());
                                    pi.Fecha = DateTime.Now;

                                }


                                dc.SaveChanges();
                            }
                            else
                            {
                                //Articulo tiene Series
                                JArray Seriales = (JArray)itemProperties.FirstOrDefault(x => x.Name == "Series").Value;
                                List<SeriesRecepcion> seriesRec2 = Seriales.ToObject<List<SeriesRecepcion>>();
                                bool existerepetidas = false;
                                //seriesRec2.ForEach(x =>
                                //{
                                //    string valor = x.datos[0].valor.ToString();
                                //    if (dc.Serie.Any(h => h.Valor == valor))
                                //    {
                                //        existerepetidas = true;
                                        
                                //    }
                                        
                                       
                                       
                                    
                                //});

                               
                                seriesRec2.ForEach(x =>
                                {
                                    Guid id = Guid.NewGuid();
                                    x.InventarioClave = id;
                                });

                                List<Inventario> invs = (from b in seriesRec2
                                                         select new Inventario
                                                         {

                                                             ArticuloClave = artclave,
                                                             Cantidad = 1,
                                                             InventarioClave = b.InventarioClave,
                                                             EntidadClave = r.EntidadClaveSolicitante,
                                                             Estatus = 1,
                                                             EstatusInv = 7,
                                                             Standby = 0

                                                         }).ToList();


                                List<ProcesoInventario> procinv = (from a in seriesRec2
                                                                   select new ProcesoInventario
                                                                   {
                                                                       InventarioClave = a.InventarioClave,
                                                                       ProcesoClave = r.ProcesoClave,
                                                                       Cantidad = 1,
                                                                       Fecha = DateTime.Now

                                                                   }).ToList();


                                List<Serie> lseries = (from c1 in seriesRec2
                                                       select new Serie
                                                       {
                                                           InventarioClave = c1.InventarioClave,
                                                           catSerieClave = 2,
                                                           Valor = c1.datos[0].valor

                                                       }).ToList();
                               
                                EFBatchOperation.For(dc, dc.Inventario).InsertAll(invs);
                                EFBatchOperation.For(dc, dc.ProcesoInventario).InsertAll(procinv);
                                EFBatchOperation.For(dc, dc.Serie).InsertAll(lseries);
                                dc.SaveChanges();
                            }


                        }
                        else
                        {
                            int anterior = 0;


                            if (pa != null)
                            {

                                if (pa.CantidadEntregada != null)
                                {
                                    anterior = pa.CantidadEntregada.Value;
                                }

                                pa.CantidadEntregada = surtiendo + anterior;


                                int cantidad_solicitada = 0;
                                int cantidad_entregada = 0;

                                if (pa.Cantidad != null)
                                {
                                    cantidad_solicitada = pa.Cantidad.Value;
                                }


                                if (pa.CantidadEntregada != null)
                                {
                                    cantidad_entregada = pa.CantidadEntregada.Value;
                                }


                                quedanTotal += cantidad_solicitada - cantidad_entregada;

                            }


                        }
                    }
                  

                    if (Pedido != null)
                    {
                        if (quedanTotal == 0)
                        {
                            Pedido.catEstatusClave = 4;
                        }
                        else
                        {
                            Pedido.catEstatusClave = 3;
                        }
                    }

                    dc.SaveChanges();
                    dbContextTransaction.Complete();
                    return Content(r.ProcesoClave.ToString());
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Dispose();
                    return Content("0");

                }
            }
            
        }      

        public class SeriesRecepcion
        {

            public Guid ArticuloClave { get; set; }
            public string EstatusTexto { get; set; }
            public int EstatusClave { get; set; }
            public int Activo { get; set; }

            public Guid InventarioClave { get; set; }
            public List<DetSerie> datos { get; set; }
        }
        public class DetSerie
        {
            public string Clave { get; set; }
            public string valor { get; set; }
        }

        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            NewSoftvModel ns = new NewSoftvModel();

            if (Request["p"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["p"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso prorecepcion = (from p in dc.Proceso
                                        where p.ProcesoClave == clave
                                        select p).SingleOrDefault();

            List<ProcesoArticulo> articulos = prorecepcion.ProcesoArticulo.ToList();
            List<ProcesoInventario> procesoinventario = prorecepcion.ProcesoInventario.ToList();

            Proceso pro = prorecepcion.Proceso2;


            StringBuilder sb = new StringBuilder();

            AlmacenDireccionPlaza direccion_plaza = new AlmacenDireccionPlaza();
            try
            {
                direccion_plaza = ns.AlmacenDireccionPlaza.Where(x => x.idcompania == pro.Entidad.idEntidadSofTV).First();

            }
            catch
            { }

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h2><b>" + empresa + "</b></h2>");
            sb.Append("<h4><b>RECEPCION DE MATERIAL</b></h4>");
            if (pro != null)
            {
                sb.Append("<b>Recepción: #</b><span>" + prorecepcion.NumeroPedido + "</span>  <b>Pedido: #</b><span>" + pro.NumeroPedido + "</span><br>");
                //sb.Append("<br>");             
            }
            else
            {
                sb.Append("<b>Recepción: #</b><span>" + prorecepcion.NumeroPedido + "</span><br>");
            }
            sb.Append(@"<b style=""font-size:10px;"">Lugar Recepción: </b><span style=""font-size:10px;"">" + prorecepcion.Entidad.Nombre+"</span><br>");           
            sb.Append(@"<b style=""font-size:10px;"">Dirección: </b><span style=""font-size:10px;"">" + direccion_plaza.calle + " " + direccion_plaza.numero_exterior + " COLONIA " + direccion_plaza.colonia + ", " + direccion_plaza.localidad + ", " + direccion_plaza.estado + ".CP" + direccion_plaza.codigo_postal+"<br>");
            
            if (pro !=null)
            {
                sb.Append(@"<b style=""font-size:10px;"">Proveedor: </b><span  style=""font-size:10px;"">" + pro.catProveedor.Nombre + "</span><br>");
            }else
            {
                sb.Append(@"<b style=""font-size:10px;"">Proveedor: </b><span style=""font-size:10px;"">" + prorecepcion.catProveedor.Nombre + "</span><br>");
            }
            
            sb.Append(@"<b style=""font-size:10px;"">Fecha: </b><span style=""font-size:10px;"">" + prorecepcion.Fecha.Value.ToShortDateString() + "</span><br>");

            if (prorecepcion.catEstatusClave == 6)
            {
                sb.Append("<font color='#FF0000'>CANCELADO</font><br><br>");
            }

            

            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<br>");

            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");

            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th colspan=""3""><b>Descripcion</b></th>");
            sb.Append(@"<th><b>Cantidad</b></th> ");
            sb.Append(@"<th><b>Unidad</b></th> ");
            sb.Append(@"<th align=""right""><b>Precio Unit</b></th> ");
            sb.Append(@"<th align=""right""><b>Subtotal</b></th> ");          
            sb.Append("</tr>");
            sb.Append("</thead>");
            List<ProcesoArticulo> arts = null;
            if (pro != null)
            {
                arts = pro.ProcesoArticulo.ToList();
            }
            else
            {
                arts = prorecepcion.ProcesoArticulo.ToList();
            }


            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {

                List<Serie> seriesart = new List<Serie>();
                Articulo a = pa.Articulo;

                ProcesoArticulo articulo = (from aa in articulos
                                                where aa.ArticuloClave == a.ArticuloClave
                                                select aa).SingleOrDefault();

                if (articulo != null && articulo.CantidadEntregada > 0)
                {

                    sb.Append("<tr>");
                    sb.Append(@"<td colspan=""3""><b>"+a.Nombre+"</b>");
                    sb.Append(@"<td align=""center"">"+ articulo.CantidadEntregada.Value+ "</td>");
                    sb.Append(@"<td align=""center"">" + articulo.Articulo.catUnidad.Nombre + "</td>");
                    sb.Append(@"<td align=""right"">"+ articulo.PrecioUnitario.Value.ToString("C")+"</td>");
                    sb.Append(@"<td align=""right"">" + (articulo.PrecioUnitario.Value * articulo.CantidadEntregada.Value).ToString("C") + "</td>");
                    sb.Append("</tr>");
                                      
                    sb.Append(@"<tr colspan=7>");
                    sb.Append(@"<td style=""font-size:8px;"">");
                    if (articulo.Articulo.catTipoArticulo.SerieTipoArticulo.Count() > 0)
                    {

                        List<Serie> series = (from aa in dc.ProcesoInventario
                                              join bb in dc.Serie on aa.InventarioClave equals bb.InventarioClave
                                              join cc in dc.Inventario on bb.InventarioClave equals cc.InventarioClave
                                              where cc.ArticuloClave == articulo.ArticuloClave && aa.ProcesoClave == prorecepcion.ProcesoClave
                                              select bb
                                             ).ToList();
                        sb.Append("<b>Series:</b><br>");
                        series.ForEach(se => {
                            sb.Append("<span>"+se.Valor+"-</span>");
                        });
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }
            
            sb.Append("</tbody>");
            sb.Append("</table>");        
            sb.Append("<br></br>");
            sb.Append("<b>Observaciones:</b><br>");
            sb.Append(prorecepcion.Observaciones);
            sb.Append("<br></br>");
            sb.Append("</font>");
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "pedido.pdf";
                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, prorecepcion);
                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Salida" + prorecepcion.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "pedido.pdf" + "'  style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }



        }
       
        private ArticuloProcesoWrapper PedidoToarticuloProcesoWrapper(ProcesoArticulo u, Guid pProcesoP)
        {
        
            ModeloAlmacen dc = new ModeloAlmacen();           
            ArticuloProcesoWrapper ap = new ArticuloProcesoWrapper();           
            ap.Clave = u.ProcesoClave + "|" + u.ArticuloClave;
            Articulo art = u.Articulo;            
            ap.ClasificacionClave = art.catTipoArticulo.catClasificacionArticuloClave.ToString();
            ap.TipoArticuloClave = art.catTipoArticuloClave.ToString();
            ap.ArticuloClave = u.ArticuloClave.ToString();
            ap.Nombre = art.Nombre;
            ap.Cantidad = (u.Cantidad == null) ? 0 : u.Cantidad.Value;
            ap.PrecioUnitario = (u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value;
            ap.PrecioUnitarioTexto = ((u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value).ToString("C");
            ap.MonedaTexto = u.catTipoMoneda.Descripcion;
            ap.MonedaClave = u.catTipoMonedaClave.ToString();           
            Proceso Pedido = u.Proceso;           
            ap.Surtida = 0;
            if (Pedido.catTipoProcesoClave == 1)
            {
                List<Proceso> salidas = (from s in dc.Proceso
                                             where s.ProcesoClaveRespuesta == Pedido.ProcesoClave
                                             && s.catTipoProcesoClave == 3
                                             select s).ToList();                
                int cantidad = 0;
                foreach (Proceso s in salidas)
                { 
                    List<ProcesoArticulo> proarts = s.ProcesoArticulo.ToList();
                    foreach (ProcesoArticulo pa in proarts)
                    {
                        if (pa.ArticuloClave == u.ArticuloClave)
                        {
                            cantidad += pa.Cantidad.Value;
                        }
                    }
                }
                ap.Surtida = cantidad;
            }
            ap.catSeriesClaves = new List<catSerieWrapper>();
            List<web.Models.SerieTipoArticulo> tiposarticulo = u.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();
            foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
            {
                catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                ap.catSeriesClaves.Add(csw);
            }
            ap.TieneSeries = tiposarticulo.Count();
            ap.Seriales = new List<SerialesWrapper>();
            int cuantos = (from c in dc.Inventario
                           where c.ArticuloClave == u.ArticuloClave
                           && c.EntidadClave == Pedido.EntidadClaveAfectada
                           select c.Cantidad.Value).ToList().Sum();
            ap.Existencia = cuantos;
            if (ap.Surtida == 0)
            {
                try
                {
                    ap.Surtida = u.CantidadEntregada.Value;
                    ap.Surtiendo = 0;
                }
                catch
                {
                    ap.Surtida = 0;
                    ap.Surtiendo = 0;
                }
            }
            List<ProcesoInventario> proinv = (from i in dc.ProcesoInventario
                                                  where i.Pedido == u.ProcesoClave || i.ProcesoClave == u.ProcesoClave
                                                  && i.Inventario.ArticuloClave == u.ArticuloClave
                                                  select i).ToList();

            foreach (ProcesoInventario pro in proinv)
            {               
                ap.Surtida -= pro.Cantidad.Value;
                ap.Surtiendo += pro.Cantidad.Value;
                Inventario inventario = pro.Inventario;
                List<Serie> series = inventario.Serie.ToList();
                SerialesWrapper sw = new SerialesWrapper();
                sw.RecepcionClave = pro.ProcesoClave.ToString();
                sw.InventarioClave = inventario.InventarioClave.ToString();
                sw.ArticuloClave = inventario.ArticuloClave.ToString();
                sw.EstatusTexto = "Buen Estado";
                sw.EstatusClave = inventario.Estatus.ToString();
                if (inventario.Cantidad > 0 && inventario.Estatus == 1)
                {
                    sw.Activo = 1;
                }
                else
                {
                    sw.Activo = 0;                
                }
                sw.datos = new List<SerialesDatosWrapper>();

                foreach (Serie s in series)
                {
                    SerialesDatosWrapper sdw = new SerialesDatosWrapper();
                    sdw.Clave = s.catSerieClave.ToString();
                    sdw.Valor = s.Valor.ToString();
                    sw.datos.Add(sdw);
                }

                ap.Seriales.Add(sw);
            }
            return ap;
        }

        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            return rw;
        }

    }


}
