﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Utils;
using web.Controllers;
using Web.Controllers;
using web.Models;

namespace web.Controllers
{
    public class DevolucionAlmacenCentralController : Controller
    {
        //
        // GET: /DevolucionAlmacenCentral/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            Usuario u = ((Usuario)Session["u"]);
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Entidad> alm = dc.Entidad.Where(x=>x.catDistribuidorClave==u.DistribuidorClave && x.catEntidadTipoClave==1).ToList();
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo  where ta.Activo orderby ta.Descripcion select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();
            List<Articulo> articulos = dc.Articulo.Where(x => x.Activo == true).ToList();
            List<catTransportista> transp = dc.catTransportista.Where(x => x.Activo == true).ToList();

            ViewData["almacenes"] = alm;
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tipo_articulos"] = tiposarticulo;
            ViewData["transportistas"] = transp;
            

            return View();
        }
        
        public ActionResult GetTipoArticulo(int id)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var tipos = dc.catTipoArticulo.Where(x => x.catClasificacionArticuloClave==id && x.Activo == true).Select(o => new { o.catTipoArticuloClave,o.Descripcion}).ToList();
            return Json(tipos, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetArticulos(int id, Guid almacen)
        {

            //ArticuloController ac = new ArticuloController();
            //var articulos = ac.GetArticulosByServicio(almacen, id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList();


            ModeloAlmacen dc = new ModeloAlmacen();
            var articulos = dc.Articulo.Where(o => o.Activo == true && o.catTipoArticuloClave == id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList();
            return Json(articulos,JsonRequestBehavior.AllowGet);
        }

        public class Inv
        {
            public Guid InventarioClave { get; set; }
            public int Cantidad { get; set; }

            public bool tieneseries { get; set; }
        }

        public ActionResult getInventario(Guid id,Guid almacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            int cantidad = 0;           
            Inv i = new Inv();
            try
            {
                 i.Cantidad = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == 7).Sum(o => o.Cantidad.Value);
                 i.InventarioClave = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == 7).Select(x=>x.InventarioClave).First();
                 i.tieneseries = tieneSeries(id);
            }
            catch
            {
                cantidad = 0;
            }
            return Json(i,JsonRequestBehavior.AllowGet);

        }

        public ActionResult ValidaSerie(string serie,Guid articulo,Guid almacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            try
            {
                var s = (from a in dc.Serie where a.Valor == serie && a.Inventario.ArticuloClave == articulo && a.Inventario.EntidadClave==almacen 
                        where a.Inventario.EstatusInv==7 && a.Inventario.Standby.Value==0 &&a.Inventario.Cantidad==1
                         select new { a.InventarioClave,a.catSerieClave,a.Valor,a.Inventario.ArticuloClave,a.Inventario.Articulo.Nombre }).First();
                return Json(s, JsonRequestBehavior.AllowGet);
            }
            catch
            { return Content("0");
            }           
        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Aparatos> data { get; set; }
        }

        public class Devolucion
        {
            public long pedido { get; set; }
            public string fecha { get; set; }
            public string almacen { get; set; }

            public int estatus { get; set; }
            public Guid proceso { get; set; }

            public int tiene_envio { get; set; }

        }

        public class lista_dev
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Devolucion> data { get; set; }
        }


        public class Aparatos
        {
           public  Guid InventarioClave { get; set; }
           public string valor { get; set; }

           public  string Plaza { get; set; }

           public Guid ArticuloClave { get; set; }

           public string Nombre { get; set; }

           public bool seleccionado { get; set; }

           public int estatus { get; set; }
        }



        public class series
        {
            public Guid ArticuloClave { get; set; }
            public Guid  InventarioClave { get; set; }
            public string Nombre { get; set; }
            public string Statustxt { get; set; }
            public string Valor { get; set; }
            public string catSerieClave { get; set; }
            public string status { get; set; }
            public int Cantidad { get; set; }

        }

        public ActionResult ListaDevoluciones(string data, string almacen, int orden,int draw, int start, int length)
        {

           

            lista_dev dataTableData = new lista_dev();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = Obtenerdevoluciones(almacen,orden).Count;
            dataTableData.data = Obtenerdevoluciones(almacen,orden).OrderByDescending(x=>x.pedido).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public List<Devolucion> Obtenerdevoluciones(string almacen,int orden)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Devolucion> dev;
            Usuario u = (Usuario)Session["u"];
            try{
                Guid alm=Guid.Parse(almacen);
                dev= dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catDistribuidorClave == u.DistribuidorClave && o.EntidadClaveAfectada==alm).Select(o => new Devolucion { pedido=o.NumeroPedido,almacen=o.EntidadClaveAfectada.Value.ToString(), estatus=o.catEstatusClave.Value,fecha=o.Fecha.ToString(),proceso=o.ProcesoClave }).ToList();
            }catch{

                dev = dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catDistribuidorClave == u.DistribuidorClave).Select(o => new Devolucion { pedido = o.NumeroPedido, almacen = o.EntidadClaveAfectada.Value.ToString(), estatus = o.catEstatusClave.Value, fecha = o.Fecha.ToString(), proceso = o.ProcesoClave }).ToList();

            }

            try
            {
                if (orden != 0)
                {
                    dev = dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catDistribuidorClave == u.DistribuidorClave && o.NumeroPedido == orden).Select(o => new Devolucion { pedido = o.NumeroPedido, almacen = o.EntidadClaveAfectada.Value.ToString(), estatus = o.catEstatusClave.Value, fecha = o.Fecha.ToString(), proceso = o.ProcesoClave }).ToList();
                }
            }
            catch
            {

                dev = dc.Proceso.Where(o => o.catTipoProcesoClave == 13 && o.catDistribuidorClave == u.DistribuidorClave).Select(o => new Devolucion { pedido = o.NumeroPedido, almacen = o.EntidadClaveAfectada.Value.ToString(), estatus = o.catEstatusClave.Value, fecha = o.Fecha.ToString(), proceso = o.ProcesoClave }).ToList();

            }


            
            List<Devolucion> lista = new List<Devolucion>();

            foreach (var a in dev)
            {
                Devolucion i = new Devolucion();
                i.proceso = a.proceso;
                i.pedido = a.pedido;
                i.fecha = a.fecha;
                i.estatus = a.estatus;
                Guid ar=Guid.Parse(a.almacen);
                Entidad al = dc.Entidad.Where(x=>x.EntidadClave==ar).First();
                i.almacen = al.Nombre;
                int env = 0;
                    try{
                       List<ProcesoEnvio>pe= dc.ProcesoEnvio.Where(x => x.ProcesoClave == a.proceso).ToList();
                       if (pe.Count>0)
                       {
                           env = 1;
                       }
                    }catch{}

                i.tiene_envio=env;
                lista.Add(i);

            }


            return lista;

        }

        public ActionResult aparatos(Guid articulo, Guid plaza,string Articulos,string idDev, int draw, int start, int length)
        {

            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series>   series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));           
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(plaza, articulo, series, idDev).Count;
            dataTableData.data = ObtenerAparatos(plaza, articulo, series, idDev).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Aparatos> ObtenerAparatos(Guid plaza, Guid articulo, List<series> listaseries, string idDev)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            bool dev;
             Guid Devolucion;
             
           
            try
            {
                dev = true;
                Devolucion = Guid.Parse(idDev);
                
            }
            catch {
                dev = false;
                Devolucion = Guid.Empty;
            }

            List<Aparatos> lista = (from a in dc.Serie where a.Inventario.EntidadClave == plaza && a.Inventario.ArticuloClave == articulo && a.Inventario.EstatusInv == 7 && a.Inventario.Cantidad.Value == 1 && a.Inventario.Standby.Value == 0 select new Aparatos { InventarioClave = a.InventarioClave, valor = a.Valor, Plaza = a.Inventario.EntidadClave.ToString(), ArticuloClave = a.Inventario.ArticuloClave.Value, Nombre = a.Inventario.Articulo.Nombre, estatus = a.Inventario.EstatusInv.Value }).OrderBy(x => x.valor).ThenBy(x => x.valor).ToList();

            if (dev)
            {

                List<Aparatos> aparatosdev = (from a in dc.ProcesoInventario
                                              where a.ProcesoClave == Devolucion && a.Inventario.Standby == 1 && a.Inventario.Estatus == 3 && a.Inventario.ArticuloClave == articulo
                                            select new Aparatos { InventarioClave=a.Inventario.InventarioClave, ArticuloClave=a.Inventario.ArticuloClave.Value, estatus=a.Inventario.EstatusInv.Value, valor=a.Inventario.Serie.Select(o=>o.Valor).FirstOrDefault(), Nombre=a.Inventario.Articulo.Nombre, Plaza=a.Inventario.EntidadClave.Value.ToString()}
                                             ).ToList();
                lista.AddRange(aparatosdev);         

            }
           
            

            foreach (var a in lista)
            {
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus=Int32.Parse(s.status);
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }

        public ActionResult GuardaDevolucion(Guid almacen,string motivo,string articulos ){
            ModeloAlmacen dc = new ModeloAlmacen();
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Entidad alm = dc.Entidad.Where(o => o.EntidadClave == almacen).First();
                    catDistribuidor distribuidor = dc.catDistribuidor.Where(x => x.catDistribuidorClave == alm.catDistribuidorClave).First();

                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    Usuario u = (Usuario)Session["u"];
                    Proceso p = new Proceso();

                    try
                    {
                        p.ProcesoClave = Guid.NewGuid();
                        p.catEstatusClave = 1;
                        p.catTipoProcesoClave = 13;
                        p.EntidadClaveSolicitante = Guid.Parse("00000000-0000-0000-0000-000000000001");
                        p.EntidadClaveAfectada = alm.EntidadClave;
                        p.UsuarioClave = u.UsuarioClave;
                        p.Fecha = DateTime.Now;
                        p.Observaciones = motivo;
                        p.catDistribuidorClave = distribuidor.catDistribuidorClave;
                        dc.Proceso.Add(p);
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-3");
                    }
                    


                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g  select new { articulo=g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach(var a in result){
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = p.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                    }                  
                    
                    foreach (var Articulo in series)
                    {

                        Inventario Inv=new Inventario();
                        try
                        {
                            Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == almacen && o.EstatusInv == 7).First();
                        }
                        catch { }
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            if(Inv.Standby==1 && Inv.Cantidad.Value==0){
                                dbContextTransaction.Rollback(); 
                                return Content("-1");
                            }
                            else if(Inv.EntidadClave != alm.EntidadClave){
                                dbContextTransaction.Rollback(); 
                                return Content("-1");
                            }
                            else
                            {
                                Inv.Standby = 1;
                                Inv.Cantidad = 0;
                                Inv.Estatus = 3;
                                Inv.EstatusInv = Int32.Parse(Articulo.status);

                                
                            }
                            
                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback(); 
                                return Content("-2");
                            }
                            else
                            {
                                Inv.Standby = Inv.Standby.Value + Articulo.Cantidad;
                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;
                                

                            }

                            
                        }
                        //dc.SaveChanges();
                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(),JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }

           
        }

        public bool tieneSeries(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art =dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if(art.catTipoArticulo.catSerieClaveInterface==null){
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool Espagare(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.EsPagare.Value == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult Envio(Guid proceso,int trans,string guia,string fecha)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (dc.ProcesoEnvio.Any(x => x.ProcesoClave == proceso))
                {
                    ProcesoEnvio p= dc.ProcesoEnvio.Where(o=>o.ProcesoClave==proceso).First();
                    p.catTranspClave = trans;
                    p.GuiaEnvio = guia;
                    p.FechaEnvio = DateTime.Parse(fecha);

                    dc.SaveChanges();
                    return Content("1");
                }
                else
                {

                    ProcesoEnvio pe = new ProcesoEnvio();
                    pe.ProcesoClave = proceso;
                    pe.catTranspClave = trans;
                    pe.GuiaEnvio = guia;
                    pe.FechaEnvio = DateTime.Parse(fecha);
                    dc.ProcesoEnvio.Add(pe);
                    dc.SaveChanges();
                    return Content("1");
                }
            }
            catch
            {
                return Content("0");
            }

        }


        public ActionResult Cancela(Guid proceso)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Proceso devolucion;
                    try
                    {
                        devolucion = dc.Proceso.Where(x => x.ProcesoClave == proceso).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback(); 
                        return Content("-1");
                    }
                    if (devolucion.catEstatusClave.Value==6){
                        dbContextTransaction.Rollback(); 
                        return Content("-2");
                    }
                    if (devolucion.catEstatusClave == 4)
                    {
                        dbContextTransaction.Rollback(); 
                        return Content("-3");
                    }

                    devolucion.catEstatusClave = 6;

                    List<ProcesoInventario> pi = dc.ProcesoInventario.Where(x => x.ProcesoClave == proceso).ToList();
                    foreach (var Articulo in pi)
                    {

                        if (tieneSeries(Articulo.Inventario.ArticuloClave.Value))
                        {
                            Articulo.Inventario.Standby = 0;
                            Articulo.Inventario.Cantidad = 1;
                            Articulo.Inventario.Estatus = 1;
                            Articulo.Inventario.EstatusInv = 7;

                            
                        }
                        else
                        {
                            Articulo.Inventario.Standby = Articulo.Inventario.Standby - Articulo.Cantidad;
                            Articulo.Inventario.Cantidad = Articulo.Inventario.Cantidad + Articulo.Cantidad;

     
                        }


                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(devolucion.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);


                }
                catch {
                    dbContextTransaction.Rollback();
                    return Content("0");
                
                }

            }
           

        }

        public class envio{
            public int tran{get; set;}
            public string guia{get; set;}
            public string fecha{get; set;}
        }

        public ActionResult detalleEnvio(Guid proceso){

            ModeloAlmacen dc = new ModeloAlmacen();
            ProcesoEnvio p= dc.ProcesoEnvio.Where(x=>x.ProcesoClave==proceso).First();
           
            envio e =new envio();

            e.fecha = p.FechaEnvio.ToString("dd/MM/yyyy");
            e.guia=p.GuiaEnvio.ToString();
            e.tran=p.catTranspClave;
            return Json(e,JsonRequestBehavior.AllowGet);
        }



        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();


            if (Request["d"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["d"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            Proceso pro = (from p in dc.Proceso
                           where p.ProcesoClave == clave
                           select p).SingleOrDefault();

            List<ProcesoInventario> procesoinventario = pro.ProcesoInventario.ToList();

            Entidad eS = (from e in dc.Entidad
                              where e.EntidadClave == pro.EntidadClaveSolicitante
                              select e).SingleOrDefault();
            Entidad eA = (from e in dc.Entidad
                              where e.EntidadClave == pro.EntidadClaveAfectada
                              select e).SingleOrDefault();
            Usuario u = pro.Usuario;

            catEstatus vEst = (from e in dc.catEstatus
                                   where e.catEstatusClave == pro.catEstatusClave
                                   select e).SingleOrDefault();

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h2><b>" + empresa + "</b></h2><br>");
            if(pro.catTipoProcesoClave==13){
                sb.Append("<h3>Devolución al Almacén Central: #" + pro.NumeroPedido + "</h3><br>");
            }
            if (pro.catTipoProcesoClave == 14)
            {
                sb.Append("<h3>Recepción de material por devolución: #" + pro.NumeroPedido + "</h3><br>");
            }
                
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 height=80 align=right>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"">");
            sb.Append("<tr>");
            sb.Append("<td>");
            if (pro.catTipoProcesoClave == 13)
            {

                sb.Append("<b>Enviar a: </b><br>");
                sb.Append(eS.Nombre + "<br>");
                sb.Append("<b>Enviado de: </b><br>");
                sb.Append(eA.Nombre + "<br>");
            }
            else
            {
                sb.Append("<b>Fecha: </b><br>");
                sb.Append(pro.Fecha + "<br>");
            }
            sb.Append("<b>Tipo de Proceso: </b><br>");
            sb.Append(pro.catTipoProceso.Nombre + "<br>");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>Estatus Proceso: </b><br>");
            sb.Append(vEst.Nombre + "<br>");

            if (pro.catTipoProcesoClave == 13)
            {
                List<ProcesoEnvio> pe = dc.ProcesoEnvio.Where(o => o.ProcesoClave == pro.ProcesoClave).ToList();
                if (pe.Count > 0)
                {
                    sb.Append("<b>Datos de envio </b><br>");
                    sb.Append("<b>Transportista: </b><br>");
                    sb.Append(pro.ProcesoEnvio.catTransportista.Descripcion);
                    sb.Append("<br>");
                    sb.Append("<b>Guia: </b><br>");
                    sb.Append(pro.ProcesoEnvio.GuiaEnvio);
                }

            }
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            
           
            if (pro.catTipoProcesoClave == 13)
            {
                sb.Append("<b>Motivo: </b><br>");
                sb.Append(pro.Observaciones + "<br>");
            }
            sb.Append("<br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=""0"" width=""100%"" class=""table table-hover-color"">");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" align=""center"">");
            sb.Append("Cantidad");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" colspan=""3"">");
            sb.Append("Concepto");
            sb.Append("</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (ProcesoArticulo pa in arts)
            {
                if (pa.Cantidad > 0)
                {
                    Articulo a = pa.Articulo;

                    sb.Append("<tr>");
                    sb.Append(@"<td align=""center"">");
                    sb.Append(pa.Cantidad);
                    sb.Append("</td>");
                    sb.Append(@"<td colspan=""3"">");
                    sb.Append(a.Nombre);
                    sb.Append("<br>");

                    foreach (ProcesoInventario pi in procesoinventario)
                    {
                        Inventario i = pi.Inventario;
                        if (i.ArticuloClave == a.ArticuloClave)
                        {
                            List<Serie> series = i.Serie.ToList();
                            int conta = 0;
                            var innerCount = 0;
                            foreach (Serie s in series)
                            {
                                if (innerCount > 0)
                                {
                                    sb.Append("-");
                                }
                                string val="";
                                if(s.Inventario.EstatusInv==7){
                                    val="Buen Estado";
                                }
                                else if(s.Inventario.EstatusInv==9){
                                    val="Revisión";
                                }
                                else if(s.Inventario.EstatusInv==10){
                                    val="Dañado";
                                }
                                else{
                                    val="Baja";
                                }
                                sb.Append(s.Valor +"->"+val);
                                innerCount++;
                            }

                            conta++;
                            

                            if (conta > 0)
                            {
                                sb.Append("<br>");
                            }
                        }
                    }
                }
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            if (pdf == 1)
            {
                sb.Append("<br><br>");
            }
            sb.Append("</font>");

            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "_dev-almcentral.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);

                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Dev-AlmCentral_" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" + "/Content/media/" + g.ToString() + "_dev-almcentral.pdf" + "' style='position: relative; height: 90%; width: 90%;'  frameborder=0></iframe>";
                    return View();
                }
            }
        }


        /// <summary>
        /// edición de devolución
        /// </summary>
        /// <param name="iddevolucion"></param>
        /// <returns></returns>
        /// 

        public ActionResult DetalleDevolucion(Guid iddevolucion)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Proceso devolucion;
            DetalleDevo objretornar=new DetalleDevo();
            try
            {
                devolucion = dc.Proceso.Where(x => x.ProcesoClave == iddevolucion).First();
                objretornar.almacen=devolucion.EntidadClaveAfectada.Value;
                objretornar.motivo=devolucion.Observaciones;

            }
            catch {

                return Content("");
            }            

            List<ProcesoInventario> pi = devolucion.ProcesoInventario.ToList();
            List<series> articulos = new List<series>();

            foreach (var a in pi)
            {
                series t = new series();
                t.ArticuloClave = a.Inventario.ArticuloClave.Value;
                t.status = a.Inventario.EstatusInv.Value.ToString();
                t.InventarioClave = a.InventarioClave;
                t.Nombre = a.Inventario.Articulo.Nombre;
                
                if (a.Inventario.Articulo.catTipoArticulo.catSerieClaveInterface == null)
                {
                    t.Cantidad = a.Cantidad.Value;
                    t.Valor = null;
                }
                else
                {
                    t.Cantidad = 1;
                    t.Valor = dc.Serie.Where(x => x.InventarioClave == a.Inventario.InventarioClave).Select(o => o.Valor).First();
                }
               
                articulos.Add(t);
            }
            objretornar.articulos = articulos;

            return Json(objretornar,JsonRequestBehavior.AllowGet);
          

        }

        public int cuantosdevolucion(Guid idDev, Guid idArticulo)
        {
             ModeloAlmacen dc = new ModeloAlmacen();
             int cantidad=0;
             try
             {
                 Proceso devolucion = dc.Proceso.Where(x => x.ProcesoClave == idDev).First();
                 cantidad = devolucion.ProcesoArticulo.Where(x => x.ArticuloClave == idArticulo).Select(o => o.Cantidad.Value).First();
             }
             catch { }
            
            return cantidad;
        }

        public class DetalleDevo
        {
            public Guid almacen {get; set;}
            public string motivo { get; set;}

            public List<series> articulos { get; set; }

        }

        public ActionResult EditaDevolucion(string motivo,string articulos,Guid devolucion){
             ModeloAlmacen dc = new ModeloAlmacen();
            Proceso Devolucion ;


             
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Entidad alm;
                    catDistribuidor distribuidor;

                    try
                    {
                        Devolucion=dc.Proceso.Where(x=>x.ProcesoClave==devolucion).First();                        
                         alm = dc.Entidad.Where(o => o.EntidadClave == Devolucion.EntidadClaveAfectada).First();
                         distribuidor = dc.catDistribuidor.Where(x => x.catDistribuidorClave == alm.catDistribuidorClave).First();
                    }catch{

                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -1;
                        e1.Mensaje = "No existe la devolución o existe un error de escritura en almacén solicitante";
                        return Json(e1, JsonRequestBehavior.AllowGet);
                    }



                    if (Devolucion.catEstatusClave == 4)
                    {

                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -7;
                        e1.Mensaje = "La orden no se puede editar,ya fue recepcionada por el almacén central";
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }

                     var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));


                    #region Eliminando devolucion  pasada

                    try
                    {
                        List<ProcesoArticulo> pa = Devolucion.ProcesoArticulo.ToList();
                        dc.ProcesoArticulo.RemoveRange(pa);
                        
                        List<ProcesoInventario> pi = Devolucion.ProcesoInventario.ToList();
                        foreach (var p in pi)
                        {
                            if (tieneSeries(p.Inventario.ArticuloClave.Value))
                            {
                                p.Inventario.Cantidad = 1;
                                p.Inventario.Standby = 0;
                                p.Inventario.Estatus = 1;
                                p.Inventario.EstatusInv = 7;
                                if (Espagare(p.Inventario.ArticuloClave.Value))
                                {
                                    Pagare pagare = dc.Pagare.Where(x => x.catDistribuidorClave == distribuidor.catDistribuidorClave && x.ArticuloClave == p.Inventario.ArticuloClave.Value).First();
                                    pagare.Utilizadas = pagare.Utilizadas.Value + 1;
                                    pagare.Disponibles = pagare.Disponibles.Value - 1;
                                }

                            }
                            else
                            {
                                Inventario Inv = dc.Inventario.Where(o => o.InventarioClave == p.Inventario.InventarioClave && o.ArticuloClave == p.Inventario.ArticuloClave && o.EntidadClave == alm.EntidadClave && o.EstatusInv == 7).First();

                                Inv.Cantidad = Inv.Cantidad + p.Cantidad;
                                Inv.Standby = Inv.Standby - p.Cantidad;
                                if (Espagare(p.Inventario.ArticuloClave.Value))
                                {
                                    Pagare pagare = dc.Pagare.Where(x => x.catDistribuidorClave == distribuidor.catDistribuidorClave && x.ArticuloClave == p.Inventario.ArticuloClave.Value).First();
                                    pagare.Utilizadas = pagare.Utilizadas.Value + p.Cantidad ;
                                    pagare.Disponibles = pagare.Disponibles.Value - p.Cantidad;
                                }

                            }

                            dc.SaveChanges();

                        }
                        dc.SaveChanges();

                        dc.ProcesoInventario.RemoveRange(pi);

                        dc.SaveChanges();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -2;
                        e1.Mensaje = "No se pudo eliminar detalles de devolución pasada";
                        return Json(e1, JsonRequestBehavior.AllowGet);
                    }
                    #endregion



                   var result = from x in series
                                group x by new { x.ArticuloClave }
                                    into g
                                    select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                   foreach (var a in result)
                   {
                       ProcesoArticulo newpa = new ProcesoArticulo();
                       newpa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                       newpa.Cantidad = a.Cantidad;
                       newpa.ProcesoClave = Devolucion.ProcesoClave;
                       dc.ProcesoArticulo.Add(newpa);
                   }


                   foreach (var Articulo in series)
                   {
                       Inventario Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == alm.EntidadClave && o.EstatusInv == 7).First();

                       ProcesoInventario pinew = new ProcesoInventario();
                       pinew.ProcesoClave = Devolucion.ProcesoClave;
                       pinew.InventarioClave = Articulo.InventarioClave;
                       pinew.Cantidad = Articulo.Cantidad;
                       pinew.Fecha = DateTime.Now;
                       dc.ProcesoInventario.Add(pinew);

                       if (tieneSeries(Articulo.ArticuloClave))
                       {
                           if (Inv.Standby == 1 && Inv.Cantidad.Value == 0)
                           {
                               dbContextTransaction.Rollback();
                               error e1 = new error();
                               e1.idError = -3;
                               e1.Mensaje = "El artículo ya se encuentra en devolución";
                               return Json(e1, JsonRequestBehavior.AllowGet);
                           }
                           else if (Inv.EntidadClave != alm.EntidadClave)
                           {
                               dbContextTransaction.Rollback();
                               dbContextTransaction.Rollback();
                               error e1 = new error();
                               e1.idError = -4;
                               e1.Mensaje = "El artículo no se encuentra en el almacén solicitado";
                               return Json(e1, JsonRequestBehavior.AllowGet);
                           }
                           else
                           {
                               Inv.Standby = 1;
                               Inv.Cantidad = 0;
                               Inv.Estatus = 3;
                               Inv.EstatusInv = Int32.Parse(Articulo.status);

                               
                           }


                       }
                       else
                       {
                           if (Inv.Cantidad < Articulo.Cantidad)
                           {
                               dbContextTransaction.Rollback();
                               error e1 = new error();
                               e1.idError = -5;
                               e1.Mensaje = "La cantidad en almacén es menor a la solicitada por la orden";
                               return Json(e1, JsonRequestBehavior.AllowGet);
                           }
                           else
                           {
                               Inv.Standby = Inv.Standby.Value + Articulo.Cantidad;
                               Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;


                           }


                       }
                       //dc.SaveChanges();
                   }

                   dc.SaveChanges();
                   dbContextTransaction.Commit();
                   return Json(Devolucion.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);


                }
                catch{

                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -6;
                    e1.Mensaje = "El sistema evitó la generación de la devolución ya que se encontraron errores en ella";
                    return Json(e1, JsonRequestBehavior.AllowGet);

                }

            }

            

        }


        public class error
        {
            public int idError { get; set; }
            public string Mensaje { get; set; }

            public string aux { get; set; }
        }
      


    }
}
