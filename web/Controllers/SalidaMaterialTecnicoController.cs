﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Controllers;

namespace web.Controllers
{
    public class SalidaMaterialTecnicoController : Controller
    {
        //
        // GET: /SalidaMaterialTecnico/


        ModeloAlmacen dc = new ModeloAlmacen();

        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {

            
            Usuario u = ((Usuario)Session["u"]);
            List<Entidad> tecnicos = null;
            List<Entidad> tecnicosGenerales = null;

            List<catClasificacionArticulo> clasificaciones = dc.catClasificacionArticulo.Where(x => x.Activo == true).OrderBy(o => o.Descripcion).ToList();
            List<catTipoArticulo> tiposarticulo = dc.catTipoArticulo.Where(o => o.Activo == true).OrderBy(o => o.Descripcion).ToList();
            if (u.DistribuidorClave == null)
            {
                tecnicos = dc.Entidad.Where(o => o.catEntidadTipoClave == 2 && o.catDistribuidorClave == null).OrderBy(o=>o.Nombre).ToList();                   
            }
            else
            {
                tecnicos = dc.Entidad.Where(o => o.catEntidadTipoClave == 2 && o.catDistribuidorClave == u.DistribuidorClave).OrderBy(o => o.Nombre).ToList();
                tecnicosGenerales = (from a in dc.Entidad join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                                     where b.DistribuidorClave == u.DistribuidorClave select a).ToList();
                tecnicos.AddRange(tecnicosGenerales);            
            }
            List<Entidad> almacenes = null;

            if (u.DistribuidorClave == null)
            {
                almacenes = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                     && ta.catDistribuidorClave == null
                             orderby ta.Nombre
                             select ta).ToList();
            }
            else
            {

                List<AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                                  where a.UsuarioClave == u.UsuarioClave
                                                  select a).ToList();
                almacenes = new List<Entidad>();


                foreach (AmbitoAlmacen a in ambito)
                {
                    Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        almacenes.Add(e);
                    }
                }
            }




            List<catTipoSalida> tipossalida = (from ta in dc.catTipoSalida
                                                   where ta.Activo
                                                   orderby ta.Descripcion
                                                   select ta).ToList();

            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["almacenes"] = almacenes;
            ViewData["tecnicos"] = tecnicos;
            ViewData["tipossalida"] = tipossalida;


            return View();
        }

        public ActionResult ObtenArticulos(int tipo)
        {
            var articulos=dc.Articulo.Where(x=>x.catTipoArticuloClave==tipo).ToList();
            return Json(articulos,JsonRequestBehavior.AllowGet);
        }

      
        public bool tieneSeries(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult aparatos(Guid articulo, Guid plaza, string Articulos, int draw, int start, int length)
        {

            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series> series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(plaza, articulo, series).Count;
            dataTableData.data = ObtenerAparatos(plaza, articulo, series).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Aparatos> ObtenerAparatos(Guid plaza, Guid articulo, List<series> listaseries)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Aparatos> lista = (from a in dc.Serie where a.Inventario.EntidadClave == plaza && a.Inventario.ArticuloClave == articulo && a.Inventario.EstatusInv == 7 && a.Inventario.Cantidad.Value == 1 && a.Inventario.Standby.Value == 0 select new Aparatos { InventarioClave = a.InventarioClave, valor = a.Valor, Plaza = a.Inventario.EntidadClave.ToString(), ArticuloClave = a.Inventario.ArticuloClave.Value, Nombre = a.Inventario.Articulo.Nombre, estatus = a.Inventario.EstatusInv.Value }).OrderBy(x => x.valor).ThenBy(x => x.valor).ToList();

            foreach (var a in lista)
            {
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus = 7;
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }




        
        public ActionResult getInventario(Guid id, Guid almacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            int cantidad = 0;
            Inv i = new Inv();
            try
            {
                i.Cantidad = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == 7).Sum(o => o.Cantidad.Value);
                i.InventarioClave = dc.Inventario.Where(o => o.ArticuloClave == id && o.EntidadClave == almacen && o.EstatusInv == 7).Select(x => x.InventarioClave).First();
                i.tieneseries = tieneSeries(id);
            }
            catch
            {
                cantidad = 0;
            }
            return Json(i, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetTipoArticulos(int id)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var tipos = dc.catTipoArticulo.Where(x=>x.catClasificacionArticuloClave==id).Select(t=>new{ t.catTipoArticuloClave,t.Descripcion }).ToList();
            return Json(tipos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetArticulos(int id)
        {

            //ArticuloController ac = new ArticuloController();
            //var articulos = ac.GetArticulosByServicio(almacen, id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList(); ;

            ModeloAlmacen dc = new ModeloAlmacen();
            var articulos = dc.Articulo.Where(o => o.Activo == true && o.catTipoArticuloClave == id).Select(p => new { p.ArticuloClave, p.Nombre, p.catTipoArticulo.EsPagare, p.catTipoArticulo.catSerieClaveInterface }).ToList();
            return Json(articulos, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DetalleArticulo(Guid idarticulo)
        {
            DetalleArticulo da = new DetalleArticulo();
            Articulo art = dc.Articulo.Where(x=>x.ArticuloClave==idarticulo).First();
            da.idArticulo = art.ArticuloClave;
            da.idTipoArticulo = art.catTipoArticuloClave;
            da.idClasificacion = art.catTipoArticulo.catClasificacionArticuloClave;
            return Json(da,JsonRequestBehavior.AllowGet);
        }

        public ActionResult Rel_Tecnico_almacen(Guid id)
        {
            Entidad almacen = dc.Entidad.Where(x=>x.EntidadClave==id).First();

          var rel=  dc.AmbitoAlmacen.Where(x => x.EntidadClaveAlmacen == id && x.Entidad.catEntidadTipoClave == 2)
                .Select(o => new { o.Entidad.Nombre,o.Entidad.EntidadClave}).ToList();

            var tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == almacen.catDistribuidorClave
                                 select new { a.Nombre,a.EntidadClave }).ToList();
            rel.AddRange(tecgen);

            return Json(rel, JsonRequestBehavior.AllowGet);
        }

        


        public ActionResult GuardaSalida(Guid idAlmacen,Guid idTecnico,string salida,string observaciones, string articulos)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                dc.Database.CommandTimeout = 0;
                try
                {
                    Entidad Almacen;
                    Entidad Tecnico;
                    try
                    {
                        Almacen = dc.Entidad.Where(x=>x.EntidadClave==idAlmacen).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -1;
                        e1.Mensaje = " No se encontró el almacén en el sistema";
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }

                    try
                    {
                        Tecnico = dc.Entidad.Where(x => x.EntidadClave == idTecnico).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -2;
                        e1.Mensaje = " No se encontró el técnico en el sistema";
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }

                    List<series> series;
                    try
                    {
                        var myArray = new Newtonsoft.Json.Linq.JArray();
                        series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -3;
                        e1.Mensaje = "Ocurrio un error de lectura en la salida";
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }

                    
                    Usuario u;
                    try{
                      u  = (Usuario)Session["u"];
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -5;
                        e1.Mensaje = "la sesión ha expirado por favor vuelva autenticarse";
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }
                    
                    Proceso p = new Proceso();
                    try
                    {
                        p.ProcesoClave = Guid.NewGuid();
                        p.catEstatusClave = 4;
                        p.catTipoProcesoClave = 3;
                        p.EntidadClaveSolicitante = Tecnico.EntidadClave;
                        p.EntidadClaveAfectada = Almacen.EntidadClave;
                        p.UsuarioClave = u.UsuarioClave;
                        p.Fecha = DateTime.Now;
                        p.Observaciones = "";
                        p.catDistribuidorClave = null;
                        p.catTipoSalidaClave = Int32.Parse(salida);
                        p.Observaciones = observaciones;
                        dc.Proceso.Add(p);

                        dc.SaveChanges();
                    }
                    catch(Exception e)
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -5;
                        e1.Mensaje = "Hubo un error al guardar el proceso" + e.Message.ToString();
                        e1.aux = p.ToString();
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }

                    try
                    {
                        var result = from x in series
                                     group x by new { x.ArticuloClave }
                                         into g
                                         select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                        foreach (var a in result)
                        {
                            ProcesoArticulo pa = new ProcesoArticulo();
                            pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                            pa.Cantidad = a.Cantidad;
                            pa.ProcesoClave = p.ProcesoClave;
                            dc.ProcesoArticulo.Add(pa);
                            dc.SaveChanges();
                        }
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -5;
                        e1.Mensaje = "Hubo un error al guardar el proceso PA";
                        return Json(e1, JsonRequestBehavior.AllowGet);

                    }

                    


                    foreach (var Articulo in series)
                    {

                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);

                        Inventario InvAlm;
                        try
                        {
                            InvAlm = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == Almacen.EntidadClave && o.EstatusInv == 7).First();

                        }
                        catch
                        {
                            dbContextTransaction.Rollback();
                            error e1 = new error();
                            e1.idError = -4;
                            e1.Mensaje = " El aparato "+Articulo.Valor+" No se encuentra en el saldo del almacén";
                            return Json(e1, JsonRequestBehavior.AllowGet);

                        }                        

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            if (InvAlm.Standby == 1 && InvAlm.Cantidad.Value == 0)
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -5;
                                e1.Mensaje = " El aparato " + Articulo.Valor + " No se encuentra en el saldo del almacén central ,se encuentra en stand by ";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }

                            InvAlm.EntidadClave = Tecnico.EntidadClave;
                            long idInventario = InvAlm.IdInventario;                           
                            Articulo art = InvAlm.Articulo;
                            catTipoArticulo tipo = art.catTipoArticulo;

                            string serie = "";
                            try
                            {
                                serie = InvAlm.Serie.Select(x => x.Valor).First();
                            }
                            catch
                            {
                                serie = dc.Serie.Where(x => x.InventarioClave == InvAlm.InventarioClave).Select(o => o.Valor).First().ToString();
                            }

                            Entidad plaza = (from e in dc.Entidad
                                                 where e.EntidadClave == p.EntidadClaveAfectada
                                                 select e).SingleOrDefault();


                            Entidad tecnico = (from t in dc.Entidad
                                                   where t.EntidadClave == p.EntidadClaveSolicitante
                                                   select t).SingleOrDefault();


                            try
                            {
                                web.Models.Newsoftv.NewSoftvModel ns = new web.Models.Newsoftv.NewSoftvModel();
                                ns.Database.ExecuteSqlCommand("exec SP_AltaCablemodems @IdDistribuidor,@IdPlaza,@IdTecnico,@IdAparato,@Mac,@TipoAparato,@Marca",
                                    new SqlParameter("@IdDistribuidor", Convert.ToInt32(InvAlm.Entidad.catDistribuidor.IdDistribuidor)),
                                     new SqlParameter("@IdPlaza", Convert.ToInt32(plaza.IdEntidad)),
                                     new SqlParameter("@IdTecnico", Convert.ToInt32(tecnico.IdEntidad)),
                                     new SqlParameter("@IdAparato", Convert.ToInt32(InvAlm.IdInventario)),
                                     new SqlParameter("@Mac", serie),
                                     new SqlParameter("@TipoAparato", art.catTipoArticulo.Letra),
                                     new SqlParameter("@Marca", art.Nombre)
                                    );
                            }
                            catch (Exception ex)
                            {
                                //dbContextTransaction.Rollback();
                                //error e1 = new error();
                                //e1.idError = -4;
                                //e1.Mensaje = "No se pudo conectar con el SAC";
                                // return Json(e1, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (InvAlm.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -6;
                                e1.Mensaje = " El saldo del artículo: " + Articulo.Nombre + "Es menor a la cantidad solicitada";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                                
                            }
                            else
                            {

                                InvAlm.Cantidad = InvAlm.Cantidad - Articulo.Cantidad;

                                List<Inventario> invfinal_l = (from i in dc.Inventario
                                                                   where i.ArticuloClave == Articulo.ArticuloClave
                                                                   && i.EntidadClave == p.EntidadClaveSolicitante
                                                                   && i.Estatus == 1 && i.EstatusInv == 7
                                                                   select i).ToList();

                                Inventario invfinal = null;

                                if (invfinal_l.Count > 0)
                                {
                                    invfinal = invfinal_l[0];
                                }

                                if (invfinal != null)
                                {
                                    invfinal.Cantidad += Articulo.Cantidad;
                                    invfinal.Standby = 0;
                                    invfinal.Estatus = 1;
                                }
                                else
                                {
                                    invfinal = new Inventario();
                                    invfinal.InventarioClave = Guid.NewGuid();
                                    invfinal.ArticuloClave =Articulo.ArticuloClave;
                                    invfinal.EntidadClave = p.EntidadClaveSolicitante;
                                    invfinal.Cantidad = Articulo.Cantidad;
                                    invfinal.Standby = 0;
                                    invfinal.Estatus = 1;
                                    invfinal.EstatusInv = 7;
                                    dc.Inventario.Add(invfinal);
                                }

                            }


                        }


                    }
                    try
                    {
                        dc.SaveChanges();
                        dbContextTransaction.Commit();
                        return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        dbContextTransaction.Rollback();
                        error e1 = new error();
                        e1.idError = -7;
                        e1.Mensaje = e.Message + e.InnerException;
                        return Json(e1, JsonRequestBehavior.AllowGet);
                    }
                   
                }
                catch(Exception e)
                {
                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -7;
                    e1.Mensaje = e.Message+e.InnerException;
                    return Json(e1, JsonRequestBehavior.AllowGet);
                    
                }

                 
            }
            
        }

    }







    #region clases
    public class error
    {
        public int idError { get; set; }
        public string Mensaje { get; set; }

        public string aux { get; set; }
    }

    public class Aparatos
    {
        public Guid InventarioClave { get; set; }
        public string valor { get; set; }

        public string Plaza { get; set; }

        public Guid ArticuloClave { get; set; }

        public string Nombre { get; set; }

        public bool seleccionado { get; set; }

        public int estatus { get; set; }

        public string txtstatus { get; set; }
    }




    public class Inv
    {
        public Guid InventarioClave { get; set; }
        public int Cantidad { get; set; }

        public bool tieneseries { get; set; }
    }



    public class series
    {
        public Guid ArticuloClave { get; set; }
        public Guid InventarioClave { get; set; }
        public string Nombre { get; set; }
        public string Statustxt { get; set; }
        public string Valor { get; set; }
        public string catSerieClave { get; set; }
        public string status { get; set; }
        public int Cantidad { get; set; }
        public bool tieneserie { get; set; }
        public string Reemplazo { get; set; }
        public bool Reemplazada { get; set; }

    }



    public class DataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<Aparatos> data { get; set; }
    }



    public class DetalleArticulo
    {
        public Guid idArticulo { get; set; }
        public int idTipoArticulo { get; set; }

        public int idClasificacion { get; set; }
    }


    #endregion


}
