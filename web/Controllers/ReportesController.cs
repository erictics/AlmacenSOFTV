﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Controllers;
using web.Models;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class ReportesController : Controller
    {
        //
        // GET: /Reportes/

        [SessionExpireFilterAttribute]
        public ActionResult Inventario()
        {

           ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidores = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidores = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }
            else
            {
                distribuidores = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }


            ViewData["distribuidores"] = distribuidores;


            return View();
        }


        [SessionExpireFilterAttribute]
        public ActionResult Detalle()
        {
            return View();
        }

        [SessionExpireFilterAttribute]
        public ActionResult InventarioTecnico()
        {
            return View();
        }


        [SessionExpireFilterAttribute]
        public ActionResult BajasExistencias()
        {
           ModeloAlmacen dc = new ModeloAlmacen();


            List<catDistribuidor> distribuidores = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidores = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }
            else
            {
                distribuidores = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }


            ViewData["distribuidores"] = distribuidores;



            return View();
        }


        //
        public ActionResult prueba(string blogPostId)
        {

            
            List<BajasExistenciasWrapper> ih = new List<BajasExistenciasWrapper>();

           ModeloAlmacen dc = new ModeloAlmacen();
             //textboxValue = Request.Form["txtOne"];
            Guid entidad1 = Guid.Parse(blogPostId);
            Entidad almacen = (from p in dc.Entidad
                                   where p.EntidadClave == entidad1
                                   select p).SingleOrDefault();

            string empresa = Config.Dame("EMPRESA");
            string nombre_almacen = almacen.Nombre;
            List<catClasificacionArticulo> clas = (from c in dc.catClasificacionArticulo
                                                       orderby c.Descripcion
                                                       select c).ToList();
            foreach (catClasificacionArticulo cl in clas)
            {
               
                
                foreach (catTipoArticulo tp in cl.catTipoArticulo)
                {
                    foreach (Articulo a in tp.Articulo)
                    {

                        int invs = (from i in dc.Inventario
                                    where i.ArticuloClave == a.ArticuloClave
                                    && i.EntidadClave == entidad1
                                    select i).ToList().Sum(o => o.Cantidad.Value);

                        if (invs < a.TopeMinimo.Value)
                        {


                            BajasExistenciasWrapper bj = new BajasExistenciasWrapper();
                            bj.CLASIFICACION = cl.Descripcion;
                            bj.TIPO = tp.Descripcion;
                            bj.ARTICULO = a.Nombre;
                            bj.EXISTENCIAS = invs.ToString();
                            bj.UNIDAD = a.catUnidad.Nombre;
                            bj.MINIMO = a.TopeMinimo.ToString();
                            ih.Add(bj);
                        }
                    }


                }
            }
            string distribuidor="";
            DataTable table = ConvertToDatatable(ih);
            
            
           
            DumpExcel(table, nombre_almacen);
            return Content("");
        }

      private void DumpExcel(DataTable tbl, string almacen)
      {
          using (ExcelPackage pck = new ExcelPackage())
          {
              //Create the worksheet
              ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Bajas existencias");

              //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
              ws.Cells["A4"].LoadFromDataTable(tbl, true);

              //Format the header for column 1-3
            
              
              
              using (ExcelRange rng = ws.Cells["A4:F4"])
              {
                  rng.Style.Font.Bold = true;
                  rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                  rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                  rng.Style.Font.Color.SetColor(Color.White);
                  
                  
              }
              //estilo a datos de plantilla
              using (ExcelRange rng = ws.Cells["A1:F3"])
              {
                  rng.Style.Font.Bold = true;
                  rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                  rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                  rng.Style.Font.Color.SetColor(Color.White);


              }
              using (ExcelRange rng = ws.Cells["A1:F1"])
              {
                  rng.Style.Font.Bold = true;
                  rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                  rng.Style.Fill.BackgroundColor.SetColor(Color.Red);  //Set color to dark blue
                  rng.Style.Font.Color.SetColor(Color.White);


              }



              ws.Column(1).Width = 30;
              ws.Column(2).Width = 30;
              ws.Column(3).Width = 30;
              ws.Column(4).Width = 30;
              ws.Column(5).Width = 30;
              ws.Column(6).Width = 30;
              string empresa = Config.Dame("EMPRESA");

              //datos de plantilla
              ws.Cells["C1"].Value = empresa;             
              ws.Cells["A3"].Value = "ALMACEN";
              ws.Cells["B3"].Value = almacen;
              ws.Cells["C2"].Value = " ARTICULOS  CON BAJAS EXISTENCIAS";




              //Example how to Format Column 1 as numeric 
              using (ExcelRange col = ws.Cells[5, 1, 5 + tbl.Rows.Count, 1])
              {
                  //col.Style.Numberformat.Format = "#,##0.00";
                  col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                  
              }


             

              //Write it back to the client
              Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
              Response.AddHeader("content-disposition", "attachment;  filename=bajas existencias.xlsx");
              Response.BinaryWrite(pck.GetAsByteArray());
          }
      }


        static DataTable ConvertToDatatable(List<BajasExistenciasWrapper> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("CLASIFICACION");
            dt.Columns.Add("TIPO");
            dt.Columns.Add("ARTICULO");
            dt.Columns.Add("MINIMO ACEPTABLE");
            dt.Columns.Add("UNIDAD");
            dt.Columns.Add("EXISTENCIAS");
            foreach (var item in list)
            {
                var row = dt.NewRow();

                row["CLASIFICACION"] = item.CLASIFICACION.ToString();
                row["TIPO"] = item.TIPO;
                row["ARTICULO"] = item.ARTICULO;
                row["MINIMO ACEPTABLE"] = item.MINIMO;
                row["UNIDAD"] = item.UNIDAD;
                row["EXISTENCIAS"] = item.EXISTENCIAS;

                dt.Rows.Add(row);
            }

            return dt;
        }


        public class ArticulosAgotados
        {
           public Guid IdArticulo { get; set; }

           public string Nombre { get; set; }

           public int cantidad { get; set; }

           public int TopeMinimo { get; set; }

           public string Clasificacion { get; set; }
        }

       


        public ActionResult DetallebajasExistencias(Guid Almacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
             List<ArticulosAgotados> lista = new List<ArticulosAgotados>();
             List<Articulo> articulos = dc.Articulo.ToList();
             foreach (var s in articulos)
             {
                 int? activos = (from a in dc.Inventario
                                 where a.ArticuloClave == s.ArticuloClave
                                 && a.EntidadClave == Almacen
                                 && a.EstatusInv == 7
                                 && a.Estatus.HasValue
                                 && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                 select a).Sum(a => a.Cantidad);

                 int cuantos_activos = 0;
                 try
                 {
                     cuantos_activos = activos.Value;
                 }
                 catch
                 {
                 }

            int minimo = dc.Articulo.Where(x => x.ArticuloClave == s.ArticuloClave).Select(x => x.TopeMinimo.Value).First();
            if (cuantos_activos >= minimo)
            {                
            }
            else
            {
                ArticulosAgotados aa = new ArticulosAgotados();
                aa.cantidad = cuantos_activos;
                aa.Nombre = s.Nombre;
                aa.IdArticulo = s.ArticuloClave;
                aa.TopeMinimo = s.TopeMinimo.Value;
                aa.Clasificacion = s.catTipoArticulo.Descripcion;
                lista.Add(aa);
            }                

             }
             return Json(lista, JsonRequestBehavior.AllowGet);
        }

        


        [SessionExpireFilterAttribute]
        public ActionResult BajasExistenciasDetalle()
        {


            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            StringBuilder sb = new StringBuilder();

            ModeloAlmacen dc = new ModeloAlmacen();
            if (Request["d"] == null)
            {
                return Content("");
            }
            Guid entidadclave = Guid.Parse(Request["d"]);
            Entidad entidad = dc.Entidad.Where(s => s.EntidadClave == entidadclave).First();

            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h3><b>ARTÍCULOS CON BAJAS EXISTENCIAS</b></h3>");
            if (entidad.catEntidadTipoClave == 1)
            {
                sb.Append("<b>Almacén: </b>" + entidad.Nombre + "<br>");
            }
            else
            {
                sb.Append("<b>Técnico: </b>" + entidad.Nombre + "<br>");
            }
            sb.Append("<b>Fecha: </b>" + DateTime.Now.ToShortDateString() + " <br>");
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 align=right><br>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<br>");





            var lista_tipos = (from a in dc.Inventario
                               join b in dc.Articulo
                                on a.ArticuloClave equals b.ArticuloClave
                               where a.EntidadClave == entidad.EntidadClave
                               && a.Cantidad > 0 && a.Articulo.TopeMinimo < a.Cantidad
                               group b by b.catTipoArticuloClave into g1
                               select new { catTipoArticuloClave = g1.Key }
                             ).ToList();

            lista_tipos.ForEach(lt => {

                catTipoArticulo ctp = dc.catTipoArticulo.Where(s => s.catTipoArticuloClave == lt.catTipoArticuloClave).First();

                sb.Append(@"<b style=""font-size:8px"">Clasificación:  " + ctp.catClasificacionArticulo.Descripcion + "</b><br>");
                sb.Append(@"<b style=""font-size:8px"">Tipo de Artículo:  " + ctp.Descripcion + "</b> <br>");

                sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                sb.Append("<tr>");
                sb.Append(@"<td colspan=3 style=""font-size:8px;text-decoration: underline;"">Artículo:</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Tope Mínimo</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Unidad</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Existencias</td>");               
                sb.Append("</tr>");

                var lista_articulos = (from a in dc.Articulo
                                       join b in dc.Inventario
                       on a.ArticuloClave equals b.ArticuloClave
                                       where b.Cantidad > 0 && b.EntidadClave == entidad.EntidadClave
                                       && a.catTipoArticuloClave == ctp.catTipoArticuloClave
                                       group a by a.ArticuloClave into g2
                                       select new { articuloClave = g2.Key }).ToList();

                lista_articulos.ForEach(s => {

                    Articulo art = dc.Articulo.Where(c => c.ArticuloClave == s.articuloClave).First();

                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == art.ArticuloClave
                                    && a.EntidadClave == entidad.EntidadClave
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);
                    int cuantos_activos = 0;
                    try
                    {
                        cuantos_activos = activos.Value;
                    }
                    catch
                    {
                    }

                    

                    sb.Append("<tr>");
                    sb.Append(@"<td colspan=3 style=""font-size:8px;"">" + art.Nombre + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + art.TopeMinimo+ "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + art.catUnidad.Nombre + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cuantos_activos + "</td>");                 
                    sb.Append("</tr>");
                });
                sb.Append("</table>");
                sb.Append("<br><br>");

            });

            PDF elpdf = new PDF();
            Guid g = Guid.NewGuid();
            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "inventario.pdf";
            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
            ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "inventario.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
            return View();





            //ModeloAlmacen dc = new ModeloAlmacen();

            //if (Request["d"] == null)
            //{
            //    return Content("");
            //}

            //Guid clave = Guid.Parse(Request["d"]);
            //int pdf = Convert.ToInt32(Request["pdf"]);
            //Entidad almacen = (from p in dc.Entidad
            //                   where p.EntidadClave == clave
            //                   select p).SingleOrDefault();
            //StringBuilder sb = new StringBuilder();
            //string empresa = Config.Dame("EMPRESA");
            //string logo = Config.Dame("LOGO_ENTRADA");
            //sb.Append("<font size=2>");
            //sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            //sb.Append("<tr>");
            //sb.Append(@"<td align=""left"">");
            //sb.Append("<h3><b>" + empresa + "</b></h3>");
            //sb.Append("<h4>Bajas Existencias " + almacen.Nombre + "</h4>");
            //sb.Append("<h5>" + DateTime.Now + "</h5><br>");
            //sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 heigth=80 align=right><br>");
            //sb.Append("</tr>");
            //sb.Append("</table>");
            //sb.Append("<br><br>");         
            ////sb.Append(@"<table cellpadding=""1""  cellspacing=""0"" bgcolor=""#EFEFEF""><tr></td>");
            //sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"" class=""table table-hover-color"">");
            //sb.Append("<thead>");
            //sb.Append("<tr>");
            //sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" >");
            //sb.Append("Tipo artículo");
            //sb.Append("</th>");
            //sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            //sb.Append("Artículo");
            //sb.Append("</th>");
            //sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            //sb.Append("En almacen");
            //sb.Append("</th>");
            //sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            //sb.Append("Minimo Aceptable");
            //sb.Append("</th>");
            //sb.Append("</tr>");
            //sb.Append("</thead>");            
            //List<ArticulosAgotados> lista = new List<ArticulosAgotados>();
            //List<Articulo> articulos = dc.Articulo.ToList();
            //foreach (var s in articulos)
            //{
            //    int? activos = (from a in dc.Inventario
            //                    where a.ArticuloClave == s.ArticuloClave
            //                    && a.EntidadClave == almacen.EntidadClave
            //                    && a.EstatusInv == 7
            //                    && a.Estatus.HasValue
            //                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
            //                    select a).Sum(a => a.Cantidad);

            //    int cuantos_activos = 0;
            //    try
            //    {
            //        cuantos_activos = activos.Value;
            //    }
            //    catch
            //    {
            //    }

            //    int minimo = dc.Articulo.Where(x => x.ArticuloClave == s.ArticuloClave).Select(x => x.TopeMinimo.Value).First();
            //    if (cuantos_activos >= minimo)
            //    {
            //    }
            //    else
            //    {
            //        sb.Append("<tr><td>" + s.catTipoArticulo.Descripcion + "</td><td>" + s.Nombre + "</td><td>" + cuantos_activos + "</td><td>" + s.TopeMinimo.Value + "</td></tr>");
                    
            //    }

            //}         
            //sb.Append("</tbody>");
            //if (pdf == 1 || pdf == 2)
            //{
            //    sb.Append("<br>");
            //}            
            //sb.Append("</table>");
            //sb.Append("</font>");
            //if (pdf != 1 && pdf != 2)
            //{
            //    ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
            //    return View();
            //}
            //else
            //{

            //    PDF elpdf = new PDF();
            //    Guid g = Guid.NewGuid();
            //    string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "bajasexistencias.pdf";
            //    elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
            //    if (pdf == 1)
            //    {
            //        return File(rutaarchivo, "application/pdf", "BajasExistencias" + clave.ToString() + ".pdf");
            //    }
            //    else
            //    {
            //        ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "bajasexistencias.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
            //        return View();

            //    }
            //}



        }



        [SessionExpireFilterAttribute]
        public ActionResult SaldoArticulos()
        {
           ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidores = null;
            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidores = (from c in dc.catDistribuidor
                                  where c.Activo
                                  orderby c.Nombre
                                  select c).ToList();
            }
            else
            {
                distribuidores = (from c in dc.catDistribuidor
                                  where
                                  c.catDistribuidorClave == u.DistribuidorClave
                                  && c.Activo
                                  orderby c.Nombre
                                  select c).ToList();
            }


            ViewData["distribuidores"] = distribuidores;



            return View();
        }



        public ActionResult ObtenSaldoExcel()
        {
           ModeloAlmacen dc = new ModeloAlmacen();
            List<Entidad> almacenes = new List<Entidad>();
            Guid clave = Guid.Empty;
            try
            {
                clave = Guid.Parse(Request["d"].ToString());
            }
            catch
            {

            }
            string salmacenes = "";
            try
            {
                salmacenes = Request["almacenes"].ToString();
            }
            catch
            {

            }
            string[] t = salmacenes.ToString().Split(',');
            foreach (string tt in t)
            {
                try
                {
                    int ec = Int32.Parse(tt);
                    Entidad ent = dc.Entidad.Where(x => x.idEntidadSofTV == ec).First();
                    almacenes.Add(ent);
                }
                catch { }
            }

            catDistribuidor dist = dc.catDistribuidor.Where(o => o.catDistribuidorClave == clave).First();

            SaldoExcel(almacenes, dist);
            return null;

        }


        public class DetalleArticulos
        {
            public string Clasificacion { get; set; }
            public string Tipo { get; set; }
            public string Articulo { get; set; }
            public string TipoArticulo { get; set; }
            public int  MinimoAceptable { get; set; }
            public string Moneda { get; set; }
            public decimal precio { get; set; }
            public int cantidad { get; set; }
            public decimal TotalMx { get; set; }
            public decimal TotalUSD { get; set; }
        }


        private void SaldoExcel(List<Entidad> almacenes,catDistribuidor distribuidor)
        {
           ModeloAlmacen dc = new ModeloAlmacen();
            using (ExcelPackage pck = new ExcelPackage())
            {
                foreach (var almacen in almacenes)
                {
                    List<DetalleArticulos> Detalle=new List<DetalleArticulos>();
                   decimal totalmx = 0;
                   decimal totalus = 0;
                List<Articulo> lista = dc.Articulo.Where(o=>o.Activo==true).ToList();
                lista.ForEach(x =>
                {
                    DetalleArticulos da = new DetalleArticulos();
                    da.Clasificacion = x.catTipoArticulo.catClasificacionArticulo.Descripcion;
                    da.Tipo = x.catTipoArticulo.Descripcion;
                    da.Articulo = x.Nombre;

                    if (x.catTipoArticulo.EsPagare == true)
                    {
                        da.TipoArticulo = "Pagaré";
                    }
                    else
                    {
                        da.TipoArticulo = "Venta";
                    }
                    da.MinimoAceptable = x.TopeMinimo.Value;
                    da.Moneda = x.catTipoMoneda.Descripcion;

                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == x.ArticuloClave
                                    && a.EntidadClave == almacen.EntidadClave
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);
                    int cant = 0;
                    try
                    {
                        cant = activos.Value;
                    }
                    catch { }
                    da.precio = x.PrecioPagare.Value;
                    da.cantidad = cant;

                    if (x.catTipoMonedaClave == 4)
                    {
                        da.TotalMx = cant * x.PrecioPagare.Value;
                        da.TotalUSD = 0;

                        totalmx = totalmx + (cant * x.PrecioPagare.Value);
                    }
                    if (x.catTipoMonedaClave == 5)
                    {
                        da.TotalMx = 0;
                        da.TotalUSD = cant * x.PrecioPagare.Value;


                        totalus = totalus + (cant * x.PrecioPagare.Value);
                    }

                    Detalle.Add(da);

                });


                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Saldo Articulos"+almacen.Nombre
                        );
                    ws.Cells["A4"].LoadFromCollection(Detalle, true);

                    using (ExcelRange rng = ws.Cells["A4:J4"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    using (ExcelRange rng = ws.Cells["A1:J3"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    using (ExcelRange rng = ws.Cells["A1:J1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                     
                        rng.Style.Fill.BackgroundColor.SetColor(Color.Red);
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    ws.Column(1).Width = 20;
                    ws.Column(2).Width = 20;
                    ws.Column(3).Width = 20;
                    ws.Column(4).Width = 20;
                    ws.Column(5).Width = 20;
                    ws.Column(6).Width = 15;
                    ws.Column(7).Width = 15;
                    ws.Column(8).Width = 15;
                    ws.Column(9).Width = 15;
                    ws.Column(10).Width = 15;

                    string empresa = Config.Dame("EMPRESA");
                    ws.Cells["C1"].Value = empresa;
                    ws.Cells["A1"].Value = distribuidor.Nombre;
                    ws.Cells["A3"].Value = "ALMACEN";
                    ws.Cells["B3"].Value = almacen.Nombre;
                    ws.Cells["C2"].Value = " SALDO DE ARTICULOS";
                    using (ExcelRange col = ws.Cells[5, 1, 5 + Detalle.Count, 1])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }



                } 
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Saldo artículos.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }


        }


        public ActionResult ObtenSaldoPDF()
        {
           ModeloAlmacen dc = new ModeloAlmacen();
            List<Entidad> almacenes = new List<Entidad>();
            Guid clave = Guid.Empty;
            try
            {
                clave = Guid.Parse(Request["d"].ToString());
            }
            catch
            {

            }
            string salmacenes = "";
            try
            {
                salmacenes = Request["almacenes"].ToString();
            }
            catch
            {

            }
            string[] t = salmacenes.ToString().Split(',');
            foreach (string tt in t)
            {
                try
                {
                  int ec=Int32.Parse(tt);
                  Entidad ent = dc.Entidad.Where(x => x.idEntidadSofTV == ec).First();
                  almacenes.Add(ent);
                }
                catch { }
            }



            
            

            catDistribuidor dist = dc.catDistribuidor.Where(o => o.catDistribuidorClave == clave).First();
             
            StringBuilder sb = new StringBuilder();
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");
            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Saldo de artículos</h3>");
            sb.Append("<p><b>Distribuidor: </b>" + dist.Nombre + "</p>");
            sb.Append("<p>" + DateTime.Now + "</p><br>");
            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");
           
            
            foreach (var almacen in almacenes.OrderBy(o => o.Nombre))
            {
                sb.Append(@"<p style=""font-size:12px;"">Nombre almacén: "+almacen.Nombre+"</p><br>");
                sb.Append(@"<table>");

              
                sb.Append("<tr>");
                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"" >");
                sb.Append("Clasificación");
                sb.Append("</td>");

                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"" >");
                sb.Append("Tipo artículo");
                sb.Append("</td>");

                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"">");
                sb.Append("Artículo");
                sb.Append("</td>");

                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"">");
                sb.Append("Tipo artículo");
                sb.Append("</td>");

                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"">");
                sb.Append("Minimo Aceptable");
                sb.Append("</td>");


                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"">");
                sb.Append("Moneda");
                sb.Append("</td>");


                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"">");
                sb.Append("Precio");
                sb.Append("</td>");

                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;""  color=""#FFFFFF"">");
                sb.Append("Cantidad");
                sb.Append("</td>");




                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"">");
                sb.Append("Total MX");
                sb.Append("</td>");

                sb.Append(@"<td bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"">");
                sb.Append("Total USD");
                sb.Append("</td>");
                sb.Append("</tr>");

                decimal totalmx = 0;
                decimal totalus = 0;
                List<Articulo> lista = dc.Articulo.Where(o=>o.Activo==true).ToList();
                lista.ForEach(x => {
                    

                    sb.Append("<tr>");
                    sb.Append(@"<td style=""font-size:8px;"">"+x.catTipoArticulo.catClasificacionArticulo.Descripcion+"</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + x.catTipoArticulo.Descripcion + "</td>");
                    sb.Append(@"<td style=""font-size:6px;"">" + x.Nombre + "</td>");
                    if (x.catTipoArticulo.EsPagare == true)
                    {
                        sb.Append(@"<td style=""font-size:8px;"">Pagaré</td>");
                    }
                    else
                    {
                        sb.Append(@"<td style=""font-size:8px;"">Venta</td>");
                    }


                    sb.Append(@"<td style=""font-size:8px;"">" + x.TopeMinimo + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + x.catTipoMoneda.Descripcion + "</td>");

                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == x.ArticuloClave
                                    && a.EntidadClave == almacen.EntidadClave
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && a.Cantidad >0
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);
                    int cant = 0;
                    try
                    {
                        cant = activos.Value;
                    }
                    catch { }
                    sb.Append(@"<td  align=""right"" style=""font-size:8px;"">$" + x.PrecioPagare.Value + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cant + "</td>");
                    

                    if (x.catTipoMonedaClave == 4)//pesos
                    {

                        sb.Append(@"<td align=""right"" style=""font-size:8px;"">$" + cant * x.PrecioPagare.Value + "</td>");
                        sb.Append(@"<td  align=""right"" style=""font-size:8px;"">$0.00</td>");

                        totalmx = totalmx + (cant * x.PrecioPagare.Value);
                    }
                    if (x.catTipoMonedaClave == 5)
                    {
                        sb.Append(@"<td  align=""right"" style=""font-size:8px;"">$0.00</td>");
                        sb.Append(@"<td  align=""right"" style=""font-size:8px;"">$" + cant * x.PrecioPagare.Value + "</td>");

                        totalus = totalus + (cant * x.PrecioPagare.Value);
                    }
                    sb.Append("</tr>");
                });

                sb.Append(@"<tr><td colspan=8><b>Total: </b></td><td style=""font-size:8px;"" align=""right"">$" + totalmx + @"</td><td style=""font-size:8px;""  align=""right"">$" + totalus + "</td></tr>");

                sb.Append("</table> <br><br>");
            }

            PDF elpdf = new PDF();

            Guid g = Guid.NewGuid();

            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "saldo.pdf";

            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);

            ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "saldo.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
            return View();

        }
        
    





        [SessionExpireFilterAttribute]
        public ActionResult CostoPromedio()
        {
            return View();
        }

        [SessionExpireFilterAttribute]
        public ActionResult SalidasMaterial()
        {
            return View();
        }

        [SessionExpireFilterAttribute]
        public ActionResult Devoluciones()
        {
            return View();
        }

        [SessionExpireFilterAttribute]
        public ActionResult SaldoTecnico()
        {

           ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidores = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidores = (from c in dc.catDistribuidor
                                  where c.Activo
                                  orderby c.Nombre
                                  select c).ToList();
            }
            else
            {
                distribuidores = (from c in dc.catDistribuidor
                                  where
                                  c.catDistribuidorClave == u.DistribuidorClave
                                  && c.Activo
                                  orderby c.Nombre
                                  select c).ToList();
            }


            ViewData["distribuidores"] = distribuidores;

            return View();

        }

        [SessionExpireFilterAttribute]
        public ActionResult MovimientosArticulos()
        {
            return View();
        }



        public ActionResult GetTipoArticulo(int id)
        {
           ModeloAlmacen dc = new ModeloAlmacen();
            var tipos = dc.catTipoArticulo.Where(x => x.catClasificacionArticuloClave == id && x.Activo == true ).Select(o => new { o.catTipoArticuloClave, o.Descripcion }).ToList();
            return Json(tipos, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetArticulosListByServicio(int id, int idservicio)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            SqlConnection conexionSQL = new SqlConnection(dc.Database.Connection.ConnectionString);
          
            List<ArticuloWrapper> aw = new List<ArticuloWrapper>();
            try
            {
               
                
                try
                {
                    conexionSQL.Open();
                }
                catch
                {
                    conexionSQL.Close();
                }
                SqlCommand comandoSql = new SqlCommand("select x1.ArticuloClave,x1.Nombre from Articulo x1 join Tbl_articulo_servicio x2 on x1.IdArticulo=x2.IdArticulo where x1.catTipoArticuloClave=@idarticulo and x2.IdServicio=@idservicio");
                comandoSql.Connection = conexionSQL;
                comandoSql.Parameters.AddWithValue("@idarticulo", id);
                comandoSql.Parameters.AddWithValue("@idservicio", idservicio);
                SqlDataReader reader = comandoSql.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ArticuloWrapper d=new ArticuloWrapper();
                        d.Clave = reader[0].ToString();
                        d.Descripcion = reader[1].ToString();
                        aw.Add(d);

                    }
                }
                reader.Close();
                conexionSQL.Close();



            }
            catch {
                conexionSQL.Close();
            }
            return Json(aw, JsonRequestBehavior.AllowGet);
        }


    }
}
