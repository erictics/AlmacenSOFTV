﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class DistribuidorController : Controller
    {
        //
        // GET: /Unidad/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            return View();
        }

        int elementos = 15;



        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catDistribuidorWrapper> data { get; set; }
        }

        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = listaDistribuidores(data).OrderByDescending(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = listaDistribuidores(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);    
                       
        }


        public List<catDistribuidorWrapper> listaDistribuidores(string data)
        {


            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> rls = (from use in dc.catDistribuidor
                                             orderby use.Nombre
                                             select use).ToList();
            List<catDistribuidorWrapper> rw = new List<catDistribuidorWrapper>();
            foreach (catDistribuidor u in rls)
            {
                catDistribuidorWrapper w = DistribuidorToWrapper(u);
                rw.Add(w);
            }

            return rw;

        }


        public ActionResult Obten(string data)
        {
            catDistribuidorWrapper rw = new catDistribuidorWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            Guid uid = Guid.Parse(data);

            catDistribuidor r = (from ro in dc.catDistribuidor
                               where ro.catDistribuidorClave == uid
                               select ro).SingleOrDefault();

            if (r != null)
            {
                rw = DistribuidorToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    catDistribuidor r = (from ro in dc.catDistribuidor
                                       where ro.catDistribuidorClave == uid
                                       select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    Guid uid = Guid.Parse(activa_id);

                    catDistribuidor r = (from ro in dc.catDistribuidor
                                             where ro.catDistribuidorClave == uid
                                             select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_rfc, string ed_CalleNumero, string ed_Colonia, string ed_CodigoPostal, string ed_Ciudad, string ed_nombrecontacto, string ed_emailcontacto, string ed_lada1contacto, string ed_telefono1contacto, string ed_lada2contacto, string ed_telefono2contacto, string ed_importepagare, string ed_activo)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catDistribuidor r = new catDistribuidor();

                if (ed_id != String.Empty)
                {
                    Guid uid = Guid.Parse(ed_id);

                    r = (from ro in dc.catDistribuidor
                         where ro.catDistribuidorClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {
                    r.catDistribuidorClave = Guid.NewGuid();
                }


                r.RFC = ed_rfc;
                r.Nombre = ed_nombre;

                r.NombreContacto = ed_nombrecontacto;
                r.Email = ed_emailcontacto;
                r.Lada1 = ed_lada1contacto;
                r.Telefono1 = ed_telefono1contacto;
                r.Lada2 = ed_lada2contacto;
                r.Telefono2 = ed_telefono2contacto;

                r.ImportePagare = Convert.ToDecimal(ed_importepagare);

                r.Activo = ed_activo == null ? false : ed_activo.Length > 0;
               

                if (ed_id == String.Empty)
                {
                    dc.catDistribuidor.Add(r);
                }


                if (r.DomicilioClave == null)
                {
                    Domicilio d = new Domicilio();
                    d.DomicilioClave = Guid.NewGuid();
                    d.CalleNumero = ed_CalleNumero;
                    d.Colonia = ed_Colonia;
                    d.CodigoPostal = ed_CodigoPostal;
                    d.Ciudad = ed_Ciudad;
                    d.Pais = "México";

                    dc.Domicilio.Add(d);
                    dc.SaveChanges();

                    r.DomicilioClave = d.DomicilioClave;
                    
                }
                else {
                    Domicilio d = (from dom in dc.Domicilio
                                       where dom.DomicilioClave == r.DomicilioClave
                                       select dom).SingleOrDefault();
                    d.CalleNumero = ed_CalleNumero;
                    d.Colonia = ed_Colonia;
                    d.CodigoPostal = ed_CodigoPostal;
                    d.Ciudad = ed_Ciudad;

                    r.DomicilioClave = d.DomicilioClave;
                }



                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }






        private catDistribuidorWrapper DistribuidorToWrapper(catDistribuidor u)
        {
            catDistribuidorWrapper rw = new catDistribuidorWrapper();
            rw.Clave = u.catDistribuidorClave.ToString();
            rw.Nombre = u.Nombre;

            if (u.ImportePagare.HasValue)
            {
                rw.ImportePagare = u.ImportePagare.Value.ToString("C").Replace("$", "");
            }
            else
            {
                rw.ImportePagare = "0";
            }

            rw.RFC = u.RFC;

            rw.NombreContacto = u.NombreContacto;
            rw.EmailContacto = u.Email;
            rw.Lada1Contacto = u.Lada1;
            rw.Telefono1Contacto = u.Telefono1;
            rw.Lada2Contacto = u.Lada2;
            rw.Telefono2Contacto = u.Telefono2;

            if (u.Domicilio != null)
            {
                Domicilio dom = u.Domicilio;
                rw.CalleNumero = dom.CalleNumero;
                rw.Colonia = dom.Colonia;
                rw.CodigoPostal = dom.CodigoPostal;
                rw.Ciudad = dom.Ciudad;
            }

            rw.Activo = Convert.ToInt32(u.Activo).ToString();

            return rw;
        }
    }
}
