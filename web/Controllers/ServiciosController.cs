﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class ServiciosController : Controller
    {
        //
        // GET: /Servicios/
         [Web.Controllers.SessionExpireFilter]
        public ActionResult Index()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
         
        }

        


         public class DataTableData
         {
             public int draw { get; set; }
             public int recordsTotal { get; set; }
             public int recordsFiltered { get; set; }
             public List<CatTipoServicios> data { get; set; }
         }

         public ActionResult ListaServicios(int data, int draw, int start, int length)
         {
             ModeloAlmacen dc = new ModeloAlmacen();
             
             List<CatTipoServicios> ListaServicio = new List<CatTipoServicios>();
            ListaServicio = dc.CatTipoServicios.ToList();

                 DataTableData dataTableData = new DataTableData();
                 dataTableData.draw = draw;
                 dataTableData.recordsTotal = 0;
                 int recordsFiltered = 0;
                 dataTableData.data = ListaServicio;
                 dataTableData.recordsFiltered = ListaServicio.Count;

                 return Json(dataTableData, JsonRequestBehavior.AllowGet);
            

         }


         public ActionResult DetalleServicio(int IdServicio)
         {

            ModeloAlmacen dc = new ModeloAlmacen();
            Servicio s = new Servicio();
            CatTipoServicios cat = dc.CatTipoServicios.Where(p => p.IdServicio == IdServicio).First();
            s.idServicio = cat.IdServicio;
            s.Nombre = cat.Descripcion;
            s.Activo = cat.Activo;
            return Json(s,JsonRequestBehavior.AllowGet);   

         }


         public ActionResult GuardaServicio(Servicio servicio)
         {

            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();
                CatTipoServicios cat = new CatTipoServicios();
                cat.Descripcion = servicio.Nombre;
                cat.Activo = servicio.Activo;
                dc.CatTipoServicios.Add(cat);
                dc.SaveChanges();
                return Json("1", JsonRequestBehavior.AllowGet);
             }
             catch(Exception e)
             {
                 return Json("0", JsonRequestBehavior.AllowGet);
                 
             }


         }

         public ActionResult EditaServicio(Servicio servicio)
         {

            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();
                CatTipoServicios obj = dc.CatTipoServicios.Where(p => p.IdServicio == servicio.idServicio).First();
                List<Tbl_articulo_servicio> list = dc.Tbl_articulo_servicio.Where(o => o.IdServicio == servicio.idServicio).ToList();
                List<Tbl_almacen_servicio> list2 = dc.Tbl_almacen_servicio.Where(p => p.IdServicio == servicio.idServicio).ToList();
                if (list.Count > 0)
                {
                    return Json("-3", JsonRequestBehavior.AllowGet);

                }
                else if(list2.Count>0)
                {
                    
                    return Json("-2", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    obj.Descripcion = servicio.Nombre;
                    obj.Activo = servicio.Activo;
                    dc.SaveChanges();
                    return Json("1", JsonRequestBehavior.AllowGet);

                }

               
             }
             catch
             {
                 return Json("0", JsonRequestBehavior.AllowGet);
               
             }


         }


    }
}
