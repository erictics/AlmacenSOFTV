﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class SaldoTecnicoController : Controller
    {
        // GET /SaldoTecnico/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();


            }


            ViewData["distribuidor"] = distribuidor;


            List<catPuestoTecnico> puestostecnico = (from c in dc.catPuestoTecnico
                                                         where c.Activo
                                                         orderby c.Descripcion
                                                         select c).ToList();
            ViewData["puestostecnico"] = puestostecnico;


            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());
            Guid dis = Guid.Empty;

            if (jObject["dis"].ToString() != "-1")
            {
                dis = Guid.Parse(jObject["dis"].ToString());
            }

            int pags = (from ro in dc.Entidad
                        where ro.catEntidadTipoClave == 2
                              && (ro.catDistribuidorClave == dis || (dis == Guid.Empty && ro.catDistribuidorClave == null))
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div).ToString());
            }
            else
            {
                return Content((div + 1).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());


            Guid dis = Guid.Empty;

            if (jObject["dis"].ToString() != "-1")
            {
                dis = Guid.Parse(jObject["dis"].ToString());
            }

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<Entidad> lista = new List<Entidad>();

            List<Entidad> rls = (from use in dc.Entidad
                                     where use.catEntidadTipoClave == 2
                                        && (use.catDistribuidorClave == dis || (dis == Guid.Empty && use.catDistribuidorClave == null))
                                     orderby use.Nombre
                                     select use).ToList();
            lista.AddRange(rls);
            List<Entidad> tecgen = (from a in dc.Entidad
                                     join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                                     join use in dc.catDistribuidor on b.DistribuidorClave equals use.catDistribuidorClave
                                    where (use.catDistribuidorClave == dis || (dis == Guid.Empty && use.catDistribuidorClave == null))
                                    select a ).ToList();
            lista.AddRange(tecgen);
            
           

            List<TecnicoWrapper> rw = new List<TecnicoWrapper>();

            foreach (Entidad u in rls.Skip(skip).Take(elementos).ToList())
            {
                TecnicoWrapper w = TecnicoToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }


        private TecnicoWrapper TecnicoToWrapper(Entidad e)
        {
            TecnicoWrapper aw = new TecnicoWrapper();
            aw.Clave = e.EntidadClave.ToString();
            aw.Nombre = e.Nombre;
            aw.catEntidadTipoClave = e.catEntidadTipoClave.ToString();
            aw.EntidadPadreClave = e.EntidadPadreClave.ToString();

            try
            {
                PuestoTecnico pt = e.PuestoTecnico;
                aw.PuestoTecnicos = pt.catPuestoTecnico.Descripcion;
                aw.catPuestosTecnicosClave = pt.catPuestosTecnicosClave.ToString();
            }
            catch
            {
                aw.PuestoTecnicos = String.Empty;
                aw.catPuestosTecnicosClave = String.Empty;
            }

            aw.catDistribuidorClave = e.catDistribuidorClave.ToString();

            aw.Activo = Convert.ToInt32(e.Activo).ToString();

            return aw;
        }



        public ActionResult prueba()
        {


            if (Request["tecnico"] == null)
            {
                return Content("");
            }


            string cuales = Request["tecnico"].ToString();

            


            List<MultiusosWrapper> lista = new List<MultiusosWrapper>();
            DataTable dt = new DataTable();
            lista = getexcel(cuales);
            foreach (var l in lista)
            {
                DumpExcel(l.listadetablas, l.variable1, l.variable2);
            }

            return Content(null);
        }



        private void DumpExcel(List<DataTablesInventarioWrapper> lista, string almacen, string distribuidor)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {



                foreach (var tbl in lista)
                {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(tbl.Tecnico.ToString());
               
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A4"].LoadFromDataTable(tbl.TablaInventario, true);

                //Format the header for column 1-3



                using (ExcelRange rng = ws.Cells["A4:G4"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);


                }
                //estilo a datos de plantilla
                using (ExcelRange rng = ws.Cells["A1:G3"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);


                }
                using (ExcelRange rng = ws.Cells["A1:G1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.Red);  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);


                }



                ws.Column(1).Width = 30;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 25;
                ws.Column(4).Width = 25;
                ws.Column(5).Width = 25;
                ws.Column(6).Width = 25;
                ws.Column(6).Width = 35;
                ws.Column(7).Width = 25;


                //datos de plantilla
                ws.Cells["A1"].Value = "";
                ws.Cells["B1"].Value = " REPORTE SALDO DE TECNICOS ";
              
                ws.Cells["A2"].Value = "DISTRIBUIDOR :";
                ws.Cells["B2"].Value = distribuidor;
                ws.Cells["C2"].Value = "ALMACEN :";
               // ws.Cells["D2"].Value = almacen;

                ws.Cells["A3"].Value = "NOMBRE DE TECNICO:";
                ws.Cells["B3"].Value = tbl.Tecnico.ToString();
               





                //Example how to Format Column 1 as numeric 
                using (ExcelRange col = ws.Cells[5, 1, 5 + tbl.TablaInventario.Rows.Count, 1])
                {
                    //col.Style.Numberformat.Format = "#,##0.00";
                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                }


                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=ExcelDemo.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }





        private List<MultiusosWrapper> getexcel(string cuales)
        {

            MultiusosWrapper mu = new MultiusosWrapper();
            ModeloAlmacen dc = new ModeloAlmacen();
            string[] t = cuales.ToString().Split(',');
            List<Guid> guidst = new List<Guid>();
            List<MultiusosWrapper> listadatos = new List<MultiusosWrapper>();
            foreach (string tt in t)
            {
                try
                {
                    guidst.Add(Guid.Parse(tt));
                }
                catch { }
            }

            List<Entidad> tecnicos = (from p in dc.Entidad
                                          where guidst.Contains(p.EntidadClave)
                                          select p).ToList();

           
            List<DataTablesInventarioWrapper> TecnicoInv = new List<DataTablesInventarioWrapper>();

           
            foreach (Entidad te in tecnicos)
            {
                List<InventarioTecnicoWrapper> listainventario = new List<InventarioTecnicoWrapper>();
                List<catClasificacionArticulo> clas = (from c in dc.catClasificacionArticulo orderby c.Descripcion select c).ToList();
                foreach (catClasificacionArticulo cl in clas)
                {
                    List<catTipoArticulo> tipos = (from tt in cl.catTipoArticulo
                                                       orderby tt.Descripcion
                                                       select tt).ToList();



                    foreach (catTipoArticulo tp in tipos)
                    {
                        List<Articulo> arts = (from a in tp.Articulo
                                                   orderby a.Nombre
                                                   select a).ToList();

                        foreach (Articulo a in arts)
                        {
                           
                            InventarioTecnicoWrapper inv = new InventarioTecnicoWrapper();
                            inv.CATEGORIA = cl.Descripcion;
                            inv.TIPO = tp.Descripcion;
                            inv.ARTICULO = a.Nombre;
                            int invs = (from i in dc.Inventario
                                        where i.ArticuloClave == a.ArticuloClave
                                        && i.EntidadClave == te.EntidadClave
                                        && i.Estatus == 1
                                        && i.EstatusInv == 7
                                        select i).ToList().Sum(o => o.Cantidad.Value);
                            inv.CANTIDAD = invs.ToString();
                            listainventario.Add(inv);
                        }

                    }

                }
                DataTablesInventarioWrapper it = new DataTablesInventarioWrapper();
                DataTable dt = ConvertToDatatable(listainventario);
                it.Tecnico = te.Nombre;
                it.TablaInventario = dt;
                TecnicoInv.Add(it);
            }
           // string Distribuidor = tecnicos.Select(x=>x.catDistribuidor.Nombre).Take(1).FirstOrDefault().ToString();

            string id_tecnico = tecnicos.Select(x=>x.EntidadClave).Take(1).FirstOrDefault().ToString();
            Guid al = Guid.Parse(id_tecnico);
            string clave_almacen=(from am in dc.AmbitoAlmacen  where am.EntidadClaveTecnico==al  select am.EntidadClaveAlmacen).FirstOrDefault().ToString();
           Guid clv = Guid.Parse(clave_almacen);
            string almacen=(from d in dc.Entidad where d.EntidadClave==clv select d.Nombre).FirstOrDefault().ToString();

            mu.listadetablas = TecnicoInv;
            mu.variable1 = "";
            mu.variable2 = "";
            listadatos.Add(mu);
           //WriteDataTableToExcel(TecnicoInv, "Saldo de técnicos ", "saldotecnicos.xlsx", "Details",Distribuidor,almacen);

           return listadatos;
        }


      


        static DataTable ConvertToDatatable(List<InventarioTecnicoWrapper> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("CLASIFICACION");
            dt.Columns.Add("TIPO");
            dt.Columns.Add("ARTICULO");
            dt.Columns.Add("CANTIDAD");

            foreach (var item in list)
            {
                var row = dt.NewRow();

                row["CLASIFICACION"] = item.CATEGORIA.ToString();
                row["TIPO"] = item.TIPO;
                row["ARTICULO"] = item.ARTICULO;
                row["CANTIDAD"] = item.CANTIDAD;


                dt.Rows.Add(row);
            }

            return dt;
        }



        [SessionExpireFilterAttribute]
        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            if (Request["tecnico"] == null)
            {
                return Content("");
            }
            string[] t = Request["tecnico"].ToString().Split(',');
            List<Guid> guidst = new List<Guid>();
            foreach (string tt in t)
            {
                try
                {
                    guidst.Add(Guid.Parse(tt));
                }
                catch { }
            }
            int pdf = Convert.ToInt32(Request["pdf"]);
            int cla = -1;
            int tip = -1;
            List<Entidad> tecnicos = (from p in dc.Entidad
                                          where guidst.Contains(p.EntidadClave)
                                          select p).ToList();
            StringBuilder sb = new StringBuilder();
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h3><b>SALDO DE TECNICOS</b></h3>");
            sb.Append("<b>Fecha: </b>" + DateTime.Now.ToShortDateString() +"< br>");
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 heigth=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");
            //sb.Append("<br><br>");
            foreach (Entidad te in tecnicos)
            {
                var results = (from c in dc.catClasificacionArticulo
                                join s in dc.Inventario on c.catClasificacionArticuloClave
                                equals s.Articulo.catTipoArticulo.catClasificacionArticuloClave
                                where  s.Cantidad > 0
                                && s.EntidadClave == te.EntidadClave
                                group c by new { c.catClasificacionArticuloClave,c.Descripcion } into a 
                                select new { a.Key.catClasificacionArticuloClave,a.Key.Descripcion }).ToList();
                
                if (results.Count > 0)
                {
                    sb.Append("<br>");
                    sb.Append("<b>Técnico: </b>" + te.Nombre + "<br>");
                    sb.Append("_________________________________");
                    sb.Append("<br>");
                    //sb.Append(@"<table cellpadding=""1""  cellspacing=""0"" bgcolor=""#EFEFEF""><tr></td>");
                    sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"" class=""table table-hover-color"">");
                   


                    sb.Append("<tbody>");
                    foreach (var  cl in results)
                    {
                        sb.Append("<tr>");
                        sb.Append(@"<td align=""left"" colspan=""6"" style=""font-size:8px;""><b>");
                        sb.Append("Clasificación: "+cl.Descripcion);
                        sb.Append("</b></td>");
                        sb.Append("</tr>");
                        List<catTipoArticulo> tipos = (from tt in dc.catTipoArticulo
                                                       where tip == -1 || tt.catTipoArticuloClave == tip
                                                       orderby tt.Descripcion
                                                       select tt).ToList();
                        foreach (catTipoArticulo tp in tipos)
                        {



                            int artcinv = 0;
                            List<Articulo> arts = (from a in tp.Articulo
                                                   orderby a.Nombre
                                                   select a).ToList();

                            StringBuilder db2 = new StringBuilder();
                           
                            foreach (Articulo a in arts)
                            {


                                int invs = (from i in dc.Inventario
                                            where i.ArticuloClave == a.ArticuloClave
                                            && i.EntidadClave == te.EntidadClave
                                            && i.Estatus == 1
                                            && i.EstatusInv == 7
                                            && i.Cantidad > 0
                                            select i).ToList().Sum(o => o.Cantidad.Value);

                                if (invs > 0)
                                {
                                    artcinv = artcinv + 1;
                                    
                                    db2.Append("<tr>");
                                    db2.Append(@"<td align=""left"" colspan=""4"" style=""font-size:8px;"">");
                                    db2.Append(a.Nombre);
                                    db2.Append("</td>");
                                    db2.Append(@"<td align=""center"" style=""font-size:8px;"">");
                                    db2.Append(invs);
                                    db2.Append("</td>");
                                    db2.Append(@"<td align=""center"" style=""font-size:8px;"">");
                                    db2.Append(a.catUnidad.Nombre);
                                    db2.Append("</td>");


                                    db2.Append("</tr>");
                                }
                            }
                            

                            if (artcinv > 0)
                            {
                                sb.Append("<tr>");
                                sb.Append(@"<td align=""left"" colspan=""6"" style=""font-size:8px;""><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                sb.Append("Tipo de Artículo: "+tp.Descripcion);
                                sb.Append("</b></td>");
                                sb.Append("</tr>");

                            }


                            if (artcinv > 0)
                            {
                                sb.Append("<tr>");
                                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"" align=""left"" colspan=""4""><b>Nombre de Artículo</b></td>");
                                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"" align=""center""><b>Existencias</b></td>");
                                sb.Append(@"<td  style=""font-size:8px;text-decoration: underline;"" align=""center""><b>Unidad</b></td>");
                                sb.Append("</tr>");
                                sb.Append(db2.ToString());
                            }


                        }


                    }
                    //sb.Append("</table>");
                    //sb.Append("</td></tr></table>");
                    sb.Append("</tbody>");
                    if (pdf == 1 || pdf == 2)
                    {
                        sb.Append("<br>");
                    }
                    sb.Append("</table>");
                }
                //else
                //{
                //    sb.Append("Sin Resultados");
                //}
               
                
            }
            sb.Append("</font>");
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "saldoarticulo.pdf";
                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "SaldoArticulo.pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "saldoarticulo.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
                    return View();
                }
            }



        }


    }
}