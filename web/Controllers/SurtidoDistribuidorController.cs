﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

using web.Wrappers;
using Web.Controllers;

namespace web.Controllers
{
    public class SurtidoDistribuidorController : Controller
    {
        ModeloAlmacen dc = new ModeloAlmacen();
        //
        // GET: /SurtidoDistribuidor/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {

            Usuario u = ((Usuario)Session["u"]);
            List<catDistribuidor> distribuidores = (from d in dc.catDistribuidor
                                                    orderby d.Nombre
                                                    select d).ToList();
            ViewData["distribuidores"] = distribuidores;
            ViewData["RolUsuario"] = u.RolClave;

            return View();
        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<PedidoWrapper> data { get; set; }
        }



        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);

            int pag = Convert.ToInt32(jObject["pag"].ToString());
            Guid autorizador = Guid.Empty;

            if (jObject["autoriza"] != null)
            {
                autorizador = ((Usuario)Session["u"]).UsuarioClave;
            }
            Guid proveedor = Guid.Empty;
            if (jObject["proveedor"] != null && jObject["proveedor"].ToString() != "-1")
            {
                proveedor = Guid.Parse(jObject["proveedor"].ToString());
            }

            Guid distribuidor = Guid.Empty;
            bool pedidodistribuidor = false;
            if (jObject["distribuidor"] != null && jObject["distribuidor"] != null && jObject["distribuidor"].ToString() != "-1")
            {
                distribuidor = Guid.Parse(jObject["distribuidor"].ToString());
                pedidodistribuidor = true;
            }


            else if (jObject["distribuidor"] != null && jObject["distribuidor"].ToString() == "-1")
            {
                pedidodistribuidor = true;
            }

            int estatus = -1;
            if (jObject["estatus"] != null)
            {
                estatus = Convert.ToInt32(jObject["estatus"].ToString());
            }
            int pedido = -1;
            if (jObject["pedido"] != null && jObject["pedido"].ToString() != String.Empty)
            {
                pedido = Convert.ToInt32(jObject["pedido"].ToString());
            }

            int esPagare = -1;
            if (jObject["esPagare"] != null && jObject["esPagare"].ToString() != String.Empty)
            {
                esPagare = Convert.ToInt32(jObject["esPagare"].ToString());
            }
            //se asigna de la misma forma pero si no existe en el objeto y si vale -1 :
            int pagadas = -1;

            try
            {
                if (jObject["pagadas"] != null && jObject["pagadas"].ToString() == "1")
                {
                    pagadas = 1;
                }

            }
            catch { }


            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData(ref recordsFiltered, start, length, pedidodistribuidor, autorizador, estatus, proveedor, distribuidor, esPagare, pedido, pagadas);
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        private List<PedidoWrapper> FilterData(ref int recordFiltered, int start, int length, bool pedidodistribuidor, Guid autorizador, int estatus, Guid proveedor, Guid distribuidor, int esPagare, int pedido, int pagadas)
        {




            Usuario u = ((Usuario)Session["u"]);

            List<Proceso> rls = null;
            List<Proceso> rls2 = null;


            //SI EL USUARIO NO ES  UN DISTRIBUIDOR


            if (u.DistribuidorClave == null)
            {
                rls = (from use in dc.Proceso
                       where

                          ((use.catDistribuidorClave == null && !pedidodistribuidor) || ((use.catDistribuidorClave != null && pedidodistribuidor)))
                          && use.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus < -1 && use.catEstatusClave != -estatus) || (estatus == use.catEstatusClave))
                          && (proveedor == Guid.Empty || (proveedor == use.catProveedorClave))
                          && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                          && (esPagare == -1 || (use.PedidoDetalle != null && use.PedidoDetalle.EsPagare.HasValue && ((esPagare == 1 && use.PedidoDetalle.EsPagare.Value) || (esPagare == 0 && !use.PedidoDetalle.EsPagare.Value))))
                          && (pedido == -1 || (use.NumeroPedido == pedido))
                          && (pagadas == -1 || (pagadas == 1 && use.catEstatusClave != 12))
                       orderby use.NumeroPedido descending
                       select use).Skip(start).Take(length).ToList();

                rls2 = (from use in dc.Proceso
                        where

                           ((use.catDistribuidorClave == null && !pedidodistribuidor) || ((use.catDistribuidorClave != null && pedidodistribuidor)))
                           && use.catTipoProcesoClave == 1
                           && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                           && (estatus == -1 || (estatus < -1 && use.catEstatusClave != -estatus) || (estatus == use.catEstatusClave))
                           && (proveedor == Guid.Empty || (proveedor == use.catProveedorClave))
                           && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                           && (esPagare == -1 || (use.PedidoDetalle != null && use.PedidoDetalle.EsPagare.HasValue && ((esPagare == 1 && use.PedidoDetalle.EsPagare.Value) || (esPagare == 0 && !use.PedidoDetalle.EsPagare.Value))))
                           && (pedido == -1 || (use.NumeroPedido == pedido))
                           && (pagadas == -1 || (pagadas == 1 && use.catEstatusClave != 12))
                        orderby use.NumeroPedido descending
                        select use).ToList();

                recordFiltered = rls2.Count;
            }

            //SI EL USUARIO  ES DISTRIBUIDOR
            else
            {
                rls = (from use in dc.Proceso
                       where
                          use.catDistribuidorClave == u.DistribuidorClave
                          && use.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus < -1 && use.catEstatusClave != -estatus) || (estatus == use.catEstatusClave))
                          && (pedido == -1 || (use.NumeroPedido == pedido))
                          && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                          && (esPagare == -1 || (use.PedidoDetalle != null && use.PedidoDetalle.EsPagare.HasValue && ((esPagare == 1 && use.PedidoDetalle.EsPagare.Value) || (esPagare == 0 && !use.PedidoDetalle.EsPagare.Value))))
                          && (pagadas == -1 || (pagadas == 1 && use.catEstatusClave != 12))
                       orderby use.Fecha descending
                       select use).Skip(start).Take(length).ToList();


                rls2 = (from use in dc.Proceso
                        where
                           use.catDistribuidorClave == u.DistribuidorClave
                           && use.catTipoProcesoClave == 1
                           && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                           && (estatus == -1 || (estatus < -1 && use.catEstatusClave != -estatus) || (estatus == use.catEstatusClave))
                           && (pedido == -1 || (use.NumeroPedido == pedido))
                           && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                           && (esPagare == -1 || (use.PedidoDetalle != null && use.PedidoDetalle.EsPagare.HasValue && ((esPagare == 1 && use.PedidoDetalle.EsPagare.Value) || (esPagare == 0 && !use.PedidoDetalle.EsPagare.Value))))
                           && (pagadas == -1 || (pagadas == 1 && use.catEstatusClave != 12))
                        orderby use.Fecha descending
                        select use).ToList();

                recordFiltered = rls2.Count;
            }
            //se asigna a una lista envoltorio y se regresa a la vista
            List<PedidoWrapper> rw = new List<PedidoWrapper>();

            foreach (Proceso p in rls)
            {
                if (p.PedidoDetalle != null)
                {
                    PedidoWrapper w = PedidoToWrapper(p);
                    rw.Add(w);
                }
            }
            // se agrega en una envoltura co los mismos campos y se manda de regreso por json 
            return rw;


        }


        private PedidoWrapper PedidoToWrapper(Proceso u)
        {



            PedidoDetalle pd = u.PedidoDetalle;

            PedidoWrapper rw = new PedidoWrapper();
            rw.NumeroPedido = u.NumeroPedido.ToString();

            try
            {
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            catch
            {
                rw.Pedido = u.NumeroPedido.ToString();
            }

            rw.Clave = u.ProcesoClave.ToString();

            rw.EntidadClaveAfectada = u.EntidadClaveAfectada.ToString();
            rw.EntidadClaveSolicitante = u.EntidadClaveSolicitante.ToString();

            if (u.PedidoDetalle.EsPagare.HasValue && u.PedidoDetalle.EsPagare.Value)
            {
                rw.EsPagare = "1";
            }
            else
            {
                rw.EsPagare = "0";
            }

            rw.TieneSalidas = "0";

            //se selecciona la cantidad de salidas que tiene el proceso
            int salidas = (from s in dc.Proceso
                           where s.ProcesoClaveRespuesta == u.ProcesoClave
                           && s.catTipoProcesoClave == 3
                           && s.catEstatusClave != 6
                           select s).Count();

            rw.TieneSalidas = salidas.ToString();

            try
            {
                catProveedor pro = u.catProveedor;
                rw.Origen = pro.Nombre;
            }
            catch
            {
                rw.Origen = "";
            }


            try
            {
                rw.Distribuidor = u.catDistribuidor.Nombre;
            }
            catch
            {
                rw.Distribuidor = "";
            }


            try
            {
                Entidad alm = u.Entidad;
                rw.Destino = alm.Nombre;
            }
            catch
            {
                rw.Destino = "";
            }


            catEstatus estado = u.catEstatus;
            rw.Estatus = estado.Nombre;

            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");

            rw.Total = pd.Total.Value.ToString("C");
            rw.Total_USD = pd.Total_USD.Value.ToString("C");

            rw.FacturacionClave = u.catEstatusFacturacionClave.ToString();

            if (u.catEstatusFacturacionClave != null)
            {
                rw.Facturacion = DameEstatus(u.catEstatusFacturacionClave.Value);
            }
            else
            {
                rw.Facturacion = String.Empty;
            }
            return rw;
        }


        List<catEstatus> _estatus = null;
        private string DameEstatus(int estatusId)
        {



            if (_estatus == null)
            {
                _estatus = (from e in dc.catEstatus
                            orderby e.catEstatusClave
                            select e).ToList();
            }

            catEstatus est = _estatus.Where(o => o.catEstatusClave == estatusId).SingleOrDefault();

            if (est != null)
            {
                return est.Nombre;
            }
            else
            {
                return String.Empty;
            }
        }

        public bool tieneSeries(Guid artclave)
        {

            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public class series
        {
            public Guid ArticuloClave { get; set; }
            public Guid InventarioClave { get; set; }
            public string Nombre { get; set; }
            public string Statustxt { get; set; }
            public string Valor { get; set; }
            public string catSerieClave { get; set; }
            public string status { get; set; }
            public int Cantidad { get; set; }

        }

        public class lista_Aparatos
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Aparatos> data { get; set; }
        }


        public ActionResult aparatos(Guid articulo, string Articulos, int draw, int start, int length)
        {


            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<series> series = new List<series>();
            try
            {
                series = (List<series>)JsonConvert.DeserializeObject(Articulos, typeof(List<series>));
            }
            catch
            {

            }

            lista_Aparatos dataTableData = new lista_Aparatos();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = ObtenerAparatos(articulo, series).Count;
            dataTableData.data = ObtenerAparatos(articulo, series).Skip(start).Take(length).ToList();

            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ValidaSerie(string serie, Guid articulo)
        {

            Guid almacenPrincipal = Guid.Parse("00000000-0000-0000-0000-000000000001");
            try
            {
                var s = (from a in dc.Serie where a.Valor == serie && a.Inventario.ArticuloClave == articulo && a.Inventario.EntidadClave == almacenPrincipal && a.Inventario.EstatusInv == 7 && a.Inventario.Standby == 0 && a.Inventario.Cantidad == 1 select new { a.InventarioClave, a.catSerieClave, a.Valor, a.Inventario.ArticuloClave, a.Inventario.Articulo.Nombre }).First();
                return Json(s, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Content("0");
            }
        }

        public List<Aparatos> ObtenerAparatos(Guid articulo, List<series> listaseries)
        {

            Guid almacenPrincipal = Guid.Parse("00000000-0000-0000-0000-000000000001");
            List<Aparatos> lista = (from a in dc.Serie where a.Inventario.EntidadClave == almacenPrincipal && a.Inventario.ArticuloClave == articulo && a.Inventario.EstatusInv == 7 && a.Inventario.Cantidad.Value == 1 && a.Inventario.Standby.Value == 0 select new Aparatos { InventarioClave = a.InventarioClave, valor = a.Valor, Plaza = a.Inventario.EntidadClave.ToString(), ArticuloClave = a.Inventario.ArticuloClave.Value, Nombre = a.Inventario.Articulo.Nombre, estatus = a.Inventario.EstatusInv.Value }).OrderBy(x => x.valor).ThenBy(o => o.valor).ToList();

            foreach (var a in lista)
            {
                if (listaseries.Any(x => x.InventarioClave == a.InventarioClave))
                {
                    a.seleccionado = true;
                    series s = listaseries.Where(x => x.InventarioClave == a.InventarioClave).First();
                    a.estatus = Int32.Parse(s.status);
                }
                else
                {
                    a.seleccionado = false;
                }
            }
            return lista;
        }


        public class Aparatos
        {
            public Guid InventarioClave { get; set; }
            public string valor { get; set; }

            public string Plaza { get; set; }

            public Guid ArticuloClave { get; set; }

            public string Nombre { get; set; }

            public bool seleccionado { get; set; }

            public int estatus { get; set; }
        }

        public class Articulos
        {
            public Guid idarticulo { get; set; }
            public int cantidad { get; set; }
        }


        public class error
        {
            public int idError { get; set; }
            public string Mensaje { get; set; }

            public string aux { get; set; }
        }



        public ActionResult GuardaSurtido(Guid pedido, string Aparatos, string Articulos)
        {

            Proceso Pedido;
            List<Proceso> Salidas;
            Usuario u = (Usuario)Session["u"];
            Guid almacenPrincipal = Guid.Parse("00000000-0000-0000-0000-000000000001");
            Proceso salida = new Proceso();



            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    if (existe_pedido(pedido))
                    {

                        try
                        {
                            Pedido = dc.Proceso.Where(x => x.ProcesoClave == pedido && x.catTipoProcesoClave == 1).First();
                            Salidas = dc.Proceso.Where(x => x.ProcesoClaveRespuesta == pedido && x.catTipoProcesoClave == 3 && x.catEstatusClave != 6).ToList();

                        }
                        catch
                        {
                            dbContextTransaction.Rollback();
                            error e1 = new error();
                            e1.idError = -1;
                            e1.Mensaje = "El pedido: " + pedido + " No se encuentra en el sistema";
                            return Json(e1, JsonRequestBehavior.AllowGet);
                        }


                        if (Pedido.catEstatusClave == 4)
                        {
                            dbContextTransaction.Rollback();
                            error e1 = new error();
                            e1.idError = -2;
                            e1.Mensaje = "El pedido: #" + Pedido.NumeroPedido + " ya se encuentra surtido completamente";
                            return Json(e1, JsonRequestBehavior.AllowGet);
                        }
                        else if (Pedido.catEstatusClave == 6)
                        {
                            dbContextTransaction.Rollback();
                            error e1 = new error();
                            e1.idError = -3;
                            e1.Mensaje = "El pedido: #" + Pedido.NumeroPedido + " fue cancelado por el distribuidor";
                            return Json(e1, JsonRequestBehavior.AllowGet);
                        }
                        else if (Pedido.catEstatusClave == 12)
                        {
                            dbContextTransaction.Rollback();
                            error e1 = new error();
                            e1.idError = -4;
                            e1.Mensaje = "El pedido: #" + Pedido.NumeroPedido + "No tiene un pago registrado en el sistema,No puede ser surtido";
                            return Json(e1, JsonRequestBehavior.AllowGet);
                        }

                        List<series> series = new List<series>();
                        List<Articulos> articulos = new List<Articulos>(); ;

                        var myArray = new Newtonsoft.Json.Linq.JArray();
                        try
                        {
                            series = (List<series>)JsonConvert.DeserializeObject(Aparatos, typeof(List<series>));
                        }
                        catch
                        {

                        }
                        try
                        {
                            articulos = (List<Articulos>)JsonConvert.DeserializeObject(Articulos, typeof(List<Articulos>));

                        }
                        catch
                        {

                        }

                        salida = new Proceso();
                        salida.ProcesoClave = Guid.NewGuid();
                        salida.catEstatusClave = 1;
                        salida.catTipoProcesoClave = 3;
                        salida.EntidadClaveAfectada = almacenPrincipal;
                        salida.EntidadClaveSolicitante = Pedido.EntidadClaveSolicitante;
                        salida.ProcesoClaveRespuesta = Pedido.ProcesoClave;
                        salida.UsuarioClave = u.UsuarioClave;
                        salida.Fecha = DateTime.Now;
                        dc.Proceso.Add(salida);
                        dc.SaveChanges();

                        #region Insertando en PA
                        var result = from x in series
                                     group x by new { x.ArticuloClave }
                                         into g
                                     select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                        foreach (var a in result)
                        {
                            if (Se_pidio(Pedido.ProcesoClave, a.articulo.ArticuloClave))
                            {

                                int cuantos_se_pidieron = Pedido.ProcesoArticulo.Where(x => x.ArticuloClave == a.articulo.ArticuloClave).Select(o => o.Cantidad.Value).First();

                                int cuantos_surtidos = cuantosSurtidos(Salidas, a.articulo.ArticuloClave);

                                if (cuantos_se_pidieron < cuantos_surtidos + a.Cantidad)
                                {
                                    dbContextTransaction.Rollback();
                                    error e1 = new error();
                                    e1.idError = -12;
                                    e1.Mensaje = "La cantidad a surtir es mayor a la cantidad pedida por el distribuidor";
                                    return Json(e1, JsonRequestBehavior.AllowGet);

                                }
                                else
                                {
                                    ProcesoArticulo pa = new ProcesoArticulo();
                                    pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                                    pa.Cantidad = a.Cantidad;
                                    pa.ProcesoClave = salida.ProcesoClave;
                                    dc.ProcesoArticulo.Add(pa);
                                    dc.SaveChanges();
                                }



                            }
                            else
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -5;
                                e1.Mensaje = "El articulo: " + a.articulo + " No se encuentra en la orden de compra";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }
                        }

                        foreach (var b in articulos)
                        {
                            if (Se_pidio(Pedido.ProcesoClave, b.idarticulo))
                            {

                                int cuantos_se_pidieron = Pedido.ProcesoArticulo.Where(x => x.ArticuloClave == b.idarticulo).Select(o => o.Cantidad.Value).First();

                                int cuantos_surtidos = cuantosSurtidos(Salidas, b.idarticulo);


                                if (cuantos_se_pidieron < cuantos_surtidos + b.cantidad)
                                {
                                    dbContextTransaction.Rollback();
                                    error e1 = new error();
                                    e1.idError = -12;
                                    e1.Mensaje = "La cantidad a surtir es mayor a la cantidad pedida por el distribuidor";
                                    return Json(e1, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    ProcesoArticulo pa = new ProcesoArticulo();
                                    pa.ArticuloClave = Guid.Parse(b.idarticulo.ToString());
                                    pa.Cantidad = b.cantidad;
                                    pa.ProcesoClave = salida.ProcesoClave;
                                    dc.ProcesoArticulo.Add(pa);
                                    dc.SaveChanges();

                                }


                            }
                            else
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -11;
                                e1.Mensaje = "El articulo con ID: " + b.idarticulo.ToString().ToUpper() + " No se encuentra en la orden de compra";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }

                        }
                        #endregion


                        #region Insertando PI y modificando inventario

                        foreach (var APA in series)
                        {
                            if (tieneSeries(APA.ArticuloClave))
                            {
                                Inventario Inv = new Inventario();
                                try
                                {
                                    Inv = dc.Inventario.Where(o => o.InventarioClave == APA.InventarioClave && o.ArticuloClave == APA.ArticuloClave && o.EntidadClave == almacenPrincipal && o.EstatusInv == 7 && o.Standby == 0 && o.Cantidad == 1).First();
                                }
                                catch
                                {
                                    dbContextTransaction.Rollback();
                                    error e1 = new error();
                                    e1.idError = -6;
                                    e1.Mensaje = "El aparato con serie: " + APA.Valor + "No se encuentra en el saldo del almacén central o no se encuentra en buen estado";
                                    return Json(e1, JsonRequestBehavior.AllowGet);
                                }

                                ProcesoInventario pi = new ProcesoInventario();
                                pi.ProcesoClave = salida.ProcesoClave;
                                pi.InventarioClave = APA.InventarioClave;
                                pi.Cantidad = APA.Cantidad;
                                pi.Fecha = DateTime.Now;
                                dc.ProcesoInventario.Add(pi);

                                Inv.Standby = 1;
                                Inv.Cantidad = 0;
                                Inv.Estatus = 2;
                            }


                        }

                        foreach (var ART in articulos)
                        {


                            Inventario Inv2 = new Inventario();

                            try
                            {
                                Inv2 = dc.Inventario.Where(o => o.ArticuloClave == ART.idarticulo && o.EntidadClave == almacenPrincipal && o.EstatusInv == 7).First();

                            }
                            catch
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -7;
                                e1.Mensaje = "El artículo: #" + ART.idarticulo + " No se encuentra en el saldo del almacén central";
                                return Json(e1, JsonRequestBehavior.AllowGet);

                            }


                            ProcesoInventario pi = new ProcesoInventario();
                            pi.ProcesoClave = salida.ProcesoClave;
                            pi.InventarioClave = Inv2.InventarioClave;
                            pi.Cantidad = ART.cantidad;
                            pi.Fecha = DateTime.Now;
                            dc.ProcesoInventario.Add(pi);

                            if (ART.cantidad > Inv2.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -8;
                                e1.Mensaje = "El artículo: " + Inv2.Articulo.Nombre + "pretende ser surtido con una cantidad mayor al saldo que se registra en el almacén";
                                return Json(e1, JsonRequestBehavior.AllowGet);
                            }

                            Inv2.Cantidad = Inv2.Cantidad - ART.cantidad;
                            Inv2.Standby = Inv2.Standby + ART.cantidad;

                        }




                        #endregion



                        try
                        {
                            var qsc = from pa in dc.ProcesoArticulo
                                      join p in dc.Proceso on pa.ProcesoClave equals p.ProcesoClave
                                      where p.ProcesoClaveRespuesta == salida.ProcesoClaveRespuesta
                                      && p.catEstatusClave != 6
                                      group pa by pa.ArticuloClave into g
                                      select new
                                      {
                                          ArticuloClave = g.Key,
                                          Cantidad = g.Sum(pa => pa.Cantidad)
                                      };






                            int arts_pend = (from pa in dc.ProcesoArticulo
                                             join pan in qsc on pa.ArticuloClave equals pan.ArticuloClave into g
                                             from x in g.DefaultIfEmpty()
                                             where pa.ProcesoClave == salida.ProcesoClaveRespuesta
                                             && pa.Cantidad != x.Cantidad
                                             select pa).ToList().Count();

                            Proceso pg = (from p in dc.Proceso
                                          where p.ProcesoClave == salida.ProcesoClaveRespuesta
                                          select p).SingleOrDefault();

                            if (pg != null)
                            {
                                pg.catEstatusClave = (arts_pend == 0) ? 4 : 3;
                            }

                            dc.SaveChanges();
                        }
                        catch { }

                    }
                    else
                    {
                        dbContextTransaction.Rollback();
                        return Content("-1");
                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(salida.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {

                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -10;
                    e1.Mensaje = "se registro un error al procesar la salida";
                    e1.aux = ex.ToString();
                    return Json(e1, JsonRequestBehavior.AllowGet);

                }
            }


        }


        public class Detalles_pedido
        {
            public Guid Almacen { get; set; }
            public Guid pedido { get; set; }
            public Guid AlmacenDestino { get; set; }

        }


        public int cuantosSurtidos(List<Proceso> salidas, Guid idArticulo)
        {

            int cantidad = 0;

            foreach (var a in salidas)
            {

                foreach (var b in a.ProcesoArticulo.Where(x => x.ArticuloClave == idArticulo))
                {
                    cantidad = cantidad + b.Cantidad.Value;
                }
            }
            return cantidad;
        }


        //public bool falta_en_pedido(Proceso Pedido,int cantidad_surtida,Guid idarticulo )
        //{
        //     
        //     Guid pedido = Pedido.ProcesoClave;

        //     int cuantos_pedido = Pedido.ProcesoArticulo.Where(x=>x.ArticuloClave==idarticulo).Select(x=>x.Cantidad.Value).First();

        //     List<ProcesoArticulo> listpa = dc.ProcesoArticulo.Where(x => x.Proceso.ProcesoClaveRespuesta==pedido).ToList();
        //     int cantidad_surtida_anterior = 0;
        //     //if (lista_salidas.Count > 0)
        //     //{
        //     //    foreach (var salida in lista_salidas)
        //     //    {
        //     //        try{
        //     //        ProcesoArticulo Listapa = dc.ProcesoArticulo.Where(x => x.ArticuloClave == idarticulo).First();
        //     //        cantidad_surtida_anterior = cantidad_surtida_anterior + Listapa.Cantidad.Value;
        //     //        }catch{

        //     //        }
        //     //    }

        //     //    if (cuantos_pedido >= cantidad_surtida_anterior + cantidad_surtida)
        //     //    {
        //     //        return true;
        //     //    }
        //     //    else
        //     //    {
        //     //        return false;
        //     //    }

        //     //}
        //     //else
        //     //{
        //     //    return true;
        //     //}


        //     return false;

        //}





        public bool existe_pedido(Guid pedido)
        {

            if (dc.Proceso.Any(x => x.ProcesoClave == pedido))
            {
                return true;
            }
            else
            {
                return false;

            }
        }


        public bool Se_pidio(Guid pedido, Guid articulo)
        {

            List<ProcesoArticulo> pa = dc.ProcesoArticulo.Where(o => o.ArticuloClave == articulo && o.ProcesoClave == pedido).ToList();
            if (pa.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }





        public bool PuedeSurtirse(Guid idArticulo, int cantidad)
        {

            try
            {
                Guid almacenPrincipal = Guid.Parse("00000000-0000-0000-0000-000000000001");
                int cuantos_inventario = dc.Inventario.Where(x => x.ArticuloClave == idArticulo && x.EntidadClave == almacenPrincipal && x.EstatusInv == 7).Sum(o => o.Cantidad.Value);

                if (cantidad > cuantos_inventario)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        //public bool existe_aparato(Guid articulo, Guid inventarioClave, string serie)
        //{
        //    
        //    List<Inventario> lista=dc.Inventario.Where(x=>x.InventarioClave==inventarioClave && x.ArticuloClave==articulo).ToList();
        //    if (lista.Count > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}




        public ActionResult CancelaSalida(Guid id)
        {

            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Guid AP = Guid.Parse("00000000-0000-0000-0000-000000000001");
                    Proceso salida;
                    Proceso Pedido;
                    Usuario u = (Usuario)Session["u"];
                    try
                    {
                        salida = dc.Proceso.Where(x => x.ProcesoClave == id).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-1");
                    }
                    try
                    {
                        Pedido = dc.Proceso.Where(x => x.ProcesoClave == salida.ProcesoClaveRespuesta).First();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        return Content("-2");
                    }

                    if (salida.catEstatusClave == 6)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-3");
                    }
                    if (salida.catEstatusClave == 3)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-4");
                    }
                    if (salida.catEstatusClave == 4)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-5");
                    }

                    salida.catEstatusClave = 6;
                    salida.Observaciones = "Cancelado " + DateTime.Now + " usuario" + u.Nombre;

                    #region 
                    List<ProcesoArticulo> pa1 = dc.ProcesoArticulo.Where(x => x.ProcesoClave == salida.ProcesoClave).ToList();
                    if (pa1.Count == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-6");
                    }
                    else
                    {

                        foreach (var p in pa1)
                        {
                            p.CantidadEntregada = 0;
                        }

                    }
                    List<ProcesoInventario> pi = dc.ProcesoInventario.Where(x => x.ProcesoClave == salida.ProcesoClave).ToList();
                    if (pi.Count == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-7");
                    }
                    else
                    {
                        foreach (ProcesoInventario o in pi)
                        {
                            if (tieneSeries(o.Inventario.ArticuloClave.Value))
                            {
                                if (o.Inventario.EntidadClave.Value == AP && o.Inventario.EstatusInv.Value == 7 && o.Inventario.Cantidad.Value == 0 && o.Inventario.Standby.Value == 1 && o.Inventario.Estatus == 2)
                                {
                                    o.Inventario.Cantidad = 1;
                                    o.Inventario.Standby = 0;
                                    o.Inventario.Estatus = 1;
                                    dc.SaveChanges();
                                }
                                else
                                {
                                    dbContextTransaction.Rollback();
                                    return Content("-8");
                                }
                            }
                            else
                            {
                                if (o.Cantidad > o.Inventario.Standby)
                                {

                                    dbContextTransaction.Rollback();
                                    return Content("-9");
                                }
                                else
                                {
                                    o.Inventario.Standby = o.Inventario.Standby - o.Cantidad;
                                    o.Inventario.Cantidad = o.Inventario.Cantidad + o.Cantidad;
                                }


                            }
                        }
                    }
                    #endregion


                    dc.SaveChanges();



                    try
                    {
                        var qsc = from pa in dc.ProcesoArticulo
                                  join p in dc.Proceso on pa.ProcesoClave equals p.ProcesoClave
                                  where p.catEstatusClave != 6 && p.ProcesoClaveRespuesta == salida.ProcesoClaveRespuesta

                                  group pa by pa.ArticuloClave into g
                                  select new
                                  {
                                      ArticuloClave = g.Key,
                                      Cantidad = g.Sum(pa => pa.Cantidad)
                                  };






                        int arts_pend = (from pa in dc.ProcesoArticulo
                                         join pan in qsc on pa.ArticuloClave equals pan.ArticuloClave into g
                                         from x in g.DefaultIfEmpty()
                                         where pa.ProcesoClave == salida.ProcesoClaveRespuesta
                                         && pa.Cantidad != x.Cantidad
                                         select pa).ToList().Count();

                        Proceso pg = (from p in dc.Proceso
                                      where p.ProcesoClave == salida.ProcesoClaveRespuesta
                                      select p).SingleOrDefault();

                        if (pg != null)
                        {
                            Pedido.catEstatusClave = (arts_pend == 0) ? 4 : 3;
                        }

                        dc.SaveChanges();
                    }
                    catch { }




                    dbContextTransaction.Commit();
                    return Content(null);

                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }
        }

    }
}
