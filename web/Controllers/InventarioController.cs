﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class InventarioController : Controller
    {
        
        [SessionExpireFilterAttribute]
        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid al = Guid.Empty;
            try
            {
                al = Guid.Parse(Request["a"]);
            }
            catch (Exception ex)
            { }

            Entidad almacen = (from a in dc.Entidad
                                   where a.EntidadClave == al
                                   select a).SingleOrDefault();

            ViewData["almacen"] = almacen;
            return View();
        }



        //public ActionResult Articulo(string data)
        //{
        //    bool distribuidor = false;
        //    JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
        //    Guid almacen = Guid.Empty;
        //    try
        //    {
        //        almacen = Guid.Parse(jObject["Almacen"].ToString());
        //    }
        //    catch
        //    {
        //        almacen = Guid.Parse("00000000-0000-0000-0000-000000000001");
        //    }          
        //    Guid almacen2 = Guid.Empty;
        //    try
        //    {
        //        almacen2 = Guid.Parse(jObject["Almacen2"].ToString());
        //    }
        //    catch
        //    {
        //        almacen2 = Guid.Parse("00000000-0000-0000-0000-000000000001");
        //    }

        //    Guid articulo = Guid.Parse(jObject["Articulo"].ToString());
        //    Guid salida = Guid.Empty;
        //    try
        //    {
        //        salida = Guid.Parse(jObject["Salida"].ToString());
        //    }
        //    catch {}
        //    string TipoSalida = "tecnico";
        //    try
        //    {
        //        TipoSalida = jObject["TipoSalida"].ToString();
        //    }
        //    catch { 

        //    }

        //    ModeloAlmacen dc = new ModeloAlmacen();
        //    List<InventarioWrapper> liw = new List<InventarioWrapper>();
        //    List<InventarioWrapper> lista_ordenada = new List<InventarioWrapper>();
        //    List<Inventario> rls = (from inv in dc.Inventario
        //                                where inv.EntidadClave == almacen
        //                                && inv.ArticuloClave == articulo
        //                                && inv.Estatus == 1
        //                                && inv.EstatusInv == 7
        //                                && inv.Cantidad.Value > 0
        //                                && inv.Serie.Count > 0
        //                                select inv).ToList();
        //    List<ProcesoInventario> invsalida = (from i in dc.ProcesoInventario
        //                                             where i.ProcesoClave == salida
        //                                             && (i.Proceso.catTipoProcesoClave == 3 || i.Proceso.catTipoProcesoClave == 12)
        //                                             && i.Inventario.ArticuloClave == articulo
        //                                             select i).ToList();

        //    int cuantosproceso = 0; 
        //    foreach (ProcesoInventario pro in invsalida)
        //    {
        //        if (pro.Inventario.Estatus == 1 || (TipoSalida == "traspaso" && pro.Inventario.Estatus == 2) || (TipoSalida == "tecnico" && pro.Inventario.Estatus == 2))
        //        {
        //            cuantosproceso++;
        //            rls.Insert(0, pro.Inventario);
        //        }
        //    }
        //    bool inicial = true;
        //    int contador = 0;
        //    foreach (Inventario i in rls)
        //    {
        //        InventarioWrapper iw = null;

        //        contador++;
        //        if (almacen2 != Guid.Empty)
        //        {
        //            if (contador > cuantosproceso)
        //            {
        //                iw = Invetnario2Wrapper(i, almacen2, inicial,true);                    
        //            }
        //            else
        //            {
        //                iw = Invetnario2Wrapper(i, almacen2, inicial,false);
        //            }
        //        }
        //        else
        //        {
        //            iw = Invetnario2Wrapper(i, almacen, inicial,true);
        //        }

        //        liw.Add(iw);               
        //    }
        //    lista_ordenada = liw.OrderBy(o=>o.Seriales.First().datos.First().Valor).ToList();
        //    return Json(lista_ordenada, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult Serie(string data)
        {
            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            ModeloAlmacen dc = new ModeloAlmacen();
            bool vbExiste = false;
            JArray arreglo = JArray.Parse(jObject["serie"].ToString());
            foreach (JObject obj in arreglo.Children<JObject>())
            {
                if (!vbExiste)
                {
                    String vaSerie = obj["Valor"].ToString();
                    vbExiste = (from s in dc.Serie
                                where s.Valor == vaSerie
                                select s).ToList().Count() != 0;
                }
            }

            return Json("{bExiste:" + ((vbExiste) ? "true" : "false") + "}", JsonRequestBehavior.AllowGet);
        }


        Guid almacen = Guid.Empty;
        //public ActionResult ObtenArbol(string data)
        //{
        //    int tipo = 1;

        //    if (Request["t"] != null)
        //    {
        //        tipo = Convert.ToInt32(Request["t"]);
        //    }

        //    if (Request["almacen"] != null)
        //    {
        //        almacen = Guid.Parse(Request["almacen"].ToString());
        //    }

        //    return Content(Arbol(tipo));
        //}

        //private string Arbol(int tipo)
        //{
        //    ModeloAlmacen dc = new ModeloAlmacen();

        //    StringBuilder sb = new StringBuilder();

        //    List<catClasificacionArticulo> r = (from e in dc.catClasificacionArticulo
        //                           where e.Activo
        //                           orderby e.Descripcion
        //                           select e).ToList();



        //    int conta = 0;
        //    foreach (catClasificacionArticulo e in r)
        //    {
        //        sb.Append(Nodo(conta++, -1, e.Descripcion, e.catClasificacionArticuloClave));
        //    }

        //    return sb.ToString();
        //}

        
        //private string Nodo(int contador, int padre, string nombre, int clave)
        //{
        //    ModeloAlmacen dc = new ModeloAlmacen();

        //    StringBuilder sb = new StringBuilder();

        //    string spadre = String.Empty;
        //    if (padre != -1)
        //    {
        //        spadre = "treegrid-parent-" + padre;
        //    }


        //    sb.Append("<tr class='treegrid-" + clave + " " + spadre + "'>");

        //    sb.Append("<td colspan='4'><b>" + nombre + "</b></td>");

        //    sb.Append("</tr>");


        //    List<catTipoArticulo> r = (from e in dc.catTipoArticulo
        //                               where e.catClasificacionArticuloClave == clave
        //                               && e.Activo == true
        //                               orderby e.Descripcion
        //                               select e).ToList();

        //    int conta = 0;
        //    foreach (catTipoArticulo e in r)
        //    {
        //        sb.Append("<tr class='treegrid-ta" + e.catTipoArticuloClave + " treegrid-parent-1'>");
        //        sb.Append("<td colspan='4'><b>" + e.Descripcion + "</b></td>");
        //        sb.Append("</tr>");

        //            //sb.Append(Nodo(conta++, clave, e.Descripcion, e.catTipoArticuloClave));

        //            List<Articulo> arts = (from a in dc.Articulo
        //                                           where a.catTipoArticuloClave == e.catTipoArticuloClave
        //                                           && a.Activo == true
        //                                           orderby e.Descripcion
        //                                           select a).ToList();
        //            foreach (Articulo a in arts)
        //            {
        //                sb.Append(NodoHijo(conta++, clave, a.Nombre, a.ArticuloClave));
        //            }    

        //    }
            
        //    return sb.ToString();
        //}





        //private string NodoHijo(int contador, int padre, string nombre, Guid clave)
        //{
        //    ModeloAlmacen dc = new ModeloAlmacen();

        //    StringBuilder sb = new StringBuilder();

        //    string spadre = String.Empty;
        //    if (padre != -1)
        //    {
        //        spadre = "treegrid-parent-" + padre;
        //    }


        //    sb.Append("<tr class='treegrid-" + clave + " " + spadre + "'>");

        //    sb.Append("<td>" + nombre + "</td>");

        //    int? activos = (from a in dc.Inventario
        //                    where a.ArticuloClave == clave
        //                    && a.EntidadClave == almacen
        //                    && a.EstatusInv == 7
        //                    && a.Estatus.HasValue
        //                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                    select a).Sum(a => a.Cantidad);


        //    int? stand_by = (from a in dc.Inventario
        //                     where a.ArticuloClave == clave
        //                     && a.EntidadClave == almacen
        //                     && a.EstatusInv == 7
        //                     && a.Estatus.HasValue
        //                     && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                     select a).Sum(a => a.Standby);


        //    int? recuperados = (from a in dc.Inventario
        //                        where a.ArticuloClave == clave
        //                        && a.EntidadClave == almacen
        //                        && a.EstatusInv == 9
        //                        && a.Estatus.HasValue
        //                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                        select a).Sum(a => a.Cantidad);

        //    int? baja = (from a in dc.Inventario
        //                        where a.ArticuloClave == clave
        //                        && a.EntidadClave == almacen
        //                        && a.EstatusInv == 11
        //                        && a.Estatus.HasValue
        //                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                        select a).Sum(a => a.Cantidad);

        //    int? danados = (from a in dc.Inventario
        //                 where a.ArticuloClave == clave
        //                 && a.EntidadClave == almacen
        //                 && a.EstatusInv == 10
        //                 && a.Estatus.HasValue
        //                 && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                 select a).Sum(a => a.Cantidad);

        //    int? Reparacion = (from a in dc.Inventario
        //                 where a.ArticuloClave == clave
        //                 && a.EntidadClave == almacen
        //                 && a.EstatusInv == 13
        //                 && a.Estatus.HasValue
        //                 && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                 select a).Sum(a => a.Cantidad);

        //    int? baja_garantia = (from a in dc.Inventario
        //                       where a.ArticuloClave == clave
        //                       && a.EntidadClave == almacen
        //                       && a.EstatusInv == 14
        //                       && a.Estatus.HasValue
        //                       && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                       select a).Sum(a => a.Cantidad);


        //    int? devolucion = (from a in dc.Inventario
        //                          where a.ArticuloClave == clave
        //                          && a.EntidadClave == almacen                             
        //                          && a.Estatus.Value ==3
        //                          select a).Sum(a => a.Standby);


        //    int cuantos_activos = 0;
        //    try
        //    {
        //        cuantos_activos = activos.Value;
        //    }
        //    catch { 
        //    }

        //    int cuantos_devolucion = 0;
        //    try
        //    {
        //        cuantos_devolucion = devolucion.Value;
        //    }
        //    catch
        //    {
        //    }


        //    int cuantos_stanby = 0;
        //    try
        //    {
        //        cuantos_stanby = stand_by.Value;
        //    }
        //    catch
        //    {
        //    }
        //    int cuantos_recuperados = 0;
        //    try
        //    {
        //        cuantos_recuperados = recuperados.Value;
        //    }
        //    catch
        //    {
        //    }
        //    int cuantos_baja = 0;
        //    try
        //    {
        //        cuantos_baja = baja.Value;
        //    }
        //    catch
        //    {
        //    }
        //    int cuantos_reparacion = 0;
        //    try
        //    {
        //        cuantos_reparacion = Reparacion.Value;
        //    }
        //    catch
        //    {
        //    }
        //    int cuantos_dan = 0;

        //    try
        //    {
        //        cuantos_dan = danados.Value;
        //    }
        //    catch
        //    {
        //    }
        //    int c_baja_gar = 0;
        //    try
        //    {
        //        c_baja_gar = baja_garantia.Value;
        //    }
        //    catch
        //    {
        //    }

        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_activos).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_stanby).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_devolucion).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_recuperados).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_baja).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_dan).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(cuantos_reparacion).ToString("N0") + "</td>");
        //    sb.Append("<td>" + Convert.ToDecimal(c_baja_gar).ToString("N0") + "</td>");
        //    sb.Append("</tr>");


        //    return sb.ToString();
        //}



        public InventarioWrapper Invetnario2Wrapper(Inventario i, Guid almacen, bool inicial, bool forzardisponible)
        {

            InventarioWrapper iw = new InventarioWrapper();
            iw.Clave = i.InventarioClave.ToString();
            iw.ArticuloClave = i.ArticuloClave.ToString();
            iw.AlmacenClave = i.EntidadClave.ToString();
            iw.Seriales = new List<SerialesWrapper>();
            if ((almacen.ToString() == iw.AlmacenClave && i.Cantidad>0) || forzardisponible)
            {
                iw.disponible = true;
            }
            else
            {
                iw.disponible = false;            
            }

            List<Serie> series = i.Serie.ToList();
            SerialesWrapper sw = new SerialesWrapper();
            sw.InventarioClave = i.InventarioClave.ToString();
            sw.ArticuloClave = i.ArticuloClave.ToString();
            ModeloAlmacen dc = new ModeloAlmacen();
            catEstatus ce = (from es in dc.catEstatus
                                     where es.catEstatusClave == i.EstatusInv
                                     select es).SingleOrDefault();

            sw.EstatusTexto = ce.Nombre;
            sw.EstatusClave = i.EstatusInv.ToString();
            sw.datos = new List<SerialesDatosWrapper>();
            foreach (Serie s in series)
            {
                SerialesDatosWrapper sdw = new SerialesDatosWrapper();
                sdw.Clave = s.catSerieClave.ToString();
                sdw.Valor = s.Valor.ToString();
                sw.datos.Add(sdw);
            }

            iw.Seriales.Add(sw);
            if (inicial)
            {
                iw.catSeriesClaves = new List<catSerieWrapper>();
                List<web.Models.SerieTipoArticulo> tiposarticulo = i.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();
                foreach (web.Models.SerieTipoArticulo sta in tiposarticulo)
                {
                    catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                    iw.catSeriesClaves.Add(csw);
                }
            }
            else {
                iw.catSeriesClaves = null;
            }

            return iw;
        }

        private catSerieWrapper SerieToWrapper(catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            return rw;
        }



        public ActionResult ReporteInventario(string id)
        {
            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            StringBuilder sb = new StringBuilder();

            ModeloAlmacen dc = new ModeloAlmacen();
            Guid entidadclave = Guid.Parse(id);
            Entidad entidad = dc.Entidad.Where(s => s.EntidadClave == entidadclave).First();

            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h3><b>Reporte de inventario</b></h3>");
            if (entidad.catEntidadTipoClave == 1)
            {
                sb.Append("<b>Almacén: </b>" + entidad.Nombre+"<br>");
            }
            else
            {
                sb.Append("<b>Técnico: </b>" + entidad.Nombre+ "<br>");
            }
            sb.Append("<b>Fecha: </b>" + DateTime.Now.ToShortDateString() + " <br>");
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 align=right><br>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<br>");





            var lista_tipos = (from a in dc.Inventario
                               join b in dc.Articulo
                                on a.ArticuloClave equals b.ArticuloClave
                               where a.EntidadClave == entidad.EntidadClave
                               && a.Cantidad > 0
                               group b by b.catTipoArticuloClave into g1
                               select new { catTipoArticuloClave = g1.Key }
                             ).ToList();

            lista_tipos.ForEach(lt => {

                catTipoArticulo ctp = dc.catTipoArticulo.Where(s => s.catTipoArticuloClave == lt.catTipoArticuloClave).First();

                sb.Append(@"<b style=""font-size:8px"">Clasificación:  " + ctp.catClasificacionArticulo.Descripcion + "</b><br>");
                sb.Append(@"<b style=""font-size:8px"">Tipo de Artículo:  " + ctp.Descripcion + "</b> <br>");

                sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                sb.Append("<tr>");
                sb.Append(@"<td colspan=3 style=""font-size:8px;text-decoration: underline;"">Artículo:</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Activos</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Revisión</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">En baja</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Dañados</td>");
                sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">En reparación</td>");
                sb.Append("</tr>");

                var lista_articulos = (from a in dc.Articulo
                                       join b in dc.Inventario
                       on a.ArticuloClave equals b.ArticuloClave
                                       where b.Cantidad > 0 && b.EntidadClave == entidad.EntidadClave
                                       && a.catTipoArticuloClave == ctp.catTipoArticuloClave
                                       group a by a.ArticuloClave into g2
                                       select new { articuloClave = g2.Key }).ToList();

                lista_articulos.ForEach(s => {

                    Articulo art = dc.Articulo.Where(c => c.ArticuloClave == s.articuloClave).First();

                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == art.ArticuloClave
                                    && a.EntidadClave == entidad.EntidadClave
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);
                    int cuantos_activos = 0;
                    try
                    {
                        cuantos_activos = activos.Value;
                    }
                    catch
                    {
                    }

                    int? recuperados = (from a in dc.Inventario
                                        where a.ArticuloClave == art.ArticuloClave
                                        && a.EntidadClave == entidad.EntidadClave
                                        && a.EstatusInv == 9
                                        && a.Estatus.HasValue
                                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                        select a).Sum(a => a.Cantidad);



                    int cuantos_recuperados = 0;
                    try
                    {
                        cuantos_recuperados = recuperados.Value;
                    }
                    catch
                    {
                    }


                    int? baja = (from a in dc.Inventario
                                 where a.ArticuloClave == art.ArticuloClave
                                 && a.EntidadClave == entidad.EntidadClave
                                 && a.EstatusInv == 11
                                 && a.Estatus.HasValue
                                 && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                 select a).Sum(a => a.Cantidad);

                    int cuantos_baja = 0;
                    try
                    {
                        cuantos_baja = baja.Value;
                    }
                    catch
                    {
                    }

                    int? danados = (from a in dc.Inventario
                                    where a.ArticuloClave == art.ArticuloClave
                                    && a.EntidadClave == entidad.EntidadClave
                                    && a.EstatusInv == 10
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);

                    int cuantos_danados = 0;
                    try
                    {
                        cuantos_danados = danados.Value;
                    }
                    catch
                    {
                    }

                    int? Reparacion = (from a in dc.Inventario
                                       where a.ArticuloClave == art.ArticuloClave
                                       && a.EntidadClave == entidad.EntidadClave
                                       && a.EstatusInv == 13
                                       && a.Estatus.HasValue
                                       && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                       select a).Sum(a => a.Cantidad);

                    int cuantos_Reparacion = 0;
                    try
                    {
                        cuantos_Reparacion = danados.Value;
                    }
                    catch
                    {
                    }

                    sb.Append("<tr>");
                    sb.Append(@"<td colspan=3 style=""font-size:8px;"">" + art.Nombre + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cuantos_activos + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cuantos_recuperados + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cuantos_baja + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cuantos_danados + "</td>");
                    sb.Append(@"<td style=""font-size:8px;"">" + cuantos_Reparacion + "</td>");
                    sb.Append("</tr>");
                });
                sb.Append("</table>");
                sb.Append("<br><br>");

            });

            PDF elpdf = new PDF();
            Guid g = Guid.NewGuid();
            string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "inventario.pdf";
            elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
            ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "inventario.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
            return View();

        }



        //public ActionResult getinventariotopdf(string blogPostId)
        //{
        //    string empresa = Config.Dame("EMPRESA");
        //    string logo = Config.Dame("LOGO_ENTRADA");
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<font size=2>");


        //    sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
        //    sb.Append("<tr>");
        //    sb.Append(@"<td align=""left"">");

        //    sb.Append("<h2>" + empresa + "</h2>");
        //    ModeloAlmacen dc = new ModeloAlmacen();
        //    Guid al = Guid.Parse(blogPostId);
        //    List<InventarioHelper> ih = new List<InventarioHelper>();
        //    var plaza = (from enti in dc.Entidad where enti.EntidadClave == al select enti);
        //    string distribuidor = "";
        //    string almacen = "";
        //    string tecnico = "";

        //    foreach (var p in plaza) { 

        //        if (p.catEntidadTipoClave == 2)//si es un tecnico
        //        {
        //            distribuidor = (from dis in dc.catDistribuidor where dis.catDistribuidorClave == p.catDistribuidorClave select dis.Nombre).FirstOrDefault();
        //            almacen = (from amb in dc.AmbitoAlmacen join ent in dc.Entidad on amb.EntidadClaveAlmacen equals ent.EntidadClave where amb.EntidadClaveTecnico == p.EntidadClave select ent.Nombre).FirstOrDefault().ToString();
        //            tecnico = p.Nombre;
        //        }
        //        else
        //        {
        //            distribuidor = (from dis in dc.catDistribuidor where dis.catDistribuidorClave == p.catDistribuidorClave select dis.Nombre).FirstOrDefault();
        //            almacen = p.Nombre;
        //            tecnico = "";
        //        }
        //    sb.Append("<h3>Reporte de inventario </h3><br>");
        //    sb.Append("<h5>Fecha: "+DateTime.Now+" </h5><br>");
        //    if (distribuidor != null)
        //    {
        //        sb.Append("<h5>Distribuidor: " + distribuidor + " </h5><br>");
        //    }

            
        //    sb.Append("<h5>Almacen: "+almacen+" </h5><br>");
        //   if (tecnico !=""){
        //       sb.Append("<h5>Nombre del técnico :" + tecnico + " </h5><br>");
        //   }

        //    sb.Append("</td>");
        //    sb.Append(@"<td align=""right"">");
        //    sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
        //    sb.Append("</td>");
        //    sb.Append("</tr>");
        //    sb.Append("</table>");
            

            

        //    Guid entidad1 = Guid.Parse(blogPostId);


        //    List<Articulo> arts = (from a in dc.Articulo
        //                               where
        //                               a.Activo == true
        //                               orderby a.Nombre
        //                               select a).ToList();
        //    foreach (Articulo ar in arts)
        //    {

        //        InventarioHelper obj = new InventarioHelper();
        //        dc = new ModeloAlmacen();
        //        var clas = (from an in dc.catClasificacionArticulo where an.catClasificacionArticuloClave == ar.catTipoArticulo.catClasificacionArticuloClave select an.Descripcion);
        //        var tipo_articulo = ar.catTipoArticulo.Descripcion;

        //        obj.CLASIFICACION = clas.FirstOrDefault();
        //        obj.TIPO = tipo_articulo.ToString();
        //        obj.ARTICULO = ar.Nombre.ToString();


        //        int? activos = (from a in dc.Inventario
        //                        where a.ArticuloClave == ar.ArticuloClave
        //                        && a.EntidadClave == entidad1
        //                        && a.EstatusInv == 7
        //                        && a.Estatus.HasValue
        //                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                        select a).Sum(a => a.Cantidad);



        //        int? stand_by = (from a in dc.Inventario
        //                         where a.ArticuloClave == ar.ArticuloClave
        //                         && a.EntidadClave == entidad1
        //                         && a.EstatusInv == 7
        //                         && a.Estatus.HasValue
        //                         && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                         select a).Sum(a => a.Standby);


        //        int? recuperados = (from a in dc.Inventario
        //                            where a.ArticuloClave == ar.ArticuloClave
        //                            && a.EntidadClave == entidad1
        //                            && a.EstatusInv == 9
        //                            && a.Estatus.HasValue
        //                            && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
        //                            select a).Sum(a => a.Cantidad);
        //        int cuantos_activos = 0;
        //        try
        //        {
        //            cuantos_activos = activos.Value;
        //        }
        //        catch
        //        {
        //        }


        //        int cuantos_stanby = 0;
        //        try
        //        {
        //            cuantos_stanby = stand_by.Value;
        //        }
        //        catch
        //        {
        //        }
        //        int cuantos_recuperados = 0;
        //        try
        //        {
        //            cuantos_recuperados = recuperados.Value;
        //        }
        //        catch
        //        {
        //        }
        //        obj.INVENTARIO = cuantos_activos.ToString();
        //        obj.REVISION = cuantos_recuperados.ToString();
        //        obj.STANDBY = cuantos_stanby.ToString();
        //        ih.Add(obj);

        //    }
        
        //    sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"">");
        //    sb.Append("<thead>");
        //    sb.Append("<tr>");
        //    sb.Append(@"<td align=""center"" bgcolor=""#021c3c"" color=""#FFFFFF"">");
        //    sb.Append("Clasificación");
        //    sb.Append("</td>");
        //    sb.Append(@"<td align=""center"" bgcolor=""#021c3c"" color=""#FFFFFF"">");
        //    sb.Append("Tipo");
        //    sb.Append("</td>");
        //    sb.Append(@"<td align=""center"" bgcolor=""#021c3c"" color=""#FFFFFF"">");
        //    sb.Append("Artículo");
        //    sb.Append("</td>");
        //    sb.Append(@"<td align=""center"" bgcolor=""#021c3c"" color=""#FFFFFF"">");
        //    sb.Append("Inventario");
        //    sb.Append("</td>");
        //    sb.Append(@"<td align=""center"" bgcolor=""#021c3c"" color=""#FFFFFF"">");
        //    sb.Append("Standby");
        //    sb.Append("</td>");
        //   sb.Append("</td>");
        //   sb.Append(@"<td align=""center"" bgcolor=""#021c3c"" color=""#FFFFFF"">");
        //    sb.Append("Revisión");
        //    sb.Append("</td>");
        //    sb.Append("</tr>");
        //    sb.Append("</thead>");
        //    sb.Append("<tbody>");
        //     foreach(var a in ih){

        //    sb.Append("<tr>");
        //    sb.Append(@"<td style=""font-size:8px;"">");
        //    sb.Append(a.CLASIFICACION);
        //    sb.Append("</td>");
        //    sb.Append(@"<td style=""font-size:8px;"">");
        //    sb.Append(a.TIPO);
        //    sb.Append("</td>");
        //    sb.Append(@"<td style=""font-size:8px;"">");
        //    sb.Append(a.ARTICULO);
        //    sb.Append("</td>");
        //    sb.Append(@"<td style=""font-size:8px;"">");
        //    sb.Append(a.INVENTARIO);
        //    sb.Append("</td>");
        //    sb.Append(@"<td style=""font-size:8px;"">");
        //    sb.Append(a.STANDBY);
        //    sb.Append("</td>");
        //    sb.Append(@"<td style=""font-size:8px;"">");
        //    sb.Append(a.REVISION);
        //    sb.Append("</td>");
        //    sb.Append("</tr>");
        //     }
        //    sb.Append("</tbody>");
        //    sb.Append("</table>");


        //    }
        //    PDF elpdf = new PDF();

        //    Guid g = Guid.NewGuid();

        //    string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "inventario.pdf";

        //    elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
        //    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "inventario.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
        //    return View();
           
        //}



        public ActionResult getinventariotoexcel(string blogPostId)
        {

            List<InventarioHelper> ih=new List<InventarioHelper>();
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid entidad1 = Guid.Parse(blogPostId);
            List<Articulo> arts = (from a in dc.Articulo
                                           where
                                           a.Activo == true
                                           orderby a.Nombre
                                           select a).ToList();
                foreach (Articulo ar in arts)
                {

                    InventarioHelper obj = new InventarioHelper();
                    dc = new ModeloAlmacen();
                    var clas = (from an in dc.catClasificacionArticulo where an.catClasificacionArticuloClave == ar.catTipoArticulo.catClasificacionArticuloClave select an.Descripcion);
                    var tipo_articulo = ar.catTipoArticulo.Descripcion;

                    obj.CLASIFICACION = clas.FirstOrDefault();
                    obj.TIPO = tipo_articulo.ToString();
                    obj.ARTICULO = ar.Nombre.ToString();
                   

                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == ar.ArticuloClave
                                    && a.EntidadClave == entidad1
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);
                   
                    

                    int? stand_by = (from a in dc.Inventario
                                     where a.ArticuloClave == ar.ArticuloClave
                                     && a.EntidadClave == entidad1
                                     && a.EstatusInv == 7
                                     && a.Estatus.HasValue
                                     && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                     select a).Sum(a => a.Standby);

                   
                    int? recuperados = (from a in dc.Inventario
                                        where a.ArticuloClave == ar.ArticuloClave
                                        && a.EntidadClave == entidad1
                                        && a.EstatusInv == 9
                                        && a.Estatus.HasValue
                                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                        select a).Sum(a => a.Cantidad);
                    int cuantos_activos = 0;
                    try
                    {
                        cuantos_activos = activos.Value;
                    }
                    catch
                    {
                    }


                    int cuantos_stanby = 0;
                    try
                    {
                        cuantos_stanby = stand_by.Value;
                    }
                    catch
                    {
                    }
                    int cuantos_recuperados = 0;
                    try
                    {
                        cuantos_recuperados = recuperados.Value;
                    }
                    catch
                    {
                    }
                    obj.INVENTARIO = cuantos_activos.ToString();
                    obj.REVISION = cuantos_recuperados.ToString();
                    obj.STANDBY = cuantos_stanby.ToString();
                    ih.Add(obj);

                }
                Guid al = Guid.Parse(blogPostId);
            var plaza=(from enti in dc.Entidad where enti.EntidadClave==al select enti);
            string distribuidor = "";
            string almacen = "";
            string tecnico = "";
            foreach (var p in  plaza)

            if (p.catEntidadTipoClave ==2)//si es un tecnico
            {
                distribuidor=(from dis in dc.catDistribuidor where dis.catDistribuidorClave == p.catDistribuidorClave select dis.Nombre).FirstOrDefault();
                almacen=(from amb in dc.AmbitoAlmacen join ent in dc.Entidad on amb.EntidadClaveAlmacen equals ent.EntidadClave where amb.EntidadClaveTecnico ==p.EntidadClave select ent.Nombre).FirstOrDefault().ToString();
                tecnico=p.Nombre;
            }
            else
            {
                distribuidor = (from dis in dc.catDistribuidor where dis.catDistribuidorClave == p.catDistribuidorClave select dis.Nombre).FirstOrDefault();
                 almacen = p.Nombre;
                 tecnico = "";
            }


            
          
            
              DataTable table=  ConvertToDatatable(ih);
             
              DumpExcel(table,almacen, distribuidor, tecnico);
            return Content("");
        }


        static DataTable ConvertToDatatable(List<InventarioHelper> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("CLASIFICACION");
            dt.Columns.Add("TIPO");
            dt.Columns.Add("ARTICULO");
            dt.Columns.Add("INVENTARIO");
            dt.Columns.Add("STANDBY");
            dt.Columns.Add("REVISION");
            foreach (var item in list)
            {
                var row = dt.NewRow();

                row["CLASIFICACION"] = item.CLASIFICACION.ToString();
                row["TIPO"] = item.TIPO;
                row["ARTICULO"] = item.ARTICULO;
                row["INVENTARIO"] = item.INVENTARIO;
                row["STANDBY"] = item.STANDBY;
                row["REVISION"] = item.REVISION;
                dt.Rows.Add(row);
            }

            return dt;
        }

        private void DumpExcel(DataTable tbl, string almacen, string distribuidor,string tecnico)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
             
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Inventario");
                ws.Cells["A4"].LoadFromDataTable(tbl, true);
                using (ExcelRange rng = ws.Cells["A4:F4"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }           
                using (ExcelRange rng = ws.Cells["A1:F3"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);

                }
                using (ExcelRange rng = ws.Cells["A1:F1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.Red);  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }
                ws.Column(1).Width = 30;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 30;
                ws.Column(4).Width = 30;
                ws.Column(5).Width = 30;
                ws.Column(6).Width = 30;
                ws.Cells["C1"].Value = "";
                ws.Cells["A2"].Value = "DISTRIBUIDOR";
                ws.Cells["B2"].Value = distribuidor;
                ws.Cells["A3"].Value = "ALMACEN";
                ws.Cells["B3"].Value = almacen;
                ws.Cells["C2"].Value = " INVENTARIO";
                if (tecnico != "") { 
                ws.Cells["C3"].Value = " TECNICO";
                ws.Cells["D3"].Value = tecnico;
                }
             
                using (ExcelRange col = ws.Cells[5, 1, 5 + tbl.Rows.Count, 1])
                {                   
                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                }              
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Inventario.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }




        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Almacen> data { get; set; }
        }

        public class Almacen
        {
            public Guid IdEntidad { get; set; }
            public string Nombre { get; set; }
        }

        public ActionResult ObtenAlmacenes(int distribuidor,int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaAlmacenes(distribuidor).OrderBy(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaAlmacenes(distribuidor).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
          }

        public List<Almacen> ListaAlmacenes(int distribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            List<Almacen> almacenes;
            if (distribuidor == -1)
            {
                almacenes = dc.Entidad.Where(o => o.IdEntidad == 1).Select(x => new Almacen { IdEntidad = x.EntidadClave, Nombre = x.Nombre }).ToList();
            }
            else if (distribuidor == -2)
            {
                List<Almacen> aux = new List<Almacen>();
                Almacen al = new Almacen();
                al.IdEntidad = Guid.Empty;
                al.Nombre = "Todos los almacenes y distribuidores";
                aux.Add(al);
                almacenes = aux;
            }
            else
            {
                almacenes = dc.Entidad.Where(x => x.catDistribuidor.IdDistribuidor == distribuidor && x.catEntidadTipoClave == 1).Select(x => new Almacen {  IdEntidad=x.EntidadClave, Nombre=x.Nombre}).ToList();
            }
            return almacenes;
        }

       

        public class StatusInv{

            public string Nombre { get; set; }
            public int enAlmacen{get; set;}
            public int standby{get;set;}
            public int enpedidos { get; set; }
            public int devolucion {get;set;}
            public int revision {get;set;}
            public int baja {get;set;}

            public int dañados {get;set;}
             public int reparacion {get;set;}
             public int bajagarantia {get;set;}
        }


        public ActionResult DetalleInventario(Guid entidad)
        {
            ModeloAlmacen dc=new ModeloAlmacen();
            List<Articulo> articulos= dc.Articulo.ToList();

            List<StatusInv> lista = new List<StatusInv>();

            foreach(var b in articulos){

                if (entidad == Guid.Empty)
                {
                    StatusInv obj = new StatusInv();
                    obj.Nombre = b.Nombre;
                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == b.ArticuloClave
                                    && a.EntidadClave !=null
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue                                   
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);


                    int? stand_by = (from a in dc.Inventario
                                     where a.ArticuloClave == b.ArticuloClave
                                     && a.EntidadClave != null
                                     && a.EstatusInv == 7
                                     && a.Estatus.HasValue
                                     && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                     select a).Sum(a => a.Standby);


                    int? recuperados = (from a in dc.Inventario
                                        where a.ArticuloClave == b.ArticuloClave
                                        && a.EntidadClave != null
                                        && a.EstatusInv == 9
                                        && a.Estatus.HasValue
                                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                        select a).Sum(a => a.Cantidad);

                    int? baja = (from a in dc.Inventario
                                 where a.ArticuloClave == b.ArticuloClave
                                 && a.EntidadClave != null
                                 && a.EstatusInv == 11
                                 && a.Estatus.HasValue
                                 && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                 select a).Sum(a => a.Cantidad);

                    int? danados = (from a in dc.Inventario
                                    where a.ArticuloClave == b.ArticuloClave
                                    && a.EntidadClave != null
                                    && a.EstatusInv == 10
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);

                    int? Reparacion = (from a in dc.Inventario
                                       where a.ArticuloClave == b.ArticuloClave
                                       && a.EntidadClave != null
                                       && a.EstatusInv == 13
                                       && a.Estatus.HasValue
                                       && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                       select a).Sum(a => a.Cantidad);

                    int? baja_garantia = (from a in dc.Inventario
                                          where a.ArticuloClave == b.ArticuloClave
                                          && a.EntidadClave != null
                                          && a.EstatusInv == 14
                                          && a.Estatus.HasValue
                                          && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                          select a).Sum(a => a.Cantidad);


                    int? devolucion = (from a in dc.Inventario
                                       where a.ArticuloClave == b.ArticuloClave
                                       && a.EntidadClave != null
                                       && a.Estatus.Value == 3
                                       select a).Sum(a => a.Standby);


                    int? enpedidos = 0;


                    int cuantos_activos = 0;
                    try
                    {
                        cuantos_activos = activos.Value;
                    }
                    catch
                    {
                    }

                    obj.enAlmacen = cuantos_activos;

                    int cuantos_devolucion = 0;
                    try
                    {
                        cuantos_devolucion = devolucion.Value;
                    }
                    catch
                    {
                    }
                    obj.devolucion = cuantos_devolucion;

                    int cuantos_stanby = 0;
                    try
                    {
                        cuantos_stanby = stand_by.Value;
                    }
                    catch
                    {
                    }
                    obj.standby = cuantos_stanby;

                    int cuantos_recuperados = 0;
                    try
                    {
                        cuantos_recuperados = recuperados.Value;
                    }
                    catch
                    {
                    }
                    obj.revision = cuantos_recuperados;


                    int cuantos_baja = 0;
                    try
                    {
                        cuantos_baja = baja.Value;
                    }
                    catch
                    {
                    }

                    obj.baja = cuantos_baja;

                    int cuantos_reparacion = 0;
                    try
                    {
                        cuantos_reparacion = Reparacion.Value;
                    }
                    catch
                    {
                    }
                    obj.reparacion = cuantos_reparacion;

                    int cuantos_dan = 0;

                    try
                    {
                        cuantos_dan = danados.Value;
                    }
                    catch
                    {
                    }
                    obj.dañados = cuantos_dan;

                    int c_baja_gar = 0;
                    try
                    {
                        c_baja_gar = baja_garantia.Value;
                    }
                    catch
                    {
                    }

                    obj.bajagarantia = c_baja_gar;
                    if (obj.enAlmacen > 0)
                    {
                        lista.Add(obj);
                    }
                }
                else
                {
                    StatusInv obj = new StatusInv();
                    obj.Nombre = b.Nombre;
                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == b.ArticuloClave
                                    && a.EntidadClave == entidad
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);


                    int? stand_by = (from a in dc.Inventario
                                     where a.ArticuloClave == b.ArticuloClave
                                     && a.EntidadClave == entidad
                                     && a.EstatusInv == 7
                                     && a.Estatus.HasValue
                                     && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                     select a).Sum(a => a.Standby);


                    int? recuperados = (from a in dc.Inventario
                                        where a.ArticuloClave == b.ArticuloClave
                                        && a.EntidadClave == entidad
                                        && a.EstatusInv == 9
                                        && a.Estatus.HasValue
                                        && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                        select a).Sum(a => a.Cantidad);

                    int? baja = (from a in dc.Inventario
                                 where a.ArticuloClave == b.ArticuloClave
                                 && a.EntidadClave == entidad
                                 && a.EstatusInv == 11
                                 && a.Estatus.HasValue
                                 && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                 select a).Sum(a => a.Cantidad);

                    int? danados = (from a in dc.Inventario
                                    where a.ArticuloClave == b.ArticuloClave
                                    && a.EntidadClave == entidad
                                    && a.EstatusInv == 10
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);

                    int? Reparacion = (from a in dc.Inventario
                                       where a.ArticuloClave == b.ArticuloClave
                                       && a.EntidadClave == entidad
                                       && a.EstatusInv == 13
                                       && a.Estatus.HasValue
                                       && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                       select a).Sum(a => a.Cantidad);

                    int? baja_garantia = (from a in dc.Inventario
                                          where a.ArticuloClave == b.ArticuloClave
                                          && a.EntidadClave == entidad
                                          && a.EstatusInv == 14
                                          && a.Estatus.HasValue
                                          && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                          select a).Sum(a => a.Cantidad);


                    int? devolucion = (from a in dc.Inventario
                                       where a.ArticuloClave == b.ArticuloClave
                                       && a.EntidadClave == entidad
                                       && a.Estatus.Value == 3
                                       select a).Sum(a => a.Standby);


                    int Enpedidos = cuantosPedidos(b.ArticuloClave, entidad);

                    obj.enpedidos = Enpedidos;

                    int cuantos_activos = 0;
                    try
                    {
                        cuantos_activos = activos.Value;
                    }
                    catch
                    {
                    }

                    obj.enAlmacen = cuantos_activos;

                    int cuantos_devolucion = 0;
                    try
                    {
                        cuantos_devolucion = devolucion.Value;
                    }
                    catch
                    {
                    }
                    obj.devolucion = cuantos_devolucion;

                    int cuantos_stanby = 0;
                    try
                    {
                        cuantos_stanby = stand_by.Value;
                    }
                    catch
                    {
                    }
                    obj.standby = cuantos_stanby;

                    int cuantos_recuperados = 0;
                    try
                    {
                        cuantos_recuperados = recuperados.Value;
                    }
                    catch
                    {
                    }
                    obj.revision = cuantos_recuperados;


                    int cuantos_baja = 0;
                    try
                    {
                        cuantos_baja = baja.Value;
                    }
                    catch
                    {
                    }

                    obj.baja = cuantos_baja;

                    int cuantos_reparacion = 0;
                    try
                    {
                        cuantos_reparacion = Reparacion.Value;
                    }
                    catch
                    {
                    }
                    obj.reparacion = cuantos_reparacion;

                    int cuantos_dan = 0;

                    try
                    {
                        cuantos_dan = danados.Value;
                    }
                    catch
                    {
                    }
                    obj.dañados = cuantos_dan;

                    int c_baja_gar = 0;
                    try
                    {
                        c_baja_gar = baja_garantia.Value;
                    }
                    catch
                    { }

                    obj.bajagarantia = c_baja_gar;
                    if (obj.enAlmacen > 0)
                    {
                        lista.Add(obj);
                    }
                }            

            }

            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        public int cuantosPedidos(Guid idArticulo, Guid entidad)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            int en_pedido = 0;
            int? cuantos_surtidos = 0;
            int? cantidadpedida = 0;

            cantidadpedida = (from a in dc.Proceso
                            join b in dc.ProcesoArticulo on a.ProcesoClave equals b.ProcesoClave
                            where  a.catTipoProcesoClave==1 &&
                            (a.catEstatusClave == 3 || a.catEstatusClave == 2 || a.catEstatusClave == 1)
                            && a.EntidadClaveSolicitante == entidad
                          && b.ArticuloClave == idArticulo
                            select b
                          ).Sum(a=>a.Cantidad);

            cuantos_surtidos = (from a in dc.Proceso
                         join b in dc.ProcesoArticulo on a.ProcesoClave equals b.ProcesoClave
                         where a.catTipoProcesoClave == 1 &&
                        ( a.catEstatusClave == 3 || a.catEstatusClave == 2 || a.catEstatusClave == 1)
                         && a.EntidadClaveSolicitante == entidad
                       && b.ArticuloClave == idArticulo
                         select b
                          ).Sum(a => a.CantidadEntregada);
            int pedidos = 0;
            int surtidos = 0;
            try
            {
                pedidos= cantidadpedida.Value;
            }
            catch { }
            try
            {
                surtidos = cuantos_surtidos.Value;
            }catch
            {

            }
            en_pedido= pedidos- surtidos;

            return en_pedido;

        }




    }
}
