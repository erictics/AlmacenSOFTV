﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Utils;
using web.Wrappers;
using System.Globalization;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using web.Models;

namespace Web.Controllers
{
    public class MovimientoTecnicosController : Controller
    {
        // GET /MovimientoTecnicos/
        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }

            List<catClasificacionArticulo> cla = dc.catClasificacionArticulo.OrderBy(x => x.Descripcion).ToList();
            ViewData["distribuidor"] = distribuidor;
            ViewData["clasificaciones"] = cla;
            return View();
        }


        public ActionResult AlmacenesDistribuidor(Guid IdDistribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid ap = Guid.Parse("00000000-0000-0000-0000-000000000001");
            if (IdDistribuidor == ap)
            {
                var lista = (from a in dc.Entidad where a.EntidadClave == ap select new { a.EntidadClave, a.Nombre }).ToList();
                return Json(lista.OrderBy(x => x.Nombre).ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lista = (from a in dc.Entidad where a.catDistribuidorClave == IdDistribuidor && a.catEntidadTipoClave == 1 select new { a.EntidadClave, a.Nombre }).ToList();
                return Json(lista.OrderBy(x => x.Nombre).ToList(), JsonRequestBehavior.AllowGet);
            }



        }

        public ActionResult TecnicosAlmacen(string idalmacen, Guid distribuidor)
        {
            Guid almacen;
            try
            {
                almacen = Guid.Parse(idalmacen);

            }
            catch
            {
                almacen = Guid.Empty;
            }

            ModeloAlmacen dc = new ModeloAlmacen();
            var tecnicos = (from a in dc.Entidad
                            join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveTecnico
                            where a.catEntidadTipoClave == 2
&& (almacen == Guid.Empty || (almacen == b.EntidadClaveAlmacen))
&& a.catDistribuidorClave == distribuidor
                            select new { a.EntidadClave, a.Nombre }).ToList();
            var tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == distribuidor
                          select new { a.EntidadClave, a.Nombre }).ToList();
            tecnicos.AddRange(tecgen);

            return Json(tecnicos.OrderBy(p => p.Nombre).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult TiposArticulos(int idclas)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var tipos = dc.catTipoArticulo.Where(x => x.catClasificacionArticuloClave == idclas).Select(o => new { o.catTipoArticuloClave, o.Descripcion }).ToList();
            return Json(tipos.OrderBy(p => p.Descripcion).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Articulos(int idtip)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var articulos = dc.Articulo.Where(x => x.catTipoArticuloClave == idtip).Select(s => new { s.ArticuloClave, s.Nombre }).ToList();
            return Json(articulos.OrderBy(p => p.Nombre).ToList(), JsonRequestBehavior.AllowGet);
        }

        public class Movimientos
        {
            public DateTime fecha { get; set; }
            public String Movimiento { get; set; }

            public String usuario { get; set; }
            public string Folio { get; set; }
            public string Articulo { get; set; }
            public int TipoMovimiento { get; set; }
            public Guid ArticuloClave { get; set; }
            public int cantidad { get; set; }
            public string Unidad { get; set; }

        }




        [SessionExpireFilterAttribute]
        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();



            Guid distribuidor = Guid.Empty;
            try
            {
                distribuidor = Guid.Parse(Request["distribuidor"]);
            }
            catch
            {
                distribuidor = Guid.Empty;
            }

            Guid almacenes = Guid.Empty;
            try
            {
                almacenes = Guid.Parse(Request["almacen"]);
            }
            catch
            {
                almacenes = Guid.Empty;
            }
            Guid tecnicos = Guid.Empty;
            try
            {
                tecnicos = Guid.Parse(Request["tecnico"]);
            }
            catch
            {
                tecnicos = Guid.Empty;
            }
            int clasificaciones = 0;
            try
            {
                clasificaciones = Convert.ToInt32(Request["clasificacion"]);
            }
            catch
            {
                clasificaciones = 0;
            }
            int tipos = 0;
            try
            {
                tipos = Convert.ToInt32(Request["tipo"]);
            }
            catch
            {
                tipos = 0;
            }
            Guid articulos = Guid.Empty;
            try
            {
                articulos = Guid.Parse(Request["Articulo"]);
            }
            catch
            {
                articulos = Guid.Empty;
            }


            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            DateTime fi = new DateTime(Convert.ToInt32(fechaInicio.Split('/')[2]), Convert.ToInt32(fechaInicio.Split('/')[1]), Convert.ToInt32(fechaInicio.Split('/')[0]));
            DateTime ffn = new DateTime(Convert.ToInt32(fechaFin.Split('/')[2]), Convert.ToInt32(fechaFin.Split('/')[1]), Convert.ToInt32(fechaFin.Split('/')[0]));
            ffn = ffn.AddDays(1);

            int pdf = Convert.ToInt32(2);

            StringBuilder sb = new StringBuilder();

            String empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"">");

            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h4><b>MOVIMIENTOS DE TÉCNICOS</b></h4>");
            sb.Append("<b>Periodo del:</b> " + fechaInicio + "<b> al: </b> " + fechaFin +"<br>");
            if (articulos == Guid.Empty)
            {
                sb.Append("<b>Artículos:</b>Todos los artículos <br>");
            }
            else
            {
                Articulo art = dc.Articulo.Where(o => o.ArticuloClave == articulos).First();
                sb.Append("<b>Artículos:</b>" + art.Nombre + " <br>");
            }
            sb.Append("<b>Fecha: " + DateTime.Now.ToShortDateString() + "</b><br>");

            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 heigth=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<br>");

            List<Entidad> lista_tecnicos = (from a in dc.Entidad
                                            join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveTecnico
                                            where (distribuidor == Guid.Empty || (distribuidor == a.catDistribuidorClave))
                                            && (almacenes == Guid.Empty || (almacenes == b.EntidadClaveAlmacen))
                                            && (tecnicos == Guid.Empty || (tecnicos == a.EntidadClave)) && a.catEntidadTipoClave == 2
                                            select a).ToList();


            var tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == distribuidor
                          && (tecnicos == Guid.Empty || (tecnicos == a.EntidadClave))
                          select a).ToList();

            lista_tecnicos.AddRange(tecgen);
            List<Guid> tecnicos_group = lista_tecnicos.GroupBy(x => x.EntidadClave).Select(o => o.Key).ToList();

            foreach (var a in tecnicos_group)
            {
                Entidad tec = dc.Entidad.Where(x => x.EntidadClave == a).First();
                List<Articulo> articuloslista = (from aa in dc.Articulo where articulos == Guid.Empty || articulos == aa.ArticuloClave orderby aa.Nombre select aa).ToList();
                                                
                                             dc.Articulo.OrderBy(x => x.Nombre).ToList();
                sb.Append("<b> Movimentos del técnico: </b>" + tec.Nombre);
                sb.Append("<br>");
                sb.Append("_____________________________________");
                sb.Append("<br>");
                articuloslista.ForEach(art =>
                {
                    

                   
                    // sb.Append(@"</thead>");
                    List<Movimientos> movimientos = new List<Movimientos>();
                    List<Movimientos> salidas = (from w in dc.Proceso
                                                 join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                 join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                 where w.catTipoProcesoClave == 3
                                                 && w.EntidadClaveSolicitante == a
                                                 && w.Fecha >= fi && w.Fecha <= ffn 
                                                 && e.ArticuloClave== art.ArticuloClave
                                                 orderby w.NumeroPedido
                                                 select new Movimientos
                                                 {
                                                     fecha = w.Fecha.Value,
                                                     cantidad = e.Cantidad.Value,
                                                     Folio = w.NumeroPedido.ToString(),
                                                     Movimiento = w.catTipoProceso.Nombre,
                                                     usuario = d.Nombre,
                                                     Articulo = e.Articulo.Nombre,
                                                     Unidad = e.Articulo.catUnidad.Nombre,
                                                     ArticuloClave = e.ArticuloClave,
                                                     TipoMovimiento = 3
                                                 }).ToList();

                    List<Movimientos> devoluciones = (from w in dc.Proceso
                                                      join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                      join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                      where w.catTipoProcesoClave == 12
                                                      && w.EntidadClaveAfectada == a &&
                                                       w.Fecha >= fi && w.Fecha <= ffn 
                                                       && e.ArticuloClave == art.ArticuloClave
                                                      orderby w.NumeroPedido
                                                      select new Movimientos
                                                      {
                                                          fecha = w.Fecha.Value,
                                                          cantidad = e.Cantidad.Value,
                                                          Folio = w.NumeroPedido.ToString(),
                                                          Movimiento = w.catTipoProceso.Nombre,
                                                          usuario = d.Nombre,
                                                          Articulo = e.Articulo.Nombre,
                                                          ArticuloClave = e.ArticuloClave,
                                                          Unidad = e.Articulo.catUnidad.Nombre,
                                                          TipoMovimiento = 12
                                                      }).ToList();

                    List<Movimientos> bitacoras = (from s in dc.BitacoraSofTV
                                                   join b in dc.BitacoraSofTVDetalle on s.BitacoraSofTVClave equals b.BitacoraSofTVClave
                                                   join c in dc.Inventario on b.InventarioClave equals c.InventarioClave
                                                   where s.EntidadClaveTecnico == a
                                                    && s.Fecha >= fi && s.Fecha <= ffn 
                                                   && c.ArticuloClave == art.ArticuloClave
                                                   select new Movimientos {
                                                       Articulo = c.Articulo.Nombre,
                                                       cantidad = b.Cantidad.Value,
                                                       fecha = b.fecha.Value,
                                                       Folio = s.NumeroOrden + "|" + s.Contrato,
                                                       Movimiento = "Bitácora SAC",
                                                       usuario = b.usuariosoft,
                                                       ArticuloClave = c.ArticuloClave.Value,
                                                       Unidad = c.Articulo.catUnidad.Nombre,
                                                       TipoMovimiento = 10 }).ToList();
                    movimientos.AddRange(salidas);
                    movimientos.AddRange(devoluciones);
                    movimientos.AddRange(bitacoras);

                    if(movimientos.Count > 0)
                    {
                        sb.Append("<br>");
                        sb.Append(@"<b  style=""font-size:8px;""> Clasificación de Artículo: " + art.catTipoArticulo.catClasificacionArticulo.Descripcion + "</b><br>");
                        sb.Append(@"<b  style=""font-size:8px;""> Tipo de Artículo: " + art.catTipoArticulo.Descripcion + "</b><br>");
                        sb.Append(@"<b  style=""font-size:8px;""> Nombre de Artículo: " + art.Nombre + " </b><br><br>");

                        sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                        //sb.Append(@"<thead>");
                        sb.Append("<tr>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Fecha</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Movimiento</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Folio</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Cantidad</td>");
                        sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Unidad</td>");
                        sb.Append("</tr>");

                        sb.Append("<tbody>");
                        movimientos.OrderBy(o => o.fecha).ThenBy(s => s.fecha).ToList().ForEach(x =>
                        {
                            sb.Append("<tr>");
                            sb.Append(@"<td  style=""font-size:8px;"">" + string.Format("{0:dd-MM-yyyy}", x.fecha) + "</td>");
                            sb.Append(@"<td  style=""font-size:8px;"">" + x.Movimiento + "</td>");
                            sb.Append(@"<td  style=""font-size:8px;"">" + x.Folio + "</td>");
                            //sb.Append(@"<td align=""right"" style=""font-size:6px;"">" + x.usuario + "</td>");
                            //sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + x.Articulo + "</td>");
                            sb.Append(@"<td  style=""font-size:8px;"">" + x.cantidad + "</td>");
                            sb.Append(@"<td  style=""font-size:8px;"">" + x.Unidad + "</td>");
                            sb.Append("</tr>");

                        });
                        sb.Append("</tbody>");
                        sb.Append("</table>");
                    }


                   

                });
              
            }
            
            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "movimientostecnico.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);


                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "MovimientosTecnico.pdf");
                }
                else
                {


                    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "movimientostecnico.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
                    return View();

                }
            }

        }




        public ActionResult Excel()
        {

            ModeloAlmacen dc = new ModeloAlmacen();



            Guid distribuidor = Guid.Empty;
            try
            {
                distribuidor = Guid.Parse(Request["distribuidor"]);
            }
            catch
            {
                distribuidor = Guid.Empty;
            }

            Guid almacenes = Guid.Empty;
            try
            {
                almacenes = Guid.Parse(Request["almacen"]);
            }
            catch
            {
                almacenes = Guid.Empty;
            }
            Guid tecnicos = Guid.Empty;
            try
            {
                tecnicos = Guid.Parse(Request["tecnico"]);
            }
            catch
            {
                tecnicos = Guid.Empty;
            }
            int clasificaciones = 0;
            try
            {
                clasificaciones = Convert.ToInt32(Request["clasificacion"]);
            }
            catch
            {
                clasificaciones = 0;
            }
            int tipos = 0;
            try
            {
                tipos = Convert.ToInt32(Request["tipo"]);
            }
            catch
            {
                tipos = 0;
            }
            Guid articulos = Guid.Empty;
            try
            {
                articulos = Guid.Parse(Request["Articulo"]);
            }
            catch
            {
                articulos = Guid.Empty;
            }


            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaFin = Convert.ToString(Request["fechaFin"]);



            DateTime fi = new DateTime(Convert.ToInt32(fechaInicio.Split('/')[2]), Convert.ToInt32(fechaInicio.Split('/')[1]), Convert.ToInt32(fechaInicio.Split('/')[0]));
            DateTime ffn = new DateTime(Convert.ToInt32(fechaFin.Split('/')[2]), Convert.ToInt32(fechaFin.Split('/')[1]), Convert.ToInt32(fechaFin.Split('/')[0]));
            ffn = ffn.AddDays(1);
            String empresa = Config.Dame("EMPRESA");



            List<Entidad> lista_tecnicos = (from a in dc.Entidad join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveTecnico where (distribuidor == Guid.Empty || (distribuidor == a.catDistribuidorClave)) && (almacenes == Guid.Empty || (almacenes == b.EntidadClaveAlmacen)) && (tecnicos == Guid.Empty || (tecnicos == a.EntidadClave)) && a.catEntidadTipoClave == 2 select a).ToList();
            List<Guid> tecnicos_group = lista_tecnicos.GroupBy(x => x.EntidadClave).Select(o => o.Key).ToList();
            DumpExcel(tecnicos_group, fi, ffn, articulos);


            return null;


        }
        private void DumpExcel(List<Guid> lista, DateTime fi, DateTime ffn, Guid articulos)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            using (ExcelPackage pck = new ExcelPackage())
            {
                foreach (var a in lista)
                {
                    Entidad tec = dc.Entidad.Where(x => x.EntidadClave == a).First();
                    List<Movimientos> movimientos = new List<Movimientos>();

                    List<Movimientos> salidas = (from w in dc.Proceso
                                                 join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                 join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                 where w.catTipoProcesoClave == 3
                                                 && w.EntidadClaveSolicitante == a
                                                 && w.Fecha >= fi && w.Fecha <= ffn &&
                                                 (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                 orderby w.NumeroPedido
                                                 select new Movimientos { fecha = w.Fecha.Value, cantidad = e.Cantidad.Value, Folio = w.NumeroPedido.ToString(), Movimiento = w.catTipoProceso.Nombre, usuario = d.Nombre, Articulo = e.Articulo.Nombre, ArticuloClave = e.ArticuloClave, TipoMovimiento = 3 }
                                  ).ToList();

                    List<Movimientos> devoluciones = (from w in dc.Proceso
                                                      join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave

                                                      join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                      where w.catTipoProcesoClave == 12
                                                      && w.EntidadClaveAfectada == a &&
                                                       w.Fecha >= fi && w.Fecha <= ffn &&
                                                      (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                      orderby w.NumeroPedido
                                                      select new Movimientos { fecha = w.Fecha.Value, cantidad = e.Cantidad.Value, Folio = w.NumeroPedido.ToString(), Movimiento = w.catTipoProceso.Nombre, usuario = d.Nombre, Articulo = e.Articulo.Nombre, ArticuloClave = e.ArticuloClave, TipoMovimiento = 12 }
                                  ).ToList();

                    List<Movimientos> bitacoras = (from s in dc.BitacoraSofTV
                                                   join b in dc.BitacoraSofTVDetalle on s.BitacoraSofTVClave equals b.BitacoraSofTVClave
                                                   join c in dc.Inventario on b.InventarioClave equals c.InventarioClave
                                                   where s.EntidadClaveTecnico == a
                                                    && s.Fecha >= fi && s.Fecha <= ffn &&
                                                   (articulos == Guid.Empty || (articulos == c.ArticuloClave))
                                                   select new Movimientos { Articulo = c.Articulo.Nombre, cantidad = b.Cantidad.Value, fecha = b.fecha.Value, Folio = s.NumeroOrden + "|" + s.Contrato, Movimiento = "Bitácora SAC", usuario = b.usuariosoft, ArticuloClave = c.ArticuloClave.Value, TipoMovimiento = 10 }).ToList();


                    movimientos.AddRange(bitacoras);
                    movimientos.AddRange(devoluciones);
                    movimientos.AddRange(salidas);


                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Movimientos tecnico " + tec.Nombre);
                    ws.Cells["A2"].LoadFromCollection(movimientos.Select(x => new { FECHA = x.fecha.ToString(), x.Movimiento, x.Folio, x.usuario, x.Articulo, x.cantidad }).ToList(), true);

                    //estilo a datos de plantilla
                    using (ExcelRange rng = ws.Cells["A1:F1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);


                    }
                    using (ExcelRange rng = ws.Cells["A2:F2"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);


                    }




                    ws.Column(1).Width = 15;
                    ws.Column(2).Width = 30;
                    ws.Column(3).Width = 15;
                    ws.Column(4).Width = 15;
                    ws.Column(5).Width = 15;
                    ws.Column(6).Width = 15;

                    string empresa = Config.Dame("EMPRESA");
                    //datos de plantilla
                    ws.Cells["A1"].Value = empresa;
                    ws.Cells["B1"].Value = "MOVIMIENTOS DE TECNICO: " + tec.Nombre;
                    ws.Cells["C1"].Value = "PERIDO DEL ";
                    ws.Cells["D1"].Value = fi.ToString();
                    ws.Cells["E1"].Value = "AL ";
                    ws.Cells["F1"].Value = ffn.ToString();

                    //ws.Cells["A3"].Value = "CATEGORIA";
                    //ws.Cells["B3"].Value = categoria;
                    //ws.Cells["C3"].Value = "TIPO DE ARTICULO";
                    //ws.Cells["D3"].Value = tipo;
                    //ws.Cells["E3"].Value = "ARTICULO";
                    //ws.Cells["F3"].Value = articulo;






                    //Example how to Format Column 1 as numeric 
                    using (ExcelRange col = ws.Cells[5, 1, 5 + movimientos.Count, 1])
                    {
                        //col.Style.Numberformat.Format = "#,##0.00";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    }


                }



                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Movimientos de tecnico.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();



            }
        }





    }
}