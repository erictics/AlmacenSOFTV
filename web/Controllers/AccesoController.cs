﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using Web.Controllers;

namespace web.Controllers
{
    public class AccesoController : Controller
    {
        //
        // GET: /Acceso/

        [SessionExpireFilterAttribute]
        public ActionResult Dashboard()
        {
            List<ArticulosAgotados> lista = new List<ArticulosAgotados>();
            int pedidos_pendientes = 0;
            int pedidos_por_pagar = 0;
            int dev_por_recibir = 0;
            int dev_mat_por_recibir = 0;
            Usuario u = (Usuario)Session["u"];
            ModeloAlmacen dc = new ModeloAlmacen();
            List<PedidosHelper> listapedidos = new List<PedidosHelper>();
            List<PedidosHelper> listarecepciones = new List<PedidosHelper>();
            if (u.DistribuidorClave == null)
            {
                Guid alm = Guid.Parse("00000000-0000-0000-0000-000000000001");
                 pedidos_pendientes= dc.Proceso.Where(o => (o.catEstatusClave == 1 || o.catEstatusClave == 2) && o.catTipoProcesoClave == 1 && o.EntidadClaveAfectada == alm).ToList().Count;
                 pedidos_por_pagar = dc.Proceso.Where(o => (o.catEstatusClave == 12) && o.catTipoProcesoClave == 1 && o.EntidadClaveAfectada == alm).ToList().Count;
                 dev_por_recibir= dc.Proceso.Where(o => o.catEstatusClave == 1 && o.catTipoProcesoClave == 5 && o.EntidadClaveAfectada == alm).ToList().Count;
                 dev_mat_por_recibir = dc.Proceso.Where(o => o.catEstatusClave == 1 && o.catTipoProcesoClave == 13 && o.EntidadClaveSolicitante == alm).ToList().Count;

                
                List<Articulo> articulos = dc.Articulo.ToList();
                foreach (var s in articulos)
                {
                    int? activos = (from a in dc.Inventario
                                    where a.ArticuloClave == s.ArticuloClave
                                    && a.EntidadClave == alm
                                    && a.EstatusInv == 7
                                    && a.Estatus.HasValue
                                    && (a.Estatus.Value == 1 || a.Estatus.Value == 2)
                                    select a).Sum(a => a.Cantidad);

                    int cuantos_activos = 0;
                    try
                    {
                        cuantos_activos = activos.Value;
                    }
                    catch
                    {
                    }

                    int minimo = dc.Articulo.Where(x => x.ArticuloClave == s.ArticuloClave).Select(x => x.TopeMinimo.Value).First();
                    if (cuantos_activos >= minimo)
                    {
                    }
                    else
                    {
                        ArticulosAgotados aa = new ArticulosAgotados();
                        aa.cantidad = cuantos_activos;
                        aa.Nombre = s.Nombre;
                        aa.IdArticulo = s.ArticuloClave;
                        aa.TopeMinimo = s.TopeMinimo.Value;
                        aa.Clasificacion = s.catTipoArticulo.Descripcion;
                        lista.Add(aa);
                    }

                }


                List<Proceso> recepciones = (from a in dc.Proceso where a.catTipoProcesoClave == 2 && a.EntidadClaveSolicitante == alm && a.catEstatusClave != 6 select a).ToList();

                List<PedidosHelper> p = new List<PedidosHelper>();
                foreach (var a in recepciones)
                {

                    foreach (var b in a.ProcesoArticulo)
                    {
                        PedidosHelper l = new PedidosHelper();
                        l.ArticuloClave = b.ArticuloClave;
                        l.cantidad = b.CantidadEntregada.Value;
                        l.NombreArticulo = b.Articulo.Nombre;
                        p.Add(l);

                    }
                }
                 listarecepciones = (from x in p
                                                        group x by new { x.NombreArticulo }
                                                            into g
                                                            select new PedidosHelper { ArticuloClave = g.Select(n => n.ArticuloClave).FirstOrDefault(), NombreArticulo = g.Key.NombreArticulo, cantidad = g.Sum(o => o.cantidad) }).ToList();



                List<Proceso> pedidos = (from a in dc.Proceso where a.catTipoProcesoClave == 1 && a.EntidadClaveSolicitante == alm && a.catEstatusClave != 6 select a).ToList();

                List<PedidosHelper> ps = new List<PedidosHelper>();
                foreach (var a in pedidos)
                {

                    foreach (var b in a.ProcesoArticulo)
                    {
                        PedidosHelper l = new PedidosHelper();
                        l.ArticuloClave = b.ArticuloClave;
                        l.cantidad = b.Cantidad.Value;
                        l.NombreArticulo = b.Articulo.Nombre;
                        ps.Add(l);

                    }
                }
                listapedidos = (from x in ps
                                                    group x by new { x.NombreArticulo }
                                                        into g
                                                        select new PedidosHelper { ArticuloClave = g.Select(n => n.ArticuloClave).FirstOrDefault(), NombreArticulo = g.Key.NombreArticulo, cantidad = g.Sum(o => o.cantidad) }).ToList();
               


            }

            


            ViewData["Agotados"] = lista;
            ViewData["pedidos_pendientes"] = pedidos_pendientes;
            ViewData["pedidos_por_pagar"] = pedidos_por_pagar;
            ViewData["dev_por_recibir"] = dev_por_recibir;
            ViewData["dev_mat_por_recibir"] = dev_mat_por_recibir;
            ViewData["recepcionesTotales"] = listarecepciones;
            ViewData["pedidosTotales"] = listapedidos;





            return View();
        }


        public class PedidosHelper
        {
            public Guid ArticuloClave { get; set; }
            public int cantidad { get; set; }

            public string NombreArticulo { get; set; }
        }

        public class ArticulosAgotados
        {
            public Guid IdArticulo { get; set; }

            public string Nombre { get; set; }

            public int cantidad { get; set; }

            public int TopeMinimo { get; set; }

            public string Clasificacion { get; set; }
        }

        public ActionResult Login()
        {

            return View();
        }


        public ActionResult CerrarSesion()
        {
            Session.Abandon();
            return Redirect("/");
        }

        public ActionResult CambiaPassword(string email)
        {
            //JObject jObject = (JObject)JsonConvert.DeserializeObject(data);


            Usuario u = (Usuario)Session["u"];

            ModeloAlmacen dc = new ModeloAlmacen();

            Usuario up = (from us in dc.Usuario
                              where us.UsuarioClave == u.UsuarioClave
                             select us).SingleOrDefault();

            if (up != null)
            {


                if (up.Password == Request["actual"].ToString())
                {
                    up.Password = Request["nueva"].ToString();

                    dc.SaveChanges();

                    return Content("1");
                }
                else
                {
                    return Content("0");
                }



            }
            else
            {
                return Content("0");
            }


        }
       

        private void EnviaContraseña(Usuario usuario)
        {
            try
            {
                string smtpserver = Config.Dame("SMTP_SERVIDOR");
                string smtppuerto = Config.Dame("SMTP_PUERTO");
                string smtpremitente = Config.Dame("SMTP_REMITENTE");
                string subject = "Solicitud de contraseña para Almacén";

                string smtpusuario = Config.Dame("SMTP_USUARIO");
                string smtpcontraseña = Config.Dame("SMTP_CONTRASEÑA");

                string smtpssl = Config.Dame("SMTP_SSL");

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = Convert.ToInt32(smtppuerto);
                client.EnableSsl = Convert.ToBoolean(smtpssl);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(smtpusuario, smtpcontraseña);
                client.Host = smtpserver;
                mail.To.Add(new MailAddress(usuario.CorreoElectronico));
                mail.From = new MailAddress(smtpremitente);
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = @"
                <h2>Solicitud de contraseña para Módulo INFONAVIT</h2>
                Los datos para acceder a tu cuenta en Módulo INFONAVIT son:<br><br>
                Usuario: <b>" + usuario.CorreoElectronico + @"</b><br>
                Contraseña: <b>" + usuario.Password + @"</b><br><br>
                Si tu no solicitaste la contraseña haz caso omiso a este correo.
                ";
                client.Send(mail);
            }
            catch (Exception ex)
            {
            }
        }



        public ActionResult Autenticacion(string usuario, string password)
        {
            //JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            string email = usuario;
            ModeloAlmacen dc = new ModeloAlmacen();        
            Usuario u = (from us in dc.Usuario
                             where us.CorreoElectronico == email
                                    && us.Password == password 
                             select us).SingleOrDefault();       
                if (u != null)
                {
                    if (u.Activo == false)
                    {
                        return Content("-1");
                    }
                    else
                    {
                        Session["u"] = u;
                        Session["r"] = u.Rol;
                        Session["m"] = Menu(u, u.Rol.Permiso.ToList());

                        return Content("1");
                    }                   
                }
                else
                {
                    return Content("0");
                }     
        }

        String yasta = String.Empty;
        List<Modulo> modulos = null;
        public string Menu(Usuario u, List<Permiso> permisos)
        {
            StringBuilder sb = new StringBuilder();
            ModeloAlmacen dc = new ModeloAlmacen();
            modulos = (from m in dc.Modulo
                       select m).ToList();
            List<Modulo> modulospadre = (from mo in modulos
                                             where mo.ModuloPadreClave == null
                                             && mo.Activo
                                             select mo).ToList();

            foreach (Modulo m in modulospadre)
            {
                foreach (Permiso p in permisos)
                {
                    if ((p.Modulo.ModuloPadreClave == m.ModuloClave && p.Consulta.Value))
                    {

                        if (yasta.IndexOf(p.Modulo.ModuloPadreClave.ToString()) == -1)
                        {
                            sb.Append(Opcion(m, permisos));
                        }
                        yasta += "|" + p.Modulo.ModuloPadreClave + "|";
                    }
                }
            }
            return sb.ToString();
        }


        private string Opcion(Modulo m, List<Permiso> p)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<li class='gui-folder'><a><div class='gui-icon'><i class='"+m.Icono+"'></i></div><span class='title'>" + m.Nombre + "</span></a>");          
             List<Modulo> hijos = (from mo in modulos
                                      where mo.ModuloPadreClave == m.ModuloClave
                                      && mo.Activo
                                      select mo).ToList();
            if (hijos.Count > 0)
            {
                sb.Append("<ul>");
                foreach(Modulo mi in hijos)
                {
                    foreach (Permiso pe in p)
                    {
                        if ((pe.ModuloClave == mi.ModuloClave && pe.Consulta.Value))
                        {
                            sb.Append(" <li><a href='" + mi.URL + "'><span class='title'>"+ mi.Nombre +"</span></a></li>");
                            //sb.Append("<li><a href='" + mi.URL + "'><i class='" + mi.Icono + "' aria-hidden='true'></i> " + mi.Nombre + "</a></li>");
                        }
                    }
                }
                                
                sb.Append("</ul>");

            }


            sb.Append("</li>");

            return sb.ToString();
        }

    }
}
