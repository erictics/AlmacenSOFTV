﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using web.Wrappers;
using System.Text;
using OfficeOpenXml;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

using web.Models;

namespace web.Controllers
{
    public class CargaMasivaController : Controller
    {
        //
        // GET: /CargaMasiva/

        public ActionResult Index()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            
           



            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                                  orderby ta.Descripcion
                                                                  select ta).ToList();

            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();

            List<Articulo> articulos = (from art in dc.Articulo
                                            where art.Activo == true
                                            orderby art.Nombre
                                            select art).ToList();

            List<catProveedor> proveedor=( from pro in dc.catProveedor
                                                   select pro).ToList();
            //ViewData["distribuidor"] = distribuidor;
            ViewData["proveedor"] = proveedor;
            
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["articulos"] = articulos;
            return View();
        }

        public JsonResult GetExcelData()
        {
            ModeloAlmacen dc =new ModeloAlmacen();
            var request = HttpContext.Request;
            Guid articulo = Guid.Parse(request["Articulo"].ToString());
            List<serial> serial = new List<serial>();
            List<serial> serialrep = new List<serial>();
            int cantidad = 0;            
            if (request.Files.Count > 0)
            {
                foreach (string file in request.Files)
                {
                   
                    try
                    {
                        var postedFile = request.Files[file];
                        var package = new ExcelPackage(postedFile.InputStream);
                        var sheet = package.Workbook.Worksheets.FirstOrDefault();
                        var columnimport = sheet.Cells["A1:A"];

                        foreach (var cell in columnimport)
                        {
                            string serie=cell.GetValue<string>();

                            if (dc.Serie.Any(x => x.Valor == serie))
                            {
                                List<serie> s2 = new List<serie>();
                                serie ser = new serie();
                                ser.Clave = 2;
                                ser.Valor = cell.GetValue<string>();
                                s2.Add(ser);

                                serial s = new serial();
                                s.Activo = 1;
                                s.ArticuloClave = articulo;
                                s.EstatusClave = 7;
                                s.EstatusTexto = "Buen Estado";
                                s.datos = s2;
                                serialrep.Add(s);
                                
                            }
                            else
                            {
                                List<serie> s2 = new List<serie>();
                                serie ser = new serie();
                                ser.Clave = 2;
                                ser.Valor = cell.GetValue<string>();
                                s2.Add(ser);

                                serial s = new serial();
                                s.Activo = 1;
                                s.ArticuloClave = articulo;
                                s.EstatusClave = 7;
                                s.EstatusTexto = "Buen Estado";
                                s.datos = s2;
                                


                                serial.Add(s);
                                cantidad = cantidad + 1; 
                            }

                                                   
                        }
                                                
                    }
                    catch
                    {
                        var postedFile = request.Files[file];
                        HSSFWorkbook hssfwb = new HSSFWorkbook(postedFile.InputStream);
                        ISheet sheet = hssfwb.GetSheetAt(0);
                        for (int row = 0; row <= sheet.LastRowNum; row++)
                        {
                            if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
                            {
                                DataFormatter formatter = new DataFormatter();
                                
                                String serie = formatter.FormatCellValue(sheet.GetRow(row).GetCell(0));
                                if (dc.Serie.Any(x => x.Valor == serie))
                                {
                                    List<serie> s2 = new List<serie>();
                                    serie ser = new serie();
                                    ser.Clave = 2;
                                    ser.Valor = serie;
                                    s2.Add(ser);

                                    serial s = new serial();
                                    s.Activo = 1;
                                    s.ArticuloClave = articulo;
                                    s.EstatusClave = 7;
                                    s.EstatusTexto = "Buen Estado";
                                    s.datos = s2;
                                    serialrep.Add(s);
                                    

                                }
                                else
                                {

                                    List<serie> s2 = new List<serie>();
                                    serie ser = new serie();
                                    ser.Clave = 2;
                                    ser.Valor = serie;
                                    s2.Add(ser);

                                    serial s = new serial();
                                    s.Activo = 1;
                                    s.ArticuloClave = articulo;
                                    s.EstatusClave = 7;
                                    s.EstatusTexto = "Buen Estado";
                                    s.datos = s2;
                                    serial.Add(s);
                                    cantidad = cantidad + 1;


                                }



                                
                            }
                        }
                    }
                }
                
            }
            TotalSeries t=new TotalSeries();
            t.series_nuevas = serial;
            t.series_repetidas = serialrep;
            t.Total = cantidad;

                
            return Json(t,JsonRequestBehavior.AllowGet);
        }

        public class TotalSeries
        {
            public int Total { get; set; }
            public List<serial> series_nuevas { get; set; }

            public List<serial> series_repetidas { get; set; }
        }


        public class serial
        {
            public Guid ArticuloClave { get; set; }
            public string EstatusTexto { get; set; }
            public int EstatusClave { get; set; }
            public int Activo { get; set; }
            public List<serie> datos { get; set; }

        }


        public class serie
        {
            public int Clave { get; set; }
            public string Valor { get; set; }
        }



        public String gettipoarticulos(int id ) { 
        StringBuilder sb=new StringBuilder();
        
            ModeloAlmacen dc = new ModeloAlmacen();
            var al = (from a in dc.catTipoArticulo where a.catClasificacionArticuloClave == id && a.catSerieClaveInterface !=null  select new { a.catTipoArticuloClave,a.Descripcion });
            foreach (var d in al ){
                sb.Append("<option value="+d.catTipoArticuloClave+">"+d.Descripcion+"</option>");
            }
        return sb.ToString();
        }


        public String getarticulos(int id)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            StringBuilder sb = new StringBuilder();
            var dat=(from f in dc.Articulo where f.catTipoArticuloClave==id select new {f.ArticuloClave,f.Nombre});
            foreach (var d in dat)
            {
                sb.Append("<option value=" + d.ArticuloClave + ">" + d.Nombre + "</option>");
            }

            return sb.ToString();
        }


       public string validaserie(string valor ){
            ModeloAlmacen dc = new ModeloAlmacen();
         string response=  dc.Serie.Any(x=>x.Valor==valor).ToString();
         return response;

        }


       public string validaseriesurtido(string valor)
       {

           ModeloAlmacen dc = new ModeloAlmacen();          
           string response = dc.Serie.Any(x => x.Valor == valor).ToString();
           return response;

       }






    }
}
