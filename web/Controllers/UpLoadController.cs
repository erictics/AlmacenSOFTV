﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class UpLoadController : Controller
    {
        //
        // GET: /UpLoad/

        public ActionResult Archivo()
        {
            string url = String.Empty;
            String strMensajeError = "{\"success\":true}";
            try
            {
                if (Request.Files.Count > 0)
                {
                    String NombreArchivo = Request.Files[0].FileName.ToLower();
                    String ruta = Server.MapPath("~/Content/media");
                    String nuevoGuid = Guid.NewGuid().ToString();

                    String rutaCompleta = ruta + "\\" + nuevoGuid + NombreArchivo;


                    if (!Directory.Exists(ruta))
                    {
                        Directory.CreateDirectory(ruta);
                    }
                    Request.Files[0].SaveAs(rutaCompleta);


                    url = nuevoGuid + NombreArchivo;

                    strMensajeError = "{\"success\":true, \"laurl\":\"" + url + "\" }";
                }
                else
                {
                    strMensajeError = "{\"error\": \"No se ha indicado ningún archivo\"}";
                }
            }
            catch (Exception ex)
            {
                strMensajeError = "{\"error\": \"Se presentó un error al cargar la plantilla\"}";
            }
            ContentResult resultado = Content(strMensajeError);
            resultado.ContentType = "text/plain";
            return resultado;
        }

        public ActionResult ToPDF()
        {


            return Content("1");
        }

    }
}
