﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

using Web.Controllers;

namespace web.Controllers
{
    public class HabilitacionEquiposController : Controller
    {


        Guid almacen_central = Guid.Parse(ConfigurationManager.AppSettings["almacenprincipal"]);
     
        ModeloAlmacen dc = new ModeloAlmacen();
        //
        // GET: /HabilitacionEquipos/
        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            
            Usuario u = ((Usuario)Session["u"]);


            List<Entidad> almacenes = (from a in dc.Entidad
                                       join b in dc.AmbitoAlmacen on a.EntidadClave equals b.EntidadClaveAlmacen
                                       where a.catEntidadTipoClave==1 && 
                                       b.UsuarioClave==u.UsuarioClave  select a).ToList();
            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();
            List<catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo where ta.Activo orderby ta.Descripcion select ta).ToList();
            //        
            List<catEntidadTipo> tipoalmacen = dc.catEntidadTipo.Where(x => x.catEntidadTipoClave != 2).ToList();
            ViewData["RolUsuario"] = u.RolClave;
            ViewData["tipoalmacen"] = tipoalmacen;
            //
            ViewData["almacenes"] = almacenes;
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tipo_articulos"] = tiposarticulo;
            return View();        
            
        }


        public ActionResult ListaEquipos(string data, string almacen, int orden, int draw, int start, int length)
        {
            Guid alm;
            try
            { alm = Guid.Parse(almacen);}
            catch
            {
                alm = Guid.Empty;
            }
            listaProcesos dataTableData = new listaProcesos();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ObtenerProcesos(ref recordsFiltered, alm, orden,start,length).OrderByDescending(x => x.pedido).ToList();
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Proceso> ObtenerProcesos(ref int recordFiltered, Guid almacen, int orden, int start, int length)
        {
            
            List<Proceso> procesos;
            List<Proceso> aux;
            Usuario u = (Usuario)Session["u"];

            procesos = (from a in dc.Proceso
                   join b in dc.AmbitoAlmacen on a.EntidadClaveSolicitante equals b.EntidadClaveAlmacen
                        where
                        b.UsuarioClave == u.UsuarioClave &&
                        (a.catTipoProcesoClave == 7 || a.catTipoProcesoClave == 8) && (orden == 0 || (a.NumeroPedido == orden))
                   && (almacen == Guid.Empty || (almacen == a.EntidadClaveSolicitante)) orderby a.NumeroPedido ascending 
                        select new Proceso { pedido = a.NumeroPedido, almacen = a.EntidadClaveSolicitante.Value.ToString(), estatus = a.catEstatusClave.Value, fecha = a.Fecha.ToString(), proceso = a.ProcesoClave, tipo=a.catTipoProceso.Nombre }
                 ).OrderByDescending(o => o.pedido).ToList();
            aux = procesos;
            recordFiltered = aux.Count;
            List<Proceso> lista = new List<Proceso>();
            foreach (var a in procesos.Skip(start).Take(length).ToList())
            {
                Proceso i = new Proceso();
                i.proceso = a.proceso;
                i.tipo = a.tipo;
                i.pedido = a.pedido;
                i.fecha = a.fecha;
                i.estatus = a.estatus;
                Guid ar = Guid.Parse(a.almacen);
                Entidad al = dc.Entidad.Where(x => x.EntidadClave == ar).First();
                i.almacen = al.Nombre;            
                lista.Add(i);
            }
            return lista;

        }

        public class listaProcesos
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Proceso> data { get; set; }
        }

        public class Proceso
        {
            public long pedido { get; set; }
            public string fecha { get; set; }
            public string almacen { get; set; }

            public string tipo { get; set; }
            public int estatus { get; set; }
            public Guid proceso { get; set; }           

        }

        

        public bool tieneSeries(Guid artclave)
        {
            
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        
        


        public ActionResult Habilita(Guid almacen, string motivo, string articulos,int status)
        {
            
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Entidad alm = dc.Entidad.Where(x=>x.EntidadClave==almacen).First();                    

                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));

                    Usuario u = (Usuario)Session["u"];
                    web.Models.Proceso p = new web.Models.Proceso();
                    p.ProcesoClave = Guid.NewGuid();
                    p.catEstatusClave = 4;
                    p.catTipoProcesoClave = 7;
                    p.EntidadClaveSolicitante = almacen;
                    p.EntidadClaveAfectada = almacen;
                    p.UsuarioClave = u.UsuarioClave;
                    p.Fecha = DateTime.Now;
                    p.Observaciones = motivo;                    
                    dc.Proceso.Add(p);

                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g
                                     select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach (var a in result)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = p.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                    }

                    foreach (var Articulo in series)
                    {
                        Inventario Inv = new Inventario();
                        try
                        {
                            Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave  && o.EstatusInv == status).First();
                        }
                        catch{

                            return Content("-1");
                        }
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            
                             if (Inv.EntidadClave != alm.EntidadClave)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-2");
                            }
                            else
                            {
                                Inv.EntidadClave = almacen;
                                Inv.EstatusInv = 7;

                               
                            }


                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-3");
                            }
                            else
                            {

                                Inventario InvActivo = new Inventario();
                                try
                                {
                                    InvActivo = dc.Inventario.Where(o =>  o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == almacen && o.EstatusInv == 7).First();
                                }
                                catch {
                                    return Content("-4");
                                }

                                
                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;
                                InvActivo.Cantidad = InvActivo.Cantidad + Articulo.Cantidad;

                                

                            }


                        }

                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                }
                catch {

                    dbContextTransaction.Rollback();
                    return Content("0");
                
                }
            }
        }


        public ActionResult Baja(Guid almacen, string motivo, string articulos,int status)
        {
            
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Entidad alm = dc.Entidad.Where(x => x.EntidadClave == almacen).First();

                    var myArray = new Newtonsoft.Json.Linq.JArray();
                    List<series> series = (List<series>)JsonConvert.DeserializeObject(articulos, typeof(List<series>));

                    Usuario u = (Usuario)Session["u"];
                    web.Models.Proceso p = new web.Models.Proceso();
                    p.ProcesoClave = Guid.NewGuid();
                    p.catEstatusClave = 4;
                    p.catTipoProcesoClave = 8;
                    p.EntidadClaveSolicitante = almacen;
                    p.EntidadClaveAfectada = almacen;
                    p.UsuarioClave = u.UsuarioClave;
                    p.Fecha = DateTime.Now;
                    p.Observaciones = motivo;
                    dc.Proceso.Add(p);

                    var result = from x in series
                                 group x by new { x.ArticuloClave }
                                     into g
                                     select new { articulo = g.Key, Cantidad = g.Sum(o => o.Cantidad) };

                    foreach (var a in result)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = Guid.Parse(a.articulo.ArticuloClave.ToString());
                        pa.Cantidad = a.Cantidad;
                        pa.ProcesoClave = p.ProcesoClave;
                        dc.ProcesoArticulo.Add(pa);
                    }

                    foreach (var Articulo in series)
                    {
                        Inventario Inv = new Inventario();
                        try
                        {
                            Inv = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave 
                            && o.ArticuloClave == Articulo.ArticuloClave
                            && o.EntidadClave == almacen && o.EstatusInv == status).First();
                        }
                        catch {
                            dbContextTransaction.Rollback();
                            return Content("-1");
                        }
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = Articulo.InventarioClave;
                        pi.Cantidad = Articulo.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);

                        if (tieneSeries(Articulo.ArticuloClave))
                        {
                            if (Inv.EntidadClave != alm.EntidadClave)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-2");
                            }
                            else
                            {

                                Inv.EstatusInv = 11;
                                Inv.EntidadClave = almacen;

                            }


                        }
                        else
                        {
                            if (Inv.Cantidad < Articulo.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-3");
                            }
                            else
                            {

                                Inventario InvBaja = new Inventario();
                                try
                                {
                                    InvBaja = dc.Inventario.Where(o => o.InventarioClave == Articulo.InventarioClave && o.ArticuloClave == Articulo.ArticuloClave && o.EntidadClave == almacen && o.EstatusInv == 11).First();
                                }
                                catch {

                                    InvBaja.ArticuloClave = Articulo.ArticuloClave;
                                    InvBaja.Cantidad = 0;
                                    InvBaja.EntidadClave = almacen;
                                    InvBaja.Estatus = 1;
                                    InvBaja.Standby = 0;
                                    InvBaja.EstatusInv = 11;
                                    InvBaja.InventarioClave = Guid.NewGuid();
                                    dc.Inventario.Add(InvBaja);
                                }


                                Inv.Cantidad = Inv.Cantidad - Articulo.Cantidad;
                                InvBaja.Cantidad = InvBaja.Cantidad + Articulo.Cantidad;



                            }


                        }

                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);
                }
                catch
                {

                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }
        }

    }
}
