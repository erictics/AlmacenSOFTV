﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using web.Controllers;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class AlmacenController : Controller
    {
        //
        // GET: /Almacen/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidores = (from d in dc.catDistribuidor
                                                        where d.Activo == true
                                                        orderby d.Nombre
                                                        select d).ToList();
            ViewData["distribuidores"] = distribuidores;

            ViewData["pantalla"] = "almacen";
            List<Entidad> almacenes = (from a in dc.Entidad
                                           where a.catEntidadTipoClave == 2
                                           select a).ToList();
            ViewData["almacenes"] = almacenes;
            
            return View();
        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Almacen> data { get; set; }
        }



        public List<Almacen> Almacenes(int IdDistribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            if (IdDistribuidor > 0)
            {
                List<catDistribuidor> distribuidores = (from d in dc.catDistribuidor
                                                            where d.Activo == true
                                                            orderby d.Nombre
                                                            select d).ToList();

                List<Almacen> almacenes = (from a in dc.Entidad
                                           where a.catEntidadTipoClave == 1
                                           && a.catDistribuidor.IdDistribuidor == IdDistribuidor
                                           && a.Activo == true
                                           select new Almacen { IdAlmacen = a.idEntidadSofTV, Nombre = a.Nombre, Activo = a.Activo.Value }).ToList();
                foreach (var a in almacenes)
                {
                    a.servicios = GetServiciosAlmacen(a.IdAlmacen);
                }
                return almacenes;
            }
            else if (IdDistribuidor == 0)
            {
                List<Almacen> almacenes = dc.Entidad.Where(x => x.IdEntidad == 1).Select(y => new Almacen { IdAlmacen=0,Nombre=y.Nombre }).ToList();
                return almacenes;
            }
            else
            {
                List<catDistribuidor> distribuidores = (from d in dc.catDistribuidor
                                                            where d.Activo == true
                                                            orderby d.Nombre
                                                            select d).ToList();
                int iddis = Int32.Parse(distribuidores[0].IdDistribuidor.ToString());
                List<Almacen> almacenes = (from a in dc.Entidad
                                           where a.catEntidadTipoClave == 1
                                           && a.catDistribuidor.IdDistribuidor == 1
                                           && a.Activo == true
                                           select new Almacen { IdAlmacen = a.idEntidadSofTV, Nombre = a.Nombre, Activo = a.Activo.Value }).ToList();
                foreach (var a in almacenes)
                {
                    a.servicios = GetServiciosAlmacen(a.IdAlmacen);
                }
                return almacenes;
            }
        }



        public ActionResult ObtenAlamcenes(int IdDistribuidor ,int draw, int start, int length){
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = Almacenes(IdDistribuidor).OrderByDescending(x => x.Nombre).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = Almacenes(IdDistribuidor).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        
        public List<Servicio> GetServiciosAlmacen(int IdAlmacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();           
            List<Servicio> ListaServicio = new List<Servicio>();
            try
            {
                ListaServicio = (from a in dc.Tbl_almacen_servicio
                                 join b in dc.CatTipoServicios on a.IdServicio equals b.IdServicio
                                 where b.Activo == true && a.IdAlmacen == IdAlmacen
                                 select new Servicio { idServicio = b.IdServicio, Nombre = b.Descripcion, Activo = b.Activo }).ToList();
               
                return ListaServicio;
            }
            catch
            {               
                return ListaServicio;                
            }
        }



        public ActionResult GetServiciosAlmacenGuid(Guid guidAlmacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            int IdAlmacen = dc.Entidad.Where(x => x.EntidadClave == guidAlmacen).Select(x => x.idEntidadSofTV).FirstOrDefault();            
            List<Servicio> ListaServicio = new List<Servicio>();
            try
            {
                ListaServicio = (from a in dc.Tbl_almacen_servicio
                                 join b in dc.CatTipoServicios
                                     on a.IdServicio equals b.IdServicio
                                 where a.IdAlmacen == IdAlmacen
                                 select new Servicio { idServicio = b.IdServicio, Nombre = b.Descripcion, Activo = b.Activo }).ToList();

                return Json(ListaServicio, JsonRequestBehavior.AllowGet);
            }
            catch
            {      
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult obtenGeneralServ(int idDis)
        {
            ModeloAlmacen dc=new ModeloAlmacen();            
            List<Servicio> serviciosplaza = GetServiciosAlmacen(idDis);
            List<Servicio> servicios = dc.CatTipoServicios.ToList().Select(s => new Servicio { Activo=s.Activo, Nombre=s.Descripcion, idServicio=s.IdServicio }).ToList();

            foreach (var a in servicios)
            {
                if (serviciosplaza.Any(x => x.idServicio == a.idServicio))
                {
                    a.Asignado = true;
                }
                else
                {
                    a.Asignado = false;
                }
            }
            return Json(servicios,JsonRequestBehavior.AllowGet);

        }


     


        public ActionResult GuardaAsignaciones(string servicios, int idAlmacen)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            var myArray = new Newtonsoft.Json.Linq.JArray();
            List<Servicio> lservicios = (List<Servicio>)JsonConvert.DeserializeObject(servicios, typeof(List<Servicio>));
            XElement xmlElements = new XElement("root", lservicios.Select(i => new XElement("Servicios", i.idServicio)));
            try
            {
                dc.Database.ExecuteSqlCommand("exec Asingna_servicio @xml,@idplaza", 
                    new SqlParameter("@xml", xmlElements.ToString()),
                    new SqlParameter("@idplaza", idAlmacen)
                    );
                
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("0", JsonRequestBehavior.AllowGet);             
            }
        }

        public class Almacen
        {
            public int IdAlmacen { get; set; }
            public string Nombre { get; set; }
            public bool Activo { get; set; }
            public Guid entidadClave { get; set; }

            public List<Servicio> servicios { get; set; }
        }
        public ActionResult ObtenPorDistribuidorTecnico(string data)
        {
            AlmacenWrapper rw = new AlmacenWrapper();
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid uid = Guid.Parse("00000000-0000-0000-0000-000000000001");
            try
            {
                uid = Guid.Parse(data.Split('|')[0]);
            }
            catch
            { }
            Guid uidusuario = Guid.Parse("00000000-0000-0000-0000-000000000001");

            try
            {
                uidusuario = Guid.Parse(data.Split('|')[1]);
            }
            catch
            {

            }
            List<Entidad> rl = (from ro in dc.Entidad
                                    where ro.catDistribuidorClave == uid
                                    && ro.catEntidadTipoClave == 1
                                    && ro.Activo.Value
                                    select ro).ToList();

            List<AlmacenWrapper> rwl = new List<AlmacenWrapper>();

            foreach (Entidad r in rl)
            {
                rw = AlmacenToWrapper(r);

                AmbitoAlmacen ambito = (from ro in dc.AmbitoAlmacen
                                            where ro.EntidadClaveTecnico == uidusuario
                                            && ro.EntidadClaveAlmacen == r.EntidadClave
                                            select ro).SingleOrDefault();
                if (ambito != null)
                {
                    rw.Activo = "1";
                }
                else
                {
                    rw.Activo = "0";
                }

                rwl.Add(rw);
            }
            return Json(rwl, JsonRequestBehavior.AllowGet);
        }





        public ActionResult ObtenPorDistribuidor(string data)
        {
            AlmacenWrapper rw = new AlmacenWrapper();
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid uid = Guid.Parse("00000000-0000-0000-0000-000000000001");
            Guid principal = Guid.Parse("00000000-0000-0000-0000-000000000001");
            try
            {
                uid = Guid.Parse(data.Split('|')[0]);
            }
            catch
            {}
            Guid uidusuario = Guid.Empty;
            try
            {
                uidusuario = Guid.Parse(data.Split('|')[1]);
            }
            catch
            {
            }
            List<Entidad> rl = (from ro in dc.Entidad
                                    where
                                   (ro.catDistribuidorClave == uid || (uid == principal && ro.catDistribuidorClave == null))
                                   && ro.catEntidadTipoClave == 1
                                   && ro.Activo.Value == true
                                    select ro).ToList();

            List<AlmacenWrapper> rwl = new List<AlmacenWrapper>();

            foreach (Entidad r in rl)
            {

                if (r.catEntidadTipoClave == 1)
                {
                    rw = AlmacenToWrapper(r);
                    AmbitoAlmacen ambito = (from ro in dc.AmbitoAlmacen
                                                where ro.UsuarioClave == uidusuario
                                                && ro.EntidadClaveAlmacen == r.EntidadClave
                                                select ro).SingleOrDefault();
                    if (ambito != null)
                    {
                        rw.Activo = "1";
                    }
                    else
                    {
                        rw.Activo = "0";
                    }

                    rwl.Add(rw);
                }
            }

            return Json(rwl, JsonRequestBehavior.AllowGet);

        }


        private AlmacenWrapper AlmacenToWrapper(Entidad e)
        {
            AlmacenWrapper aw = new AlmacenWrapper();
            aw.Clave = e.EntidadClave.ToString();
            aw.Nombre = e.Nombre;
            aw.catEntidadTipoClave = e.catEntidadTipoClave.ToString();
            aw.EntidadPadreClave = e.EntidadPadreClave.ToString();
            aw.Activo = Convert.ToInt32(e.Activo).ToString();
            return aw;
        }




        public ActionResult GetAlmacenByDistribuidor(Guid distribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Almacen> lista = dc.Entidad.Where(o => o.catDistribuidorClave == distribuidor && o.catEntidadTipoClave==1).Select(o => new Almacen { IdAlmacen=o.idEntidadSofTV, entidadClave=o.EntidadClave, Nombre =o.Nombre }).ToList();
            return Json(lista,JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetAlmacenByIdDistribuidor(int distribuidor)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            if (distribuidor == 0)
            {                
                List<Almacen> lista = dc.Entidad.Where(o => o.IdEntidad == 1).Select(o => new Almacen { IdAlmacen = o.idEntidadSofTV, Nombre = o.Nombre }).ToList();
                return Json(lista, JsonRequestBehavior.AllowGet);

            }
            else
            {
                List<Almacen> lista = dc.Entidad.Where(o => o.catDistribuidor.IdDistribuidor == distribuidor && o.catEntidadTipoClave == 1).Select(o => new Almacen { IdAlmacen = o.idEntidadSofTV, Nombre = o.Nombre }).ToList();
                return Json(lista, JsonRequestBehavior.AllowGet);
            }          
           

        }


        




    }
}
