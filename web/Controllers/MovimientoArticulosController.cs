﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.Utils;
using web.Wrappers;
using System.Globalization;
using System.Data;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using web.Models;

namespace Web.Controllers
{
    public class MovimientoArticulosController : Controller
    {
        // GET /MovimientoArticulos/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<catDistribuidor> distribuidor = null;

            Usuario u = ((Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                distribuidor = (from c in dc.catDistribuidor
                                where c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }
            else
            {
                distribuidor = (from c in dc.catDistribuidor
                                where
                                c.catDistribuidorClave == u.DistribuidorClave
                                && c.Activo
                                orderby c.Nombre
                                select c).ToList();
            }

            List<catClasificacionArticulo> cla = dc.catClasificacionArticulo.ToList();
            ViewData["distribuidor"] = distribuidor;
            ViewData["clasificaciones"] = cla;
            return View();
        }

        public class Movimientos
        {
            public DateTime fecha { get; set; }
            public String Movimiento { get; set; }

            public String usuario { get; set; }
            public string Folio { get; set; }
            public string Articulo { get; set; }

            public int TipoMovimiento { get; set; }
            public Guid ArticuloClave { get; set; }
            public int cantidad { get; set; }
            public string Unidad { get; set; }
        }

        [SessionExpireFilterAttribute]
        public ActionResult Detalle()
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Guid distribuidor = Guid.Empty;
            try
            {
                distribuidor = Guid.Parse(Request["distribuidor"]);
            }
            catch
            {
                distribuidor = Guid.Empty;
            }

            Guid almacenes = Guid.Empty;
            try
            {
                almacenes = Guid.Parse(Request["almacen"]);
            }
            catch
            {
                almacenes = Guid.Empty;
            }

            int clasificaciones = 0;
            try
            {
                clasificaciones = Convert.ToInt32(Request["clasificacion"]);
            }
            catch
            {
                clasificaciones = 0;
            }
            int tipos = 0;
            try
            {
                tipos = Convert.ToInt32(Request["tipo"]);
            }
            catch
            {
                tipos = 0;
            }
            Guid articulos = Guid.Empty;
            try
            {
                articulos = Guid.Parse(Request["Articulo"]);
            }
            catch
            {
                articulos = Guid.Empty;
            }

            int proceso = 0;
            try
            {
                proceso = Int32.Parse(Request["proceso"]);
            }
            catch
            {
                proceso = 0;
            }


            string fechaInicio = Convert.ToString(Request["fechainicio"]);
            string fechaFin = Convert.ToString(Request["fechaFin"]);
            DateTime fi = new DateTime(Convert.ToInt32(fechaInicio.Split('/')[2]), Convert.ToInt32(fechaInicio.Split('/')[1]), Convert.ToInt32(fechaInicio.Split('/')[0]));
            DateTime ffn = new DateTime(Convert.ToInt32(fechaFin.Split('/')[2]), Convert.ToInt32(fechaFin.Split('/')[1]), Convert.ToInt32(fechaFin.Split('/')[0]));
            ffn = ffn.AddDays(1);
            int pdf = Convert.ToInt32(2);
            StringBuilder sb = new StringBuilder();
            String empresa = Config.Dame("EMPRESA");
            List<Movimientos> movimientos = new List<Movimientos>();
            List<Movimientos> ordered = new List<Movimientos>();
            string logo = Config.Dame("LOGO_ENTRADA");
            sb.Append("<font size=2>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""center"" colspan=2>");
            sb.Append("<h3><b>" + empresa + "</b></h3>");
            sb.Append("<h4><b>RESUMEN DE MOVIMIENTOS DE ARTÍCULOS</b></h4>");
            sb.Append("<b>Periodo del:</b> " + fechaInicio + "<b> al: </b> " + fechaFin+"<br>");
            
            if (articulos == Guid.Empty)
            {
                sb.Append("<b>Artículos:</b>Todos los artículos <br>");
            }
            else
            {
                Articulo art = dc.Articulo.Where(o => o.ArticuloClave == articulos).First();
                sb.Append("<b>Artículos:</b>" + art.Nombre + " <br>");
            }
            sb.Append("<b>Fecha: " + DateTime.Now.ToShortDateString() + "</b><br>");
            sb.Append("</td>");
            //sb.Append(@"<td align=""right"">");
            //sb.Append("<img src='_RUTA_/media/" + logo + "' width=80 heigth=80 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            Entidad alm = dc.Entidad.Where(s => s.EntidadClave == almacenes).First();
            // movimientos.AddRange(MovimientosAp(almacenes, fi, ffn, articulos,proceso,tipos));
            //sb.Append(Contenido(alm.Nombre));

            List<Articulo> lista_articulos = (from h in dc.Articulo where articulos == Guid.Empty || articulos == h.ArticuloClave select h).ToList();
            List<Entidad> lista_almacenes = (from h in dc.Entidad where almacenes == Guid.Empty || almacenes == h.EntidadClave select h).ToList();

            lista_almacenes.ForEach(h =>
            {
                sb.Append(@"<b>Movimientos de Almacén: </b><span>"+h.Nombre+"</span><br>");
                sb.Append(@"__________________________________________________________<br>");

                List<catClasificacionArticulo> l_clasificaciones = (from clas in dc.catClasificacionArticulo where clasificaciones == 0 || clasificaciones == clas.catClasificacionArticuloClave select clas).ToList();

                l_clasificaciones.ForEach(clas => {

                    sb.Append(@"<b>Clasificación : </b><span>" + clas.Descripcion + "</span><br>");
                    

                 

                List<catTipoArticulo>l_tipos = (from tip in dc.catTipoArticulo where tipos == 0 || tipos == tip.catTipoArticuloClave && tip.catClasificacionArticuloClave==clas.catClasificacionArticuloClave select tip).ToList();

                    l_tipos.ForEach(p => {


                        List<Articulo> lista = lista_articulos.Where(s => s.catTipoArticulo.catClasificacionArticuloClave == clas.catClasificacionArticuloClave
                         && s.catTipoArticuloClave == p.catTipoArticuloClave
                        ).ToList();

                        int countarticulos = 0;

                        lista.ForEach(hh => {

                            countarticulos = countarticulos + 1;

                            List<Movimientos> movi = Movimientosalmacen(h.EntidadClave, fi, ffn, hh.ArticuloClave, proceso);
                            if (movi.Count > 0)
                            {
                                if(countarticulos == 1)
                                {
                                    sb.Append(@"<b>Tipo de Artículo : </b><span>" + p.Descripcion + "</span><br>");
                                }



                                



                                sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                                //sb.Append(@"<thead>");
                                if (countarticulos == 1)
                                {
                                    sb.Append("<tr>");
                                    sb.Append(@"<td colspan=3 style=""font-size:8px;text-decoration: underline;"">Nombre de Artículo</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Recepciones</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Salidas</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Devolucion</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Habilit.</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Bajas</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Saldo</td>");
                                    sb.Append(@"<td style=""font-size:8px;text-decoration: underline;"">Unidad</td>");
                                    sb.Append("</tr>");

                                }
                               

                                sb.Append("<tbody>");


                                sb.Append("<tr>");
                                sb.Append(@"<td  colspan=3 style=""font-size:8px;"">" + hh.Nombre + "</td>");

                                int recepciones = 0;
                                recepciones = movi.Where(d => d.TipoMovimiento == 2 || d.TipoMovimiento == 4).Sum(s => s.cantidad);
                                sb.Append(@"<td  style=""font-size:8px;"">" + recepciones + "</td>");

                                int salidas = 0;
                                salidas = movi.Where(d => d.TipoMovimiento == 3).Sum(s => s.cantidad);
                                sb.Append(@"<td  style=""font-size:8px;"">" + salidas + "</td>");

                                int devoluciones = 0;
                                devoluciones = movi.Where(d => d.TipoMovimiento == 12).Sum(s => s.cantidad);
                                sb.Append(@"<td  style=""font-size:8px;"">" + devoluciones + "</td>");

                                int habilitaciones = 0;
                                habilitaciones = movi.Where(d => d.TipoMovimiento == 7).Sum(s => s.cantidad);
                                sb.Append(@"<td  style=""font-size:8px;"">" + habilitaciones + "</td>");

                                int bajas = 0;
                                habilitaciones = movi.Where(d => d.TipoMovimiento == 8).Sum(s => s.cantidad);
                                sb.Append(@"<td  style=""font-size:8px;"">" + bajas + "</td>");

                                int saldo = recepciones - salidas + devoluciones - bajas + habilitaciones;
                                sb.Append(@"<td  style=""font-size:8px;"">" + saldo + "</td>");

                                sb.Append(@"<td  style=""font-size:8px;"">" + hh.catUnidad.Nombre + "</td>");

                                sb.Append("</tr>");


                                sb.Append("</tbody>");
                                sb.Append("</table>");


                            }
                            
                        });
                     
                    });

                  
                    

                });
                

            });


            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {
                PDF elpdf = new PDF();
                Guid g = Guid.NewGuid();
                string rutaarchivo = Server.MapPath("/Content") + "/media/" + g.ToString() + "movimientostecnico.pdf";
                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, null);
                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "MovimientosTecnico.pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe id='unIframe' src='" + "/Content/media/" + g.ToString() + "movimientostecnico.pdf" + "' frameborder=0  scrolling='no' width='100%' style='height: 100vh;'></iframe>";
                    return View();

                }
            }

        }


        //public StringBuilder Contenido(string almacen)
        //{
        //    List<Movimientos> ordered = new List<Movimientos>();
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<b> Movimentos del almacén: </b>" + almacen);
        //    sb.Append("<br>");
        //    sb.Append("<br>");


        //    sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"" class=""table"">");



        //    sb.Append("</table>");
        //    //sb.Append("<thead>");
        //    //sb.Append("<tr>");
        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"" colspan=6 align=""center"">");
        //    //sb.Append("MOVIMIENTOS");
        //    //sb.Append("</th>");
        //    //sb.Append("</tr>");
        //    //sb.Append("<tr>");

        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"">");
        //    //sb.Append("FECHA");
        //    //sb.Append("</th>");
        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"" colspan=""1"">");
        //    //sb.Append("MOVIMIENTO");
        //    //sb.Append("</th>");
        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"" colspan=""1"">");
        //    //sb.Append("FOLIO");
        //    //sb.Append("</th>");
        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"" colspan=""1"">");
        //    //sb.Append("USUARIO");
        //    //sb.Append("</th>");
        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"" colspan=""1"">");
        //    //sb.Append("ARTICULO");
        //    //sb.Append("</th>");

        //    //sb.Append(@"<th bgcolor=""#424242"" style=""font-size:8px;"" color=""#FFFFFF"" colspan=""1"">");
        //    //sb.Append("CANTIDAD");
        //    //sb.Append("</th>");
        //    //sb.Append("</tr>");
        //    //sb.Append("</thead>");
        //    //sb.Append("<tbody>");

        //    //ordered = movimientos.OrderBy(o => o.fecha).ThenBy(s => s.fecha).ToList();

        //    //ordered.ForEach(x =>
        //    //{
        //    //    sb.Append("<tr>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + string.Format("{0:dd-MM-yyyy}", x.fecha) + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + x.Movimiento + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + x.Folio + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:6px;"">" + x.usuario + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + x.Articulo + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + x.cantidad + "</td>");
        //    //    sb.Append("</tr>");

        //    //});


        //    //sb.Append("</tbody>");
        //    //sb.Append("</table>");

        //    //sb.Append("<table>");
        //    //sb.Append(@"<tr><td colspan=3  style=""font-size:7px;"" align=""center"">Resumen de movimientos</td></tr>");
        //    //sb.Append(@"<tr><td  align=""center"" style=""font-size:7px;"">Movimiento</td><td align=""center"" style=""font-size:7px;"">Artículo</td><td  align=""center"" style=""font-size:7px;"">Cantidad</td></tr>");

        //    //var resumen = ordered.GroupBy(o => new { o.ArticuloClave, o.TipoMovimiento }).Select(s => new { Articulo = s.First().Articulo, Movimiento = s.First().Movimiento, cantidad = s.Sum(d => d.cantidad) });
        //    //foreach (var p in resumen.OrderBy(x=>x.Movimiento))
        //    //{
        //    //    sb.Append("<tr>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + p.Movimiento + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + p.Articulo + "</td>");
        //    //    sb.Append(@"<td align=""right"" style=""font-size:7px;"">" + p.cantidad + "</td>");

        //    //    sb.Append("</tr>");
        //    //}





        //    return sb;
        //}





        public List<Movimientos> Movimientosalmacen(Guid idalmacen, DateTime fi, DateTime ffn, Guid articulos, int proceso)
        {
            List<Movimientos> lista = new List<Movimientos>();
            ModeloAlmacen dc = new ModeloAlmacen();

            if (proceso == 0 || proceso == 4)
            {

                List<Movimientos> recepciones_m = (from w in dc.Proceso
                                                   join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                   join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                   where w.catTipoProcesoClave == 4
                                                   && w.EntidadClaveSolicitante == idalmacen
                                                   && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                                   (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                   orderby w.NumeroPedido
                                                   select new Movimientos
                                                   {
                                                       fecha = w.Fecha.Value,
                                                       cantidad = e.Cantidad.Value,
                                                       Folio = w.NumeroPedido.ToString(),
                                                       Movimiento = w.catTipoProceso.Nombre,
                                                       usuario = d.Nombre,
                                                       Articulo = e.Articulo.Nombre,
                                                       ArticuloClave = e.ArticuloClave,
                                                       TipoMovimiento = 4,
                                                       Unidad=e.Articulo.catUnidad.Nombre
                                                   }).ToList();
                lista.AddRange(recepciones_m);

            }
            if (proceso == 0 || proceso == 2)
            {

                List<Movimientos> recepciones_m = (from w in dc.Proceso
                                                   join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                   join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                   where w.catTipoProcesoClave == 2
                                                   && w.EntidadClaveSolicitante == idalmacen
                                                   && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                                   (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                   orderby w.NumeroPedido
                                                   select new Movimientos
                                                   {
                                                       fecha = w.Fecha.Value,
                                                       cantidad = e.CantidadEntregada.Value,
                                                       Folio = w.NumeroPedido.ToString(),
                                                       Movimiento = w.catTipoProceso.Nombre,
                                                       usuario = d.Nombre,
                                                       Articulo = e.Articulo.Nombre,
                                                       ArticuloClave = e.ArticuloClave,
                                                       TipoMovimiento = 2,
                                                       Unidad = e.Articulo.catUnidad.Nombre
                                                   }).ToList();
                lista.AddRange(recepciones_m);

            }
            if (proceso == 0 || proceso == 3)
            {
                List<Movimientos> salidas_m = (from w in dc.Proceso
                                               join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                               join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                               where w.catTipoProcesoClave == 3
                                               && w.EntidadClaveAfectada == idalmacen
                                               && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                               (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                               orderby w.NumeroPedido
                                               select new Movimientos
                                               {
                                                   fecha = w.Fecha.Value,
                                                   cantidad = e.Cantidad.Value,
                                                   Folio = w.NumeroPedido.ToString(),
                                                   Movimiento = w.catTipoProceso.Nombre,
                                                   usuario = d.Nombre,
                                                   Articulo = e.Articulo.Nombre,
                                                   ArticuloClave = e.ArticuloClave,
                                                   TipoMovimiento = 3,
                                                   Unidad = e.Articulo.catUnidad.Nombre
                                               }).ToList();
                lista.AddRange(salidas_m);


            }
            if (proceso == 0 || proceso == 12)
            {
                List<Movimientos> devoluciones_m = (from w in dc.Proceso
                                                    join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                    join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                    where w.catTipoProcesoClave == 12
                                                    && w.EntidadClaveSolicitante == idalmacen
                                                    && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                                    (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                    orderby w.NumeroPedido
                                                    select new Movimientos
                                                    {
                                                        fecha = w.Fecha.Value,
                                                        cantidad = e.Cantidad.Value,
                                                        Folio = w.NumeroPedido.ToString(),
                                                        Movimiento = w.catTipoProceso.Nombre,
                                                        usuario = d.Nombre,
                                                        Articulo = e.Articulo.Nombre,
                                                        ArticuloClave = e.ArticuloClave,
                                                        TipoMovimiento = 12,
                                                        Unidad = e.Articulo.catUnidad.Nombre
                                                    }).ToList();
                lista.AddRange(devoluciones_m);

            }
            if (proceso == 0 || proceso == 5)
            {
                List<Movimientos> devoluciones_sac = (from w in dc.Proceso
                                                      join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                      join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                      where w.catTipoProcesoClave == 5
                                                      && w.EntidadClaveSolicitante == idalmacen
                                                      && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                                      (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                      orderby w.NumeroPedido
                                                      select new Movimientos
                                                      {
                                                          fecha = w.Fecha.Value,
                                                          cantidad = e.Cantidad.Value,
                                                          Folio = w.NumeroPedido.ToString(),
                                                          Movimiento = w.catTipoProceso.Nombre,
                                                          usuario = d.Nombre,
                                                          Articulo = e.Articulo.Nombre,
                                                          ArticuloClave = e.ArticuloClave,
                                                          TipoMovimiento = 5,
                                                          Unidad = e.Articulo.catUnidad.Nombre
                                                      }).ToList();
                lista.AddRange(devoluciones_sac);

            }
            if (proceso == 0 || proceso == 13)
            {
                List<Movimientos> devoluciones_mat = (from w in dc.Proceso
                                                      join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                      join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                      where w.catTipoProcesoClave == 13
                                                      && w.EntidadClaveAfectada == idalmacen
                                                      && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                                      (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                      orderby w.NumeroPedido
                                                      select new Movimientos
                                                      {
                                                          fecha = w.Fecha.Value,
                                                          cantidad = e.Cantidad.Value,
                                                          Folio = w.NumeroPedido.ToString(),
                                                          Movimiento = w.catTipoProceso.Nombre,
                                                          usuario = d.Nombre,
                                                          Articulo = e.Articulo.Nombre,
                                                          ArticuloClave = e.ArticuloClave,
                                                          TipoMovimiento = 13,
                                                          Unidad = e.Articulo.catUnidad.Nombre
                                                      }).ToList();
                lista.AddRange(devoluciones_mat);
            }

            else if (proceso == 0 || proceso == 16)
            {
                List<Movimientos> rec_dev_mat = (from w in dc.Proceso
                                                 join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
                                                 join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
                                                 where w.catTipoProcesoClave == 16
                                                 && w.Fecha >= fi.Date && w.Fecha <= ffn.Date &&
                                                 (articulos == Guid.Empty || (articulos == e.ArticuloClave))
                                                 && w.EntidadClaveAfectada == idalmacen
                                                 orderby w.NumeroPedido
                                                 select new Movimientos
                                                 {
                                                     fecha = w.Fecha.Value,
                                                     cantidad = e.Cantidad.Value,
                                                     Folio = w.NumeroPedido.ToString(),
                                                     Movimiento = w.catTipoProceso.Nombre,
                                                     usuario = d.Nombre,
                                                     Articulo = e.Articulo.Nombre,
                                                     ArticuloClave = e.ArticuloClave,
                                                     TipoMovimiento = 16,
                                                     Unidad = e.Articulo.catUnidad.Nombre
                                                 }).ToList();

                lista.AddRange(rec_dev_mat);
            }
            
                return lista;
            }



            //public List<Movimientos> MovimientosAp(Guid almacen,DateTime fi, DateTime ffn, Guid articulos,int proceso,int tipo)
            //{

            //    List<Movimientos> lista = new List<Movimientos>();

            //    ModeloAlmacen dc = new ModeloAlmacen();

            //    try
            //    {
            //        if(proceso==0||proceso==2){

            //            List<Movimientos> recepciones_m = (from w in dc.Proceso
            //                                               join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                               join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                               join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                               join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                               where w.catTipoProcesoClave == 2 &&
            //                                               w.catEstatusClave != 6
            //                                               && w.Fecha >= fi && w.Fecha <= ffn 
            //                                               && w.EntidadClaveSolicitante== almacen &&
            //                                               (tipo==0||tipo==s.catTipoArticuloClave) &&
            //                                               (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                               orderby w.NumeroPedido
            //                                               select new Movimientos
            //                                               {
            //                                                   fecha = w.Fecha.Value,
            //                                                   cantidad = e.CantidadEntregada.Value,
            //                                                   Folio = w.NumeroPedido.ToString(),
            //                                                   Movimiento = w.catTipoProceso.Nombre,
            //                                                   usuario = d.Nombre,
            //                                                   Articulo = e.Articulo.Nombre,
            //                                                   ArticuloClave = e.ArticuloClave,
            //                                                   TipoMovimiento = 2
            //                                               }).ToList();

            //            lista.AddRange(recepciones_m);
            //        }
            //        if (proceso == 0 || proceso == 3)
            //        {
            //            List<Movimientos> salidas = (from w in dc.Proceso
            //                                         join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                         join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                         join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                         join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                         where w.catTipoProcesoClave == 3 && w.catEstatusClave !=6
            //                                               && w.Fecha >= fi && w.Fecha <= ffn
            //                                               && w.EntidadClaveAfectada == almacen &&
            //                                               (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                               (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                         orderby w.NumeroPedido
            //                                         select new Movimientos
            //                                         {
            //                                             fecha = w.Fecha.Value,
            //                                             cantidad = e.Cantidad.Value,
            //                                             Folio = w.NumeroPedido.ToString(),
            //                                             Movimiento = w.catTipoProceso.Nombre,
            //                                             usuario = d.Nombre,
            //                                             Articulo = e.Articulo.Nombre,
            //                                             ArticuloClave = e.ArticuloClave,
            //                                             TipoMovimiento = 3
            //                                         }).ToList();
            //            lista.AddRange(salidas);
            //        }
            //        if (proceso == 0 || proceso == 7)
            //        {
            //            List<Movimientos> habilitaciones = (from w in dc.Proceso
            //                                              join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                         join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                         join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                         join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                         where w.catTipoProcesoClave == 7
            //                                         && w.catEstatusClave != 6
            //                                               && w.Fecha >= fi && w.Fecha <= ffn
            //                                               && w.EntidadClaveAfectada == almacen &&
            //                                               (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                               (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                                orderby w.NumeroPedido
            //                                                select new Movimientos
            //                                                {
            //                                                    fecha = w.Fecha.Value,
            //                                                    cantidad = e.Cantidad.Value,
            //                                                    Folio = w.NumeroPedido.ToString(),
            //                                                    Movimiento = w.catTipoProceso.Nombre,
            //                                                    usuario = d.Nombre,
            //                                                    Articulo = e.Articulo.Nombre,
            //                                                    ArticuloClave = e.ArticuloClave,
            //                                                    TipoMovimiento = 7
            //                                                }).ToList();
            //            lista.AddRange(habilitaciones);

            //        }
            //       if (proceso == 0 || proceso == 8)
            //        {
            //            List<Movimientos> bajas = (from w in dc.Proceso
            //                                       join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                       join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                       join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                       join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                       where w.catTipoProcesoClave == 8
            //                                             && w.Fecha >= fi && w.Fecha <= ffn
            //                                             && w.EntidadClaveAfectada == almacen &&
            //                                             (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                             (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                       orderby w.NumeroPedido
            //                                       select new Movimientos
            //                                       {
            //                                           fecha = w.Fecha.Value,
            //                                           cantidad = e.Cantidad.Value,
            //                                           Folio = w.NumeroPedido.ToString(),
            //                                           Movimiento = w.catTipoProceso.Nombre,
            //                                           usuario = d.Nombre,
            //                                           Articulo = e.Articulo.Nombre,
            //                                           ArticuloClave = e.ArticuloClave,
            //                                           TipoMovimiento = 8
            //                                       }).ToList();
            //            lista.AddRange(bajas);


            //        }
            //        if (proceso == 0 || proceso == 9)
            //        {
            //            List<Movimientos> reparaciones = (from w in dc.Proceso
            //                                              join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                              join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                              join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                              join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                              where w.catTipoProcesoClave == 9
            //                                                    && w.Fecha >= fi && w.Fecha <= ffn
            //                                                    && w.EntidadClaveAfectada == almacen &&
            //                                                    (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                                    (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                              orderby w.NumeroPedido
            //                                              select new Movimientos
            //                                              {
            //                                                  fecha = w.Fecha.Value,
            //                                                  cantidad = e.Cantidad.Value,
            //                                                  Folio = w.NumeroPedido.ToString(),
            //                                                  Movimiento = w.catTipoProceso.Nombre,
            //                                                  usuario = d.Nombre,
            //                                                  Articulo = e.Articulo.Nombre,
            //                                                  ArticuloClave = e.ArticuloClave,
            //                                                  TipoMovimiento = 9
            //                                              }).ToList();

            //            lista.AddRange(reparaciones);
            //        }
            //       if (proceso == 0 || proceso == 6)
            //        {
            //            List<Movimientos> rec_dev_sac = (from w in dc.Proceso
            //                                             join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                             join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                             join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                             join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                             where w.catTipoProcesoClave == 6
            //                                                   && w.Fecha >= fi && w.Fecha <= ffn
            //                                                   && w.EntidadClaveAfectada == almacen &&
            //                                                   (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                                   (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                             orderby w.NumeroPedido
            //                                             select new Movimientos
            //                                             {
            //                                                 fecha = w.Fecha.Value,
            //                                                 cantidad = e.Cantidad.Value,
            //                                                 Folio = w.NumeroPedido.ToString(),
            //                                                 Movimiento = w.catTipoProceso.Nombre,
            //                                                 usuario = d.Nombre,
            //                                                 Articulo = e.Articulo.Nombre,
            //                                                 ArticuloClave = e.ArticuloClave,
            //                                                 TipoMovimiento = 6
            //                                             }).ToList();

            //            lista.AddRange(rec_dev_sac);
            //        }
            //        if (proceso == 0 || proceso == 14)
            //        {
            //            List<Movimientos> rec_dev_mat = (from w in dc.Proceso
            //                                             join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                             join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                             join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                             join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                             where w.catTipoProcesoClave == 14
            //                                                   && w.Fecha >= fi && w.Fecha <= ffn
            //                                                   && w.EntidadClaveAfectada == almacen &&
            //                                                   (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                                   (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                             orderby w.NumeroPedido
            //                                             select new Movimientos
            //                                             {
            //                                                 fecha = w.Fecha.Value,
            //                                                 cantidad = e.Cantidad.Value,
            //                                                 Folio = w.NumeroPedido.ToString(),
            //                                                 Movimiento = w.catTipoProceso.Nombre,
            //                                                 usuario = d.Nombre,
            //                                                 Articulo = e.Articulo.Nombre,
            //                                                 ArticuloClave = e.ArticuloClave,
            //                                                 TipoMovimiento = 14
            //                                             }).ToList();

            //            lista.AddRange(rec_dev_mat);
            //        }
            //         if (proceso == 0 || proceso == 16)
            //        {
            //            List<Movimientos> rec_dev_mat = (from w in dc.Proceso
            //                                             join e in dc.ProcesoArticulo on w.ProcesoClave equals e.ProcesoClave
            //                                             join v in dc.Articulo on e.ArticuloClave equals v.ArticuloClave
            //                                             join s in dc.catTipoArticulo on v.catTipoArticuloClave equals s.catTipoArticuloClave
            //                                             join d in dc.Usuario on w.UsuarioClave equals d.UsuarioClave
            //                                             where w.catTipoProcesoClave == 16
            //                                                   && w.Fecha >= fi && w.Fecha <= ffn
            //                                                   && w.EntidadClaveSolicitante == almacen &&
            //                                                   (tipo == 0 || tipo == s.catTipoArticuloClave) &&
            //                                                   (articulos == Guid.Empty || (articulos == v.ArticuloClave))
            //                                             orderby w.NumeroPedido
            //                                             select new Movimientos
            //                                             {
            //                                                 fecha = w.Fecha.Value,
            //                                                 cantidad = e.Cantidad.Value,
            //                                                 Folio = w.NumeroPedido.ToString(),
            //                                                 Movimiento = w.catTipoProceso.Nombre,
            //                                                 usuario = d.Nombre,
            //                                                 Articulo = e.Articulo.Nombre,
            //                                                 ArticuloClave = e.ArticuloClave,
            //                                                 TipoMovimiento = 16
            //                                             }).ToList();

            //            lista.AddRange(rec_dev_mat);
            //        }





            //    }
            //    catch { }


            //    return lista;

            //}








            //public ActionResult Excel()
            //{

            //    ModeloAlmacen dc = new ModeloAlmacen();



            //    Guid distribuidor = Guid.Empty;
            //    try
            //    {
            //        distribuidor = Guid.Parse(Request["distribuidor"]);
            //    }
            //    catch
            //    {
            //        distribuidor = Guid.Empty;
            //    }

            //    Guid almacenes = Guid.Empty;
            //    try
            //    {
            //        almacenes = Guid.Parse(Request["almacen"]);
            //    }
            //    catch
            //    {
            //        almacenes = Guid.Empty;
            //    }
            //    Guid tecnicos = Guid.Empty;
            //    try
            //    {
            //        tecnicos = Guid.Parse(Request["tecnico"]);
            //    }
            //    catch
            //    {
            //        tecnicos = Guid.Empty;
            //    }
            //    int clasificaciones = 0;
            //    try
            //    {
            //        clasificaciones = Convert.ToInt32(Request["clasificacion"]);
            //    }
            //    catch
            //    {
            //        clasificaciones = 0;
            //    }
            //    int tipos = 0;
            //    try
            //    {
            //        tipos = Convert.ToInt32(Request["tipo"]);
            //    }
            //    catch
            //    {
            //        tipos = 0;
            //    }
            //    Guid articulos = Guid.Empty;
            //    try
            //    {
            //        articulos = Guid.Parse(Request["Articulo"]);
            //    }
            //    catch
            //    {
            //        articulos = Guid.Empty;
            //    }
            //    int proceso = 0;
            //    try
            //    {
            //        proceso = Int32.Parse(Request["proceso"]);
            //    }
            //    catch
            //    {
            //        proceso = 0;
            //    }


            //    string fechaInicio = Convert.ToString(Request["fechainicio"]);
            //    string fechaFin = Convert.ToString(Request["fechaFin"]);



            //    DateTime fi = new DateTime(Convert.ToInt32(fechaInicio.Split('/')[2]), Convert.ToInt32(fechaInicio.Split('/')[1]), Convert.ToInt32(fechaInicio.Split('/')[0]));
            //    DateTime ffn = new DateTime(Convert.ToInt32(fechaFin.Split('/')[2]), Convert.ToInt32(fechaFin.Split('/')[1]), Convert.ToInt32(fechaFin.Split('/')[0]));
            //    ffn = ffn.AddDays(1);
            //    String empresa = Config.Dame("EMPRESA");
            //    DumpExcel(almacenes, fi, ffn, articulos, proceso, tipos);
            //    return null;


            //}
            //private void DumpExcel(Guid almacenes, DateTime fi, DateTime ffn, Guid articulos, int proceso, int tipo)
            //{
            //    ModeloAlmacen dc = new ModeloAlmacen();

            //    using (ExcelPackage pck = new ExcelPackage())
            //    {

            //        Entidad alm = dc.Entidad.Where(s => s.EntidadClave == almacenes).First();
            //        List<Movimientos> movimientos = new List<Movimientos>();


            //        movimientos.AddRange(MovimientosAp(almacenes, fi, ffn, articulos, proceso, tipo));




            //        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Movimientos de artículos de almecén " + alm.Nombre);
            //        ws.Cells["A2"].LoadFromCollection(movimientos.Select(x => new { FECHA = x.fecha.ToString(), x.Movimiento, x.Folio, x.usuario, x.Articulo, x.cantidad }).ToList(), true);

            //        estilo a datos de plantilla
            //            using (ExcelRange rng = ws.Cells["A1:F1"])
            //        {
            //            rng.Style.Font.Bold = true;
            //            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
            //            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
            //            rng.Style.Font.Color.SetColor(Color.White);


            //        }
            //        using (ExcelRange rng = ws.Cells["A2:F2"])
            //        {
            //            rng.Style.Font.Bold = true;
            //            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
            //            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
            //            rng.Style.Font.Color.SetColor(Color.White);


            //        }




            //        ws.Column(1).Width = 15;
            //        ws.Column(2).Width = 30;
            //        ws.Column(3).Width = 15;
            //        ws.Column(4).Width = 15;
            //        ws.Column(5).Width = 15;
            //        ws.Column(6).Width = 15;





            //        datos de plantilla
            //            string empresa = Config.Dame("EMPRESA");
            //        ws.Cells["A1"].Value = empresa;


            //        if (articulos == Guid.Empty)
            //        {
            //            ws.Cells["B1"].Value = "MOVIMIENTOS DE ARTICULOS: TODOS LOS ARTICULOS";
            //        }
            //        else
            //        {
            //            Articulo art = dc.Articulo.Where(o => o.ArticuloClave == articulos).First();
            //            ws.Cells["B1"].Value = "MOVIMIENTOS DE ARTICULOS:" + art.Nombre;
            //        }



            //        ws.Cells["C1"].Value = "PERIDO DEL ";
            //        ws.Cells["D1"].Value = fi.ToString();
            //        ws.Cells["E1"].Value = "AL ";
            //        ws.Cells["F1"].Value = ffn.ToString();

            //        ws.Cells["A3"].Value = "CATEGORIA";
            //        ws.Cells["B3"].Value = categoria;
            //        ws.Cells["C3"].Value = "TIPO DE ARTICULO";
            //        ws.Cells["D3"].Value = tipo;
            //        ws.Cells["E3"].Value = "ARTICULO";
            //        ws.Cells["F3"].Value = articulo;






            //        Example how to Format Column 1 as numeric
            //            using (ExcelRange col = ws.Cells[5, 1, 5 + movimientos.Count, 1])
            //        {
            //            col.Style.Numberformat.Format = "#,##0.00";
            //            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //        }






            //        Response.Clear();
            //        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //        Response.AddHeader("content-disposition", "attachment;  filename=Movimientos de tecnico.xlsx");
            //        Response.BinaryWrite(pck.GetAsByteArray());
            //        Response.End();



            //    }
            //}




        }
}




