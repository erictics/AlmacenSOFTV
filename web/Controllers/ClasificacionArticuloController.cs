﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class ClasificacionArticuloController : Controller
    {
        //
        // GET: /ClasificacionArticulo/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
           Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
            
        }

        int elementos = 15;

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catClasificacionArticuloWrapper> data { get; set; }
        }


        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ObtenClasificaciones(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ObtenClasificaciones(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }



        public List<catClasificacionArticuloWrapper> ObtenClasificaciones(string data)
        {

            

           

           ModeloAlmacen dc =new ModeloAlmacen();

            List<catClasificacionArticulo> rls = (from use in dc.catClasificacionArticulo
                                 orderby use.Descripcion
                                 select use).ToList();

            List<catClasificacionArticuloWrapper> rw = new List<catClasificacionArticuloWrapper>();

            foreach (catClasificacionArticulo u in rls)
            {
                catClasificacionArticuloWrapper w = ClasificacionArticuloToWrapper(u);
                rw.Add(w);
            }

            return rw;
        }


        public ActionResult Obten(string data)
        {
            catClasificacionArticuloWrapper rw = new catClasificacionArticuloWrapper();

           ModeloAlmacen dc =new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

           catClasificacionArticulo r = (from ro in dc.catClasificacionArticulo
                         where ro.catClasificacionArticuloClave == uid
                         select ro).SingleOrDefault();

            if (r != null)
            {
                rw = ClasificacionArticuloToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
               ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                   catClasificacionArticulo r = (from ro in dc.catClasificacionArticulo
                                 where ro.catClasificacionArticuloClave == uid
                                 select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
               ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                   catClasificacionArticulo r = (from ro in dc.catClasificacionArticulo
                                                      where ro.catClasificacionArticuloClave == uid
                                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_activa)
        {

            try
            {

               ModeloAlmacen dc = new ModeloAlmacen();

               catClasificacionArticulo r = new catClasificacionArticulo();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catClasificacionArticulo
                         where ro.catClasificacionArticuloClave == uid
                         select ro).SingleOrDefault();

                    if (ed_activa != String.Empty && ed_activa != null)
                    {
                        r.Activo = true;
                    }
                    else { 
                        r.Activo = false; 
                    }

                }
                else
                {
                    r.Activo = true;
                }

                r.Descripcion = ed_nombre;

                if (ed_id == String.Empty)
                {
                    dc.catClasificacionArticulo.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        private catClasificacionArticuloWrapper ClasificacionArticuloToWrapper(catClasificacionArticulo u)
        {
            catClasificacionArticuloWrapper rw = new catClasificacionArticuloWrapper();
            rw.Clave = u.catClasificacionArticuloClave.ToString();
            rw.Descripcion = u.Descripcion;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }

    }
}
