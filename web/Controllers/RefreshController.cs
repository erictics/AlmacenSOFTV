﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class RefreshController : Controller
    {
        //
        // GET: /Refresh/
         [Web.Controllers.SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }


         public ActionResult valida(string clave)
         {
             Usuario u = ((Usuario)Session["u"]);
             ModeloAlmacen dc = new ModeloAlmacen();
             Guid distribuidor;
             try
             {
                distribuidor = u.catDistribuidor.catDistribuidorClave;
             }
             catch {
                 distribuidor = Guid.Empty;
             }            
             Serie serie = (from a in dc.Serie where a.Valor==clave select a).FirstOrDefault();
            if (serie == null)
            {
                return Json("-2", JsonRequestBehavior.AllowGet);
            }
            else
            {
                Inventario inv = (from d in dc.Inventario where d.InventarioClave == serie.InventarioClave  && d.Articulo.IdArticulo==20 select d).FirstOrDefault();
                if (inv == null)
                {
                    return Json("-4", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (distribuidor != Guid.Empty)
                    {
                        Entidad tecnico = (from g in dc.Entidad where g.EntidadClave == inv.EntidadClave && g.catDistribuidorClave == distribuidor && g.catEntidadTipoClave == 2 select g).FirstOrDefault();

                        if (tecnico == null)
                        {
                            return Json("-1", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(tecnico.Nombre, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        Entidad tecnico = (from g in dc.Entidad where g.EntidadClave == inv.EntidadClave && g.catEntidadTipoClave == 2 select g).FirstOrDefault();

                        if (tecnico == null)
                        {
                            return Json("-1", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(tecnico.Nombre, JsonRequestBehavior.AllowGet);
                        }
                    }                   
                }
            }
        }


         public ActionResult HacerRefresh(string clave)
                     
         {

             if (clave == "")
             {
                 return Json("0", JsonRequestBehavior.AllowGet);
             }
             Usuario u = ((Usuario)Session["u"]);
            web.Models.Newsoftv.NewSoftvModel ns = new Models.Newsoftv.NewSoftvModel();
             SqlConnection conexionSQL = new SqlConnection(ns.Database.Connection.ConnectionString);
             try
             {
                 conexionSQL.Open();
             }
             catch
             {
                 conexionSQL.Close();
                 return Json("-1", JsonRequestBehavior.AllowGet);
             }
             try
             {



                 SqlCommand comandoSql = new SqlCommand("MANDA_CNR_DIG_AlmacenPorDistribuidor_Dev", conexionSQL);
                 comandoSql.CommandType = CommandType.StoredProcedure;


                 comandoSql.Parameters.AddWithValue("mac_address", clave);
                 comandoSql.Parameters.AddWithValue("LOGIN", u.UsuarioClave);
                 comandoSql.Parameters.AddWithValue("PASAPORTE", u.Password);

                 SqlParameter param = new SqlParameter("Msj", SqlDbType.VarChar, 400);
                 param.Direction = ParameterDirection.Output;
                 comandoSql.Parameters.Add(param);
                 comandoSql.ExecuteNonQuery();


                 string MSJ = comandoSql.Parameters["Msj"].Value.ToString();
                 conexionSQL.Close();
                 return Json(MSJ, JsonRequestBehavior.AllowGet);
                
             }
             catch
             {
                 conexionSQL.Close();
                 return Json("0", JsonRequestBehavior.AllowGet);
             }
            

         }
    }
}
