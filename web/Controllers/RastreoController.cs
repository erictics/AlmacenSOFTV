﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class RastreoController : Controller
    {
        //      //
        //      // GET: /Rastreo/
        //      ModeloAlmacen dc = new ModeloAlmacen();

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {

            return View();
        }


        public ActionResult DetalleAparato(string serie)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = (Usuario)Session["u"];
            List<Guid> idinventario = new List<Guid>();
            try
            {
                
                idinventario = dc.Serie.Where(o => o.Valor.Contains(serie)).Select(s => s.InventarioClave).Take(20).ToList();
            }
            catch
            { }
            if (idinventario.Count == 0)
            {
                return Content("-1");
            }
            else
            {
                List<Aparato> aparatos = new List<Aparato>();
                idinventario.ForEach(xx=>{
                    Aparato a = new Aparato();
                    Inventario inventario = dc.Inventario.Where(o => o.InventarioClave == xx).First();
                     a= ObtenDetalleAparato(inventario);
                     aparatos.Add(a);
                });
                       
               
                return Json(aparatos, JsonRequestBehavior.AllowGet);
            }
        }

        public bool? instaladoPorDistribuidor(Inventario inventario, long distribuidor)
        {
            return (inventario.BitacoraSofTVDetalle
                .OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.IdDistribuidor == distribuidor) ? true : false;
        }

        public class Aparato
        {
            public string serie { get; set; }
            public string idInventario { get; set; }
            public string Nombre { get; set; }
            public string Status { get; set; }
            public string Ubicacion { get; set; }

        }

        public Aparato ObtenDetalleAparato(Inventario inventario)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Aparato aparato = new Aparato();

            aparato.idInventario = inventario.IdInventario.ToString();
            aparato.Nombre = inventario.Articulo.Nombre;
            aparato.serie = dc.Serie.Where(p => p.InventarioClave == inventario.InventarioClave).Select(o => o.Valor).First();
            aparato.Status = dc.catEstatus.Where(o => o.catEstatusClave == inventario.EstatusInv.Value).Select(o => o.Nombre).First();
            if (inventario.Entidad != null)
            {
                aparato.Ubicacion = inventario.Entidad.Nombre;
            }
            else
            {
                try
                {
                    aparato.Ubicacion = "";
                    if (inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.Contrato != String.Empty)
                    {
                        aparato.Ubicacion += "Contrato:" + inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.Contrato + ", ";
                    }

                    if (inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.NumeroOrden != null)
                    {
                        aparato.Ubicacion += " Orden: " + inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.NumeroOrden.Value.ToString();
                    }
                }
                catch
                {
                }

            }
            return aparato;
        }





        //      public ActionResult ObtenPorClasificacion(string data, int draw, int start, int length)
        //      {




        //          int sortColumn = -1;
        //          string sortDirection = "asc";



        //          JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
        //          int pag = Convert.ToInt32(jObject["pag"].ToString());
        //          string search = jObject["buscar"].ToString();
        //          int tipo = -1;
        //          int cla = -1;



        //          try  { tipo = Convert.ToInt32(jObject["tipo"].ToString());}
        //          catch{ }

        //          try
        //          {
        //              cla = Convert.ToInt32(jObject["cla"].ToString());
        //          }
        //          catch
        //          {
        //          }








        //          DataTableData dataTableData = new DataTableData();
        //          dataTableData.draw = draw;
        //          dataTableData.recordsTotal = 0;
        //          int recordsFiltered = 0;
        //          dataTableData.data = FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection,tipo,cla);
        //          dataTableData.recordsFiltered = recordsFiltered;

        //          return Json(dataTableData, JsonRequestBehavior.AllowGet);

        //      }

        //      private List<RastreoWrapper> FilterData(ref int recordFiltered, int start, int length, string search, int sortColumn, string sortDirection, int tipo, int cla)
        //      {

        //          Usuario u = (Usuario)Session["u"];
        //           List<Inventario> invs=null;
        //           try
        //           {
        //               if (u.catDistribuidor.catDistribuidorClave != null)
        //               {

        //                   invs = (from inv in dc.Inventario
        //                           where
        //                           inv.Articulo.Activo == true
        //                           && (inv.Articulo.catTipoArticulo.catClasificacionArticuloClave == cla || cla == -1)
        //                           && inv.Cantidad > 0
        //                           && (inv.Articulo.catTipoArticuloClave == tipo || tipo == -1)
        //                           && (inv.Articulo.catTipoArticulo.SerieTipoArticulo.Count > 0)
        //                           && (inv.Entidad.catDistribuidorClave == u.DistribuidorClave)
        //                           orderby inv.Articulo.Nombre
        //                           select inv).ToList();
        //               }
        //           }
        //           catch
        //           {
        //               invs = (from inv in dc.Inventario
        //                       where
        //                       inv.Articulo.Activo == true
        //                       && (inv.Articulo.catTipoArticulo.catClasificacionArticuloClave == cla || cla == -1)
        //                       && inv.Cantidad > 0
        //                       && (inv.Articulo.catTipoArticuloClave == tipo || tipo == -1)
        //                       && (inv.Articulo.catTipoArticulo.SerieTipoArticulo.Count > 0)
        //                       orderby inv.Articulo.Nombre
        //                       select inv).ToList();
        //           }
        //          List<catSerie> catseries = (from s in dc.catSerie
        //                                          where s.Activo
        //                                          select s).ToList();


        //          status = (from s in dc.catEstatus
        //                    select s).ToList();

        //          List<RastreoWrapper> rwl = new List<RastreoWrapper>();

        //          List<Inventario> inv2 = new List<Inventario>();
        //          try
        //          {
        //              inv2 = invs.GetRange(start, Math.Min(length, invs.Count - start));
        //          }
        //          catch
        //          {
        //              inv2 = invs.GetRange(0, 10);
        //          }


        //          foreach (Inventario i in inv2)
        //          {

        //              try
        //              {




        //                      rwl.Add(Inventario2Rastreo(i, catseries));

        //              }
        //              catch
        //              { }
        //          }
        //          List<RastreoWrapper> list = new List<RastreoWrapper>();



        //          if (search == ""||search ==null)
        //          {
        //              list = rwl;
        //          }
        //          else
        //          {

        //              List<RastreoWrapper> rwl2 = new List<RastreoWrapper>();
        //              List<Serie> series = (from s in dc.Serie
        //                                        where s.Valor.Contains(search)
        //                                        && (u.DistribuidorClave == null || s.Inventario.Entidad.catDistribuidorClave == u.DistribuidorClave || s.Inventario.EntidadClave == null)
        //                                        select s).ToList();

        //              List<catSerie> catseries1 = (from s in dc.catSerie
        //                                              where s.Activo
        //                                              select s).ToList();


        //              status = (from s in dc.catEstatus
        //                        select s).ToList();


        //              foreach (Serie s in series)
        //              {
        //                  // si la clave de distribuidor es nula o la clave sea nula 
        //                  // se a
        //                  if (u.DistribuidorClave == null || s.Inventario.EntidadClave != null || s.Inventario.BitacoraSofTVDetalle.ToArray()[0].BitacoraSofTV.catDistribuidorClave == u.DistribuidorClave)
        //                  {
        //                      //se enviara cada clave de inventario y el catalago de series 
        //                      rwl2.Add(Inventario2Rastreo(s.Inventario, catseries));
        //                      //se recibe el wrapper
        //                  }
        //              }


        //              // simulate search
        //              list = rwl2.ToList();
        //              return list;
        //          }



        //          recordFiltered = invs.Count;

        //          // get just one page of data


        //          return list;
        //      }





        //      List<catEstatus> status = new List<catEstatus>();

        //      public class DataTableData
        //      {
        //          public int draw { get; set; }
        //          public int recordsTotal { get; set; }
        //          public int recordsFiltered { get; set; }
        //          public List<RastreoWrapper> data { get; set; }
        //      }


        //      /// <summary>
        //      /// 
        //      /// 
        //      /// 
        //      /// </summary>
        //      /// <param name="data"></param>
        //      /// <returns></returns>



        //      public ActionResult ObtenPorSerie(string data)
        //      {



        //          JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
        //          int pag = Convert.ToInt32(jObject["pag"].ToString());

        //          string serie = jObject["serie"].ToString();


        //          Usuario u = (Usuario)Session["u"];


        //          List<Serie> series = (from s in dc.Serie
        //                                    where s.Valor.Contains(serie)
        //                                    && (u.DistribuidorClave == null || s.Inventario.Entidad.catDistribuidorClave == u.DistribuidorClave || s.Inventario.EntidadClave == null)
        //                                    select s).ToList();

        //          List<catSerie> catseries = (from s in dc.catSerie
        //                                          where s.Activo
        //                                    select s).ToList();


        //          status = (from s in dc.catEstatus
        //                    select s).ToList();

        //          List<RastreoWrapper> rwl = new List<RastreoWrapper>();

        //          foreach (Serie s in series)
        //          {
        //              // si la clave de distribuidor es nula o la clave sea nula 
        //              // se a
        //              if (u.DistribuidorClave==null || s.Inventario.EntidadClave != null || s.Inventario.BitacoraSofTVDetalle.ToArray()[0].BitacoraSofTV.catDistribuidorClave == u.DistribuidorClave)
        //              {
        //                  //se enviara cada clave de inventario y el catalago de series 
        //                  rwl.Add(Inventario2Rastreo(s.Inventario, catseries));
        //                  //se recibe el wrapper
        //              }
        //          }


        //          return Json(rwl,JsonRequestBehavior.AllowGet);

        //      }

        ////**************************************************************************************************************



        //      //se recibe varias objectos de inventario y la lista de cat series
        //      public RastreoWrapper Inventario2Rastreo(Inventario inventario, List<catSerie> catseries) {
        //      // se recibe un solo row con los campos de inventario


        //          //se crea un wrapper 

        //  //         public class RastreoWrapper    {

        //  //    public string Clave { get; set; }
        //  //    public List<string> SeriesDef { get; set; }
        //  //    public List<string> Series { get; set; }
        //  //    public string Estatus { get; set; }
        //  //    public string Ubicacion { get; set; }
        //  //    public string Descripcion { get; set; }

        //  //}
        //          RastreoWrapper rw = new RastreoWrapper();

        //          //se agrega al wrapper la clave de inventario 
        //          rw.Clave = inventario.InventarioClave.ToString();
        //          //se agrega una lista de series
        //          rw.Series = new List<string>();
        //          //se iguala lista de series a las series de el objeto que envie 
        //          List<Serie> series = inventario.Serie.ToList();


        //          //se recorre la lista de catseries

        //          foreach (catSerie cs in catseries)
        //          {
        //              //selecciona  las series 
        //              Serie s = (from se in series
        //                             where se.catSerieClave == cs.catSerieClave
        //                             select se).SingleOrDefault();
        //              //si el objeto inv tiene series 
        //              if (s != null)
        //              {
        //                  //se agrega en wrapper 
        //                  rw.Series.Add(s.Valor);
        //                  rw.valor = s.Valor;
        //              }
        //              else
        //              {
        //                  rw.Series.Add(String.Empty);
        //              }
        //          }

        //          //se agrega status al wrapper se busca en estatus la concidencia 
        //          // del obj inventario 
        //          catEstatus ce = (from c in status
        //                                   where c.catEstatusClave == inventario.EstatusInv
        //                               select c).SingleOrDefault();
        //          if (ce != null)
        //          {
        //              //se agrega el nombre del status
        //              rw.Estatus = ce.Nombre;
        //          }


        //          //para agregar ubicacion al wrapper  se iguala al obj inventario

        //          if (inventario.Entidad != null)
        //          {
        //              rw.Ubicacion = inventario.Entidad.Nombre;
        //          }
        //          else
        //          {
        //              try
        //              {
        //                  rw.Ubicacion = "";
        //                  if (inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.Contrato != String.Empty)
        //                  {
        //                      rw.Ubicacion += "Contrato:" + inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.Contrato + ", ";
        //                  }

        //                  if (inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.NumeroOrden != null)
        //                  {
        //                      rw.Ubicacion += " Orden: " + inventario.BitacoraSofTVDetalle.OrderByDescending(x => x.BitacoraSofTVClave).ToArray()[0].BitacoraSofTV.NumeroOrden.Value.ToString();
        //                  }
        //              }
        //              catch
        //              { 
        //              }

        //          }
        //          //se agrega descripcion al wrapper con el nombre del articulo
        //          rw.Descripcion = inventario.Articulo.Nombre;
        //          //retorna el wrapper
        //          return rw;
        //      }


    }
}
