﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;
using web.Models;
using web.Utils;
using web.Wrappers;
using Web.Controllers;

namespace web.Controllers
{
    public class BitacoraAlmacenController : Controller
    {
        ModeloAlmacen dc = new ModeloAlmacen();

        [SessionExpireFilterAttribute]
        public ActionResult Index()
        {
            Usuario u = ((Usuario)Session["u"]);
            List<catDistribuidor> distribuidores = null;
            if (u.DistribuidorClave == null){
                distribuidores = dc.catDistribuidor.Where(x => x.Activo == true).ToList();
            }
            else {
                distribuidores = dc.catDistribuidor.Where(x => x.catDistribuidorClave==u.DistribuidorClave).ToList();
            }

            List<catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                              where ta.Activo
                                                              orderby ta.Descripcion
                                                              select ta).ToList();  
            ViewData["clasificaciones"] = clasificaciones;
            ViewData["distribuidores"] = distribuidores;
            return View();
        }

        public ActionResult GetalmacenbyDistribuidor(Guid distribuidor)
        {
            var almacenes = dc.Entidad.Where(x => x.catDistribuidorClave == distribuidor && x.catEntidadTipoClave == 1)
                 .Select(x => new { x.EntidadClave, x.IdEntidad, x.Nombre }).OrderBy(d => d.Nombre);
            return Json(almacenes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GettecnicosbyDistribuidor(Guid distribuidor)
        {
            var tecnicos = dc.Entidad.Where(x => x.catDistribuidorClave == distribuidor && x.catEntidadTipoClave == 2)
                   .Select(x => new { x.EntidadClave, x.IdEntidad, x.Nombre }).ToList();

            var tecgen = (from a in dc.Entidad
                          join b in dc.RelTecnicoDistribuidor on a.EntidadClave equals b.EntidadClave
                          where b.DistribuidorClave == distribuidor
                          select new {a.EntidadClave ,a.IdEntidad, a.Nombre, }).ToList();
            tecnicos.AddRange(tecgen);

            return Json(tecnicos, JsonRequestBehavior.AllowGet);
        }

       public class detserie
        {
            public long ? IdInventario { get; set;}
            public string serie { get; set; }
        }

        public class InventarioTecnioW
        {
            public string Articulo { get; set; }
            public Guid Clave { get; set; }
            public bool tieneSeries { get; set; }
            public List<detserie> series { get; set; }
            public int cantidad { get; set; }
        }


        public ActionResult ObtenInventarioTecnico(Guid tecnico)
        {
            var inventario = dc.Inventario.Where(x => x.EntidadClave == tecnico && x.EstatusInv == 7 && x.Standby == 0 && x.Cantidad > 0)
                  .Select(x => new { x.Articulo.Nombre, x.ArticuloClave }).ToList();
            List<InventarioTecnioW> results = (from p in inventario
                          group p by p.ArticuloClave into g
                          select new InventarioTecnioW { Clave = g.Key.Value,Articulo=g.First().Nombre }).ToList();

            results.ForEach(y=> {
                y.tieneSeries = tieneSeries(y.Clave);
            if (y.tieneSeries)
            {
                y.series = (from a in dc.Inventario
                            join b in dc.Serie on a.InventarioClave equals b.InventarioClave
                            where a.EntidadClave == tecnico && a.EstatusInv == 7 && a.ArticuloClave == y.Clave
                            select new detserie { IdInventario = a.IdInventario, serie = b.Valor }).ToList();


            } else
            {
                int? cantidad = (from a in dc.Inventario
                                 where a.EntidadClave == tecnico && a.EstatusInv == 7 && a.ArticuloClave == y.Clave
                                 select a.Cantidad ).First();
                    y.cantidad = cantidad.Value;

                }
            });

            return Json(results, JsonRequestBehavior.AllowGet);
        }


        public bool tieneSeries(Guid artclave)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult GuardaBitacora(Guid distribuidor, Guid almacen, Guid tecnico, int ? orden, string tipo, string contrato, string usuario, string fecha,string articulos)
        {
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                catDistribuidor dist;
                Entidad alm;
                Entidad tec;
                DateTime dt;
                long clave = 0;
                try
                {
                    dist = dc.catDistribuidor.Where(x => x.catDistribuidorClave == distribuidor).First();
                    alm = dc.Entidad.Where(x => x.EntidadClave == almacen).First();
                    tec = dc.Entidad.Where(x => x.EntidadClave == tecnico).First();
                    dt = Convert.ToDateTime(fecha);
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -1;
                    e1.Mensaje = ex.InnerException.Message;
                    return Json(e1, JsonRequestBehavior.AllowGet);
                }

                try
                {
                    BitacoraSofTV bit = new BitacoraSofTV();
                    bit.catDistribuidorClave = dist.catDistribuidorClave;
                    bit.Contrato = contrato;
                    bit.EntidadClaveAlmacen = alm.EntidadClave;
                    bit.EntidadClaveTecnico = tec.EntidadClave;
                    bit.Fecha = dt;
                    bit.IdDistribuidor = dist.IdDistribuidor;
                    bit.IdEntidadAlmacen = alm.idEntidadSofTV;
                    bit.IdEntidadTecnico = tec.IdEntidad;
                    bit.NombreUsuario = usuario;
                    bit.NumeroOrden = orden.HasValue ? orden : 0;
                    bit.TipoServicio = "A";
                    dc.BitacoraSofTV.Add(bit);
                    dc.SaveChanges();
                    clave = bit.BitacoraSofTVClave;
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -1;
                    e1.Mensaje = ex.InnerException.Message;
                    return Json(e1, JsonRequestBehavior.AllowGet);
                    
                }

                try
                {
                    Entidad e = dc.Entidad.Where(x => x.EntidadClave == tecnico).First();
                    BitacoraSofTV bsoftv = dc.BitacoraSofTV.Where(x => x.BitacoraSofTVClave == clave).First();
                    Usuario u = ((Usuario)Session["u"]);
                    List<DetalleBitacora> articulos_ = (List<DetalleBitacora>)JsonConvert.DeserializeObject(articulos, typeof(List<DetalleBitacora>));
                    foreach (var x in articulos_)
                    {


                        if (tieneSeries(x.Clave))
                        {
                            Inventario i = null;
                            try
                            {
                                i = dc.Inventario.Where(y => y.IdInventario == x.IdInventario && y.EntidadClave == e.EntidadClave && y.EstatusInv == 7).First();
                            }
                            catch
                            {

                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -2;
                                e1.Mensaje = "El aparato no se encuentra en el saldo del técnico";
                                return Json(e1, JsonRequestBehavior.AllowGet);

                            }
                            BitacoraSofTVDetalle bsoftvd = new BitacoraSofTVDetalle();
                            bsoftvd.BitacoraSofTVClave = bsoftv.BitacoraSofTVClave;
                            bsoftvd.fecha = DateTime.Now;
                            bsoftvd.activo = true;
                            bsoftvd.usuariosoft = u.Nombre.Substring(0, 5);
                            bsoftvd.Cantidad = 1;
                            bsoftvd.InventarioClave = i.InventarioClave;
                            dc.BitacoraSofTVDetalle.Add(bsoftvd);
                            dc.SaveChanges();

                            i.EstatusInv = 8;
                            i.EntidadClave = null;
                            dc.SaveChanges();

                        }
                        else
                        {

                            Inventario i = dc.Inventario.Where(y => y.ArticuloClave == x.Clave && y.EntidadClave == e.EntidadClave && y.EstatusInv == 7).First();
                            if (i.Cantidad < x.Cantidad)
                            {
                                dbContextTransaction.Rollback();
                                error e1 = new error();
                                e1.idError = -1;
                                e1.Mensaje = " La cantidad solicitada del  articulo " + i.Articulo.Nombre + " es mayor que la cantidad que cuenta el técnico";
                                return Json(e1, JsonRequestBehavior.AllowGet);

                            }
                            else
                            {
                                BitacoraSofTVDetalle bsoftvd = new BitacoraSofTVDetalle();
                                bsoftvd.BitacoraSofTVClave = bsoftv.BitacoraSofTVClave;
                                bsoftvd.fecha = DateTime.Now;
                                bsoftvd.activo = true;
                                bsoftvd.usuariosoft = u.Nombre.Substring(0, 5);
                                bsoftvd.InventarioClave = i.InventarioClave;
                                bsoftvd.Cantidad = x.Cantidad;
                                dc.BitacoraSofTVDetalle.Add(bsoftvd);
                                dc.SaveChanges();

                                i.Cantidad = i.Cantidad - x.Cantidad;
                                dc.SaveChanges();
                            }


                        }




                    };


                    dbContextTransaction.Commit();
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {

                    dbContextTransaction.Rollback();
                    error e1 = new error();
                    e1.idError = -1;
                    e1.Mensaje = ex.InnerException.Message;
                    return Json(e1, JsonRequestBehavior.AllowGet);
                }

                
            }
        }


        public class DetalleBitacora
        {
        public Guid Clave { get; set; }
        public long IdInventario { get; set; }
        public string Serie { get; set; }
         public string Articulo { get; set; }
        public int Cantidad { get; set; }
        }


        public ActionResult CancelaBitacora(long bitacoraClave)
        {
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Usuario u = ((Usuario)Session["u"]);
                    BitacoraSofTV bs = dc.BitacoraSofTV.Where(x => x.BitacoraSofTVClave == bitacoraClave).First();
                    Entidad tecnico = dc.Entidad.Where(x=>x.EntidadClave==bs.EntidadClaveTecnico).First();
                    List<BitacoraSofTVDetalle> bsd = dc.BitacoraSofTVDetalle.Where(x=>x.BitacoraSofTVClave==bs.BitacoraSofTVClave).ToList();
                    bs.TipoServicio = "C";
                    bs.NombreUsuario = "Cancelado por usuario: " + u.Nombre +" Fecha: "+DateTime.Now;

                    foreach (var detalle in bsd)
                    {
                        if (tieneSeries(detalle.Inventario.ArticuloClave.Value))
                        {
                            Inventario i = dc.Inventario
                                .Where(x=>x.InventarioClave==detalle.InventarioClave  && x.EstatusInv==8 && x.Cantidad==1 && x.EntidadClave==null).First();
                            i.EstatusInv = 7;
                            i.EntidadClave = tecnico.EntidadClave;
                            dc.SaveChanges();

                        }else{

                            Inventario i = dc.Inventario
                               .Where(x => x.InventarioClave == detalle.InventarioClave && x.EstatusInv == 7  && x.EntidadClave == tecnico.EntidadClave).First();

                            i.Cantidad = i.Cantidad + detalle.Cantidad;
                            dc.SaveChanges();

                        }

                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json("1", JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();

                    error e1 = new error();
                    e1.idError = -1;
                    e1.Mensaje = ex.InnerException.Message;
                    return Json(e1, JsonRequestBehavior.AllowGet);
                   
                }
            }

        } 


       
        public ActionResult ObtenListaBitacora(string data, int draw, int start, int length)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            string contrato;
            try
            {
                contrato = jObject["pag"].ToString();
            }
            catch
            {
                contrato = "";
            }

            int vnEstatus = 0;
            Guid pedido = Guid.Empty,
                vgAlmacen = Guid.Empty; try
            {
                pedido = Guid.Parse(jObject["pedido"].ToString());
            }
            catch { }

            try
            {
                vgAlmacen = Guid.Parse(jObject["almacen"].ToString());
            }
            catch { }

            try
            {
                vnEstatus = int.Parse(jObject["estatus"].ToString());
            }
            catch { }

            Guid tecnico = Guid.Empty;

            try
            {
                tecnico = Guid.Parse(jObject["tecnico"].ToString());
            }
            catch { }


            Guid plaza = Guid.Empty;
            try
            {
                plaza = Guid.Parse(jObject["plaza"].ToString());
            }
            catch { }

            string tipobitacora = String.Empty;
            try
            {
                tipobitacora = jObject["tipobitacora"].ToString();
            }
            catch { }


            int tipoentidad = 2;

            try
            {
                tipoentidad = Convert.ToInt32(jObject["tipoentidad"].ToString());
            }
            catch
            {

            }


            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = FilterData2(ref recordsFiltered, start, length, pedido, vgAlmacen, vnEstatus, tecnico, plaza, tipobitacora, tipoentidad, contrato);
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);



        }

        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<SalidaMaterialWrapper> data { get; set; }
        }

        private List<SalidaMaterialWrapper> FilterData2(ref int recordFiltered, int start, int length, Guid pedido, Guid vgAlmacen, int vnEstatus, Guid tecnico, Guid plaza, string tipobitacora, int tipoentidad, string contrato)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Usuario u = (Usuario)Session["u"];
            List<BitacoraSofTV> rls = null;
            List<BitacoraSofTV> rls2 = null;
            if (u.DistribuidorClave == null)
            {
                rls = (from use in dc.BitacoraSofTV
                       where
                       (tecnico == Guid.Empty || tecnico == use.EntidadClaveTecnico)
                       && use.catDistribuidorClave == null
                       && (plaza == Guid.Empty || use.EntidadClaveAlmacen == plaza)
                       && (use.TipoServicio == "A" || use.TipoServicio == "C")
                       orderby use.Fecha ascending
                       select use).ToList();

                recordFiltered = rls.Count();
            }
            else
            {

                if (contrato != "")
                {
                    rls = (from use in dc.BitacoraSofTV where use.Contrato == contrato select use).ToList();

                }
                else
                {
                    rls = (from use in dc.BitacoraSofTV
                           where
                           (tecnico == Guid.Empty || tecnico == use.EntidadClaveTecnico)
                           && use.catDistribuidorClave == u.DistribuidorClave
                           && (plaza == Guid.Empty || use.EntidadClaveAlmacen == plaza)
                            && (use.TipoServicio == "A" || use.TipoServicio == "C")
                           orderby use.Fecha ascending
                           select use).ToList();
                    recordFiltered = rls.Count();
                }

            }
            List<SalidaMaterialWrapper> rw = new List<SalidaMaterialWrapper>();

            foreach (BitacoraSofTV p in rls.Skip(start).Take(length).ToList())
            {
                SalidaMaterialWrapper w = SalidaMaterialBitacoraToWrapper(p, false);
                rw.Add(w);
            }

            return rw;


        }



        private SalidaMaterialWrapper SalidaMaterialBitacoraToWrapper(BitacoraSofTV u, bool arts)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            SalidaMaterialWrapper rw = new SalidaMaterialWrapper();
            rw.Clave = u.BitacoraSofTVClave.ToString();
            rw.EntidadClaveSolicitante = null;
            rw.EntidadClaveAfectada = u.EntidadClaveTecnico.Value.ToString();
            rw.NumeroPedido = u.BitacoraSofTVClave.ToString();
            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");
            try
            {
                rw.BitacoraLetra = u.TipoServicio.ToString();
                rw.BitacoraNumero = u.NumeroOrden.ToString();
                if (rw.BitacoraNumero == "")
                {
                    rw.BitacoraNumero = u.Contrato;
                }

                rw.Plaza = u.EntidadClaveAlmacen.ToString();
            }
            catch
            {
            }
            Entidad tec = (from ea in dc.Entidad
                           where ea.EntidadClave == u.EntidadClaveTecnico
                           select ea).SingleOrDefault();

            if (tec != null)
            {
                rw.Destino = tec.Nombre;
            }

            return rw;
        }







    }
}
