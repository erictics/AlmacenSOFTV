﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Controllers;
using web.Models;

namespace Web.Controllers
{
    public class OrdenCompraController : Controller
    {
        // GET: /OrdenCompra/

        ModeloAlmacen dc = new ModeloAlmacen();

        class PedidoPagoWP
        {
            public string Forma { get; set; }
            public string Banco { get; set; }
            public string FechaPago { get; set; }
            public string Monto { get; set; }
            public string MontoUSD { get; set; }
            public string Usuario { get; set; }
            public string NumeroCuenta { get; set; }
        }

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {

            List<catProveedor> proveedores = (from ta in dc.catProveedor
                                              where ta.Activo
                                              orderby ta.Nombre
                                              select ta).ToList();
            Usuario u = (Usuario)Session["u"];


            ViewData["proveedores"] = proveedores;

            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        [SessionExpireFilterAttribute]
        public ActionResult Pagos()
        {

            Usuario u = (Usuario)Session["u"];

            List<catDistribuidor> distribuidores = (from ta in dc.catDistribuidor
                                                    where ta.Activo
                                                    orderby ta.Nombre
                                                    select ta).ToList();

            ViewData["distribuidores"] = distribuidores;

            List<catBanco> bancos = (from ta in dc.catBanco
                                     where ta.Activo.Value
                                     orderby ta.Descripcion
                                     select ta).ToList();
            ;
            List<CatTipoPago> listapago = dc.CatTipoPago.Where(o => o.Activo == true).ToList();

            ViewData["bancos"] = bancos;
            ViewData["pagos"] = listapago;
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }




        public ActionResult Pago(string data)
        {

            PedidoPago vPedPag = new PedidoPago();
            PedidoPagoWP vPedPagWP = new PedidoPagoWP();

            try
            {
                JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                Guid vProceso = Guid.Parse(jObject["proceso"].ToString());

                vPedPag = (from pp in dc.PedidoPago
                           where pp.ProcesoClave == vProceso
                           select pp).SingleOrDefault();



                if (vPedPag != null)
                {
                    vPedPagWP.NumeroCuenta = vPedPag.NumeroCuenta;
                    vPedPagWP.Forma = vPedPag.Forma;
                    vPedPagWP.Banco = vPedPag.Banco;
                    vPedPagWP.FechaPago = vPedPag.FechaPago.GetValueOrDefault().ToString("dd/MM/yyyy");
                    vPedPagWP.Monto = vPedPag.Monto.GetValueOrDefault().ToString();
                    vPedPagWP.MontoUSD = vPedPag.MontoUSD.GetValueOrDefault().ToString();
                    vPedPagWP.Usuario = (from u in dc.Usuario
                                         where u.UsuarioClave == vPedPag.UsuarioClave
                                         select u.Nombre).SingleOrDefault();
                }
            }
            catch { }

            return Json(vPedPagWP, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilterAttribute]
        public ActionResult Autoriza(string data)
        {

            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                    string comentarios = String.Empty;
                    try
                    {
                        comentarios = jObject["comentario"].ToString();
                    }
                    catch { }
                    string accion = jObject["accion"].ToString();
                    Guid clave = Guid.Parse(jObject["clave"].ToString());




                    Proceso pro = (from use in dc.Proceso
                                   where use.ProcesoClave == clave
                                   select use).SingleOrDefault();

                    if (pro.catEstatusClave != 4 || pro.catEstatusClave != 6)
                    {
                        if (accion == "autoriza")
                        {
                            pro.catEstatusClave = 2;
                        }
                        else if (accion == "rechaza")
                        {
                            pro.catEstatusClave = 12;
                        }
                    }

                    pro.Observaciones = comentarios;

                    if (accion == "autoriza" || accion == "edita")
                    {
                        try
                        {
                            Decimal montopesos = Convert.ToDecimal(jObject["montopesos"].ToString());
                            Decimal montodolares = Convert.ToDecimal(jObject["montodolares"].ToString());
                            string formapago = jObject["formapago"].ToString();
                            string banco = jObject["banco"].ToString();
                            string cuenta = "";
                            try
                            {
                                cuenta = jObject["cuenta"].ToString();
                            }
                            catch
                            {
                            }
                            DateTime fechapago = new DateTime(Convert.ToInt32(jObject["fechapago"].ToString().Split('/')[2]),
                                Convert.ToInt32(jObject["fechapago"].ToString().Split('/')[1]),
                                Convert.ToInt32(jObject["fechapago"].ToString().Split('/')[0]));

                            PedidoPago pepa = (from p in dc.PedidoPago
                                               where p.ProcesoClave == clave
                                               select p).SingleOrDefault();

                            if (pepa == null)
                            {
                                pepa = new PedidoPago();

                                pepa.ProcesoClave = clave;
                                pepa.Monto = montopesos;
                                pepa.MontoUSD = montodolares;
                                pepa.Forma = formapago;
                                pepa.Banco = banco;
                                pepa.FechaPago = fechapago;
                                pepa.Fecha = DateTime.Now;
                                pepa.UsuarioClave = ((Usuario)Session["u"]).UsuarioClave;
                                pepa.NumeroCuenta = cuenta;

                                dc.PedidoPago.Add(pepa);
                            }
                            else
                            {
                                pepa.ProcesoClave = clave;
                                pepa.Monto = montopesos;
                                pepa.MontoUSD = montodolares;
                                pepa.Forma = formapago;
                                pepa.Banco = banco;
                                pepa.FechaPago = fechapago;
                                pepa.Fecha = DateTime.Now;
                                pepa.NumeroCuenta = cuenta;
                                pepa.UsuarioClave = ((Usuario)Session["u"]).UsuarioClave;
                            }
                        }
                        catch { }
                    }
                    else if (accion == "rechaza")
                    {
                        dc.PedidoPago.RemoveRange(dc.PedidoPago.Where(pp => pp.ProcesoClave == clave));
                    }

                    dc.SaveChanges();
                    dbContextTransaction.Commit();

                    return Content("1");
                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }
        }
    }
}
