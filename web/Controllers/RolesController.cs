﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class RolesController : Controller
    {
        //
        // GET: /Roles/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            int pags = (from ro in dc.Rol
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div - 1).ToString());
            }
            else
            {
                return Content((div).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<Rol> rls = (from use in dc.Rol
                                 where use.Activo == true
                                    orderby use.Nombre
                                    select use).Skip(skip).Take(elementos).ToList();

            List<RolWrapper> rw = new List<RolWrapper>();

            foreach (Rol u in rls)
            {
                RolWrapper w = RolToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Obten(string data)
        {
            RolWrapper rw = new RolWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            Rol r = (from ro in dc.Rol
                             where ro.RolClave  == uid
                             && ro.Activo == true
                             select ro).SingleOrDefault();

            if (r != null)
            {
                rw = RolToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    Rol r = (from ro in dc.Rol
                         where ro.RolClave == uid
                         select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guarda(string ed_id, string ed_nombre)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                Rol r = new Rol();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.Rol
                         where ro.RolClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Nombre = ed_nombre;
                r.Activo = true;

                if (ed_id == String.Empty)
                {
                    dc.Rol.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch(Exception ex)
            {
                return Content("0");
            }

        }

        private RolWrapper RolToWrapper(Rol u)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            List<Usuario> usuarios = (from a in dc.Usuario
                                          where a.RolClave
                                              == u.RolClave
                                          select a).ToList();
            bool us=true;
            if (usuarios.Count == 0)
            {
                us=false;
            }

            RolWrapper rw = new RolWrapper();
            rw.Id = u.RolClave.ToString();
            rw.Nombre = u.Nombre;
            rw.tieneusuarios = us;
            return rw;
        }


    }
}
