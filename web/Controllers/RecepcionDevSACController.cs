﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;


namespace web.Controllers
{
    public class RecepcionDevSACController : Controller
    {
        //
        // GET: /RecepcionDevSAC/

        ModeloAlmacen dc = new ModeloAlmacen();
       
        public ActionResult Index()
        {
          
          Usuario u = ((Usuario)Session["u"]);
            List<Entidad> almacenes=(from a in dc.Entidad where a.catEntidadTipoClave==1 && a.catDistribuidorClave==null select a).ToList();
            ViewData["almacenes"] = almacenes;
            ViewData["RolUsuario"] = u.RolClave;
            return View();         
        }

        public class Devolucion
        {
            public long pedido { get; set; }
            public string fecha { get; set; }
            public string almacen { get; set; }
            public string Destino { get; set; }
            public int estatus { get; set; }
            public Guid proceso { get; set; }

            public string Recepcion { get; set; }

        }

        public class lista_dev
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<Devolucion> data { get; set; }
        }

        public ActionResult ListaDevoluciones(string data, string almacen, int orden, int draw, int start, int length)
        {
            lista_dev dataTableData = new lista_dev();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = Obtenerdevoluciones(ref recordsFiltered,almacen, orden, start, length);
            dataTableData.recordsFiltered = recordsFiltered;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<Devolucion> Obtenerdevoluciones(ref int recordFiltered,string almacen, int orden, int start, int length)
        {
            
            Usuario u = ((Usuario)Session["u"]);
            List<Devolucion> lista = new List<Devolucion>();
            List<Proceso> dev;
            List<Proceso> aux;
            Guid alm;
            try
            {
                alm = Guid.Parse(almacen);               
            }
            catch
            {
                alm = Guid.Empty;
            }

            dev = (from a in dc.Proceso
                   //join b in dc.AmbitoAlmacen on a.EntidadClaveAfectada equals b.EntidadClaveAlmacen
                   where a.catTipoProcesoClave == 5 && a.catEstatusClave != 6 && (orden == 0 || (a.NumeroPedido == orden)) && (alm == Guid.Empty || (alm == a.EntidadClaveAfectada)) 
                  // && b.UsuarioClave == u.UsuarioClave
                   orderby a.NumeroPedido descending 
                   select a ).ToList();
            aux = dev;
            recordFiltered = aux.Count;
            foreach (var a in dev.Skip(start).Take(length).ToList())
            { 
                Devolucion d = new Devolucion();
                Entidad ent = dc.Entidad.Where(x => x.EntidadClave == a.EntidadClaveSolicitante).First();
                Entidad entdes = dc.Entidad.Where(x => x.EntidadClave == a.EntidadClaveAfectada).First();
                d.almacen = ent.Nombre;
                d.Destino = entdes.Nombre;
                d.pedido = a.NumeroPedido;
                d.estatus = a.catEstatusClave.Value;
                d.fecha = a.Fecha.ToString();
                d.proceso = a.ProcesoClave;
                try
                {
                    d.Recepcion = dc.Proceso.Where(p => p.ProcesoClaveRespuesta == a.ProcesoClave).Select(y => y.ProcesoClave).First().ToString();
                }
                catch
                {
                    d.Recepcion = "";
                }
                lista.Add(d);
            }
            return lista.OrderByDescending(x => x.pedido).ToList();
        }

        public class detalledev
        {
            public Guid ClaveArticulo { get; set; }
            public int Cantidad { get; set; }
            public bool tieneseries { get; set; }

            public string articulo { get; set; }

        }



        public ActionResult Detalledevolucion(Guid devolucion)
        {
            
            List<detalledev> lista = new List<detalledev>();
            Proceso proceso = dc.Proceso.Where(o => o.ProcesoClave == devolucion && o.catTipoProcesoClave == 5).First();
            List<ProcesoArticulo> pa = dc.ProcesoArticulo.Where(x => x.ProcesoClave == proceso.ProcesoClave).ToList();
            foreach (var a in pa)
            {
                detalledev d = new detalledev();
                d.Cantidad = a.Cantidad.Value;
                d.ClaveArticulo = a.ArticuloClave;
                d.articulo = a.Articulo.Nombre;
                d.tieneseries = tieneSeries(a.ArticuloClave);
                lista.Add(d);
            }
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public bool tieneSeries(Guid artclave)
        {
            
            Articulo art = dc.Articulo.Where(x => x.ArticuloClave == artclave).First();
            if (art.catTipoArticulo.catSerieClaveInterface == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public class DetSerie
        {
            public string valor { get; set; }
            public string estatus { get; set; }
            public string Nombre { get; set; }
            public int clv_estatus { get; set; }
        }

        public ActionResult detalleSeries(Guid devolucion, Guid articulo)
        {
           
            List<DetSerie> lista = new List<DetSerie>();
            List<ProcesoInventario> pi = dc.ProcesoInventario.Where(x => x.ProcesoClave == devolucion && x.Inventario.ArticuloClave == articulo).ToList();
            foreach (var a in pi)
            {

                DetSerie s = new DetSerie();
                s.valor = a.Inventario.Serie.Select(x => x.Valor).First();
                s.clv_estatus = a.Inventario.EstatusInv.Value;
                string estatus = dc.catEstatus.Where(x => x.catEstatusClave == a.Inventario.EstatusInv.Value).Select(o => o.Nombre).First().ToString();
                s.Nombre = a.Inventario.Articulo.Nombre;
                s.estatus = estatus;
                lista.Add(s);

            }
            return Json(lista.OrderBy(o => o.valor), JsonRequestBehavior.AllowGet);

        }


        public ActionResult guarda(Guid devolucion)
        {
            
            using (var dbContextTransaction = dc.Database.BeginTransaction())
            {
                try
                {
                    Proceso devo = dc.Proceso.Where(x => x.ProcesoClave == devolucion).First();
                    if (devo.catEstatusClave == 6)
                    {
                        dbContextTransaction.Rollback();
                        return Content("-2");
                    }


                    devo.catEstatusClave = 4;

                    Proceso p = new Proceso();
                    p.catEstatusClave = 4;
                    p.catTipoProcesoClave = 6;
                    p.ProcesoClave = Guid.NewGuid();
                    p.ProcesoClaveRespuesta = devo.ProcesoClave;
                    p.EntidadClaveSolicitante = devo.EntidadClaveAfectada;
                    p.EntidadClaveAfectada = devo.EntidadClaveSolicitante;
                    p.catDistribuidorClave = devo.catDistribuidorClave;
                    p.Fecha = DateTime.Now;
                    Usuario u = (Usuario)Session["u"];
                    p.UsuarioClave = u.UsuarioClave;
                    dc.Proceso.Add(p);


                    List<ProcesoArticulo> pad = dc.ProcesoArticulo.Where(x => x.ProcesoClave == devo.ProcesoClave).ToList();
                    List<ProcesoInventario> pid = dc.ProcesoInventario.Where(x => x.ProcesoClave == devo.ProcesoClave).ToList();
                    foreach (var a in pad)
                    {
                        ProcesoArticulo pa = new ProcesoArticulo();
                        pa.ArticuloClave = a.ArticuloClave;
                        pa.ProcesoClave = p.ProcesoClave;
                        pa.Cantidad = a.Cantidad;
                        dc.ProcesoArticulo.Add(pa);
                    }
                    foreach (var b in pid)
                    {
                        ProcesoInventario pi = new ProcesoInventario();
                        pi.ProcesoClave = p.ProcesoClave;
                        pi.InventarioClave = b.InventarioClave;
                        pi.Cantidad = b.Cantidad;
                        pi.Fecha = DateTime.Now;
                        dc.ProcesoInventario.Add(pi);
                        Guid alm = devo.EntidadClaveAfectada.Value;
                        Inventario inv = dc.Inventario.Where(x => x.InventarioClave == b.InventarioClave).First();
                        if (tieneSeries(b.Inventario.ArticuloClave.Value))
                        {
                            if (inv.Standby == 0 && inv.Cantidad == 1 && inv.EntidadClave == alm)
                            {
                                dbContextTransaction.Rollback();
                                return Content("-1");
                            }
                            inv.Cantidad = 1;
                            inv.EntidadClave = alm;
                            inv.Standby = 0;
                            inv.Estatus = 1;
                            inv.EstatusInv = 9;                            
                           
                        }
                        else
                        {
                            try
                            {
                                Inventario invalm = dc.Inventario.Where(x => x.EntidadClave == alm && x.ArticuloClave == b.Inventario.ArticuloClave && x.EstatusInv == 21).First();
                                //rebajando del standby 

                                invalm.Cantidad = invalm.Cantidad.Value + b.Cantidad;
                            }
                            catch
                            {
                                Inventario i = new Inventario();
                                i.InventarioClave = Guid.NewGuid();
                                i.ArticuloClave = b.Inventario.ArticuloClave;
                                i.EntidadClave = alm;
                                i.Cantidad = b.Cantidad;
                                i.Standby = 0;
                                i.Estatus = 1;
                                i.EstatusInv = 9;
                                dc.Inventario.Add(i);

                                pi.InventarioClave = i.InventarioClave;
                                //rebajando del standby 



                            }
                            inv.Standby = inv.Standby.Value - b.Cantidad;

                        }


                        dc.SaveChanges();
                    }
                    dc.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(p.ProcesoClave.ToString(), JsonRequestBehavior.AllowGet);

                }
                catch
                {
                    dbContextTransaction.Rollback();
                    return Content("0");

                }
            }
        }

    }
}
