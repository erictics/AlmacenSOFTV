﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class BancoController : Controller
    {
        //
        // GET: /Banco/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
            
        }

        int elementos = 15;


        public class DataTableData
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<catBancoWrapper> data { get; set; }
        }



        public ActionResult ObtenLista(string data, int draw, int start, int length)
        {

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = 0;
            int recordsFiltered = 0;
            dataTableData.data = ListaBanco(data).OrderByDescending(x => x.Descripcion).Skip(start).Take(length).ToList();
            dataTableData.recordsFiltered = ListaBanco(data).Count;
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public List<catBancoWrapper> ListaBanco(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            List<catBanco> rls = (from use in dc.catBanco
                                                      orderby use.Descripcion
                                                      select use).ToList();

            List<catBancoWrapper> rw = new List<catBancoWrapper>();

            foreach (catBanco u in rls)
            {
                catBancoWrapper w = BancoToWrapper(u);
                rw.Add(w);
            }
            return rw;
        }


        public ActionResult Obten(string data)
        {
            catBancoWrapper rw = new catBancoWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catBanco r = (from ro in dc.catBanco
                                   where ro.catBancoClave == uid
                                              select ro).SingleOrDefault();

            if (r != null)
            {
                rw = BancoToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catBanco r = (from ro in dc.catBanco
                                           where ro.catBancoClave == uid
                                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catBanco r = (from ro in dc.catBanco
                                           where ro.catBancoClave == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_activa)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catBanco r = new catBanco();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catBanco
                         where ro.catBancoClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {

                }

                r.Descripcion = ed_nombre;
                r.Activo = (ed_activa == "on") ? true : false;

                if (ed_id == String.Empty)
                {
                    dc.catBanco.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }


        private catBancoWrapper BancoToWrapper(catBanco u)
        {
            catBancoWrapper rw = new catBancoWrapper();
            rw.Clave = u.catBancoClave.ToString();
            rw.Descripcion = u.Descripcion;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }

    }
}
