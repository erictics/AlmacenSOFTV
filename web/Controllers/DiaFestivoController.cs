﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Wrappers;

namespace Web.Controllers
{
    public class DiaFestivoController : Controller
    {
        //
        // GET: /DiaFestivo/

        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            Usuario u = ((Usuario)Session["u"]);
            ViewData["RolUsuario"] = u.RolClave;
            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            ModeloAlmacen dc = new ModeloAlmacen();

            int pags = (from ro in dc.catDiaFestivo
                        select ro).Count();
            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div).ToString());
            }
            else
            {
                return Content((div+1).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            int skip = (pag - 1) * elementos;

            ModeloAlmacen dc = new ModeloAlmacen();

            List<catDiaFestivo> rls = (from use in dc.catDiaFestivo
                                 orderby use.Descripcion
                                 select use).Skip(skip).Take(elementos).ToList();

            List<catDiasFestivosWrapper> rw = new List<catDiasFestivosWrapper>();

            foreach (catDiaFestivo u in rls)
            {
                catDiasFestivosWrapper w = DiaFestivoToWrapper(u);
                rw.Add(w);
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Obten(string data)
        {
            catDiasFestivosWrapper rw = new catDiasFestivosWrapper();

            ModeloAlmacen dc = new ModeloAlmacen();

            int uid = Convert.ToInt32(data);

            catDiaFestivo r = (from ro in dc.catDiaFestivo
                         where ro.catDiaFestivoClave == uid
                         select ro).SingleOrDefault();

            if (r != null)
            {
                rw = DiaFestivoToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Elimina(string del_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (del_id != String.Empty)
                {
                    int uid = Convert.ToInt32(del_id);

                    catDiaFestivo r = (from ro in dc.catDiaFestivo
                                 where ro.catDiaFestivoClave  == uid
                                 select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Activa(string activa_id)
        {
            try
            {
                ModeloAlmacen dc = new ModeloAlmacen();

                if (activa_id != String.Empty)
                {
                    int uid = Convert.ToInt32(activa_id);

                    catDiaFestivo r = (from ro in dc.catDiaFestivo
                                           where ro.catDiaFestivoClave == uid
                                           select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = true;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }

        public ActionResult Guarda(string ed_id, string ed_nombre, string ed_fecha, string ed_activa)
        {

            try
            {

                ModeloAlmacen dc = new ModeloAlmacen();

                catDiaFestivo r = new catDiaFestivo();

                if (ed_id != String.Empty)
                {
                    int uid = Convert.ToInt32(ed_id);

                    r = (from ro in dc.catDiaFestivo
                         where ro.catDiaFestivoClave == uid
                         select ro).SingleOrDefault();
                }
                else
                {
                    DateTime uid = Convert.ToDateTime(ed_fecha);

                    catDiaFestivo fecha = (from ro in dc.catDiaFestivo
                                               where ro.Fecha == uid
                                               select ro).SingleOrDefault();

                    if (fecha != null)
                    {
                        if (fecha.Activo)
                        {
                            return Content("-1");
                        }
                        else
                        {
                            r = fecha;
                            ed_id = r.Fecha.ToString("dd/MM/yyyy");
                        }
                    }

                }

                r.Descripcion = ed_nombre;
                r.Fecha = Convert.ToDateTime(ed_fecha);
                r.Activo = (ed_activa == "on") ? true : false;

                if (ed_id == String.Empty)
                {


                    dc.catDiaFestivo.Add(r);
                }
                dc.SaveChanges();
                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("0");
            }

        }

        private catDiasFestivosWrapper DiaFestivoToWrapper(catDiaFestivo u)
        {
            catDiasFestivosWrapper rw = new catDiasFestivosWrapper();
            rw.Clave = u.catDiaFestivoClave.ToString();
            rw.Fecha = u.Fecha.ToString("dd/MM/yyyy");
            rw.Descripcion = u.Descripcion;
            rw.Activo = Convert.ToInt32(u.Activo).ToString();
            return rw;
        }

    }
}



