﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using web.Utils;
using web.Wrappers;

namespace Web.Controllers
{
    public class PedidoController : Controller
    {
        //
        // GET: /Articulo/
        
        [Compress]
        [SessionExpireFilterAttribute]
        public ActionResult Lista()
        {
            dal.ModeloEntities dc = new dal.ModeloEntities();


            dal.Usuario u = ((dal.Usuario)Session["u"]);


            List<dal.catClasificacionArticulo> clasificaciones = (from ta in dc.catClasificacionArticulo
                                                                  where ta.Activo
                                                      orderby ta.Descripcion
                                                      select ta).ToList();

            List<dal.catTipoArticulo> tiposarticulo = (from ta in dc.catTipoArticulo
                                                       where ta.Activo
                                                       orderby ta.Descripcion
                                                       select ta).ToList();


            List<dal.catProveedor> proveedores = (from ta in dc.catProveedor
                                                  where ta.Activo
                                                      orderby ta.Nombre
                                                      select ta).ToList();


            List<dal.catTipoMoneda> tiposmoneda = (from ta in dc.catTipoMoneda
                                                   where ta.Activo
                                                  orderby ta.Descripcion
                                                  select ta).ToList();

            List<dal.catUnidad> unidades = (from ta in dc.catUnidad
                                            where ta.Activo
                                                                  orderby ta.Nombre
                                                                  select ta).ToList();

            List<dal.catUbicacion> ubicaciones = (from ta in dc.catUbicacion
                                                  where ta.Activo
                                                   orderby ta.Nombre
                                                   select ta).ToList();


            List<dal.AmbitoAlmacen> ambito = (from a in dc.AmbitoAlmacen
                                        where a.UsuarioClave == u.UsuarioClave
                                        select a).ToList();

            List<dal.Entidad> entidades = null;

            ViewData["usuarios"] = new List<dal.Usuario>();

            if (u.DistribuidorClave == null)
            {

                entidades = (from ta in dc.Entidad
                             where ta.catEntidadTipoClave == 1
                                 && ta.catDistribuidorClave == null
                                 && ta.Activo.Value
                             orderby ta.Nombre
                             select ta).ToList();

                string autorizador = Config.Dame("ROL_AUTORIZADOR");

                dal.Rol elrol = (from r in dc.Rol
                                 where r.Nombre == autorizador
                                 select r).SingleOrDefault();

                try
                {
                    List<dal.Usuario> usuarios = (from ta in dc.Usuario
                                                  where ta.DistribuidorClave == null
                                                  && ta.RolClave == elrol.RolClave
                                                  orderby ta.Nombre
                                                  select ta).ToList();
                    ViewData["usuarios"] = usuarios;
                }
                catch { }
            }
            else
            {
                entidades = new List<dal.Entidad>();

                foreach (dal.AmbitoAlmacen a in ambito)
                {
                    dal.Entidad e = a.Entidad1;
                    if (e.Activo.Value)
                    {
                        entidades.Add(e);
                    }
                }

                /*
                entidades = (from ta in dc.Entidad
                             where 
                                 ta.catDistribuidorClave == u.DistribuidorClave
                                 && ta.catEntidadTipoClave == 1
                                 && ta.Activo.Value
                             orderby ta.Nombre
                             select ta).ToList();       
                 */ 
            }




            ViewData["clasificaciones"] = clasificaciones;
            ViewData["tiposarticulo"] = tiposarticulo;
            ViewData["proveedores"] = proveedores;
            ViewData["tiposmoneda"] = tiposmoneda;
            ViewData["ubicaciones"] = ubicaciones;
            ViewData["unidades"] = unidades;

            ViewData["almacenes"] = entidades;


            string iva = Config.Dame("IVA");
            string tipocambio = Config.Dame("TIPOCAMBIO");

            ViewData["iva"] = iva;
            ViewData["tipocambio"] = tipocambio;

            return View();
        }

        int elementos = 15;

        public ActionResult ObtenPaginas(string data)
        {
            dal.ModeloEntities dc = new dal.ModeloEntities();

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            Guid autorizador = Guid.Empty;

            if (jObject["autoriza"] != null)
            {
                autorizador = ((dal.Usuario)Session["u"]).UsuarioClave;
            }

            Guid proveedor = Guid.Empty;
            if (jObject["proveedor"] != null && jObject["proveedor"].ToString() != "-1")
            {
                proveedor = Guid.Parse(jObject["proveedor"].ToString());
            }


            Guid distribuidor = Guid.Empty;
            if (jObject["distribuidor"] != null && jObject["distribuidor"].ToString() != "-1")
            {
                distribuidor = Guid.Parse(jObject["distribuidor"].ToString());
            }

            int estatus = -1;
            if (jObject["estatus"] != null)
            {
                estatus = Convert.ToInt32(jObject["estatus"].ToString());
            }

            int pedido = -1;
            if (jObject["pedido"] != null && jObject["pedido"].ToString() != String.Empty)
            {
                pedido = Convert.ToInt32(jObject["pedido"].ToString());
            }


            int pags = 0;


            dal.Usuario u = ((dal.Usuario)Session["u"]);

            if (u.DistribuidorClave == null)
            {
                pags = (from ro in dc.Proceso
                        where
                           ro.catDistribuidorClave == null
                           && ro.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == ro.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus == ro.catEstatusClave))
                          && (proveedor == Guid.Empty || (proveedor == ro.catProveedorClave))
                          && (distribuidor == Guid.Empty || (distribuidor == ro.catDistribuidorClave))
                          && (pedido == -1 || (ro.NumeroPedido == pedido))
                        select ro).Count();
            }
            else
            {
                pags = (from ro in dc.Proceso
                        where
                           ro.catProveedorClave == u.DistribuidorClave
                           && ro.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == ro.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus == ro.catEstatusClave))
                          && (pedido == -1 || (ro.NumeroPedido == pedido))
                          && (distribuidor == Guid.Empty || (distribuidor == ro.catDistribuidorClave))
                        select ro).Count();            
            }


            int div = pags / elementos;
            if (pags % elementos == 0)
            {
                return Content((div).ToString());
            }
            else
            {
                return Content((div+1).ToString());
            }
        }

        public ActionResult ObtenLista(string data)
        {

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            int pag = Convert.ToInt32(jObject["pag"].ToString());

            Guid autorizador = Guid.Empty;

            if (jObject["autoriza"] != null)
            {
                autorizador = ((dal.Usuario)Session["u"]).UsuarioClave;
            }

            Guid proveedor = Guid.Empty;
            if (jObject["proveedor"] != null && jObject["proveedor"].ToString() != "-1")
            {
                proveedor = Guid.Parse(jObject["proveedor"].ToString());
            }

            Guid distribuidor = Guid.Empty;
            bool pedidodistribuidor = false;
            if (jObject["distribuidor"] != null && jObject["distribuidor"] != null  && jObject["distribuidor"].ToString() != "-1")
            {
                distribuidor = Guid.Parse(jObject["distribuidor"].ToString());
                pedidodistribuidor = true;
            }
            else if (jObject["distribuidor"] != null && jObject["distribuidor"].ToString() == "-1")
            {
                pedidodistribuidor = true;
            }
            


            int estatus = -1;
            if (jObject["estatus"] != null)
            {
                estatus = Convert.ToInt32(jObject["estatus"].ToString());
            }

            int pedido = -1;
            if (jObject["pedido"] != null && jObject["pedido"].ToString()!=String.Empty)
            {
                pedido = Convert.ToInt32(jObject["pedido"].ToString());
            }


            int skip = (pag - 1) * elementos;

            dal.ModeloEntities dc = new dal.ModeloEntities();

            dal.Usuario u = ((dal.Usuario)Session["u"]);

            List<dal.Proceso> rls = null;

            if (u.DistribuidorClave == null)
            {
                rls = (from use in dc.Proceso
                       where
                          ((use.catDistribuidorClave == null && !pedidodistribuidor) || ((use.catDistribuidorClave != null && pedidodistribuidor)))
                          && use.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus == use.catEstatusClave))
                          && (proveedor == Guid.Empty || (proveedor == use.catProveedorClave))
                          && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                          && (pedido == -1 || (use.NumeroPedido == pedido))
                       orderby use.NumeroPedido descending
                       select use).Skip(skip).Take(elementos).ToList();
            }
            else
            {
                rls = (from use in dc.Proceso
                       where
                          use.catDistribuidorClave == u.DistribuidorClave
                          && use.catTipoProcesoClave == 1
                          && (autorizador == Guid.Empty || (autorizador == use.UsuarioClaveAutorizacion))
                          && (estatus == -1 || (estatus == use.catEstatusClave))
                          && (pedido == -1 || (use.NumeroPedido == pedido))
                          && (distribuidor == Guid.Empty || (distribuidor == use.catDistribuidorClave))
                       orderby use.Fecha descending
                       select use).Skip(skip).Take(elementos).ToList();            
            }

            List<PedidoWrapper> rw = new List<PedidoWrapper>();

            foreach (dal.Proceso p in rls)
            {
                if (p.PedidoDetalle != null)
                {
                    PedidoWrapper w = PedidoToWrapper(p);
                    rw.Add(w);
                }
            }

            return Json(rw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenUI(string data)
        {
            PedidoWrapperUI rw = new PedidoWrapperUI();

            dal.ModeloEntities dc = new dal.ModeloEntities();

            Guid uid = Guid.Parse(data);

            dal.Proceso r = (from ro in dc.Proceso
                             where ro.ProcesoClave == uid
                             select ro).SingleOrDefault();

            if (r != null)
            {
                rw = PedidoToUI(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Obten(string data)
        {
            PedidoWrapper rw = new PedidoWrapper();

            dal.ModeloEntities dc = new dal.ModeloEntities();

            Guid uid = Guid.Parse(data);

            dal.Proceso r = (from ro in dc.Proceso
                                              where ro.ProcesoClave  == uid
                                              select ro).SingleOrDefault();

            if (r != null)
            {
                rw = PedidoToWrapper(r);
                return Json(rw, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        
        [Compress]
        public ActionResult Articulos(string data)
        {
            PedidoWrapper rw = new PedidoWrapper();

            dal.ModeloEntities dc = new dal.ModeloEntities();

            JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
            dal.Proceso r = null;

            int recepcion = -1;

            try
            {
                recepcion = Convert.ToInt32(jObject["recepcion"].ToString());
            }
            catch { }

            Guid vgRecepcion = Guid.Empty;

            try
            {
                vgRecepcion = Guid.Parse(jObject["recepcion"].ToString());
            }
            catch { }

            int pedido = 0;
            Guid pedidoguid = Guid.Empty;

            try
            {
                pedido = Convert.ToInt32(jObject["pedido"].ToString());

                r = (from ro in dc.Proceso
                     where ro.NumeroPedido == pedido
                     select ro).SingleOrDefault();
            }
            catch
            {
                pedidoguid = Guid.Parse(jObject["pedido"].ToString());

                r = (from ro in dc.Proceso
                     where ro.ProcesoClave == pedidoguid
                     select ro).SingleOrDefault();
            }

            //if(recepcion.Equals(-1))
            //{
            //    if (!vgRecepcion.Equals(Guid.Empty))
            //    {
            //        r = (from ro in dc.Proceso
            //             where ro.ProcesoClave == vgRecepcion
            //             && (ro.ProcesoClaveRespuesta == pedidoguid || pedidoguid == Guid.Empty)
            //             select ro).SingleOrDefault();
            //    }
            //}


            if (r != null)
            {
                if (r.catEstatusClave == 1)
                {
                    return Json("-2: No autorizada", JsonRequestBehavior.AllowGet);
                }

                List<dal.ProcesoArticulo> articulos = r.ProcesoArticulo.ToList();

                List<ArticuloProcesoWrapper> apws = new List<ArticuloProcesoWrapper>();

                foreach (dal.ProcesoArticulo a in articulos)
                {
                    ArticuloProcesoWrapper apw = PedidoToarticuloProcesoWrapper(a, vgRecepcion);
                    apws.Add(apw);
                }


                return Json(apws, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("-3", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Cancela(string cancela_id)
        {
            try
            {
                dal.ModeloEntities dc = new dal.ModeloEntities();

                if (cancela_id != String.Empty)
                {
                    Guid uid = Guid.Parse(cancela_id);

                    dal.Proceso r = (from ro in dc.Proceso
                                     where ro.ProcesoClave == uid
                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.catEstatusClave = 6;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Elimina(string del_id)
        {
            try
            {
                dal.ModeloEntities dc = new dal.ModeloEntities();

                if (del_id != String.Empty)
                {
                    Guid uid = Guid.Parse(del_id);

                    dal.Articulo r = (from ro in dc.Articulo
                                                      where ro.ArticuloClave == uid
                                                      select ro).SingleOrDefault();

                    if (r != null)
                    {
                        r.Activo = false;
                        dc.SaveChanges();
                        return Content("1");
                    }

                }

                return Content("0");
            }
            catch
            {
                return Content("0");
            }
        }


        public ActionResult Guarda(string data)
        {
            try
            {
                JObject jObject = (JObject)JsonConvert.DeserializeObject(data);
                string ed_id = String.Empty;

                if (jObject["ProcesoClave"] != null)
                {
                    ed_id = jObject["ProcesoClave"].ToString();
                }

                dal.ModeloEntities dc = new dal.ModeloEntities();
                dal.Proceso r = new dal.Proceso();

                if (ed_id != String.Empty)
                {
                    Guid uid = Guid.Parse(ed_id);

                    r = (from ro in dc.Proceso
                         where ro.ProcesoClave == uid
                         select ro).SingleOrDefault();

                    List<dal.ProcesoArticulo> losarts = r.ProcesoArticulo.ToList();
                    foreach (dal.ProcesoArticulo pa in losarts)
                    {
                        dc.ProcesoArticulo.Remove(pa);
                    }

                    List<dal.PedidoGastos> losgastos = r.PedidoGastos.ToList();
                    foreach (dal.PedidoGastos pg in losgastos)
                    {
                        dc.PedidoGastos.Remove(pg);
                    }
                }
                else
                {
                    r.ProcesoClave = Guid.NewGuid();
                }

                
                r.catTipoProcesoClave = 1;
                r.EntidadClaveSolicitante = Guid.Parse(jObject["EntidadClaveSolicitante"].ToString());

                if (jObject["catProveedorClave"].ToString() == "-1" || jObject["catProveedorClave"].ToString() == "")
                {
                    r.EntidadClaveAfectada = Guid.Parse("00000000-0000-0000-0000-000000000001");
                    r.catProveedorClave = null;
                    r.catEstatusClave = 12;
                }
                else
                {
                    r.EntidadClaveAfectada = null;
                    r.catProveedorClave = Guid.Parse(jObject["catProveedorClave"].ToString());
                    r.catEstatusClave = 1;
                }

                try
                {
                    r.UsuarioClaveAutorizacion = Guid.Parse(jObject["UsuarioClaveAutorizacion"].ToString());
                }
                catch
                { 
                
                }


                try
                {
                    dal.Usuario usu = (dal.Usuario)Session["u"];
                    r.UsuarioClave = usu.UsuarioClave;
                }
                catch
                { 
                
                }


                r.Fecha = DateTime.Now;
                r.Observaciones = jObject["Observaciones"].ToString();

                Guid? distribuidor = ((dal.Usuario)Session["u"]).DistribuidorClave;
                r.catDistribuidorClave = distribuidor;

                if (ed_id == String.Empty)
                {
                    dc.Proceso.Add(r);
                }
                dc.SaveChanges();


                foreach (var item in ((JArray)jObject["Articulos"]).Children())
                {
                    dal.ProcesoArticulo procesoarticulo = new dal.ProcesoArticulo();
                        procesoarticulo.ProcesoClave = r.ProcesoClave;
                    var itemProperties = item.Children<JProperty>();
                    var articuloclave = itemProperties.FirstOrDefault(x => x.Name == "ArticuloClave");
                        procesoarticulo.ArticuloClave = Guid.Parse(articuloclave.Value.ToString());
                    var cantidad = itemProperties.FirstOrDefault(x => x.Name == "Cantidad");
                        procesoarticulo.Cantidad = Convert.ToInt32(cantidad.Value.ToString());

                    var cantidadentregada = itemProperties.FirstOrDefault(x => x.Name == "CantidadEntregada");
                        procesoarticulo.CantidadEntregada = Convert.ToInt32(cantidadentregada.Value.ToString());

                    var preciounitario = itemProperties.FirstOrDefault(x => x.Name == "PrecioUnitario");
                        procesoarticulo.PrecioUnitario = Convert.ToDecimal(preciounitario.Value.ToString());
                    var tipomoneda = itemProperties.FirstOrDefault(x => x.Name == "catTipoMonedaClave");
                        procesoarticulo.catTipoMonedaClave = Convert.ToInt32(tipomoneda.Value.ToString());
                    dc.ProcesoArticulo.Add(procesoarticulo);
                }
                dc.SaveChanges();

                dal.PedidoDetalle detalle = new dal.PedidoDetalle();
                detalle.ProcesoClave = r.ProcesoClave;
                detalle.IVA = Convert.ToDecimal(jObject["Iva"].ToString());
                detalle.Descuento = 0;
                try
                {
                    detalle.Descuento = Convert.ToDecimal(jObject["Descuento"].ToString());
                }
                catch
                { }


                detalle.TipoCambio = 0;

                try
                {
                    detalle.TipoCambio = Convert.ToDecimal(jObject["TipoCambio"].ToString());
                }
                catch
                { 
                }

                detalle.DiasCredito = 0;
                try
                {
                    detalle.DiasCredito = Convert.ToInt32(jObject["DiasCredito"].ToString());
                }
                catch
                { }

                detalle.Subtotal_Total = Convert.ToDecimal(jObject["ToSubtotal"].ToString());
                detalle.Descuento_Total = Convert.ToDecimal(jObject["ToDescuento"].ToString());
                detalle.IVA_Total = Convert.ToDecimal(jObject["ToIVA"].ToString());
                detalle.Total = Convert.ToDecimal(jObject["Total"].ToString());

                detalle.Subtotal_Total_USD = Convert.ToDecimal(jObject["ToSubtotal_usd"].ToString());
                detalle.Descuento_Total_USD = Convert.ToDecimal(jObject["ToDescuento_usd"].ToString());
                detalle.IVA_Total_USD = Convert.ToDecimal(jObject["ToIVA_usd"].ToString());
                detalle.Total_USD = Convert.ToDecimal(jObject["Total_usd"].ToString());


                if (ed_id != String.Empty)
                {
                }
                else
                {
                    dc.PedidoDetalle.Add(detalle);
                }

                dc.SaveChanges();

                foreach (var item in ((JArray)jObject["OtrosGastos"]).Children())
                {
                    try
                    {
                        dal.PedidoGastos pedidogasto = new dal.PedidoGastos();
                        pedidogasto.ProcesoClavePedido = r.ProcesoClave;
                        pedidogasto.GastoClave = Guid.NewGuid();

                        var itemProperties = item.Children<JProperty>();
                        var concepto = itemProperties.FirstOrDefault(x => x.Name == "Concepto");
                        pedidogasto.Concepto = concepto.Value.ToString();

                        var costo = itemProperties.FirstOrDefault(x => x.Name == "Precio");
                        pedidogasto.Costo = Convert.ToDecimal(costo.Value.ToString());

                        var moneda = itemProperties.FirstOrDefault(x => x.Name == "Moneda");
                        pedidogasto.catTipoMonedaClave = Convert.ToInt32(moneda.Value.ToString());

                        dc.PedidoGastos.Add(pedidogasto);
                    }
                    catch { 
                    }
                }

                dc.SaveChanges();
                return Content(r.ProcesoClave.ToString());
            }
            catch (Exception ex)
            {
                return Content("0");
            }
        }


        private ArticuloProcesoWrapper PedidoToarticuloProcesoWrapper(dal.ProcesoArticulo u, Guid pProcesoP)
        {

            dal.ModeloEntities dc = new dal.ModeloEntities();

            ArticuloProcesoWrapper ap = new ArticuloProcesoWrapper();
            ap.Clave = u.ProcesoClave + "|" + u.ArticuloClave;
            ap.ArticuloClave = u.ArticuloClave.ToString();
            ap.Nombre = u.Articulo.Nombre;
            ap.Cantidad = (u.Cantidad == null) ? 0 : u.Cantidad.Value;
            ap.PrecioUnitario = (u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value;
            ap.PrecioUnitarioTexto = ((u.PrecioUnitario == null) ? 0 : u.PrecioUnitario.Value).ToString("C");



            dal.Proceso Pedido = u.Proceso;
            ap.Surtida = 0;
            if (Pedido.catTipoProcesoClave==1)
            {

                List<dal.Proceso> salidas = (from s in dc.Proceso
                                             where s.ProcesoClaveRespuesta == Pedido.ProcesoClave
                                             && s.catTipoProcesoClave == 3
                                             select s).ToList();
                int cantidad = 0;
                foreach (dal.Proceso s in salidas)
                {
                    List<dal.ProcesoArticulo> proarts = s.ProcesoArticulo.ToList();

                    foreach (dal.ProcesoArticulo pa in proarts)
                    {
                        if (pa.ArticuloClave == u.ArticuloClave)
                        {
                            cantidad += pa.Cantidad.Value;
                        }
                    }

                }

                ap.Surtida = cantidad;
            }



            ap.catSeriesClaves = new List<catSerieWrapper>();

            List<dal.SerieTipoArticulo> tiposarticulo = u.Articulo.catTipoArticulo.SerieTipoArticulo.ToList();

            foreach (dal.SerieTipoArticulo sta in tiposarticulo)
            {
                catSerieWrapper csw = SerieToWrapper(sta.catSerie);
                ap.catSeriesClaves.Add(csw);
            }

            ap.TieneSeries = tiposarticulo.Count();

            ap.Seriales = new List<SerialesWrapper>();




            int cuantos = (from c in dc.Inventario
                           where c.ArticuloClave == u.ArticuloClave
                           && c.EntidadClave == Pedido.EntidadClaveAfectada
                           select c.Cantidad.Value).ToList().Sum();


            ap.Existencia = cuantos;


            List<dal.ProcesoInventario> proinv = (from i in dc.ProcesoInventario
                                                    where i.Pedido == u.ProcesoClave
                                                    && i.Inventario.ArticuloClave == u.ArticuloClave
                                                      select i).ToList();

            foreach(dal.ProcesoInventario pro in proinv)
            {
                dal.Inventario inventario = pro.Inventario;

                List<dal.Serie> series = inventario.Serie.ToList();


                SerialesWrapper sw = new SerialesWrapper();
                sw.RecepcionClave = pro.ProcesoClave.ToString();
                sw.InventarioClave = inventario.InventarioClave.ToString();
                sw.ArticuloClave = inventario.ArticuloClave.ToString();
                sw.EstatusTexto = "Buen Estado";
                sw.EstatusClave = inventario.Estatus.ToString();
                sw.Activo = 1;

                sw.datos = new List<SerialesDatosWrapper>();

                foreach(dal.Serie s in series)
                {
                    SerialesDatosWrapper sdw = new SerialesDatosWrapper();
                    sdw.Clave = s.catSerieClave.ToString();
                    sdw.Valor = s.Valor.ToString();
                    sw.datos.Add(sdw);
                }

                ap.Seriales.Add(sw);
            }


            if (ap.Surtida == 0)
            {
                try
                {
                    ap.Surtida = u.CantidadEntregada.Value;
                    ap.Surtiendo = 0;
                }
                catch
                {
                    ap.Surtida = 0;
                    ap.Surtiendo = 0;
                }
            }

            return ap;
        }

        private catSerieWrapper SerieToWrapper(dal.catSerie u)
        {
            catSerieWrapper rw = new catSerieWrapper();
            rw.Clave = u.catSerieClave.ToString();
            rw.Nombre = u.Nombre;
            rw.Hexadecimal = u.Hexadecimal.ToString();
            rw.Min = u.Min;
            rw.Max = u.Max;
            return rw;
        }


        private PedidoWrapperUI PedidoToUI(dal.Proceso u)
        {
            PedidoWrapperUI pwui = new PedidoWrapperUI();
            pwui.ProcesoClave = u.ProcesoClave.ToString();
            pwui.catProveedorClave = u.catProveedorClave.ToString();
            pwui.EntidadClaveSolicitante = u.EntidadClaveSolicitante.ToString();
            pwui.UsuarioClaveAutorizacion = u.UsuarioClaveAutorizacion.ToString();
            pwui.Observaciones = u.Observaciones;

            dal.PedidoDetalle pd = u.PedidoDetalle;

            pwui.TipoCambio = pd.TipoCambio.Value.ToString();
            pwui.Iva = pd.IVA.Value.ToString();
            pwui.Descuento = pd.Descuento.Value.ToString();
            pwui.DiasCredito = pd.DiasCredito.Value.ToString();
            pwui.ToSubtotal = pd.Subtotal_Total.Value.ToString();
            pwui.ToDescuento = pd.Descuento_Total.Value.ToString();
            pwui.ToIVA = pd.IVA_Total.Value.ToString();
            pwui.Total= pd.Total.Value.ToString();
            pwui.ToSubtotal_usd= pd.Subtotal_Total_USD.Value.ToString();
            pwui.ToDescuento_usd= pd.Descuento_Total_USD.Value.ToString();
            pwui.ToIVA_usd= pd.IVA_Total_USD.Value.ToString();
            pwui.Total_usd = pd.Total_USD.Value.ToString();


            pwui.OtrosGastos = new List<OtroGastoUI>();
            List<dal.PedidoGastos> otrosgastos = u.PedidoGastos.ToList();

            foreach (dal.PedidoGastos pg in otrosgastos)
            { 
                OtroGastoUI ogui = new OtroGastoUI();
                ogui.Concepto = pg.Concepto;
                ogui.Moneda = pg.catTipoMonedaClave.ToString();
                ogui.Precio = pg.Costo.ToString();
                pwui.OtrosGastos.Add(ogui);
            }

            pwui.Articulos = new List<ArticuloUI>();
            List<dal.ProcesoArticulo> proarts = u.ProcesoArticulo.ToList();

            foreach (dal.ProcesoArticulo pa in proarts)
            {
                dal.Articulo art = pa.Articulo;

                ArticuloUI arui = new ArticuloUI();

                arui.ArticuloClasificacionClave = art.catTipoArticulo.catClasificacionArticuloClave.ToString();
                arui.ArticuloTipoClave = art.catTipoArticulo.catTipoArticuloClave.ToString();

                arui.ArticuloClave = art.ArticuloClave.ToString();
                arui.ArticuloTexto = art.Nombre;
                arui.Cantidad = pa.Cantidad.ToString();

                if (pa.CantidadEntregada != null)
                {
                    arui.CantidadEntregada = pa.CantidadEntregada.ToString();
                }
                else
                {
                    pa.CantidadEntregada = 0;                
                }
                arui.PrecioUnitario = pa.PrecioUnitario.ToString();
                arui.catTipoMonedaClave = pa.catTipoMonedaClave.ToString();
                arui.MonedaTexto = pa.catTipoMoneda.Descripcion;

                pwui.Articulos.Add(arui);
            }
            
            //pwui.OtrosGastos { get; set; }
            //pwui.Articulos { get; set; }


            return pwui;
        }

        private PedidoWrapper PedidoToWrapper(dal.Proceso u)
        {

            dal.ModeloEntities dc = new dal.ModeloEntities();

            dal.PedidoDetalle pd = u.PedidoDetalle;

            PedidoWrapper rw = new PedidoWrapper();
            rw.NumeroPedido = u.NumeroPedido.ToString();

            try
            {
                rw.Pedido = u.Proceso2.NumeroPedido.ToString();
            }
            catch
            {
                rw.Pedido = u.NumeroPedido.ToString();            
            }

            rw.Clave = u.ProcesoClave.ToString();

            rw.EntidadClaveAfectada = u.EntidadClaveAfectada.ToString();
            rw.EntidadClaveSolicitante = u.EntidadClaveSolicitante.ToString();

            /**
            rw.Surtido = "0";
            if (u.catTipoProcesoClave == 3)
            {

                List<dal.Proceso> salidas = (from s in dc.Proceso
                                             where s.ProcesoClaveRespuesta == u.ProcesoClave
                                             select s).ToList();

                int cantidad = 0;
                foreach(dal.Proceso s in salidas)
                {
                    List<dal.ProcesoArticulo> proarts = s.ProcesoArticulo.ToList();

                    foreach (dal.ProcesoArticulo pa in proarts)
                    {
                        cantidad += pa.Cantidad.Value;
                    }

                }

                rw.Surtido = cantidad.ToString();

            }
             **/



            try
            {
                dal.catProveedor pro = u.catProveedor;
                rw.Origen = pro.Nombre;
            }
            catch {
                rw.Origen = "";
            }



            try
            {
                dal.Entidad alm = u.Entidad;
                rw.Destino = alm.Nombre;
            }
            catch
            {
                rw.Destino = "";
            }

            
            dal.catEstatus estado = u.catEstatus;
            rw.Estatus = estado.Nombre;

            rw.Fecha = u.Fecha.Value.ToString("dd/MM/yyyy");

            rw.Total = pd.Total.Value.ToString("C");
            rw.Total_USD = pd.Total_USD.Value.ToString("C");

            return rw;
        }



        public ActionResult Detalle()
        {
            dal.ModeloEntities dc = new dal.ModeloEntities();

            if (Request["p"] == null)
            {
                return Content("");
            }

            Guid clave = Guid.Parse(Request["p"]);
            int pdf = Convert.ToInt32(Request["pdf"]);


            List<dal.catTipoMoneda> monedas = (from m in dc.catTipoMoneda
                                               orderby m.Descripcion
                                               select m).ToList();


            dal.Proceso pro = (from p in dc.Proceso
                               where p.ProcesoClave == clave
                               select p).SingleOrDefault();


            dal.Entidad e = pro.Entidad;
            dal.Usuario u = pro.Usuario;
            dal.Usuario elaboro = pro.Usuario1;

            StringBuilder sb = new StringBuilder();

            string empresa = Config.Dame("EMPRESA");
            string logo = Config.Dame("LOGO_ENTRADA");

            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
            sb.Append("<tr>");
            sb.Append(@"<td align=""left"">");

            sb.Append("<h2>" + empresa + "</h2>");
            sb.Append("<h3>Pedido #" + pro.NumeroPedido + "</h3><br>");






            sb.Append("</td>");
            sb.Append(@"<td align=""right"">");
            sb.Append("<img src='_RUTA_/media/" + logo + "' width=130 align=right><br>");
            sb.Append("</tr>");
            sb.Append("</table>");

            if (pro.catDistribuidor != null)
            {
                sb.Append("<h4><b>Distribuidor</b>&nbsp;");
                sb.Append(pro.catDistribuidor.Nombre + "</h4><br>");
            }

            sb.Append("<font size=2>");

            if (pro.catProveedor != null)
            {
                sb.Append("<b>Proveedor</b>&nbsp;");
                sb.Append(pro.catProveedor.Nombre + "<br>");
            }

            sb.Append("<b>Enviar a</b>&nbsp;");
            sb.Append(e.Nombre + "<br>");

            if (elaboro != null)
            {
                sb.Append("<b>Elaboró</b>&nbsp;");
                sb.Append(elaboro.Nombre + "<br>");
            }
            else
            {

            }

            if (u != null)
            {
                sb.Append("<b>Autorizó</b>&nbsp;");
                sb.Append(u.Nombre);
                sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            }
            else
            { 
            
            }

            if (pro.catEstatusClave == 1)
            {
                sb.Append("<FONT COLOR='#FF0000'>PENDIENTE DE AUTORIZAR</FONT><br>");
            }
            else
            {
                sb.Append("<FONT COLOR='#FF0000'>" + pro.catEstatus.Nombre.ToUpper() + "</FONT><br>");
            }

            sb.Append("<br><br>");

            sb.Append("<b>Artículos</b><br><br>");
            //sb.Append(@"<table cellpadding=""1""  cellspacing=""0"" bgcolor=""#EFEFEF""><tr></td>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=1 width=""100%"" class=""table table-hover-color"">");

            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Cantidad");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"" colspan=""3"">");
            sb.Append("Concepto");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Precio Unitario");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Importe");
            sb.Append("</th>");
            sb.Append(@"<th bgcolor=""#424242"" color=""#FFFFFF"">");
            sb.Append("Importe USD");
            sb.Append("</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");

            List<dal.ProcesoArticulo> arts = pro.ProcesoArticulo.ToList();

            sb.Append("<tbody>");
            foreach (dal.ProcesoArticulo pa in arts)
            {
                dal.Articulo a = pa.Articulo;


                sb.Append("<tr>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pa.Cantidad);
                sb.Append("</td>");
                sb.Append(@"<td colspan=""3"">");
                sb.Append(a.Nombre);
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pa.PrecioUnitario.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");

                if (pa.catTipoMonedaClave == 1)
                {
                    decimal importe = pa.PrecioUnitario.Value * pa.Cantidad.Value;
                    sb.Append(importe.ToString("C"));
                }
                else {
                    sb.Append(Convert.ToDecimal(0).ToString("C"));
                }


                sb.Append("</td>");

                sb.Append(@"<td align=""right"">");


                dal.catTipoMoneda m = (from mo in monedas
                                       where mo.catTipoMonedaClave == pa.catTipoMonedaClave
                                       select mo).SingleOrDefault();

                if (m.Descripcion.ToLower().IndexOf("dolar")>-1)
                {
                    decimal importeusd = pa.PrecioUnitario.Value * pa.Cantidad.Value;
                    sb.Append(importeusd.ToString("C"));
                }
                else
                {
                    sb.Append(Convert.ToDecimal(0).ToString("C"));
                }
                sb.Append("</td>");
                sb.Append("</tr>");
            }


            sb.Append("</table>");
            //sb.Append("</td></tr></table>");
            sb.Append("</tbody>");

            if (pdf == 1 || pdf==2)
            {
                sb.Append("<br>");
            }

            sb.Append("<b>Otros Gastos</b><br>");
            List<dal.PedidoGastos> gtos = pro.PedidoGastos.ToList();
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");

            foreach (dal.PedidoGastos g in gtos)
            {

                sb.Append("<tr>");
                sb.Append(@"<td>");
                sb.Append("<br>");
                sb.Append("</td>");
                sb.Append(@"<td colspan=""4"">");
                sb.Append(g.Concepto);
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");

                if (g.catTipoMonedaClave == 1)
                {

                    decimal importe = g.Costo.Value;
                    sb.Append(importe.ToString("C"));
                }
                else
                {
                    sb.Append(Convert.ToDecimal(0).ToString("C"));
                }

                sb.Append("</td>");

                sb.Append(@"<td align=""right"">");

                dal.catTipoMoneda m = (from mo in monedas
                                       where mo.catTipoMonedaClave == g.catTipoMonedaClave
                                       select mo).SingleOrDefault();

                if (m.Descripcion.ToLower().IndexOf("dolar")>-1)
                {

                    decimal importe = g.Costo.Value;
                    sb.Append(importe.ToString("C"));
                }
                else
                {
                    sb.Append(Convert.ToDecimal(0).ToString("C"));
                }

                sb.Append("</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");



            dal.PedidoDetalle pedido = pro.PedidoDetalle;

            sb.Append("<b>Total</b><br>");
            sb.Append(@"<table cellpadding=""2""  cellspacing=""0"" border=0 width=""100%"">");
                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"">");
                sb.Append("Subtotal");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.Subtotal_Total.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.Subtotal_Total_USD.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"">");
                sb.Append("Descuento");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.Descuento_Total.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.Descuento_Total_USD.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"">");
                sb.Append("IVA");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.IVA_Total.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.IVA_Total_USD.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append("</tr>");


                sb.Append("<tr>");
                sb.Append(@"<td colspan=""5"" align=""right"">");
                sb.Append("Total");
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.Total.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append(@"<td align=""right"">");
                sb.Append(pedido.Total_USD.Value.ToString("C"));
                sb.Append("</td>");
                sb.Append("</tr>");

            sb.Append("</table>");


            sb.Append("</font>");


            if (pdf != 1 && pdf != 2)
            {
                ViewData["html"] = sb.ToString().Replace(@"bgcolor=""#424242""", "").Replace(@"color=""#FFFFFF""", "").Replace("border=1", "").Replace("_RUTA_", "/Content");
                return View();
            }
            else
            {

                PDF elpdf = new PDF();

                Guid g = Guid.NewGuid();

                string rutaarchivo = Server.MapPath("/Content") + "/media/"+ g.ToString()  +"pedido.pdf";

                elpdf.HTMLToPdf(sb.ToString().Replace("_RUTA_", Server.MapPath("/Content")), rutaarchivo, pro);


                if (pdf == 1)
                {
                    return File(rutaarchivo, "application/pdf", "Pedido" + pro.NumeroPedido.ToString() + ".pdf");
                }
                else
                {
                    ViewData["html"] = "<iframe src='" +  "/Content/media/" + g.ToString() + "pedido.pdf" + "' width=100% height=380 frameborder=0></iframe>";
                    return View();
                }
            }


            
        }


    }
}
