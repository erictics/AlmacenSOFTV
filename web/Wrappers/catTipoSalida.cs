﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class catTipoSalidaWrapper
    {
        public string Clave { get; set; }
        public string Descripcion { get; set; }
        public string Activo { get; set; }
       
    }
}
