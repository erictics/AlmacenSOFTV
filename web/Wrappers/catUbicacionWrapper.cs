﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wrappers
{
    public class catUbicacionWrapper
    {
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string catUbicacionPadreClave { get; set; }
        public string Activo { get; set; }
    }
}
