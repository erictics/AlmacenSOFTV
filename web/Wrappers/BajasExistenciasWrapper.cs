﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class BajasExistenciasWrapper
    {
        public string CLASIFICACION { get; set; }
        public string TIPO { get; set; }

        public string ARTICULO { get; set; }

       public string MINIMO { get; set; }

       public  string UNIDAD { get; set;  }
        public string EXISTENCIAS { get; set; }

    }
}