﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class MultiusosWrapper
    {
       public DataTable tabla { get; set; }
        public string variable1 { get; set; }
        public string variable2 { get; set; }
        public string variable3 { get; set; }
        public string variable4 { get; set; }
        public string variable5 { get; set; }
        public string variable6 { get; set; }
        public string variable7 { get; set; }
        public string variable8 { get; set; }
        public List<string>lista { get; set; }
        public List<DataTablesInventarioWrapper> listadetablas { get; set; }// no quitar 
    }
}