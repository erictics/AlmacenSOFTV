﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class catTransportistaWrapper
    {
        public string Clave { get; set; }
        public string Descripcion { get; set; }
        public string URLSeguimiento { get; set; }
        public string Activo { get; set; }
       
    }
}
