﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class ArticuloProcesoWrapper
    {
        public string Clave { get; set; }
        public string ArticuloClave { get; set; }
        public string TipoArticuloClave { get; set; }
        public string ClasificacionClave { get; set; }
        public List<catSerieWrapper> catSeriesClaves { get; set; }
        
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string PrecioUnitarioTexto { get; set; }
        public int Surtida { get; set; }
        public int Surtiendo { get; set; }
        public int Recepcion { get; set; }

        public int Existencia { get; set; }

        public string MonedaTexto { get; set; }
        public string MonedaClave { get; set; }


        public List<SerialesWrapper> Seriales { get; set; }

        public int TieneSeries { get; set; }

        public int Standby { get; set; }//agregue este
    }
}