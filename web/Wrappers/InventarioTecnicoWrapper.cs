﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class InventarioTecnicoWrapper
    {
        public string CATEGORIA { get; set; }
        public string TIPO { get; set; }
        public string ARTICULO { get; set; }
        public string CANTIDAD { get; set; }
    }
}