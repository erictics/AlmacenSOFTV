﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class catTipoArticuloWrapper
    {
        public string Clave { get; set; }
        public string catClasificacionArticuloClave { get; set; }
        public string Descripcion { get; set; }
        public List<string> Series { get; set; }
        public string DevMatriz { get; set; }
        public string EsPagare { get; set; }
        public string SerieInterface { get; set; }
        public string Letra { get; set; }
        public string Activo { get; set; }
    }
}
