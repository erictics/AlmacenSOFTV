﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class ProcesoEnvioWrapper
    {
        public string ProcesoClave      { get; set; }
        public string IdTransportista   { get; set; }
        public string GuiaEnvio         { get; set; }
        public string FechaEnvio        { get; set; }
    }
}
