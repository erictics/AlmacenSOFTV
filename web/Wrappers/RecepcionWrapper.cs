﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class RecepcionWrapper
    {
        public string NumeroPedido { get; set; }
        public string Pedido { get; set; }
        public string Clave { get; set; }
        public string ClavePedido { get; set; }
        public string Proveedor { get; set; }
        public string Destino { get; set; }
        public string Origen { get; set; }
        public string Fecha { get; set; }
        public string Estatus { get; set; }
        public string EstatusClave { get; set; }


        public string AlmacenClave { get; set; }
        public string ProveedorClave { get; set; }

        public string TipoCambio { get; set; }
        public string Iva { get; set; }
        public string Descuento { get; set; }
        public string DiasCredito { get; set; }

        public List<ArticuloProcesoWrapper> Articulos { get; set; }
    }
}
