﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class InventarioWrapper
    {
        public string Clave { get; set; }
        public string ArticuloClave { get; set; }
        public string AlmacenClave { get; set; }
        public List<SerialesWrapper> Seriales { get; set; }
        public List<catSerieWrapper> catSeriesClaves { get; set; }
        public bool disponible { get; set; }
        public int posicion { get; set; }
    }
}