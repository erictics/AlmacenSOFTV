﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class catProveedorWrapper
    {
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string RFC { get; set; }


        public string DomicilioClave { get; set; }
        public string CalleNumero { get; set; }
        public string Colonia { get; set; }
        public string CodigoPostal { get; set; }
        public string Ciudad { get; set; }

        public string NombreContacto { get; set; }
        public string EmailContacto { get; set; }
        public string Lada1Contacto { get; set; }
        public string Telefono1Contacto { get; set; }
        public string Lada2Contacto { get; set; }
        public string Telefono2Contacto { get; set; }

        public string Activo { get; set; }



    }
}
