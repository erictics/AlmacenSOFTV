﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class PedidoWrapper
    {
        public string NumeroPedido { get; set; }
        public string Pedido { get; set; }
        public string Clave { get; set; }
        public string Distribuidor { get; set; }
        public string Destino { get; set; }
        public string EntidadClaveAfectada { get; set; }
        public string EntidadClaveSolicitante { get; set; }
        public string Origen { get; set; }
        public string Fecha { get; set; }
        public string Total { get; set; }
        public string Total_USD { get; set; }
        public string Estatus { get; set; }
        public string EstatusClave { get; set; }
        public string EsPagare { get; set; }
        public string TieneSalidas { get; set; }
        public string Surtido{ get; set; }
        public string FacturacionClave { get; set; }
        public string Facturacion { get; set; }
        public string Tiene_Accesorios { get; set;}
        public bool es_internet { get; set; }


    }

    public class PedidoWrapperUI
    {
        public string NumeroPedido { get; set; }
        public string Clave { get; set; }
        public string ProcesoClave { get; set; }
        public string catProveedorClave { get; set; }
        public string EntidadClaveSolicitante { get; set; }
        public string EntidadClaveAfectada { get; set; }
        public string UsuarioClaveAutorizacion { get; set; }
        public string Observaciones{ get; set; }
        public string TipoCambio{ get; set; }
        public string Iva{ get; set; }
        public string Descuento{ get; set; }
        public string DiasCredito{ get; set; }
        public string ToSubtotal{ get; set; }
        public string ToDescuento{ get; set; }
        public string ToIVA{ get; set; }
        public string Total{ get; set; }
        public string ToSubtotal_usd{ get; set; }
        public string ToDescuento_usd{ get; set; }
        public string ToIVA_usd{ get; set; }
        public string Total_usd { get; set; }
        public string TipoSalida { get; set; }
        public string EsPagare { get; set; }
        public string Estatus { get; set; }
        public string Pedido { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public string Fecha { get; set; }
        public string Total_USD { get; set; }
        public bool soyinternet { get; set; }
        
        
        
        public List<OtroGastoUI> OtrosGastos { get; set; }
        public List<ArticuloUI> Articulos { get; set; }
    }

    public class OtroGastoUI
    {
        public string Concepto { get; set; }
        public string Precio { get; set; }
        public string Moneda { get; set; }
    }

    public class ArticuloUI
    {
        public string ArticuloClasificacionClave { get; set; }
        public string ArticuloTipoClave { get; set; }
        public string ArticuloClave { get; set; }
        public string ArticuloTexto { get; set; }
        public string Cantidad { get; set; }
        public string PrecioUnitario { get; set; }
        public string catTipoMonedaClave { get; set; }
        public string MonedaTexto { get; set; }
        public string CantidadEntregada { get; set; }
    }


    public class objPedido
    {
        public string Orden { get; set; }
        public string Recepcion { get; set; }
        public string Proveedor { get; set; }
        public string Almacen { get; set; }
        public string AlmacenAfectado { get; set; }
        public string TipoAsignacion { get; set; }
        public List<ArticuloOrden> Articulos { get; set; }
        public string Fecha { get; set; }
        public string Usuario { get; set; }
        public string Status { get; set; }
        public List<ArticuloOrden> Costos { get; set; }
    }


    public class ArticuloOrden
    {
        public Guid ArticuloClave { get; set; }
        public string Articulo { get; set; }
        public decimal Precio { get; set; }
        public int CantidadPedida { get; set; }
        public int CantidadSurtida { get; set; }
        public int CantidadPendiente { get; set; }
        public int CantidadRecepcion { get; set; }
        public int ClaveInterface { get; set; }
        public List<serie> series { get; set; }
        public string TipoSerie { get; set; }
        public bool Hexadecimal { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }

        public int TipoMoneda { get; set; }
    }

    public class serie
    {
        public Guid InventarioClave { get; set; }
        public string Valor { get; set; }
        public int Status { get; set; }
        public bool Editable { get; set; }
    }
}
