﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class SerieTipoArticulo
    {
        public string Clave { get; set; }
        public string catTipoArticuloClave { get; set; }
        public string Activo { get; set; }
    }
}
