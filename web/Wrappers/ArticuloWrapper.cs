﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class ArticuloWrapper
    {
        public string Clave { get; set; }
        public string catTipoArticuloClave { get; set; }
        public string catClasificacionMaterialClave { get; set; }

        public List<catSerieWrapper> catSeriesClaves = new List<catSerieWrapper>();

        public string Clasificacion { get; set; }
        public string TipoArticulo { get; set; }

        public string Descripcion { get; set; }
        public string catUnidadClave { get; set; }
        public string Existencias { get; set; }

        public string EsPagare { get; set; }

        public string PrecioPagare { get; set; }
        public string catMonedaPagareClave { get; set; }


        public string Pagare { get; set; }
        public string TopeMinimo { get; set; }

        public string Porcentaje { get; set; }

        public string proveedor1 { get; set; }
        public string precio1 { get; set; }
        public string moneda1 { get; set; }

        public string proveedor2 { get; set; }
        public string precio2 { get; set; }
        public string moneda2 { get; set; }

        public string proveedor3 { get; set; }
        public string precio3 { get; set; }
        public string moneda3 { get; set; }

        public string parte { get; set; }

        public string tieneseries { get; set; }

        public string catUbicacionClave { get; set; }

        public string Activo { get; set; }

        public List<string> Servicios { get; set; }

        public List<string> idServicios { get; set; }

    }

    public class SerialesWrapper
    {
        public string InventarioClave { get; set; }
        public string RecepcionClave { get; set; }
        public string ArticuloClave { get; set; }
        public string EstatusTexto { get; set; }
        public string EstatusClave { get; set; }
        public int Activo { get; set; }
        public List<SerialesDatosWrapper> datos { get; set; }
    }

    public class SerialesDatosWrapper
    {
        public string Clave { get; set; }
        public string Valor { get; set; }
    }
}
