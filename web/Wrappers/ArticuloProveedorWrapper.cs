﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class ArticuloProveedorWrapper
    {
        public string Clave { get; set; }
        public string catProveedorClave { get; set; }
        public string Activo { get; set; }
    }
}
