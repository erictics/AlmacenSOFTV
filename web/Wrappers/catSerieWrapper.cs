﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class catSerieWrapper
    {
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Activo { get; set; }
        public string Hexadecimal { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
    }
}
