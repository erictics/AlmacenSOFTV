﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class RolWrapper
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public bool tieneusuarios { get; set; }
    }
}