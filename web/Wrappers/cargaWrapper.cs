﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class cargaWrapper
    {
        public DateTime fecha { get; set; }
        public string movimiento { get; set; }
        public string folio { get; set; }
        public string cantidad { get; set; }
        public string usuario { get; set; }
    }
}