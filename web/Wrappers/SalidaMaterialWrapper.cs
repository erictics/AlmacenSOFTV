﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class SalidaMaterialWrapper
    {

        public string NumeroPedido { get; set; }
        public string Clave { get; set; }
        public string ProcesoClave { get; set; }
        public string catProveedorClave { get; set; }
        public string EntidadClaveSolicitante { get; set; }
        public string EntidadClaveAfectada { get; set; }
        public string UsuarioClaveAutorizacion { get; set; }
        public string Observaciones { get; set; }
        public string TipoCambio { get; set; }
        public string Iva { get; set; }
        public string Descuento { get; set; }
        public string DiasCredito { get; set; }
        public string ToSubtotal { get; set; }
        public string ToDescuento { get; set; }
        public string ToIVA { get; set; }
        public string Total { get; set; }
        public string ToSubtotal_usd { get; set; }
        public string ToDescuento_usd { get; set; }
        public string ToIVA_usd { get; set; }
        public string Total_usd { get; set; }
        public string TipoSalida { get; set; }
        public string EsPagare { get; set; }
        public string Estatus { get; set; }
        public string Pedido { get; set; }
        public string Origen { get; set; }
        public string EsCancelable { get; set; }
        public string Fecha { get; set; }
        public string Destino { get; set; }
        public string Total_USD { get; set; }
        public string BitacoraLetra { get; set; }
        public string BitacoraNumero { get; set; }
        public string Plaza { get; set; }

        public List<ArticuloSalidaMaterialUI> Articulos { get; set; }
    }


    public class ArticuloSalidaMaterialUI
    {
        public string Clasificacion { get; set; }
        public string TipoMaterial { get; set; }
        public string ArticuloClave { get; set; }
        public string ArticuloTexto { get; set; }
        public string tieneseries { get; set; }
        public string Cantidad { get; set; }
        public string Existencias { get; set; }
        public string EnAlmacen { get; set; }
        public string EsCancelable { get; set; }
        public List<string> Inventario { get; set; }
    }
}
