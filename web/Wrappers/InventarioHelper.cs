﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class InventarioHelper
    {

        public string CLASIFICACION { get; set; }
        public string TIPO { get; set; }
        public string ARTICULO { get; set; }
        public string  INVENTARIO { get; set; }

        public string STANDBY {get; set;}

        public string REVISION { get; set; }
    }
}