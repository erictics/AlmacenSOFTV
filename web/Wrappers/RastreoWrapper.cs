﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class RastreoWrapper
    {

        public string Clave { get; set; }
        public List<string> SeriesDef { get; set; }
        public List<string> Series { get; set; }
        public string Estatus { get; set; }
        public string Ubicacion { get; set; }
        public string Descripcion { get; set; }
        public string valor { get; set; }

    }
}