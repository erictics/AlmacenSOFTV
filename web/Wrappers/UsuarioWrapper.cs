﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Wrappers
{
    public class UsuarioWrapper
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Rol { get; set; }
        public string RolId { get; set; }
        public string Sucursal { get; set; }
        public string SucursalClave { get; set; }
        public string Correo { get; set; }
        public string Firma { get; set; }
        public string Clave { get; set; }
        public string Asignaciones { get; set; }

        public bool Activo { get; set; }
    }
}