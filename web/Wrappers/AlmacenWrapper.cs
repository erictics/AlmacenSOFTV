﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web.Wrappers
{
    public class AlmacenWrapper
    {
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string catEntidadTipoClave { get; set; }
        public string catDistribuidorClave { get; set; }
        public string EntidadPadreClave { get; set; }
        public string Activo { get; set; }
    }
}
