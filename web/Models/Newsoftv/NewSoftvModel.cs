namespace web.Models.Newsoftv
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class NewSoftvModel : DbContext
    {
        public NewSoftvModel()
            : base("name=NewSoftvModelo")
        {
        }

        public virtual DbSet<companias> companias { get; set; }
        public virtual DbSet<Plaza> Plaza { get; set; }
        public virtual DbSet<DetFacturaAlmacen> DetFacturaAlmacen { get; set; }
        public virtual DbSet<FacturasAlmacen> FacturasAlmacen { get; set; }
        public virtual DbSet<GeneralPrincipal> GeneralPrincipal { get; set; }
        public virtual DbSet<AlmacenDireccionPlaza> AlmacenDireccionPlaza { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<companias>()
                .Property(e => e.razon_social)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.clave_compania)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.RFC)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.calle)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.numero_exterior)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.numero_interior)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.entre_calles)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.colonia)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.localidad)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.referencia)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.municipio)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.estado)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.pais)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.codigo_postal)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.numero_certificado)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.serie)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.KEY)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.password_KEY)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.CER)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.formato_factura)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.cuerpo_email_envio_facturas)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.ContratoReporte)
                .IsUnicode(false);

            modelBuilder.Entity<companias>()
                .Property(e => e.contacto)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.RFC)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Calle)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.NumEx)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.NumIn)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Colonia)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.CP)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Localidad)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Estado)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.EntreCalles)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Municipio)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Pais)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.lada1)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.lada2)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.Telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.NombreContacto)
                .IsUnicode(false);

            modelBuilder.Entity<Plaza>()
                .Property(e => e.ImportePagare)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.PrecioUnitario)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.SubTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.IVA)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.IEPS)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.subtotalUSD)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.totalUSD)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetFacturaAlmacen>()
                .Property(e => e.TipoUnidad)
                .IsUnicode(false);

            modelBuilder.Entity<FacturasAlmacen>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<FacturasAlmacen>()
                .Property(e => e.Error)
                .IsUnicode(false);

            modelBuilder.Entity<FacturasAlmacen>()
                .Property(e => e.importeUSD)
                .HasPrecision(19, 4);

            modelBuilder.Entity<FacturasAlmacen>()
                .Property(e => e.formaPago)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.colonia)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.ciudad)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.estado)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.rfc)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.Curp)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.TELefonos)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.CP)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.numInt)
                .IsUnicode(false);

            modelBuilder.Entity<GeneralPrincipal>()
                .Property(e => e.numExt)
                .IsUnicode(false);

            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.idcompania);
               


            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.calle)
             .IsUnicode(false);

            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.codigo_postal)
             .IsUnicode(false);
            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.colonia)
             .IsUnicode(false);
            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.entre_calles)
             .IsUnicode(false);
            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.estado)
                 .IsUnicode(false);

            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.localidad)
             .IsUnicode(false);
            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.municipio)
             .IsUnicode(false);

            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.numero_exterior)
             .IsUnicode(false);

            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.numero_interior)
             .IsUnicode(false);

            modelBuilder.Entity<AlmacenDireccionPlaza>()
                .Property(e => e.pais)
             .IsUnicode(false);


        }
    }
}
