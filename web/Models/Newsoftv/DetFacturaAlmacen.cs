namespace web.Models.Newsoftv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetFacturaAlmacen")]
    public partial class DetFacturaAlmacen
    {
        public long ID { get; set; }

        public long? IDFactura { get; set; }

        [StringLength(50)]
        public string ClaveArticulo { get; set; }

        public int? Cantidad { get; set; }

        [StringLength(250)]
        public string Descripcion { get; set; }

        [Column(TypeName = "money")]
        public decimal? PrecioUnitario { get; set; }

        [Column(TypeName = "money")]
        public decimal? SubTotal { get; set; }

        [Column(TypeName = "money")]
        public decimal? IVA { get; set; }

        [Column(TypeName = "money")]
        public decimal? IEPS { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total { get; set; }

        [Column(TypeName = "money")]
        public decimal? subtotalUSD { get; set; }

        [Column(TypeName = "money")]
        public decimal? totalUSD { get; set; }

        [StringLength(30)]
        public string TipoUnidad { get; set; }
    }
}
