namespace web.Models.Newsoftv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class companias
    {
        [Key]
        public int id_compania { get; set; }

        [StringLength(255)]
        public string razon_social { get; set; }

        [StringLength(16)]
        public string clave_compania { get; set; }

        [StringLength(15)]
        public string RFC { get; set; }

        [StringLength(255)]
        public string calle { get; set; }

        [StringLength(15)]
        public string numero_exterior { get; set; }

        [StringLength(15)]
        public string numero_interior { get; set; }

        [StringLength(255)]
        public string entre_calles { get; set; }

        [StringLength(255)]
        public string colonia { get; set; }

        [StringLength(255)]
        public string localidad { get; set; }

        [StringLength(255)]
        public string referencia { get; set; }

        [StringLength(255)]
        public string municipio { get; set; }

        [StringLength(255)]
        public string estado { get; set; }

        [StringLength(255)]
        public string pais { get; set; }

        [StringLength(5)]
        public string codigo_postal { get; set; }

        [StringLength(255)]
        public string telefono { get; set; }

        [StringLength(255)]
        public string fax { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        public int? numero_aprobacion { get; set; }

        public int? ano_aprobacion { get; set; }

        [StringLength(30)]
        public string numero_certificado { get; set; }

        [StringLength(30)]
        public string serie { get; set; }

        public int? siguiente_folio { get; set; }

        [StringLength(255)]
        public string KEY { get; set; }

        [StringLength(30)]
        public string password_KEY { get; set; }

        [StringLength(255)]
        public string CER { get; set; }

        [StringLength(255)]
        public string formato_factura { get; set; }

        [StringLength(255)]
        public string cuerpo_email_envio_facturas { get; set; }

        public int? id_usr { get; set; }

        public DateTime? f_r { get; set; }

        public DateTime? f_m { get; set; }

        [StringLength(800)]
        public string ContratoReporte { get; set; }

        public int Clv_Plaza { get; set; }

        [StringLength(250)]
        public string contacto { get; set; }
    }
}
