namespace web.Models.Newsoftv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Plaza")]
    public partial class Plaza
    {
        [Key]
        public int Clv_Plaza { get; set; }

        [StringLength(150)]
        public string Nombre { get; set; }

        [StringLength(150)]
        public string RFC { get; set; }

        [StringLength(250)]
        public string Calle { get; set; }

        [StringLength(50)]
        public string NumEx { get; set; }

        [StringLength(50)]
        public string NumIn { get; set; }

        [StringLength(150)]
        public string Colonia { get; set; }

        [StringLength(50)]
        public string CP { get; set; }

        [StringLength(150)]
        public string Localidad { get; set; }

        [StringLength(150)]
        public string Estado { get; set; }

        [StringLength(150)]
        public string EntreCalles { get; set; }

        [StringLength(50)]
        public string Telefono { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(150)]
        public string Municipio { get; set; }

        [StringLength(150)]
        public string Pais { get; set; }

        [StringLength(10)]
        public string lada1 { get; set; }

        [StringLength(10)]
        public string lada2 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        [StringLength(250)]
        public string NombreContacto { get; set; }

        [Column(TypeName = "money")]
        public decimal? ImportePagare { get; set; }

        public int? TiposDistribuidor { get; set; }
    }
}
