﻿
namespace web.Models.Newsoftv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;
    [Table("AlmacenDireccionPlaza")]
    public partial class AlmacenDireccionPlaza
    {
        [Key]
        public long idcompania { get; set; }
        [StringLength(255)]
        public string calle { get; set; }
        [StringLength(255)]
        public string entre_calles { get; set; }
        [StringLength(255)]
        public string numero_exterior { get; set; }
        [StringLength(255)]
        public string numero_interior { get; set; }
        [StringLength(255)]
        public string colonia { get; set; }
        [StringLength(255)]
        public string codigo_postal { get; set; }
        [StringLength(255)]
        public string localidad { get; set; }
        [StringLength(255)]
        public string estado { get; set; }
        [StringLength(255)]
        public string municipio { get; set; }
        [StringLength(255)]
        public string pais { get; set; }

    }

}