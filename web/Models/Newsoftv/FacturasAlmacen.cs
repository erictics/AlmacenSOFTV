namespace web.Models.Newsoftv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FacturasAlmacen")]
    public partial class FacturasAlmacen
    {
        [Key]
        [Column(Order = 0)]
        public long IdFactura { get; set; }

        public long? IdCompra { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime fecha { get; set; }

        [StringLength(150)]
        public string Usuario { get; set; }

        public decimal? importe { get; set; }

        public byte? cancelada { get; set; }

        public long? IdEmisor { get; set; }

        public long? IdReceptor { get; set; }

        public DateTime? hora { get; set; }

        public long? IdCFD { get; set; }

        [StringLength(800)]
        public string Error { get; set; }

        [Column(TypeName = "money")]
        public decimal? importeUSD { get; set; }

        [StringLength(50)]
        public string formaPago { get; set; }

        public int? NumeroCuenta { get; set; }
    }
}
