namespace web.Models.Newsoftv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GeneralPrincipal")]
    public partial class GeneralPrincipal
    {
        public int id { get; set; }

        [StringLength(250)]
        public string nombre { get; set; }

        [StringLength(250)]
        public string direccion { get; set; }

        [StringLength(250)]
        public string colonia { get; set; }

        [StringLength(250)]
        public string ciudad { get; set; }

        [StringLength(250)]
        public string estado { get; set; }

        [StringLength(20)]
        public string rfc { get; set; }

        [StringLength(50)]
        public string Curp { get; set; }

        [StringLength(100)]
        public string TELefonos { get; set; }

        [StringLength(20)]
        public string CP { get; set; }

        [StringLength(10)]
        public string numInt { get; set; }

        [StringLength(10)]
        public string numExt { get; set; }
    }
}
