namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vehiculo")]
    public partial class vehiculo
    {
        [Key]
        public int IdVehiculo { get; set; }

        [StringLength(50)]
        public string conductorTitular { get; set; }

        [StringLength(20)]
        public string Serie { get; set; }

        [StringLength(15)]
        public string Placas { get; set; }

        [StringLength(15)]
        public string Poliza { get; set; }

        [StringLength(50)]
        public string DistribuidorClave { get; set; }

        [StringLength(50)]
        public string PlazaEncierro { get; set; }

        public bool? Asegurado { get; set; }

        public bool? Activo { get; set; }
    }
}
