namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SerieTipoArticulo")]
    public partial class SerieTipoArticulo
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int catSerieClave { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int catTipoArticuloClave { get; set; }

        public bool? Activo { get; set; }

        public virtual catSerie catSerie { get; set; }

        public virtual catTipoArticulo catTipoArticulo { get; set; }
    }
}
