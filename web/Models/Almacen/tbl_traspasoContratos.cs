namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_traspasoContratos
    {
        [Key]
        public int idTraspaso { get; set; }

        public DateTime? fecha { get; set; }

        public int? idDistribuidor_ant { get; set; }

        public int? idplaza_ant { get; set; }

        public int? idplaza_nue { get; set; }

        public int? idDistribuidor_nue { get; set; }
    }
}
