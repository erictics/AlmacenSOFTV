﻿
namespace web.Models
{
    


    public class Servicio
    {
        
        public int idServicio { get; set; }
        public string Nombre { get; set; }

        public bool Activo { get; set; }

        public bool Asignado { get; set; }

    }

}