namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catBanco")]
    public partial class catBanco
    {
        [Key]
        public int catBancoClave { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public bool? Activo { get; set; }
    }
}
