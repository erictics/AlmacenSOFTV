namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Permiso")]
    public partial class Permiso
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RolClave { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModuloClave { get; set; }

        public bool? Consulta { get; set; }

        public bool? Agrega { get; set; }

        public bool? Edita { get; set; }

        public bool? Elimina { get; set; }

        public bool? Ejecuta { get; set; }

        public virtual Modulo Modulo { get; set; }

        public virtual Rol Rol { get; set; }
    }
}
