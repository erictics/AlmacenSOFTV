namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tbl_almacen_servicio
    {
        public int Id { get; set; }

        public int? IdServicio { get; set; }

        public int? IdAlmacen { get; set; }
    }
}
