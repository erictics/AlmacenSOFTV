namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PuestoTecnico")]
    public partial class PuestoTecnico
    {
        [Key]
        public Guid EntidadClave { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdPuestoTecnico { get; set; }

        public int catPuestosTecnicosClave { get; set; }

        public bool Activo { get; set; }

        public virtual catPuestoTecnico catPuestoTecnico { get; set; }

        public virtual Entidad Entidad { get; set; }
    }
}
