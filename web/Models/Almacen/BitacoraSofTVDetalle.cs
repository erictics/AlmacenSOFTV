namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BitacoraSofTVDetalle")]
    public partial class BitacoraSofTVDetalle
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long BitacoraSofTVClave { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid InventarioClave { get; set; }

        public int? Cantidad { get; set; }

        [StringLength(6)]
        public string usuariosoft { get; set; }

        public bool? activo { get; set; }

        public DateTime? fecha { get; set; }

        public virtual BitacoraSofTV BitacoraSofTV { get; set; }

        public virtual Inventario Inventario { get; set; }
    }
}
