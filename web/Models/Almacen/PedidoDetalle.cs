namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PedidoDetalle")]
    public partial class PedidoDetalle
    {
        [Key]
        public Guid ProcesoClave { get; set; }

        public bool? EsPagare { get; set; }

        public decimal? IVA { get; set; }

        public decimal? IVA_USD { get; set; }

        public decimal? Descuento { get; set; }

        public decimal? Descuento_USD { get; set; }

        public decimal? TipoCambio { get; set; }

        public decimal? TipoCambio_USD { get; set; }

        public int? DiasCredito { get; set; }

        public decimal? Subtotal_Total { get; set; }

        public decimal? Subtotal_Total_USD { get; set; }

        public decimal? IVA_Total { get; set; }

        public decimal? IVA_Total_USD { get; set; }

        public decimal? Descuento_Total { get; set; }

        public decimal? Descuento_Total_USD { get; set; }

        public decimal? Total { get; set; }

        public decimal? Total_USD { get; set; }

        public virtual Proceso Proceso { get; set; }
    }
}
