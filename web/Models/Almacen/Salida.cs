namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Salida")]
    public partial class Salida
    {
        [Key]
        [StringLength(10)]
        public string SalidaClave { get; set; }
    }
}
