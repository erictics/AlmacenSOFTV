namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Proceso")]
    public partial class Proceso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Proceso()
        {
            PedidoGastos = new HashSet<PedidoGastos>();
            Proceso1 = new HashSet<Proceso>();
            ProcesoArticulo = new HashSet<ProcesoArticulo>();
            ProcesoInventario = new HashSet<ProcesoInventario>();
            ProcesoInventario1 = new HashSet<ProcesoInventario>();
        }

        [Key]
        public Guid ProcesoClave { get; set; }

        public int? catEstatusClave { get; set; }

        public int? catTipoProcesoClave { get; set; }

        public Guid? EntidadClaveSolicitante { get; set; }

        public Guid? catProveedorClave { get; set; }

        public Guid? EntidadClaveAfectada { get; set; }

        public Guid? ProcesoClaveRespuesta { get; set; }

        public Guid? UsuarioClave { get; set; }

        public Guid? UsuarioClaveAutorizacion { get; set; }

        public DateTime? Fecha { get; set; }

        public string Observaciones { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long NumeroPedido { get; set; }

        public Guid? catDistribuidorClave { get; set; }

        public int? catTipoSalidaClave { get; set; }

        [StringLength(1)]
        public string BitacoraLetra { get; set; }

        [StringLength(150)]
        public string BitacoraNumero { get; set; }

        public Guid? EntidadPlaza { get; set; }

        public Guid? catEstatusFacturacion { get; set; }

        public int? catEstatusFacturacionClave { get; set; }

        public virtual catDistribuidor catDistribuidor { get; set; }

        public virtual catEstatus catEstatus { get; set; }

        public virtual catProveedor catProveedor { get; set; }

        public virtual catTipoProceso catTipoProceso { get; set; }

        public virtual catTipoSalida catTipoSalida { get; set; }

        public virtual Entidad Entidad { get; set; }

        public virtual PedidoDetalle PedidoDetalle { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PedidoGastos> PedidoGastos { get; set; }

        public virtual PedidoPago PedidoPago { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proceso> Proceso1 { get; set; }

        public virtual Proceso Proceso2 { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Usuario Usuario1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcesoArticulo> ProcesoArticulo { get; set; }

        public virtual ProcesoEnvio ProcesoEnvio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcesoInventario> ProcesoInventario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcesoInventario> ProcesoInventario1 { get; set; }

        public virtual Reingreso Reingreso { get; set; }
    }
}
