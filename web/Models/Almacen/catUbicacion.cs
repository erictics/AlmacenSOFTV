namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catUbicacion")]
    public partial class catUbicacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catUbicacion()
        {
            Articulo = new HashSet<Articulo>();
        }

        [Key]
        public int catUbicacionClave { get; set; }

        public Guid? EntidadClave { get; set; }

        [StringLength(150)]
        public string Nombre { get; set; }

        public int? catUbicacionPadreClave { get; set; }

        public bool Piezas { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Articulo> Articulo { get; set; }

        public virtual Entidad Entidad { get; set; }
    }
}
