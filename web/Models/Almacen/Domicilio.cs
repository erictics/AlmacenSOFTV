namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Domicilio")]
    public partial class Domicilio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Domicilio()
        {
            catDistribuidor = new HashSet<catDistribuidor>();
            catProveedor = new HashSet<catProveedor>();
        }

        [Key]
        public Guid DomicilioClave { get; set; }

        [StringLength(1500)]
        public string CalleNumero { get; set; }

        [StringLength(70)]
        public string Colonia { get; set; }

        [StringLength(5)]
        public string CodigoPostal { get; set; }

        [StringLength(70)]
        public string Ciudad { get; set; }

        [StringLength(70)]
        public string Pais { get; set; }

        [StringLength(50)]
        public string Lat { get; set; }

        [StringLength(50)]
        public string Lon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<catDistribuidor> catDistribuidor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<catProveedor> catProveedor { get; set; }
    }
}
