namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_bitacoraTraspaso
    {
        public int Id { get; set; }

        [StringLength(20)]
        public string Usuario { get; set; }

        public DateTime? Fecha { get; set; }

        public int? IdTraspaso { get; set; }

        [StringLength(100)]
        public string Error { get; set; }
    }
}
