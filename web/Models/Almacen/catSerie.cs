namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catSerie")]
    public partial class catSerie
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catSerie()
        {
            catTipoArticulo = new HashSet<catTipoArticulo>();
            Serie = new HashSet<Serie>();
            SerieTipoArticulo = new HashSet<SerieTipoArticulo>();
        }

        [Key]
        public int catSerieClave { get; set; }

        [Required]
        [StringLength(250)]
        public string Nombre { get; set; }

        public bool Activo { get; set; }

        public int Min { get; set; }

        public int Max { get; set; }

        public bool Hexadecimal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<catTipoArticulo> catTipoArticulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Serie> Serie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SerieTipoArticulo> SerieTipoArticulo { get; set; }
    }
}
