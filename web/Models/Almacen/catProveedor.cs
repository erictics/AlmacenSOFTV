namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catProveedor")]
    public partial class catProveedor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catProveedor()
        {
            ArticuloProveedor = new HashSet<ArticuloProveedor>();
            Proceso = new HashSet<Proceso>();
        }

        [Key]
        public Guid catProveedorClave { get; set; }

        [Required]
        [StringLength(250)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(250)]
        public string RFC { get; set; }

        public Guid? DomicilioClave { get; set; }

        [StringLength(3)]
        public string Lada1 { get; set; }

        [StringLength(12)]
        public string Telefono1 { get; set; }

        [StringLength(3)]
        public string Lada2 { get; set; }

        [StringLength(12)]
        public string Telefono2 { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        [StringLength(250)]
        public string NombreContacto { get; set; }

        public decimal? ImportePagare { get; set; }

        public bool Activo { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdProveedor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ArticuloProveedor> ArticuloProveedor { get; set; }

        public virtual Domicilio Domicilio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proceso> Proceso { get; set; }
    }
}
