namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Usuario")]
    public partial class Usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            AmbitoAlmacen = new HashSet<AmbitoAlmacen>();
            Proceso = new HashSet<Proceso>();
            Proceso1 = new HashSet<Proceso>();
        }

        [Key]
        public Guid UsuarioClave { get; set; }

        [Required]
        [StringLength(150)]
        public string Nombre { get; set; }

        public int RolClave { get; set; }

        [Required]
        [StringLength(150)]
        public string CorreoElectronico { get; set; }

        [Required]
        [StringLength(150)]
        public string Password { get; set; }

        public Guid? DistribuidorClave { get; set; }

        [StringLength(150)]
        public string Firma { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AmbitoAlmacen> AmbitoAlmacen { get; set; }

        public virtual catDistribuidor catDistribuidor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proceso> Proceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proceso> Proceso1 { get; set; }

        public virtual Rol Rol { get; set; }
    }
}
