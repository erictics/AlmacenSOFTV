namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProcesoArticulo")]
    public partial class ProcesoArticulo
    {
        [Key]
        [Column(Order = 0)]
        public Guid ProcesoClave { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid ArticuloClave { get; set; }

        public int? Cantidad { get; set; }

        public int? CantidadEntregada { get; set; }

        public decimal? PrecioUnitario { get; set; }

        public int? catTipoMonedaClave { get; set; }

        public virtual Articulo Articulo { get; set; }

        public virtual catTipoMoneda catTipoMoneda { get; set; }

        public virtual Proceso Proceso { get; set; }
    }
}
