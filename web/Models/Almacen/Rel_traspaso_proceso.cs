namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rel_traspaso_proceso
    {
        public int id { get; set; }

        public int? idTraspaso { get; set; }

        [StringLength(50)]
        public string ProcesoClave { get; set; }
    }
}
