namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catDistribuidor")]
    public partial class catDistribuidor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catDistribuidor()
        {
            BitacoraSofTV = new HashSet<BitacoraSofTV>();
            Entidad = new HashSet<Entidad>();
            Pagare = new HashSet<Pagare>();
            Proceso = new HashSet<Proceso>();
            Usuario = new HashSet<Usuario>();
        }

        [Key]
        public Guid catDistribuidorClave { get; set; }

        public long IdDistribuidor { get; set; }

        [Required]
        [StringLength(250)]
        public string Nombre { get; set; }

        [StringLength(250)]
        public string RFC { get; set; }

        public Guid? DomicilioClave { get; set; }

        [StringLength(3)]
        public string Lada1 { get; set; }

        [StringLength(12)]
        public string Telefono1 { get; set; }

        [StringLength(3)]
        public string Lada2 { get; set; }

        [StringLength(12)]
        public string Telefono2 { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        [StringLength(250)]
        public string NombreContacto { get; set; }

        public decimal? ImportePagare { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BitacoraSofTV> BitacoraSofTV { get; set; }

        public virtual Domicilio Domicilio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Entidad> Entidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagare> Pagare { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proceso> Proceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
