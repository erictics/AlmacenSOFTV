namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inventario")]
    public partial class Inventario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inventario()
        {
            BitacoraSofTVDetalle = new HashSet<BitacoraSofTVDetalle>();
            ProcesoInventario = new HashSet<ProcesoInventario>();
            Serie = new HashSet<Serie>();
        }

        [Key]
        public Guid InventarioClave { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdInventario { get; set; }

        public Guid? DistribuidorClave { get; set; }

        public Guid? ArticuloClave { get; set; }

        public Guid? EntidadClave { get; set; }

        public int? Cantidad { get; set; }

        public int? Standby { get; set; }

        public int? Estatus { get; set; }

        public int? EstatusInv { get; set; }

        public virtual Articulo Articulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BitacoraSofTVDetalle> BitacoraSofTVDetalle { get; set; }

        public virtual Entidad Entidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcesoInventario> ProcesoInventario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Serie> Serie { get; set; }
    }
}
