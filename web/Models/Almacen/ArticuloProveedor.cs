namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ArticuloProveedor")]
    public partial class ArticuloProveedor
    {
        [Key]
        [Column(Order = 0)]
        public Guid ArticuloClave { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid catProveedorClave { get; set; }

        public decimal? Precio { get; set; }

        public int? catTipoMonedaClave { get; set; }

        public int? Indice { get; set; }

        public bool? Activo { get; set; }

        public virtual Articulo Articulo { get; set; }

        public virtual catProveedor catProveedor { get; set; }

        public virtual catTipoMoneda catTipoMoneda { get; set; }
    }
}
