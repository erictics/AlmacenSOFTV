﻿

namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RelTecnicoDistribuidor")]
    public class RelTecnicoDistribuidor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)][Key]
        public int Id { get; set; }

        public Guid EntidadClave { get; set; }

        public Guid DistribuidorClave { get; set; }
    }
}