namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catPuestoTecnico")]
    public partial class catPuestoTecnico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catPuestoTecnico()
        {
            PuestoTecnico = new HashSet<PuestoTecnico>();
            PuestoTecnicoClasificacionArticulo = new HashSet<PuestoTecnicoClasificacionArticulo>();
        }

        [Key]
        public int catPuestosTecnicosClave { get; set; }

        [Required]
        [StringLength(250)]
        public string Descripcion { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PuestoTecnico> PuestoTecnico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PuestoTecnicoClasificacionArticulo> PuestoTecnicoClasificacionArticulo { get; set; }
    }
}
