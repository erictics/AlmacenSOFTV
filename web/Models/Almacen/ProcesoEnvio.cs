namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProcesoEnvio")]
    public partial class ProcesoEnvio
    {
        [Key]
        public Guid ProcesoClave { get; set; }

        public int catTranspClave { get; set; }

        [Required]
        [StringLength(250)]
        public string GuiaEnvio { get; set; }

        public DateTime FechaEnvio { get; set; }

        public virtual catTransportista catTransportista { get; set; }

        public virtual Proceso Proceso { get; set; }
    }
}
