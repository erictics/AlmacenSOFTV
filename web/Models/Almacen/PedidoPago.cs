namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PedidoPago")]
    public partial class PedidoPago
    {
        [Key]
        public Guid ProcesoClave { get; set; }

        [StringLength(50)]
        public string Forma { get; set; }

        [StringLength(50)]
        public string Banco { get; set; }

        public DateTime? FechaPago { get; set; }

        public decimal? Monto { get; set; }

        public decimal? MontoUSD { get; set; }

        public DateTime Fecha { get; set; }

        public Guid? UsuarioClave { get; set; }

        [StringLength(10)]
        public string NumeroCuenta { get; set; }

        public virtual Proceso Proceso { get; set; }
    }
}
