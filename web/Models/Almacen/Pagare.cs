namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Pagare")]
    public partial class Pagare
    {
        [Key]
        [Column(Order = 0)]
        public Guid ArticuloClave { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid catDistribuidorClave { get; set; }

        public int? Asignadas { get; set; }

        public int? Utilizadas { get; set; }

        public int? Disponibles { get; set; }

        public virtual Articulo Articulo { get; set; }

        public virtual catDistribuidor catDistribuidor { get; set; }
    }
}
