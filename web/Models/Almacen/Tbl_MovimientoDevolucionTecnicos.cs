namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tbl_MovimientoDevolucionTecnicos
    {
        public int Id { get; set; }

        public Guid? ProcesoClave { get; set; }

        public DateTime? Fecha { get; set; }

        public Guid? catDistribuidorClave { get; set; }

        public Guid? InventarioClave { get; set; }

        [StringLength(250)]
        public string Valor { get; set; }

        public long? Clv_CableModem { get; set; }
    }
}
