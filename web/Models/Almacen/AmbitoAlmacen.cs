namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AmbitoAlmacen")]
    public partial class AmbitoAlmacen
    {
        [Key]
        public Guid AmbitoAlmacenClave { get; set; }

        public Guid? UsuarioClave { get; set; }

        public Guid? EntidadClaveTecnico { get; set; }

        public Guid? EntidadClaveAlmacen { get; set; }

        public virtual Entidad Entidad { get; set; }

        public virtual Entidad Entidad1 { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
