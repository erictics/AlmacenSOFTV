namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BitacoraSofTV")]
    public partial class BitacoraSofTV
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BitacoraSofTV()
        {
            BitacoraSofTVDetalle = new HashSet<BitacoraSofTVDetalle>();
        }

        [Key]
        public long BitacoraSofTVClave { get; set; }

        [StringLength(1)]
        public string TipoServicio { get; set; }

        public long? NumeroOrden { get; set; }

        [StringLength(50)]
        public string Contrato { get; set; }

        [StringLength(150)]
        public string NombreUsuario { get; set; }

        public Guid? catDistribuidorClave { get; set; }

        public long? IdDistribuidor { get; set; }

        public Guid? EntidadClaveTecnico { get; set; }

        public long? IdEntidadTecnico { get; set; }

        public Guid? EntidadClaveAlmacen { get; set; }

        public long? IdEntidadAlmacen { get; set; }

        public DateTime? Fecha { get; set; }

        public virtual catDistribuidor catDistribuidor { get; set; }

        public virtual Entidad Entidad { get; set; }

        public virtual Entidad Entidad1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BitacoraSofTVDetalle> BitacoraSofTVDetalle { get; set; }
    }
}
