namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catTipoArticulo")]
    public partial class catTipoArticulo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catTipoArticulo()
        {
            Articulo = new HashSet<Articulo>();
            SerieTipoArticulo = new HashSet<SerieTipoArticulo>();
        }

        [Key]
        public int catTipoArticuloClave { get; set; }

        public int catClasificacionArticuloClave { get; set; }

        [Required]
        [StringLength(250)]
        public string Descripcion { get; set; }

        public bool Activo { get; set; }

        public bool? DevMatriz { get; set; }

        public bool? EsPagare { get; set; }

        public int? catSerieClaveInterface { get; set; }

        [StringLength(1)]
        public string Letra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Articulo> Articulo { get; set; }

        public virtual catClasificacionArticulo catClasificacionArticulo { get; set; }

        public virtual catSerie catSerie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SerieTipoArticulo> SerieTipoArticulo { get; set; }
    }
}
