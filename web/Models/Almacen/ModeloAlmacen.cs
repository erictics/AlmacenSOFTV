namespace web.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
 

    public partial class ModeloAlmacen : DbContext
    {
        public ModeloAlmacen()
            : base("name=ModeloAlmacen")
        {
        }

        public virtual DbSet<AmbitoAlmacen> AmbitoAlmacen { get; set; }
        public virtual DbSet<Articulo> Articulo { get; set; }
        public virtual DbSet<ArticuloProveedor> ArticuloProveedor { get; set; }
        public virtual DbSet<BitacoraSofTV> BitacoraSofTV { get; set; }
        public virtual DbSet<BitacoraSofTVDetalle> BitacoraSofTVDetalle { get; set; }
        public virtual DbSet<catBanco> catBanco { get; set; }
        public virtual DbSet<catClasificacionArticulo> catClasificacionArticulo { get; set; }
        public virtual DbSet<catDiaFestivo> catDiaFestivo { get; set; }
        public virtual DbSet<catDistribuidor> catDistribuidor { get; set; }
        public virtual DbSet<catEntidadTipo> catEntidadTipo { get; set; }
        public virtual DbSet<catEstatus> catEstatus { get; set; }
        public virtual DbSet<catFalla> catFalla { get; set; }
        public virtual DbSet<catProveedor> catProveedor { get; set; }
        public virtual DbSet<catPuestoTecnico> catPuestoTecnico { get; set; }
        public virtual DbSet<catSerie> catSerie { get; set; }
        public virtual DbSet<catTipoArticulo> catTipoArticulo { get; set; }
        public virtual DbSet<catTipoMoneda> catTipoMoneda { get; set; }
        public virtual DbSet<catTipoProceso> catTipoProceso { get; set; }
        public virtual DbSet<catTipoSalida> catTipoSalida { get; set; }
        public virtual DbSet<catTransportista> catTransportista { get; set; }
        public virtual DbSet<catUbicacion> catUbicacion { get; set; }
        public virtual DbSet<catUnidad> catUnidad { get; set; }
        public virtual DbSet<Configuracion> Configuracion { get; set; }
        public virtual DbSet<DetalleVehiculo> DetalleVehiculo { get; set; }
        public virtual DbSet<Domicilio> Domicilio { get; set; }
        public virtual DbSet<Entidad> Entidad { get; set; }
        public virtual DbSet<GrupoConfiguracion> GrupoConfiguracion { get; set; }
        public virtual DbSet<Inventario> Inventario { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<Pagare> Pagare { get; set; }
        public virtual DbSet<PedidoDetalle> PedidoDetalle { get; set; }
        public virtual DbSet<PedidoGastos> PedidoGastos { get; set; }
        public virtual DbSet<PedidoPago> PedidoPago { get; set; }
        public virtual DbSet<Permiso> Permiso { get; set; }
        public virtual DbSet<Proceso> Proceso { get; set; }
        public virtual DbSet<ProcesoArticulo> ProcesoArticulo { get; set; }
        public virtual DbSet<ProcesoEnvio> ProcesoEnvio { get; set; }
        public virtual DbSet<ProcesoInventario> ProcesoInventario { get; set; }
        public virtual DbSet<PuestoTecnico> PuestoTecnico { get; set; }
        public virtual DbSet<PuestoTecnicoClasificacionArticulo> PuestoTecnicoClasificacionArticulo { get; set; }
        public virtual DbSet<Reingreso> Reingreso { get; set; }
        public virtual DbSet<relcatDistribuidor> relcatDistribuidor { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<Salida> Salida { get; set; }
        public virtual DbSet<Serie> Serie { get; set; }
        public virtual DbSet<SerieTipoArticulo> SerieTipoArticulo { get; set; }
        public virtual DbSet<Tbl_proceso_Internet> Tbl_proceso_Internet { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<vehiculo> vehiculo { get; set; }
        public virtual DbSet<CatTipoPago> CatTipoPago { get; set; }
        public virtual DbSet<CatTipoServicios> CatTipoServicios { get; set; }
        public virtual DbSet<Rel_traspaso_proceso> Rel_traspaso_proceso { get; set; }
        public virtual DbSet<Tbl_almacen_servicio> Tbl_almacen_servicio { get; set; }
        public virtual DbSet<Tbl_articulo_servicio> Tbl_articulo_servicio { get; set; }
        public virtual DbSet<tbl_bitacoraTraspaso> tbl_bitacoraTraspaso { get; set; }
        public virtual DbSet<Tbl_MovimientoDevolucionTecnicos> Tbl_MovimientoDevolucionTecnicos { get; set; }
        public virtual DbSet<tbl_traspasoContratos> tbl_traspasoContratos { get; set; }
        public virtual DbSet<RelTecnicoDistribuidor> RelTecnicoDistribuidor { get; set; }
        //public virtual DbSet<Servicio> Servicio { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Articulo>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Articulo>()
                .HasMany(e => e.ArticuloProveedor)
                .WithRequired(e => e.Articulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Articulo>()
                .HasMany(e => e.Pagare)
                .WithRequired(e => e.Articulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Articulo>()
                .HasMany(e => e.ProcesoArticulo)
                .WithRequired(e => e.Articulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BitacoraSofTV>()
                .Property(e => e.TipoServicio)
                .IsUnicode(false);

            modelBuilder.Entity<BitacoraSofTV>()
                .Property(e => e.Contrato)
                .IsUnicode(false);

            modelBuilder.Entity<BitacoraSofTV>()
                .Property(e => e.NombreUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<BitacoraSofTV>()
                .HasMany(e => e.BitacoraSofTVDetalle)
                .WithRequired(e => e.BitacoraSofTV)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BitacoraSofTVDetalle>()
                .Property(e => e.usuariosoft)
                .IsUnicode(false);

            modelBuilder.Entity<catBanco>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catClasificacionArticulo>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catClasificacionArticulo>()
                .HasMany(e => e.catTipoArticulo)
                .WithRequired(e => e.catClasificacionArticulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catClasificacionArticulo>()
                .HasMany(e => e.PuestoTecnicoClasificacionArticulo)
                .WithRequired(e => e.catClasificacionArticulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catDiaFestivo>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.RFC)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.Lada1)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.Telefono1)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.Lada2)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.Telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .Property(e => e.NombreContacto)
                .IsUnicode(false);

            modelBuilder.Entity<catDistribuidor>()
                .HasMany(e => e.Pagare)
                .WithRequired(e => e.catDistribuidor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catDistribuidor>()
                .HasMany(e => e.Usuario)
                .WithOptional(e => e.catDistribuidor)
                .HasForeignKey(e => e.DistribuidorClave);

            modelBuilder.Entity<catEntidadTipo>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catEntidadTipo>()
                .HasMany(e => e.Entidad)
                .WithRequired(e => e.catEntidadTipo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catEstatus>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catFalla>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.RFC)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.Lada1)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.Telefono1)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.Lada2)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.Telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .Property(e => e.NombreContacto)
                .IsUnicode(false);

            modelBuilder.Entity<catProveedor>()
                .HasMany(e => e.ArticuloProveedor)
                .WithRequired(e => e.catProveedor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catPuestoTecnico>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catPuestoTecnico>()
                .HasMany(e => e.PuestoTecnico)
                .WithRequired(e => e.catPuestoTecnico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catPuestoTecnico>()
                .HasMany(e => e.PuestoTecnicoClasificacionArticulo)
                .WithRequired(e => e.catPuestoTecnico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catSerie>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catSerie>()
                .HasMany(e => e.catTipoArticulo)
                .WithOptional(e => e.catSerie)
                .HasForeignKey(e => e.catSerieClaveInterface);

            modelBuilder.Entity<catSerie>()
                .HasMany(e => e.Serie)
                .WithRequired(e => e.catSerie)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catSerie>()
                .HasMany(e => e.SerieTipoArticulo)
                .WithRequired(e => e.catSerie)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catTipoArticulo>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catTipoArticulo>()
                .Property(e => e.Letra)
                .IsUnicode(false);

            modelBuilder.Entity<catTipoArticulo>()
                .HasMany(e => e.Articulo)
                .WithRequired(e => e.catTipoArticulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catTipoArticulo>()
                .HasMany(e => e.SerieTipoArticulo)
                .WithRequired(e => e.catTipoArticulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catTipoMoneda>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catTipoProceso>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catTipoProceso>()
                .Property(e => e.IO)
                .IsUnicode(false);

            modelBuilder.Entity<catTipoSalida>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catTransportista>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<catTransportista>()
                .Property(e => e.URLSeguimiento)
                .IsUnicode(false);

            modelBuilder.Entity<catTransportista>()
                .HasMany(e => e.ProcesoEnvio)
                .WithRequired(e => e.catTransportista)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<catUbicacion>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<catUnidad>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Configuracion>()
                .Property(e => e.Parametro)
                .IsUnicode(false);

            modelBuilder.Entity<Configuracion>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Configuracion>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleVehiculo>()
                .Property(e => e.color)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleVehiculo>()
                .Property(e => e.Modelo)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleVehiculo>()
                .Property(e => e.Marca)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleVehiculo>()
                .Property(e => e.Especificaciones)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.CalleNumero)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.Colonia)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.CodigoPostal)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.Ciudad)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.Pais)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.Lat)
                .IsUnicode(false);

            modelBuilder.Entity<Domicilio>()
                .Property(e => e.Lon)
                .IsUnicode(false);

            modelBuilder.Entity<Entidad>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Entidad>()
                .HasMany(e => e.AmbitoAlmacen)
                .WithOptional(e => e.Entidad)
                .HasForeignKey(e => e.EntidadClaveTecnico);

            modelBuilder.Entity<Entidad>()
                .HasMany(e => e.AmbitoAlmacen1)
                .WithOptional(e => e.Entidad1)
                .HasForeignKey(e => e.EntidadClaveAlmacen);

            modelBuilder.Entity<Entidad>()
                .HasMany(e => e.BitacoraSofTV)
                .WithOptional(e => e.Entidad)
                .HasForeignKey(e => e.EntidadClaveTecnico);

            modelBuilder.Entity<Entidad>()
                .HasMany(e => e.BitacoraSofTV1)
                .WithOptional(e => e.Entidad1)
                .HasForeignKey(e => e.EntidadClaveAlmacen);

            modelBuilder.Entity<Entidad>()
                .HasMany(e => e.Entidad1)
                .WithOptional(e => e.Entidad2)
                .HasForeignKey(e => e.EntidadPadreClave);

            modelBuilder.Entity<Entidad>()
                .HasMany(e => e.Proceso)
                .WithOptional(e => e.Entidad)
                .HasForeignKey(e => e.EntidadClaveSolicitante);

            modelBuilder.Entity<Entidad>()
                .HasOptional(e => e.PuestoTecnico)
                .WithRequired(e => e.Entidad);

            modelBuilder.Entity<GrupoConfiguracion>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<GrupoConfiguracion>()
                .HasMany(e => e.Configuracion)
                .WithRequired(e => e.GrupoConfiguracion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Inventario>()
                .HasMany(e => e.BitacoraSofTVDetalle)
                .WithRequired(e => e.Inventario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Inventario>()
                .HasMany(e => e.ProcesoInventario)
                .WithRequired(e => e.Inventario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Inventario>()
                .HasMany(e => e.Serie)
                .WithRequired(e => e.Inventario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.Icono)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .HasMany(e => e.Permiso)
                .WithRequired(e => e.Modulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PedidoGastos>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<PedidoPago>()
                .Property(e => e.Forma)
                .IsUnicode(false);

            modelBuilder.Entity<PedidoPago>()
                .Property(e => e.Banco)
                .IsUnicode(false);

            modelBuilder.Entity<PedidoPago>()
                .Property(e => e.NumeroCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<Proceso>()
                .Property(e => e.Observaciones)
                .IsUnicode(false);

            modelBuilder.Entity<Proceso>()
                .Property(e => e.BitacoraLetra)
                .IsUnicode(false);

            modelBuilder.Entity<Proceso>()
                .Property(e => e.BitacoraNumero)
                .IsUnicode(false);

            modelBuilder.Entity<Proceso>()
                .HasOptional(e => e.PedidoDetalle)
                .WithRequired(e => e.Proceso);

            modelBuilder.Entity<Proceso>()
                .HasMany(e => e.PedidoGastos)
                .WithOptional(e => e.Proceso)
                .HasForeignKey(e => e.ProcesoClavePedido);

            modelBuilder.Entity<Proceso>()
                .HasOptional(e => e.PedidoPago)
                .WithRequired(e => e.Proceso);

            modelBuilder.Entity<Proceso>()
                .HasMany(e => e.Proceso1)
                .WithOptional(e => e.Proceso2)
                .HasForeignKey(e => e.ProcesoClaveRespuesta);

            modelBuilder.Entity<Proceso>()
                .HasMany(e => e.ProcesoArticulo)
                .WithRequired(e => e.Proceso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proceso>()
                .HasOptional(e => e.ProcesoEnvio)
                .WithRequired(e => e.Proceso);

            modelBuilder.Entity<Proceso>()
                .HasMany(e => e.ProcesoInventario)
                .WithRequired(e => e.Proceso)
                .HasForeignKey(e => e.ProcesoClave)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proceso>()
                .HasMany(e => e.ProcesoInventario1)
                .WithOptional(e => e.Proceso1)
                .HasForeignKey(e => e.Pedido);

            modelBuilder.Entity<Proceso>()
                .HasOptional(e => e.Reingreso)
                .WithRequired(e => e.Proceso);

            modelBuilder.Entity<ProcesoEnvio>()
                .Property(e => e.GuiaEnvio)
                .IsUnicode(false);

            modelBuilder.Entity<Reingreso>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Rol>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.Permiso)
                .WithRequired(e => e.Rol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.Usuario)
                .WithRequired(e => e.Rol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Salida>()
                .Property(e => e.SalidaClave)
                .IsFixedLength();

            modelBuilder.Entity<Serie>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.CorreoElectronico)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Firma)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Proceso)
                .WithOptional(e => e.Usuario)
                .HasForeignKey(e => e.UsuarioClaveAutorizacion);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Proceso1)
                .WithOptional(e => e.Usuario1)
                .HasForeignKey(e => e.UsuarioClave);

            modelBuilder.Entity<vehiculo>()
                .Property(e => e.conductorTitular)
                .IsUnicode(false);

            modelBuilder.Entity<vehiculo>()
                .Property(e => e.Serie)
                .IsUnicode(false);

            modelBuilder.Entity<vehiculo>()
                .Property(e => e.Placas)
                .IsUnicode(false);

            modelBuilder.Entity<vehiculo>()
                .Property(e => e.Poliza)
                .IsUnicode(false);

            modelBuilder.Entity<vehiculo>()
                .Property(e => e.DistribuidorClave)
                .IsUnicode(false);

            modelBuilder.Entity<vehiculo>()
                .Property(e => e.PlazaEncierro)
                .IsUnicode(false);

            modelBuilder.Entity<CatTipoPago>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<CatTipoServicios>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Rel_traspaso_proceso>()
                .Property(e => e.ProcesoClave)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_bitacoraTraspaso>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_bitacoraTraspaso>()
                .Property(e => e.Error)
                .IsUnicode(false);

            modelBuilder.Entity<Tbl_MovimientoDevolucionTecnicos>()
                .Property(e => e.Valor)
                .IsUnicode(false);
        }
    }
}
