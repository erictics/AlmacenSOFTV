namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PedidoGastos
    {
        [Key]
        public Guid GastoClave { get; set; }

        public Guid? ProcesoClavePedido { get; set; }

        [StringLength(250)]
        public string Concepto { get; set; }

        public decimal? Costo { get; set; }

        public int? catTipoMonedaClave { get; set; }

        public virtual catTipoMoneda catTipoMoneda { get; set; }

        public virtual Proceso Proceso { get; set; }
    }
}
