namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Articulo")]
    public partial class Articulo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Articulo()
        {
            ArticuloProveedor = new HashSet<ArticuloProveedor>();
            Inventario = new HashSet<Inventario>();
            Pagare = new HashSet<Pagare>();
            ProcesoArticulo = new HashSet<ProcesoArticulo>();
        }

        [Key]
        public Guid ArticuloClave { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdArticulo { get; set; }

        public int catTipoArticuloClave { get; set; }

        [StringLength(150)]
        public string Nombre { get; set; }

        public int? catUnidadClave { get; set; }

        public int? Existencias { get; set; }

        public int? TopeMinimo { get; set; }

        public int? Porcentaje { get; set; }

        public int? catUbicacionClave { get; set; }

        public decimal? PrecioPagare { get; set; }

        public int? catTipoMonedaClave { get; set; }

        public bool? Activo { get; set; }

        //public string parte { get; set; }

        public virtual catTipoArticulo catTipoArticulo { get; set; }

        public virtual catTipoMoneda catTipoMoneda { get; set; }

        public virtual catUbicacion catUbicacion { get; set; }

        public virtual catUnidad catUnidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ArticuloProveedor> ArticuloProveedor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inventario> Inventario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagare> Pagare { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcesoArticulo> ProcesoArticulo { get; set; }
    }
}
