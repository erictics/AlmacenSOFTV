namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProcesoInventario")]
    public partial class ProcesoInventario
    {
        [Key]
        [Column(Order = 0)]
        public Guid ProcesoClave { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid InventarioClave { get; set; }

        public Guid? Pedido { get; set; }

        public int? Cantidad { get; set; }

        public DateTime? Fecha { get; set; }

        public virtual Inventario Inventario { get; set; }

        public virtual Proceso Proceso { get; set; }

        public virtual Proceso Proceso1 { get; set; }
    }
}
