namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Configuracion")]
    public partial class Configuracion
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GrupoConfiguracionClave { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(150)]
        public string Parametro { get; set; }

        [StringLength(250)]
        public string Nombre { get; set; }

        [StringLength(550)]
        public string Valor { get; set; }

        public virtual GrupoConfiguracion GrupoConfiguracion { get; set; }
    }
}
