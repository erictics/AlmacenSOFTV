namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("relcatDistribuidor")]
    public partial class relcatDistribuidor
    {
        [Key]
        [Column(Order = 0)]
        public Guid catDistribuidorClave { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int iddistribuidor { get; set; }
    }
}
