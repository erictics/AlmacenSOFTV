namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catFalla")]
    public partial class catFalla
    {
        [Key]
        public int catFallaClave { get; set; }

        [Required]
        [StringLength(250)]
        public string Descripcion { get; set; }

        public bool Activo { get; set; }
    }
}
