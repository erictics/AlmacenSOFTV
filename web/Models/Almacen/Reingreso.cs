namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reingreso")]
    public partial class Reingreso
    {
        [Key]
        public Guid ProcesoClave { get; set; }

        public int? NumeroOrden { get; set; }

        [StringLength(8)]
        public string Usuario { get; set; }

        public DateTime? Fecha { get; set; }

        public virtual Proceso Proceso { get; set; }
    }
}
