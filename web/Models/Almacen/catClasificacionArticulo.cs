namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catClasificacionArticulo")]
    public partial class catClasificacionArticulo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public catClasificacionArticulo()
        {
            catTipoArticulo = new HashSet<catTipoArticulo>();
            PuestoTecnicoClasificacionArticulo = new HashSet<PuestoTecnicoClasificacionArticulo>();
        }

        [Key]
        public int catClasificacionArticuloClave { get; set; }

        [Required]
        [StringLength(250)]
        public string Descripcion { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<catTipoArticulo> catTipoArticulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PuestoTecnicoClasificacionArticulo> PuestoTecnicoClasificacionArticulo { get; set; }
    }
}
