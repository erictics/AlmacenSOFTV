namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PuestoTecnicoClasificacionArticulo")]
    public partial class PuestoTecnicoClasificacionArticulo
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int catPuestosTecnicosClave { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int catClasificacionArticuloClave { get; set; }

        public bool Activo { get; set; }

        public virtual catClasificacionArticulo catClasificacionArticulo { get; set; }

        public virtual catPuestoTecnico catPuestoTecnico { get; set; }
    }
}
