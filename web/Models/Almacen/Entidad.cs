namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Entidad")]
    public partial class Entidad
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Entidad()
        {
            AmbitoAlmacen = new HashSet<AmbitoAlmacen>();
            AmbitoAlmacen1 = new HashSet<AmbitoAlmacen>();
            BitacoraSofTV = new HashSet<BitacoraSofTV>();
            BitacoraSofTV1 = new HashSet<BitacoraSofTV>();
            catUbicacion = new HashSet<catUbicacion>();
            Entidad1 = new HashSet<Entidad>();
            Inventario = new HashSet<Inventario>();
            Proceso = new HashSet<Proceso>();
        }

        [Key]
        public Guid EntidadClave { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdEntidad { get; set; }

        [Required]
        [StringLength(150)]
        public string Nombre { get; set; }

        public int catEntidadTipoClave { get; set; }

        public Guid? catDistribuidorClave { get; set; }

        public Guid? EntidadPadreClave { get; set; }

        public bool? Activo { get; set; }

        public int idEntidadSofTV { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AmbitoAlmacen> AmbitoAlmacen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AmbitoAlmacen> AmbitoAlmacen1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BitacoraSofTV> BitacoraSofTV { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BitacoraSofTV> BitacoraSofTV1 { get; set; }

        public virtual catDistribuidor catDistribuidor { get; set; }

        public virtual catEntidadTipo catEntidadTipo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<catUbicacion> catUbicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Entidad> Entidad1 { get; set; }

        public virtual Entidad Entidad2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inventario> Inventario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proceso> Proceso { get; set; }

        public virtual PuestoTecnico PuestoTecnico { get; set; }
    }
}
