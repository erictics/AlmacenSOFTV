namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Serie")]
    public partial class Serie
    {
        [Key]
        [Column(Order = 0)]
        public Guid InventarioClave { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int catSerieClave { get; set; }

        [StringLength(250)]
        public string Valor { get; set; }

        public virtual catSerie catSerie { get; set; }

        public virtual Inventario Inventario { get; set; }
    }
}
