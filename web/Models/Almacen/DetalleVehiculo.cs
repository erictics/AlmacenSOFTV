namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleVehiculo")]
    public partial class DetalleVehiculo
    {
        [StringLength(20)]
        public string color { get; set; }

        [StringLength(30)]
        public string Modelo { get; set; }

        [StringLength(30)]
        public string Marca { get; set; }

        [StringLength(100)]
        public string Especificaciones { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdVehiculo { get; set; }
    }
}
