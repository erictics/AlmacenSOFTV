namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("catDiaFestivo")]
    public partial class catDiaFestivo
    {
        [Key]
        public int catDiaFestivoClave { get; set; }

        public DateTime Fecha { get; set; }

        [Required]
        [StringLength(250)]
        public string Descripcion { get; set; }

        public bool Activo { get; set; }
    }
}
