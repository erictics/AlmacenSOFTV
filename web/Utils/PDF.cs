﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using web.Models;

namespace web.Utils
{
    public class PDF
    {
        public void HTMLToPdf(string HTML, string FilePath, Proceso proceso)
        {
            Document document = new Document();

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(FilePath, FileMode.Create));
            document.Open();

            iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
            styles.LoadTagStyle("table", "border", "1");
            styles.LoadTagStyle("table", "borderColor", "#CCCCCC");
            styles.LoadStyle("red", " borderColor", "#ff0000");

            iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document, null, styles);
            hw.Parse(new StringReader(HTML));



            try
            {

                Usuario autorizo = null;

                try
                {
                    autorizo = proceso.Usuario;
                }
                catch
                { }


                Usuario elaboro = null;
                try
                {
                    elaboro = proceso.Usuario1;
                }
                catch
                { }

                if (elaboro != null)
                {
                    PdfContentByte cb = writer.DirectContent;
                    ColumnText ct = new ColumnText(cb);

                    cb.BeginText();
                    cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10.0f);
                    cb.SetTextMatrix(document.LeftMargin, document.BottomMargin);

                    if (proceso.catTipoProcesoClave == 2)
                    {
                        cb.ShowText(String.Format("{0} {1}", "Recibió:", elaboro.Nombre));
                    }
                    else
                    {
                        cb.ShowText(String.Format("{0} {1}", "Elaboró:", elaboro.Nombre));
                    }
                    cb.EndText();

                    if (elaboro.Firma != null && elaboro.Firma != String.Empty)
                    {
                        string imageURL = HttpContext.Current.Server.MapPath("/Content") + "/media/" + elaboro.Firma;

                        if (File.Exists(imageURL))
                        {
                            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                            //Resize image depend upon your need
                            jpg.ScaleToFit(140f, 120f);
                            jpg.SetAbsolutePosition(10f, 50f);
                            document.Add(jpg);
                        }
                    }
                }

                if (proceso != null && proceso.catTipoProcesoClave == 3)
                {
                    ModeloAlmacen dc = new ModeloAlmacen();
                    Guid gtecnico = proceso.EntidadClaveSolicitante.Value;
                    Entidad tecnico = (from t in dc.Entidad
                                       where t.EntidadClave == gtecnico
                                       && t.catEntidadTipoClave == 2
                                       select t).SingleOrDefault();
                    if (tecnico != null)
                    {
                        PdfContentByte cb = writer.DirectContent;
                        ColumnText ct = new ColumnText(cb);

                        cb.BeginText();
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10.0f);
                        cb.SetTextMatrix(450, document.BottomMargin);
                        cb.ShowText(String.Format("{0} {1}", "Técnico:", tecnico.Nombre));
                        cb.EndText();
                    }
                }

                if (proceso != null && proceso.catEstatusClave > 1 && autorizo != null)
                {
                    PdfContentByte cb2 = writer.DirectContent;
                    ColumnText ct2 = new ColumnText(cb2);
                    cb2.BeginText();
                    cb2.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10.0f);
                    cb2.SetTextMatrix(450, document.BottomMargin);
                    cb2.ShowText(String.Format("{0} {1}", "Autorizó:", autorizo.Nombre));
                    cb2.EndText();


                    if (autorizo.Firma != null)
                    {
                        string imageURL = HttpContext.Current.Server.MapPath("/Content") + "/media/" + autorizo.Firma;

                        if (File.Exists(imageURL))
                        {
                            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                            //Resize image depend upon your need
                            jpg.ScaleToFit(140f, 120f);
                            jpg.SetAbsolutePosition(450f, 50f);
                            document.Add(jpg);
                        }
                    }

                }
            }
            catch { }


            document.Close();
            //ShowPdf("Chap0101.pdf");
        }

        public void HTMLToPdfBitacora(string HTML, string FilePath, BitacoraSofTV proceso)
        {
            Document document = new Document();

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(FilePath, FileMode.Create));
            document.Open();

            iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
            styles.LoadTagStyle("table", "border", "1");
            styles.LoadTagStyle("table", "borderColor", "#CCCCCC");

            iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document, null, styles);
            hw.Parse(new StringReader(HTML));


            document.Close();
            //ShowPdf("Chap0101.pdf");
        }
    }
}