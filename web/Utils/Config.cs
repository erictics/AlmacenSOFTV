﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web.Models;

namespace web.Utils
{
    public class Config
    {
        public static string Dame(string cual)
        {
            ModeloAlmacen dc = new ModeloAlmacen();
            Configuracion config = (from c in dc.Configuracion
                                        where c.Parametro == cual
                                        select c).SingleOrDefault();

            if (config != null)
            {
                return config.Valor;
            }

            return String.Empty;
        }
    }
}