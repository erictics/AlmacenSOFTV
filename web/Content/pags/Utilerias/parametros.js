﻿

$(document).ready(function () {

    LlenaGrid(1);
    LlenaGrid(2);
    LlenaGrid(3);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $("#titulowin").html("Agregar Rol");
        $('#edita').modal('show');
    });

 
    $("#formaedita").submit(function (e) {
        e.preventDefault();

        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var url = '/Parametros/Guarda/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formaedita').serialize(),
                type: "POST",
                success: function (data) {

                    $("#edita").modal('hide');
                    toastr.success('El parámetro se guardó correctamente');

                    LlenaGrid(1);
                },
                error: function (request, error) {
                    toastr.error('Error,Surgió un error, intente nuevamente más tarde');
                }
            });

        }

    });


    $("#formaeditalogo").submit(function (e) {
        e.preventDefault();

        var valid = jQuery('#formaeditalogo').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var url = '/Parametros/GuardaLogo/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formaeditalogo').serialize(),
                type: "POST",
                success: function (data) {

                    $("#editalogo").modal('hide');

                    toastr.success('El logo se guardó correctamente');

                    LlenaGrid(2);
                },
                error: function (request, error) {

                    toastr.error('Error,Surgió un error, intente nuevamente más tarde');
                }
            });

        }

    });


    $("#formaeditacolor").submit(function (e) {
        e.preventDefault();

        var valid = jQuery('#formaeditacolor').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var url = '/Parametros/GuardaColor/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formaeditacolor').serialize(),
                type: "POST",
                success: function (data) {

                    $("#editacolor").modal('hide');
                    

                    toastr.success('El color se guardó correctamente');

                    LlenaGrid(3);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });

        }

    });
});


var _pag = 1;
function LlenaGrid(pag)
{
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;

    var myJSONText = JSON.stringify(objRoles);


    $("#loader").show();
    var url = '/Parametros/ObtenLista/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#loader").hide();

            var arreglo = eval(data);

            if (pag == 1) {
                $("#datos tbody").empty();
                for (var i = 0; i < arreglo.length; i++) {
                    var renglon = "<tr><td>" + (i + 1) + "</td><td>" + arreglo[i].Descripcion + "</td><td>" + arreglo[i].Valor + "</td><td>" + DameAcciones(arreglo[i].Nombre) + "</td></tr>"
                    $("#datos tbody").append(renglon);
                }
                AfterAjax();

            }
            else if (pag == 2) {
                $("#logos tbody").empty();
                for (var i = 0; i < arreglo.length; i++) {
                    var renglon = "<tr><td>" + (i + 1) + "</td><td>" + arreglo[i].Descripcion + "</td><td><img width='100' height='100' src='/Content/media/" + arreglo[i].Valor + "'/></td><td>" + DameAccionesLogo(arreglo[i].Nombre) + "</td></tr>"
                    $("#logos tbody").append(renglon);
                }

                AfterAjaxLogo();

            } else if (pag == 3) {
                $("#colores tbody").empty();
                for (var i = 0; i < arreglo.length; i++) {
                    var renglon = "<tr><td>" + (i + 1) + "</td><td>" + arreglo[i].Descripcion + "</td><td><div style='border:solid 1px #CCC;width:50px;height:50px;background-color:" + arreglo[i].Valor + "'></div></td><td>" + DameAccionesColor(arreglo[i].Nombre) + "</td></tr>"
                    $("#colores tbody").append(renglon);
                }
                AfterAjaxColor();
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });


    url = '/Parametros/ObtenPaginas/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            LlenaPaginacion(data);
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function AfterAjax() {


    $(".edit").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Parametro");
        $('#edita').modal('show');

        var url = '/Parametros/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oParam = eval(data);

                $("#ed_id").val(oParam.Nombre);
                $("#titulowin").html("Edita Parametro " + oParam.Descripcion);
                $("#ed_valor").val(oParam.Valor);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}


function AfterAjaxLogo() {


    $(".editlogo").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowinlogo").html("Edita Parametro");
        $('#editalogo').modal('show');

        var url = '/Parametros/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oParam = eval(data);

                $("#edlogo_id").val(oParam.Nombre);
                $("#titulowinlogo").html("Edita Logo " + oParam.Descripcion);
                $("#edlogo_valor").val(oParam.Valor);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}


function AfterAjaxColor() {


    $(".editcolor").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowincolor").html("Edita Color");
        $('#editacolor').modal('show');

        var url = '/Parametros/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oParam = eval(data);


                $("#edcolor_id").val(oParam.Nombre);
                $("#titulowincolor").html("Edita Color " + oParam.Descripcion);
                $("#edcolor_valor").val(oParam.Valor);
                $("#edcolor").css("background-color",oParam.Valor);


                $('.color').data('color', oParam.Valor);
                $('.color .span2').val(oParam.Valor);
                $('.color i').css('background-color', oParam.Valor);

                //$('.color').setColor(oParam.Valor);
                $('.color').colorpicker();

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}


function DameAcciones(elid)
{
    return "<button type='button' class='btn btn-warning btn-xs edit' rel='" + elid + "'><i class='fa fa-pencil'></i> Editar</button>";
}

function DameAccionesLogo(elid) {
    return "<button type='button' class='btn btn-warning btn-xs editlogo' rel='" + elid + "'><i class='fa fa-pencil'></i> Editar</button>";
}

function DameAccionesColor(elid) {
    return "<button type='button' class='btn btn-warning btn-xs editcolor' rel='" + elid + "'><i class='fa fa-pencil'></i> Editar</button>";
}

