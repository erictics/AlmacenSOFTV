﻿

$(document).ready(function () {

    LlenaGrid(1);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $("#titulowin").html("Agregar Unidad");
        $('#edita').modal('show');
        $("#ed_id").val('');
        $("#ed_nombre").val('');
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Unidad/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {

                $("#elimina").modal('hide');
                toastr.success('La Unidad se desactivó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#formaactiva").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Unidad/Activa/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaactiva').serialize(),
            type: "POST",
            success: function (data) {

                $("#activa").modal('hide');
                toastr.success('La unidad se activó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#formaedita").submit(function (e) {
        e.preventDefault();

        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var url = '/Unidad/Guarda/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formaedita').serialize(),
                type: "POST",
                success: function (data) {

                    if (data == "-1")
                    {
                        toastr.error('Error,El elemento ya está registrado en el catálogo');
                        return;
                    }

                    $("#edita").modal('hide');
                    toastr.success('La Unidad se guardó correctamente');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Error,Surgió un error, intente nuevamente más tarde');
                }
            });

        }

    });

});


var _pag = 1;
function LlenaGrid(pag)
{
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;

    var myJSONText = JSON.stringify(objRoles);

    $("#datos tbody").empty();
    $("#loader").show();
    var url = '/Unidad/ObtenLista/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#datos tbody").empty();
            $("#loader").hide();

            var arreglo = eval(data);
            if (arreglo.length > 0) {
                $("#datos").show();
            }
            else {
                $("#resultados").html("No se encontraron resultados");
            }


            for (var i = 0; i < arreglo.length; i++) {
                var renglon = "<tr><td>" + (i + 1) + "</td><td>";

                if (arreglo[i].Activo == 1) {
                    renglon += arreglo[i].Nombre;
                }
                else {
                    renglon += "<s>" + arreglo[i].Nombre + "</s>";
                }

                renglon += "</td>";
                renglon += "<td>" + DameActivar(arreglo[i].Clave, arreglo[i].Activo) + "</td>";
                renglon += "<td>" + DameAcciones(arreglo[i].Clave, arreglo[i].Activo) + "</td>";
                renglon += "</tr>"
                $("#datos tbody").append(renglon);
            }

            AfterAjax();
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });


    url = '/Unidad/ObtenPaginas/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            LlenaPaginacion(data);
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

function AfterAjax() {

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Elimina Unidad");
        $('#elimina').modal('show');

        var url = '/Unidad/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#del_id").val(oRol.Clave);
                $("#titulowindel").html("Desactiva Unidad: " + oRol.Nombre);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    });


    $(".activar").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowinactiva").html("Activar Unidad");
        $('#activa').modal('show');

        var url = '/Unidad/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#activa_id").val(oRol.Clave);
                $("#titulowinactiva").html("Activa Unidad: " + oRol.Descripcion);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    });

    $(".edit").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Unidad");
        $('#edita').modal('show');

        var url = '/Unidad/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oRol = eval(data);

                $("#ed_id").val(oRol.Clave);
                $("#ed_nombre").val(oRol.Nombre);
                $("#ed_activo").val(oRol.Activo);
                if (oRol.Activo == "True" || oRol.Activo=="1") {
                    $("#ed_activo").prop('checked', 'checked');
                    $("#ed_activo").parent().addClass('checked');
                }
                else {
                    $("#ed_activo").removeProp('checked');
                    $("#ed_activo").parent().removeClass('checked');
                }


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}

function DameActivar(elid, activo) {
    var accion = "";

    if (activo == 1) {
        accion += "<img src='/Content/assets/img/activo.png'>";
    }
    else {
        accion += "<img src='/Content/assets/img/inactivo.png'>";
    }

    return accion;
}

function DameAcciones(elid, activo) {
    var accion = "<button type='button' class='btn btn-warning  btn-xs edit' rel='" + elid + "'><i class='fa fa-pencil'></i> Editar</button>";

    return accion;
}