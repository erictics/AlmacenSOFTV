﻿

function DameAccionesArticuloSinPedido(elid, i) {
    var regresa = "";
    regresa += "<button type='button' data-indice='" + i + "' class='btn btn-warning btn-xs editasinpedido' rel='" + elid + "'> <i class='fa fa-pencil' aria-hidden='true'></i>&nbsp;Editar</button>"
    regresa += "<button type='button' data-indice='" + i + "' class='btn btn-danger btn-xs eliminasinpedido' rel='" + elid + "' style='display:none'><i class='fa fa-trash-o' aria-hidden='true'></i>&nbsp;Elimina</button>";
    return regresa;
}

function DameAccionesArticulo(cual) {
    return "<button type='button' class='btn btn-danger btn-xs delarticulo' rel='" + cual + "'>&nbsp;Elimina</button>";
}



function DamePedido(elid, numero) {


    if (numero == null) {
        return "<button type='button' class='btn btn-xs ink-reaction btn-default-bright'>Sin orden de compra</button>"
    }
    else {
                
        return "<button type='button' class='btn btn-xs ink-reaction btn-default-bright ver' rel='" + elid + "'>&nbsp; Orden (" + numero + ")</button>"
            + "<button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-danger pdf' data-toggle='tooltip' data-placement='right' title='' data-original-title='Descarga PDF pedido'"
            + "rel='" + elid + "'><i class='md md-file-download' aria-hidden='true'></i></button>";
    }
}


function DameAcciones(elid) {
    return "<button type='button' class='btn btn-info btn-xs autoriza' rel='" + elid + "'>&nbsp;Recibir Material</button>";
}


function DetalleSelecciona(elid, i, pedidonumero) {
    return "<button type='button' data-indice='" + i + "' class='btn ink-reaction btn-success btn-xs btn-info seleccionarpedido' data-toggle='tooltip' data-placement='right' data-original-title='Selecciona la orden de compra para surtirla'   data-numeropedido='" + pedidonumero + "' rel='" + elid + "'><i class='fa fa-check'></i> Selecciona pedido</button>"
    
}

function DetalleRecepcion(tieneseries, elid, i, max, nombreproducto) {
    var regresa = "";
    if (tieneseries > 0) {
        regresa += "<td><button type='button' data-indice='" + i + "' class='btn btn-info btn-xs recibir' data-nombreproducto='" + nombreproducto + "' rel='" + elid + "'><i class='fa fa-check-square' aria-hidden='true'></i> Recibir Material</button></td><td><button type='button' data-indice='" + i + "' class='btn btn-warning btn-xs recibircm' style='background-color: #0d4984; border-color: #0736a9;' data-nombreproducto='" + nombreproducto + "' rel='" + elid + "'>&nbsp;<i class='fa fa-check-square' aria-hidden='true'></i> Carga masiva</button></td>";
    }
    else {
        regresa += "<td><input type='' size=3 id='qty_" + i + "' data-indice='" + i + "' rel='" + elid + "' class='recibegenerico validate[custom[integer],min[0],max[" + max + "]]'></td>";
    }

    return regresa;
}

function LimpiaRecepcion(tieneseries, elid, i) {
    var regresa = '';
   // regresa += "<button type='button' data-indice='" + i + "' class='btn btn-warning btn-xs limpia' rel='" + elid + "'>&nbsp;Limpia</button>";
    return regresa;
}
