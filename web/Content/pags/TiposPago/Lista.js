﻿
$(document).ready(function () {

    llenaLista();

});


$('#agrega').click(function () {

    $('#guarda').show();
    $('#btnedita').hide();

    $('#titulowin').empty().append("Agrega Tipo de pago");
    $('#ed_nombre').val('');
    $('#ed_id').val('');
    $('#edita').modal('show');
    $("[type=checkbox]").removeProp('checked');
    $("[type=checkbox]").parent().removeClass('checked');

    

})


$('#btnedita').click(function () {

    TipoPago = {};
    TipoPago.Idpago = $("#ed_id").val();
    TipoPago.Descripcion = $('#ed_nombre').val();
    var activo = false;

    var activo = $(".activo").parent('[class*="icheckbox"]').hasClass("checked");
    if (activo) {
        TipoPago.Activo = true;

    } else {
        TipoPago.Activo = false;
    }

    var activoc = $(".cuenta").parent('[class*="icheckbox"]').hasClass("checked");
    if (activoc) {
        TipoPago.Cuenta = true;

    } else {
        TipoPago.Cuenta = false;
    }


    var url = '/TipoPago/EditaTipoPago/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: TipoPago,
        type: "POST",
        success: function (data) {

            if (data == "0") {
                toastr.error("No se pudo agregar el tipo de pago");
            } else if (data == "1") {
                llenaLista();
                $('#edita').modal('hide');
                toastr.success("El tipo de pago se editó corectamente");
            }
            else {
                toastr.error("No se pudo editar el tipo de pago");
            }

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }



    });


});


$('#guarda').click(function () {

    TipoPago = {};    
    TipoPago.Descripcion = $('#ed_nombre').val();
    var activo = false;
   
    var activo = $(".activo").parent('[class*="icheckbox"]').hasClass("checked");
    if (activo) {
        TipoPago.Activo = true;

    } else {
        TipoPago.Activo = false;
    }

    var activoc = $(".cuenta").parent('[class*="icheckbox"]').hasClass("checked");
    if (activoc) {
        TipoPago.Cuenta = true;

    } else {
        TipoPago.Cuenta = false;
    }  

    var url = '/TipoPago/AgregaTipoPago/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: TipoPago,
        type: "POST",
        success: function (data) {

            if (data == "0") {
                toastr.error("No se pudo agregar el tipo de pago");
            } else if (data == "1") {
                llenaLista();
                $('#edita').modal('hide');
                toastr.success("El tipo de pago se agregó corectamente");
            }
            else {
                toastr.error("No se pudo agregar el tipo de pago");
            }

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }



    });


});

    function edita(id){

        $('#guarda').hide();
        $('#btnedita').show();
        $("#titulowin").empty().append("Edita Tipo de Salida");
        $('#edita').modal('show');

        var url = '/TipoPago/ObtenTipoPago/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'id': id },
            type: "POST",
            success: function (data) {
                console.log(data);
                var oRol = eval(data);

                $("#ed_id").val(oRol.Idpago);
                $("#ed_nombre").val(oRol.Descripcion);

                $("[type=checkbox]").removeProp('checked');
                $("[type=checkbox]").parent().removeClass('checked');

                if (oRol.Activo ==true) {
                    $("#ed_activa").prop('checked', 'checked');
                    $("#ed_activa").parent().addClass('checked');
                }

                if (oRol.Cuenta == true) {
                    $("#ed_cuenta").prop('checked', 'checked');
                    $("#ed_cuenta").parent().addClass('checked');
                }

            },
            error: function (request, error) {
                toastr.error('Sucedio un error de conexión /su sesión ha expirado');
            }
        });

    };




    function llenaLista() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bDestroy": true,
            "info": true,
            "stateSave": false,
            "lengthMenu": [[5], [5]],
            "ajax": {
                "url": "/TipoPago/ObtenLista/",
                "type": "POST",
                "data": {
                    'id': 1
                }
            },
            "fnInitComplete": function (oSettings, json) {

            },
            "fnDrawCallback": function (oSettings) {

            },

            "columns": [
               
                    { "data": "Descripcion", "orderable": false },
                    {
                        sortable: false,
                        "render": function (data, type, full, meta) {
                            console.log(full);
                            if (full.Activo === true) {
                                return "<img src='/Content/assets/img/activo.png'>";
                            }
                            else {
                                return "<img src='/Content/assets/img/inactivo.png'>";
                            }
                        }

                    },
                   
                    {
                        sortable: false,
                        "render": function (data, type, full, meta) {
                            return '<button type="button" class="btn btn-warning btn-xs edit" onClick="edita(\'' + full.Idpago + '\')" ><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Editar</button>';

                        }

                    },




            ],

            language: {

                search: "Buscar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ orden",
                info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
                infoEmpty: "No hay elementos para mostrar",
                infoFiltered: "(filtrados _MAX_ )",
                infoPostFix: "",
                //loadingRecords: "Búsqueda en curso...",
                zeroRecords: "No hay elementos para mostrar",
                emptyTable: "No hay elementos disponibles",
                paginate: {
                    first: "Primera",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultima"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },


            "order": [[0, "asc"]]
        })

    }