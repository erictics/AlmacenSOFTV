﻿$('#cla').change(function () {
    var id = $('#cla').val();
    $('#tip').empty().append("<option disabled selected>Selecciona</option>");
    var url = '/ReporteSurtidoDistribuidor/OntenerTipoArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id':id  },
        type: "POST",
        success: function (data) {
            $('#tip').append("<option value='-1'>Todos los tipos de articulos</option>");
            for (var a = 0; a < data.length; a++) {
                $('#tip').append("<option value="+data[a].catTipoArticuloClave+">"+data[a].Descripcion+"</option>");
            }

            
        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

});


$('#tip').change(function () {
    var id = $('#tip').val();
    $('#articulos').empty().append("<option disabled selected>Selecciona</option>");
    var url = '/ReporteSurtidoDistribuidor/obtenerArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
            
            for (var a = 0; a < data.length; a++) {
                $('#articulos').append("<option value=" + data[a].ArticuloClave + ">" + data[a].Nombre + "</option>");
            }


        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

});




$('#excel').click(function(){
   

    if ($('#cla').val() == null || $('#cla').val() == "") {
        toastr.warning("Atención", "Selecciona clasificación");
    }
    else if ($('#tip').val() == null || $('#tip').val() == "") {
        toastr.warning("Atención", "Selecciona tipo de articulo");
    }
    else if (($('#articulos').val() == null || $('#articulos').val() == "") && $('#tip').val() != -1) {

        toastr.warning("Atención", "Selecciona artículo");
    }
    else {
        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            
            var distribuidor = document.getElementById("dis").value;
            var clasificacion = document.getElementById("cla").value;
            var tipo = document.getElementById("tip").value;
            var articulo = document.getElementById("articulos").value;
            var fechainicio = document.getElementById("fechainicio").value;
            var fechafin = document.getElementById("fechafin").value;


            window.location.href = '/ReporteSurtidoDistribuidor/excelDetalle?pdf=2&distribuidor=' + distribuidor + '&clasificacion=' + clasificacion
                                + '&tipo=' + tipo + '&articulo=' + articulo + '&fechainicio=' + fechainicio + '&fechafin=' + fechafin;

            
        }
        else {
            toastr.error('Error', 'Debes de llenar los campos obligatorios');
        }

    }


})

$('#pdf').click(function () {
    


    
    if($('#cla').val()==null || $('#cla').val()==""){
        toastr.error("Atención","Selecciona clasificación");
    }
    else if ($('#tip').val()==null || $('#tip').val()=="") {
        toastr.error("Atención", "Selecciona tipo de articulo");
    }
    else if (($('#articulos').val() == null || $('#articulos').val() == "" )&& $('#tip').val() != -1) {
             
        toastr.error("Atención", "Selecciona artículo");   
    }
    else {
        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            
            var distribuidor = document.getElementById("dis").value;
            var clasificacion = document.getElementById("cla").value;
            var tipo = document.getElementById("tip").value;
            var articulo = document.getElementById("articulos").value;
            var fechainicio = document.getElementById("fechainicio").value;
            var fechafin = document.getElementById("fechafin").value;

           
            window.location.href = '/ReporteSurtidoDistribuidor/Detalle?pdf=2&distribuidor=' + distribuidor + '&clasificacion=' + clasificacion
                                    + '&tipo=' + tipo + '&articulo=' + articulo + '&fechainicio=' + fechainicio + '&fechafin=' + fechafin;

                //$("#loader").show();
                //$("#eliframe").attr('src', '/ReporteSurtidoDistribuidor/Detalle?pdf=1&distribuidor=' + distribuidor + '&clasificacion=' + clasificacion
                //                    + '&tipo=' + tipo + '&articulo=' + articulo + '&fechainicio=' + fechainicio + '&fechafin=' + fechafin);
            



           


        }
        else {
            toastr.error('Error', 'Debes de llenar los campos obligatorios');
        }

    }

   
});