﻿$(window).load(function (e) {
    obtenTecnicos($('#Entidad').val());
});




$('#Entidad').change(function () {

    obtenTecnicos($('#Entidad').val());
});


function obtenTecnicos(id) {


    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": false,
        "error": "Su sesión ha expirado,por favor vuelva a auticarse",
        "stateSave": true,
        "lengthMenu": [[10], [10]],
        "ajax": {
            "url": "/InventarioTecnico/ObtenTecnicos/",
            "type": "POST",
            "data": { 'distribuidor': id },
        },
     
        "columns": [
            { "data": "Nombre", "orderable": false },
            {

                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";
                    renglon += '<button class="btn btn-info ink-reaction  btn-xs"   onClick="reporte(\'' + full.IdEntidad + '\')">inventario</button>'

                    return renglon;

                }

            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    return "";

                }

            },

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

function reporte(id) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Reporte de Inventario");
    $("#framecanvas").attr("src", "/Inventario/ReporteInventario?pdf=2&id=" + id);
}


//function VerInventario(id, Nombre) {
//    $('#ModalInventario').modal('show');
//    $('#tituloInv').empty().append("Inventario de técnico: "+Nombre);
//    $('#tbInventario').empty();

//    var url = '/InventarioTecnico/GetInventario/';
//    $.ajax({
//        url: url,
//        dataType: 'json',
//        data: { 'tecnico': id },
//        type: "POST",
//        success: function (data) {
//            for(var a=0; a<data.length; a++){
//                $('#tbInventario').append("<tr><td>"+data[a].Nombre+"</td><td>"+data[a].enAlmacen+"</td></tr>")
//            }            
//        },
//        error: function (request, error) {
//            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
//        }
//    });




//}