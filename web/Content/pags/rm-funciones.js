﻿
series_repetidas = new Array();

function AgregaConPedido() {
    _conpedido = true;
    _recepcionClave = '';
    $("#at0").click();
    $("#at2").addClass('disabled');
    $("#titulowin").html("Recibir Material");
    $("#buscarproveedor").val('-1');
    $("#datospedido tbody").empty();
    $(".nav-tabs a").first().click();
    $('#edita').modal('show');
    $("#t0").removeClass('active');

    $("#datos2pedido tbody").empty();


    $("#at1").parent().addClass('active');
    $("#at2").parent().removeClass('active');
    $("#at0").parent().removeClass('active');

    $("#at1").parent().show();
    $("#at2").parent().hide();
    $("#at0").parent().hide();

    _objArticulos = new Array();
    series_repetidas = new Array();
    series_no_repetidas = new Array();
    uniqueNames = new Array();
}

function ConPedido_Edita(idrecepcion, idpedido, pedidonumero)
{
    ObtenPedido(idpedido, idrecepcion, pedidonumero);
    $("#edita").modal('show');
}

function CancelaRecepcion() {
    var objCancelaRecepcion = {};
    objCancelaRecepcion.ed_id = _recepcionClave;
    var myJSONText = JSON.stringify(objCancelaRecepcion);  
    var url = '/RecepcionMaterial/Cancela/';
    $.ajax({
        url: url,
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {


            if (data == '200A') {
                toastr.error('No se puede cancelar la recepción, la cantidad de accesorios es mayor a la que se tiene en el inventario. ');
               
            }

            else if (data == '200B')
            {
                toastr.error('El sistema evitó que se cancelara la recepción, alguno de los artículos ya no se encuentran en el almacén central.');
               
               
            }
            else if (data == '0') {
                toastr.error('Un proceso importante se está ejecutando, espere un momento, actualice la página y vuelva a intentarlo. \n Si el problema persiste, contacte al administrador del sitio');
                
            }
            else if (data == '100') {
                toastr.error('La recepción ya se encuentra cancelada, quizá otro usuario la canceló anteriormente, actualice la página y vuelva a intentarlo.');
                
            }
            else if (data == 'ok') {

                $("#eliminarecepcion").modal("hide");
                toastr.error('La recepción se canceló correctamente');               
                LlenaGrid(_pag);
            }
            else {

               
            }

           
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
           
        }
    });
}

function Cancela_Ventana() {
    $("#eliminarecepcion").modal('show');
}

function SinPedido_Edita(idrecepcion) {
    ObtenRecepcion(idrecepcion);
    $("#editasinpedido").modal('show');
}

function AgregaSinPedido() {
    _conpedido = false;
    $("#at0sinpedido").click();
    $("#titulowinsinpedido").html("Recibir material sin Orden de Compra");
    $("#datospedidosinpedido tbody").empty();
    $("#articulostable tbody").empty();
    $(".nav-tabs a").first().click();
    $("#proveedorsinpedido").val('');
    $('#editasinpedido').modal('show');
    _objArticulos = new Array();
}

function SinPedido_EliminaArticulo() {
    ActualizaLista();
}

function SinPedido_EditaArticulo(id, indice) {

    var elrel = id;
    $("#articulo").modal("show");
    var objArticulo = _objArticulos[_indiceArticulo];
    $("#ed_clasificacionmaterial").val(objArticulo.Clasificacion);
    $("#ed_tipomaterial").val(objArticulo.TipoMaterial);

    tipo = $("#ed_tipomaterial").val();
    SeleccionaTipoArticulo(tipo);

    //$("#ed_articulo").val('');
    $("#ed_cantidad").val(objArticulo.Cantidad);
    $("#ed_precio").val(objArticulo.PrecioUnitario);
    $("#ed_moneda").val(objArticulo.catTipoMonedaClave);

    $('#ed_clasificacionmaterial').attr('disabled', true);
    $('#ed_tipomaterial').attr('disabled', true);
    $('#ed_articulosinpedido').attr('disabled', true);
    
    
}

function SinPedido_AgregaArticulo() {

    _indiceArticulo = -1;
    $('#articulo').modal('show');
    $("#ed_clasificacionmaterial").val('');
    $("#ed_articulosinpedido").val('');
    $("#ed_tipomaterial").val('');
    $("#ed_articulo").val('');
    $("#ed_cantidad").val('');
    $("#ed_precio").val('');
    $("#ed_moneda").val('');
    $('#ed_clasificacionmaterial').attr('disabled', false);
    $('#ed_tipomaterial').attr('disabled', false);
    $('#ed_articulosinpedido').attr('disabled', false);

    $("#lasseriessp").hide();

    _seriales = new Array();
}

function SinPedido_AgregaArticulo_carga_masiva() {

    _indiceArticulo = -1;
    
    $('#cm').appendTo("body").modal('show');
    $("#inputFile").val('');
    $("#series > tbody").html("");
    $("#mas_articulo").val('');
    $("#mas_cantidad").val('');
    $("#mas_precio").val('');
    $("#mas_moneda").val('');
    $("#existen").val('');
    $("#mas_tipo_articulo").val('');
    
    document.getElementById("agregararticulo_carga_masiva").disabled = true;
   
   
    _seriales = new Array();
    series_repetidas = new Array();
    series_no_repetidas = new Array();
    uniqueNames = new Array();
}

function SeleccionaTipoArticulo(tipo) {
    var objRoles = {};
    objRoles.tipo = tipo;
    $("#ed_articulosinpedido").empty();
    $("#ed_articulosinpedido").append("<option value=''>Seleccione</option>");

    var myJSONText = JSON.stringify(objRoles);
    var url = '/Articulo/ObtenPorTipo2/?cs=1';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            var arreglo = eval(data);
            for (var i = 0; i < arreglo.length; i++) {

                var yaesta = false;
                var selected = '';
                for (var j = 0; j < _objArticulos.length; j++) {
                    if (_objArticulos[j].ArticuloClave == arreglo[i].Clave) {

                        if (_indiceArticulo == -1) {
                            yaesta = true;
                        }
                        else {
                            selected = 'selected';
                        }
                    }
                }

                if (!yaesta) {
                    if (arreglo[i].Activo == "1") {
                        $("#ed_articulosinpedido").append("<option value='" + arreglo[i].Clave + "' data-catseriesclaves='" + JSON.stringify(arreglo[i].catSeriesClaves) + "' "+ selected +">" + arreglo[i].Descripcion + "</option>");
                    }
                }
            }

            if (_indiceArticulo != -1)
            {
                if (_objArticulos[_indiceArticulo].TieneSeries > 0) {
                    _seriales = _objArticulos[_indiceArticulo].Series;
                    IniciaDetalle(_conpedido);
                }
                else {
                    _seriales = new Array();
                    $("#lasseriessp").hide();
                    $("#lasseries").hide();
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
          
        }
    });
}

function SeleccionaMaterial() {
    if ($("#ed_articulosinpedido option:selected").attr("data-catseriesclaves") == "[]") {
        $("#lasseriessp").hide();
        return
    }
    else {
        $("#lasseriessp").show();
    }
    IniciaDetalle(_conpedido);
    $("#ed_cantidad").focus();
}

function GetSerie(data, conorden) {
    console.log(data);
    if (conorden == true) {
        if ($('#ed_pendientecm').val() < data.series_nuevas.length) {
            toastr.error("El documento sobrepasa las series requeridas, necesita menos de " + $('#ed_pendientecm').val());
            return;
        }
    }

    _seriales = [];
    
   
   
    var nrep = [];
    var serierep = [];
    var arr = [];

    serierep=data.series_repetidas;
    nrep=data.series_nuevas;
    arr=data.series_nuevas;

    console.log(serierep);
    if (serierep.length > 0) {
        mostrarmodalseries(data.series_repetidas);
    }
    $.each(nrep, function (i, el) {
        if ($.inArray(el, arr) === -1) arr.push(el);
    });

    $('#mas_cantidad').val(data.series_nuevas.length);
    $('#ed_asurtircm').val(data.series_nuevas.length);

    if (conorden == true) {
        $('#tabla_carga_masiva_con_orden >tbody').empty();
        for (var f1 = 0; f1 < data.series_nuevas.length; f1++) {
            var serie = data.series_nuevas[f1].datos[0].Valor;
            $('#tabla_carga_masiva_con_orden >tbody').append('<tr><td>' + serie + '</td></tr>');
        }         
        
        ConPedido_AgregaSeriales_carga_masiva(data.series_nuevas);
    } else {
        $('#series >tbody').empty();
        for (var f12 = 0; f12 < data.series_nuevas.length; f12++) {
            var serie = data.series_nuevas[f12].datos[0].Valor;
            $('#series >tbody').append('<tr><td>' + serie + '</td></tr>');
            
        }


        AgregaSerialescargamasiva(data.series_nuevas);
        
              
    }

}

function mostrarmodalseries(serierep) {

    $('#eleccion_series').appendTo("body").modal('show');
    for (var i = 0; i < serierep.length; i++) {
        $('#series-repetidas >tbody').append('<tr><td>' + serierep[i].datos[0].Valor.toUpperCase() + '</td></tr>')

    }


}

function AgregaSerialescargamasiva(arr) {
   

    var indice = _indiceArticulo;
    if (indice == -1) {
        indice = _objArticulos.length;
    }

     _seriales=arr;
   
    document.getElementById("agregararticulo_carga_masiva").disabled = false;   
    var pendiente = 0;         
}

//cuando se agrega un nuevo serial manualmente
function AgregaSeriales() {
    var indice = _indiceArticulo;
    if (indice == -1) {
        indice = _objArticulos.length;
    }


    var valid = jQuery('#formaarticulo').validationEngine('validate');
    if (valid) {

        vbExiste = false;

        var serial = {};
        serial.ArticuloClave = $("#ed_articulosinpedido").val();
        serial.EstatusTexto = $("#estatusserial option:selected").text();
        serial.EstatusClave = $("#estatusserial").val();
        serial.Activo = 1;
        serial.datos = new Array();

        
        $("#lasseriessp .seriales").each(function () {
            var clave = $(this).attr("id");
            var valor = $(this).val();
            console.log(clave);
            console.log(valor);
            
            var serie = {};
            serie.Clave = clave;
            serie.Valor = valor.toUpperCase();

            serial.datos.push(serie);
        });
        console.log(serial);

        ValidaSerie(indice, serial.datos, serial);


        if (vbExiste) {
            $("#s6").select();
            return;
        }
        _seriales.push(serial);


        var pendiente = 0;

        IniciaDetalle(_conpedido);

    }
}

function SinPedido_AgregaArticulo_Guarda_carga_masiva() {
    
    var objArticulo = {};
    objArticulo.Clasificacion = $("#mas_clasificacion").val();
    objArticulo.TipoMaterial = $("#mas_tipo_articulo").val();
    objArticulo.ArticuloClave = $("#mas_articulo").val();
    objArticulo.ArticuloTexto = $("#mas_articulo option:selected").text();
    objArticulo.catSeriesClaves = eval($("#mas_articulo option:selected").attr('data-catseriesclaves'));
    objArticulo.Cantidad = $("#mas_cantidad").val();  
    objArticulo.Surtiendo  = $("#mas_cantidad").val();
    objArticulo.PrecioUnitario=$("#mas_precio").val();
    objArticulo.catTipoMonedaClave = $("#mas_moneda").val();    
    objArticulo.MonedaTexto = $("#mas_moneda option:selected").text();
    objArticulo.TieneSeries = objArticulo.catSeriesClaves.length;
    objArticulo.Series = _seriales;    
   
    if (_indiceArticulo == -1) {
        _objArticulos.push(objArticulo);
    }
    else {
        objArticulo.Eliminados = _objArticulos[_indiceArticulo].Eliminados;
        _objArticulos[_indiceArticulo] = objArticulo;
    }
    ActualizaLista();
    $('#cm').modal('hide');
}


function SinPedido_AgregaArticulo_Guarda() {
    $("#ed_precio").attr('class', 'form-control validate[required,custom[number]]');
    $("#ed_moneda").attr('class', 'form-control validate[required]');
    $(".seriales").attr('class', 'form-control');

    if ($("#ed_cantidad").val() == null || $("#ed_cantidad").val() == "" || parseInt($("#ed_cantidad").val()) <= 0 || $("#ed_cantidad") == "0") {
        toastr.warning("Necesita colocar la cantidad de artículos, además la cantidad debe ser mayor a 0");
        return
    }

    if ($("#ed_precio").val() == null || $("#ed_precio").val() == "") {
        toastr.warning("Necesita colocar el precio del artículo");
        return
    }

    if ($("#ed_moneda").val() == null || $("#ed_moneda").val() == "") {
        toastr.warning("Necesita colocar un tipo de moneda");
        return
    }


    var valid = jQuery('#formaarticulo').validationEngine('validate');
    if (valid) {


        var objArticulo = {};
        objArticulo.Clasificacion = $("#ed_clasificacionmaterial").val();
        objArticulo.TipoMaterial = $("#ed_tipomaterial").val();
        objArticulo.ArticuloClave = $("#ed_articulosinpedido").val();
        objArticulo.ArticuloTexto = $("#ed_articulosinpedido option:selected").text();
        objArticulo.catSeriesClaves = eval($("#ed_articulosinpedido option:selected").attr('data-catseriesclaves'));
        objArticulo.Cantidad = $("#ed_cantidad").val();
        objArticulo.Surtiendo = $("#ed_cantidad").val();
        objArticulo.PrecioUnitario = $("#ed_precio").val();
        objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
        objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();
        objArticulo.TieneSeries = objArticulo.catSeriesClaves.length;
        objArticulo.Series = _seriales;

        if (_indiceArticulo == -1) {
            _objArticulos.push(objArticulo);
        }
        else {
            objArticulo.Eliminados = _objArticulos[_indiceArticulo].Eliminados;
            _objArticulos[_indiceArticulo] = objArticulo;
        }
        ActualizaLista();

        $('#articulo').modal('hide');

    }


    
}

function SinPedido_Guarda() {


    var valid = jQuery('#formaeditasinpedido').validationEngine('validate');
    if (valid) {


        if (_objArticulos.length == 0) {
            toastr.error("La recepción debe de contener al menos un Artículo");
           
            return;
        }

        if ($('#proveedorsinpedido').val() == null || $('#proveedorsinpedido').val() == "") {
            toastr.warning("Necesita colocar un proveedor para ejecutar la recepción");

            return;

        }



        //_objArticulos.Eliminados = _eliminados;

        var objGuardaSinPedido = {};

        if ($("#ed_id").val() != null && $("#ed_id").val() != '') {
            objGuardaSinPedido.ed_id = $("#ed_id").val();
        }

        objGuardaSinPedido.Almacen = $("#almacensinpedido").val();
        objGuardaSinPedido.Proveedor = $("#proveedorsinpedido").val();
        objGuardaSinPedido.TipoCambio = $("#tipocambiosinpedido").val();
        objGuardaSinPedido.Iva = $("#ivasinpedido").val();
        objGuardaSinPedido.Descuento = $("#descuentosinpedido").val();
        objGuardaSinPedido.DiasCredito = $("#diascreditosinpedido").val();
        objGuardaSinPedido.Articulos = _objArticulos;
        objGuardaSinPedido.ArticulosEliminados = _eliminaLineas;
        objGuardaSinPedido.Observaciones = $("#Observaciones").val();

        var myJSONText = JSON.stringify(objGuardaSinPedido);
         

        var url = '/RecepcionMaterial/Guarda/';
        $.ajax({
            url: url,
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                if (data == "0") {

                    toastr.error("Un proceso importante está siendo procesado, por favor espere un momento");                    
                    return;
                }
                else if (data == "300") {

                    toastr.error("Se ha detectado que alguna de las series ya se encuentra en el inventario, por lo cual no se puede procesar esta recepción");
                                       
                }
                else if (data == "1000") {

                    toastr.error("No puede realizar una recepcion sin articulos");

                }
                else {

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle de Recepcion sin pedido");
                    $("#framecanvas").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + data);

                   // $("#eliframe").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + data);
                    //$('#ver').modal('show');
                    $("#editasinpedido").modal('hide');
                    toastr.success("La recepción se ha guardado correctamente");
                    LlenaGrid(_pag);

                }


                
            },
            error: function (request, error) {
                toastr.error("Surgió un error, intente nuevamente más tarde");
               
            }
        });
    }
    else {
        toastr.error("Debe llenar los campos obligatorios");
       
    }
}


function BuscaPedidos() {
    var objBusquedaPedidos = {};
    objBusquedaPedidos.pedido = $("#pedidoseleccionar").val();
    objBusquedaPedidos.proveedor = $("#buscarproveedor").val();
    objBusquedaPedidos.pag = -1;
    objBusquedaPedidos.act = "1";

    var myJSONText = JSON.stringify(objBusquedaPedidos);

    $("#datospedido").show();
    $("#datospedido tbody").empty();
    $("#datos2pedido tbody").empty();
    $("#selecciona").hide();

    var url = '/RecepcionMaterial/Pedidos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {


            var info = "";
            var info2 = "";

            var arreglo = eval(data);
            _articulospedidos = arreglo;


            _pedido = objBusquedaPedidos.pedido;

            for (var i = 0; i < arreglo.length; i++) {

                arreglo[i].Estatus = arreglo[i].Estatus.replace('Autorizado', 'Pendiente por surtir');

                var renglon = "<tr><td>" + arreglo[i].NumeroPedido + "</td><td>" + arreglo[i].Fecha + "</td><td>" + arreglo[i].Estatus + "</td>";

                if (arreglo[i].Estatus == 'Por autorizar' || arreglo[i].Estatus == "Completo" || arreglo[i].Estatus == "Rechazado") {
                    renglon += "<td></td>";
                }
                else {
                    renglon += "<td>" + DetalleSelecciona(arreglo[i].Clave, i, arreglo[i].NumeroPedido) + "</td>";
                }

                renglon += "</tr>"

                $("#datospedido tbody").append(renglon);
            }

            //AfterAjax();

        },
        error: function (request, error) {
            toastr.error("Surgió un error, intente nuevamente más tarde");
          
        }
    });
}


function ObtenRecepcion(idrecepcion) {

    
    $("#ed_id").val(idrecepcion);
    var objRecepcion = {};
    objRecepcion.recepcion = idrecepcion;
    var myJSONText = JSON.stringify(objRecepcion);

    var url = '/RecepcionMaterial/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {





            _objArticulos = new Array();

            $("#datos2pedido tbody").html('');
            $("#datos2pedido tbody").empty();
            $("#loaderseleccionarpedido").hide();

            /*
            if (data.indexOf('-2:') > -1) {
                Mensaje('Error', 'El pedido no ha sido autorizado');
                return;
            }

            if (data.indexOf('-3:') > -1) {
                Mensaje('Error', 'Error desconocido');
                return;
            }
            */


            var _recepcion = eval(data);

            $("#ed_id").val(_recepcion.Clave);

            $("#almacensinpedido").val(_recepcion.AlmacenClave);
            $("#proveedorsinpedido").val(_recepcion.ProveedorClave);
            $("#tipocambiosinpedido").val(_recepcion.TipoCambio);
            $("#ivasinpedido").val(_recepcion.Iva);
            $("#descuentosinpedido").val(_recepcion.Descuento);
            $("#diascreditosinpedido").val(_recepcion.DiasCredito);

            var arreglo = _recepcion.Articulos;

            for (var i = 0; i < arreglo.length; i++) {
                var objArticulo = {};
                var objArticulo = {};
                objArticulo.Clasificacion = arreglo[i].ClasificacionClave;
                objArticulo.TipoMaterial = arreglo[i].TipoArticuloClave;
                objArticulo.ArticuloClave = arreglo[i].ArticuloClave;
                objArticulo.ArticuloTexto = arreglo[i].Nombre;
                objArticulo.catSeriesClaves = arreglo[i].catSeriesClaves;
                objArticulo.Cantidad = arreglo[i].Surtiendo;
                objArticulo.Surtiendo = arreglo[i].Surtiendo;
                objArticulo.Surtida = arreglo[i].Surtida;
                objArticulo.PrecioUnitario = arreglo[i].PrecioUnitario;
                objArticulo.catTipoMonedaClave = arreglo[i].MonedaClave;
                objArticulo.MonedaTexto = arreglo[i].MonedaTexto;
                objArticulo.TieneSeries = arreglo[i].TieneSeries;
                objArticulo.Seriales = arreglo[i].Seriales;

                objArticulo.Series = new Array();

                objArticulo.Eliminados = new Array();

                for (var ii = 0; ii < objArticulo.Seriales.length; ii++) {
                    if (idrecepcion == objArticulo.Seriales[ii].RecepcionClave) {
                        objArticulo.Series.push(objArticulo.Seriales[ii]);
                    }
                }


                _objArticulos.push(objArticulo);
            }

            //_articulospedidos = arreglo;
            //_pedido = objObtenPedido.pedido;

            _conpedido = false;

            ActualizaLista();
            //IniciaDetalle(_conpedido);

        },
        error: function (request, error) {
            toastr.error("Surgió un error, intente nuevamente más tarde");
           
        }
    });


}


function ObtenPedido(idpedido, idrecepcion, pedidonumero) {

    $("#titulowin").html("Recibir Material de la Orden de Compra #" + pedidonumero);
    //$("#recibir h4").html(pedidonumero);
    var objObtenPedido = {};
    objObtenPedido.pedido = idpedido;
    objObtenPedido.recepcion = idrecepcion;

    var myJSONText = JSON.stringify(objObtenPedido);

    $("#datos2pedido tbody").empty();
    $("#selecciona").hide();
    $("#at1").parent().hide();
    $("#at2").parent().show();
    $("#at0").parent().show();
    $("#guarda").removeClass('hide');
    $("#guarda").show();

    $("#loaderseleccionarpedido").show();
    $(".seleccionarpedido").hide();

    var url = '/Pedido/Articulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            _objArticulos = new Array();

            $("#datos2pedido tbody").html('');
            $("#datos2pedido tbody").empty();

            $("#loaderseleccionarpedido").hide();

            if (data.indexOf('-2:') > -1) {
                toastr.error("El pedido no ha sido autorizado");
                
                return;
            }

            if (data.indexOf('-3:') > -1) {
                toastr.error("Error desconocido");
              
                return;
            }

           
            var arreglo = eval(data);

            for (var i = 0; i < arreglo.length; i++)
            {
                var objArticulo = {};
                var objArticulo = {};
                objArticulo.Clasificacion = '';
                objArticulo.TipoMaterial = arreglo[i].TipoArticuloClave;
                objArticulo.ArticuloClave = arreglo[i].ArticuloClave;
                objArticulo.ArticuloTexto = arreglo[i].Nombre;
                objArticulo.catSeriesClaves = arreglo[i].catSeriesClaves;
                objArticulo.Cantidad = arreglo[i].Cantidad;
                objArticulo.Surtiendo = arreglo[i].Surtiendo;
                objArticulo.Surtida = arreglo[i].Surtida;
                objArticulo.PrecioUnitario = arreglo[i].PrecioUnitario;
                objArticulo.catTipoMonedaClave = '';
                objArticulo.MonedaTexto = '';
                objArticulo.TieneSeries = arreglo[i].TieneSeries;
                objArticulo.Seriales = arreglo[i].Seriales;

                objArticulo.Series = new Array();

                objArticulo.Eliminados = new Array();

                for (var ii = 0; ii < objArticulo.Seriales.length; ii++) {
                    if (idrecepcion == objArticulo.Seriales[ii].RecepcionClave) {
                        objArticulo.Series.push(objArticulo.Seriales[ii]);
                    }
                }


                _objArticulos.push(objArticulo);
            }

            //_articulospedidos = arreglo;
            _pedido = objObtenPedido.pedido;

            ConPedido_ArticulosPedidos();

            $(".nav li").removeClass("active");
            $(".nav li a").removeClass("disabled");
            $("#at2").click();

        },
        error: function (request, error) {
            toastr.error("Surgió un error, intente nuevamente más tarde");
           
        }
    });
}

function ConPedido_ArticulosPedidos() {

    $("#datos2pedido tbody").html('');

    var info = "";
    var info2 = "";


    for (var i = 0; i < _objArticulos.length; i++) {

        var pendiente = _objArticulos[i].Cantidad - _objArticulos[i].Surtida - _objArticulos[i].Surtiendo;

        if (pendiente != 0 || (pendiente == 0 && _objArticulos[i].Surtiendo != 0)) {
            if (pendiente != 0 || _recepcionClave != '') {
                info2 += "<tr>" + "<td>" + _objArticulos[i].ArticuloTexto + "</td>" + "<td>" + _objArticulos[i].Cantidad + "</td>"
                    + "<td id='surtida_" + i + "'>" + _objArticulos[i].Surtida + "</td>"
                    + "<td id='surtiendo_" + i + "'>" + _objArticulos[i].Surtiendo + "</td>"
                    + "<td id='pendiente_" + i + "'>" + pendiente + "</td>"
                    + "<td id='preciounitario_" + i + "' class='precio' data-indice='" + i + "'><button type='button' class='btn btn-default btn-xs'>&nbsp;" + ToMoneda(_objArticulos[i].PrecioUnitario) + "</button></td>";


                var mo = _objArticulos[i].Surtiendo * _objArticulos[i].PrecioUnitario;
                info2 += "<td id='importe_" + i + "'>" + ToMoneda(mo) + "</td>";

                info2 += "" + DetalleRecepcion(_objArticulos[i].TieneSeries, _objArticulos[i].ArticuloClave, i, pendiente + _objArticulos[i].Surtiendo, _objArticulos[i].ArticuloTexto) + "";
               info2 += "<td>" + LimpiaRecepcion(_objArticulos[i].TieneSeries, _objArticulos[i].ArticuloClave, i) + "</td>";

                info2 += "</tr>";
            }
        }
    }

    $("#datos2pedido tbody").append(info2);



    for (var i = 0; i < _objArticulos.length; i++) {
        $("#qty_" + i).val(_objArticulos[i].Surtiendo);
    }

    //AfterPedido();
    //AfterAjax();

}

function ConPedido_RecibeGenerico(indice) {

    if ($("#qty_" + indice).val() == '')
    {
        $("#qty_" + indice).val('0');
        $("#qty_" + indice)[0].select();
    } else if (parseFloat($("#qty_" + indice).val()) > parseFloat(_objArticulos[_indiceArticulo].Cantidad) - parseInt(_objArticulos[_indiceArticulo].Surtida)) {
        $("#qty_" + indice).val(parseInt(_objArticulos[_indiceArticulo].Cantidad)- parseInt(_objArticulos[_indiceArticulo].Surtida));
        $("#qty_" + indice)[0].select();
    }

    _indiceArticulo = indice;

    var cantidad = _objArticulos[_indiceArticulo].Cantidad;
    var surtida = _objArticulos[_indiceArticulo].Surtida;
    var pendiente = cantidad - surtida;
    var clave = _objArticulos[_indiceArticulo].Clave;

    var nuevos = parseInt($("#qty_" + indice).val());
    _objArticulos[_indiceArticulo].Surtiendo = nuevos;


    pendiente = parseInt(_objArticulos[_indiceArticulo].Cantidad) - parseInt(_objArticulos[_indiceArticulo].Surtida) - parseInt(nuevos);
    $("#surtiendo_" + _indiceArticulo).html(nuevos);
    $("#pendiente_" + _indiceArticulo).html(pendiente);
    var monto = nuevos * _objArticulos[_indiceArticulo].PrecioUnitario;
    $("#importe_" + _indiceArticulo).html(ToMoneda(monto));

}


function ConPedido_Recibecm(indice) {
    $("#camposagrega").show();

    $("#recibircm .modal-title").html("Recibe Material por carga masiva: " + _objArticulos[indice].ArticuloTexto);

    _indiceArticulo = indice;
    _seriales = new Array();   
    var cantidad = _objArticulos[_indiceArticulo].Cantidad;
    var surtida = _objArticulos[_indiceArticulo].Surtida;
    var pendiente = cantidad - surtida;
    var clave = _objArticulos[_indiceArticulo].ArticuloClave;

    $("#ed_solicitadacm").val(cantidad);
    $("#ed_pendientecm").val(pendiente);
    $("#ed_articulo").val(clave);

    $('#recibircm').modal('show');
}


function ConPedido_Recibe(indice) {
    $("#camposagrega").show();

    $("#recibir .modal-title").html("Recibe Material: " + _objArticulos[indice].ArticuloTexto);

    _indiceArticulo = indice;

    if (_objArticulos[_indiceArticulo].Series != undefined && _objArticulos[_indiceArticulo].Series != null) {
        _seriales = _objArticulos[_indiceArticulo].Series;
    }
    else {
        _seriales = new Array();
    }


    if (_seriales.length == 0) {
        $("#ed_asurtir").val('');
    }
    else {
        $("#ed_asurtir").val(_seriales.length);
    }
    var cantidad = _objArticulos[_indiceArticulo].Cantidad;
    var surtida = _objArticulos[_indiceArticulo].Surtida;
    var pendiente = cantidad - surtida;
    var clave = _objArticulos[_indiceArticulo].Clave;

    $("#guardarsurtir").hide();
    $("#detalle").hide();
    $("#lasseries").hide();

    if (_objArticulos[_indiceArticulo].catSeriesClaves == '') {
        $("#guardarsurtir").removeClass("hide");
        $("#guardarsurtir").show();
    }
    else {
        $("#detalle").removeClass("hide");
        $("#detalle").show();
        $("#lasseries").show();
    }

    $("#resumen").html("Artículos agregados <b>(" + _seriales.length + ")</b>.");


    $("#lasseries tbody").empty();
    $("#lasseries").hide();
    //alert(articuloclave);

    $("#ed_solicitada").val(cantidad);
    $("#ed_pendiente").val(pendiente);
    $("#ed_articulo").val(clave);

    IniciaDetalle(_conpedido);
    /*
    if (_objSurtidas[_indiceArticulo] != null) {
        $("#ed_surtida").val(_articulospedidos[_indiceArticulo].Surtiendo);
        //$("#lasseries").show();
        IniciaDetalle(_conpedido);
    }
    else {
        $("#ed_surtida").val(_articulospedidos[_indiceArticulo].Surtiendo);
    }
    */


    if (_objArticulos[_indiceArticulo].catSeriesClaves == '') {
    }
    else {
        $("#lasseries").show();
    }

    $('#recibir').modal('show');
}

function ConPedido_DetalleArticulos()
{
    if ($("#ed_asurtir").val() == '') {
        toastr.error("Debe escribir la cantidad a surtir");
        
        return;
    }

    var surtiendo = parseInt($("#ed_asurtir").val());
    var pendiente = parseInt($("#ed_pendiente").val());

    if (surtiendo > pendiente) {
        toastr.error("La cantidad surtida no puede ser mayor a la pendiente");        
        return;
    }

   // if (_articulospedidos != null && _articulospedidos != undefined) {
        _objArticulos[_indiceArticulo].Surtiendo += parseInt(surtiendo);
    //}
    //else {
        var cadena = JSON.stringify(_objArticulos);
    //}

    $("#lasseries").show();
    IniciaDetalle(_conpedido);

    var serialeslongitud = 0;

    try {

        for (var i = 0; i < _objArticulos[_indiceArticulo].Series.length; i++) {
            if (_objArticulos[_indiceArticulo].Series[i].Activo == 1) {
                serialeslongitud++;
            }
        }

        //

    }
    catch (e) {
    }

    var quedan = -1;
    if (_conpedido) {
        quedan = parseInt(surtiendo) - parseInt(serialeslongitud);
    }
    else {
        quedan = parseInt(surtiendo) - parseInt(serialeslongitud);
    }

    $("#resumen").html("Artículos agregados <b>(" + serialeslongitud + ")</b>. Faltan <b>(" + quedan + ")</b>");

    $("#addart").val("Agregar");
    //$("#addart").prop('disabled', 'disabled');
}

function ConPedido_Limpia(indice) {
    //alert("a");
    var serialeslongitud = 0;

    try {
        //serialeslongitud = _objRecepcion[_indiceArticulo].length;
    }
    catch (e) {
    }


    var elrel = $(this).attr('rel');
    _indiceArticulo = $(this).attr('data-indice');

    _objArticulos[_indiceArticulo].Surtiendo = 0;
    //_objRecepcion[_indiceArticulo] = _objRecepcionInicial[_indiceArticulo].slice(0);

    $("#ed_surtida").val(_objArticulos[_indiceArticulo].Surtiendo);
    var pendiente = parseInt(_objArticulos[_indiceArticulo].Cantidad) - parseInt(_objArticulos[_indiceArticulo].Surtida);
    $("#pendiente_" + _indiceArticulo).html(pendiente);

    $("#surtiendo_" + _indiceArticulo).html(_objArticulos[_indiceArticulo].Surtiendo);

    var monto = _objArticulos[_indiceArticulo].Surtiendo * _objArticulos[_indiceArticulo].PrecioUnitario;
    $("#importe_" + _indiceArticulo).html(ToMoneda(monto));

}

function ConPedido_AgregaSeriales_carga_masiva(arr) {
    console.log(arr);
    var indice = _indiceArticulo;

        if (indice == -1) {
            indice = _objArticulos.length;
        }
        var cuantosRecibir = parseInt($("#ed_asurtir").val());
        _seriales=arr;
        console.log(_seriales);

        document.getElementById("agregararticulo_carga_masiva").disabled = false;       
        var pendiente = 0;
        var nuevos = _seriales.length;
        if (_objArticulos != null && _objArticulos != undefined) {
            _objArticulos[_indiceArticulo].Series = _seriales.slice(0);
            _objArticulos[_indiceArticulo].Surtiendo = parseInt(nuevos);

            pendiente = parseInt(_objArticulos[_indiceArticulo].Cantidad) - parseInt(_objArticulos[_indiceArticulo].Surtida) - parseInt(nuevos);
            $("#surtiendo_" + _indiceArticulo).html(nuevos);
            $("#pendiente_" + _indiceArticulo).html(pendiente);
            var monto = nuevos * _objArticulos[_indiceArticulo].PrecioUnitario;
            $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
        }     
    
}

function ConPedido_AgregaSeriales()
{

    var valid = jQuery('#formarecepcion').validationEngine('validate');
    if (valid) {


        var cuantosRecibir = parseInt($("#ed_asurtir").val());

        if (_seriales.length >= cuantosRecibir)
        {
            toastr.error("La cantidad de números de serie ha superado la cantidad esperada");
            
            return;
        }

        vbExiste = false;

        var indice = _indiceArticulo;

        if (indice == -1) {
            indice = _objArticulos.length;
        }


        if (_conpedido) {
            //$("#addart").prop('disabled', 'disabled');
        }

        var serial = {};
        serial.ArticuloClave = _objArticulos[_indiceArticulo].ArticuloClave;
        serial.EstatusTexto = $("#estatusserial option:selected").text();
        serial.EstatusClave = $("#estatusserial").val();
        serial.Activo = 1;
        serial.datos = new Array();

        $("#lasseries .seriales").each(function () {
            var clave = $(this).attr("id");
            var valor = $(this).val();

            var serie = {};
            serie.Clave = clave;
            serie.Valor = valor.toUpperCase();

            serial.datos.push(serie);
        });

        ValidaSerie(indice, serial.datos, serial);

        if (vbExiste) {
            $("#s6").select();
            return;
        }
        _seriales.push(serial);


        var nuevos = _seriales.length;

        var pendiente = 0;

        if (_objArticulos != null && _objArticulos != undefined) {


            _objArticulos[_indiceArticulo].Series = _seriales.slice(0);
            _objArticulos[_indiceArticulo].Surtiendo = parseInt(nuevos);

            pendiente = parseInt(_objArticulos[_indiceArticulo].Cantidad) - parseInt(_objArticulos[_indiceArticulo].Surtida) - parseInt(nuevos);
            $("#surtiendo_" + _indiceArticulo).html(nuevos);
            $("#pendiente_" + _indiceArticulo).html(pendiente);
            var monto = nuevos * _objArticulos[_indiceArticulo].PrecioUnitario;
            $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
        }

        $("#addart").val("Agregar");

        $("#resumen").html("Articulos agregados <b>(" + nuevos + ")</b>. Faltan <b>(" + (cuantosRecibir - nuevos) + ")</b>.");

        /*
        $("#addart").removeAttr('disabled');

        $('input[type="button"]').each(function () {
            this.disabled = false;
        });
        */

        IniciaDetalle(_conpedido);



    }
}

function ConPedido_Guarda() {
    var valid = jQuery('#formaedita').validationEngine('validate');
   
    if (valid) {

        var objRecepcionMaterial = {};

        var totalsurtido = 0;
        for (var i = 0; i < _objArticulos.length; i++) {
            try {
                totalsurtido += parseInt(_objArticulos[_indiceArticulo].Cantidad);
            }
            catch (e) {

            }
        }

        $(".recibegenerico").each(function () {
            try {
                totalsurtido += parseInt($(this).val());
            }
            catch (e) {

            }
        });


        if (totalsurtido == 0) {
            toastr.error("La recepción debe tener al menos un artículo");
            
            return;
        }


        if ($("#ed_idsinpedido").val() != null && $("#ed_idsinpedido").val() != '') {
            objRecepcionMaterial.ed_id = $("#ed_idsinpedido").val();
        }

        if ($("#ed_id").val() != null && $("#ed_id").val() != '') {
            objRecepcionMaterial.ed_id = $("#ed_id").val();
        }



        //alert(_pedido);

        //_objArticulos.Eliminados = _eliminados;

        if (_recepcionClave != '') {
            objRecepcionMaterial.ed_id = _recepcionClave;
        }
        else {
            objRecepcionMaterial.ed_id = null;
        }

        objRecepcionMaterial.Almacen = $("#almacen").val();
        objRecepcionMaterial.ProcesoClaveRespuesta = _pedidoClave;


        for (var i = 0; i < _objArticulos.length; i++)
        {
            if (_objArticulos[i].TieneSeries == 0)
            {
                //_objArticulos[i].Surtiendo = _objArticulos[i].Cantidad;
            }
            else
            {
                _objArticulos[i].Surtiendo = _objArticulos[i].Series.length;
            }
        }

        objRecepcionMaterial.Articulos = _objArticulos;


        var myJSONText = JSON.stringify(objRecepcionMaterial);

        

        var url = '/RecepcionMaterial/Guarda/';
        $.ajax({
            url: url,
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                if (data == "0") {
                    toastr.error("Una transacción está siendo procesada, intente más tarde");                    
                    return;
                }
                else if (data == "200") {
                    toastr.error("El sistema evitó esta recepción, la cantidad que quiere ingresar en alguno de los artículos excede la cantidad del pedido. \n Tip no. 1: Actualice esta página quizá algún otro usuario ya completó el pedido \nTip no.2: Revise las cantidades de los artículos");
                    
                }
                else if (data == "300") {

                    toastr.error("El sistema evitó esta recepción, alguna de las series que intenta guardar ya se encuentra en el inventario");
                                       
                }
                else if (data == "1000") {

                    toastr.error("No puede realizar una recepcion sin artículos");

                }
                else if (data == "-1")
                {
                    toastr.error("Alguno de los artículos de la orden no se encuentran disponibles en el almacén");

                   
                    return;
                }
                else {

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle de Recepcion");
                    $("#framecanvas").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + data);               
               
                $("#edita").modal('hide');
                toastr.success("La recepción de material se guardó correctamente");
                LlenaGrid(_pag);
                }

            },
            error: function (request, error) {

                toastr.error("Surgió un error, intente nuevamente más tarde");
                
            }
        });
    }
    else {
        toastr.error("Debe llenar los campos obligatorios");       
    }
}

function IniciaDetalle(conpedido) {


    var indice = _indiceArticulo;

    if (indice == -1) {
        indice = _objArticulos.length;
    }


    _conpedido = conpedido;

    var latabla = "lasseries";
    if (conpedido == 1) {
        latabla = "lasseries";
    }
    else {
        latabla = "lasseriessp";
    }

    $("#" + latabla + " thead").empty();
    $("#" + latabla + " tbody").empty();

    var tiposeries = null;

    if (conpedido) {
        tiposeries = _objArticulos[_indiceArticulo].catSeriesClaves;
    }
    else {
        
        tiposeries = eval($("#ed_articulosinpedido option:selected").attr("data-catseriesclaves"));
    }

    var tr = "";
    tr += "<tr>";
    tr += "<th>Estatus</th>";

        //if(tiposeries==null )
    for (var ii = 0; ii < tiposeries.length; ii++) {
        tr += "<th>" + tiposeries[ii].Nombre + "</th>";
    }

    tr += "</tr>";

    $("#" + latabla + " thead").append(tr);

    var tbody = "";

    var asurtir = 0;

    try {
        if ($("#ed_asurtir").val() != '') {
            asurtir = parseInt($("#ed_asurtir").val());
        }
    }
    catch (e) {
    }


    var surtiendo = asurtir;
    var serialeslongitud = 0;

    try {


        for (var i = 0; i < _seriales.length; i++) {
            if (_seriales[i].Activo == 1) {
                serialeslongitud++;
            }
        }

    }
    catch (e) {
    }

    var quedan = -1;
    if (conpedido) {

        quedan = parseInt(surtiendo) + parseInt(_seriales.length) - parseInt(serialeslongitud);
    }
    else {

        try {
            $("#ed_cantidad").val(_seriales.length);
        }
        catch (e) {
            $("#ed_cantidad").val(0);

        }
    }


    tbody += "<tr>";

    tbody += "<td><select class='form-control validate[required]' id='estatusserial' ";


    if (conpedido == 1) {
        if (quedan == 0) {
            tbody += " disabled";
        }
    }

    tbody += "><option value=1>Buen estado</option><option value=2>Dañado</option><option value=3>CRC</option><option value=4>Reparación</option></select></td>";
    for (var ii = 0; ii < tiposeries.length; ii++) {

        var ultimo = "";

        if (ii == tiposeries.length - 1)
        {
            ultimo = "ultimo";
        }

        tbody += "<td><input type='text' style='text-transform:uppercase;' tabindex='" + ii + "' class='" + ultimo + " form-control validate[required";

        if (tiposeries[ii].Hexadecimal == 'True') {
            tbody += "";
        }
        tbody += ",minSize[" + tiposeries[ii].Min + "]";
        tbody += ",maxSize[" + tiposeries[ii].Max + "]";

        tbody += "] seriales' id='s" + tiposeries[ii].Clave + "'";

        if (conpedido == 1) {
            if (quedan == 0) {
                tbody += " disabled";
            }
        }

        tbody += "></td>";
    }


    if (quedan < 0) {
        quedan = 0;
    }

    if (conpedido) {
        tbody += "<td><input type='button' class='form-control' id='addart'  value='Agregar' ";
        if (quedan == 0) {

            tbody += " disabled";
        }
    }
    else {
        tbody += "<td><input type='button' class='form-control' id='addartsp' value='Agregar' ";
    }

    tbody += "/></td>";
    tbody += "</tr>";

    var serialeslongitud = 0;
    var _serialesactual = 0;

    try {
        serialeslongitud = _seriales.length;
    }
    catch (e) {
    }

    for (var ji = 0; ji < serialeslongitud; ji++) {
        //if (_seriales[ji].Activo != 0) {
        if ((_seriales[ji].InventarioClave == '' || _seriales[ji].InventarioClave == null || _seriales[ji].RecepcionClave == _recepcionClave)) {


                tbody += "<tr><td>" + _seriales[ji].EstatusTexto + "</td>";
                for (var ii = 0; ii < tiposeries.length; ii++) {
                    try {
                        tbody += "<td>" + _seriales[ji].datos[ii].Valor + "</td>";
                    }
                    catch (e)
                    { }
                }

                if ((_seriales[ji].InventarioClave != '' && _seriales[ji].InventarioClave != null && _seriales[ji].RecepcionClave != _recepcionClave)) {
                    tbody += "<td></td>";
                }
                else {
                    _serialesactual++;

                    if (_seriales[ji].Activo == 1) {
                        tbody += "<td><button type='button' class='btn btn-danger btn-xs delserial' rel='" + ji + "'>&nbsp;Elimina</button></td>";
                    }
                    else {
                        tbody += "<td><button type='button' class='btn btn-default btn-xs' >&nbsp;No disponible</button></td>";
                    }
                }
                tbody += "</tr>";

            }
       // }
    }

    if (conpedido == 1) {
        $("#lasseries tbody").append(tbody);
    }
    else {
        $("#lasseriessp tbody").append(tbody);
    }
    if (conpedido == 0) {
        $("#lasseriessp").show();
       
    }
    $("#addart").click(function () {        
    });

    $("#addartsp").on("click", function () {
        AgregaSeriales();
    });

    Lectora();

    if (conpedido == 1) {
        $("#lasseries .seriales:first").focus();
        setTimeout(function () { $("#lasseries .seriales:first").focus(); }, 50);
    }
    else {
        $("#lasseriessp .seriales:first").focus();
        setTimeout(function () { $("#lasseriessp .seriales:first").focus(); }, 50);
    }
}

function ActualizaLista() {
    $("#articulostable tbody").empty();
    for (var i = 0; i < _objArticulos.length; i++) {
        var total = 0;
        var total_usd = 0;

        if (_objArticulos[i].MonedaTexto.toLowerCase() == "pesos") {
            total = parseFloat(_objArticulos[i].PrecioUnitario) * parseInt(_objArticulos[i].Cantidad);
        }
        else if (_objArticulos[i].MonedaTexto.toLowerCase().indexOf("dólar") > -1 || _objArticulos[i].MonedaTexto.toLowerCase().indexOf("dolar") > -1) {
            total_usd = parseFloat(_objArticulos[i].PrecioUnitario) * parseInt(_objArticulos[i].Cantidad);
        }
        var renglon = "<tr><td>" + _objArticulos[i].ArticuloTexto + "</td><td>" + _objArticulos[i].Cantidad + "</td><td>" + ToMoneda(_objArticulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td><td>" + ToMoneda(total_usd) + "</td><td>" + DameAccionesArticuloSinPedido(_objArticulos[i].ArticuloClave, i) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }
}

function ValidaSerie(indice, serie, series) {
    ValidaSerieInmediata(series);
    ValidaSerieLocal(indice, serie, series);


    //Revisamos que no esté eliminado para no validarlo
    //Primero en los articulos eliminados
    var existeeliminado = false;
    for (var h = 0; h < _objArticulos.length; h++) {

        if (_objArticulos[h].Eliminados != undefined && _objArticulos[h].Eliminados != null) {
            for (var i = 0; i < _objArticulos[h].Eliminados.length; i++) {
                for (var j = 0; j < _objArticulos[h].Eliminados[i].datos.length; j++) {
                    if (_objArticulos[h].Eliminados[i].datos[j].Valor.toUpperCase() == serie[0].Valor.toUpperCase()) {
                        existeeliminado = true;
                    }
                }
            }
        }
    }


    for (var h = 0; h < _eliminaLineas.length; h++)
    {
        for (var i = 0; i < _eliminaLineas[h].Series.length; i++) {
            for (var j = 0; j < _eliminaLineas[h].Series[i].datos.length; j++) {
                if (_eliminaLineas[h].Series[i].datos[j].Valor.toUpperCase() == serie) {
                    existeeliminado = true;
                }
            }
        }
    }


    if (!existeeliminado) {
        ValidaSerieRemota(indice, serie);
    }
}

function ValidaSerieRemota(indice, serie) {

    if (!vbExiste) {
        var objSerieVal = {};
        objSerieVal.serie = serie;
        var myJSONText = JSON.stringify(objSerieVal);

        var url = '/Inventario/Serie/';
        $.ajax({
            async: false,
            cache: false,
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                vbExiste = eval(data);

                if (vbExiste)
                {
                    $('#existen').val("existe");
                    toastr.error("La serie ya existe en el inventario");
                   
                }
            },
            error: function (request, error) {
                toastr.error("Surgió un error, intente nuevamente más tarde");
               
            }
        });
    }


}

function ValidaSerieLocal(indice, serie, series) {

    if (!vbExiste) {
        if (_seriales != null) {
            for (var foo = 0; foo < _seriales.length; foo++) {
                for (var j = 0; j < series.datos.length; j++) {
                    if (_seriales[foo].datos[0].Valor.toUpperCase() == series.datos[j].Valor.toUpperCase()) {
                        vbExiste = true;
                        if (vbExiste) {
                            toastr.error("Los seriales fueron capturados previamente");

                            
                        }
                        break;
                    }
                }
            }
        }
    }
}

function CalculaAbajo()
{
    if ($("#ed_asurtir").val() == '') {
        $("#resumen").html("");
        return;
    }

    var surtiendo = parseInt($("#ed_asurtir").val());

    var serialeslongitud = 0;
    try {

        for (var i = 0; i < _objArticulos[_indiceArticulo].Series.length; i++) {
            if (_objArticulos[_indiceArticulo].Series[i].Activo == 1) {
                serialeslongitud++;
            }
        }

        //

    }
    catch (e) {
    }
    quedan = parseInt(surtiendo) - parseInt(serialeslongitud);

    $("#resumen").html("Artículos agregados <b>(" + serialeslongitud + ")</b>. Faltan <b>(" + quedan + ")</b>");
}

function ValidaSerieInmediata(seriales) {

    if (!vbExiste) {

        for (var i = 0; i < seriales.datos.length; i++) {
            for (var j = 0; j < seriales.datos.length; j++) {
                if (i != j)
                {
                    if (seriales.datos[i].Valor.toUpperCase() == seriales.datos[j].Valor.toUpperCase())
                    {
                        vbExiste = true;
                        if (vbExiste) {
                            toastr.error("Los seriales no se pueden repetir");
                            
                        }
                    }
                }
            }
        }
    }

}

function LlenaGrid(pag) {

    var objRoles = {};
    objRoles.pag = pag;
    objRoles.tiporecepcion = $("#tiporecepcion").val();
    objRoles.buscaproveedor = $("#buscaproveedor").val();
    objRoles.abuscar = $("#abuscar").val();


    var myJSONText = JSON.stringify(objRoles);
    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
       
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "error":"Su sesión ha expirado, por favor vuelva a auticarse",
        "stateSave": true,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/RecepcionMaterial/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText },
        },
        "fnInitComplete": function (oSettings, json) {
           
        },
        "fnDrawCallback": function (oSettings) {

           
        },
        "columns": [
            { "data": "NumeroPedido", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Destino", "orderable": false },
            {
              sortable: false,
              "render": function (data, type, full, meta) {                 
                  return DamePedido(full.ClavePedido, full.Pedido);
              }
            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                   

                    var renglon="";
                    renglon +="<button type='button'  class='btn ink-reaction btn-floating-action btn-xs btn-primary pdfrecepcionmaterial' "
                        + "data-toggle='tooltip' data-placement='right' title='' data-original-title='Descarga PDF' rel='" + full.Clave + "'><i class='md md-content-paste'></i></button>"
                        + "<button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-success recepcionmaterial' "
                        + "data-toggle='tooltip' data-placement='right' title='' data-original-title='Ver detalle de recepción' rel='" + full.Clave + "'><i class='fa fa-search'></i></button>";

                    if (full.Estatus != "Cancelado") {

                        if (full.ClavePedido == '' || full.ClavePedido == null) {
                                
                            }
                            else {
                                
                            }

                            renglon += "<button type='button' class='btn btn-danger btn-xs cancelarecepcion' rel='" + full.Clave + "' data-pedido='" + full.ClavePedido + "' data-numeropedido='" + full.ClavePedido + "'><i class='fa fa-trash-o' aria-hidden='true'></i>&nbsp;Cancelar</button>";
                        }
                    return renglon;
                    
                }

            }
        ],
        
        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })
}