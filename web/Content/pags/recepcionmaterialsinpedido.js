﻿
$(document).ready(function () {


    jQuery('#formaeditasinpedido').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false
    });



    $("#guardasinpedido").click(function (e) {
        e.preventDefault();



        var valid = jQuery('#formaeditasinpedido').validationEngine('validate');
        if (valid) {



            var objRoles = {};

            objRoles.Almacen = $("#almacensinpedido").val();
            //objRoles.ProcesoClaveRespuesta = _pedido;
            objRoles.Recepcion = _objRecepcion;
            objRoles.Articulos = _objArticulos;

            var myJSONText = JSON.stringify(objRoles);

            //alert(myJSONText);
            //return;

            $("#loader").show();

            var url = '/RecepcionMaterial/Guarda/';
            $.ajax({
                url: url,
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#eliframe").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + data);
                    $('#ver').modal('show');
                    $("#editasinpedido").modal('hide');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
                }
            });
        }
        else {
            Mensaje('Error', 'Debe llenar los campos obligatorios');
        }
    });


    $("#addsin").click(function () {

        $("#at0sinpedido").click();
        $("#titulowinsinpedido").html("Recibir material sin pedido");

        $("#datospedidosinpedido tbody").empty();

        $(".nav-tabs a").first().click();
        _objArticulos = new Array();
        $('#editasinpedido').modal('show');
    });


    $("#agregar").click(function () {
        $('#articulo').modal('show');

        $("#ed_clasificacionmaterial").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_articulo").val('');
        $("#ed_cantidad").val('');
        $("#ed_precio").val('');
        $("#ed_moneda").val('');

        $("#ed_articulosinpedido").val('');
        $("#lasseriessp tbody").empty();

    });



    $("#at2sinpedido").click(function () {
        $("#establecelugarsinpedido").hide();
        $("#guardasinpedido").removeClass('hide');
        $("#guardasinpedido").show();
    })

    $("#establecelugarsinpedido").click(function () {
        $("#at2sinpedido").click();
    });


    $("#ed_articulosinpedido").change(function () {

        if ($("#ed_articulosinpedido option:selected").attr("data-catseriesclaves") == "[]") {
            $("#lasseriessp").hide();
            //$("#ed_cantidad").removeAttr('readonly')
            return
        }
        else {
            $("#lasseriessp").show();
            //$("#ed_cantidad").prop('readonly', true)
        }

        IniciaDetalle(0);

        $("#ed_cantidad").focus();

    });






    $("#ed_tipomaterial").change(function () {
        var tipo = $(this).val();
        var objRoles = {};
        objRoles.tipo = tipo;
        $("#ed_articulosinpedido").empty();
        $("#ed_articulosinpedido").append("<option value=''>Seleccione</option>");

        var myJSONText = JSON.stringify(objRoles);
        var url = '/Articulo/ObtenPorTipo/?cs=1';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                var arreglo = eval(data);
                for (var i = 0; i < arreglo.length; i++) {

                    var yaesta = false;
                    var selected = '';
                    for (var j = 0; j < _objArticulos.length; j++) {
                        if (_objArticulos[j].ArticuloClave == arreglo[i].Clave) {

                            if (_indiceArticulo == -1) {
                                yaesta = true;
                            }
                            else {
                                selected = 'selected';
                            }
                        }
                    }

                    if (!yaesta) {
                        if (arreglo[i].Activo == "1") {
                            $("#ed_articulosinpedido").append("<option value='" + arreglo[i].Clave + "' data-catseriesclaves='" + JSON.stringify(arreglo[i].catSeriesClaves) + "'>" + arreglo[i].Descripcion + "</option>");
                        }
                    }
                }
            },
            error: function (request, error) {
                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    });



    $("#agregararticulo").click(function () {

        $("#ed_precio").attr('class', 'form-control validate[required,custom[number]]');
        $("#ed_moneda").attr('class', 'form-control validate[required]');
        $(".seriales").attr('class', 'form-control');

        var valid = jQuery('#formaarticulo').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var objArticulo = {};
            objArticulo.Clasificacion = $("#ed_clasificacionmaterial").val();
            objArticulo.TipoMaterial = $("#ed_tipomaterial").val();
            objArticulo.ArticuloClave = $("#ed_articulosinpedido").val();
            objArticulo.ArticuloTexto = $("#ed_articulosinpedido option:selected").text();
            objArticulo.catSeriesClaves = eval($("#ed_articulosinpedido option:selected").attr('data-catseriesclaves'));
            objArticulo.Cantidad = $("#ed_cantidad").val();
            objArticulo.Surtiendo = $("#ed_cantidad").val();
            objArticulo.PrecioUnitario = $("#ed_precio").val();
            objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
            objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();
            objArticulo.TieneSeries = objArticulo.catSeriesClaves.length;

            _objArticulos.push(objArticulo);
            ActualizaLista();

            $('#articulo').modal('hide');

        }
    });



});



function AfterAjaxsinpedido() {
    $(".editasinpedido").click(function () {
        alert('implementando');
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');

        $("#articulo").modal("show");
        
        var objArticulo = _objArticulos[_indiceArticulo];

        $("#ed_clasificacionmaterial").val(objArticulo.Clasificacion);
        $("#ed_tipomaterial").val(objArticulo.TipoMaterial);
        //$("#ed_articulo").val('');
        $("#ed_cantidad").val(objArticulo.Cantidad);
        $("#ed_precio").val(objArticulo.PrecioUnitario);
        $("#ed_moneda").val(objArticulo.catTipoMonedaClave);

       

    });


    $(".eliminasinpedido").click(function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');
        _objArticulos.splice(_indiceArticulo, 1);
        ActualizaLista();
    });


    $(".recibirsinpedido").click(function () {

        $("#camposagrega").hide();

        var elrel = $(this).attr('rel');

        _indiceArticulo = $(this).attr('data-indice');

        var clave = _objArticulos[_indiceArticulo].ArticuloClave;



        $("#guardarsurtir").hide();
        $("#detalle").hide();
        $("#lasseries").hide();

        if (_objArticulos[_indiceArticulo].catSeriesClaves == '') {
            $("#guardarsurtir").removeClass("hide");
            $("#guardarsurtir").show();
        }
        else {
            $("#detalle").removeClass("hide");
            $("#detalle").show();
            $("#lasseries").show();
        }

        $("#lasseries tbody").empty();
        $("#lasseries").hide();
        //alert(articuloclave);

        $("#ed_articulo").val(clave);

        $("#ed_asurtir").val('');


        IniciaDetalle(false);

        //alert(_objArticulos[_indiceArticulo].Surtiendo);


        var asurtir = 0;

        try {
            asurtir = parseInt(_objArticulos[_indiceArticulo].Surtiendo) - parseInt(_objRecepcion[_indiceArticulo].length);
        }
        catch (e) {
            asurtir = parseInt(_objArticulos[_indiceArticulo].Surtiendo);
        }

        $("#ed_asurtir").val(asurtir);
        $("#detalle").click();;

        if (_objArticulos[_indiceArticulo].catSeriesClaves == '') {
        }
        else {
            $("#lasseries").show();
        }


        //$("#rechaza_id").val(elrel);
        $('#recibir').modal('show');




    });

}

function ActualizaLista() {
    $("#articulostable tbody").empty();

    for (var i = 0; i < _objArticulos.length; i++) {

        var total = 0;
        var total_usd = 0;

        if (_objArticulos[i].MonedaTexto.toLowerCase() == "pesos") {
            total = parseFloat(_objArticulos[i].PrecioUnitario) * parseInt(_objArticulos[i].Cantidad);
        }
        else if (_objArticulos[i].MonedaTexto.toLowerCase().indexOf("dólar") > -1 || _objArticulos[i].MonedaTexto.toLowerCase().indexOf("dolar") > -1) {
            total_usd = parseFloat(_objArticulos[i].PrecioUnitario) * parseInt(_objArticulos[i].Cantidad);
        }

        var renglon = "<tr><td>" + _objArticulos[i].ArticuloTexto + "</td><td>" + _objArticulos[i].Cantidad + "</td><td>" + ToMoneda(_objArticulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td><td>" + ToMoneda(total_usd) + "</td><td>" + DameAccionesArticuloSinPedido(_objArticulos[i].ArticuloClave, i) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }

    //Resumen();
    AfterAjaxsinpedido();
}


function DameAccionesArticuloSinPedido(elid, i) {

    var regresa = "";
    regresa += "<button type='button' data-indice='" + i + "' class='btn btn-warning btn-xs editasinpedido' rel='" + elid + "'>&nbsp;Editar</button>"
    regresa += "<button type='button' data-indice='" + i + "' class='btn btn-danger btn-xs eliminasinpedido' rel='" + elid + "'>&nbsp;Elimina</button>";
    return regresa;
}
