﻿
$(document).ready(function () {

    LlenaGrid(1)
    $("#formaedita").submit(function (e) {
        e.preventDefault();
        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
           
            

            var distribuidor = document.getElementById("dis").value;
            var almacen = document.getElementById("almacen").value;
            var tecnico = document.getElementById("tecnico").value;
            var clasificacion = document.getElementById("cla").value;
            var tipo = document.getElementById("tip").value;
            var articulo = document.getElementById("articulos").value;
            var fechainicio = document.getElementById("fechainicio").value;
            var fechafin = document.getElementById("fechafin").value;
            window.location.href = '/MovimientoTecnicos/Detalle?pdf=2&distribuidor=' + distribuidor + '&almacen=' + almacen + '&tecnico=' + tecnico + '&clasificacion=' + clasificacion
                                + '&tipo=' + tipo + '&articulo=' + articulo + '&fechainicio=' + fechainicio + '&fechafin=' + fechafin;

        }
        else {
            toastr.error('Error', 'Debes de llenar los campos obligatorios');
        }
    });    


    $("#dis").change(function () {
       LlenaGrid(1)
    });

    $("#almacen").change(function () {

        $("#tecnico").val('');

        if ($("#temporal1").length == 0) {
            var select = '';
            select += "<select id='temporal1' style='display:none'>";

            $(".optiontecnico").each(function () {
                select += "<option value='" + $(this).val() + "' data-tecnico='" + $(this).attr('data-tecnico') + "'>" + $(this).text() + "</option>";

            });
            select += "</select>";
            $("#tecnico").after(select);
        }
        $("#tecnico").empty();
        $("#tecnico").append("<option value='' selected>Técnicos</option>");


        $("#temporal1 option").each(function () {
            
            if ($(this).attr("data-tecnico").indexOf($("#almacen").val()) > -1) {
                $("#tecnico").append("<option value='" + $(this).val() + "' data-tecnico='" + $(this).attr('data-tecnico') + "'>" + $(this).text() + "</option>");
            }
        });

        $('#tecnico').val('');
        $('#cla').val('');
        $('#tip').val('');
        $('#articulos').val('');

    });


   




    $("#cla").change(function () {

        $("#tip").val('');

        if ($("#temporal2").length == 0) {
            var select = '';
            select += "<select id='temporal2' style='display:none'>";

            $(".optiontipo").each(function () {
                select += "<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "'>" + $(this).text() + "</option>";
            });
            select += "</select>";
            $("#tip").after(select);
        }
        $("#tip").empty();
        $("#tip").append("<option value='' selected>Tipos de Artículo</option>");


        $("#temporal2 option").each(function () {
            if ($(this).attr("data-cla") == $("#cla").val()) {
                $("#tip").append("<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "'>" + $(this).text() + "</option>");
            }
        });

        $('#tip').val('');
        $('#articulos').val('');
    });

    $("#tip").change(function () {

        $("#articulos").val('');

        if ($("#temporal3").length == 0) {
            var select = '';
            select += "<select id='temporal3' style='display:none'>";

            $(".optionarticulos").each(function () {
                select += "<option value='" + $(this).val() + "' data-tipo='" + $(this).attr('data-tipo') + "'>" + $(this).text() + "</option>";
            });
            select += "</select>";
            $("#articulos").after(select);
        }
        $("#articulos").empty();
        $("#articulos").append("<option value='' selected>Artículos</option>");


        $("#temporal3 option").each(function () {
            if ($(this).attr("data-tipo") == $("#tip").val()) {
                $("#articulos").append("<option value='" + $(this).val() + "' data-tipo='" + $(this).attr('data-tipo') + "'>" + $(this).text() + "</option>");
            }
        });

        $('#articulos').val('');
    });
});

var _pag = 1;
function LlenaGrid(pag) {
    _pag = pag;

    $("#almacen").val('');

    if ($("#temporal").length == 0) {
        var select = '';
        select += "<select id='temporal' style='display:none'>";

        $(".optionalmacen").each(function () {
            select += "<option value='" + $(this).val() + "' data-dis='" + $(this).attr('data-dis') + "'>" + $(this).text() + "</option>";
        });
        select += "</select>";
        $("#almacen").after(select);
    }
    $("#almacen").empty();
    $("#almacen").append("<option value='' selected>Almacenes</option>");


    $("#temporal option").each(function () {
        if ($(this).attr("data-dis") == $("#dis").val()) {
            $("#almacen").append("<option value='" + $(this).val() + "' data-dis='" + $(this).attr('data-dis') + "'>" + $(this).text() + "</option>");
        }
    });
    $('#almacen').val('');
    $('#tecnico').val('');
    $('#cla').val('');
    $('#tip').val('');
    $('#articulos').val('');

}
