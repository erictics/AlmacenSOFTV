﻿//lista de recepciones

var Aparatos = new Array();
var Accesorios = new Array();
var soyconpedido = false;
var _pedidoClave = 0;
var _objArticulos = new Array();



$(document).ready(function () {
    LlenaGrid(1);
});

function LlenaGrid(pag) {
   
    var objRoles = {};
    objRoles.pag = 1;
    objRoles.tiporecepcion = $("#tiporecepcion").val();
    objRoles.buscaproveedor = $("#buscaproveedor").val();
    objRoles.abuscar = $("#abuscar").val();
    var myJSONText = JSON.stringify(objRoles);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "error": "Su sesión ha expirado,por favor vuelva a auticarse",
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/RecepcionMaterial/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText },
        },
        "fnInitComplete": function (oSettings, json) {


        },
        "fnDrawCallback": function (oSettings) {


        },
        "columns": [
            { "data": "NumeroPedido", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Destino", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                   // return DamePedido(full.ClavePedido, full.Pedido);//elid,numero


                    if (full.Pedido == null) {
                        return "<button type='button' class='btn btn-default btn-xs'>Sin orden de compra</button>"
                    }
                    else {


                        return "<button type='button' class='btn btn-default btn-xs ver' rel='" + full.ClavePedido + "'>&nbsp; Orden (" + full.Pedido + ")</button><button type='button' class='btn btn-info btn-xs pdf' rel='" + full.ClavePedido + "'><i class='fa fa-file-text' aria-hidden='true'></i>&nbsp;PDF</button>";
                    }

                }
            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                   // return DameDetalle(full.Clave, full.ClavePedido, full.NumeroPedido, full.Estatus);//elid, pedido, numeropedido, status

                    

                    var regresa = "<button type='button' class='btn btn-default btn-xs recepcionmaterial' rel='" + full.Clave + "'><i class='fa fa-info-circle' style='color:#54c6e4' aria-hidden='true'></i>&nbsp;Ver Detalle</button><button type='button' class='btn btn-info btn-xs pdfrecepcionmaterial' rel='" + full.Clave + "'><i class='fa fa-download' aria-hidden='true'></i>&nbsp;PDF</button>";

                    if (full.Estatus != "Cancelado") {

                        if (full.ClavePedido == '' || full.ClavePedido == null) {
                            //regresa += "<button type='button' class='btn btn-warning btn-xs editarecepcionsp' rel='" + elid + "' data-pedido='" + pedido + "'>&nbsp;Editar</button>";
                        }
                        else {
                            // regresa += "<button type='button' class='btn btn-warning btn-xs editarecepcion' rel='" + elid + "' data-pedido='" + pedido + "' data-numeropedido='" + numeropedido + "'><i class='fa fa-pencil' aria-hidden='true'></i>&nbsp;Editar</button>";
                        }

                        regresa += "<button type='button' class='btn btn-danger btn-xs cancelarecepcion' rel='" + full.Clave + "' data-pedido='" + full.ClavePedido + "' data-numeropedido='" + full.NumeroPedido + "'><i class='fa fa-trash-o' aria-hidden='true'></i>&nbsp;Cancelar</button>";
                    }
                    return regresa;


                }

            }
        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })
}




