﻿$("#add").on("click", function () {   
   
    AgregaConPedido();
    soyconpedido = true;
});

function AgregaConPedido() {
    
    $("#at0").click();
    $("#at2").addClass('disabled');
    $("#titulowin").html("Recibir Material");
    $("#buscarproveedor").val('-1');
    $("#datospedido tbody").empty();
    $(".nav-tabs a").first().click();
    $('#edita').modal('show');
    $("#t0").removeClass('active');

    $("#datos2pedido tbody").empty();

    $("#at1").parent().addClass('active');
    $("#at2").parent().removeClass('active');
    $("#at0").parent().removeClass('active');

    $("#at1").parent().show();
    $("#at2").parent().hide();
    $("#at0").parent().hide();

    Aparatos = new Array();
    Accesorios = new Array();
}

function BuscaPedidos() {
    var objBusquedaPedidos = {};
    objBusquedaPedidos.pedido = $("#pedidoseleccionar").val();
    objBusquedaPedidos.proveedor = $("#buscarproveedor").val();
    objBusquedaPedidos.pag = -1;
    objBusquedaPedidos.act = "1";

    var myJSONText = JSON.stringify(objBusquedaPedidos);

    $("#datospedido").show();
    $("#datospedido tbody").empty();
    $("#datos2pedido tbody").empty();
    $("#selecciona").hide();

    var url = '/RecepcionMaterial/Pedidos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            var arreglo = eval(data);
            for (var i = 0; i < arreglo.length; i++) {

                arreglo[i].Estatus = arreglo[i].Estatus.replace('Autorizado', 'Pendiente por surtir');
                var renglon = "<tr><td>" + arreglo[i].NumeroPedido + "</td><td>" + arreglo[i].Fecha + "</td><td>" + arreglo[i].Estatus + "</td>";
                if (arreglo[i].Estatus == 'Por autorizar' || arreglo[i].Estatus == "Completo" || arreglo[i].Estatus == "Rechazado") {
                    renglon += "<td></td>";
                }
                else {
                    renglon += "<td><button type='button' data-indice='" + i + "' class='btn ink-reaction btn-floating-action btn-xs btn-success seleccionarpedido' data-numeropedido='" + arreglo[i].NumeroPedido + "' rel='" + arreglo[i].Clave + "'><i class='fa fa-check'></i></button></td>"
                    }
                renglon += "</tr>"
                $("#datospedido tbody").append(renglon);
            }
        },
        error: function (request, error) {
            toastr.error("Surgió un error, intente nuevamente más tarde");

        }
    });
}



$("#datospedido").on("click", ".seleccionarpedido", function () {
    _pedidoClave = $(this).attr('rel');
    var numeropedido = $(this).attr('data-numeropedido');
    ObtenPedido($(this).attr('rel'), -1, numeropedido);
});



function ObtenPedido(idpedido, idrecepcion, pedidonumero) {

    $("#titulowin").html("Recibir Material de la Orden de Compra #" + pedidonumero);    
    var objObtenPedido = {};
    objObtenPedido.pedido = idpedido;
    objObtenPedido.recepcion = idrecepcion;
    var myJSONText = JSON.stringify(objObtenPedido);

    _objArticulos = new Array();

    $("#datos2pedido tbody").empty();
    $("#selecciona").hide();
    $("#at1").parent().hide();
    $("#at2").parent().show();
    $("#at0").parent().show();
    $("#guarda").removeClass('hide');
    $("#guarda").show();
    $("#loaderseleccionarpedido").show();
    $(".seleccionarpedido").hide();

    var url = '/Pedido/Articulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {


            var arreglo = eval(data);

            for (var i = 0; i < arreglo.length; i++) {
                var objArticulo = {};
                var objArticulo = {};
                objArticulo.Clasificacion = '';
                objArticulo.TipoMaterial = arreglo[i].TipoArticuloClave;
                objArticulo.ArticuloClave = arreglo[i].ArticuloClave;
                objArticulo.ArticuloTexto = arreglo[i].Nombre;
                objArticulo.catSeriesClaves = arreglo[i].catSeriesClaves;
                objArticulo.Cantidad = arreglo[i].Cantidad;
                objArticulo.Surtiendo = arreglo[i].Surtiendo;
                objArticulo.Surtida = arreglo[i].Surtida;
                objArticulo.PrecioUnitario = arreglo[i].PrecioUnitario;
                objArticulo.catTipoMonedaClave = '';
                objArticulo.MonedaTexto = '';
                objArticulo.TieneSeries = arreglo[i].TieneSeries;
                objArticulo.Seriales = arreglo[i].Seriales;

                objArticulo.Series = new Array();

                objArticulo.Eliminados = new Array();

                for (var ii = 0; ii < objArticulo.Seriales.length; ii++) {
                    if (idrecepcion == objArticulo.Seriales[ii].RecepcionClave) {
                        objArticulo.Series.push(objArticulo.Seriales[ii]);
                    }
                }


                _objArticulos.push(objArticulo);
            }




            
            var info2 = "";
            for (var i = 0; i < data.length; i++) {
                var pendiente = data[i].Cantidad - data[i].Surtida - data[i].Surtiendo;
                if (pendiente != 0 || (pendiente == 0 && data[i].Surtiendo != 0)) {
                    if (pendiente != 0) {
                        info2 += "<tr>" + "<td>" + data[i].Nombre + "</td>" + "<td>" + data[i].Cantidad + "</td>"
                            + "<td id='surtida_" + i + "'>" + data[i].Surtida + "</td>"
                            + "<td id='surtiendo_" + i + "'>" + data[i].Surtiendo + "</td>"
                            + "<td id='pendiente_" + i + "'>" + pendiente + "</td>"
                            + "<td id='preciounitario_" + i + "' class='precio' data-indice='" + i + "'><button type='button' class='btn btn-default btn-xs'>&nbsp;" + ToMoneda(data[i].PrecioUnitario) + "</button></td>";
                        
                        var mo = data[i].Surtiendo * data[i].PrecioUnitario;
                        info2 += "<td id='importe_" + i + "'>" + ToMoneda(mo) + "</td>";
                        info2 += "" + DetalleRecepcion(data[i].TieneSeries, data[i].ArticuloClave, i, pendiente + data[i].Surtiendo, data[i].Nombre) + "";                        
                        info2 += "</tr>";
                    }
                }
            }

            $("#datos2pedido tbody").append(info2);



            //for (var i = 0; i < _objArticulos.length; i++) {
            //    $("#qty_" + i).val(_objArticulos[i].Surtiendo);
            //}

            $(".nav li").removeClass("active");
            $(".nav li a").removeClass("disabled");
            $("#at2").click();

        },
        error: function (request, error) {
            toastr.error("Surgió un error, intente nuevamente más tarde");

        }
    });
}



function DetalleRecepcion(tieneseries, elid, i, max, nombreproducto) {
    var regresa = "";
    if (tieneseries > 0) {
        regresa += "<td><button type='button' data-indice='" + i + "' class='btn btn-info btn-xs recibir' data-nombreproducto='" + nombreproducto + "' rel='" + elid + "'><i class='fa fa-check-square' aria-hidden='true'></i>&nbsp;Recibir Material</button></td><td><button type='button' data-indice='" + i + "' class='btn btn-warning btn-xs recibircm' style='background-color: #0d4984; border-color: #0736a9;' data-nombreproducto='" + nombreproducto + "' rel='" + elid + "'>&nbsp;<i class='fa fa-check-square' aria-hidden='true'></i>carga masiva</button></td>";
    }
    else {
        regresa += "<td><input type='' size=3 id='qty_" + i + "' data-indice='" + i + "' rel='" + elid + "' class='recibegenerico validate[custom[integer],min[0],max[" + max + "]]'></td>";
    }

    return regresa;
}


$("#datos2pedido").on("click", ".recibir", function () {
    var elrel = $(this).attr('rel');
    _indiceArticulo = $(this).attr('data-indice');
    var nombreproducto = $(this).attr('data-indice');
    ConPedido_Recibe(_indiceArticulo);
});


function ConPedido_Recibe(indice) {
    $("#camposagrega").show();
    $("#recibir .modal-title").html("Recibe Material: " + _objArticulos[indice].ArticuloTexto);
    _indiceArticulo = indice;  
    $("#guardarsurtir").hide();
    $("#detalle").hide();


    var surtida = _objArticulos[_indiceArticulo].Surtida;
   
    $("#ed_solicitada").val(_objArticulos[_indiceArticulo].Cantidad);
    $("#ed_pendiente").val(_objArticulos[_indiceArticulo].Cantidad - surtida);
    $("#ed_articulo").val(_objArticulos[_indiceArticulo].Clave);

    if (_objArticulos[_indiceArticulo].catSeriesClaves == '') {
        $("#guardarsurtir").removeClass("hide");
        $("#guardarsurtir").show();
        $("#lasseries").hide();
    }
    else {
        $("#detalle").removeClass("hide");
        $("#detalle").show();
        $("#lasseries").show();
    }   
    $("#lasseries tbody").empty(); 
   
    $('#recibir').modal('show');
    IniciaDetalle(soyconpedido, indice);
}


$("#datos2pedido").on("click", ".recibircm", function () {
    var elrel = $(this).attr('rel');
    _indiceArticulo = $(this).attr('data-indice');
    $('#ed_asurtircm').val('');
    $('#inputFile').val('');
    $('#id_tbody_tabla_carga_masiva_con_orden').empty();
    var nombreproducto = $(this).attr('data-indice');
    ConPedido_Recibecm(_indiceArticulo);
});


$('#detalle').click(function () {




})






function ConPedido_Recibecm(indice) {
    $("#camposagrega").show();

    $("#recibircm .modal-title").html("Recibe Material por carga masiva: " + _objArticulos[indice].ArticuloTexto);

    _indiceArticulo = indice;   
    var cantidad = _objArticulos[_indiceArticulo].Cantidad;
    var surtida = _objArticulos[_indiceArticulo].Surtida;
    var pendiente = cantidad - surtida;
    var clave = _objArticulos[_indiceArticulo].ArticuloClave;

    $("#ed_solicitadacm").val(cantidad);
    $("#ed_pendientecm").val(pendiente);
    $("#ed_articulocm").val(clave);

    $('#recibircm').modal('show');
    
}








function IniciaDetalle(conpedido, indice) {

    var indice = _indiceArticulo;
    if (indice == -1) {
        indice = _objArticulos.length;
    }
    _conpedido = conpedido;
    var latabla = "lasseries";
    if (conpedido == true) {
        latabla = "lasseries";
    }
    else {
        latabla = "lasseriessp";
    }
    $("#" + latabla + " thead").empty();
    $("#" + latabla + " tbody").empty();

    var tiposeries = null;
     if (conpedido) {
        tiposeries = _objArticulos[_indiceArticulo].catSeriesClaves;
    }
    else {
        tiposeries = eval($("#ed_articulosinpedido option:selected").attr("data-catseriesclaves"));
     }
    var tr = "";
    tr += "<tr>";
    tr += "<th>Estatus</th>";

    //if(tiposeries==null )
    for (var ii = 0; ii < tiposeries.length; ii++) {
        tr += "<th>" + tiposeries[ii].Nombre + "</th>";
    }
    tr += "</tr>";
    $("#" + latabla + " thead").append(tr);


    var tbody = "";
    tbody += "<tr>";

    tbody += "<td><select class='form-control validate[required]' id='estatusserial' ";
    tbody += "><option value=1>Buen Estado</option><option value=2>Dañado</option><option value=3>CRC</option><option value=4>Reparación</option></select></td>";
    for (var ii = 0; ii < tiposeries.length; ii++) {
         var ultimo = "";
         if (ii == tiposeries.length - 1) {
             ultimo = "ultimo";
         }
         tbody += "<td><input type='text' style='text-transform:uppercase;' tabindex='" + ii + "' class='" + ultimo + " form-control validate[required";
         if (tiposeries[ii].Hexadecimal == 'True') {
             tbody += ",custom[hexa]";
         }
         tbody += ",minSize[" + tiposeries[ii].Min + "]";
         tbody += ",maxSize[" + tiposeries[ii].Max + "]";

         tbody += "] seriales' id='s" + tiposeries[ii].Clave + "'";

        

         tbody += "></td>";
     }

    if (conpedido) {
            tbody += "<td><input type='button' class='form-control' id='addart'  value='Agregar'></td> ";
            
        }
        else {
            tbody += "<td><input type='button' class='form-control' id='addartsp' value='Agregar'></td> ";
        }


    tbody += "</tr>";
     console.log(_objArticulos[_indiceArticulo]);
     console.log(tiposeries);
     $("#" + latabla + " thead").append(tbody);

     

}

$('#addart').click(function(){

    var valid = jQuery('#formarecepcion').validationEngine('validate');
    if (valid) {    


    }
})


