﻿
$(document).ready(function () {
    toastr.info('¡Atención! El sistema de almacén se ha actualizado y tiene nuevas caracteristicas, le recomendamos borrar su historial de navegación.');

    $("#forma").submit(function (e) {
        e.preventDefault();
        
            $("#loader").show();
            var url = '/Acceso/Autenticacion/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $("#forma").serialize(),
                type: "POST",
                success: function (data) {
                    if (data == 0) {

                        $("#usuario").val('');
                        $("#password").val('');

                        errorLogin();
                    } else if (data == 1) {
                      
                        window.location.replace("/Acceso/Dashboard/");
                    } else if (data == -1) {
                        $('#inactivo').modal('show');
                       
                    } else if (data == -2) {
                        //$(".passwordcambiar").click();
                    }
                    $("#loader").hide();
                },
                error: function (request, error) {
                   

                }
            });
        
    });
});



function errorLogin() {

    //alert("no hay usuario o contraseña");    
    //Toast.error(message [,title [, options]])    
    toastr.error('Usuario y/o contraseña incorrectos', '', { showMethod: 'fadeIn' });
    // toastr.error('Usuario y/o contraseña incorrectos', '', {positionClass: 'toast-bottom-center',  closeButton: true });

}