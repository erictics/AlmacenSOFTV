﻿
llenaLista(0,0,0);
$('#add').click(function () {

    $('#addVehiculo').modal('show');
    $('#Guardar').show();
    $('#edita').hide();
    $('#titulowinactiva').empty();
    $('#titulowinactiva').append("Guardar vehículo");
    reset();
});


$('#Guardar').click(function () {  

        var activo = $(".activo").parent('[class*="icheckbox"]').hasClass("checked");
        var asegurado = $(".asegurado").parent('[class*="icheckbox"]').hasClass("checked");
        var vehiculo = {};
        vehiculo.Conductor = $('#nombreC').val();
        vehiculo.serie = $('#serie').val();
        vehiculo.placas = $('#placas').val();
        vehiculo.poliza = $('#poliza').val();
        vehiculo.DistribuidorClave = $('#distrib').val();
        vehiculo.plazaEncierro = $('#plaza').val();
        if (asegurado) {
            vehiculo.Asegurado = true;
        } else {
            vehiculo.Asegurado = false;
        }

        if (activo) {

            vehiculo.Activo = true;

        } else {

            vehiculo.Activo = false;
        }
        vehiculo.color = $('#color').val();
        vehiculo.modelo = $('#modelo').val();
        vehiculo.marca = $('#marca').val();
        vehiculo.Especificaciones = $('#detalles').val();
        var url = '/Vehiculos/GuardaVehiculo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: vehiculo,
            type: "POST",
            success: function (data) {
                if (data == "1") {
                    toastr.success('El Vehículo se ha agregado correctamente');                
                    llenaLista(0, 0, 0);
                    $('#addVehiculo').modal('hide');
                }
                if (data == "0") {
                   
                    toastr.error('No se pudo agregar el vehículo');
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
                
            }
        });
    
    

});



function llenaLista(distribuidor, placas, seguro) {
   
    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[20, 30, 50, 100], [20, 30, 50, 100]],
        "ajax": {
            "url": "/Vehiculos/ObtenLista/",
            "type": "POST",
            "data": {
                'data': "1",
                'distribuidor':distribuidor,
                'placas':placas,
                'seguro':seguro

            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "Distribuidor", "orderable": false },
                 { "data": "Conductor", "orderable": false },
                 { "data": "serie", "orderable": false },
                 { "data": "placas", "orderable": false },
                 { "data": "poliza", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         console.log(full.idVehiculo);
                         
                         return '<button class="btn btn-info btn-xs" onclick="VerDetalle(' + full.idVehiculo + ')" ><i class="fa fa-search"></i>Ver Detalle</button><button class="btn btn-warning btn-xs" onclick="EditaVehiculo(' + full.idVehiculo + ')"><i class="fa fa-pencil"></i>Editar</button>';


                     }

                 }
                 
        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ vehiculos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay vehiculos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay vehiculos para mostrar",
            emptyTable: "No hay vehiculos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}



function VerDetalle(id) {
    $('#dchofer').empty();
    $('#dvehiculo').empty();
    $('#DetalleVehiculo').modal('show');
    var url = '/Vehiculos/DetalleVehiculo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
          
            $('#dchofer').append("<tr><td><b>Distribuidor:</b></td><td>" + data.Distribuidor + "</td></tr><tr><td><b>Conductor:</b></td><td>" + data.Conductor + "</td></tr><tr><td><b>Póliza</b></td><td>" + data.poliza + "</td></tr>");
            $('#dvehiculo').append("<tr><td><b>Marca:</b></td><td>" + data.marca + "</td></tr><tr><td><b>Modelo:</b></td><td>" + data.modelo + "</td></tr><tr><td><b>Placas:</b></td><td>" + data.placas + "</td></tr><tr><td><b>Serie:</b></td><td>" + data.serie + "</td></tr><tr><td><b>Color:</b></td><td>" + data.color + "</td></tr><tr><td><b>Detalles generales:</b></td><td>" + data.Especificaciones + "</td></tr>");
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
       
        }
    });


}


function reset() {


    $('#nombreC').val('');
    $('#serie').val('');
    $('#placas').val('');
    $('#poliza').val('');
    $('#distrib').val(0);
    $('#plaza')
  .empty()
  .append('<option selected="selected" disabled value="x">Selecciona</option>');
    $('#color').val('');
    $('#modelo').val('');
    $('#marca').val('');
    $('#detalles').val('');

}



function EditaVehiculo(id) {
    $('#addVehiculo').modal('show');
    $('#titulowinactiva').empty();
    $('#titulowinactiva').append("Edita vehículo");

    $('#Guardar').hide();
    $('#edita').show();

    reset();
    var url = '/Vehiculos/DetalleVehiculo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
            Console.log(data);
            $('#nombreC').val(data.Conductor);
            $('#serie').val(data.serie);
            $('#placas').val(data.placas);
            $('#poliza').val(data.poliza);
            $('#distrib').val(data.DistribuidorClave);
            obtenerplazas(data.DistribuidorClave, data.plazaEncierro);            
            if (data.Asegurado) {
                $(".asegurado").iCheck('check');
            } else {
                $(".asegurado").iCheck('uncheck');
            }
            if (data.Activo) {
                $(".activo").iCheck('check');
            }
            else {
                $(".activo").iCheck('uncheck');
            }
            $('#color').val(data.color);
            $('#modelo').val(data.modelo);
            $('#marca').val(data.marca);
            $('#detalles').val(data.Especificaciones);
            $('#idVehiculo').val(data.idVehiculo)
            
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
           
        }
    });
      

}




$('#edita').click(function () {
        var activo = $(".activo").parent('[class*="icheckbox"]').hasClass("checked");
        var asegurado = $(".asegurado").parent('[class*="icheckbox"]').hasClass("checked");
        var vehiculo = {};
        vehiculo.Conductor = $('#nombreC').val();
        vehiculo.serie = $('#serie').val();
        vehiculo.placas = $('#placas').val();
        vehiculo.poliza = $('#poliza').val();
        vehiculo.DistribuidorClave = $('#distrib').val();
        vehiculo.plazaEncierro = $('#plaza').val();

        if (asegurado) {
            vehiculo.Asegurado = true;
        } else {
            vehiculo.Asegurado = false;
        }

        if (activo) {

            vehiculo.Activo = true;

        } else {

            vehiculo.Activo = false;
        }

        vehiculo.color = $('#color').val();
        vehiculo.modelo = $('#modelo').val();
        vehiculo.marca = $('#marca').val();
        vehiculo.Especificaciones = $('#detalles').val();
        vehiculo.idVehiculo = $('#idVehiculo').val()


        var url = '/Vehiculos/EditaVehiculo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: vehiculo,
            type: "POST",
            success: function (data) {
                if (data == "1") {
                    toastr.success('El Vehículo se ha editado correctamente');
                   
                    llenaLista(0, 0, 0);
                    $('#addVehiculo').modal('hide');
                }
                if (data == "0") {
                    toastr.error('No se pudo editar el vehículo');
                   

                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
               
            }
        });    
})


$('#bdistrib').change(function () {
    
    if ($('#bdistrib').val() == "0") {
        llenaLista(0, 0, 0);
    } else {
        llenaLista($('#bdistrib').val(), 0, 0);
    }
    
})


$('#pbuscar').click(function () {

    
    llenaLista(0,$('#bplacas').val(), 0);
});


$('#sbuscar').click(function () {
    

    llenaLista(0, 0,$('#bseguro').val());
  

});


$('#distrib').change(function () {

    var id = $('#distrib').val();
    $('#plaza').empty();

    var url = '/Vehiculos/ObtenerPlaza/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: {'id':id},
        type: "POST",
        success: function (data) {            
            for (var a = 0; a < data.length; a++) {
                $('#plaza').append("<option value=" + data[a].id + ">" + data[a].nombre + "</option>");
            }
            
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
            
        }
    });


});

function obtenerplazas(id,pla) {
   
    $('#plaza').empty();

    var url = '/Vehiculos/ObtenerPlaza/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                if(data[a].id==pla){
                     $('#plaza').append("<option selected value=" + data[a].id + ">" + data[a].nombre + "</option>");
                }else{
             $('#plaza').append("<option value=" + data[a].id + ">" + data[a].nombre + "</option>");
            }
               
            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}