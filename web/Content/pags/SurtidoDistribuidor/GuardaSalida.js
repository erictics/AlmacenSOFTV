﻿
$("#formaeditadistribuidor").submit(function (e) {
    e.preventDefault();

    GuardaInfo();

});





function GuardaInfo() {

    var valid = jQuery('#formaeditadistribuidor').validationEngine('validate');
    if (valid) {


        detalleSinSerie();

        //Detalles_pedido.Almacen = $(id).attr("data-almacen");
        //Detalles_pedido.pedido = $(id).attr('rel');
        //Detalles_pedido.AlmacenDestino = $(id).attr("data-destino");

        console.log(series_surtir.length);
        console.log(articulos_sin_serie.length);



        if (series_surtir.length == 0 && articulos_sin_serie.length == 0) {

            toastr.warning("Error, No puede realizar un surtido de material sin artículos");


        } else {
            var url = '/SurtidoDistribuidor/GuardaSurtido/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'pedido': Detalles_pedido.pedido, 'Aparatos': JSON.stringify(series_surtir), 'Articulos': JSON.stringify(articulos_sin_serie) },
                type: "POST",
                success: function (data) {
                    console.log(data);


                    if (data.idError == "-1") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-2") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-3") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-4") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-5") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-6") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-7") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-8") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-10") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-11") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else if (data.idError == "-12") {
                        toastr.warning("Atención " + data.Mensaje);
                    }
                    else {
                        toastr.success("Correcto, El surtido se guardó correctamente");
                        $("#editadistribuidor").modal('hide');

                        $('#repo').click();
                        $('#titulocanvas').empty().append("Detalle de surtido distribuidor");
                        $("#framecanvas").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + data);


                        // $("#eliframe").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + data);
                        //$('#ver').modal('show');
                        LlenaGrid(1);
                    }





                    console.log(data);
                },
                error: function (request, error) {
                    toastr.warning('Error, Surgió un error, intente nuevamente más tarde');
                }
            });

        }

    } else {
        toastr.warning("Error, Existen errores de llenado");
    }



};


function detalleSinSerie() {
    articulos_sin_serie = [];
    $(".saliendo").each(function () {
        var cual = $(this).attr('rel');
        if ($(this).val() > 0) {
            var Articulos = {};
            Articulos.idarticulo = cual;
            Articulos.cantidad = $(this).val();
            articulos_sin_serie.push(Articulos);

        }


    });
}
