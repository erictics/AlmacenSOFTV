﻿
LlenaGrid(1);
var series_surtir = [];
var articulos_sin_serie = [];
var Detalles_pedido = {};


function LlenaGrid(pag) {
    _pag = pag;
    var objOrdenCompraDistribuidor = {};
    objOrdenCompraDistribuidor.pag = pag;
    objOrdenCompraDistribuidor.estatus = $("#buscastatus").val();
    objOrdenCompraDistribuidor.distribuidor = $("#buscadistribuidor").val();
    objOrdenCompraDistribuidor.pedido = $("#abuscar").val();
    objOrdenCompraDistribuidor.esPagare = $("#buscapagvta").val();
    objOrdenCompraDistribuidor.pagadas = 1;

    var myJSONText = JSON.stringify(objOrdenCompraDistribuidor);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/SurtidoDistribuidor/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText },
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },
        "columns": [
            { "data": "NumeroPedido", "orderable": true },
            { "data": "Distribuidor", "orderable": true },
            { "data": "Destino", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Total", "orderable": false },
            { "data": "Estatus", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    if (full.EsPagare == "1") {
                        return "Pagare"
                    }
                    else {
                        return "Venta"
                    }
                }
            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    return DameSalidas(full.Clave, full.Estatus.substring(0, 3)) + "&nbsp;" + DameAcciones(full.Clave, full.EntidadClaveAfectada, full.EntidadClaveSolicitante, full.Estatus.substring(0, 3));
                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "order": [[0, "asc"]]
    })

}


function DameSalidas(elid, estatus) {
    return '<button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-success versalidas" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver salidas"'
        + 'onclick="VerSalidas(\'' + elid + '\')" rel=' + elid + "' " + ((estatus == "Aut" || estatus == "Can") ? "disabled" : "") + '><i class="md md-content-paste " aria-hidden="true"></i></button>';
}
function DameAcciones(elid, retirarde, poneren, estatus) {

    return '<button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-primary ver"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver orden de compra"'
        + ' onclick="VerOrdenCompra(\'' + elid + '\')" rel="' + elid + '"><i class="fa fa-search" aria-hidden="true"></i></button>'
    + "<button type='button' class='btn btn-info btn-xs seleccionarpedido' onclick='DetallePedido(this);' rel='" + elid + "' data-almacen='" + retirarde + "' data-destino='" + poneren + "' " + ((estatus == "Com" || estatus == "Can") ? "disabled" : "") + ">Surtir pedido</button>";
}



function VerOrdenCompra(id) {
    $("#comentario").val('');
    $("#autoriza_id").val(id);

    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle de surtido distribuidor");
    $("#framecanvas").attr("src", "/Pedido/Detalle?pdf=2&p=" + id);


    //$("#eliframe").attr("src", "/Pedido/Detalle?pdf=2&p=" + id);
    // $('#ver').modal('show');
};

$("#buscastatus").change(function () {
    LlenaGrid(1);
});

$("#buscadistribuidor").change(function () {
    LlenaGrid(1);
});


$("#buscapagvta").change(function () {
    LlenaGrid(1);
});

$("#buscar").click(function () {

    var valid = jQuery('#formbusca').validationEngine('validate');
    if (valid) {
        LlenaGrid(1);
    }
});

$("#buscastatus,#buscadistribuidor,#buscapagvta").change(function () {

    var valid = jQuery('#formbusca').validationEngine('validate');
    if (valid) {
        LlenaGrid(1);
    }
});

