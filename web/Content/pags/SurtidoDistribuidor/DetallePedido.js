﻿
$('#serie').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});





//jQuery('#formaeditadistribuidor').validationEngine({
//    validateNonVisibleFields: true,
//    updatePromptsPosition: false
//});





function DetallePedido(id) {
    series_surtir = [];
    articulos_sin_serie = [];
    Detalles_pedido = {};

    $("#editadistribuidor").modal("show");
    var objRoles = {};
    objRoles.pedido = $(id).attr('rel');
    Detalles_pedido.Almacen = $(id).attr("data-almacen");
    Detalles_pedido.pedido = $(id).attr('rel');
    Detalles_pedido.AlmacenDestino = $(id).attr("data-destino");

    var myJSONText = JSON.stringify(objRoles);
    $("#selecciona").hide();

    $("#articulostabledistribuidor tbody").empty();

    var url = '/Pedido/Articulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#botonesdistribuidor").removeClass('hide');
            $("#botonesdistribuidor").show();

            var info = "";
            var arreglo = eval(data);

            for (var i = 0; i < arreglo.length; i++) {

                var pendiente = parseInt(arreglo[i].Cantidad) - parseInt(arreglo[i].Surtida);

                var maximo = pendiente;
                if (maximo > parseInt(arreglo[i].Existencia)) {
                    maximo = parseInt(arreglo[i].Existencia);
                }

                if (pendiente != 0) {
                    //info += "<tr >" + "<td>" + arreglo[i].Nombre + "</td><td id='AlmacenEx" + i + "'>" + arreglo[i].Existencia + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td  id='" + arreglo[i].ArticuloClave + "'><input type='hidden' id='totalsurtido' value='" + arreglo[i].Surtida + "'>" + arreglo[i].Surtida + "</td><td id='Pen" + i + "'>" + pendiente + "</td>";
                    info += "<tr >" + "<td>" + arreglo[i].Nombre + "</td><td id='AlmacenEx" + i + "'>" + arreglo[i].Existencia + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td ><input type='hidden' id='totalsurtido' value='" + arreglo[i].Surtida + "'><input type='text' value='0' style='width:50%' id='" + arreglo[i].ArticuloClave + "' disabled></td><td id='Pen" + i + "'>" + pendiente + "</td>";

                    if (arreglo[i].catSeriesClaves.length > 0) {
                        info += '<td><button type="button" class="btn btn-warning btn-xs"   onclick="DetalleSeries(\'' + arreglo[i].ArticuloClave + '\',\'' + arreglo[i].Cantidad + '\',\'' + arreglo[i].Existencia + '\',\'' + arreglo[i].Nombre + '\',\'' + pendiente + '\')" ">Series</button></td>';
                    } else {
                        info += '<td><input type="text" placeholder="Cantidad" size=3 maxlength=4 name="c' + i + '" id="c' + i + '" rel="' + arreglo[i].ArticuloClave + '" class="form-control validate[required,custom[integer],min[0],max[' + maximo + ']] saliendo"></td>';
                    }

                    info += "</tr>";
                }

            }
            $("#articulostabledistribuidor tbody").append(info);
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

};


function DetalleSeries(idarticulo, cant_pedida, cant_inv, nombre, pendiente) {

    $('#tituloSeries').empty();
    $('#tituloSeries').append("Surtido de aparatos " + nombre);
    $('#Detalleseries').modal('show');
    $('#detallesurtido').empty();
    $('#detallesurtido').append("<tr><td><input type='hidden' id='ArticuloClave' value='" + idarticulo + "'>" + cant_inv + "</td><td id='cantped'><input type='hidden' id='cantpedida' value='" + cant_pedida + "'>" + cant_pedida + "</td><td id='cantped'><input type='hidden' id='cantpend' value='" + pendiente + "'>" + pendiente + "</td><td><input type='text' id='cantAsig' readonly style='width:50%'></td><td id='cantfalt'><input type='text' id='cantFaltante' readonly style='width:50%'></td></tr>")

    Aparatos(idarticulo);
    actualizaContador();
}



function activa(ArticuloClave, InventarioClave, Valor, Nombre, elem) {
    $(elem).css('width', '25px !important');
    $(elem).css('height', '25px !important;');
    $(elem).css('padding', '2px;');
    var Serie = {};
    Serie.InventarioClave = InventarioClave;
    Serie.catSerieClave = 2;
    Serie.Valor = Valor;
    Serie.ArticuloClave = ArticuloClave;
    Serie.Nombre = Nombre;
    Serie.Cantidad = 1;
    Serie.status = 7;
    Serie.Statustxt = "";


    if (Valida_Serie_arreglo(InventarioClave) == "1") {
        EliminarDeArreglo(series_surtir, 'InventarioClave', InventarioClave);
        ActualizaDetalle();
        actualizaContador();
        Aparatos(ArticuloClave);
    }
    else {
        series_surtir.push(Serie);
        actualizaContador();
        ActualizaDetalle();
        Aparatos(ArticuloClave);
    }
}

function Valida_Serie_arreglo(idinventario) {
    var result = $.grep(series_surtir, function (e) { return e.InventarioClave == idinventario });
    if (result.length == 0) {
        return "0";
    } else {
        return "1";
    }
}



function EliminarDeArreglo(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}






$('#serie').bind("enterKey", function (e) {

    if ($('#serie').val() == "") {
        $('#serie').val('');
        toastr.warning("Error, Necesita ingresar una serie válida");
    }
    else {
        if (parseInt($('#cantpend').val()) > parseInt(cantidadagregada($('#ArticuloClave').val()))) {
            validaserie($('#serie').val(), $('#ArticuloClave').val());
            $('#serie').val('');
        }
        else {

            toastr.error("Error, El surtido de pedido esta completo");
        }
    }
});





function validaserie(serie, articulo) {

    var url = '/SurtidoDistribuidor/ValidaSerie/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie, 'articulo': articulo },
        type: "POST",
        success: function (data) {

            if (data == "0") {
                toastr.warning("Atención, La serie no existe en el inventario, no pertenece al artículo seleccionado o no esta disponible en este almacén");
            } else {
                var Serie = {};
                Serie.InventarioClave = data.InventarioClave;
                Serie.catSerieClave = data.catSerieClave;
                Serie.Valor = data.Valor;
                Serie.ArticuloClave = data.ArticuloClave;
                Serie.Nombre = data.Nombre;
                Serie.Cantidad = 1;
                Serie.status = 7;
                Serie.Statustxt = "";

                if (Valida_Serie_arreglo(data.InventarioClave) == "1") {
                    toastr.warning("Error, La serie ya fue agregada a la orden");

                } else {
                    series_surtir.push(Serie);
                    actualizaContador();
                    ActualizaDetalle();
                    Aparatos(data.ArticuloClave);
                }
            }
        },
        error: function (request, error) {

            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

}



function Valida_Serie_arreglo(idinventario) {
    var result = $.grep(series_surtir, function (e) { return e.InventarioClave == idinventario });
    if (result.length == 0) {
        return "0";
    } else {
        return "1";
    }
}



function actualizaContador() {
    var result = $.grep(series_surtir, function (e) { return e.ArticuloClave == $('#ArticuloClave').val() });
    if (result == null) {
        result = 0;
    }

    $('#cantAsig').val(result.length);
    var cantpedida = $('#cantpend').val();
    $('#cantFaltante').val(parseInt(cantpedida) - parseInt(result.length));
}


function cantidadagregada(idarticulo) {
    var result = $.grep(series_surtir, function (e) { return e.ArticuloClave == idarticulo });
    var cantidad = 0;
    if (result.length == 0) {
        return 0;
    } else {
        for (var a = 0; a < result.length; a++) {
            cantidad = parseInt(cantidad) + parseInt(result[a].Cantidad);
        }
        return cantidad;
    }

}

function ActualizaDetalle() {

    var linq = Enumerable.From(series_surtir);
    var result =
        linq.GroupBy(function (x) { return x.Nombre; })
            .Select(function (x) {
                return { Nombre: x.Key(), Clave: x.source[0].ArticuloClave, Cantidad: x.Sum(function (y) { return y.Cantidad | 0; }) };
            })
            .ToArray();
    console.log(result);
    for (var a = 0; a < result.length; a++) {
        $("#" + result[a].Clave + "").val(result[a].Cantidad);
    }
}




//$(document).on('keyup', '#serie', function (e) {
//    if (e.keyCode == 13) {
//        $(this).trigger("enterKey");
//    }
//});





