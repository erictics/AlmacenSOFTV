﻿function Aparatos(idarticulo) {




    $('#tablaseries').dataTable({
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": "/SurtidoDistribuidor/Aparatos/",
            "type": "POST",
            "data": {
                'articulo': idarticulo,
                'Articulos': JSON.stringify(series_surtir)
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         if (full.seleccionado == 1) {
                             return '<button class="btn btn-info btn-xs .selecciona" style=" width: 25px !important; height: 25px !important;padding:2px;"  onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';

                         }
                         else {

                             if (parseInt($('#cantpend').val()) > parseInt(cantidadagregada(idarticulo))) {
                                 return '<button class="btn btn-defaul-bright btn-xs .selecciona" style="color:#FFFFFF;"   onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)"  ><i class="fa fa-check" aria-hidden="true"></i></button>';

                             } else {

                                 return '<button class="btn btn-defaul-bright btn-xs .selecciona" style="color:#FFFFFF;"   onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" disabled><i class="fa fa-check" aria-hidden="true"></i></button>';
                             }


                         }

                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         return "<button class='btn btn-default-bright btn-xs'>" + full.Nombre + "</button>"
                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         return "<button class='btn btn-default-bright btn-xs'>" + full.valor + "</button>"
                     }
                 }


        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Aparatos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",

            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}