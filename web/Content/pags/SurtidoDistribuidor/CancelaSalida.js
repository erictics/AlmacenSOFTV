﻿var idEliminar = {}

function CancelaSalidas(id) {
    idEliminar = {};
    idEliminar.id = id;
    $('#eliminasalida').modal('show');
}


$('#eliminarsalida').click(function () {

    var url = '/SurtidoDistribuidor/CancelaSalida/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idEliminar.id },
        type: "POST",
        success: function (data) {

            if (data == "0") {

                toastr.warning("¡Atención!, Se evitó la cancelación, esto puede ocurrir por que la sesión ha expirado o ocurrió un error al ejecutar la cancelación, vuelva a iniciar sesión o contacte al administrador ")
            }
            else if (data == "-1") {
                toastr.warning("¡Atención!, Se evitó la cancelación, La salida se se eliminó de la base de datos");
            }
            else if (data == "-2") {
                toastr.warning("¡Atención!, Se evitó la cancelación, El pedido no se encontró en el sistema");
            }
            else if (data == "-3") {
                toastr.warning("¡Atención!, Se evitó la cancelación, La salida ya fue cancelada ");
            }
            else if (data == "-4") {
                toastr.warning("¡Atención!, Se evitó la cancelación, La salida ya fue recepcionada parcialmente por el distribuidor");
            }
            else if (data == "-5") {
                toastr.warning("¡Atención!, se evitó la cancelación, La salida ya fue recepcionada completamente por el distribuidor");
            }
            else if (data == "-6") {
                toastr.warning("¡Atención!, Se evitó la cancelación, La salida fue modificada en la base de datos y no cuenta con registros PA");
            }
            else if (data == "-7") {
                toastr.warning("¡Atención!, Se evitó la cancelación, La salida fue modificada en la base de datos y no cuenta con registros PI");
            }
            else if (data == "-8") {
                toastr.warning("¡Atención!, Se evitó la cancelación, Alguno de los aparatos no se encuentra en standby, en buen estado o no esta en el inventario de almacén principal");
            }
            else if (data == "-9") {
                toastr.warning("¡Atención!, Se evitó la cancelación, la cantidad de artículos en standby sobrepasa la cantidad que se quiere cancelar contacte al administrador para mayor información");
            }
            else {

                toastr.success("Correcto, La salida se canceló correctamente");
                $('#eliminasalida').modal('hide');
                $('#salidasdetalle').modal('hide');
                LlenaGrid(1);

            }
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });


});