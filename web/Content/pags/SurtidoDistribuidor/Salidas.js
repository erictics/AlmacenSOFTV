﻿function VerSalidas(id) {

    ShowSalidas(id);
};

function ShowSalidas(cual) {
    $("#titulosalidasdetalle").html("Ver Salidas");
    $('#salidasdetalle').modal('show');
    var objRoles = {};
    objRoles.pag = 1;
    objRoles.pedido = cual;
    objRoles.tipoentidad = 1;
    var myJSONText = JSON.stringify(objRoles);
    var url = '/SalidaMaterial/ObtenLista2/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            $("#lassalidas tbody").empty();
            var arreglo = eval(data);
            var tbody = '';
            for (var i = 0; i < arreglo.length; i++) {
                tbody += "<tr>";
                tbody += "<td>" + arreglo[i].NumeroPedido + "</td>";
                tbody += "<td>" + arreglo[i].Fecha + "</td>";
                arreglo[i].Estatus.replace('Por autorizar', 'Por recepcionar');
                tbody += "<td >" + arreglo[i].Estatus + "</td>";
                tbody += "<td ><button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-primary versalida' data-toggle='tooltip' data-placement='top' title='' data-original-title='Detalle salida'  rel='" + arreglo[i].Clave + "'><i class='fa fa-search' aria-hidden='true'></i></button>";

                if (arreglo[i].Estatus != "Cancelado") {
                    tbody += "&nbsp;<button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-info envio'  rel='" + arreglo[i].Clave + "' data-send='" + arreglo[i].Total_USD + "'><i class='fa fa-envelope'></i></button>";
                }
                tbody += "";

                tbody += "<td>";
                if (arreglo[i].Estatus != "Cancelado") {

                    tbody += "<button type='button' class='btn btn-warning btn-xs editarsalida' style='display:none;' data-pedido='" + arreglo[i].NumeroPedido + "' rel='" + arreglo[i].Clave + "'>&nbsp;Editar</button>";
                    if (arreglo[i].Estatus != "Completo" && arreglo[i].Estatus != "Parcial") {
                        tbody += '<button type="button" style="display:block" onclick="CancelaSalidas(\'' + arreglo[i].Clave + '\')"  class="btn btn-danger btn-xs cancelarsalida" rel="' + arreglo[i].Clave + '"><i class="fa fa-trash-o" aria-hidden="true"></i> Cancelar</button>';
                    }
                }
                tbody += "</td>";
                tbody += "</tr>";
            }

            $("#lassalidas tbody").append(tbody);

            $(".versalida").click(function () {
                var elrel = $(this).attr('rel');

                //$('#repo').click();
                //$('#titulocanvas').empty().append("Detalle de surtido distribuidor");
                //$("#framecanvas").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + elrel);

                $("#eliframesalida").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + elrel);
                $('#versalida').modal('show');
            });

            $(".envio").click(function () {
                _sub = 1;
                //Envio($(this).attr("rel"), $(this).attr("data-send"));
            });


        }
    });
}