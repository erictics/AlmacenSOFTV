﻿$('#Distribuidor').change(function () {       
    $('#recepcion').empty().append('<option value="1">Con orden de compra</option>  <option value="2">Sin orden de compra</option>');
    obtenAlmacenes();
});

function obtenAlmacenes( ) {
    var url = '/Almacen/GetAlmacenByDistribuidor/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'distribuidor': $('#Distribuidor').val() },
        type: "POST",
        success: function (data) {
            $('#almacen').empty();
            $('#almacen').append("<option value=''>Todos los almacenes</option>");
            for (var r = 0; r < data.length; r++) {
                $('#almacen').append("<option value=" + data[r].entidadClave + ">" + data[r].Nombre + "</option>");;
            }

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
}

$('#clasificacion').change(function () {
    if ($('#clasificacion').val() == null || $('#clasificacion').val()==undefined || $('#clasificacion').val() == "") {
        $('#tipo').empty().append("<option value=''>selecciona</option>");
        return;
    }
    $('#tipo').empty();
    var url = '/Reportes/GetTipoArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': $('#clasificacion').val() },
        type: "POST",
        success: function (data) {
            $('#tipo').append("<option value='0'>Todos los tipos</option>");
            for (var r = 0; r < data.length; r++) {
                $('#tipo').append("<option value=" + data[r].catTipoArticuloClave + ">"+data[r].Descripcion+"</option>");;
            } 
            
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

});

$('#tipo').change(function () {
    if ($('#tipo').val() == null || $('#tipo').val() == undefined) {
        $('#articulo').append("<option>Selecciona</option>");
        return;
    }

    var id = $('#tipo').val();
    var idservicio= $('#servicio').val();
    $('#articulo').empty();
    if ($('#tipo').val() == "0") {
        $('#articulo').append("<option value='0'>Todos los artículos</option>");
        return;
    }

    var url = '/Articulo/GetArticuloByTipo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
            if (data.length > 0) {
                $('#articulo').append("<option value='0'>Todos los Artículos</option>");
                for (var r = 0; r < data.length; r++) {
                    $('#articulo').append("<option value=" + data[r].EntidadClave + ">" + data[r].Nombre + "</option>");
                }
            } else {
                $('#articulo').append("<option>No asignado</option>");
            }
            
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
});

$('#pdfexcel').click(function () {
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    var servicio = $('#servicio').val();
    var proveedor = $('#proveedor').val();
    var TipoArticulo = $('#tipo').val();
    var Articulo = $('#articulo').val();
    var recepcion = $('#recepcion').val();
    var Distribuidor = $('#Distribuidor option:selected').text();
    var clasificacion = $('#clasificacion option:selected').text();
    var reporte = $('#reporte').val();
    if (Almacenes.length == 0) {
        toastr.warning("Seleccione al menos un almacén");
        return;
    }
    var valid = jQuery('#formreporte').validationEngine('validate');
    if (valid) {
        window.location.href = '/ReportesGenerales/RecepcionesExcel?Almacenes=' + Almacenes +
       '&fechainicio=' + fechainicio +
       '&fechaFin=' + fechafin +
       '&servicio=' + servicio +
       '&proveedor=' + proveedor +
       '&TipoArticulo=' + TipoArticulo +
        '&Articulo=' + Articulo +
        '&recepcion=' + recepcion +
        '&Distribuidor=' + Distribuidor +
        '&clasificacion=' + clasificacion +
           '&reporte=' + reporte;
    }


});

$('#pdfrecepcion').click(function () {
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    var servicio = $('#servicio').val();
    var proveedor = $('#proveedor').val();
    var TipoArticulo = $('#tipo').val();
    var Articulo = $('#articulo').val();
    var recepcion = $('#recepcion').val();
    var Distribuidor = $('#Distribuidor option:selected').text();
    var clasificacion = $('#clasificacion').text();
    var almacen = $('#almacen').val();
  
   
    var valid = jQuery('#formreporte').validationEngine('validate');
    if (valid) {
        window.location.href = '/ReportesGenerales/Detalle?almacen=' + almacen +
       '&fechainicio=' + fechainicio +
       '&fechaFin=' + fechafin +
       //'&servicio=' + servicio +
       '&proveedor=' + proveedor +
       '&TipoArticulo=' + TipoArticulo +
        '&Articulo=' + Articulo +
        //'&recepcion=' + 1 +
        '&Distribuidor=' + Distribuidor +
        '&clasificacion=' + clasificacion;
        //'&reporte=' + 1;
    }


});




