﻿$(document).ready(function () {

    llenaLista();

});



$("#distribuidor").change(function () {
    llenaLista();
});

$("#rol").change(function () {
    llenaLista();
});

$("#obten").click(function () {
    llenaLista();
});




function llenaLista() {

    var objUsuarios = {};
    objUsuarios.pag = 1;
    objUsuarios.rol = $("#rol").val();
    objUsuarios.distribuidor = $("#distribuidor").val();
    objUsuarios.keyword = $("#keyword").val();

    var myJSONText = JSON.stringify(objUsuarios);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[10, 20, 30, 50], [10, 20, 30, 50]],
        "ajax": {
            "url": "/Usuarios/ObtenUsuarios/",
            "type": "POST",
            "data": {
                'data': myJSONText
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "Nombre", "orderable": false },
                 //{ "data": "Sucursal", "orderable": false },
                 { "data": "Rol", "orderable": false },
                 { "data": "Correo", "orderable": false },                 
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         var str;
                         if (full.Activo == 1) { str = "<button class='btn btn-info btn-xs'>Activo</button>" }
                         else { str = str = "<button class='btn btn-default btn-xs'>Deshabilitado</button>" }
                         return str;


                     }

                 },
                 // { "data": "Nombre", "orderable": false },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.Activo == true) {
                              return '<button type="button" class="btn btn-warning btn-xs edit" onClick="Edita(\'' + full.Id + '\')" ><i class="fa fa-pencil"></i> Editar</button>';
                          }
                          else {
                              return '<button type="button" class="btn btn-warning btn-xs edit" onClick="Edita(\'' + full.Id + '\')" >&nbsp;Editar</button>';
                          }
                      }

                  }
                 

        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay almacenes para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay almacenes para mostrar",
            emptyTable: "No hay almacenes disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}