﻿$("#add").click(function () {
    $("#titulowin").html("Agregar Usuario");
    $('#edita').modal('show');

    $("#ed_nombre").val('');
    $("#ed_paterno").val('');
    $("#ed_materno").val('');
    $("#ed_rol").val('');
    $("#ed_clave").val('');
    $("#ed_email").val('');
    $("#ed_id").val('');
    $("#ed_estatus").val('');
    $("#ed_distribuidor").val('-1');

    ShowAlmacenes($("#ed_distribuidor").val(), null);
});

$('#ed_distribuidor').change(function () {

    ShowAlmacenes($("#ed_distribuidor").val(), null);
})



$("#formaedita").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        $("#loader").show();

        var url = '/Usuarios/Guarda/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaedita').serialize(),
            type: "POST",
            success: function (data) {

                if (data == "-2") {
                    toastr.warning("Atención,Este correo ya esta registrado");
                }
                else {
                    $("#edita").modal('hide');
                    llenaLista();
                    toastr.success('Correcto,El usuario se guardó correctamente');
                }
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    }

});


$("#formaalmacen").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        var url = '/Usuarios/Almacenes/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaalmacen').serialize(),
            type: "POST",
            success: function (data) {

                $("#almacen").modal('hide');
               
                toastr.success('Correcto,El usuario se guardó correctamente');
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    }

});


function Edita(cual) {
    
    $("#titulowin").html("Edita Usuario");
    $('#edita').modal('show');

    var url = '/Usuarios/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {
            console.log(data);
            var oUsuario = eval(data);

            $("#ed_nombre").val(oUsuario.Nombre);
            $("#ed_paterno").val(oUsuario.Paterno);
            $("#ed_materno").val(oUsuario.Materno);
            $("#ed_rol").val(oUsuario.RolId);
            $("#ed_clave").val(oUsuario.Clave);
            $("#ed_email").val(oUsuario.Correo);
            $("#ed_id").val(oUsuario.Id);
            $("#ed_distribuidor").val(oUsuario.SucursalClave);

            if (oUsuario.Activo) {

                $("#ed_estatus").val(1);

            } else {
                $("#ed_estatus").val(0);
            }
           
            $("#imgfirma").attr("src", "/Content/media/" + oUsuario.Firma + "");
            $("#edfirma_valor").val(oUsuario.Firma);

            ShowAlmacenes($("#ed_distribuidor").val(), oUsuario.Id);


            llenaLista();

        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });

};


function ShowAlmacenes(distribuidor, usuario) {

    $('#almacen').modal('show');
    $("#distribuidoralmacen_id").val(distribuidor);
    $("#usuarioalmacen_id").val(usuario);

    var url = '/Almacen/ObtenPorDistribuidor/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': distribuidor + "|" + usuario },
        type: "POST",
        success: function (data) {

            $("#almacenes").empty();
            var arreglo = eval(data);

            var html = '';
            for (var i = 0; i < arreglo.length; i++) {
                html += "<div class='col-md-12'>";
                html += "<input type='checkbox' ";

                if (arreglo[i].Activo == "1") {
                    html += "checked"
                }

                html += " class='checkbox' name='almacen" + arreglo[i].Clave + "' id='almacen" + arreglo[i].Clave + "' /> ";
                html += arreglo[i].Nombre;
                html += "</div>";
            }

            $("#almacenes").append(html);

            $('input[type="checkbox"]:not(".switch")').iCheck({
                checkboxClass: 'icheckbox_square-blue ',
                increaseArea: '20%' // optional
            });

            //$('.checkbox').iCheckbox();
            //LlenaGrid(_pag);

        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

function DameScope(elid, distribuidor) {
    return "<button type='button' class='btn btn-default btn-xs almacen' rel='" + elid + "' data-distribuidor='" + distribuidor + "'>&nbsp;Almacenes</button>";
}