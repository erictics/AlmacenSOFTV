﻿
llenaLista();

var id_editar = "";

$('#add').click(function () {
    id_editar = "";
    $('#edita').modal('show');
    $('#titulowin').empty().append("Agrega Servicio");
    $('#ed_activa').iCheck('uncheck');
    $('#ed_nombre').val();
});

$('#guarda').click(function () {
    Servicio = {};
    Servicio.idServicio = id_editar;
    Servicio.Nombre = $('#ed_nombre').val();
    if ($('#ed_activa').is(":checked")) {
        Servicio.Activo = true;
    }
    else {
        Servicio.Activo = false;
    }
    var url;
    if (id_editar == "") {
        url = '/Servicios/GuardaServicio/';
    }
    else {
        url = '/Servicios/EditaServicio/';
    }

    $.ajax({
        url: url,
        dataType: 'json',
        data: Servicio,
        type: "POST",
        success: function (data) {
            $('#edita').modal('hide');
            if (data == "1") {
                toastr.success("Correcto", "El servicio se ha agregado correctamente");
                llenaLista();
            }else if(data=="-2"){
                toastr.warning( "El servicio no se puede editar ya que hay artículos asignados a este servicio");
            } else if (data == "-3") {
                toastr.warning("El servicio no se puede editar ya que hay almacenes asignados a este servicio");
            }
            else {
                toastr.warning("Error", "Ocurrio un error ,no se pudo agregar el servicio");
            }
        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
});






function EditaServicio(id) {
  
    $('#edita').modal('show');
    $('#titulowin').empty().append("Edita Servicio");

    var url = '/Servicios/DetalleServicio/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'IdServicio': id },
        type: "POST",
        success: function (data) {
            console.log(data);
            id_editar = data.idServicio;
            data.Nombre;
            if (data.Activo == true) {
                $('#ed_activa').iCheck('check');

            } else {
                $('#ed_activa').iCheck('uncheck');
            }
            $('#ed_nombre').val(data.Nombre)
        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}


function llenaLista() {

    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[20, 30, 50, 100], [20, 30, 50, 100]],
        "ajax": {
            "url": "/Servicios/ListaServicios/",
            "type": "POST",
            "data": {
                'data': "1"
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "IdServicio", "orderable": false },
                 { "data": "Descripcion", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         console.log(full.Activo);
                         var str;
                         if (full.Activo == 1) { str = "<button class='btn btn-info btn-xs'>Activo</button>" }
                         else { str = str = "<button class='btn btn-default btn-xs'>Deshabilitado</button>" }
                         return str;


                     }

                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         var str;
                         str = "<button class='btn btn-warning btn-xs' onclick=EditaServicio(" + full.IdServicio + ")><i class='fa fa-pencil' aria-hidden='true'></i> Editar</button>"
                         return str;


                     }

                 },

        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}
