﻿
function recepcion(devolucion) {
    $('#DetalleDevolucion').modal('show');
    $('#contenidoresumen').empty();
    $('#contenidoseries').empty();
    $('#iddev').val(devolucion);
    var url = '/RecepcionDevSAC/Detalledevolucion/';
    $.ajax({
        url: url,
        data: { 'devolucion': devolucion },
        type: "POST",
        success: function (data) {

            for (var a = 0; a < data.length; a++) {
                console.log(data[a].tieneseries);
                if (data[a].tieneseries == false) {
                    $('#contenidoresumen').append("<tr><td>" + data[a].articulo + "</td><td>" + data[a].Cantidad + "</td></tr>");
                }
                else {
                    $('#contenidoresumen').append("<tr><td>" + data[a].articulo + "</td><td>" + data[a].Cantidad + "</td><td><button class='btn btn-success btn-xs series' rel='" + devolucion + "' data-claveart='" + data[a].ClaveArticulo + "'>Series</button></td></tr>");
                }

            };

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });


};

$('#tabladetalle').on('click', '.series', function () {
    $('#listaaparatos').empty();
    //$('#DetalleSerie').modal('show');
    $('#contenidoseries').empty();
    var devolucion = $(this).attr('rel');
    var articulo = $(this).attr('data-claveart');
    var url = '/RecepcionDevSAC/detalleSeries/';
    $.ajax({
        url: url,
        data: { 'devolucion': devolucion, 'articulo': articulo },
        type: "POST",
        success: function (data) {
            console.log(data);
            for (var a = 0; a < data.length; a++) {

                var str='<li class="tile"><a class="tile-content ink-reaction" href="#2"><div class="tile-icon">';
                str+='<i class="fa fa-inbox"></i></div>'
                str += '<div class="tile-text" style="font-size:12px;">' + data[a].valor + '&nbsp;&nbsp;&nbsp;&nbsp;'+data[a].Nombre+'</div></a><a class="btn btn-flat ink-reaction">';
                str+='<button class="btn btn-warning btn-sm">'+data[a].estatus+'</button></a></li>';                               
                $('#listaaparatos').append(str);
            }
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
});



$('#guardaRecepcion').click(function () {

    var devolucion = $('#iddev').val();
    var url = '/RecepcionDevSAC/guarda/';
    $.ajax({
        url: url,
        data: { 'devolucion': devolucion },
        type: "POST",
        success: function (data) {

            if (data == "-1") {
                toastr.warning("Atención, se evitó la recepción, Alguno de los aparatos ya fue recibido por el almacén principal.");
            }
            else if (data == "-2") {
                toastr.warning("Error, La devolución fue cancelada por el distribuidor");
            }
            else {

                $('#DetalleDevolucion').modal('hide');
                llenaGrid();
                $('#repo').click();
                $('#titulocanvas').empty().append("Detalle recepción de material");
                $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + data);
            }

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

});