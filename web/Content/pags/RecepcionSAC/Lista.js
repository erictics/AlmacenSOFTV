﻿$(document).ready(function () {
    llenaGrid(0, 0);
});

function llenaGrid(alm, orden) {



    if (alm == undefined) {
        alm = "";
    }
    if (orden == undefined || orden == "") {
        orden = 0;
    }
    var data = { 'data': "1", 'almacen': alm, 'orden': orden };
    var url = "/RecepcionDevSAC/ListaDevoluciones/";
    var columnas = [{ "data": "pedido", "orderable": false },
                   { "data": "fecha", "orderable": false },
                   {
                       sortable: false, "render": function (data, type, full, meta) {
                           var str = "";
                           if (full.estatus == 1) { str = "Por Autorizar" }
                           else if (full.estatus == 4) { str = "Recibido" }
                           else { str = "Cancelada" }
                           return str;
                       }
                   },
                 {
                     sortable: false, "render": function (data, type, full, meta) {

                         return full.almacen;
                     }
                 },
                 {
                     sortable: false, "render": function (data, type, full, meta) {

                         return full.Destino;
                     }
                 },
                 {
                     sortable: false, "render": function (data, type, full, meta) {

                         return '<button class="btn ink-reaction btn-floating-action btn-xs btn-warning  " onClick="descargapdf(\'' + full.proceso + '\')"><i class="fa fa-download" aria-hidden="true" ></i></button>&nbsp<button class="btn ink-reaction btn-floating-action btn-xs btn-primary " onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search" aria-hidden="true"></i></button>';
                     }
                 },
                 {
                     sortable: false, "render": function (data, type, full, meta) {
                         if (full.estatus == 1) {
                             return '<button class="btn btn-success btn-xs  " id="recibir" onClick="recepcion(\'' + full.proceso + '\')"><i class="md md-assignment-returned"></i> Recibir material</button>';
                         }
                         if (full.estatus == 4) {
                             return '<button class="btn btn-accent btn-xs  " onClick="verrecepcion(\'' + full.Recepcion + '\')"><i class="md md-assignment-turned-in"  aria-hidden="true"></i> ver recepción</button>&nbsp<button class="btn btn-default-bright btn-xs  " style="display:none;" onClick="descargarecepcion(\'' + full.Recepcion + '\')">PDF</button>';
                         }
                     }
                 }
    ];

       

        $('#indice').dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bDestroy": true,
            "info": true,
            "stateSave": true,
            "lengthMenu": [[5], [5]],
            "ajax": {
                "url": url,
                "type": "POST",
                "data":data
            },
          

            "columns": columnas,

            language: {

                search: "Buscar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ orden",
                info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
                infoEmpty: "No hay elementos para mostrar",
                infoFiltered: "(filtrados _MAX_ )",
                infoPostFix: "",
                //loadingRecords: "Búsqueda en curso...",
                zeroRecords: "No hay elementos para mostrar",
                emptyTable: "No hay elementos disponibles",
                paginate: {
                    first: "Primera",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultima"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },


            "order": [[0, "asc"]]
        })

    

}


$('#almacen').change(function () {
    llenaGrid($('#almacen').val(),0);
});

$('#buscar').click(function () {
    llenaGrid(0, $('#orden').val());
});

function verrecepcion(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción SAC");
    $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + devolucion);

};

function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle devolución SAC");
    $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + devolucion);
}


function descargapdf(devolucion) {
    window.location.href = "/Devolucion/Detalle?pdf=1&d=" + devolucion;
};