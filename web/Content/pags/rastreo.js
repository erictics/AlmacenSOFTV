﻿$('#buscar').click(function () {
    if ($('#abuscar').val() == undefined || $('#abuscar').val() == null || $('#abuscar').val() == "") {
        toastr.warning('Coloque una serie correcta');
    } else {
        var url = '/Rastreo/DetalleAparato/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'serie': $('#abuscar').val() },
            type: "POST",

            success: function (data) {
                if (data == "0" || data == "-1") {
                    $('#resultado').empty();
                    $('#resultado').append(' <div class="col-md-4 text-right" ><i class="md md-history" style="font-size:80px; color:#4CAF50"></i>' +
                '</div>  <div class="col-md-6 text-center" style="margin-top:50px;">' + '<h3 style="font-weight:bold;">No se encontró un equipo asignado a esa serie</h3>' +
                '</div>');

                }
                else if (data == "-3") {
                    $('#resultado').empty();
                    $('#resultado').append(' <div class="col-md-4 text-right" >' +
                '</div>  <div class="col-md-6 text-center" style="margin-top:20px;">' + '<i class="md md-cloud-off" style="font-size:80px; color:red"></i> <h3 style="font-weight:bold;">No tienes acceso a esta información</h3>' +
                '</div>');

                }


                else {
                    $('#resultado').empty();
                    data.forEach(function(ele){
                        $('#resultado').append(
                            '<tr>' +
                            '<td style="font-weight:bold;">' + ele.serie + '</td>' +
                            '<td style="font-weight:bold;">' + ele.Nombre + '</td>' +
                            '<td style="font-weight:bold;">' + ele.Ubicacion + '</td>' +
                            '<td style="font-weight:bold;">' + ele.Status + '</td>' +
                            '</tr>'
                        )
                    });



                    //$('#resultado').forEach(function(){

                    //}
                        //data.forEach(function(ele){
                            /*'<td>' + ele.serie + '</td>' +
                            '<td>' + ele.Nombre + '</td>' +
                            '<td>' + ele.Ubicacion + '</td>' +
                            '<td>' + ele.Status + '</td>'*/
                        //})
                   // );
                    /*$('#resultado').append('<div class="col-md-4 text-right" style="margin-top:50px;">' +
                   '<i class="md md-settings-remote" style="font-size:150px; color:#3F51B5"></i></div>' +
                   '<div class="col-md-6 text-center" style="margin-top:50px;">' +
                   '<h1 style="font-weight:bold;">' + data.Nombre + '</h1>' +
                   '<h3 style="font-weight:bold;">' + data.Ubicacion + '</h3>' +
                   '<h4>' + data.serie + '</h4><h4>' + data.Status + '</h4>' +
                   //'<button class="btn btn-success btn-sm ink-reaction" onClick="historial(\'' + data.idInventario + '\',\'' + data.serie + '\',\'' + data.Nombre + '\')" id="historial">Historial de aparato</button>' +
               '</div>');*/
                }
            },
            error: function (request, error) {
                $('#resultado').empty();
                $('#resultado').append(' <div class="col-md-4 text-right" ><i class="md md-history" style="font-size:100px; color:#607d8b"></i>' +

            '</div>  <div class="col-md-6 text-center" style="margin-top:50px;">' + '<h3 style="font-weight:bold;">No se encontró un equipo asignado a esa serie </h3>' +

            '</div>');

                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});