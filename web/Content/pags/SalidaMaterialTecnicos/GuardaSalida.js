﻿$('#guardaSalida').click(function () {

    if (articulos.length == 0) {
        toastr.warning('Atención, No puede realizar una salida sin artículos');
       

    }
    else if ($('#ed_almacen').val() == "0") {
        toastr.warning('Atención, No puede realizar una salida sin seleccionar un almacén');
        
    }
    else if ($('#ed_tecnico').val() == "0" || $('#ed_tecnico').val() == null) {
        toastr.warning('Atención, No puede realizar una salida sin seleccionar un técnico');
      
    }
    else if ($('#ed_tiposalida').val() == "0") {

        toastr.warning('Atención, No puede realizar una salida sin seleccionar un tipo de salida');       
    }
    else {


        var url = '/SalidaMaterialTecnico/GuardaSalida/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'idAlmacen': $('#ed_almacen').val(), 'idTecnico': $('#ed_tecnico').val(), 'salida': $('#ed_tiposalida').val(), 'observaciones': $('#ed_observaciones').val(), 'articulos': JSON.stringify(articulos) },
            type: "POST",
            success: function (data) {
                if (data.idError == "-1") {
                   
                    toastr.warning("Atención", data.Mensaje);
                }
                else if (data.idError == "-2") {
                    toastr.warning("Atención", data.Mensaje);
                }
                else if (data.idError == "-3") {
                    toastr.warning("Atención", data.Mensaje);
                }
                else if (data.idError == "-4") {
                    toastr.warning("Atención", data.Mensaje);
                }
                else if (data.idError == "-5") {
                    toastr.warning("Atención", data.Mensaje);
                }
                else if (data.idError == "-6") {
                    toastr.warning("Atención", data.Mensaje);
                }
                else if (data.idError == "-7") {
                    toastr.warning("Atención", data.Mensaje);
                }
                else {
                    toastr.success('Correcto, La salida de material se realizó correctamente');
                   
                    LlenaGrid(1);
                    $('#edita').modal('hide');

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle de salida a técnico");
                    $("#framecanvas").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + data);

                 
                    
                }
               
                


            },
            error: function (request, error) {
                toastr.error('Error, Surgió un error, intente nuevamente más tarde');
              
            }
        });




    }



});