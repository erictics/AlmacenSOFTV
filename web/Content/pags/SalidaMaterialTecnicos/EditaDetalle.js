﻿function EditaArticulo(idArticulo,cantidad) {
    $('#articulo').modal('show');
    $('#guardaAccesorios').hide();
    var url = '/SalidaMaterialTecnico/DetalleArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idarticulo': idArticulo },
        type: "POST",
        success: function (data) {

            $('#ed_clasificacionmaterial').val(data.idClasificacion);
            $('#ed_tipomaterial').val(data.idTipoArticulo);
            var valor = AsignaArticulo(data.idArticulo, data.idTipoArticulo);
            console.log(valor);
          
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');            
        }
    });
    
}

function AsignaArticulo(idArticulo,idTipoArticulo) {

    var a="";
    $('#ed_articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');
    
    var url = '/SalidaMaterialTecnico/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idTipoArticulo },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                if (data[a].ArticuloClave == idArticulo) {

                    $('#ed_articulo').append('<option selected rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');

                    var b = (data[a].catSerieClaveInterface == null ? "0" : "1");
                    DetalleArticulo(b);
                } else {

                    $('#ed_articulo').append('<option rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');
                }
                               
            }



        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

    return a;

}

