﻿function LlenaGrid(pag) {
    _pag = pag;
    var objRoles = {};
    objRoles.pag = pag;
    objRoles.tecnico = $("#tecnico").val();
    objRoles.buscar = $("#abuscar").val();
    var myJSONText = JSON.stringify(objRoles);


    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/SalidaMaterial/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText },
        },
        "columns": [
            { "data": "NumeroPedido", "orderable": true },
            { "data": "Pedido", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Destino", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    console.log(full);
                    var estatus = "Entregado";
                    if (full.Estatus == "Cancelado") {
                        estatus = "Cancelado";
                    }
                    return estatus;
                }
            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {

                    var estatus = "Entregado";
                    if (full.Estatus == "Cancelado") {
                        estatus = "Cancelado";
                    }

                    return DameOpciones(full.Clave, estatus)
                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })


}


function DameOpciones(elid, estatus) {
    var  opciones = '<button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"'
        + '  onClick="Descargapdf(\'' + elid + '\')"  rel="' + elid + '"><i class="md-file-download"></i></button>';
        opciones += '<button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-success ver" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver detalle de salida a técnico"'
        + ' onClick="verpdf(\'' + elid + '\')" rel="' + elid + '"><i class="fa fa-search"></i></button>';   
    return opciones;
}


function verpdf(id) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle de salida a técnico");
    $("#framecanvas").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + id);  
}


function Descargapdf(id) {
    window.location.href = "/SalidaMaterial/Detalle?pdf=1&s=" + id;
}


$('#buscar').click(function () {

    LlenaGrid(1);
});

LlenaGrid(1);