﻿var articulos = [];


$('#add').click(function () {
    articulos = [];
    $('#contenidoresumen').empty();
    $('#edita').modal('show');
    $('#ed_almacen').val(0);
    $('#ed_tiposalida').val(0);
    $('#ed_tecnico').val(0);
});


$('#ed_almacen').change(function () {
    articulos = [];
    ActualizaDetalle();
    $('#ed_tecnico').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');
    asignaTecnicos($('#ed_almacen').val());

});


function asignaTecnicos(idAlmacen) {

    var url = '/SalidaMaterialTecnico/Rel_Tecnico_almacen/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idAlmacen },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                $('#ed_tecnico').append('<option value=' + data[a].EntidadClave + '>' + data[a].Nombre + '</option>');
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
           
        }
    });

}


$('#agregar').click(function () {
    if ($('#ed_almacen').val() == null || $('#ed_almacen').val() == 0) {
        toastr.warning('Atención, para continuar, seleccione un almacén para realizar la salida');
        
    } else {
        $('#articulo').modal('show');
        $('#ed_clasificacionmaterial').val(0);
        $('#ed_tipomaterial').val(0);
        $('#ed_articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');
        $('#en_almacen').val(0);
        $('#ed_cantidad').val(0);
        $('#panelbusqueda_series').hide();
        $('#serie').hide();
        $('#guardaAccesorios').hide();
    }   
});



$('#ed_clasificacionmaterial').change(function () {
    var id = $('#ed_clasificacionmaterial').val();
    $('#ed_tipomaterial').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');;
     var url = '/SalidaMaterialTecnico/GetTipoArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                $('#ed_tipomaterial').append('<option  value=' + data[a].catTipoArticuloClave + '>' + data[a].Descripcion + '</option>');
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');

        }
    });
});

$('#ed_tipomaterial').change(function () {
    $('#guardaAccesorios').hide();
    $('#en_almacen').val(0);
    $('#ed_articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');
    var id = $('#ed_tipomaterial').val();
    var url = '/SalidaMaterialTecnico/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                $('#ed_articulo').append('<option rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
           
        }
    });
});


function DetalleArticulo(valor) {
    Obtenerinventario($('#ed_articulo').val(), $('#ed_almacen').val());
    if (valor == "1") {
        //ActualizaTablaSeries();
        $('#serie').show();
        verTodosAparatos();
        $('#panelbusqueda_series').show();
    }
    else {
        $('#serie').hide();
        $('#PanelSeleccionSeries').hide();
        $('#guardaAccesorios').show();
        $('#panelbusqueda_series').hide();
    }
    
}

function Obtenerinventario(idarticulo, almacen) {   
    var url = '/SalidaMaterialTecnico/getInventario/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idarticulo, 'almacen': almacen },
        type: "POST",
        success: function (data) {
            if (data.tieneseries == true) {
                $('#en_almacen').val(data.Cantidad);               
            } else {
                $('#InventarioClave').val(data.InventarioClave);                
                $('#en_almacen').val(data.Cantidad);
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
            
        }
    });

}


function ActualizaTablaSeries() {
    actualizaContador();
    $('#contenidoT').empty();
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == $('#ed_articulo').val(); });
    for (var a = 0; a < result.length; a++) {
        $('#contenidoT').append("<tr><td><button class='btn btn-info btn-xs'>" + result[a].Nombre + "</button></td><td><button class='btn btn-default btn-xs'>" + result[a].Valor + "</button></td><td><button class='btn btn-default btn-xs'>" + result[a].Statustxt + "</button></td><td><button class='btn btn-danger btn-xs eliminaserie' rel='" + result[a].InventarioClave + "'  >Eliminar</button></td></tr>")
    }

}

function EliminarDeArreglo(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}


function Valida_Serie_arreglo(idinventario) {
    var result = $.grep(articulos, function (e) { return e.InventarioClave == idinventario });
    if (result.length == 0) {
        return "0";
    } else {
        return "1";
    }
}


function actualizaContador() {
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == $('#ed_articulo').val() });
    $('#ed_cantidad').val(result.length);
}


function ActualizaDetalle() {
    $('#contenidoresumen').html('');
    var linq = Enumerable.From(articulos);
    var result =
        linq.GroupBy(function (x) { return x.Nombre; })
            .Select(function (x) {
                return { Nombre: x.Key(), Clave: x.source[0].ArticuloClave, Cantidad: x.Sum(function (y) { return y.Cantidad | 0; }) };
            })
            .ToArray();

    if (result.length == 0) {
        $('#tablaresumen').hide();
        $('#mDetalle').show();
        
    } else {
        $('#tablaresumen').show();
        $('#mDetalle').hide();
        for (var a = 0; a < result.length; a++) {

            $('#contenidoresumen').append('<tr><td>' + result[a].Nombre + '</td><td>' + result[a].Cantidad + '</td><td><button class="btn btn-danger btn-xs" onClick="EliminaArticulo(\'' + result[a].Clave + '\')" rel=' + result[a].Clave + '><i class="fa fa-trash-o" aria-hidden="true"></i>Elimina</button><button class="btn btn-warning btn-xs" onClick="EditaArticulo(\'' + result[a].Clave + '\',\'' + result[a].Cantidad + '\')" rel=' + result[a].Clave + '><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button></td></tr>');
        }
    }
    


}


function existe_en_arreglo(idarticulo) {
    var result = $.grep(articulos, function (obj) { return obj.ArticuloClave == idarticulo; });
    if (result.length == 0) {
        return false;
    } else {
        return true;
    }
}


$('#serie').bind("enterKey", function (e) {

    if ($('#serie').val() == "") {
        $('#serie').val('');
        toastr.error("Error", "Necesita ingresar una serie válida");
    } else {       
            validaserie($('#serie').val(), $('#ed_articulo').val(), $('#ed_almacen').val());
            $('#serie').val('');             
    }
});


$('#serie').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});


function validaserie(serie, articulo, almacen) {
    var url = '/DevolucionAlmacenCentral/ValidaSerie/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie, 'articulo': articulo, 'almacen': almacen },
        type: "POST",
        success: function (data) {

            if (data == "0") {
                toastr.warning("Atención, la serie no existe en el inventario, no pertenece al artículo seleccionado o no esta disponible en este almacén");
              
            } else {
                var Serie = {};
                Serie.InventarioClave = data.InventarioClave;
                Serie.catSerieClave = data.catSerieClave;
                Serie.Valor = data.Valor;
                Serie.ArticuloClave = data.ArticuloClave;
                Serie.Nombre = data.Nombre;
                Serie.Cantidad = 1;
                Serie.status = $('#statusaparato').val();
                Serie.Statustxt = $("#statusaparato option:selected").text();

                if (Valida_Serie_arreglo(data.InventarioClave) == "1") {
                    toastr.warning("Atención, La serie ya fue agregada a la orden");
                  

                } else {
                    articulos.push(Serie);
                    actualizaContador();
                    ActualizaTablaSeries();
                    ActualizaDetalle();
                    verTodosAparatos();
                }
            }
        },
        error: function (request, error) {

            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
            
        }
    });

}



$('#guardaAccesorios').click(function () {
    GuardaAccesorios();
});

function GuardaAccesorios() {

    if ($('#ed_cantidad').val() <= 0) {

        toastr.warning("Atención, Necesita colocar una cantidad mayor a 0 para realizar la devolución");

       
    }
    else if ($('#ed_cantidad').val() % 1 != 0) {
        toastr.warning("Atención, Necesita colocar un número entero");
     
    }
    else if ($('#en_almacen').val() == 0) {
        toastr.warning("Atención, Este almacén no cuenta con piezas de este artículo para realizar la devolución");
        
    }
    else if (parseInt($('#en_almacen').val()) < parseInt($('#ed_cantidad').val())) {
        toastr.warning("Atención, La cantidad solicitada supera la cantidad en almacén");
        
    }

    else {
        if (existe_en_arreglo($('#ed_articulo').val()) == true) {
            EditaArreglo($('#ed_articulo').val(), $('#ed_cantidad').val());
            ActualizaDetalle();
            $('#articulo').modal('hide');
        }else{
            var Serie = {};
            Serie.InventarioClave = $("#InventarioClave").val();
            Serie.catSerieClave = "";
            Serie.Valor = "";
            Serie.ArticuloClave = $('#ed_articulo').val();
            Serie.Nombre = $("#ed_articulo option:selected").text();
            Serie.Cantidad = $('#ed_cantidad').val();
            Serie.status = 7;
            Serie.Statustxt = "";
            articulos.push(Serie);
            ActualizaDetalle();
            $('#articulo').modal('hide');
        }        
    }
};


function EliminaArticulo(idarticulo) {
    EliminarDeArreglo(articulos, 'ArticuloClave', idarticulo);
    ActualizaDetalle();
}


function EditaArreglo(idArticulo,cantidad) {
    for (var i in articulos) {
        if (articulos[i].ArticuloClave == idArticulo) {
            articulos[i].Cantidad = cantidad;            
        }
    }
}




