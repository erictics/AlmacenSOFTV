﻿

function verTodosAparatos() {
    $('#datos2').dataTable({
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": "/SalidaMaterialTecnico/Aparatos/",
            "type": "POST",
            "data": {
                'articulo': $('#ed_articulo').val(),
                'plaza': $('#ed_almacen').val(),
                'Articulos': JSON.stringify(articulos)
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         if (full.seleccionado == 1) {
                             return '<button class="btn btn-info btn-xs .selecciona" style=" width: 25px !important; height: 25px !important;padding:2px;"  onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';

                         } else {
                             return '<button class="btn btn-defaul btn-xs .selecciona" style="color:#FFFFFF;"   onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';
                         }

                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         return "<button class='btn btn-default btn-xs'>" + full.valor + "</button>"
                     }
                 },
                  {

                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.estatus == 7) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' selected>Buen Estado</option> <option value='9'>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }
                              else {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' selected>Buen Estado</option> <option value='9'>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }

                          }
                          else if (full.estatus == 9) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' selected>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }
                              else {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' selected>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }


                          }
                          else if (full.estatus == 10) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' selected>Dañado</option><option value='11'>Baja</option></select>";
                              }
                              else {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' selected>Dañado</option><option value='11'>Baja</option></select>";
                              }

                          }
                          else if (full.estatus == 11) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' >Dañado</option><option value='11' selected>Baja</option></select>";
                              }
                              else {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' >Dañado</option><option value='11' selected>Baja</option></select>";
                              }

                          }

                      }
                  },

        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Aparatos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",

            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

};



function activa(ArticuloClave, InventarioClave, Valor, Nombre, elem) {


    $(elem).css('width', '25px !important');
    $(elem).css('height', '25px !important;');
    $(elem).css('padding', '2px;');

    var Serie = {};
    Serie.InventarioClave = InventarioClave;
    Serie.catSerieClave = 2;
    Serie.Valor = Valor;
    Serie.ArticuloClave = ArticuloClave;
    Serie.Nombre = Nombre;
    Serie.Cantidad = 1;
    Serie.status = $("#" + InventarioClave + "").val();
    Serie.Statustxt = $("#" + InventarioClave + " option:selected").text();


    if (Valida_Serie_arreglo(InventarioClave) == "1") {
        EliminarDeArreglo(articulos, 'InventarioClave', InventarioClave);

        verTodosAparatos();
        actualizaContador();
        ActualizaDetalle();

    }
    else {
        document.getElementById(InventarioClave).setAttribute("disabled", "disabled");

        articulos.push(Serie);

        verTodosAparatos();
        actualizaContador();
        ActualizaDetalle();
    }
}