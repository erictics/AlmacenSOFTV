﻿
var Tecnicos = new Array();


llenaLista($('#distribuidor').val());

function llenaLista(iddistribuidor) {

    var objRoles = {};
    objRoles.dis = iddistribuidor;
    var myJSONText = JSON.stringify(objRoles); 

    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5,10,15], [5,10,15]],
        "ajax": {
            "url": "/InventarioTecnico/ObtenTecnicos",
            "type": "POST",
            "data": { 'distribuidor': iddistribuidor },
        },      

        "columns": [
             {
                 sortable: false,
                 "render": function (data, type, full, meta) {
                     
                     if (existeTecnico(full.IdEntidad)) {
                         return '<label ><input onchange="Activa(\'' + full.IdEntidad + '\')"  type="checkbox" checked value="' + full.IdEntidad + '"><span></span></label>';
                     } else {
                         return '<label><input onchange="Activa(\'' + full.IdEntidad + '\')"  type="checkbox"  value="' + full.IdEntidad + '"><span></span></label>';
                     }
                     
                 }
             },
               { "data": "Nombre", "orderable": false },
                              
        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

function Activa(id) {
   
    if (existeTecnico(id)) {       
        removeItem(id);        
    } else {        
        Tecnicos.push(id);       
    }
}

function existeTecnico(id) {
    for (var i = 0; i < Tecnicos.length; i++) {
        if (Tecnicos[i] == id) {
            return true;
        }
    }
    return false;
}

function removeItem(item) {
    var i = Tecnicos.indexOf(item);
    if (i !== -1) {
        Tecnicos.splice(i, 1);
    }
}

$('#pdf').click(function () {
    if (Tecnicos.length > 0) {
        window.location.href = "/SaldoTecnico/Detalle?pdf=2&tecnico=" + Tecnicos.toString();
    } else {
        toastr.warning('Seleccione por lo menos un técnico para generar el reporte');
    }
});

$('#distribuidor').change(function () {
    llenaLista($('#distribuidor').val());

})


$('#excel').click(function () {
    if (Tecnicos.length > 0) {
        window.location.href = "/SaldoTecnico/prueba?tecnico=" + Tecnicos.toString();
    } else {
        toastr.warning('Seleccione por lo menos un técnico para generar el reporte');
    }
    
});
