﻿
var idEdiccion = null;
function Edita(id) {
    articulos = [];
    idEdiccion = id;
    DetalleDevolucion(id);
    $('#guardaDevolucion').hide();
    $('#EditaDevolucion').show();
}

function DetalleDevolucion(id) {

    $('#AgregaDevolucion').modal('show');
    var url = '/DevolucionAlmacenCentral/DetalleDevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'iddevolucion': id },
        type: "POST",
        success: function (data) {

            $('#almacen').val(data.almacen);
            $('#motivo').val(data.motivo);

            for (var a = 0; a < data.articulos.length; a++) {
                var Serie = {};
                Serie.InventarioClave = data.articulos[a].InventarioClave;
                Serie.catSerieClave = 2;
                Serie.Valor = data.articulos[a].Valor;
                Serie.ArticuloClave = data.articulos[a].ArticuloClave;
                Serie.Nombre = data.articulos[a].Nombre;
                Serie.Cantidad = data.articulos[a].Cantidad;
                Serie.status = data.articulos[a].status;
                Serie.Statustxt = "";
                articulos.push(Serie);
                ActualizaDetalle();
            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });



}


function EditaArticulo(idArticulo, cantidad) {
    $('#AgregaArticulo').modal('show');

    var url = '/SalidaMaterialTecnico/DetalleArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idarticulo': idArticulo },
        type: "POST",
        success: function (data) {

            $('#clasificacion').val(data.idClasificacion);
            obtentipomat(data.idClasificacion, data.idTipoArticulo);
            var valor = AsignaArticulo(data.idArticulo, data.idTipoArticulo);

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

}

function AsignaArticulo(idArticulo, idTipoArticulo) {

    var a = "";
    $('#articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');

    var url = '/SalidaMaterialTecnico/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idTipoArticulo },
        type: "POST",
        success: function (data) {

            for (var a = 0; a < data.length; a++) {
                if (data[a].ArticuloClave == idArticulo) {

                    $('#articulo').append('<option selected rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');

                    var b = (data[a].catSerieClaveInterface == null ? "0" : "1");
                    DetalleArticulo(b);
                } else {

                    $('#articulo').append('<option rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

    return a;

}



