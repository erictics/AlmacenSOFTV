﻿function RecibeGarantia() {
    //var cuantosfaltan = 0;
    //articulos.forEach(function (item) {
    //    if (item.Selected == false) {
    //        cuantosfaltan = cuantosfaltan + 1;
    //    }
    //});

    //if (cuantosfaltan > 0) {
    //    toastr.warning("Error, hay articulos que no se han marcado como recibidos, solo se puede realizar una receoción completa");
    //    return;
    //}

    var url = '/Garantia/Recepcion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'devolucion': id_recepcion, 'articulos': JSON.stringify(articulos) },
        type: "POST",
        success: function (data) {
            if (data == "-1") {
               toastr.warning("Error, la sessión expiro ,vuelva a autenticarse por favor (1)");
            }
            else if (data == "-2") {
               toastr.warning("Error, El articulo no se encuentra con un estatus de reparación/garantia (2)");
            }
            else if (data == "-3") {
                toastr.warning("Error, El sistema evitó que se generará el proceso, Alguna de la series de reemplazo en la recepcion se encuentra en registrada en el inventario");
            }
            else if (data == "-4") {
                toastr.warning("Error, El sistema evitó que se generará el proceso, La cantidad a recibir es mayor a la cantidad en el inventario con estatus de reparación y garantias");
            }
            else if (data == "-5") {
                toastr.warning("Error, El sistema evitó que se generará el proceso, Se encontró un error al registrar la orden ");
            }
            else {
               $('#repo').click();
               $('#titulocanvas').empty().append("Detalle Recepcion de proveedor por garantía");
               $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);
               toastr.success("Correcto, la devolución se ha generado correctamente");
               $("#RecibeDevolucion").modal("hide");
               ObtenerLista(0, 0);
           }
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });


}
