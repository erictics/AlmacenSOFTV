﻿$('#guardaDevolucion').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Atención, No puede realizar una devolución sin artículos");
    }

    else if (comment.length == 0) {
        toastr.warning("Atención, No puede realizar una devolución sin especificar el motivo de esta.");
    }
    else {

        var url = '/Garantia/GuardaDevolucion/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'almacen': $('#almacen').val(), 'motivo': $('#motivo').val(), 'articulos': JSON.stringify(articulos), 'proveedor': $('#proveedor').val(),'status':$('#status').val() },
            type: "POST",
            success: function (data) {
                if (data == "-1") {
                    toastr.warning("Error, Alguno de los aparatos no se encuentra en el almacén o ya fue recepcionado por el almacén principal \n contacta al administrador para mayor información");
                }
                else if (data == "-2") {
                    toastr.warning("Error, La cantidad solicitada excede el saldo en el inventario");
                }
                else if (data == "0") {
                    toastr.warning("Error, El sistema evitó que se generará el proceso, Contacta al administrador del sistema para mayor información ");
                }
                else {
                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle Devolución de material a proveedor");
                    $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + data);
                    toastr.success("Correcto, la devolución se ha generado correctamente");
                    $("#AgregaDevolucion").modal("hide");
                    ObtenerLista(0, 0);
                }
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});