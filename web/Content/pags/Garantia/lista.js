﻿
var id_recepcion = 0;
function ObtenerLista(almacen, orden) {
    var Numero=(orden == "" || orden == undefined)?0:orden;    
    var Alm=(almacen == "" || almacen == undefined)?0:almacen    
    var data = { 'data': "1", 'almacen': Alm, 'orden': Numero }
    var url = "/Garantia/ListaDevoluciones/";
    var columnas = [
                 { "data": "pedido", "orderable": false },
                 { "data": "fechacorta", "orderable": false },
                 { "data": "txtestatus", "orderable": false },
                 { "data": "almacen", "orderable": false },
				 { "data": "proveedor", "orderable": false },
                 {
                        sortable: false,
                        "render": function (data, type, full, meta) {
                            return '<button class="btn btn-primary btn-xs"  onclick="verpdf(\'' + full.proceso + '\');"> <i class="fa fa-search"></i> ver orden</button>';                          
                           
                        }
                    },
                     {
                         sortable: false,
                         "render": function (data, type, full, meta) {
                             var str="";
                             if (full.tiene_envio == 1) {
                                 str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger" onclick="DetalleEnvio(\'' + full.proceso + '\');"><i class="fa fa-envelope"></i></buttton>';
                             } else {
                                 str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-default-bright" onclick="Envio(\'' + full.proceso + '\');"><i class="fa fa-envelope-o"></i></buttton>';
                             }

                             if (full.estatus == 4 || full.estatus == 6) {
                                 str += '<button class="btn btn-success btn-xs" id="recibir" disabled onclick="Recibir(\'' + full.proceso + '\');"> Recibir</button>';
                                 if (full.estatus == 6) {
                                      str += '<button class="btn btn-success btn-xs" disabled onclick="verpdf(\'' + full.recepcion_proveedor + '\');"> ver recepción</button>';
                                 } else {
                                     str += '<button class="btn btn-success btn-xs"  onclick="verpdf(\'' + full.recepcion_proveedor + '\');"> ver recepción</button>';
                                 }
                                
                                 str += '<button class="btn btn-danger btn-xs" disabled  id="cancelar"  onclick="cancelar(\'' + full.proceso + '\');"><i class="fa fa-trash-o" aria-hidden="true"></i> cancelar</button>';
                             } else {
                                 str += '<button class="btn btn-success btn-xs" id="recibir"  onclick="Recibir(\'' + full.proceso + '\');"> Recibir</button>';
                                  str += '<button class="btn btn-success btn-xs" disabled  onclick="verpdf(\'' + full.proceso + '\');"> ver recepción</button>';
                                  str += '<button class="btn btn-danger btn-xs"  id="cancelar"  onclick="cancelar(\'' + full.proceso + '\');"><i class="fa fa-trash-o" aria-hidden="true"></i> cancelar</button>';
                             }
                             return str;


                         }
                     },


                 ];
    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": url,
            "type": "POST",
            "data": data
        },


        "columns": columnas,

        language:  {

        search: "Buscar&nbsp;:",
        lengthMenu: "Mostrar _MENU_ orden",
        info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
        infoEmpty: "No hay aparatos para mostrar",
        infoFiltered: "(filtrados _MAX_ )",
        infoPostFix: "",
        loadingRecords: "Búsqueda en curso...",
        zeroRecords: "No hay aparatps para mostrar",
        emptyTable: "No hay aparatos disponibles",
        paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
        },
            aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción proveedor por garantía/reparación de material");
    $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + devolucion);  
};

function Recibir(devolucion) {
    id_recepcion = devolucion;
    articulos = [];
    $('#tablaRecepcionSerie tbody').empty();
    $('#titulorecser').empty();
    $('#panelrecep').hide();
    DetalleDevolucion_Recepcion(devolucion);

}

var idcancela;
function cancelar(id) {
    idcancela = id;
    $('#CancelaDev').modal('show');
}

function filtrarProveedor() {
   
    ObtenerLista($('#proveedorfiltro').val(), 0);
}

function filtrarOrden() {    
    ObtenerLista('', $('#ord').val());
}

function cancelarDevolucion() {
    var url = '/Garantia/CancelaDevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'devolucion': idcancela, },
        type: "POST",
        success: function (data) {
            if (data == "-1") {
                toastr.warning("Error,No se pudo cancelar la devolución ");
            }
            
            else {
                $('#repo').click();
                $('#CancelaDev').modal('hide');
                $('#titulocanvas').empty().append("Detalle orden");
                $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + data);
                toastr.success("Correcto, la devolución se ha cancelado correctamente");                
                ObtenerLista(0, 0);
            }
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}


ObtenerLista();