﻿//tipo articulo


$(document).ready(function () {
    llenaLista();


    $('input[type="checkbox"]:not(".switch,.AllSel")').on('ifChecked', function (event) {
        RevisaSeriales();
    });

    $('input[type="checkbox"]:not(".switch,.AllSel")').on('ifUnchecked', function (event) {
        RevisaSeriales();
    });

});

function llenaLista() {


    var objRoles = {};
    objRoles.pag = 1;
    objRoles.cla = $("#cla").val();

    var myJSONText = JSON.stringify(objRoles);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/TipoArticulo/ObtenLista/",
            "type": "POST",
            "data": {
                'data': myJSONText
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         if (full.Activo == 1) {
                             return full.Descripcion;
                         }
                         else {
                             return  "<s>" + full.Descripcion + "</s>";
                         }
                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         if (full.EsPagare == 1) {
                             return "Pagare";
                         }
                         else {
                            return "Para venta";
                         }


                     }

                 },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {
                         
                          if (full.Activo == 1) {
                             
                              return "<img src='/Content/assets/img/activo.png'>";
                          }
                          else {
                              
                              return "<img src='/Content/assets/img/inactivo.png'>";
                          }
                      }

                  },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          return '<button type="button" class="btn btn-warning btn-xs edit"  onClick="edita(\'' + full.Clave + '\')" >&nbsp;Editar</button>';
                      }

                  },
                  


        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

