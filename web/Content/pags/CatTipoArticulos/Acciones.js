﻿


$("#cla").change(function () {
    $("#ed_clasificacion").val($("#cla").val());
    llenaLista();
});









$("#add").click(function () {
    $("#titulowin").html("Agregar Tipo de Artículo");
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');
    $("#ed_letra").val('');
    $("[type=checkbox]").removeProp('checked');
    $("[type=checkbox]").parent().removeClass('checked');

    $("[type=radio]").removeProp('checked');
    $("[type=radio]").parent().removeClass('checked');
    $("#panelInterfaz").removeClass("disabledbutton");

});



$("#formaedita").submit(function (e) {
    e.preventDefault();

    var haycheckserie = false;
    $(".serie").each(function () {
        if ($(this).is(':checked')) {
            haycheckserie = true;
        }
    });

    var haycheckserieinterface = false;
    $(".serieinterface").each(function () {
        if ($(this).is(':checked')) {
            haycheckserieinterface = true;
        }
    });

    RevisaSeriales();


    if (haycheckserie && !haycheckserieinterface) {
        toastr.error('Error', 'Debe de existir una serie de interface');
        return;
    }

    var s = $(".serieinterface:checked").val();
    if (s != undefined && !$("#serie" + s).is(':checked')) {
        toastr.error('La serie de Interface debe de estar marcada como serie utilizada');
        return;
    }

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        

        var url = '/TipoArticulo/Guarda/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaedita').serialize(),
            type: "POST",
            success: function (data) {
                if (data == "0") {
                    toastr.error('Ocurrio un error al guardar el registro');

                }
                else if (data == "1") {
                    $("#edita").modal('hide');
                    toastr.success('El tipo de artículo se guardó correctamente');

                    llenaLista();

                }
                else if (data == "2") {

                    toastr.error('El tipo de artículo no se puede editar ya que hay artículos registrados con esta categoria en el sistema');
                    
                }

              
               
            },
            error: function (request, error) {
                toastr.info('Surgió un error, intente nuevamente más tarde');
                
            }
        });

    }

});



function RevisaSeriales() {
    var cuantos = 0;
    $(".serie").each(function () {
        if ($(this).is(":checked")) {
            cuantos++;
        }
    });

    if (cuantos == 0) {
        $(".serieinterface").prop("checked", false);
        $(".serieinterface").parent().removeClass("checked");
        $("#ed_letra").val('');
    }
}


function edita(cual) {

    $("#titulowin").html("Edita Tipo de Artículo");
    $('#edita').modal('show');

    var url = '/TipoArticulo/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {
           // $("#panelInterfaz").addClass("disabledbutton");            
            var oRol = eval(data);

            $("#ed_id").val(oRol.Clave);
            $("#ed_nombre").val(oRol.Descripcion);

            $("[type=checkbox]").removeProp('checked');
            $("[type=checkbox]").parent().removeClass('checked');

            for (var i = 0; i < oRol.Series.length; i++) {
                $("#serie" + oRol.Series[i]).prop('checked', 'checked');
                $("#serie" + oRol.Series[i]).parent().addClass('checked');
            }
            //LlenaGrid(_pag);
            $("#ed_activa").val(oRol.Activo);
            if (oRol.Activo == "True" || oRol.Activo == "1") {
                $("#ed_activa").prop('checked', 'checked');
                $("#ed_activa").parent().addClass('checked');
            }
            else {
                $("#ed_activa").removeProp('checked');
                $("#ed_activa").parent().removeClass('checked');
            }

            //$("#ed_paraventa").val(oRol.NoVenta);
            if (oRol.EsPagare == "True" || oRol.EsPagare == "1") {
                $("#ed_pagare").prop('checked', 'checked');
                $("#ed_pagare").parent().addClass('checked');
            }
            else {
                $("#ed_pagare").removeProp('checked');
                $("#ed_pagare").parent().removeClass('checked');
            }

            $("#ed_letra").val(oRol.Letra);

            $(".serieinterface").each(function () {
                if ($(this).val() == oRol.SerieInterface) {
                    $(this).prop('checked', 'checked');
                    $(this).parent().addClass('checked');
                }
                else {
                    $(this).removeProp('checked');
                    $(this).parent().removeClass('checked');
                }
            });

            //$("#ed_devmatriz").val(oRol.DevMatriz);
            if (oRol.DevMatriz == "True" || oRol.DevMatriz == "1") {
                $("#ed_devolucionmatriz").prop('checked', 'checked');
                $("#ed_devolucionmatriz").parent().addClass('checked');
            }
            else {
                $("#ed_devolucionmatriz").removeProp('checked');
                $("#ed_devolucionmatriz").parent().removeClass('checked');
            }


        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}


