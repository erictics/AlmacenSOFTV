﻿var articulos = [];

var idreemplazo = "";
var idinvreemplazo = "";
var artinvreemplazo = "";
var reemplazonombreart = "";


function Obtenerinventario(idarticulo, almacen, status) {
    $('#cantidad').val(0);
    $('#cantidad_anterior').val(0);   
    $.ajax({
        url: '/Devolucion/getInventario/',
        dataType: 'json',
        data: { 'id': idarticulo, 'almacen': almacen, 'status': status },
        type: "POST",
        success: function (data) {
            if (data.tieneseries == true) {
                $('#enalmacen').val(data.Cantidad);
                $('#InventarioClave').val(data.InventarioClave);
                $('#cantidad_anterior').hide();
                $('#colcantant').hide();
            } else {
                if (idEdiccion != null) {
                    $('#label-cantidad').empty().append('Cantidad nueva');
                    $('#InventarioClave').val(data.InventarioClave);
                    $('#enalmacen').val(data.Cantidad);
                    cuantosdevolucion(idEdiccion, $('#articulo').val());
                    $('#cantidad_anterior').show();
                    $('#colcantant').show();

                } else {
                    $('#label-cantidad').empty().append('Cantidad');
                    $('#InventarioClave').val(data.InventarioClave);
                    $('#enalmacen').val(data.Cantidad);
                    $('#cantidad_anterior').hide();
                    $('#colcantant').hide();
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function cantidadagregada(idarticulo) {
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == idarticulo });
    var cantidad = 0;
    if (result.length == 0) {
        return 0;
    } else {
        for (var a = 0; a < result.length; a++) {
            cantidad = parseInt(cantidad) + parseInt(result[a].Cantidad);
        }
        return cantidad;
    }
}

function existe_en_arreglo(idarticulo) {
    var result = $.grep(articulos, function (obj) { return obj.ArticuloClave == idarticulo; });
    if (result.length == 0) {
        return false;
    } else {
        return true;
    }
}

function EditaArreglo(idArticulo, cantidad) {
    for (var i in articulos) {
        if (articulos[i].ArticuloClave == idArticulo) {
            articulos[i].Cantidad = cantidad;
        }
    }
}

function ActualizaDetalle() {
    $('#contenidoresumen').html('');
    var linq = Enumerable.From(articulos);
    var result =
        linq.GroupBy(function (x) { return x.Nombre; })
            .Select(function (x) {
                return { Nombre: x.Key(), Clave: x.source[0].ArticuloClave, Cantidad: x.Sum(function (y) { return y.Cantidad | 0; }) };
            })
            .ToArray();
    if (result.length == 0) {
        $('#tablaresumen').hide();
        $('#msn-detalle').show();
    } else {
        $('#tablaresumen').show();
        $('#msn-detalle').hide();
        for (var a = 0; a < result.length; a++) {
            $('#contenidoresumen').append('<tr><td>' + result[a].Nombre + '</td><td>' + result[a].Cantidad + '</td><td><button class="btn btn-primary btn-xs" onClick="EditaArticulo(\'' + result[a].Clave + '\',\'' + result[a].Cantidad + '\')" rel=' + result[a].Clave + '><i class="fa fa-pencil" aria-hidden="true"></i>Edita</button><button class="btn btn-danger btn-xs" onClick="EliminaArticulo(\'' + result[a].Clave + '\')" rel=' + result[a].Clave + '><i class="fa fa-trash" aria-hidden="true"></i>Elimina</button></td></tr>');
        }
    }
}

function ActualizaDetalle_recepcion() {
    $('#tablaresumenrec').hide();
    $('#msn-detallerec').hide();
    $('#contenidoresumenrec').empty();
    var linq = Enumerable.From(articulos);
    var result =
        linq.GroupBy(function (x) { return x.Nombre; })
            .Select(function (x) {
                return { Nombre: x.Key(), Clave: x.source[0].ArticuloClave, Cantidad: x.Sum(function (y) { return y.Cantidad | 0; }) };
            })
            .ToArray();
   
    if (result.length == 0) {
        $('#tablaresumenrec').hide();
        $('#msn-detallerec').show();
    } else {
      
        $('#tablaresumenrec').show();
        $('#msn-detallerec').hide();
        for (var a = 0; a < result.length; a++) {           
            $('#contenidoresumenrec').append('<tr><td>' + result[a].Nombre + '</td><td>' + result[a].Cantidad + '</td><td><button class="btn btn-warning btn-xs" onClick="RecibeArticulo(\'' + result[a].Clave + '\',\'' + result[a].Cantidad + '\',\'' + result[a].Nombre + '\')" rel=' + result[a].Clave + '><i class="md md-inbox" aria-hidden="true"></i> Recibir</button></td></tr>');
        }
    }
}



function actualizaContador() {
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == $('#articulo').val() });
    $('#cantidad').val(result.length);
}

function Valida_Serie_arreglo(idinventario) {
    var result = $.grep(articulos, function (e) { return e.InventarioClave == idinventario });
    if (result.length == 0) {
        return "0";
    } else {
        return "1";
    }
}

function activa(ArticuloClave, InventarioClave, Valor, Nombre, elem) {
    $(elem).css('width', '25px !important');
    $(elem).css('height', '25px !important;');
    $(elem).css('padding', '2px;');
    var Serie = {};
    Serie.InventarioClave = InventarioClave;
    Serie.catSerieClave = 2;
    Serie.Valor = Valor;
    Serie.ArticuloClave = ArticuloClave;
    Serie.Nombre = Nombre;
    Serie.Cantidad = 1;
    Serie.status = $('#status').val();
    Serie.Statustxt ='';

    if (Valida_Serie_arreglo(InventarioClave) == "1") {
        EliminarDeArreglo(articulos, 'InventarioClave', InventarioClave);
        ActualizaTablaSeries();
        verTodosAparatos($('#articulo').val(), $('#almacen').val(), articulos, $('#status').val());
        actualizaContador();
        ActualizaDetalle();
    }
    else {
        //document.getElementById(InventarioClave).setAttribute("disabled", "disabled");
        articulos.push(Serie);
        ActualizaTablaSeries();
        verTodosAparatos($('#articulo').val(), $('#almacen').val(), articulos, $('#status').val());
        actualizaContador();
        ActualizaDetalle();
    }
}


function verTodosAparatos(articulo,almacen,articulos_array,status) {
    var iddev = 0;
    if (idEdiccion != null) {
        iddev = idEdiccion;
    }
    $('#datos').dataTable({
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": "/Devolucion/Aparatos/",
            "type": "POST",
            "data": {
                'articulo': articulo,
                'plaza': almacen,
                'Articulos': JSON.stringify(articulos_array),
                'idDev': iddev,
                'status': status
            }
        },     
        "columns": [
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         if (full.seleccionado == 1) {
                             return '<button class="btn btn-info btn-xs .selecciona" style=" width: 25px !important; height: 25px !important;padding:2px;"  onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';

                         } else {
                             return '<button class="btn btn-defaul btn-xs .selecciona" style="color:#FFFFFF;"   onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';
                         }

                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         return "<button class='btn btn-default btn-xs'>" + full.valor + "</button>"
                     }
                 },
                  { "data": "txtstatus", "orderable": false }],

        language: lenguaje,


        "order": [[0, "asc"]]
    })

};

function ActualizaTablaSeries() {
    actualizaContador();
    $('#contenidoT').empty();
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == $('#articulo').val(); });
    for (var a = 0; a < result.length; a++) {
        $('#contenidoT').append("<tr><td><button class='btn btn-info btn-xs'>" + result[a].Nombre + "</button></td><td><button class='btn btn-default btn-xs'>" + result[a].Valor + "</button></td><td><button class='btn btn-default btn-xs'>" + result[a].Statustxt + "</button></td><td><button class='btn btn-danger btn-xs eliminaserie' rel='" + result[a].InventarioClave + "'  >Eliminar</button></td></tr>")
    }

}


function EliminarDeArreglo(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}


function EliminaArticulo(idarticulo) {
    EliminarDeArreglo(articulos, 'ArticuloClave', idarticulo);
    ActualizaDetalle();
}



function validaserie(serie, articulo, almacen, status) {
    var url = '/Devolucion/ValidaSerie/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie, 'articulo': articulo, 'almacen': almacen, 'status': status },
        type: "POST",
        success: function (data) {
            if (data == "0") {
                toastr.warning("Atención, la serie no existe en el inventario, no pertenece al artículo seleccionado o no esta disponible en este almacén");
            } else {
                var Serie = {};
                Serie.InventarioClave = data.InventarioClave;
                Serie.catSerieClave = data.catSerieClave;
                Serie.Valor = data.Valor;
                Serie.ArticuloClave = data.ArticuloClave;
                Serie.Nombre = data.Nombre;
                Serie.Cantidad = 1;
                Serie.status = $('#status').val();
                Serie.Statustxt = $("#statusaparato option:selected").text();
                if (Valida_Serie_arreglo(data.InventarioClave) == "1") {
                    toastr.warning("Error, La serie ya fue agregada a la orden");
                } else {
                    articulos.push(Serie);
                    actualizaContador();
                    ActualizaTablaSeries();
                    ActualizaDetalle();
                    verTodosAparatos($('#articulo').val(), $('#almacen').val(), articulos, $('#status').val());
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

}

$('#add').click(function () {
    articulos = [];
    idEdiccion = null;
    $('#AgregaDevolucion').modal('show');
    $('#motivo').val('');
    $('#contenidoresumen').empty();    
    $('#EditaDevolucion').hide();
    $('#guardaDevolucion').show();

});


$('#addArticulo').click(function () {
    resetear();
    $('#panelbusqueda_series').hide();
    $('#AgregaArticulo').modal('show');
    $('#cantidad_anterior').hide();
    $('#colcantant').hide();
});


$('#almacen').change(function () {
    articulos = [];
    idEdiccion = null;
    $('#contenidoresumen').empty();
    resetear();
});


$('#clasificacion').change(function () {

    $('#tipomat').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
    var url = '/Devolucion/GetTipoArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': $('#clasificacion').val() },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {

                $('#tipomat').append("<option value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

});


$('#tipomat').change(function () {
    $('#guardaAccesorios').hide();
    $('#panelbusqueda_series').hide();
    $('#articulo').empty().append('<option  value="x">Selecciona</option>');
    var url = '/Devolucion/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': $('#tipomat').val(), almacen: $('#almacen').val() },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                $('#articulo').append("<option value=" + data[a].ArticuloClave + " rel=" + (data[a].catSerieClaveInterface == null ? "0" : "1") + ">" + data[a].Nombre + "</option>");

            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});




$('#serie').bind("enterKey", function (e) {

    if ($('#serie').val() == "") {
        $('#serie').val('');
        toastr.warning("Error, Necesita ingresar una serie válida");
    } else {
        
            validaserie($('#serie').val(), $('#articulo').val(), $('#almacen').val(), $('#status').val());
            $('#serie').val('');

        
        
    }
});

$('#serie').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});

$('#guardaAccesorios').click(function () {

    GuardaAccesorios();
});

function obtentipomat(idclas, id) {

    $('#tipomat').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
    var url = '/Devolucion/GetTipoArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idclas },
        type: "POST",
        success: function (data) {
            for (var a = 0; a < data.length; a++) {
                if (id == data[a].catTipoArticuloClave) {
                    $('#tipomat').append("<option selected value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
                }
                else {
                    $('#tipomat').append("<option value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
                }

            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function DetalleArticulo(valor) {
    Obtenerinventario($('#articulo').val(), $('#almacen').val(), $('#status').val());
    if (valor == "1") {
        DetalleSeries($('#articulo').val(), $('#almacen').val())
        $('#guardaAccesorios').hide();
        ActualizaTablaSeries();
        verTodosAparatos($('#articulo').val(), $('#almacen').val(), articulos, $('#status').val());
        $('#panelbusqueda_series').show();
    }
    else {
        $('#PanelSeleccionSeries').hide();
        $('#guardaAccesorios').show();
        $('#panelbusqueda_series').hide();
    }
}

function CambioStatus() {
    articulos = [];
    $('#articulo').val("x");
}

function DetalleSeries(id, almacen) {
    $('#PanelSeleccionSeries').show();
}

function resetear() {
    $('#PanelSeleccionSeries').hide();
    $('#enalmacen').val('0');
    $('#cantidad').val('0');
    $('#serie').val('');
    $('#articulo').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
    $('#tipomat').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
    $('#guardaAccesorios').hide();
    $('#clasificacion').val(0);

}

function GuardaAccesorios() {
    if ($('#cantidad').val() <= 0) {
        toastr.warning("Error, necesita colocar una cantidad mayor a 0 para realizar la devolución");
    }
    else if ($('#cantidad').val() % 1 != 0) {
        toastr.warning("Error, necesita colocar un número entero");
    }
    else if ($('#enalmacen').val() == 0) {
        toastr.warning("Error, este almacen no cuenta con piezas de este artículo para realizar la devolución");
    }
    else if (idEdiccion == null && parseInt($('#enalmacen').val()) < parseInt($('#cantidad').val())) {
        toastr.warning("Error, la cantidad solicitada supera la cantidad en almacén");
    }
    else if (idEdiccion != null && parseInt($('#cantidad_anterior').val()) + parseInt($('#enalmacen').val()) < parseInt($('#cantidad').val())) {
        toastr.warning("Error, la cantidad solicitada es mayor a la cantidad anterior y a la cantidad en almacén");
    }
    else {

        if (existe_en_arreglo($('#articulo').val()) == true) {
            EditaArreglo($('#articulo').val(), $('#cantidad').val());
            ActualizaDetalle();
            $('#AgregaArticulo').modal('hide');
        } else {
            var Serie = {};
            Serie.InventarioClave = $("#InventarioClave").val();
            Serie.catSerieClave = "";
            Serie.Valor = "";
            Serie.ArticuloClave = $('#articulo').val();
            Serie.Nombre = $("#articulo option:selected").text();
            Serie.Cantidad = $('#cantidad').val();
            Serie.status = $('#status').val();
            Serie.Statustxt = "";
            articulos.push(Serie);
            ActualizaDetalle();
            $('#AgregaArticulo').modal('hide');
        }
    }
};

function EditaArticulo(idArticulo, cantidad) {
    $('#AgregaArticulo').modal('show');   
    $.ajax({
        url: '/Devolucion/DetalleArticulo/',
        dataType: 'json',
        data: { 'idarticulo': idArticulo },
        type: "POST",
        success: function (data) {
            $('#clasificacion').val(data.idClasificacion);
            obtentipomat(data.idClasificacion, data.idTipoArticulo);
            var valor = AsignaArticulo(data.idArticulo, data.idTipoArticulo);
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

}

function DetalleDevolucion_Recepcion(id) {
    articulos = [];
    $('#RecibeDevolucion').modal('show');
    var url = '/Devolucion/DetalleDevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'iddevolucion': id },
        type: "POST",
        success: function (data) {
            
            $('#titulo_RecibeDevolucion').empty().append("Recepción de devolución #"+data.orden);
            for (var a = 0; a < data.articulos.length; a++) {
                var Serie = {};
                Serie.InventarioClave = data.articulos[a].InventarioClave;
                Serie.catSerieClave = 2;
                Serie.Valor = data.articulos[a].Valor;
                Serie.ArticuloClave = data.articulos[a].ArticuloClave;
                Serie.Nombre = data.articulos[a].Nombre;
                Serie.Cantidad = data.articulos[a].Cantidad;
                Serie.status = data.articulos[a].status;
                Serie.Statustxt = "";
                Serie.Reemplazo = "";
                Serie.Reemplazada = false;
                Serie.Selected = false;
                Serie.tieneserie = data.articulos[a].tieneserie;
                articulos.push(Serie);
                ActualizaDetalle_recepcion();
            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function DetalleDevolucion(id) {
    $('#AgregaDevolucion').modal('show');
    var url = '/Devolucion/DetalleDevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'iddevolucion': id },
        type: "POST",
        success: function (data) {
            
            $('#almacen').val(data.almacen);
            $('#motivo').val(data.motivo);

            for (var a = 0; a < data.articulos.length; a++) {
                var Serie = {};
                Serie.InventarioClave = data.articulos[a].InventarioClave;
                Serie.catSerieClave = 2;
                Serie.Valor = data.articulos[a].Valor;
                Serie.ArticuloClave = data.articulos[a].ArticuloClave;
                Serie.Nombre = data.articulos[a].Nombre;
                Serie.Cantidad = data.articulos[a].Cantidad;
                Serie.status = data.articulos[a].status;
                Serie.Statustxt = "";
                Serie.Selected = false;
                articulos.push(Serie);
                ActualizaDetalle();
            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function Envio(proceso) {
    $('#edProcesoEnvio').modal('show');
    $('#devEnv').val(proceso);
    $('#edPE_guia').val('');
    $('#edPE_transportista').val('');
    $('#edPE_fecha').val('');
}

function DetalleEnvio(proceso) {
    $('#edProcesoEnvio').modal('show');
    $('#devEnv').val(proceso);

    $('#edPE_guia').val('');
    $('#edPE_transportista').val('');
    $('#edPE_fecha').val('');


    var url = '/Devolucion/detalleEnvio/';
    $.ajax({
        url: url,
        data: { 'proceso': proceso },
        type: "POST",
        success: function (data) {

            $('#edPE_guia').val(data.guia);
            $('#edPE_transportista').val(data.tran);
            $('#edPE_fecha').val(data.fecha);

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
}

function RecibeArticulo(articuloclave, cantidad, nombre) {
    
    $('#titulorecser').empty().append("Selecciona las series a recibir del artículo "+nombre);
    $('#tablaRecepcionSerie').empty();
    $('#panelrecep').show();
    articulos.forEach(function (item) {
        console.log(item);
        if (item.ArticuloClave === articuloclave) {
            if (item.tieneserie == true) {
                $('#titulorecser').empty().append("Selecciona las series a recibir del artículo " + nombre);
            } else {
                $('#titulorecser').empty().append("Selecciona la cantidad  a recibir del artículo " + nombre);
            }
            var str = '';
            if (item.tieneserie == true) {
                 str += '<tr>';
                if (item.Selected == true) {
                    str += '<td><label class="checkbox-inline checkbox-styled checkbox-info"><input type="checkbox" Onchange="recibe(\'' + item.InventarioClave + '\',\'' + item.Selected + '\')" checked><span></span></label></td>'
                } else {
                    str += '<td><label class="checkbox-inline checkbox-styled checkbox-info"><input type="checkbox" Onchange="recibe(\'' + item.InventarioClave + '\',\'' + item.Selected + '\')"><span></span></label></td>'
                }
                
                if (item.Reemplazada == true) {
                    str += '<td><i style="color:red;" class="fa fa-minus" aria-hidden="true"></i> ' + item.Valor + '</td><td><button onclick="reemplazo(\'' + item.InventarioClave + '\',\'' + item.Valor + '\',\'' + item.Nombre + '\',\'' + item.ArticuloClave + '\')" class="btn  ink-reaction btn-xs btn-primary-bright">reemplazar</button></td>';
                    str += '<td  > <i style="color:green;" class="fa fa-plus" aria-hidden="true"></i> ' + item.Reemplazo + '</td>';
                } else {
                    str += '<td>' + item.Valor + '</td><td><button onclick="reemplazo(\'' + item.InventarioClave + '\',\'' + item.Valor + '\',\'' + item.Nombre + '\',\'' + item.ArticuloClave + '\')" class="btn  ink-reaction btn-xs btn-danger">Serie reemplazo</button></td>';
                    str += '<td  >' + item.Reemplazo + '</td>';
                }

                str += '</tr>';
               
            } else {

                str += '<tr><td>' + item.Nombre + '</td><td><input type="number" value=' + item.Cantidad + ' class="form-control input-sm" onkeyup="recibeAccesorios(\'' + item.ArticuloClave + '\',$(this).val())"  ></td><td></td></tr>';
            }
            $('#tablaRecepcionSerie').append(str);
        }
        
    });
}

function recibeAccesorios(id, value) {
    if (value == null || undefined || value<0 ) {
        value = 0;
        toastr.error('Error, La cantidad no debe ser mayor a 0');
    } else {
        EditaArreglo(id, value);
        //ActualizaDetalle_recepcion();
    }    
}

function reemplazo(id, valor, nombre,articulo) {
    idreemplazo = "";
    idinvreemplazo = "";
    $('#sreemplazonueva').val("");
    $('#modalReemplazo').modal('show');
    $('#TitulomodalReemplazo').empty().append("Captura equipo de reemplazo "+nombre);
    $('#sreemplazo').val(valor);   
    idreemplazo = valor;
    idinvreemplazo = id;
    artinvreemplazo = articulo;
    reemplazonombreart=nombre;
}

function guardareemplazo() {   
    if ($('#sreemplazonueva').val() != null || $('#sreemplazonueva').val() !=undefined) {
        var nueva=$('#sreemplazonueva').val();    
    articulos.forEach(function (item) {
        if (item.InventarioClave == idinvreemplazo) {
            item.Reemplazo = nueva;
            item.Reemplazada = true;
        }
    });
    $("#modalReemplazo").modal("hide");
    RecibeArticulo(artinvreemplazo, 1, reemplazonombreart)
    } else {
        toastr.error('Error, debes de ingresar una serie de reemplazo');
    }
   
}

function recibe(id,status) {   
    
    articulos.forEach(function (item) {
        if (item.InventarioClave === id) {            
            item.Selected = (status == true) ? false : true;
        }
    });
    
}


$('#btn_PEguardar').click(function () {

    var d = $('#devEnv').val();
    var tra = $('#edPE_transportista').val();
    var guia = $('#edPE_guia').val();
    var fecha = $('#edPE_fecha').val();
    var url = '/DevolucionAlmacenCentral/Envio/';
    $.ajax({
        url: url,
        data: { 'proceso': d, 'trans': tra, 'guia': guia, 'fecha': fecha },
        type: "POST",
        success: function (data) {
            if (data == "1") {
                $('#edProcesoEnvio').modal('hide');
                ObtenerLista();
                toastr.success("Se han agregado los datos de envio");
            }
            else {
                toastr.warning("No se pudo ingresar los datos de envio");
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});

function AsignaArticulo(idArticulo, idTipoArticulo) {

    var a = "";
    $('#articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');

    var url = '/SalidaMaterialTecnico/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idTipoArticulo },
        type: "POST",
        success: function (data) {

            for (var a = 0; a < data.length; a++) {
                if (data[a].ArticuloClave == idArticulo) {

                    $('#articulo').append('<option selected rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');

                    var b = (data[a].catSerieClaveInterface == null ? "0" : "1");
                    DetalleArticulo(b);
                } else {

                    $('#articulo').append('<option rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');
                }
            }
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });

    return a;

}
