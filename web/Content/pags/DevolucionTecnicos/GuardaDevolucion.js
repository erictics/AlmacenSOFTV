﻿$('#guardaSalida').click(function () {

    if (articulos.length == 0) {
        toastr.warning("Atención, no puede realizar una salida sin artículos");

    }
    else if ($('#ed_almacen').val() == "0") {
        toastr.warning("Atención, no puede realizar una salida sin seleccionar un almacén");
    }
    else if ($('#ed_tecnico').val() == "0") {
        toastr.warning("Atención, no puede realizar una salida sin seleccionar un técnico");
    }
    
    else {


        var url = '/DevolucionMaterialTecnicos/Guardar/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'idAlmacen': $('#ed_almacen').val(), 'idTecnico': $('#ed_tecnico').val(), 'articulos': JSON.stringify(articulos), 'obs': $('#ed_observaciones').val() },
            type: "POST",
            success: function (data) {
                if (data.idError == "-1") {
                    toastr.warning("Atención "+ data.Mensaje);
                }
                else if (data.idError == "-2") {
                    toastr.warning("Atención "+ data.Mensaje);
                }
                else if (data.idError == "-3") {
                    toastr.warning("Atención " + data.Mensaje);
                }
                else if (data.idError == "-4") {
                    toastr.warning("Atención " + data.Mensaje);
                }
                else if (data.idError == "-5") {
                    toastr.warning("Atención" + data.Mensaje);
                }
                else if (data.idError == "-6") {
                    toastr.warning("Atención"+ data.Mensaje);
                }
                else if (data.idError == "-7") {
                    toastr.warning("Atención"+ data.Mensaje);
                }
                else {
                    toastr.success("Correcto, la devolución de material se realizó correctamente");
                    LlenaGrid(1);

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle Devolución");
                    $("#framecanvas").attr("src", "/DevolucionMaterialTecnicos/Detalle?pdf=2&s=" + data);

                    $('#edita').modal('hide');
                    //$("#eliframe").attr("src", "/DevolucionMaterialTecnicos/Detalle?pdf=2&s=" + data);
                    //$('#ver').modal('show');
                    
                }
               
                


            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });




    }



});