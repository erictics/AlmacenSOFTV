﻿


SinFormato = function (n) {
    return n.replace(',', '').replace(',', '').replace('$', '').replace('$', '').replace('$', '');
}

ToMoneda = function (n) {
    return ToMonedaFormato(n, 2,',','.');
}


ToMonedaFormato = function (n, decPlaces, thouSeparator, decSeparator) {
    var n = n,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + "$" + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

var lenguaje = {

    search: "Buscar&nbsp;:",
    lengthMenu: "Mostrar _MENU_ Aparatos",
    info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
    infoEmpty: "No hay aparatos para mostrar",
    infoFiltered: "(filtrados _MAX_ )",
    infoPostFix: "",

    zeroRecords: "No hay aparatps para mostrar",
    emptyTable: "No hay aparatos disponibles",
    paginate: {
        first: "Primera",
        previous: "Anterior",
        next: "Siguiente",
        last: "Ultima"
    },
    aria: {
        sortAscending: ": activer pour trier la colonne par ordre croissant",
        sortDescending: ": activer pour trier la colonne par ordre décroissant"
    }
}




