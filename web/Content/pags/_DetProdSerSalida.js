﻿var Metodo = 0;
var nTotOrd = 0;
var nTotSur = 0;
var nPend = 0;
var idx_art = 0;


$(document).ready(function () {
    $("#formaeditadistribuidor").submit(function (e) {
        e.preventDefault();
        GuardaInfo();
    });
});

$(".inventarios").click(function () {
    alert("hola");
});


function SalidaMaterialDetalle() {

    $(".detalle").click(function () {

        var elrel = $(this).attr('rel');
        idx_art = elrel;

        nTotOrd = $("#TotPed" + elrel).html();
        nTotSur = $("#TotSur" + elrel).html();
        nPend = nTotOrd - nTotSur;

        if (nPend == 0) {
            $("#_DetProdSer-Busca").prop("disabled", true);
        }
        else {
            $("#_DetProdSer-Busca").removeAttr("disabled");
        }

        $("#resumen tbody").empty();
        var tbody = "<tr><td id='cellTO'>" + nTotOrd + "</td><td id='cellTS'>" + nTotSur + "</td><td id='cellP'>" + nPend + "</td></tr>";
        $("#resumen tbody").append(tbody);

        var objArticuloDetalle = {};
        objArticuloDetalle.Articulo = objArticulos[parseInt(elrel)].ArticuloClave;
        objArticuloDetalle.Proceso = _pedido;
        objArticuloDetalle.Salida = _salida;
        objArticuloDetalle.TipoSalida = "traspaso";

        _indiceArticulo = elrel;

        var myJSONText = JSON.stringify(objArticuloDetalle);

        var url = '/Inventario/Articulo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                console.log(data);
                var arreglo = eval(data);
                _articulospedidos = arreglo;

                if (_articulospedidos.length > 0) {
                    IniciaDetalle();
                    IniciaContenido();
                    $("#_DetProdSer-Titulo").html("Surtido de Ordenes - " + objArticulos[parseInt(elrel)].ArticuloTexto);
                    $("#lblBusqueda").html("Capturar " + _articulospedidos[0].catSeriesClaves[0].Nombre + ":");
                    $("#_DetProdSer-Busca").attr("placeholder", _articulospedidos[0].catSeriesClaves[0].Nombre);
                    $("#edDetProdSer").modal("show");
                    $("#_DetProdSer-Busca").focus();
                }
                else {
                    toastr.error('Error', 'No existe inventario en el almacén para surtir el pedido');
                    $("#_DetProdSer-Busca").prop("disabled", true);
                }
            },
            error: function (request, error) {
                toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });

        $("#_DetProdSer-Busca").bind("keypress keydown keyup", function (e) {
            var texto = $(this).val();
            if (e.which == 13) {
                e.preventDefault();

                if (texto.length == 0) {
                    e.stopPropagation();
                    return false;
                }

                var idx = 0;
                var chB;
                var vbEncontrado = false;
                var rows = $("#lasseries tbody tr");

                for (var foo = 0; foo < rows.length; foo++) {
                    if ($(rows[foo]).attr("data-rel").indexOf(texto.toUpperCase()) > -1) {
                        chB = $("input[type='checkbox'][name*='cb']")[foo].id;
                        if ($("#" + chB).prop("checked")) {
                            $("#_DetProdSer-Busca").val("");
                            $("#_DetProdSer-Busca").focus();
                            return false;
                        }
                        nTotSur++;
                        nPend--;
                        $("#" + chB).prop("checked", "checked");
                        $("#" + chB).parent().addClass("checked");
                        $("#_DetProdSer-Busca").val("");
                        objArticulos[_indiceArticulo].Inventario.push($("#" + chB).attr("rel"));
                        if (nTotOrd == nTotSur) {
                            $(".inventarios:not(:checked)").prop("disabled", true);
                            $("#_DetProdSer-Busca").prop("disabled", true);
                        }
                        else {
                            $("#_DetProdSer-Busca").focus();
                        }

                        ActRes();

                        break;
                    }
                }
                e.stopPropagation();
                return false;
            }
        });
    });

    $("#btn_cerrar").click(function () {
        $("#TotPed" + idx_art).html(nTotOrd);
        $("#TotSur" + idx_art).html(nTotSur);
        $("#Pen" + idx_art).html(nPend);
    });
}

function IniciaDetalle() {
    $("#lasseries thead").empty();
    $("#lasseries tbody").empty();

    if (_articulospedidos[0] != undefined || _articulospedidos[0] != null) {
        var tiposeries = _articulospedidos[0].catSeriesClaves;
        var tr = "";
        tr += "<tr>";
        tr += "<th>Seleccionar</th>";
        for (var ii = 0; ii < tiposeries.length; ii++) {
            tr += "<th>" + tiposeries[ii].Nombre + "</th>";
        }
        tr += "</tr>";
        $("#lasseries thead").append(tr);
    }
    else {
        $("#lasseries thead").append('<tr><td>No ha artículos en almacen</td></tr>');
    }
}

function IniciaContenido() {
    var tbody = "";
    for (var ii = 0; ii < _articulospedidos.length; ii++) {
        var cadena = '';
        tbody += "<tr data-rel='_remplazar_'>";
        tbody += "<td><input type='checkbox' id='cb" + _articulospedidos[ii].Clave + "' name='cb" + _articulospedidos[ii].Clave + "' rel='" + _articulospedidos[ii].Clave + "' class='inventarios' _checked_ /></td>";
        for (var ij = 0; ij < _articulospedidos[ii].Seriales[0].datos.length; ij++) {
            tbody += "<td>" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "</td>";
            cadena += "|" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "|";
        }
        tbody = tbody.replace("_remplazar_", cadena);
        var array = objArticulos[_indiceArticulo].Inventario;
        var i = array.indexOf(_articulospedidos[ii].Clave);
        if (i != -1) {
            tbody = tbody.replace("_checked_", "checked");
        }
        else {
            tbody = tbody.replace("_checked_", "");
        }
        tbody += "</tr>";
    }

    $("#lasseries tbody").append(tbody);

    $('input[type="checkbox"]:not(".switch")').iCheck({
        checkboxClass: 'icheckbox_square-blue ',
        increaseArea: '20%'
    });

    $('input[type="checkbox"]:not(".switch")').on('ifChecked', function (event) {

        var cantidad = nTotOrd;
        if (cantidad > nTotSur) {
            objArticulos[_indiceArticulo].Inventario.push($(this).attr('rel'));
            nTotSur++;
            nPend--;
            ActRes();
        }
        if (cantidad == nTotSur) {
            $("#_DetProdSer-Busca").prop("disabled", true);
            $(".inventarios:not(:checked)").prop("disabled", true);
        }
        else {
            $("#_DetProdSer-Busca").focus();
        }
    });

    $('input[type="checkbox"]:not(".switch")').on('ifUnchecked', function (event) {

        var cantidad = nTotOrd;
        var array = objArticulos[_indiceArticulo].Inventario;
        var i = array.indexOf($(this).attr('rel'));
        if (i != -1) {
            objArticulos[_indiceArticulo].Inventario.splice(i, 1);
            nTotSur--;
            nPend++;
            ActRes();
        }
        if (cantidad > nTotSur) {
            $("#_DetProdSer-Busca").removeAttr("disabled");
            $(".inventarios").removeAttr("disabled");
            $("#_DetProdSer-Busca").focus();
        }
    });
}

function ActRes() {
    // Limpiamos contenido
    $("#cellTS").html("");
    $("#cellP").html("");

    // Actualizamos Resumen
    $("#cellTS").html(nTotSur);
    $("#cellP").html(nPend);
}