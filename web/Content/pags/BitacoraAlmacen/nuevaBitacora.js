﻿var claveBitacora = 0;
$('#btn-guardar').attr('disabled', false);
$('#distribuidor-bitacora').attr('disabled', false);
$('#almacen-bitacora').attr('disabled', false);
$('#tecnicos-bitacora').attr('disabled', false);
$('#numeroOrden').attr('disabled', false);
$('#numeroOrden').attr('disabled', false);
$('#contrato').attr('disabled', false);
$('#fecha').attr('disabled', false);
$('#panelArticulos').hide();
$('#seleccionaSeries').hide();
$('#seleccionaArticulo').hide();

$('#guardaAparatos').hide();
$('#guardaAccesorios').hide();

var articulos_tecnico = [];
var articulos_bitacora = [];

function entidadesDistribuidor() {
    getAlmacenes();
    getTecnicos();
}

function getAlmacenes() {
    var url = '/BitacoraAlmacen/GetalmacenbyDistribuidor/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'distribuidor': $('#distribuidor-bitacora').val() },
        type: "POST",
        success: function (data) {
            data.forEach(function (item) {
                $('#almacen-bitacora').append("<option value=" + item.EntidadClave + ">" + item.Nombre + "</option>");
            });

        },
        error: function (request, error) {
            toastr.error('Sucedio un error de conexión /su sesión ha expirado');
        }
    });
}

function ObtenInventarioTecnico() {
    articulos_tecnico = [];
    $('#articulos-tecnico').empty();
    var url = '/BitacoraAlmacen/ObtenInventarioTecnico/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { "tecnico": $('#tecnicos-bitacora').val() },
        type: "POST",
        success: function (data) {
            articulos_tecnico = data;
            $('#articulos-tecnico').append("<option value=''>--Selecciona--</option>");
            data.forEach(function (item) {
                $('#articulos-tecnico').append("<option value=" + item.Clave + " data-cantmax=" + item.cantidad + " >" + item.Articulo + "</option>");
            });         
          
        },
        error: function (request, error) {
            toastr.error('Sucedio un error de conexión /su sesión ha expirado');
        }
    });
}



$("#formbitacora").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formbitacora').validationEngine('validate');
    if (valid) {
        var data = {
            'distribuidor': $('#distribuidor-bitacora').val(),
            'almacen': $('#almacen-bitacora').val(),
            'tecnico': $('#tecnicos-bitacora').val(),
            'orden': $('#numeroOrden').val(),
            'tipo': '',
            'contrato': $('#contrato').val(),
            'usuario': '',
            'fecha': $('#fecha').val(),
            'articulos': JSON.stringify(articulos_bitacora)
        }

        var url = '/BitacoraAlmacen/GuardaBitacora/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: data,
            type: "POST",
            success: function (data) {
                if (data == "-2" || data == "-1") {
                    toastr.warning(data.Mensaje);
                }
                else {              
                    claveBitacora = data;
                    $('#btn-guardar').attr('disabled', true);
                    $('#distribuidor-bitacora').attr('disabled', true);
                    $('#almacen-bitacora').attr('disabled', true);
                    $('#tecnicos-bitacora').attr('disabled', true);
                    $('#numeroOrden').attr('disabled', true);
                    $('#numeroOrden').attr('disabled', true);
                    $('#contrato').attr('disabled', true);
                    $('#fecha').attr('disabled', true);
                    $('#panelArticulos').show();               
                    ObtenInventarioTecnico();
                    $('#msj').empty().append(' El detalle de la bitácora se guardó correctamente, ahora puedes ingresar los articulos necesarios a la bitácora #' + claveBitacora);
                    toastr.success('la bitacora se ha creado correctamente');

                } 
            },
            error: function (request, error) {
                toastr.error('Sucedio un error de conexión /su sesión ha expirado');
            }
        });


    }
});
    
function cambioTecnico() {
    articulos_tecnico = [];
    articulos_bitacora = [];
    ObtenInventarioTecnico()
    $('#tseries').empty();
    actualizaarticulosBitacora();
}



function getTecnicos() {
    $('#tecnicos-bitacora').empty();
    var url = '/BitacoraAlmacen/GettecnicosbyDistribuidor/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'distribuidor': $('#distribuidor-bitacora').val() },
        type: "POST",
        success: function (data) {
            data.forEach(function (item) {
                $('#tecnicos-bitacora').append("<option value=" + item.EntidadClave + ">" + item.Nombre + "</option>");
            });

        },
        error: function (request, error) {
            toastr.error('Sucedio un error de conexión /su sesión ha expirado');
        }
    });
}




function ObtenDetalleArticulo() {
    $('#tseries').empty();
    $('#cantidad-articulo').val(1);   

    articulos_tecnico.forEach(function (item) {
        if (item.Clave == $('#articulos-tecnico').val()) {
            console.log(item);
            if (item.tieneSeries) {
                $('#seleccionaSeries').show();
                $('#guardaAparatos').show();
                $('#guardaAccesorios').hide();
                $('#seleccionaCantidad').hide();
                item.series.forEach(function (serie_) {
                    console.log(serie_);
                    $('#tseries').append("<option value=" + serie_.IdInventario + ">" + serie_.serie + "</option>");
                });
            }
            else {
                $('#seleccionaSeries').hide();
                $('#seleccionaCantidad').show();
                $('#guardaAparatos').hide();
                $('#guardaAccesorios').show();
            }
        }        
    });
}

function guardarAparato() {

    if ($('#articulos-tecnico').val() != "" && $('#tseries').val()) {

    }
    if(!existe_serie($("#tseries").val())){
        var articulo = {
            "Clave":$("#articulos-tecnico").val(),
            "IdInventario":$("#tseries").val(),
            "Serie": $("#tseries option:selected").text(),
            "Articulo":$("#articulos-tecnico option:selected").text(),
            "Cantidad":1
        }
        articulos_bitacora.push(articulo);
        actualizaarticulosBitacora();
    }
   

}


function eliminarArticulo(Articulo) {
    EliminarDeArreglo(articulos_bitacora, 'Articulo', Articulo);
    actualizaarticulosBitacora();
}


function EliminarDeArreglo(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}

function guardarAccesorio() {
    if (!existeAccesorio($("#articulos-tecnico option:selected").text())) {
        var articulo = {
            "Clave": $("#articulos-tecnico").val(),
            "IdInventario": 0,
            "Serie": '',
            "Articulo": $("#articulos-tecnico option:selected").text(),
            "Cantidad": $('#cantidad-articulo').val()
        }
        articulos_bitacora.push(articulo);
    } else {
        EditaArreglo($("#articulos-tecnico").val(), $('#cantidad-articulo').val())
    }
   
    actualizaarticulosBitacora($("#articulos-tecnico").val(), $('#cantidad-articulo').val());
}


function EditaArreglo(idArticulo, cantidad) {
    for (var i in articulos_bitacora) {
        if (articulos_bitacora[i].Clave == idArticulo) {
            articulos_bitacora[i].Cantidad = cantidad;
        }
    }
}


function actualizaarticulosBitacora() {
    $('#articulosBitacora').empty();
    articulos_bitacora.forEach(function (item) {
        $('#articulosBitacora').append('<tr><td>' + item.Articulo + '</td><td>' + item.Serie + '</td><td>'+ item.Cantidad + '</td><td><button onClick="eliminarArticulo(\'' + item.Articulo + '\')"  type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Quitar</button></td></tr>');
    });
   
}

function existe_serie(IdInventario) {
    var result = $.grep(articulos_bitacora, function (obj) { return obj.IdInventario == IdInventario; });
    return (result.length == 0) ?false:true;   
}

function existeAccesorio(articulo){
    var result = $.grep(articulos_bitacora, function (obj) { return obj.Articulo == articulo; });
    return (result.length == 0) ? false : true;

} 
