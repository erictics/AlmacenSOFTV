﻿var objArticulos = new Array();
var _pesos = 0;
var _dolares = 0;
var _fecha = '';

$(document).ready(function () {

    $("#validaimportes").click(function () {
        Confirma(true);
    });

    $('#cuenta').attr('disabled', true);
    


    $(".editOk").click(function () {
        Confirma(false);
    });

    $("#buscastatus,#buscaproveedor").change(function () {
        LlenaGrid(1);
    });

    $("#buscar").click(function () {

        var valid = jQuery('#formbusca').validationEngine('validate');
        if (valid) {
            LlenaGrid(1);
        }
    });

    $("#btnOkConfirm").click(function () {
        $("#Confirma").modal("hide");

        var objParametros = {};
        objParametros.clave = $("#confirma_id").val();
        objParametros.accion = 'rechaza';

        var myJSONText = JSON.stringify(objParametros);
        

        $.ajax({
            url: '/OrdenCompra/Autoriza/',
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                $("#loader").hide();
                LlenaGrid(_pag);
                toastr.success( "Pago de orden correctamente cancelado");
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    LlenaGrid(1);
});




var _pag = 1;



function LlenaGrid(pag) {

        var objOrdenCompraPagos = {};
        objOrdenCompraPagos.pag = pag;
        objOrdenCompraPagos.estatus = $("#buscastatus").val();
        objOrdenCompraPagos.distribuidor = $("#buscadistribuidor").val();
        objOrdenCompraPagos.pedido = $("#abuscar").val();
        objOrdenCompraPagos.esPagare = 0;

        var myJSONText = JSON.stringify(objOrdenCompraPagos);

        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bDestroy": true,
            "info": true,
            "stateSave": true,
            "lengthMenu": [[5], [5]],
            "ajax": {
                "url": "/Pedido/ObtenLista/",
                "type": "POST",
                "data": { 'data': myJSONText },
            },
            "fnInitComplete": function (oSettings, json) {

                

            },
            "fnDrawCallback": function (oSettings) {

                AfterAjax();

            },
            "columns": [
                { "data": "NumeroPedido", "orderable": false },
                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        
                        return "<p style='font-size:10px;'>"+full.Distribuidor+"</p>"
                    }

                },
                { "data": "Fecha", "orderable": false },
                { "data": "Total", "orderable": false },
                { "data": "Total_USD", "orderable": false },
                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        
                        return full.Estatus.replace('Autorizado', 'Pendiente Por Surtir');;
                    }

                },
                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        console.log(full);
                       return DameFacturacion(full.Clave, full.Estatus, full.FacturacionClave, full.EsPagare);
                    }

                },
                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        
                       return DameAcciones(full.Clave, full.Estatus, SinFormato(full.Total), SinFormato(full.Total_USD), full.Fecha, full.TieneSalidas, full.Facturacion)
                    }
                }

            ],

            language: {
                processing: "Procesando información...",
                search: "Buscar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ Elementos",
                info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
                infoEmpty: "No hay elemetos para mostrar",
                infoFiltered: "(filtrados _MAX_ )",
                infoPostFix: "",
                loadingRecords: "Búsqueda en curso...",
                zeroRecords: "No hay registros para mostrar",
                emptyTable: "No hay registros disponibles",
                paginate: {
                    first: "Primera",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultima"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },


            "order": [[0, "asc"]]
        })
    

}


function MostrarCuenta(cuenta) {
    if (cuenta == "si") {
        $('#cuenta').attr('disabled', false);
        
        $("#cuenta").prop('required', true);

        $('#ed_cuenta').attr('disabled', false);

        $("#ed_cuenta").prop('required', true);

    } else {
        $('#cuenta').val(0);
        $("#ed_cuenta").val(0);
        $('#cuenta').attr('disabled', true);
        $("#cuenta").prop('required', false);
        $('#ed_cuenta').attr('disabled', true);
        $("#ed_cuenta").prop('required', false);
    }
}

$('#ed_forma_pago').change(function () {    
    var cuenta = $('option:selected', this).attr('cuenta');
    MostrarCuenta(cuenta);

});

$('#forma_pago').change(function () {
    var cuenta = $('option:selected', this).attr('cuenta');
    MostrarCuenta(cuenta);

});






function AfterAjax() {
    $(".ver").click(function () {
        var elrel = $(this).attr('rel');
        $("#comentario").val('');
        $("#autoriza_id").val(elrel);

        $('#repo').click();
        $('#titulocanvas').empty().append("Detalle de orden de compra");
        $("#framecanvas").attr("src", "/Pedido/Detalle?pdf=2&p=" + elrel);

        //$("#eliframe").attr("src", "/Pedido/Detalle?pdf=2&p=" + elrel);
        //$('#ver').modal('show');
    });

    $(".pago").click(function () {
        var elrel = $(this).attr('rel');
        _pesos = $(this).attr('data-pesos');
        _dolares = $(this).attr('data-dolares');
        _fecha = $(this).attr('data-fecha');

        $("#pago_id").val(elrel);
        $("#eliframepago").attr("src", "/Pedido/Detalle?pdf=2&p=" + elrel);
        $("#montopesos").val(0);
        $("#montodolares").val(0);
        $("#formapago").val("");
        $("#banco").val("");
        $("#fechapago").val("");
        $('#pago').modal('show');
    });

    $(".edit").click(function () {
        var elrel = $(this).attr("rel");
        _pesos = $(this).attr("data-pesos");
        _dolares = $(this).attr("data-dolares");
        _fecha = $(this).attr("data-fecha");

        $("#ed_pago_id").val(elrel);
        $("#ed_pago").modal("show");

        var objParametros = {};
        objParametros.proceso = elrel;
        var myJSONText = JSON.stringify(objParametros);
        $("#loader").show();

        $.ajax({
            url: '/OrdenCompra/Pago/',
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                $("#loader").hide();

                var dp = eval(data);
                console.log(dp.NumeroCuenta);
                $("#ed_cuenta").val(dp.NumeroCuenta);
                $("#ed_montopesos").val(dp.Monto);
                $("#ed_montodolares").val(dp.MontoUSD);
                $("#ed_forma_pago").val(dp.Forma);
                $("#ed_banco").val(dp.Banco);
                $("#ed_fechapago").val(dp.FechaPago);
                $("#ed_user").html(dp.Usuario);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });



    $(".factura").click(function () {

        _productoSeleccionado = -1;

        var cual = $(this).attr('rel');

        $("#cancela_id").val(cual);

        var url = '/Factura/Solicita/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                toastr.warning('Se ha solicitado la factura');

                LlenaGrid(_pag);
            }
        });
    });



    $(".pdf").click(function () {
        var elrel = $(this).attr('rel');
        window.location.href = "/Pedido/Detalle?pdf=1&p=" + elrel;
    });

    $(".cancel").click(function () {
        $("#confirma_id").val($(this).attr("rel"));
        $("#Confirma button[id='btnOkConfirm']").removeClass("btn-info")
        $("#Confirma button[id='btnOkConfirm']").addClass("btn-danger")
        //$("#Confirma h4").html($("body div[class='page-title']").html().trim());
        $("#Confirma label").html("¿ Deseas cancelar el pago de la orden #" + $(this).parent().siblings(":first").text() + "?");
        $("#Confirma").modal("show");
    });
}

function DameAcciones(elid, status, pesos, dolares, fecha, tienesalidas,facturacion)
{
    console.log(status);
    console.log(facturacion);
    var controles = "<button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-primary ver' data-toggle='tooltip' data-placement='right' title='' data-original-title='Ver pedido (detalle de orden de compra)' "
        + " rel='"        + elid + "'><i class='fa fa-search'></i></button>";


    if (status == "Por pagar") {
        controles += "<button type='button' class='btn btn-default-bright btn-xs pago' rel='" + elid + "' data-pesos='" + pesos + "' data-dolares='" + dolares + "' data-fecha='" + fecha + "'><i class='fa fa-usd' aria-hidden='true'></i> Capturar Pago</button>";
    }
    else {
        if (status == "Cancelado") { }

        else {

            if (tienesalidas == "0") {

                if (facturacion == "Factura solicitada") {

                }
                else {
                    controles += "<button type='button' class='btn btn-warning btn-xs edit' rel='" + elid + "' data-pesos='" + pesos + "' data-dolares='" + dolares + "' data-fecha='" + fecha + "'><i class='fa fa-edit'></i> Editar</button>&nbsp;<button type='button' class='btn btn-danger btn-xs cancel' rel='" + elid + "'><i class='fa fa-trash'></i> Cancelar</button>";
                }
            }

        }
        
    }
    return controles;
}


function DameFacturacion(elid, estado, estadofacturacion,espagare) {
    var elregresa = "";
    if (estado != 'Por pagar' ) {
        console.log(espagare);
        if (estadofacturacion == '' || estadofacturacion == '16') {

            if (estado != 'Cancelado') {
                if (espagare == 1) {

                } else {

                    elregresa += "<button type='button' class='btn btn-info btn-xs factura' rel='" + elid + "'>Facturar</button>";
                }
            }
        } else if (estadofacturacion == '18' || estadofacturacion == '17') {
            // elregresa += "<button type='button' class='btn btn-danger btn-xs cancelarfactura' rel='" + elid + "'>&nbsp;Cancelar Factura</button>";
            elregresa += "<button type='button' class='btn btn-success btn-xs ' rel='" + elid + "'>En proceso</button>";
        }
    }
    console.log(elregresa);
    return elregresa;
}


function Confirma(autoriza) {

    var vPref = "";
    if (!autoriza)
    {
        vPref = "ed_";
    }

    var montopesos = SinFormato($("#" + vPref + "montopesos").val());
    var montodolares = SinFormato($("#" + vPref + "montodolares").val());

    if (parseFloat(montopesos) == parseFloat(_pesos) && parseFloat(montodolares) == parseFloat(_dolares)) {

        var valid = jQuery("#" + vPref + "formapago").validationEngine("validate");
        if (valid) {

            var f = new Date();

            var fecha = $("#" + vPref + "fechapago").val().split('/')[2] + "/" + $("#" + vPref + "fechapago").val().split('/')[1] + "/" + $("#" + vPref + "fechapago").val().split('/')[0];
            if (Date.parse(fecha) > Date.parse(f + 1)) {
                toastr.warning('Pago', 'la fecha no puede ser mayor al día de hoy');
                return;
            }

            var fechapedido = _fecha.split('/')[2] + "/" + _fecha.split('/')[1] + "/" + _fecha.split('/')[0];
            if (Date.parse(fecha) < Date.parse(fechapedido)) {
                toastr.warning('Pago', 'la fecha no puede ser menor a la fecha del pedido');
                return;
            }

            var objRoles = {};
            objRoles.accion = (autoriza) ? "autoriza" : "edita";
            objRoles.montopesos = $("#" + vPref + "montopesos").val();
            objRoles.montodolares = $("#" + vPref + "montodolares").val();
            objRoles.formapago = $("#" + vPref + "forma_pago").val();
            objRoles.banco = $("#" + vPref + "banco").val();
            objRoles.cuenta = $("#" + vPref + "cuenta").val();
            objRoles.fechapago = $("#" + vPref + "fechapago").val();
            objRoles.clave = $("#" + vPref + "pago_id").val();

            var myJSONText = JSON.stringify(objRoles);

            var url = '/OrdenCompra/Autoriza/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#" + vPref + "pago").modal("hide");
                    var Msj = (autoriza) ? "Se registró el pago, y el pedido se autorizó correctamente" : "Se editó el pago de manera satisfactoria";
                    toastr.success(Msj);

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });
        }
    }
    else {
        toastr.warning('El monto de pago no coincide, los montos de pago no coindicen entre lo pagado y la orden de compra');
    }
}