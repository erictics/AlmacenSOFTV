﻿$(document).ready(function () {

    $(".permiso").prop("disabled", true);
    ObtenPermisos(1);
    $("#rol").val(1);


    $("#forma").submit(function (e) {
        e.preventDefault();
        var valid = jQuery('#forma').validationEngine('validate');
        if (valid) {

            var url = '/Permisos/Guarda/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $("#forma").serialize(),
                type: "POST",
                success: function (data) {


                    toastr.success('Los permisos se establecieron correctamente');

                },
                error: function (request, error) {

                    toastr.error('Surgió un error, intente nuevamente más tarde');

                }
            });
        }
    });


});
   

    $("#rol").change(function () {
        $(".permiso").prop("disabled", true);

        $(".permiso").removeAttr('checked');
        $(".permiso").parent().removeClass('checked');
        var valor;
        if ($(this).val() != '-1') {

            valor = $(this).val();
        } else {
            valor = 1;
        }      
        ObtenPermisos(valor);
    });

    function ObtenPermisos(idRol) {

        var url = '/Permisos/Obten/?rol=' + idRol
        $.ajax({
            url: url,
            dataType: 'json',
            type: "POST",
            success: function (data) {

                var array = data.toString().split(",");

                for (var i = 0; i < array.length; i++) {
                    var datos = array[i].split('|');

                    var modulo = datos[0];
                    var consulta = datos[1];
                    if (consulta == '1') {
                        $("#co_" + modulo).prop('checked', 'checked');
                        $("#co_" + modulo).parent().addClass('checked');
                    }
                    else {
                        //alert("bb");
                        $("#co_" + modulo).removeAttr('checked');
                        $("#co_" + modulo).parent().removeClass('checked');
                    }

                    var agregar = datos[2];
                    if (agregar == '1') {
                        $("#ag_" + modulo).prop('checked', 'checked');
                        $("#ag_" + modulo).parent().addClass('checked');
                    }
                    else {
                        //alert("bb");
                        $("#ag_" + modulo).removeAttr('checked');
                        $("#ag_" + modulo).parent().removeClass('checked');
                    }

                    var editar = datos[3];
                    if (editar == '1') {
                        $("#ed_" + modulo).prop('checked', 'checked');
                        $("#ed_" + modulo).parent().addClass('checked');
                    }
                    else {
                        //alert("bb");
                        $("#ed_" + modulo).removeAttr('checked');
                        $("#ed_" + modulo).parent().removeClass('checked');
                    }

                    var eliminar = datos[4];
                    if (eliminar == '1') {
                        $("#el_" + modulo).prop('checked', 'checked');
                        $("#el_" + modulo).parent().addClass('checked');
                    }
                    else {
                        //alert("bb");
                        $("#el_" + modulo).removeAttr('checked');
                        $("#el_" + modulo).parent().removeClass('checked');
                    }

                    var ejecutar = datos[5];
                    if (ejecutar == '1') {
                        $("#ej_" + modulo).prop('checked', 'checked');
                        $("#ej_" + modulo).parent().addClass('checked');
                    }
                    else {
                        //alert("bb");
                        $("#ej_" + modulo).removeAttr('checked');
                        $("#ej_" + modulo).parent().removeClass('checked');
                    }


                }

                //Mensaje('Permisos', 'Los permisos se establecieron correctamente');

                $(".permiso").removeAttr("disabled");
                $(".permiso").parent().removeClass("disabled");

                $("#loader").hide();
            },
            error: function (request, error) {
                Mensaje('Surgió un error, intente nuevamente más tarde');
                $("#loader").hide();
            }
        });

    }




    
  


