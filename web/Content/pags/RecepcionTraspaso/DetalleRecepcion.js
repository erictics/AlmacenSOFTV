﻿var idTraspaso;


$(document).ready(function () {
    jQuery('#formaeditadistribuidor').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false

    });

});


function DetalleRecepcion(id) {
    idTraspaso = id;
    articulos = [];
    $('#drecepcion').empty();
    var url = '/RecepcionTraspaso/DetalleTraspaso/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idTraspaso': id },
        type: "POST",
        success: function (data) {
            $('#AgregaRecepcion').modal('show');
            for (var a = 0; a < data.length; a++) {
                if (parseInt(data[a].pendiente)>0) {

                    var renglon = "";
                    renglon += "<tr><td><b>" + data[a].Nombre + "</b></td><td>" + data[a].Cantidad + "</td><td>" + data[a].pendiente + "</td><td><input type='text' class='form-control input-sm' style='width:40%' id='input" + data[a].ArticuloClave + "' disabled></td>";
                    if (data[a].catSerieClaveInterface != null) {
                        renglon += '<td><button type="button" class="btn btn-xs btn-warning" onClick="Aparatos(\'' + data[a].ProcesoClave + '\',\'' + data[a].ArticuloClave + '\')">Aparatos</button></td>';
                    } else {

                        renglon += "<td><input type='text placeholder='Cantidad'  ArticuloClave=" + data[a].ArticuloClave + " style='width:40%' class='form-control txt input-sm validate[required,custom[integer],min[0],max[" + data[a].pendiente + "]]'></td>";
                    }
                    renglon += "</tr>";
                    $('#drecepcion').append(renglon);
                }

               
            }
            
        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}



function ResumenRecepcion(idtraspaso) {
    $('#Detalle').modal('show');
    $('#listaRecepciones').empty();
    var url = '/RecepcionTraspaso/DetalleAvenzadoRecepcion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idTraspaso': idtraspaso },
        type: "POST",
        success: function (data) {
            var str="";
            for (var a = 0; a < data.length; a++) {
                str += '<li class="tile"><div class="tile-text"><b>Recepción #' + data[a].NumeroPedido + '</b><small> <b>Fecha/Hora: </b>' + data[a].Fecha + ' <b>Usuario: </b>' + data[a].Usuario + '';
                str += '<table class="table"><tr><td>Artículo</td><td>Cantidad</td></tr>';
                for (var b = 0; b < data[a].DetArticulo.length; b++) {
                    
                    str += '<tr><td>' + data[a].DetArticulo[b].Articulo + '</td><td>' + data[a].DetArticulo[b].cantidad + ' </td></tr>';
                }
                str += "</table>";
                str += "</small></div>'";
                str += '<div class="tile-content"><button type="button"  onClick="descargarecepcion(\'' + data[a].Traspaso + '\')"  class="btn ink-reaction btn-floating-action btn-sm btn-info"><i class="fa fa-download"></i></button></div></li>';
            }
            $('#listaRecepciones').append(str);
        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });


}