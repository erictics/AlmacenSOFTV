﻿var articulos = [];

function Aparatos(idtraspaso, idarticulo) {

    $('#AgregaArticulo').modal('show');
    actualizaContador(idarticulo);
    $('#datos').dataTable({
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[10], [10]],
        "ajax": {
            "url": "/RecepcionTraspaso/Aparatos/",
            "type": "POST",
            "data": {
                'idtraspaso': idtraspaso,
                'idarticulo': idarticulo,
                'Articulos': JSON.stringify(articulos),
               
            }
        },        
        "columns": [
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         if (full.seleccionado == 1) {
                             return '<button class="btn btn-info btn-xs .selecciona" style=" width: 25px !important; height: 25px !important;padding:2px;"  onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';

                         } else {
                             return '<button class="btn btn-defaul btn-xs .selecciona" style="color:#FFFFFF;"   onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';
                         }

                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         return "<button class='btn btn-default btn-xs'>" + full.valor + "</button>"
                     }
                 }
                  
        ],

        language: {

            search: "Buscar&nbsp;:",            
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",

            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}
function activa(ArticuloClave, InventarioClave, Valor, Nombre, elem) {
    $(elem).css('width', '25px !important');
    $(elem).css('height', '25px !important;');
    $(elem).css('padding', '2px;');

    var Serie = {};
    Serie.InventarioClave = InventarioClave;
    Serie.catSerieClave = 2;
    Serie.Valor = Valor;
    Serie.ArticuloClave = ArticuloClave;
    Serie.Nombre = Nombre;
    Serie.Cantidad = 1;
    Serie.status = 7
    Serie.Statustxt ='Buen Estado'

    if (Valida_Serie_arreglo(InventarioClave) == "1") {
        EliminarDeArreglo(articulos, 'InventarioClave', InventarioClave);       
        Aparatos(idTraspaso, ArticuloClave);
        actualizaContador(ArticuloClave);
        ActualizaDetalle();
    }
    else {
      
        articulos.push(Serie);       
        Aparatos(idTraspaso, ArticuloClave);
        actualizaContador(ArticuloClave);
        ActualizaDetalle();
    }
}



function EliminarDeArreglo(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}



function actualizaContador(idarticulo) {
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == idarticulo });
    $('#cantidad').val(result.length);
}


function Valida_Serie_arreglo(idinventario) {
    var result = $.grep(articulos, function (e) { return e.InventarioClave == idinventario });
    if (result.length == 0) {
        return "0";
    } else {
        return "1";
    }
}

function ActualizaDetalle() {
    $('#contenidoresumen').html('');
    var linq = Enumerable.From(articulos);
    var result =
        linq.GroupBy(function (x) { return x.Nombre; })
            .Select(function (x) {
                return { Nombre: x.Key(), Clave: x.source[0].ArticuloClave, Cantidad: x.Sum(function (y) { return y.Cantidad | 0; }) };
            })
            .ToArray();
        $('#tablaresumen').show();
        $('#msn-detalle').hide();
        for (var a = 0; a < result.length; a++) {
            $("#input" + result[a].Clave + "").val(result[a].Cantidad);            
        }
}