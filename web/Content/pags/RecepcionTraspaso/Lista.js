﻿$(document).ready(function () {

    jQuery('#formaedita').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false
    });

    // llenagrid();
    llenagrid($("#Distribuidor").val(), $("#orden").val());
   
});





function llenagrid(alm, orden) {
    if ((alm == undefined) || (alm == 0)) {
        alm = 0;
    }
    if (orden == undefined || orden == "") {
        orden = 0;
    }

    var url = "/RecepcionTraspaso/ListaTraspasos/";
    var data = {
        'data': "1",
        'almacen': alm,
        'orden': orden
    }
    var columnas = [
                   { "data": "pedido", "orderable": false },
                   { "data": "fecha", "orderable": false },
                   {
                       sortable: false,
                       "render": function (data, type, full, meta) {
                           console.log(full);
                           var str;
                           if (full.estatus == 1) { str = "Por Autorizar" }
                           else if (full.estatus == 4) { str = "Recepcion Completa" }
                           else if (full.estatus == 3) { str = "Recibido Parcialmente" }

                           else { str = "Cancelada" }
                           return str;
                       }
                   },
                   { "data": "almacen", "orderable": false },
                    { "data": "almacen2", "orderable": false },
                   {
                       sortable: false,
                       "render": function (data, type, full, meta) {
                           return '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger btn-xs" onClick="descargapdf(\'' + full.proceso + '\')"><i class="fa fa-download"  aria-hidden="true" ></i> </button>&nbsp<button class="btn ink-reaction btn-floating-action btn-xs btn-info " onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search"  aria-hidden="true"></i></button>';

                       }
                   },
          {
              sortable: false,
              "render": function (data, type, full, meta) {
                  var str = "";
                  if (full.estatus == 1) {
                      str += '<button class="btn btn-success btn-xs  " onClick="DetalleRecepcion(\'' + full.proceso + '\')"> Recibir material</button>';
                      return str;
                  }
                  else if (full.estatus == 3) {

                      if (full.Recepciones.length > 0) {
                          str += '<button class="btn btn-warning btn-xs  " onClick="ResumenRecepcion(\'' + full.proceso + '\')">Recepciones</button>';
                      }

                      str += '<button class="btn btn-success btn-xs  " onClick="DetalleRecepcion(\'' + full.proceso + '\')"> Recibir material</button>';
                      return str;
                  }
                  else if (full.estatus == 4) {
                      if (full.Recepciones.length > 0) {
                          str += '<button class="btn btn-warning btn-xs  " onClick="ResumenRecepcion(\'' + full.proceso + '\')">Recepciones</button>';
                      }
                      return str;
                  }
                  else if (full.estatus == 5) {
                      return "";
                  }
                  else {
                      return "";
                  }
              }
          }
    ];
    llenaLista(url, data, columnas);
}


function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción de material");
    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + devolucion);
};


function descargapdf(devolucion) {
    window.location.href = "/DevolucionAlmacenCentral/Detalle?pdf=1&d=" + devolucion;
};

function verrecepcion(recepcion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción de material");
    $("#framecanvas").attr("src", "/RecepcionTraspaso/Detalle?pdf=2&d=" + recepcion);

}

function descargarecepcion(recepcion) {
    window.location.href = "/RecepcionTraspaso/Detalle?pdf=1&d=" + recepcion;
}


$('#buscar').click(function () {

    //llenaLista(0, $('#orden').val());
    llenagrid(0, $('#orden').val());

});

$('#Distribuidor').change(function () {

    //llenaLista($('#Distribuidor').val(), 0);
    llenagrid($('#Distribuidor').val(), 0);
});


var table;

function llenaLista(url, data, columnas) {

    table = $('#indice').DataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": url,
            "type": "POST",
            "data": data
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": columnas,

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay registros para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}