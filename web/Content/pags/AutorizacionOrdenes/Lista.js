﻿var _pag = 1;

function LlenaGrid(pag) {
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;
    objRoles.autoriza = "autoriza";
    objRoles.estatus = $("#buscastatus").val();
    objRoles.proveedor = $("#buscaproveedor").val();
    objRoles.pedido = $("#abuscar").val();

    var myJSONText = JSON.stringify(objRoles);


    $('#datos').dataTable({
        "processing": true,
        "responsive": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "/Pedido/PedidosAutorizador/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "columns": [
            { "data": "NumeroPedido", "orderable": false },
            { "data": "Destino", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Total", "orderable": false },
            { "data": "Total_USD", "orderable": false },
            { "data": "Estatus", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    return DameDetalle(full.Clave);
                }
            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    return DameAcciones(full.Clave, full.Estatus);
                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "order": [[0, "asc"]]
    })


}