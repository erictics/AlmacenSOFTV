﻿
function AutorizaOrden(id) {
    var objRoles = {};
    objRoles.comentario = "";
    objRoles.accion = 'autoriza';
    objRoles.clave = id;
    var myJSONText = JSON.stringify(objRoles);

    var url = '/OrdenCompra/Autoriza/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            toastr.success('Correcto, La Orden de Compra se autorizó correctamente');
            LlenaGrid(_pag);
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

function RechazaOrden(id) {
    var objRoles = {};
    objRoles.accion = 'rechaza';
    objRoles.clave = id;
    var myJSONText = JSON.stringify(objRoles);
    var url = '/OrdenCompra/Autoriza/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            toastr.success('La Orden de Compra se rechazó correctamente');
            LlenaGrid(_pag);
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

$("#buscastatus").change(function () {
    LlenaGrid(1);
});

$('#close').click(function () {
    $('#autorizacion').hide();
})

$("#buscaproveedor").change(function () {
    LlenaGrid(1);
});

$("#buscar").click(function () {
    var valid = jQuery('#formbusca').validationEngine('validate');
    if (valid) {
        LlenaGrid(1);
    }
});

$(document).ready(function () {
    LlenaGrid(1);
});
function verOrden(id) {
    $('#btnaut').click();
    $('#titulocanvas-aut').empty().append("Autorización de pedido");
    $("#frameaut").attr("src", "/Pedido/Detalle?pdf=2&p=" + id);
    $("#comentario").val('');
    $("#autoriza_id").val(id);
    $('#formadetalle').show();
}

function solover(id) {
    $('#btnaut').click();
    $('#titulocanvas-aut').empty().append("Autorización de pedido");
    $("#frameaut").attr("src", "/Pedido/Detalle?pdf=2&p=" + id);
    $('#formadetalle').hide();
}
function Descarga(id) {
    window.location.href = "/Pedido/Detalle?pdf=1&p=" + id;
};

function DameDetalle(elid) {
    var opt = "";
    opt += '<button type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver orden de compra" class="btn ink-reaction btn-floating-action btn-xs btn-primary" onClick="verOrden(\'' + elid + '\')"><i class="fa fa-search" aria-hidden="true"></i></button>';
    opt += '<button type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Descargar Orden de compra" class="btn ink-reaction btn-floating-action btn-xs btn-danger" onClick="Descarga(\'' + elid + '\')"  ><i class="md-content-paste"></i></button>';
    return opt;
}

function DameAcciones(elid, estatus) {
    if (estatus.toLowerCase().indexOf('autorizado') > -1 || estatus.toLowerCase().indexOf('rechazado') > -1 || estatus.toLowerCase().indexOf('parcial') > -1 || estatus.toLowerCase().indexOf('completo') > -1) {
        return '<button type="button" class="btn btn-info btn-xs solover"><i class="md-security" aria-hidden="true"></i> Autorizado</button>';
    }
    else {
        if (estatus.toLowerCase().indexOf('cancelado') > -1) {
            return "<button class='btn btn-warning btn-xs'><i class='fa fa-ban' aria-hidden='true'></i>&nbsp;sin acciones</button>";
        }
        else {
            return '<button class="btn btn-success btn-xs"  onClick="AutorizaOrden(\'' + elid + '\')"><i class="md-thumb-up"></i> Autoriza</button><button class="btn btn-warning btn-xs"  onClick="RechazaOrden(\'' + elid + '\')"><i class="md-thumb-down"></i> Rechaza</button>';
        }
    }
}