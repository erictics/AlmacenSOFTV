﻿$("#formaedita").submit(function (e) {
    e.preventDefault();
    if (_objPedido.Articulos.length == 0) {
        toastr.error('Faltan artículos en el pedido, debe agregar al menos un artículo para realizar un pedido');
        return;
    }
    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        Resumen();
        _objPedido.Servicio = $("#selectServicio").val();
        _objPedido.catProveedorClave = $("#ed_proveedor").val();
        _objPedido.EntidadClaveSolicitante = $("#ed_almacen").val();
        _objPedido.UsuarioClaveAutorizacion = $("#ed_autorizador").val();
        _objPedido.Observaciones = $("#ed_observaciones").val();
        _objPedido.TipoCambio = $("#ed_tipocambio").val();
        _objPedido.Iva = $("#ed_iva").val();
        _objPedido.Descuento = $("#ed_descuento").val();
        _objPedido.DiasCredito = $("#ed_diascredito").val();
        var objOtroGasto1 = {};
        objOtroGasto1.Concepto = $("#ed_conceptooc1").val();
        objOtroGasto1.Precio = $("#ed_preciooc1").val();
        objOtroGasto1.Moneda = $("#ed_monedaoc1").val();
        var objOtroGasto2 = {};
        objOtroGasto2.Concepto = $("#ed_conceptooc2").val();
        objOtroGasto2.Precio = $("#ed_preciooc2").val();
        objOtroGasto2.Moneda = $("#ed_monedaoc2").val();
        var objOtrosGastos = new Array();
        objOtrosGastos.push(objOtroGasto1);
        objOtrosGastos.push(objOtroGasto2);
        _objPedido.OtrosGastos = objOtrosGastos;
        _objPedido.ToSubtotal = $("#to_subtotal").val();
        _objPedido.ToDescuento = $("#to_descuento").val();
        _objPedido.ToIVA = $("#to_iva").val();
        _objPedido.Total = $("#to_total").val();
        _objPedido.ToSubtotal_usd = $("#to_subtotal_usd").val();
        _objPedido.ToDescuento_usd = $("#to_descuento_usd").val();
        _objPedido.ToIVA_usd = $("#to_iva_usd").val();
        _objPedido.Total_usd = $("#to_total_usd").val();
        if (_pagare) {
            _objPedido.EsPagare = 1;
        }
        else {
            _objPedido.EsPagare = 0;
        }
        var myJSONText = JSON.stringify(_objPedido);
        var url = '/Pedido/Guarda/';
        $.ajax({
            url: url,
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                $("#framecanvas").attr("src", "/Pedido/Detalle?pdf=2&p=" + data);
                $("#titulocanvas").empty().append("Orden de compra");
                $('#repo').click();
                $("#edita").modal('hide');
                toastr.success('Hecho, el pedido se guardó correctamente');
                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Error, surgió un error, intente nuevamente más tarde');
            }
        });
    }
    else {
        toastr.error('Error, debe llenar los campos obligatorios');
    }
});