﻿var _pag = 1;
function LlenaGrid(pag) {
    AfterAjax();
    var objRoles = {};
    objRoles.pag = pag;
    objRoles.estatus = $("#buscastatus").val();
    objRoles.proveedor = $("#buscaproveedor").val();
    objRoles.pedido = $("#abuscar").val();
    var myJSONText = JSON.stringify(objRoles);
    $('#datos').dataTable({
        "processing": true,
        "responsive": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": "/Pedido/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "fnInitComplete": function (oSettings, json) {


        },
        "fnDrawCallback": function (oSettings) {

            AfterAjax();
        },
        "columns": [
        { "data": "NumeroPedido", "orderable": false },
            { "data": "Destino", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Total", "orderable": false },
            { "data": "Total_USD", "orderable": false },
            { "data": "Estatus", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    return '<button type="button" rel=' + full.Clave + ' class="btn ink-reaction btn-floating-action btn-xs btn-danger btn-xs pdf" data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"  > <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button><button class="btn ink-reaction btn-floating-action btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver orden"   onClick="Ver(\'' + full.Clave + '\')"  ><i class="fa fa-search" ></i></button>';
                }
            },

            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";
                    if (_principal == false) {
                        renglon = DameFacturacion(full.Clave, full.Estatus, full.FacturacionClave, full.EsPagare);
                    }
                    if (full.Estatus == 'Por autorizar' || full.Estatus == 'Por surtir' || full.Estatus == 'Rechazado' || full.Estatus == 'Autorizado (Por Surtir)' || full.Estatus == 'Por pagar') {

                        if (full.EntidadClaveSolicitante != '00000000-0000-0000-0000-000000000001' && full.EsPagare == "0") {
                            if (full.Estatus == 'Por autorizar' || full.Estatus == 'Por pagar') {
                                renglon += '<td><button type="button" class="btn btn-warning ink-reaction btn-xs edita" rel=' + full.Clave + '><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edita</button></td>';
                                renglon += '<td><button type="button" class="btn btn-danger ink-reaction btn-xs cancela" rel="' + full.Clave + '"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Cancela</button></td>';
                            }
                        }
                        else {
                            renglon += '<td><button type="button" class="btn btn-warning btn-xs" rel="' + full.Clave + '"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edita</button></td>';
                            renglon += '<td><button type="button" class="btn btn-danger ink-reaction btn-xs cancela" rel="' + full.Clave + '"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Cancela</button></td>';
                        }
                    }
                    return renglon;
                }
            }

        ],

        language: {
            processing: "",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "order": [[0, "asc"]]
    })
}