﻿$("#agregararticulo").click(function () {
    var valid = jQuery('#formaarticulo').validationEngine('validate');
    if (valid) {
        var objArticulo = {};
        objArticulo.ArticuloClasificacionClave = $("#ed_clasificacionmaterial").val();
        objArticulo.ArticuloTipoClave = $("#ed_tipomaterial").val();
        objArticulo.ArticuloClave = $("#ed_articulo").val();
        objArticulo.ArticuloTexto = $("#ed_articulo option:selected").text();
        objArticulo.Cantidad = $("#ed_cantidad").val();
        objArticulo.CantidadEntregada = 0;
        objArticulo.PrecioUnitario = $("#ed_precio").val().replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
        objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();
        if (_productoSeleccionado == -1) {
            _objPedido.Articulos.push(objArticulo);
        }
        else {
            _objPedido.Articulos[_productoSeleccionado] = objArticulo;
        }
        ActualizaLista();
        $('#articulo').modal('hide');
        Resumen();
    }
});

$("#add").click(function () {
    $('#selectServicio').attr('disabled', false);
    $('#ed_almacen').attr('disabled', false);
    if (_pagare) {
        $('#titulowin').html('Agregar Orden de Compra (Pagaré)');
        $("#c_importedolares").hide();
    }
    else {
        $('#titulowin').html('Agregar Orden de Compra (Venta)');
        $("#c_importedolares").show();
    }
    _objPedido = {};
    _objPedido.Articulos = new Array();
    _productoSeleccionado = -1;
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');
    $("#ed_proveedor").val('');
    $("#ed_almacen").val('');
    $("#ed_autorizador").val('');
    $("#ed_observaciones").val('');
    $("#ed_conceptooc1").val('');
    $("#ed_preciooc1").val('');
    $("#ed_monedaoc1").val('');
    $("#ed_conceptooc2").val('');
    $("#ed_preciooc2").val('');
    $("#ed_monedaoc2").val('');
    $("#ed_clasificacionmaterial").val('');
    $("#ed_tipomaterial").val('');
    $("#ed_articulo").val('');
    $("#ed_cantidad").val('');
    $("#ed_precio").val('');
    $("#ed_moneda").val('');
    ActualizaLista();
    $("#tt1").click();
});