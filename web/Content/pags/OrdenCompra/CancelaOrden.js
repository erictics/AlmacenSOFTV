﻿$("#formacancela").submit(function (e) {
    e.preventDefault();
    var url = '/Pedido/Cancela/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: $('#formacancela').serialize(),
        type: "POST",
        success: function (data) {
            $("#cancela").modal('hide');
            toastr.success('El pedido se canceló correctamente');
            LlenaGrid(_pag);
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});

$("#formaelimina").submit(function (e) {
    e.preventDefault();
    var url = '/Articulo/Elimina/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: $('#formaelimina').serialize(),
        type: "POST",
        success: function (data) {
            $("#elimina").modal('hide');
            toastr.success('El artículo se eliminó correctamente');
            LlenaGrid(_pag);
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});