﻿
var idEdiccion = null;
function Edita(id) {
    articulos = [];
    idEdiccion = id;
    DetalleDevolucion(id);
    $('#guardaTraspaso').hide();
    $('#EditaTraspaso').show();
    $('#almacen').attr('disabled', true);
    $('#almacenAfectado').attr('disabled', true);
}

function DetalleDevolucion(id) {
    
    $('#AgregaDevolucion').modal('show');
    var url = '/Traspaso/DetalleTraspaso/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idtraspaso': id },
        type: "POST",
        success: function (data) {
            console.log(data);
            $('#almacen').val(data.almacen);
            
            $('#motivo').val(data.motivo);
            $('#selectenvio').val(data.TipoTraspaso);
            if (data.TipoTraspaso == 1) {
                $('#mismodis').show();
                $('#difdis').hide();
                $('#selectenvio').attr('disabled', true);
                $('#almacenAfectado').attr('disabled', true);
                $('#almacenAfectado').val(data.almacenAfectado);
            } else {
                $('#mismodis').hide();
                $('#difdis').show();
                $('#distribuidor').val(data.distribuidor);
                $('#distribuidor').attr('disabled', true);
                $('#almacenAfectadodis').attr('disabled', true);
                $('#selectenvio').attr('disabled',true);
                $('#almacenAfectadodis').empty().append("<option value=''>selecciona</option>");
                var url = '/almacen/GetAlmacenByDistribuidor/';
                $.ajax({
                    url: url,
                    dataType: 'json',
                    data: { 'distribuidor': $('#distribuidor').val() },
                    type: "POST",
                    success: function (data_) {
                        console.log(data);
                        for (var i = 0; i < data_.length; i++) {
                            if (data_[i].IdAlmacen == data.Idalmacen) {
                                $('#almacenAfectadodis').append("<option selected value=" + data_[i].IdAlmacen + ">" + data_[i].Nombre + "</option>");
                            } else {
                                $('#almacenAfectadodis').append("<option value=" + data_[i].IdAlmacen + ">" + data_[i].Nombre + "</option>");
                            }
                           
                        }

                    },
                    error: function (request, error) {
                        toastr.error('Surgió un error, intente nuevamente más tarde');
                    }
                });


                
            }

            for (var a = 0; a < data.articulos.length; a++) {
                var Serie = {};
                Serie.InventarioClave = data.articulos[a].InventarioClave;
                Serie.catSerieClave = 2;
                Serie.Valor = data.articulos[a].Valor;
                Serie.ArticuloClave = data.articulos[a].ArticuloClave;
                Serie.Nombre = data.articulos[a].Nombre;
                Serie.Cantidad = data.articulos[a].Cantidad;
                Serie.status = data.articulos[a].status;
                Serie.Statustxt = "";
                articulos.push(Serie);
                ActualizaDetalle();
            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });



}


function EditaArticulo(idArticulo, cantidad) {
    $('#AgregaArticulo').modal('show');

    var url = '/Traspaso/DetalleArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idarticulo': idArticulo },
        type: "POST",
        success: function (data) {

            $('#clasificacion').val(data.idClasificacion);
            obtentipomat(data.idClasificacion, data.idTipoArticulo);
            var valor = AsignaArticulo(data.idArticulo, data.idTipoArticulo);

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

}

function AsignaArticulo(idArticulo, idTipoArticulo) {

    var a = "";
    $('#articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');

    var url = '/Traspaso/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idTipoArticulo },
        type: "POST",
        success: function (data) {

            for (var a = 0; a < data.length; a++) {
                if (data[a].ArticuloClave == idArticulo) {

                    $('#articulo').append('<option selected rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');

                    var b = (data[a].catSerieClaveInterface == null ? "0" : "1");
                    DetalleArticulo(b);
                } else {

                    $('#articulo').append('<option rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

    return a;

}


function cuantosdevolucion(idDev, idArticulo) {

    var url = '/Traspaso/cuantosdevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idDev': idDev, 'idArticulo': idArticulo },
        type: "POST",
        success: function (data) {
            $('#cantidad_anterior').val(data);
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}


$('#EditaDevolucion').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Alerta, no puede realizar una traspaso de material sin artículos");
    }

    else if (comment.length == 0) {
        toastr.warning("Alerta, no puede realizar una traspaso de material sin especificar el motivo de esta.");
    }
    else {


        var url = '/Traspaso/EditaTraspaso/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'motivo': $('#motivo').val(), 'almacenAfectado': $('#almacenAfectado').val(), 'articulos': JSON.stringify(articulos), 'Tras': idEdiccion },
            type: "POST",
            success: function (data) {
                if (data.idError == "-1") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else if (data.idError == "-2") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else if (data.idError == "-3") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else if (data.idError == "-4") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else if (data.idError == "-5") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else if (data.idError == "-6") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else if (data.idError == "-7") {
                    toastr.warning("Atención " + data.toastr.error);
                }
                else {


                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle traspaso de material");
                    $("#framecanvas").attr("src", "/Traspaso/Detalle?pdf=2&d=" + data);

                    //$("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);
                    toastr.success("Correcto, se ha generado correctamente");
                    // $('#ver').modal('show');

                    $("#AgregaDevolucion").modal("hide");

                    llenarLista();
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});


