﻿$('#add').click(function () {
	articulos = [];
	idEdiccion = null;
	$('#almacen').attr('disabled', false);
	$('#almacenAfectado').attr('disabled', false);
	$('#AgregaDevolucion').modal('show');
	$('#motivo').val('');
	$('#contenidoresumen').empty();
	//$('#home').click();
	$('#EditaDevolucion').hide();
	//$('#guardaTraspaso').show();
	$('#distribuidor').attr('disabled', false);
	$('#almacenAfectadodis').attr('disabled', false);
	$('#selectenvio').attr('disabled', false);
	$('#mismodis').show();
	$('#difdis').hide();
	$('#selectenvio').val(1);
});


$('#addArticulo').click(function () {
	resetear();
	$('#panelbusqueda_series').hide();
	$('#AgregaArticulo').modal('show');
	$('#cantidad_anterior').hide();
	$('#colcantant').hide();
});

$('#selectenvio').change(function () {
    
    if ($('#selectenvio').val() == 1) {
        $('#mismodis').show();
        $('#difdis').hide();
        

    } else {
        $('#mismodis').hide();
        $('#difdis').show();
    }

});

$('#distribuidor').change(function () {
    $('#almacenAfectadodis').empty().append("<option value=''>selecciona</option>");
    var url = '/almacen/GetAlmacenByDistribuidor/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'distribuidor': $('#distribuidor').val() },
        type: "POST",
        success: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                $('#almacenAfectadodis').append("<option value=" + data[i].IdAlmacen + ">" + data[i].Nombre + "</option>");
            }
            
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
    
});


$('#almacen').change(function () {
	articulos = [];
	idEdiccion = null;
	$('#contenidoresumen').empty();
	resetear();
});


$('#clasificacion').change(function () {

	$('#tipomat').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
	var url = '/Traspaso/GetTipoArticulo/';
	$.ajax({
		url: url,
		dataType: 'json',
		data: { 'id': $('#clasificacion').val() },
		type: "POST",
		success: function (data) {
			for (var a = 0; a < data.length; a++) {

				$('#tipomat').append("<option value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
			}
		},
		error: function (request, error) {
			toastr.error('Surgió un error, intente nuevamente más tarde');
		}
	});

});


$('#tipomat').change(function () {
	$('#guardaAccesorios').hide();
	$('#panelbusqueda_series').hide();
	$('#articulo').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
	var url = '/Traspaso/GetArticulos/';
	$.ajax({
		url: url,
		dataType: 'json',
		data: { 'id': $('#tipomat').val(), almacen: $('#almacen').val() },
		type: "POST",
		success: function (data) {
			for (var a = 0; a < data.length; a++) {
				$('#articulo').append("<option value=" + data[a].ArticuloClave + " rel=" + (data[a].catSerieClaveInterface == null ? "0" : "1") + ">" + data[a].Nombre + "</option>");

			}
		},
		error: function (request, error) {
			toastr.error('Surgió un error, intente nuevamente más tarde');
		}
	});
});




$('#serie').bind("enterKey", function (e) {

	if ($('#serie').val() == "") {
		$('#serie').val('');
		toastr.warning("Error, necesita ingresar una serie válida");
	} else {
		if ($('#statusaparato').val() != null) {
			validaserie($('#serie').val(), $('#articulo').val(), $('#almacen').val());
			$('#serie').val('');

		}
		else {
			toastr.error("Error, seleccione en que estado se encuentra el aparato");
		}
	}
});


$('#serie').keyup(function (e) {
	if (e.keyCode == 13) {
		$(this).trigger("enterKey");
	}
});


$('#guardaAccesorios').click(function () {

	GuardaAccesorios();
});

function obtentipomat(idclas, id) {

	$('#tipomat').empty().append('<option selected="selected" disabled value="x">Selecciona</option>');
	var url = '/Traspaso/GetTipoArticulo/';
	$.ajax({
		url: url,
		dataType: 'json',
		data: { 'id': idclas },
		type: "POST",
		success: function (data) {
			for (var a = 0; a < data.length; a++) {
				if (id == data[a].catTipoArticuloClave) {
					$('#tipomat').append("<option selected value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
				}
				else {
					$('#tipomat').append("<option value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
				}

			}
		},
		error: function (request, error) {
			toastr.error('Surgió un error, intente nuevamente más tarde');
		}
	});
}


//$('#contenidoT').on("click", ".eliminaserie", function () {

//    var id = $(this).attr('rel');
//    EliminarDeArreglo(articulos, "InventarioClave", id);
//    actualizaContador();
//    ActualizaTablaSeries();
//})
$(document).ready(function () {

    
    $('#difdis').hide();
});