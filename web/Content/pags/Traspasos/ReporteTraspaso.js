﻿$('#distribuidor').change(function () {
    var id=$('#distribuidor').val();
    if(id!= null||id!=""){
        $('#almacen').empty().append("<option value=''>Selecciona</option>");
        var url = '/Almacen/GetAlmacenByDistribuidor/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'distribuidor': id },
            type: "POST",
            success: function (data) {
                console.log(data);
                $('#almacen').append("<option value='0'>Todos los almacenes</option>");
                for (var r = 0; r < data.length; r++)
                    $('#almacen').append("<option value=" + data[r].IdAlmacen + ">" + data[r].Nombre + "</option>");
                console.log(data);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }
    
});

$('#clasificacion').change(function () {
    if ($('#clasificacion').val() == null || $('#clasificacion').val() == undefined || $('#clasificacion').val() == "") {
        $('#tipo').empty().append("<option value=''>selecciona</option>");
        return;
    }

    $('#tipo').empty();
    var url = '/Reportes/GetTipoArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': $('#clasificacion').val() },
        type: "POST",
        success: function (data) {
            $('#tipo').append("<option value='0'>Todos los tipos</option>");
            for (var r = 0; r < data.length; r++) {
                $('#tipo').append("<option value=" + data[r].catTipoArticuloClave + ">" + data[r].Descripcion + "</option>");;
            }

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

});

$('#tipo').change(function () {
    if ($('#tipo').val() == null || $('#tipo').val() == undefined) {
        $('#articulo').append("<option>Selecciona</option>");
        return;
    }

    var id = $('#tipo').val();
    var idservicio = $('#servicio').val();
    $('#articulo').empty();
    if ($('#tipo').val() == "0") {
        $('#articulo').append("<option value='0'>Todos los artículos</option>");
        return;
    }

    var url = '/Reportes/GetArticulosListByServicio/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': id, 'idservicio': idservicio },
        type: "POST",
        success: function (data) {
            if (data.length > 0) {
                for (var r = 0; r < data.length; r++) {
                    $('#articulo').append("<option value=" + data[r].Clave + ">" + data[r].Descripcion + "</option>");
                }
            } else {
                $('#articulo').append("<option>No asignado</option>");
            }

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
});

$('#pdf').click(function () {

    var valid = jQuery('#formReporte').validationEngine('validate');
    if (valid) {

        var fechainicio = $('#fechainicio').val();
        var fechafin = $('#fechafin').val();
        var servicio = $('#servicio').val();        
        var TipoArticulo = $('#tipo').val();
        var Articulo = $('#articulo').val();       
        //var Distribuidor = $('#distribuidor').val();
       // var clasificacion = $('#clasificacion').text();
        var tiporeporte = $('#tiporeporte').val();
        var Almacen = $('#almacen').val();

        window.location.href = '/ReportesGenerales/TraspasoPDF?fechainicio=' + fechainicio +
       '&fechaFin=' + fechafin +
       '&servicio=' + servicio +       
       '&TipoArticulo=' + TipoArticulo +
        '&Articulo=' + Articulo +       
        //'&Distribuidor=' + Distribuidor +
        //'&clasificacion=' + clasificacion +
        '&Almacen=' + Almacen +
           '&tiporeporte=' + tiporeporte;

    }

});

