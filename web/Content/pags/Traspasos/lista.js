﻿

llenarLista($("#falmacen").val(), $("#ord").val());


$('#datepicker1').datepicker(
    {
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true

    });




function llenarLista(almacen, orden) {

    var Numero;
    if (orden == "" || orden == undefined) {
        Numero = 0;
    } else {
        Numero = orden;
    }
    var Alm;
    if (almacen == "" || almacen == undefined) {
        Alm = 0;
    } else {
        Alm = almacen;
    }

    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/Traspaso/ListaTraspaso/",
            "type": "POST",
            "data": {
                'data': "1", 'almacen': Alm, 'orden': Numero
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "pedido", "orderable": false },
                 { "data": "fecha", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         console.log(full.estatus);
                         var str = "";
                         if (full.estatus == 1) { str = "Por Autorizar" }
                         else if (full.estatus == 4) { str = "Recibido en almacen destino" }
                         else if (full.estatus == 3) { str = " Recibido parcialmente en almacén destino" }
                         else { str = "Cancelada" }
                         return str;


                     }

                 },
                 { "data": "almacen", "orderable": false },
                 { "data": "almacenAfectado", "orderable": false },
                 {

                     sortable: false,
                     "render": function (data, type, full, meta) {

                         var str = "";
                         str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger btn-xs pdf" onClick="descargapdf(\'' + full.proceso + '\')"><i class="fa fa-download" aria-hidden="true" ></i></button>';
                         str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-success " onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search" aria-hidden="true" ></i></button>';

                        

                         return str;




                     }

                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         var str = "";
                        

                         if (full.estatus == 1) {

                             str += '<button class="btn btn-warning btn-xs" onclick="Edita(\'' + full.proceso + '\');"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button>';
                             str += '<button class="btn btn-danger btn-xs" onclick="Cancela(\'' + full.proceso + '\');"><i class="fa fa-trash-o" aria-hidden="true"></i> Cancelar</button>';

                             if (full.tiene_envio == 1) {
                                 //DetalleEnvio2
                                   var url = '/DevolucionAlmacenCentral/detalleEnvio/';
                                   str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-default-bright btn-xs" onclick="DetalleEnvio(\'' + full.proceso
                                     + '\');"><i class="fa fa-envelope-o" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="" data-original-title="Detalle de Envio" style="color:#2196f3"></i></buttton>';
                             } else {
                                 str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-default-bright btn-xs"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Envio" onclick="Envio(\'' + full.proceso + '\');"><i class="fa fa-envelope-o" aria-hidden="true" style="color:#2196f3"></i></buttton>';
                             }
                         }

                         return str;




                     }
                 }


        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}


function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle Devolución de material");
    $("#framecanvas").attr("src", "/Traspaso/Detalle?pdf=2&d=" + devolucion);

    //$("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + devolucion );   
    //$("#titulo_ver").html($("body div[class='page-title']").html().trim());    
    //$('#ver').modal('show');
};


function descargapdf(devolucion) {

    window.location.href = "/Traspaso/Detalle?pdf=1&d=" + devolucion;
};




function Envio(proceso) {
    $('#edProcesoEnvio').modal('show');
    $('#devEnv').val(proceso);
    $('#edPE_guia').val('');
    $('#edPE_transportista').val('');
    $('#edPE_fecha').val('');
}




function DetalleEnvio(proceso) {

    $('#edProcesoEnvio').modal('show');
    $('#devEnv').val(proceso);

    $('#edPE_guia').val('');
    $('#edPE_transportista').val('');
    $('#edPE_fecha').val('');

    var url = '/Traspaso/detalleEnvio/';
    $.ajax({
        url: url,
        data: { 'proceso': proceso },
        type: "POST",
        success: function (data) {

            $('#edPE_guia').val(data.guia);
            $('#edPE_transportista').val(data.tran);


            $('#edPE_fecha').val(data.fecha);
            //$("#edPE_fecha").datepicker("setDate", data.fecha);
           
            //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}


function DetalleEnvio2(proceso) {

    $('#DetalleEnvio').modal('show');
    $('#tbdet').empty(); //body tabla



    $('#guiaSel').val('');
    $('#transportistaSel').val('');
    $('#edPE_fecha').val('');
    //
    var url = '/DevolucionAlmacenCentral/detalleEnvio/';
    $.ajax({
        url: url,
        data: { 'proceso': proceso },
        type: "POST",
        success: function (data) {
            //$('#tbdet').append('<tr>'
            //    + '<td style="width: 40%"><b><h5><i class="fa fa-bus" aria-hidden="true"></i> Transportista:</h5></b></td>'+ '<td style="width: 40%">' + data.tran + '</td></tr>'
            //    + '<tr><td style="width: 40%"><h5><i class="fa fa-ticket" aria-hidden="true"></i><b> Guia: </b></h5></td>' + '<td style="width: 40%"> ' + data.guia + '</td></tr>'
            //    + '<tr><td style="width: 40%"><h5><i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha de envio:</h5></td>' + '<td td style="width: 40%">' + data.fecha + '</td></tr>')
            
            $('#guiaSel').val(data.guia);
            $('#transportistaSel').val(data.tran);
            //$('#fechaSel').val(data.fecha);
            $("#fechaSel").datepicker("setDate", data.fecha);
        
        
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}


//$('#add').click(function () {
//    articulos = [];
//    idEdiccion = null;
//    $('#AgregaDevolucion').modal('show');
//    $('#motivo').val('');
//    $('#contenidoresumen').empty();
//    //$('#home').click();
//    $('#EditaDevolucion').hide();
//    $('#guardaTraspaso').show();
//});










function Cancela(proceso) {


    $('#CancelaDev').modal('show');
    $('#id_procesoc').val(proceso);
}



$('#Cancelar-dev').click(function () {
    var proceso = $('#id_procesoc').val();
    var url = '/Traspaso/Cancela/';
    $.ajax({
        url: url,
        data: { 'proceso': proceso },
        type: "POST",
        success: function (data) {
            if (data == "-1") {
                toastr.warning("Error, el traspaso ya no se encuentra en el sistema, contacte al administrador del sistema");

            }
            else if (data == "-2") {
                toastr.warning("Error, el traspaso ya se encuentra con estatus de cancelado, probablemente otro usuario ya ejecutó la cancelacíón");
            }
            else if (data == "-3") {
                toastr.warning("Error, el traspaso ya se recepcionó en almacén principal, no puede ser cancelado");
            }

            else {
                // $("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);
                toastr.success("Correcto, el traspaso se ha cancelado correctamente");
                //$('#ver').modal('show');
                $('#CancelaDev').modal('hide');
                llenarLista();

            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});

$('#btn_PEguardar').click(function () {

    var d = $('#devEnv').val();

    var tra = $('#edPE_transportista').val();
    var guia = $('#edPE_guia').val();
    var fecha = $('#edPE_fecha').val();

    if (tra == -1) {
        toastr.warning("Alerta, debe elegir un transportista");
    }
    else if ($.trim(guia) == 0) {
        toastr.warning("Alerta, debe especificar el número de guía ");
    }
    else if ($.trim(fecha) == 0) {
        toastr.warning("Alerta, debe seleccionar una fecha ");
    }
    else {
        var url = '/Traspaso/Envio/';
        
        $.ajax({
            url: url,
            data: { 'proceso': d, 'trans': tra, 'guia': guia, 'fecha': fecha },
            type: "POST",
            success: function (data) {
                if (data == "1") {
                    $('#edProcesoEnvio').modal('hide');
                    llenarLista();
                    toastr.success("Se han agregado los datos de envio");
                }
                else {
                    toastr.warning("No se pudo ingresar los datos de envio");
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    }
});




$('#buscar').click(function () {

    var val = $('#ord').val();
    llenarLista("", val);


});




$('#falmacen').change(function () {

    llenarLista($("#falmacen").val(), $("#ord").val());
});

