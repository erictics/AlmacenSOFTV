﻿var articulos = [];



function DetalleArticulo(valor) {

    Obtenerinventario($('#articulo').val(), $('#almacen').val());

    if (valor == "1") {

        $('#cantidad').prop('readonly', true);
        DetalleSeries($('#articulo').val(), $('#almacen').val())
        $('#guardaAccesorios').hide();
        ActualizaTablaSeries();
        verTodosAparatos();
        $('#panelbusqueda_series').show();
    }
    else {
        //muestra series
        $('#PanelSeleccionSeries').hide();
        $('#cantidad').prop('readonly', false); //evitar que se cambie manualmente la cantidad
        $('#guardaAccesorios').show();
        $('#panelbusqueda_series').hide();
    }
}

function Obtenerinventario(idarticulo, almacen) {

    $('#cantidad').val(0);
    $('#cantidad_anterior').val(0);

    var url = '/Traspaso/getInventario/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idarticulo, 'almacen': almacen },
        type: "POST",
        success: function (data) {
            if (data.tieneseries == true) {
                $('#enalmacen').val(data.Cantidad);
                $('#InventarioClave').val(data.InventarioClave);
                $('#cantidad_anterior').hide();
                $('#colcantant').hide();
            } else {

                if (idEdiccion != null) {

                    $('#label-cantidad').empty().append('Cantidad nueva');
                    $('#InventarioClave').val(data.InventarioClave);
                    $('#enalmacen').val(data.Cantidad);
                    cuantosdevolucion(idEdiccion, $('#articulo').val());
                    $('#cantidad_anterior').show();
                    $('#colcantant').show();
                } else {

                    $('#label-cantidad').empty().append('Cantidad');
                    $('#InventarioClave').val(data.InventarioClave);
                    $('#enalmacen').val(data.Cantidad);
                    $('#cantidad_anterior').hide();
                    $('#colcantant').hide();
                }




            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function DetalleSeries(id, almacen) {
    $('#PanelSeleccionSeries').show();
}


function validaserie(serie, articulo, almacen) {
    var url = '/Traspaso/ValidaSerie/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie, 'articulo': articulo, 'almacen': almacen },
        type: "POST",
        success: function (data) {
            if (data == "0") {
                toastr.warning("Atención, la serie no existe en el inventario, no pertenece al artículo seleccionado o no esta disponible en este almacén");
            } else {
                var Serie = {};
                Serie.InventarioClave = data.InventarioClave;
                Serie.catSerieClave = data.catSerieClave;
                Serie.Valor = data.Valor;
                Serie.ArticuloClave = data.ArticuloClave;
                Serie.Nombre = data.Nombre;
                Serie.Cantidad = 1;
                Serie.status = $('#statusaparato').val();
                Serie.Statustxt = $("#statusaparato option:selected").text();
                if (Valida_Serie_arreglo(data.InventarioClave) == "1") {
                    toastr.warning("Error, la serie ya fue agregada a la orden");
                } else {
                    articulos.push(Serie);
                    actualizaContador();
                    ActualizaTablaSeries();
                    ActualizaDetalle();
                    verTodosAparatos();
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

}



function ActualizaTablaSeries() {
    actualizaContador();
    $('#contenidoT').empty();
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == $('#articulo').val(); });
    for (var a = 0; a < result.length; a++) {
        $('#contenidoT').append("<tr><td><button class='btn btn-info btn-xs'>" + result[a].Nombre + "</button></td><td><button class='btn btn-default btn-xs'>" + result[a].Valor + "</button></td><td><button class='btn btn-default btn-xs'>" + result[a].Statustxt + "</button></td><td><button class='btn btn-danger btn-xs eliminaserie' rel='" + result[a].InventarioClave + "'  >Eliminar</button></td></tr>")
    }

}


function EliminarDeArreglo(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}


function verTodosAparatos() {
    var iddev = 0;
    if (idEdiccion != null) {
        iddev = idEdiccion;
    }

    $('#datos').dataTable({

        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": "/Traspaso/Aparatos/",
            "type": "POST",
            "data": {
                'articulo': $('#articulo').val(),
                'plaza': $('#almacen').val(),
                'Articulos': JSON.stringify(articulos),
                'idDev': iddev
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         if (full.seleccionado == 1) {
                             return '<button class="btn btn-info btn-xs .selecciona" style=" width: 25px !important; height: 25px !important;padding:2px;"  onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';

                         } else {
                             return '<button class="btn btn-defaul btn-xs .selecciona" style="color:#FFFFFF;"   onClick="activa(\'' + full.ArticuloClave + '\',\'' + full.InventarioClave + '\',\'' + full.valor + '\',\'' + full.Nombre + '\',this)" ><i class="fa fa-check" aria-hidden="true"></i></button>';
                         }

                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         return "<button class='btn btn-default btn-xs'>" + full.valor + "</button>"
                     }
                 },
                  {

                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.estatus == 7) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' selected>Buen Estado</option> <option value='9'>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }
                              else {
                                  return "<select id='" + full.InventarioClave + "'  class='form-control'><option value='7' selected>Buen Estado</option> <option value='9'>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }

                          }
                          else if (full.estatus == 9) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' selected>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }
                              else {
                                  return "<select id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' selected>Revisión</option><option value='10'>Dañado</option><option value='11'>Baja</option></select>";
                              }


                          }
                          else if (full.estatus == 10) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' selected>Dañado</option><option value='11'>Baja</option></select>";
                              }
                              else {
                                  return "<select id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' selected>Dañado</option><option value='11'>Baja</option></select>";
                              }

                          }
                          else if (full.estatus == 11) {
                              if (full.seleccionado == 1) {
                                  return "<select disabled id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' >Dañado</option><option value='11' selected>Baja</option></select>";
                              }
                              else {
                                  return "<select id='" + full.InventarioClave + "'  class='form-control'><option value='7' >Buen Estado</option> <option value='9' >Revisión</option><option value='10' >Dañado</option><option value='11' selected>Baja</option></select>";
                              }

                          }

                      }
                  },
        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Aparatos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",

            zeroRecords: "No hay traspasos para mostrar",
            emptyTable: "No hay traspasos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

};



function activa(ArticuloClave, InventarioClave, Valor, Nombre, elem) {
    $(elem).css('width', '25px !important');
    $(elem).css('height', '25px !important;');
    $(elem).css('padding', '2px;');

    var Serie = {};
    Serie.InventarioClave = InventarioClave;
    Serie.catSerieClave = 2;
    Serie.Valor = Valor;
    Serie.ArticuloClave = ArticuloClave;
    Serie.Nombre = Nombre;
    Serie.Cantidad = 1;
    Serie.status = $("#" + InventarioClave + "").val();
    Serie.Statustxt = $("#" + InventarioClave + " option:selected").text();

    if (Valida_Serie_arreglo(InventarioClave) == "1") {
        EliminarDeArreglo(articulos, 'InventarioClave', InventarioClave);
        ActualizaTablaSeries();
        verTodosAparatos();
        actualizaContador();
        ActualizaDetalle();
    }
    else {
        document.getElementById(InventarioClave).setAttribute("disabled", "disabled");
        articulos.push(Serie);
        ActualizaTablaSeries();
        verTodosAparatos();
        actualizaContador();
        ActualizaDetalle();
    }
}


function actualizaContador() {
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == $('#articulo').val() });
    $('#cantidad').val(result.length);
}

function Valida_Serie_arreglo(idinventario) {
    var result = $.grep(articulos, function (e) { return e.InventarioClave == idinventario });
    if (result.length == 0) {
        return "0";
    } else {
        return "1";
    }
}

function ActualizaDetalle() {


    $('#contenidoresumen').html('');
    var linq = Enumerable.From(articulos);
    var result =
        linq.GroupBy(function (x) { return x.Nombre; })
            .Select(function (x) {
                return { Nombre: x.Key(), Clave: x.source[0].ArticuloClave, Cantidad: x.Sum(function (y) { return y.Cantidad | 0; }) };
            })
            .ToArray();


    if (result.length == 0) {

        $('#tablaresumen').hide();
        $('#msn-detalle').show();

    } else {
        $('#tablaresumen').show();
        $('#msn-detalle').hide();
        for (var a = 0; a < result.length; a++) {
            $('#contenidoresumen').append('<tr><td>' + result[a].Nombre + '</td><td>' + result[a].Cantidad + '</td><td><button class="btn btn-primary btn-xs" onClick="EditaArticulo(\'' + result[a].Clave
            + '\',\'' + result[a].Cantidad + '\')" rel=' + result[a].Clave + '><i class="fa fa-pencil" aria-hidden="true"></i>Edita</button>'
            + '<button class="btn btn-danger btn-xs" onClick="EliminaArticulo(\''
            + result[a].Clave + '\')" rel=' + result[a].Clave + '><i class="fa fa-trash" aria-hidden="true"></i>Elimina</button></td></tr>');
        }
    }



}

function resetear() {
    $('#PanelSeleccionSeries').hide();
    $('#enalmacen').val('0');
    $('#cantidad').val('0');
    $('#serie').val('');
    $('#articulo')
   .empty()
   .append('<option selected="selected" disabled value="x">Selecciona</option>')
    ;
    $('#tipomat')
   .empty()
   .append('<option selected="selected" disabled value="x">Selecciona</option>')
    ;
    $('#guardaAccesorios').hide();
    $('#clasificacion').val(0);

}

function EliminaArticulo(idarticulo) {
    EliminarDeArreglo(articulos, 'ArticuloClave', idarticulo);
    ActualizaDetalle();
}


function GuardaAccesorios() {
    if ($('#cantidad').val() <= 0) {
        toastr.warning("Error, necesita colocar una cantidad mayor a 0 para realizar el traspaso");
    }
    else if ($('#cantidad').val() % 1 != 0) {
        toastr.warning("Error, necesita colocar un número entero");
    }
    else if ($('#enalmacen').val() == 0) {
        toastr.warning("Error, este almacén no cuenta con piezas de este artículo para realizar el traspaso");
    }
    else if (idEdiccion == null && parseInt($('#enalmacen').val()) < parseInt($('#cantidad').val())) {
        toastr.warning("Error, la cantidad solicitada supera la cantidad en almacén");
    }
    else if (idEdiccion != null && parseInt($('#cantidad_anterior').val()) + parseInt($('#enalmacen').val()) < parseInt($('#cantidad').val())) {
        toastr.warning("Error, la cantidad solicitada es mayor a la cantidad anterior y la cantidad en almacén");
    }

    else {

        if (existe_en_arreglo($('#articulo').val()) == true) {
            EditaArreglo($('#articulo').val(), $('#cantidad').val());
            ActualizaDetalle();
            $('#AgregaArticulo').modal('hide');
        } else {
            var Serie = {};
            Serie.InventarioClave = $("#InventarioClave").val();
            Serie.catSerieClave = "";
            Serie.Valor = "";
            Serie.ArticuloClave = $('#articulo').val();
            Serie.Nombre = $("#articulo option:selected").text();
            Serie.Cantidad = $('#cantidad').val();
            Serie.status = 7;
            Serie.Statustxt = "";
            articulos.push(Serie);
            ActualizaDetalle();
            $('#AgregaArticulo').modal('hide');
        }
    }
};

function cantidadagregada(idarticulo) {
    var result = $.grep(articulos, function (e) { return e.ArticuloClave == idarticulo });
    var cantidad = 0;
    if (result.length == 0) {
        return 0;
    } else {
        for (var a = 0; a < result.length; a++) {
            cantidad = parseInt(cantidad) + parseInt(result[a].Cantidad);
        }
        return cantidad;
    }
}

function existe_en_arreglo(idarticulo) {
    var result = $.grep(articulos, function (obj) { return obj.ArticuloClave == idarticulo; });
    if (result.length == 0) {
        return false;
    } else {
        return true;
    }
}

function EditaArreglo(idArticulo, cantidad) {
    for (var i in articulos) {
        if (articulos[i].ArticuloClave == idArticulo) {
            articulos[i].Cantidad = cantidad;
        }
    }
}




$('#guardaTraspaso').click(function () {



    if ($('#selectenvio').val() == 1) {
        if ($("#almacen").val() == $("#almacenAfectado").val()) {
            toastr.warning("Atención, no puede realizar un traspaso al mismo almacén de origen");
            return;
        }

    } else {
        if ($('#almacenAfectadodis').val() == "" || $('#almacenAfectadodis').val() == null) {
            toastr.warning("Atención, necesita seleccionar un almacén");
            return;
        }

    }


    var comment = $.trim($('#motivo').val());
   
    if (comment.length == 0) {
        toastr.warning("Atención, no puede realizar un traspaso sin especificar el motivo de este.");
    }
    else if (articulos.length == 0) {
        toastr.warning("Atención, no puede realizar un traspaso sin artículos");
    }
    else {

        var url = '/Traspaso/GuardaTraspaso/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'almacen': $('#almacen').val(), 'almacenAfectado': $('#almacenAfectado').val(), 'almacenAfectadoDis': $('#almacenAfectadodis').val(), 'tipo': $('#selectenvio').val(), 'motivo': $('#motivo').val(), 'articulos': JSON.stringify(articulos) },
            type: "POST",
            success: function (data) {
                if (data == "-1") {
                    toastr.warning("Error, alguno de los aparatos no se encuentra en el almacén \n o ya fue recepcionado por el almacén principal \n contacta al administrador para mayor información");
                }
                else if (data == "-2") {
                    toastr.warning("Error, la cantidad solicitada excede el saldo en el inventario");
                }
                else if (data == "0") {
                    toastr.warning("Error, el sistema evitó que se generará el proceso. \n Contacta al administrador del sistema para mayor información ");
                }
                else {

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle traspaso de material");
                    $("#framecanvas").attr("src", "/Traspaso/Detalle?pdf=2&d=" + data);


                    toastr.success("Correcto, el traspaso se ha generado correctamente");
                    //$('#ver').modal('show');

                    $("#AgregaDevolucion").modal("hide");

                    llenarLista();
                }



            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});





