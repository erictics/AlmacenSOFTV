﻿//ed_asurtir

$(document).ready(function () {

    
    //Es el botón de Agregar Seriales
    $("#detalle").click(function () {


        if ($("#ed_asurtir").val() == '') {
            Mensaje("Recepción de Material", 'Debe escribir la cantidad a surtir');
            return;
        }


        var surtiendo = parseInt($("#ed_asurtir").val());
        var pendiente = parseInt($("#ed_pendiente").val());

        if (surtiendo > pendiente) {
            Mensaje("Recepción de Material", 'La cantidad surtida no puede ser mayor a la pendiente');
            return;
        }

        if (_articulospedidos != null && _articulospedidos != undefined) {
            _articulospedidos[_indiceArticulo].Surtiendo += parseInt(surtiendo);
        }
        else {

            var cadena = JSON.stringify(_objArticulos);
            //alert(cadena);

            //[_indiceArticulo].Surtiendo += parseInt(surtiendo);
        }

        $("#lasseries").show();
        IniciaDetalle(_conpedido);



        /***
        _objSurtiendo[_indiceArticulo] = parseInt(surtiendo);

        if (_articulospedidos != null && _articulospedidos != undefined) {
            var pendiente = parseInt(_articulospedidos[_indiceArticulo].Cantidad) - parseInt(_articulospedidos[_indiceArticulo].Surtida) - parseInt(_articulospedidos[_indiceArticulo].Surtiendo);
            $("#surtiendo_" + _indiceArticulo).html(_articulospedidos[_indiceArticulo].Surtiendo);
            $("#pendiente_" + _indiceArticulo).html(pendiente);
            var monto = _articulospedidos[_indiceArticulo].Surtiendo * _articulospedidos[_indiceArticulo].PrecioUnitario;
            $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
        }
        else {
            $("#surtiendo_" + _indiceArticulo).html(_objArticulos[_indiceArticulo].Surtiendo);
            var monto = _objArticulos[_indiceArticulo].Surtiendo * _objArticulos[_indiceArticulo].PrecioUnitario;
            $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
        }
        ***/

        var serialeslongitud = 0;

        try {

            for (var i = 0; i < _objRecepcion[_indiceArticulo].length; i++)
            {
                if (_objRecepcion[_indiceArticulo][i].Activo == 1) {
                    serialeslongitud++;
                }
            }

            //

        }
        catch (e) {
        }

        var quedan = -1;
        if (_conpedido) {
            quedan = parseInt(surtiendo) + parseInt(_objRecepcionInicial[_indiceArticulo].length) - parseInt(serialeslongitud);
        }
        else {
            quedan = parseInt(surtiendo) - parseInt(serialeslongitud);
        }



        $("#addart").val("Agregar (" + quedan + ")");
        //$("#addart").prop('disabled', 'disabled');


    });



});

function DetalleFuncion() {

}

var _serialesactual = 0;


function IniciaDetalle(conpedido) {


    var indice = _indiceArticulo;

    if (indice == -1) {
        indice = _objArticulos.length;
    }


    /*
    for (var i = _objRecepcion.length; i <= indice; i++) {
        _objRecepcion.push(null);
    }
    */


    _conpedido = conpedido;

    var latabla = "lasseries";
    if (conpedido == 1) {
        latabla = "lasseries";
    }
    else {
        latabla = "lasseriessp";
    }

    $("#" + latabla + " thead").empty();
    $("#" + latabla + " tbody").empty();

    var tiposeries = null;

    if (conpedido) {
        tiposeries = _articulospedidos[_indiceArticulo].catSeriesClaves;
    }
    else {
        tiposeries = eval($("#ed_articulosinpedido option:selected").attr("data-catseriesclaves"));
    }

    var tr = "";
    tr += "<tr>";
    tr += "<th>Estatus</th>";

    for (var ii = 0; ii < tiposeries.length; ii++) {
        tr += "<th>" + tiposeries[ii].Nombre + "</th>";
    }

    tr += "</tr>";

    $("#" + latabla + " thead").append(tr);

    var tbody = "";

    var asurtir = 0;

    try {
        if ($("#ed_asurtir").val() != '') {
            asurtir = parseInt($("#ed_asurtir").val());
        }
    }
    catch (e) {
    }

    //parseInt($("#ed_surtida").val()) + 

    var surtiendo = asurtir;
    var serialeslongitud = 0;

    try {


        for (var i = 0; i < _objRecepcion[indice].length; i++) {
            if (_objRecepcion[indice][i].Activo == 1) {
                serialeslongitud++;
            }
        }

    }
    catch (e) {
    }

    //alert(serialeslongitud);

    var quedan = -1;
    if (conpedido) {

        //alert(surtiendo + '...' + parseInt(_objRecepcionInicial[_indiceArticulo].length) + '...' + serialeslongitud);

        quedan = parseInt(surtiendo) + parseInt(_objRecepcionInicial[indice].length) - parseInt(serialeslongitud);
    }
    else {

        try {
            $("#ed_cantidad").val(_objRecepcion[indice].length);
        }
        catch (e) {
            $("#ed_cantidad").val(0);

        }
        //quedan = parseInt(surtiendo) - parseInt(serialeslongitud);
    }


    //alert(quedan + '...' + surtiendo + '...'  + serialeslongitud);

    tbody += "<tr>";

    tbody += "<td><select class='form-control validate[required]' id='estatusserial' ";


    if (conpedido == 1) {
        if (quedan == 0) {
            tbody += " disabled";
        }
    }

    tbody += "><option value=1>Buen Estado</option><option value=2>Dañado</option><option value=3>CRC</option><option value=4>Reparación</option></select></td>";
    for (var ii = 0; ii < tiposeries.length; ii++) {


        tbody += "<td><input type='text' class='form-control validate[required";

        if (tiposeries[ii].Hexadecimal == 'True') {
            tbody += ",custom[hexa]";
        }
        tbody += ",minSize[" + tiposeries[ii].Min + "]";
        tbody += ",maxSize[" + tiposeries[ii].Max + "]";

        tbody += "] seriales' id='s" + tiposeries[ii].Clave + "'";

        if (conpedido == 1) {
            if (quedan == 0) {
                tbody += " disabled";
            }
        }

        tbody += "></td>";
    }


    if (quedan < 0) {
        quedan = 0;
    }

    if (conpedido) {
        tbody += "<td><input type='button' class='form-control' id='addart' value='Agregar (" + quedan + ")' ";
        if (quedan == 0) {

            tbody += " disabled";
        }
    }
    else {
        tbody += "<td><input type='button' class='form-control' id='addartsp' value='Agregar' ";
    }

    tbody += "/></td>";
    tbody += "</tr>";

    var serialeslongitud = 0;
    var _serialesactual = 0;

    try {
        serialeslongitud = _objRecepcion[indice].length;
    }
    catch (e) {
    }

    for (var ji = 0; ji < serialeslongitud; ji++) {
        if (_objRecepcion[indice][ji].Activo != 0) {
            if ((_objRecepcion[indice][ji].InventarioClave == '' || _objRecepcion[indice][ji].InventarioClave == null || _objRecepcion[indice][ji].RecepcionClave == _recepcionClave)) {
                tbody += "<tr><td>" + _objRecepcion[indice][ji].EstatusTexto + "</td>";
                for (var ii = 0; ii < tiposeries.length; ii++) {
                    try {
                        tbody += "<td>" + _objRecepcion[indice][ji].datos[ii].Valor + "</td>";
                    }
                    catch (e)
                    { }
                }

                if ((_objRecepcion[indice][ji].InventarioClave != '' && _objRecepcion[indice][ji].InventarioClave != null && _objRecepcion[indice][ji].RecepcionClave != _recepcionClave)) {
                    tbody += "<td></td>";
                }
                else {
                    _serialesactual++;
                    tbody += "<td><button type='button' class='btn btn-danger btn-xs delserial' rel='" + ji + "'>&nbsp;Elimina</button></td>";
                }
                tbody += "</tr>";

            }
        }
    }

    if (conpedido == 1) {
        $("#lasseries tbody").append(tbody);
    }
    else {
        $("#lasseriessp tbody").append(tbody);
    }
    if (conpedido == 0) {
        $("#lasseriessp").show();
        //$("#estatusserial").show();
    }

    $(".seriales").first().focus();

    $(".delserial").click(function () {
        var indice = $(this).attr('rel');
        _objRecepcion[indice].splice(indice, 1);
        IniciaDetalle(true);
    });

    $("#addart").click(function () {

        var valid = jQuery('#formarecepcion').validationEngine('validate');
        if (valid) {

            if (conpedido) {
                $("#addart").prop('disabled', 'disabled');
            }

            var seriales = {};
            seriales.ArticuloClave = $("#ed_articulo").val();
            seriales.EstatusTexto = $("#estatusserial option:selected").text();
            seriales.EstatusClave = $("#estatusserial").val();
            seriales.Activo = 1;
            seriales.datos = new Array();


            $(".seriales").each(function () {
                var clave = $(this).attr("id");
                var valor = $(this).val();

                var serial = {};
                serial.Clave = clave;
                serial.Valor = valor;

                seriales.datos.push(serial)

            });

            ValidaSerie(seriales.datos[0].Valor);

            if (!vbExiste) {
                if (_objRecepcion[_indiceArticulo] != null) {
                    for (var foo = 0; foo < _objRecepcion[_indiceArticulo].length; foo++) {
                        if (_objRecepcion[_indiceArticulo][foo].datos[0].Valor == seriales.datos[0].Valor) {
                            vbExiste = true;
                            break;
                        }
                    }
                }
            }

            if (vbExiste) {
                $("#addart").removeAttr('disabled');
                Mensaje('Error', 'Serie (' + seriales.datos[0].Valor + ') previamente capturada');
                $("#s6").select();
                seriales = {};
                return;
            }

            if (_objRecepcion[_indiceArticulo] == null) {
                _objRecepcion[_indiceArticulo] = new Array();
            }


            if (_objRecepcion[_indiceArticulo].length > 0) {
                _objRecepcion[_indiceArticulo].splice(0, 0, seriales);
            }
            else {
                _objRecepcion[_indiceArticulo].push(seriales);
            }


            var nuevos = 0;
            for (var ji = 0; ji < _objRecepcion[_indiceArticulo].length; ji++) {
                if ((_objRecepcion[_indiceArticulo][ji].InventarioClave == '' || _objRecepcion[_indiceArticulo][ji].InventarioClave == null || _objRecepcion[_indiceArticulo][ji].RecepcionClave == _recepcionClave) && _objRecepcion[_indiceArticulo][ji].Activo == 1) {
                    nuevos++;
                }
            }

            var pendiente = 0;

            if (conpedido) {
                if (_articulospedidos != null && _articulospedidos != undefined) {

                    _articulospedidos[_indiceArticulo].Surtiendo = parseInt(nuevos);

                    pendiente = parseInt(_articulospedidos[_indiceArticulo].Cantidad) - parseInt(_articulospedidos[_indiceArticulo].Surtida) - parseInt(nuevos);
                    $("#surtiendo_" + _indiceArticulo).html(nuevos);
                    $("#pendiente_" + _indiceArticulo).html(pendiente);
                    var monto = nuevos * _articulospedidos[_indiceArticulo].PrecioUnitario;
                    $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
                }
                else {
                    pendiente = parseInt(_articulospedidos[_indiceArticulo].Cantidad) - parseInt(nuevos);

                    _objArticulos[_indiceArticulo].Surtiendo = parseInt(nuevos);

                    $("#surtiendo_" + _indiceArticulo).html(nuevos);
                    $("#pendiente_" + _indiceArticulo).html(pendiente);
                    var monto = nuevos * _objArticulos[_indiceArticulo].PrecioUnitario;
                    $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
                }
            }

            var cadena = JSON.stringify(_objRecepcion)
            $("#addart").removeAttr('disabled');

            $('input[type="button"]').each(function () {
                this.disabled = false;
            });

            IniciaDetalle(true);

        }

    });

    $("#addartsp").on("click", function () {

        var indice = _indiceArticulo;

        if (indice == -1) {
            indice = _objArticulos.length;
        }

        for (var i = _objRecepcion.length; i <= indice; i++) {
            _objRecepcion.push(null);
        }

        var valid = jQuery('#formaarticulo').validationEngine('validate');
        if (valid) {

            var seriales = {};
            seriales.ArticuloClave = $("#ed_articulosinpedido").val();
            seriales.EstatusTexto = $("#estatusserial option:selected").text();
            seriales.EstatusClave = $("#estatusserial").val();
            seriales.Activo = 1;
            seriales.datos = new Array();


            $(".seriales").each(function () {
                var clave = $(this).attr("id");
                var valor = $(this).val();

                var serial = {};
                serial.Clave = clave;
                serial.Valor = valor;

                seriales.datos.push(serial)

            });

            ValidaSerie(seriales.datos[0].Valor);

            if (!vbExiste) {
                if (_objRecepcion[indice] != null) {
                    for (var foo = 0; foo < _objRecepcion[indice].length; foo++) {
                        if (_objRecepcion[indice][foo].datos[0].Valor == seriales.datos[0].Valor) {
                            vbExiste = true;
                            break;
                        }
                    }
                }
            }

            if (vbExiste) {
                Mensaje('Error', 'Serie (' + seriales.datos[0].Valor + ') previamente capturada');
                $("#s6").select();
                seriales = {};
                return;
            }

            if (_objRecepcion[indice] == null) {
                _objRecepcion[indice] = new Array();
            }


            if (_objRecepcion[indice].length > 0) {
                _objRecepcion[indice].splice(0, 0, seriales);
            }
            else {
                _objRecepcion[indice].push(seriales);
            }


            var nuevos = 0;
            for (var ji = 0; ji < _objRecepcion[indice].length; ji++) {
                if ((_objRecepcion[indice][ji].InventarioClave == '' || _objRecepcion[indice][ji].InventarioClave == null || _objRecepcion[indice][ji].RecepcionClave == _recepcionClave) && _objRecepcion[indice][ji].Activo == 1) {
                    nuevos++;
                }
            }

            var pendiente = 0;

            var cadena = JSON.stringify(_objRecepcion)

            IniciaDetalle(false);

        }

    });

}

var vbExiste = false;

function ValidaSerie(serie) {
    var objSerieVal = {};
    objSerieVal.serie = serie;
    var myJSONText = JSON.stringify(objSerieVal);
    var url = '/Inventario/Serie/';
    $.ajax({
        async: false,
        cache: false,
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            vbExiste = eval(data);
        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}

function AfterPedido() {
    $(".recibegenerico").blur(function () {

        jQuery('#formaedita').validationEngine('validate');

        _indiceArticulo = $(this).attr('data-indice');

        var surtiendo = $(this).val();

        if (surtiendo == '') {
            return;
        }

        var pendiente = parseInt(_articulospedidos[_indiceArticulo].Cantidad - _articulospedidos[_indiceArticulo].Surtida);
        _articulospedidos[_indiceArticulo].Surtiendo = parseInt(surtiendo);

        pendiente = parseInt(_articulospedidos[_indiceArticulo].Cantidad) - parseInt(_articulospedidos[_indiceArticulo].Surtida) - parseInt(_articulospedidos[_indiceArticulo].Surtiendo);
        $("#ed_pendiente").val(pendiente)
        $("#surtiendo_" + _indiceArticulo).html(_articulospedidos[_indiceArticulo].Surtiendo);
        $("#pendiente_" + _indiceArticulo).html(pendiente);

        var monto = _articulospedidos[_indiceArticulo].Surtiendo * _articulospedidos[_indiceArticulo].PrecioUnitario;
        $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
        _objRecepcion[_indiceArticulo].Cantidad = surtiendo;

        $("#recibir").modal("hide");

    });

}

