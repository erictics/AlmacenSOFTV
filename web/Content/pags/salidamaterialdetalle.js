﻿var _Metodo = 0;
var nTotOrd = 0;
var nTotSur = 0;
var nPend = 0;
var idx_art = 0;
var idx_serie = 0;
var vbExiste = false;
var Tabla;

function ActRes() {
    $("#ed_cantidad").val(nTotSur);
}

function AfterInventarios()
{

}


function SalidaMaterialDetalle(cual)
{
    if ($("#ed_almacen").val() == '')
    {
        Mensaje('Error', 'Debe seleccionar un almacen para hacer la salida');
        return;
    }

    var elrel = cual;
    var objArticuloDetalle = {};
    objArticuloDetalle.Almacen = $("#ed_almacen").val();
    objArticuloDetalle.Articulo = cual;

    if (_salida != null) {
        objArticuloDetalle.Salida = _salida.Clave;
    }
    //_indiceArticulo = elrel;


    var myJSONText = JSON.stringify(objArticuloDetalle);

    //alert(myJSONText);
    var url = '/Inventario/Articulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            var arreglo = eval(data);
            _articulospedidos = arreglo;


                
            try
            {

                IniciaDetalle();

                IniciaContenido();
            }catch(e)
            {
                
            }
              
        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}

function IniciaDetalle() {

    $("#lasseries thead").empty();
    $("#lasseries tbody").empty();



    var tiposeries = _articulospedidos[0].catSeriesClaves;

    var tr = "";
    tr += "<tr>";
    tr += "<th>Seleccionar</th>";

    for (var ii = 0; ii < tiposeries.length; ii++) {
        tr += "<th>" + tiposeries[ii].Nombre + "</th>";
    }

    tr += "</tr>";

    $("#lasseries thead").append(tr);

    

}

function IniciaContenido() {

    var tbody = "";
    nTotSur = 0;

    for (var ii = 0; ii < _articulospedidos.length; ii++) {

        var cadena = '';

        tbody += "<tr data-rel='_remplazar_'>";
        tbody += "<td><input type='checkbox' id='cb" + _articulospedidos[ii].Clave + "' name='cb" + _articulospedidos[ii].Clave + "' rel='" + _articulospedidos[ii].Clave + "' class='inventarios' _checked_ /></td>";

        for (var ij = 0; ij < _articulospedidos[ii].Seriales[0].datos.length; ij++) {
            tbody += "<td>" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "</td>";

            cadena += "|" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "|";

        }
        
        tbody = tbody.replace("_remplazar_", cadena);

        if (_indiceArticulo != -1) {
            var array = objArticulos[_indiceArticulo].Inventario;
            var i = array.indexOf(_articulospedidos[ii].Clave);
            if (i != -1) {
                tbody = tbody.replace("_checked_", "checked");
                nTotSur++;
            }
            else {
                tbody = tbody.replace("_checked_", "");
            }
        }

        ActRes();

        tbody = tbody.replace("_checked_", "");

        tbody += "</tr>";
    }

    $("#lasseries tbody").append(tbody);

    $('input[type="checkbox"]:not(".switch")').iCheck({
        checkboxClass: 'icheckbox_square-blue ',
        increaseArea: '20%'
    });


    $('input[type="checkbox"]:not(".switch")').on('ifChecked', function (event) {

        var cantidad = nTotOrd;
        if (cantidad > nTotSur) {

            if (_indiceArticulo != -1) {
                objArticulos[_indiceArticulo].Inventario.push($(this).attr('rel'));
            }
            nTotSur++;
            nPend--;
            ActRes();
        }
        if (cantidad == nTotSur) {
            $("#_DetBuscar").prop("disabled", true);
            $(".inventarios:not(:checked)").prop("disabled", true);
        }
        else {
            $("#_DetBuscar").focus();
        }
    });

    $('input[type="checkbox"]:not(".switch")').on('ifUnchecked', function (event) {

        var cantidad = nTotOrd;
        if (_indiceArticulo != -1) {

            var array = objArticulos[_indiceArticulo].Inventario;
            var i = array.indexOf($(this).attr('rel'));
            if (i != -1) {
                objArticulos[_indiceArticulo].Inventario.splice(i, 1);
                nTotSur--;
                nPend++;
                ActRes();
            }
        } else {
            nTotSur--;
            nPend++;
            ActRes();
        }
        if (cantidad > nTotSur) {
            $("#_DetBuscar").removeAttr("disabled");
            $(".inventarios").removeAttr("disabled");
            $("#_DetBuscar").focus();
        }
    });


}


$(document).ready(function () {

    $("#_DetBuscar").bind("keypress keydown keyup", function (e) {
        var texto = $(this).val();
        if (e.which == 13) {
            e.preventDefault();

            if (texto.length == 0) {
                e.stopPropagation();
                return false;
            }

            var idx = 0;
            var chB;
            var vbEncontrado = false;
            var rows = $("#lasseries tbody tr");

            for (var foo = 0; foo < rows.length; foo++) {
                if ($(rows[foo]).attr("data-rel").indexOf(texto.toUpperCase()) > -1) {
                    chB = $("input[type='checkbox'][name*='cb']")[foo].id;
                    if ($("#" + chB).prop("checked")) {
                        $("#_DetBuscar").val("");
                        $("#_DetBuscar").focus();
                        return false;
                    }
                    nTotSur++;
                    nPend--;
                    $("#" + chB).prop("checked", "checked");
                    $("#" + chB).parent().addClass("checked");
                    $("#_DetBuscar").val("");

                    if (_indiceArticulo != -1) {
                        if (_Metodo != 2) {
                            objArticulos[_indiceArticulo].Inventario.push($("#" + chB).attr("rel"));
                        }
                        else {
                            objArticulos[_indiceArticulo].Inventario.push(new Array($(this).attr('rel'), "7", ''));
                        }
                    }
                    ActRes();

                    if (_Metodo == 2) {
                        $("#cbx" + $("#" + chB).attr('rel')).removeAttr("disabled");
                    }

                    if (nTotOrd == nTotSur) {
                        $(".inventarios:not(:checked)").prop("disabled", true);
                        $("#_DetBuscar").prop("disabled", true);
                    }
                    else {
                        $("#_DetBuscar").focus();
                    }

                    break;
                }
            }



                //$("#ed_cantidad").val($('input[type="checkbox"].inventarios:checked').length);

            e.stopPropagation();
            return false;
        }
    });


    $("#ed_buscar").keyup(function (e) {

        var texto = $(this).val();

        $("#lasseries tbody tr").each(function () {
            if ($(this).attr('data-rel').indexOf(texto) > -1)
            {
                $(this).show();
            }
            else
            {
                $(this).hide();
            }
        });

    });


});

