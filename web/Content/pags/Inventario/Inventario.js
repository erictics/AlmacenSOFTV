﻿
$(window).load(function (e) {
    obtenAlmacenes($('#Entidad').val());
});




$('#Entidad').change(function () {

    obtenAlmacenes($('#Entidad').val());
});


function obtenAlmacenes(id) {

  
    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "error": "Su sesión ha expirado,por favor vuelva a auticarse",
        "stateSave": false,
        "lengthMenu": [[10], [10]],
        "ajax": {
            "url": "/Inventario/ObtenAlmacenes/",
            "type": "POST",
            "data": { 'distribuidor': id },
        },
        "fnInitComplete": function (oSettings, json) {


        },
        "fnDrawCallback": function (oSettings) {


        },
        "columns": [
            { "data": "Nombre", "orderable": false },
            {
                
                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";
                    renglon += '<button class="btn btn-primary ink-reaction  btn-xs"   onClick="reporte(\'' + full.IdEntidad + '\')">inventario</button>'
                   
                    return renglon;
                    
                }

            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";
                    renglon += "<a class='btn ink-reaction btn-floating-action btn-xs btn-success' "
                        + "data-toggle='tooltip' data-placement='right' title='' data-original-title='Descargar XLS'"
                        + " href='/Inventario/getinventariotoexcel?blogPostId=" + full.IdEntidad + "'><i class='fa fa-file-excel-o' aria-hidden='true'></i></a>";
                   
                    return renglon;
                    
                }

            },
            
        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

function reporte(id) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Reporte de Inventario");
    $("#framecanvas").attr("src", "/Inventario/ReporteInventario?pdf=2&id=" + id);
}



//function VerInventario(id, nombre) {

//    $('#tbInventario').empty();

//    $('#tituloInv').empty().append("Inventario de "+nombre);
//    $('#ModalInventario').modal('show');

//    var url = '/Inventario/DetalleInventario/';
//    $.ajax({
//        url: url,
//        data: { 'entidad': id },
//        type: "POST",
//        success: function (data) {
//            for (var a = 0; a < data.length; a++) {
//                $('#tbInventario').append("<tr><td>" + data[a].Nombre + "</td><td>" + data[a].enAlmacen + "</td><td>" + data[a].enpedidos + "</td><td>" + data[a].standby + "</td><td>" + data[a].devolucion + "</td><td>" + data[a].revision + "</td><td>" + data[a].baja + "</td><td>" + data[a].dañados + "</td><td>" + data[a].reparacion + "</td><td>" + data[a].bajagarantia + "</td></tr>");
//            }

          

//        },
//        error: function (request, error) {
//            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
//        }
//    });
//}