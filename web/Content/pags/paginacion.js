﻿
var _totalpaginas = 0;
function LlenaPaginacion(cuantas) {

    _totalpaginas = cuantas;

    var activo = "";
    var html = "<div class='dataTables_paginate paging_simple_numbers' id='table_id_paginate'><ul class='pagination'>";
    //<li class='paginate_button previous' aria-controls='table_id' tabindex='0' id='table_id_previous'><a href='#'>Anterior</a></li>";

    for (var i = 1; i <= cuantas; i++)
    {
        activo = "";
        if (i == _pag)
        {
            activo = "active";
        }
        html += "<li class='paginate_button " + activo + "' rel='"+ i +"' aria-controls='table_id' tabindex='0'><a href='#'>" + i + "</a></li>";
    }

    //html += "<li class='paginate_button next' aria-controls='table_id' tabindex='0' id='table_id_next'><a href='#'>Siguiente</a></li>";
    html += "</ul></div>";

    $("#paginador").html(html);

    $(".paginate_button").click(function () {

        if (!$(this).hasClass('active')) {
            LlenaGrid($(this).attr('rel'));
        }
    });

    Pagina();
}

function Pagina()
{
    $("#table_id_next").removeClass("disabled");
    $("#table_id_previous").removeClass("disabled");

    if (_pag == 1) {
        $("#table_id_previous").addClass("disabled");
    }

    if (_totalpaginas == _pag) {
        $("#table_id_next").addClass("disabled");
    }
}

$(document).ready(function () {


});