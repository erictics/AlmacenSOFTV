﻿


$('#buscar').click(function () {
    if ($('#abuscar').val() == undefined || $('#abuscar').val() == null || $('#abuscar').val() == "") {
        toastr.warning('Coloque una serie correcta');
    } else {
        var url = '/AdministracionAparato/DetalleAparato/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'serie': $('#abuscar').val() },
            type: "POST",
            success: function (data) {
                if (data == "0" || data == "-1") {
                    $('#resultado').empty();
                    $('#resultado').append(' <div class="col-md-4 text-right" ><i class="md md-history" style="font-size:100px; color:#4CAF50"></i>' +
                '</div>  <div class="col-md-6 text-center" style="margin-top:50px;">' + '<h3 style="font-weight:bold;">No se encontró un equipo asignado a esa serie</h3>' +
                '</div>');

                } else {
                    $('#resultado').empty();
                    $('#resultado').append('<div class="col-md-4 text-right" style="margin-top:50px;">' +
                   '<i class="md md-settings-remote" style="font-size:150px; color:#3F51B5"></i></div>' +
                   '<div class="col-md-6 text-center" style="margin-top:50px;">' +
                   '<h1 style="font-weight:bold;">' + data.Nombre + '</h1>' +
                   '<h3 style="font-weight:bold;">' + data.Ubicacion + '</h3>' +
                   '<h4>' + data.serie + '</h4><h4>' + data.Status + '</h4>' +
                   '<button class="btn btn-success btn-sm ink-reaction" onClick="historial(\'' + data.idInventario + '\',\'' + data.serie + '\',\'' + data.Nombre + '\')" id="historial">Historial de aparato</button>' +
               '</div>');
                }
            },
            error: function (request, error) {
                $('#resultado').empty();
                $('#resultado').append(' <div class="col-md-4 text-right" ><i class="md md-history" style="font-size:100px; color:#607d8b"></i>' +

            '</div>  <div class="col-md-6 text-center" style="margin-top:50px;">' + '<h3 style="font-weight:bold;">No se encontró un equipo asignado a esa serie </h3>' +

            '</div>');

                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});

function historial(id, serie, nombre) {
    $('#historialap').modal('show');
    $('#titulowin').empty().append("Movimientos de " + nombre + " Serie:" + serie);
    var url = '/AdministracionAparato/HistorialAparato/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idInventario': id },
        type: "POST",
        success: function (data) {
            console.log(data);
            $('#thistorial').empty();
            for (var a = 0; a < data.length; a++) {
                $('#thistorial').append("<tr><td style='font-size:10px;'><b>" + data[a].TipoProceso + "</b></td><td style='font-size:10px;' >" + data[a].Orden + "</td><td style='font-size:10px;'>" + data[a].Usuario + "</td><td style='font-size:10px;'>" + data[a].Fecha2 + "</td><td style='font-size:10px;'>" + data[a].Hora + "</td><td style='font-size:10px;'>" + data[a].Observaciones + "</td></tr>");
            }
        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}

function validar() {
    var serie = $('#serieError').val();
    var url = '/AdministracionAparato/DetalleAparato/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie },
        type: "POST",
        success: function (data) {
            if (data == "0" || data == "-1") {
                $('#serieCorrecta').hide();
                $('#cambiar').hide();
                $('#resultado2').empty();
                $('#resultado2').append(' <div class="col-md-4 text-right" ><i class="md md-cast-connected" style="font-size:100px; color:#607d8b"></i>' +

            '</div>  <div class="col-md-6 text-center">' + '<h5 style="font-weight:bold;">No se encontró el  aparato </h5>' +

            '</div>');

            } else {
                $('#serieCorrecta').show();
                $('#cambiar').show();

                $('#resultado2').empty();
                $('#resultado2').append('<div class="col-md-4 text-right" >' +
               '<i class="md md-settings-remote" style="font-size:100px; color:#3F51B5"></i></div>' +
               '<div class="col-md-6 text-center">' +
               '<h5 style="font-weight:bold;">' + data.Nombre + '</h5>' +
               '<h5 style="font-weight:bold;">' + data.Ubicacion + '</h5>' +
               '<h5>' + data.serie + '</h4><h4>' + data.Status + '</h5>' +
               '<button class="btn btn-success btn-sm ink-reaction" onClick="historial(\'' + data.idInventario + '\',\'' + data.serie + '\',\'' + data.Nombre + '\')" id="historial">Historial de aparato</button>' +
           '</div>');
            }
        },
        error: function (request, error) {
            $('#resultado2').empty();
            $('#resultado2').append(' <div class="col-md-4 text-right" ><i class="md cast-connected" style="font-size:100px; color:#607d8b"></i>' +

        '</div>  <div class="col-md-6 text-center" >' + '<h5 style="font-weight:bold;">No se ha encontró historial reciente del aparato </h5>' +

        '</div>');

            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}

function validarSerie() {
    var serie = $('#seriestatus').val();
    var url = '/AdministracionAparato/DetalleAparato/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie },
        type: "POST",
        success: function (data) {
            if (data == "0" || data == "-1") {
                $('#estatusserie').hide();
                $('#cambiarstatus').hide();
                $('#resultado3').empty();
                $('#resultado3').append(' <div class="col-md-4 text-right" ><i class="md md-cast-connected" style="font-size:100px; color:#607d8b"></i>' +
            '</div>  <div class="col-md-6 text-center">' + '<h5 style="font-weight:bold;">No se encontró el  aparato </h5>' +
            '</div>');

            } else {
                $('#estatusserie').show();
                $('#cambiarstatus').show();

                $('#resultado3').empty();
                $('#resultado3').append('<div class="col-md-4 text-right">' +
               '<i class="md md-settings-remote" style="font-size:100px; color:#3F51B5"></i></div>' +
               '<div class="col-md-6 text-center">' +
               '<h4 style="font-weight:bold;">' + data.Nombre + '</h4>' +
               '<h5 style="font-weight:bold;">' + data.Ubicacion + '</h5>' +
               '<h5>' + data.serie + '</h4><h4>' + data.Status + '</h5>' +
               '<button class="btn btn-success btn-sm ink-reaction" onClick="historial(\'' + data.idInventario + '\',\'' + data.serie + '\',\'' + data.Nombre + '\')" id="historial">Historial de aparato</button>' +
           '</div>');
            }
        },
        error: function (request, error) {
            $('#resultado3').empty();
            $('#resultado3').append(' <div class="col-md-4 text-right" ><i class="md cast-connected" style="font-size:150px; color:#607d8b"></i>' +

        '</div>  <div class="col-md-6 text-center" style="margin-top:50px;">' + '<h1 style="font-weight:bold;">No se ha encontró historial reciente del aparato </h1>' +

        '</div>');

            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}

function limpiar() {
    $('#seriestatus').val('');
    $('#serieError').val('');
    $('#estatusserie').hide();
    $('#cambiarstatus').hide();
    $('#resultado3').empty();
    $('#resultado3').append(' <div class="col-md-4 text-right" ><i class="md md-wifi-tethering" style="font-size:100px; color:#ff9800"></i>' +
'</div>  <div class="col-md-6 text-center" >' + '<h5 style="font-weight:bold;">Cambio de status</h5>' +
'</div>');

    $('#serieCorrecta').hide();
    $('#cambiar').hide();
    $('#resultado2').empty();
    $('#resultado2').append(' <div class="col-md-4 text-right" ><i class="md md-cast-connected" style="font-size:100px; color:#607d8b"></i>' +

'</div>  <div class="col-md-6 text-center" >' + '<h5 style="font-weight:bold;">Cambio de serie </h5>' +

'</div>');
}


$('#cambiarstatus').click(function () {
    
    var serie = $('#seriestatus').val();
    var status = $('#estatusserie').val();
    var url = "/AdministracionAparato/CambioEstatus/";
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serie': serie, 'status': status },
        type: "POST",
        success: function (data) {
            if (data == "0") {
                toastr.error("No se pudo procesar la petición. intente de nuevo");
            }
            if (data == "-1") {
                toastr.warning("La serie que trata de modificar debe encontrarse en buen estado para cambiar su estatus");
            }
            else if (data == "-2") {
                toastr.warning("La serie que trata de modificar no se encontro en el sistema");
                limpiar();
            }
            else if (data == "-3") {
                toastr.warning("Tu usuario no tiene permisos para realizar este proceso ");
                limpiar();
            }
            else if (data == "-4") {
                toastr.warning("No puedes realizar un cambio de status directamente a un aparato que este en un distribuidor, realiza los procesos correspondientes o contacta a tu administrador");
                limpiar();
            }
            else if (data == "-5") {
                toastr.warning("El equipo se encuentra en el almacen central con un status diferente a buen estado  ");
                limpiar();
            }
            else if (data == "-6") {
                toastr.warning("El cambio no puede ser procesado, el aparato esta en proceso de envio");
                limpiar();
            }
            else if (data == "-7") {
                toastr.warning("El cambio no puede ser procesado, el aparato esta en estatus de baja");
                limpiar();
            }
            else if (data == "-8") {
                toastr.warning("El cambio no puede ser procesado, el aparato esta instalado");
                limpiar();
            }
            else {
                $('#ModalcambioStatus').modal('hide');
                toastr.success("Se ha cambiado el status del equipo correctamente");
                limpiar();
                $('#repo').click();
                $('#titulocanvas').empty().append("Detalle cambio de serie");
                $("#framecanvas").attr("src", "/AdministracionAparato/Detalle?pdf=2&d=" + data);
            }
        },
        error: function (request, error) {

            toastr.error("hubo un error de conexión, su sesión ha expirado");
        }
    });


});

function verOrden(id) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle de orden");
    $("#framecanvas").attr("src", "/AdministracionAparato/Detalle?pdf=2&d=" + id);
}



function buscaproceso() {
    ObtenerLista($('#bproceso').val(), $('#orden').val());
}

function cambiostatus() {
    limpiar();
    $('#ModalcambioStatus').modal('show');
}

function cambioSerie() {
    limpiar();
    $('#ModalcambioSerie').modal('show');
}

function ObtenerLista(tipo,orden) {
    console.log(tipo, orden);
    var tipo_ = (tipo == undefined || tipo == null||tipo=="") ? 19 : tipo;
    var orden_ = (orden == undefined || orden == null || orden == "") ? 0 : orden;
    console.log(tipo_, orden_);
    var data = { 'tipo': tipo_, 'orden': orden_ }
    var url = "/AdministracionAparato/lista/";
    var columnas = [
                 { "data": "numeropedido", "orderable": false },
                 { "data": "fechacorta", "orderable": false },
                 { "data": "usuario", "orderable": false },
                 { "data": "nombreproceso", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         
                         return '<button class="btn btn-primary btn-xs" onclick="verOrden(\'' + full.procesoclave + '\')"><i class="fa fa-search"></i> Ver orden</button>';
                     }
                 }];

    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[8], [8]],
        "ajax": {
            "url": url,
            "type": "POST",
            "data": data
        },
        "columns": columnas,
        language: lenguaje,
        "order": [[0, "asc"]]
    })

}





$('#cambiar').click(function () {

    var serieError = $('#serieError').val();
    var serieCorrecta = $('#serieCorrecta').val();

    if (serieCorrecta == null || serieCorrecta == undefined) {
        toastr.error("Coloque la serie a reemplazar");
        return;
    }

    var url = "/AdministracionAparato/CambioSerie/";
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'serieError': serieError, 'serieCorrecta': serieCorrecta },
        type: "POST",
        success: function (data) {
            if (data == "-1") {
                limpiar();
                toastr.warning("La serie que trata de modificar no existe");
            } else if (data == "-2") {
                limpiar();
                toastr.warning("La nueva serie no puede cambiar correctamente, ya que el minimo o el max de caracteres no es correcto");
            }
            else if (data == "-3") {
                limpiar();
                toastr.warning("La series son identícas no se puede realizar el cambio");
            }
            else if (data == "-4") {
                limpiar();
                toastr.warning("La serie de reemplazo ya se encuentra en el sistema, coloque otra para continuar");
            }
            else if (data == "-6") {
                limpiar();
                toastr.error("El cambio no puede ser procesado, el aparato esta instalado");
            }
            else {
                $('#ModalcambioSerie').modal('hide');
                toastr.success("La serie del aparato se modificó correctamente");
                $('#repo').click();
                $('#titulocanvas').empty().append("Detalle Cambio de status");
                $("#framecanvas").attr("src", "/AdministracionAparato/Detalle?pdf=2&d=" + data);
            }

        },
        error: function (request, error) {
        }

    });


});





ObtenerLista($('#bproceso').val(), $('#orden').val());
