﻿$(window).load(function (e) {
    obtenAlmacenes($('#Entidad').val());
});




$('#Entidad').change(function () {

    obtenAlmacenes($('#Entidad').val());
});


function obtenAlmacenes(id) {
    
    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "error": "Su sesión ha expirado,por favor vuelva a auticarse",
        "stateSave": false,
        "lengthMenu": [[10], [10]],
        "ajax": {
            "url": "/Inventario/ObtenAlmacenes/",
            "type": "POST",
            "data": { 'distribuidor': id },
        },
        "fnInitComplete": function (oSettings, json) {


        },
        "fnDrawCallback": function (oSettings) {


        },
        "columns": [
            { "data": "Nombre", "orderable": false },
            {

                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";
                    renglon += '<button class="btn btn-primary ink-reaction  btn-xs"   onClick="reporte(\'' + full.IdEntidad + '\')">Detalle Bajas existencias</button>'

                    return renglon;

                }

            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";

                    //renglon += "<button class='btn ink-reaction btn-floating-action btn-xs btn-success' "
                    //    + " data-toggle='tooltip' data-placement='right' title='' data-original-title='Descargar excel' "
                    //+ " href='/Inventario/getinventariotoexcel?blogPostId=" + full.IdEntidad + "'><i class='fa fa-file-excel-o' aria-hidden='true'></i></button>"

                    //renglon += '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger" '
                    //+ ' data-toggle="tooltip" data-placement="right" title="" data-original-title="Descargar PDF"'
                    //+ '  onClick="VerReporte(\'' + full.IdEntidad + '\')" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>'
                    return renglon;

                }

            },

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })




}

//function VerReporte(id) {


//    window.location.href = "/Reportes/BajasExistenciasDetalle?pdf=2&d=" + id;
//    //$("#eliframe").attr("src", "/Reportes/BajasExistenciasDetalle?pdf=2&d=" + id);
//    //$('#ver').modal('show');
//}


function reporte(id) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Reporte de Inventario");
    $("#framecanvas").attr("src", "/Reportes/BajasExistenciasDetalle?pdf=2&d=" + id);
}




//function VerBajasExistencias(id, nombre) {

//    $('#tbInventario').empty();

//    $('#tituloInv').empty().append("Bajas existencias de " + nombre);
//    $('#ModalInventario').modal('show');

//    var url = '/Reportes/DetallebajasExistencias/';
//    $.ajax({
//        url: url,
//        data: { 'Almacen': id },
//        type: "POST",
//        success: function (data) {
//            for (var a = 0; a < data.length; a++) {
//                $('#tbInventario').append("<tr><td>" + data[a].Clasificacion + "</td><td>" + data[a].Nombre + "</td><td><button class='btn btn-danger btn-xs '>" + data[a].cantidad + "</button></td><td><button class='btn btn-default-bright btn-xs '>" + data[a].TopeMinimo + "</button></td><td></td></tr>");
//            }
//        },
//        error: function (request, error) {
//            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
//        }
//    });

//}