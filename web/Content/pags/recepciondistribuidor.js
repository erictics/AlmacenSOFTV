﻿var objArticulos = new Array();
var _articulospedidos = null;
var _indiceArticulo = -1;
var _recepcion = "";

$(document).ready(function () {

    jQuery('#formaedita').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false
    });

    $(".btn-success").click(function () {
        $("#acepta_rechaza").val('1');
        var url = '/SalidaMaterial/Recepcion/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formarecepciondetalle').serialize(),
            type: "POST",
            success: function (data) {
                $('#ver').modal('hide');
                $("#elimina").modal('hide');
                toastr.success('Hecho, la recepción se realizó correctamente');               
                LlenaGrid(_pag);
            },
            error: function (request, error) {
               
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });

    //$(".btn-danger").click(function () {
    //    $("#acepta_rechaza").val('0');
    //    var url = '/SalidaMaterial/Recepcion/';
    //    $.ajax({
    //        url: url,
    //        dataType: 'json',
    //        data: $('#formarecepciondetalle').serialize(),
    //        type: "POST",
    //        success: function (data) {
    //            $('#ver').modal('hide');
    //            $("#elimina").modal('hide');

    //            toastr.success('La recepción se rechazó correctamente')
               
    //            LlenaGrid(_pag);
    //        },
    //        error: function (request, error) {
    //            toastr.error('Error, ha ocurrido un error de conexión')
    //        }
    //    });
    //});

    $("#busca_estatus,#busca_almacen").change(function () {
        LlenaGrid(1);
    });

    LlenaGrid(1);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $("#titulowin").html("Agregar Salida");
        objArticulos = new Array();
        $('#edita').modal('show');
        $("#ed_id").val('');
        $("#ed_nombre").val('');
        $("#ed_existencias").val('');
        $("#ed_tope").val('');
        $("#ed_unidad").val('');
        $("#ed_porcentaje").val('');
        $("#ed_unidad").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_clasificacionmaterial").val('');
        $("#ed_proveedor1").val('');
        $("#ed_precio1").val('');
        $("#ed_moneda1").val('');
        $("#ed_proveedor2").val('');
        $("#ed_precio2").val('');
        $("#ed_moneda2").val('');
        $("#ed_proveedor3").val('');
        $("#ed_precio3").val('');
        $("#ed_moneda3").val('');
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();
        $("#loader").show();
        var url = '/Articulo/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {
                $("#elimina").modal('hide');
                toastr.success('El artículo se eliminó correctamente')
                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Ha ocurrido un error de conexión')
            }
        });
    });

    $("#agregararticulo").click(function () {
        var valid = jQuery('#formaarticulo').validationEngine('validate');
        if (valid) {
            $("#loader").show();
            var objArticulo = {};
            objArticulo.ArticuloClave = $("#ed_articulo").val();
            objArticulo.ArticuloTexto = $("#ed_articulo option:selected").text();
            objArticulo.tieneseries = $("#ed_articulo option:selected").attr('data-rel');
            objArticulo.Cantidad = $("#ed_cantidad").val();
            objArticulo.PrecioUnitario = $("#ed_precio").val();
            objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
            objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();
            objArticulo.Inventario = new Array();
            objArticulos.push(objArticulo);
            ActualizaLista();
            $('#articulo').modal('hide');
        }
    });

    $("#formaedita").submit(function (e) {
        e.preventDefault();
        GuardaInfo();
    });

    $("#ed_tipomaterial").change(function () {


        var tipo = $(this).val();

        var objRoles = {};
        objRoles.tipo = tipo;

        $("#ed_articulo").empty();
        $("#ed_articulo").append("<option value=''>Seleccione</option>");

        var myJSONText = JSON.stringify(objRoles);
        var url = '/Articulo/ObtenPorTipo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                var arreglo = eval(data);
                for (var i = 0; i < arreglo.length; i++) {
                    $("#ed_articulo").append("<option value='" + arreglo[i].Clave + "' data-rel='" + arreglo[i].tieneseries + "'>" + arreglo[i].Descripcion + "</option>");
                }
            },
            error: function (request, error) {
                toastr.error('Ha ocurrido un error de conexión')
            }
        });
    });

    $("#recepdetalle").on("hidden.bs.modal", function () {
        _recepcion = "";
    });

    $("#btnOkConfirm").click(function () {
        $("#Confirma").modal("hide");

        var objParametros = {};
        objParametros.proceso = _recepcion;

        var myJSONText = JSON.stringify(objParametros);
        $("#loader").show();

        var url = '/RecepcionDistribuidor/Cancela/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                $("#loader").hide();
                $('#recepdetalle').modal('hide');
                LlenaGrid(1);

                toastr.success('Recepción correctamente cancelada')
             
            },
            error: function (request, error) {
                toastr.error('Ha ocurrido un error de conexión')
            }
        });
    });

});

function Resumen() {

    var suma = 0;
    var tipocambio = 1;

    for (var i = 0; i < objArticulos.length; i++) {

        if(objArticulos[i].MonedaTexto.indexOf('Dólar')>-1)
        {
            tipocambio = parseFloat($("#ed_tipocambio").val());
        }

        var totalproducto = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);
            totalproducto = totalproducto * tipocambio;
            suma += totalproducto;
    }

    if ($("#ed_preciooc1").val() != '') {
        suma += parseFloat($("#ed_preciooc1").val());
    }

    if ($("#ed_preciooc2").val() != '') {
        suma += parseFloat($("#ed_preciooc2").val());
    }


    var eliva = parseFloat($("#ed_iva").val());

    var descuento = 0;

    var descuento_po = 0;
    try
    {
        if ($("#ed_descuento").val() != "") {
            descuento_po = parseFloat($("#ed_descuento").val());
            descuento = descuento_po * suma / 100;
        }
    }
    catch (e)
    { }

    var subtotaldescuento = suma - descuento;
    var iva = (subtotaldescuento * eliva / 100);
    var total = subtotaldescuento + (subtotaldescuento * eliva / 100);


    $("#to_subtotal").val(suma);
    $("#to_descuento").val(descuento);
    $("#to_iva").val(iva);
    $("#to_total").val(total);


    $("#reiva_po").html("(" + $("#ed_iva").val() + "%" + ")");
    $("#reiva").html(ToMoneda(iva) + "%");

    $("#resubtotal").html(ToMoneda(suma));
    $("#redescuento").html(ToMoneda(descuento));
    $("#redescuento_po").html("(" + descuento_po + "%"+")");
    $("#retotal").html(ToMoneda(total));

}

function ActualizaLista()
{
    $("#articulostable tbody").empty();

    for (var i = 0; i < objArticulos.length; i++) {

        var total = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);

        var renglon = "<tr><td>" + objArticulos[i].ArticuloTexto + "</td><td>" + objArticulos[i].Cantidad + "</td><td>" + DameAccionesArticulo(i, objArticulos[i].tieneseries) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }

    Resumen();
    AfterAjax();
}

var _pag = 1;


function viewrecepcion(id) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción");
    $("#framecanvas").attr("src", "/RecepcionDistribuidor/Detalle?pdf=2&s=" + id);
}



function LlenaGrid(pag)
{
    _pag = pag;
    var objRoles = {};
    objRoles.pag = pag;
    objRoles.tipoentidad = 1;
    objRoles.almacen = $("#busca_almacen").val();
    objRoles.estatus = $("#busca_estatus").val();

    var myJSONText = JSON.stringify(objRoles);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/SalidaMaterial/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "fnInitComplete": function (oSettings, json) {

           
        },
        "fnDrawCallback": function (oSettings) {

            AfterAjax();
        },

        "columns": [
            { "data": "NumeroPedido", "orderable": true },
            { "data": "Pedido", "orderable": false },
            { "data": "Fecha", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {

                    return full.Estatus.replace('Por autorizar', 'Por recibir');
                 }
            },

                { "data": "Destino", "orderable": false },


            {
                sortable: false,
                "render": function (data, type, full, meta) {

                    return DameOpciones(full.Clave, full.Estatus.substr(0, 3), (full.DiasCredito == "1") ? true : false)

                    


                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })






}






function AfterAjax() {

    //SalidaMaterialDetalle();

    $(".verdatosenv").click(function () {
        var id = $(this).attr("rel");
        var ds = $(this).attr("ds");

        Envio(id, ds);
    });

    $(".verrecep").click(function () {
        var cual = $(this).attr("rel");
        var st = $(this).attr("st");
        var stR = "";
        $('#recepdetalle').modal('show');

        var objRoles = {};
        objRoles.pag = 1;
        objRoles.pedido = cual;
        objRoles.tipoentidad = 1;

        var myJSONText = JSON.stringify(objRoles);

        var url = '/RecepcionDistribuidor/ObtenLista/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                $("#lassalidas tbody").empty();
                var arreglo = eval(data);
                var tbody = '';
                for (var i = 0; i < arreglo.length; i++) {
                    stR = arreglo[i].Estatus.substr(0, 3);
                    tbody += "<tr>";
                    tbody += "<td>" + arreglo[i].NumeroPedido + "</td>";
                    tbody += "<td>" + arreglo[i].Fecha + "</td>";
                    tbody += "<td>" + arreglo[i].Estatus + "</td>";                   
                    tbody += '<td><button type="button" class="btn btn-info btn-xs " onClick="viewrecepcion(\'' + arreglo[i].Clave + '\')"><i class="fa fa-search"></i> Detalle</button></td>';
                    tbody += "</tr>";
                }

                $("#lassalidas tbody").append(tbody);

              

                $(".editrecepcion").click(function () {
                    _recepcion = $(this).attr("rel");
                    var idS = $(this).attr("is");
                    $(".recepciondistr[rel='" + idS + "']").click();
                });

                $(".cancelrecepcion").click(function () {
                    _recepcion = $(this).attr("rel");
                    
                    $("#Confirma button[id='btnOkConfirm']").removeClass("btn-info")
                    $("#Confirma button[id='btnOkConfirm']").addClass("btn-danger")
                    $("#Confirma h4").html($("body div[class='page-title']").html().trim());
                    $("#Confirma label").html("¿ Deseas cancelar la recepción #" + $(this).attr("np") + " ?<br /><br />Este proceso retirara los equipos ingresados al almacén.");
                    $("#Confirma").modal("show");
                });
            }
        });
    });

    $(".recepciondistr").click(function () {

        objArticulos = new Array();
        $("#editadistribuidor").modal("show");
        var objRoles = {};
        objRoles.pedido = $(this).attr("rel");
        objRoles.recepcion = _recepcion

        var myJSONText = JSON.stringify(objRoles);

        $("#selecciona").hide();

        $("#articulostabledistribuidor tbody").empty();

        var url = '/RecepcionDistribuidor/Articulos/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                $("#botonesdistribuidor").removeClass('hide');
                $("#botonesdistribuidor").show();
                var info = "";
                var info2 = "";

                var arreglo = eval(data);
                _articulospedidos = arreglo;
                _pedido = objRoles.pedido;

                for (var i = 0; i < arreglo.length; i++) {
                    var pendiente = parseInt(arreglo[i].Cantidad) - parseInt(arreglo[i].Surtida);

                    if (pendiente > 0 || pendiente == 0 && arreglo[i].Surtiendo > 0) {//&& _recepcion != "")) {

                        if (_recepcion != "") {
                            if (arreglo[i].catSeriesClaves.length == 0) {
                                info += "<tr>" + "<td>" + arreglo[i].Nombre + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td id='TotSur" + i + "'>" + (arreglo[i].Surtida - arreglo[i].Surtiendo) + "</td>" + "<td id='Pen" + i + "'>" + pendiente + "</td>";
                            }
                            else {
                        info += "<tr>" + "<td>" + arreglo[i].Nombre + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td id='TotSur" + i + "'>" + arreglo[i].Surtida + "</td>" + "<td id='Pen" + i + "'>" + pendiente + "</td>";
                            }
                        }
                        else {
                            info += "<tr>" + "<td>" + arreglo[i].Nombre + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td id='TotSur" + i + "'>" + arreglo[i].Surtida + "</td>" + "<td id='Pen" + i + "'>" + pendiente + "</td>";
                        }
                        if (arreglo[i].catSeriesClaves.length > 0) {
                            info += '<td><button type="button" class="btn btn-warning btn-xs detalle" rel="' + i + '">&nbsp;Seriales</button></td>';
                        } else {
                            if (_recepcion == "") {
                            info += '<td><input type="text" size=3 maxlength=4 name="c' + i + '" id="c' + i + '" rel="' + i + '" class="validate[required,custom[integer],min[0],max[' + pendiente + ']] saliendo" value="0"></td>';
                        }
                            else {
                                info += '<td><input type="text" size=3 maxlength=4 name="c' + i + '" id="c' + i + '" rel="' + i + '" class="validate[required,custom[integer],min[' + ((arreglo[i].Existencia < arreglo[i].Surtida) ? arreglo[i].Surtida - arreglo[i].Existencia : 0) + '],max[' + (parseInt(arreglo[i].Surtiendo) + parseInt(pendiente)) + ']] saliendo" value="' + arreglo[i].Surtiendo + '"></td>';
                            }
                            
                        }
                        info += "</tr>";
                    }

                    var objArticulo = {};

                    objArticulo.ProcesoClave = objRoles.pedido;
                    objArticulo.ArticuloClave = arreglo[i].ArticuloClave;
                    objArticulo.ArticuloTexto = arreglo[i].Nombre;
                    objArticulo.tieneseries = arreglo[i].TieneSeries;
                    objArticulo.Cantidad = arreglo[i].Cantidad;
                    objArticulo.Inventario = new Array();
                    objArticulo.Saliendo = 0;
                    objArticulo.Existencia = arreglo[i].Existencia;
                    objArticulos.push(objArticulo);
                }

                $("#articulostabledistribuidor tbody").append(info);

                SalidaMaterialDetalle();

                CargaDetalles();
            },
            error: function (request, error) {

                toastr.error('Error, Surgió un error, intente nuevamente más tarde')
                
            }
        });
    });

    $(".pdf").click(function () {

        var elrel = $(this).attr('rel');
        window.location.href = "/SalidaMaterial/Detalle?pdf=1&s=" + elrel;
    });
}

function CargaDetalles() {

    var objParametros = {};
    var myJSONText = "";

    for (var foo = 0; foo < objArticulos.length; foo++) {

        objParametros.Articulo = objArticulos[foo].ArticuloClave;
        objParametros.Proceso = objArticulos[foo].ProcesoClave;
        objParametros.Recepcion = _recepcion;

        myJSONText = JSON.stringify(objParametros);

        $.ajax({
            url: "/RecepcionDistribuidor/Articulo/",
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            async: false,
            success: function (data) {
                var arreglo = eval(data);

                for (var ii = 0; ii < arreglo.length; ii++) {
                    if (arreglo[ii].Seriales[0].EstatusTexto == "1") {
                        objArticulos[foo].Inventario.push(arreglo[ii].Clave);
                    }
                }
            },
            error: function (request, error) {
                Mensaje('Surgió un error, intente nuevamente más tarde');
            }
        });
    }
}

function DameOpciones(elid, ssEstatus, dEnv) {
    //return "<button type='button' class='btn btn-default btn-xs verrecep' rel='" + elid + "' title='Ver Recepciones'" + ((ssEstatus == "Por") ? " disabled" : "") + " st='" + ssEstatus + "'>&nbsp;&#128269;</button>&nbsp;<button type='button' class='btn btn-default btn-xs recepciondistr' rel='" + elid + "'" + ((ssEstatus == "Com") ? " disabled" : "") + ">&nbsp;Recepción&nbsp;</button>&nbsp;<button type='button' class='btn btn-info btn-xs pdf' rel='" + elid + "'>&nbsp;PDF</button>";
    return "<button type='button' class='btn btn-default-bright btn-xs verdatosenv' rel='" + elid + "' title='Ver Datos de Envio'" + ((!dEnv) ? " disabled" : " ds='2'") + ">&nbsp;Datos Envio</button>&nbsp;"
                + "<button type='button' class='btn btn-info btn-xs recepciondistr' rel='" + elid + "'" + ((ssEstatus == "Com" || ssEstatus == "Can") ? " disabled" : "") + ">&nbsp;Recibir&nbsp;</button>&nbsp;"
        + "<button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-success verrecep' data-toggle='tooltip' data-placement='right' title='' data-original-title='Ver detalle de recepciones'"
        + " rel='" + elid + "' title='Ver Recepciones'" + ((ssEstatus == "Por" || ssEstatus == "Can") ? " disabled" : "") + " st='" + ssEstatus + "'><i class='fa fa-search'></i></button>"
        +"<button type='button' class='btn ink-reaction btn-floating-action btn-xs btn-danger pdf' data-toggle='tooltip' data-placement='right' title='' data-original-title='Descarga PDF' rel='"
        + elid + "'><i class='md md-file-download' aria-hidden='true'></i></button>";
}

function GuardaInfo() {
    var valid = jQuery('#formaeditadistribuidor').validationEngine('validate');
    if (valid) {

        $(".saliendo").each(function () {
            var cual = $(this).attr('rel');
            objArticulos[cual].Saliendo = $(this).val();
        });

        if ($.grep(objArticulos, function (n, i) {
            return ((n.tieneseries == 0 && n.Saliendo > 0) ||
                (n.tieneseries > 0 && n.Inventario.length > 0));
        }).length == 0) {
            toastr.warning('Error, debes registrar la recepción de algun artículo');
            return false;
        }

        var objDatos = {};
        objDatos.ProcesoSal = objArticulos[0].ProcesoClave;
        // Se agrega este parametro para la edicion de las recepciones.
        objDatos.Recepcion = _recepcion;
        objDatos.Observaciones = $("#ed_observaciones").val();
        objDatos.Inventario = $.grep(objArticulos, function (n, i) {
            return ((n.tieneseries == 0 && n.Saliendo > 0) ||
                (n.tieneseries > 0 && n.Inventario.length > 0));
        });

        var myJSONText = JSON.stringify(objDatos);     

        var url = '/RecepcionDistribuidor/Guarda/';
        $.ajax({
            url: url,
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                toastr.success("La recepción se guardó correctamente");
                $("#editadistribuidor").modal('hide');
                $('#repo').click();
                $('#titulocanvas').empty().append("Detalle recepción");
                $("#framecanvas").attr("src", "/RecepcionDistribuidor/Detalle?pdf=2&s=" + data);

               // $("#eliframesalida").attr("src", "/RecepcionDistribuidor/Detalle?pdf=2&s=" + data);
               // $('#verrecepcion').modal('show');


                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }
    else {
        
    }
}
