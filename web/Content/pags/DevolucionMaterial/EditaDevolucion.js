﻿
var idEdiccion=null;
function Edita(id) {
    articulos = [];
    idEdiccion = id;
    DetalleDevolucion(id);
    $('#guardaDevolucion').hide();
    $('#EditaDevolucion').show();
}

function DetalleDevolucion(id) {

    $('#AgregaDevolucion').modal('show');
    var url = '/DevolucionAlmacenCentral/DetalleDevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'iddevolucion': id },
        type: "POST",
        success: function (data) {

            $('#almacen').val(data.almacen);
            $('#motivo').val(data.motivo);

            for (var a = 0; a < data.articulos.length; a++) {
                var Serie = {};
                Serie.InventarioClave = data.articulos[a].InventarioClave;
                Serie.catSerieClave = 2;
                Serie.Valor = data.articulos[a].Valor;
                Serie.ArticuloClave = data.articulos[a].ArticuloClave;
                Serie.Nombre = data.articulos[a].Nombre;
                Serie.Cantidad = data.articulos[a].Cantidad;
                Serie.status = data.articulos[a].status;
                Serie.Statustxt = "";
                articulos.push(Serie);
                ActualizaDetalle();
            }

        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

   

}


function EditaArticulo(idArticulo, cantidad) {
    $('#AgregaArticulo').modal('show');

    var url = '/SalidaMaterialTecnico/DetalleArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idarticulo': idArticulo },
        type: "POST",
        success: function (data) {

            $('#clasificacion').val(data.idClasificacion);
            obtentipomat(data.idClasificacion, data.idTipoArticulo);            
            var valor = AsignaArticulo(data.idArticulo, data.idTipoArticulo);
            
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

}

function AsignaArticulo(idArticulo, idTipoArticulo) {

    var a = "";
    $('#articulo').empty().append('<option selected="selected" disabled value="0">Selecciona</option>');

    var url = '/SalidaMaterialTecnico/GetArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': idTipoArticulo },
        type: "POST",
        success: function (data) {
            
            for (var a = 0; a < data.length; a++) {
                if (data[a].ArticuloClave == idArticulo) {

                    $('#articulo').append('<option selected rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');

                    var b = (data[a].catSerieClaveInterface == null ? "0" : "1");
                    DetalleArticulo(b);
                } else {

                    $('#articulo').append('<option rel="' + (data[a].catSerieClaveInterface == null ? "0" : "1") + '" value=' + data[a].ArticuloClave + '>' + data[a].Nombre + '</option>');
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

    return a;

}


function cuantosdevolucion(idDev, idArticulo) {
    
    var url = '/DevolucionAlmacenCentral/cuantosdevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idDev': idDev, 'idArticulo': idArticulo },
        type: "POST",
        success: function (data) {
            $('#cantidad_anterior').val(data);
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });   
}


$('#EditaDevolucion').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Alerta, no puede realizar una devolución sin articulos");
    }

    else if (comment.length == 0) {
        toastr.warning("Alerta", "no puede realizar una devolución sin especificar el motivo de esta.");
    }
    else {

        var url = '/DevolucionAlmacenCentral/EditaDevolucion/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'motivo': $('#motivo').val(), 'articulos': JSON.stringify(articulos), 'devolucion': idEdiccion },
            type: "POST",
            success: function (data) {
                if (data.idError == "-1") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else if (data.idError == "-2") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else if (data.idError == "-3") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else if (data.idError == "-4") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else if (data.idError == "-5") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else if (data.idError == "-6") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else if (data.idError == "-7") {
                    toastr.warning("Atención "+ data.toastr.error);
                }
                else {
                    

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle Devolución de material");
                    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);

                    //$("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);
                    toastr.success("Correcto, Se ha generado correctamente");
                   // $('#ver').modal('show');

                    $("#AgregaDevolucion").modal("hide");

                    llenaLista();
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});


