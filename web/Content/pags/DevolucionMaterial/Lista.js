﻿
llenaLista();


function llenaLista(almacen, orden) {
    var Numero;
    if (orden == "" || orden == undefined) {
        Numero = 0;
    } else {
        Numero = orden;
    }
    var Alm;
    if (almacen == "" || almacen == undefined) {
        Alm = 0;
    } else {
        Alm = almacen;
    }

    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/DevolucionAlmacenCentral/ListaDevoluciones/",
            "type": "POST",
            "data": {
                'data': "1",'almacen':Alm,'orden':Numero
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "pedido", "orderable": false },
                 { "data": "fecha", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         console.log(full.estatus);
                         var str;
                         if (full.estatus == 1) { str = "Por Autorizar" }
                         else if (full.estatus == 4) { str = "Recibido en almacen central" }
                         else { str = "Cancelada" }
                         return str;


                     }

                 },
                 { "data": "almacen", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var str = "";
                         str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger"  data-toggle="tooltip" data-placement="right" title=""  data-original-title="Descarga PDF"   ' + ' onClick="descargapdf(\'' + full.proceso + '\')"><i class="md md-file-download" aria-hidden="true"></i></button>';
                         str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-success" data-toggle="tooltip" data-placement="right" title=""  data-original-title="Ver detalle de devolución" onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search" aria-hidden="true"></i></button>';
                         return str;
                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var str = "";                         
                         if (full.estatus == 1) {
                             if (full.tiene_envio==1) {
                                 str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-default-bright" onclick="DetalleEnvio(\'' + full.proceso + '\');"><i class="fa fa-envelope-o"></i></buttton>';
                             } else {
                                 str += '<button class="btn ink-reaction btn-floating-action btn-xs btn-default-bright" onclick="Envio(\'' + full.proceso + '\');"><i class="fa fa-envelope-o"></i></buttton>';
                             }
                             str += '<button class="btn btn-warning btn-xs" onclick="Edita(\'' + full.proceso + '\');"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button>';
                             str += '<button class="btn btn-danger btn-xs" onclick="Cancela(\'' + full.proceso + '\');"><i class="fa fa-trash-o" aria-hidden="true"></i> Cancelar</button>';
                         }
                         return str;
                     }
                     
                 }


        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle Devolución de material");
    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + devolucion);
    
    //$("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + devolucion );   
    //$("#titulo_ver").html($("body div[class='page-title']").html().trim());    
    //$('#ver').modal('show');
};


function descargapdf(devolucion) {   
   
    window.location.href = "/DevolucionAlmacenCentral/Detalle?pdf=1&d=" + devolucion;
};

function Envio(proceso) {
    $('#edProcesoEnvio').modal('show');
    $('#devEnv').val(proceso);
    $('#edPE_guia').val('');
    $('#edPE_transportista').val('');
    $('#edPE_fecha').val('');
}


function DetalleEnvio(proceso) {
    $('#edProcesoEnvio').modal('show');
    $('#devEnv').val(proceso);

    $('#edPE_guia').val('');
    $('#edPE_transportista').val('');
    $('#edPE_fecha').val('');


    var url = '/DevolucionAlmacenCentral/detalleEnvio/';
        $.ajax({
            url: url,
            data: { 'proceso': proceso },
            type: "POST",
            success: function (data) {
               
                $('#edPE_guia').val(data.guia);
                $('#edPE_transportista').val(data.tran);
                $('#edPE_fecha').val(data.fecha);               
                
            },
            error: function (request, error) {
                toastr.error('Error, Surgió un error, intente nuevamente más tarde');
            }
        });

}


//function DetalleEnvio(proceso) {
//    $('#DetalleEnvio').modal('show');
//    $('#tbdet').empty();
//    var url = '/DevolucionAlmacenCentral/detalleEnvio/';
//    $.ajax({
//        url: url,
//        data: { 'proceso': proceso },
//        type: "POST",
//        success: function (data) {
//            $('#tbdet').append('<tr><td><b><h5><i class="fa fa-bus" aria-hidden="true"></i> Transportista:</h5></b></td><td>' + data.tran + '</td></tr><tr><td><h5><i class="fa fa-ticket" aria-hidden="true"></i><b>Guia</b></h5></td><td> ' + data.guia + '</td></tr><tr><td> <h5><i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha de envio:</h5></td><td>' + data.fecha + '</td></tr>')

//        },
//        error: function (request, error) {
//            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
//        }
//    });



//}


function Cancela(proceso) {
    $('#CancelaDev').modal('show');
    $('#id_procesoc').val(proceso);
}

$('#Cancelar-dev').click(function () {
   var proceso= $('#id_procesoc').val();
    var url = '/DevolucionAlmacenCentral/Cancela/';
    $.ajax({
        url: url,
        data: { 'proceso': proceso },
        type: "POST",
        success: function (data) {
            if (data == "-1") {
                toastr.warning("Error, la devolución ya no se encuentra en el sistema, contacte al administrador del sistema");

            }
            else if (data == "-2") {
                toastr.warning("Error, la devolución ya se encuentra con estatus de cancelado, probablemente otro usuario ya ejecutó la cancelacíón");
            }
            else if (data == "-3") {
                toastr.warning("Error, la devolución ya se recepcionó en almacén principal, No se puede ser cancelada");
            }

            else {
               // $("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);
                toastr.success("Correcto, se ha cancelado correctamente");
                //$('#ver').modal('show');
                $('#CancelaDev').modal('hide');
                llenaLista();

            }
           
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});

$('#btn_PEguardar').click(function () {

   var d= $('#devEnv').val();
  var tra = $('#edPE_transportista').val();
   var guia= $('#edPE_guia').val();
    var fecha=$('#edPE_fecha').val();
    var url = '/DevolucionAlmacenCentral/Envio/';
    $.ajax({
        url: url,
        data: { 'proceso': d ,'trans':tra,'guia':guia,'fecha':fecha},
        type: "POST",
        success: function (data) {
            if (data == "1") {
                $('#edProcesoEnvio').modal('hide');
                llenaLista();
                toastr.success("Se han agregado los datos de envio");
            }
            else {
                toastr.warning("No se pudo ingresar los datos de envio");
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
});

$('#buscar').click(function () {

    var val=$('#ord').val();
    llenaLista("",val);


});

$('#falmacen').change(function () {

    var val = $('#falmacen').val();
    llenaLista(val,"");
});