﻿

$('#guardaDevolucion').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Atención, no puede realizar una devolución sin artículos");
    }
    
    else if (comment.length == 0) {
        toastr.warning("Atención, no puede realizar una devolución sin especificar el motivo de esta.");
    }
    else {

        var url = '/DevolucionAlmacenCentral/GuardaDevolucion/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'almacen': $('#almacen').val(), 'motivo': $('#motivo').val(), 'articulos': articulos },
            type: "POST",
            success: function (data) {
                if (data == "-1") {
                    toastr.warning("Error, alguno de los aparatos no se encuentra en el almacén o ya fue recepcionado por el almacén principal \n contacta al administrador para mayor información");
                }
                else if(data=="-2"){
                    toastr.warning("Error, la cantidad solicitada excede el saldo en el inventario");
                }
                else if (data == "0") {
                    toastr.warning("Error, el sistema evitó que se generará el proceso, contacta al administrador del sistema para mayor información ");
                }
                else if (data == "-3") {
                    toastr.warning("Error, su sesión ha expirado,vuelva a autenticarse");
                }
                else {
                   
                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle Devolución de material");
                    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);

                   
                    toastr.success("Correcto, la devolución se ha generado correctamente");
                    //$('#ver').modal('show');

                    $("#AgregaDevolucion").modal("hide");

                    llenaLista();
                }

                
               
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }
    
});