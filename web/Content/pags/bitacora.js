﻿var objArticulos = new Array();
var _articulospedidos = null;
var _artculosEliminados = new Array();
var _indiceArticulo = -1;
var _salida = null;
$(document).ready(function () { 

    $("#ed_tecnico").change(function () {
        if ($("#temporal").length == 0) {
            var select = '';
            select += "<select id='temporal' style='display:none'>";
            $("#ed_almacen option").each(function () {
                select += "<option value='" + $(this).val() + "'>" + $(this).text() + "</option>";
            });
            select += "</select>";
            $("#ed_almacen").after(select);
        }
        $("#ed_almacen").empty();
        $("#ed_almacen").append("<option value='' selected>Seleccione</option>");


        $("#temporal option").each(function () {
            var almacenes = $("#ed_tecnico option:selected").attr("data-almacen");
            var valor = $(this).attr('value');
            if (almacenes.indexOf(valor) > -1 && valor != "") {
                $("#ed_almacen").append("<option value='" + $(this).val() + "' data-almacen='" + $(this).attr('data-almacen') + "'>" + $(this).text() + "</option>");
            }
        });

    });

    $("#agregar").click(function () {
        if ($("#ed_almacen").val() == '') {
            toastr.error('Error', ' debe seleccionar un almacén para hacer la salida');
            return;
        }
        _indiceArticulo = -1;
        $('#articulo').modal('show');

        $("#ed_clasificacionmaterial").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_articulo").val('');
        $("#en_almacen").val('');
        $("#ed_cantidad").val('');
        $("#lasseries tbody").empty();
        $("#lasseries thead").empty();
    });

    $(".optiontipo").hide();

    $("#ed_clasificacionmaterial").change(function () {

        $("#ed_tipomaterial").val('');

        $(".optiontipo").each(function () {
            if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val()) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });

    LlenaGrid();

    $("#tipobitacora").change(function () {
        LlenaGrid(1,"");
    });
     $("#almacen").change(function () {
        LlenaGrid(1,"");
    });

    $("#tecnico").change(function () {
        LlenaGrid(1,"");
    });

    $("#obten").click(function () {
        LlenaGrid(1,"");
    });  

    $("#ed_tipomaterial").change(function () {
        var tipo = $(this).val();
        ObtenPorTipo(tipo);
    });


    $("#ed_articulo").change(function () {
        SeleccionaArticulo();
    });
});

var _pag = 1;

function LlenaGrid(pag,contrato) {    

        var objRoles = {};
        objRoles.pag = contrato;
        objRoles.tecnico = $("#tecnico").val();
        objRoles.plaza = $("#almacen").val();
        objRoles.tipobitacora = $("#tipobitacora").val();

        var myJSONText = JSON.stringify(objRoles);
        
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bDestroy": true,
            "info": true,
            "stateSave": false,
            "lengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
            "ajax": {
                "url": "/BitacoraAlmacen/ObtenListaBitacora/",
                "type": "POST",
                "data": { 'data': myJSONText }
            },
            "fnInitComplete": function (oSettings, json) {

                AfterAjax();
            },
            "fnDrawCallback": function (oSettings) {

                AfterAjax();
            },


            "columns": [
                { "data": "NumeroPedido", "orderable": true },
                { "data": "Fecha", "orderable": false },
                { "data": "Destino", "orderable": false },
                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        var estatus = (full.BitacoraLetra == 'C') ? 'CANCELADO' : 'Entregado';
                        console.log(full.BitacoraLetra);                       
                        return estatus;
                    }
                },

             {
                 sortable: false,
                 "render": function (data, type, full, meta) {                     
                     var renglon = "";
                     renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-default versoftv" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver bitácora"'
                         + '  onClick="VerSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-search"></i> Ver bitácora</button>';

                     renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-primary pdfsoftv"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"'
                     + '  onClick="pdfSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar</button>';
                     return renglon;
                 }
             },
             {
                 sortable: false,
                 "render": function (data, type, full, meta) {
                     if (full.BitacoraLetra != 'C') {
                         return '<button class="btn btn-danger btn-xs" onClick="cancela(\'' + full.Clave + '\')"><i class="fa fa-trash"></i> Cancelar</button>';
                     } else {
                         return "";
                     }
                   
                 }
             }

            ],

            language: {
                processing: "Procesando información...",
                search: "Buscar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ Elementos",
                info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
                infoEmpty: "No hay elemetos para mostrar",
                infoFiltered: "(filtrados _MAX_ )",
                infoPostFix: "",
                loadingRecords: "Búsqueda en curso...",
                zeroRecords: "No hay registros para mostrar",
                emptyTable: "No hay registros disponibles",
                paginate: {
                    first: "Primera",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultima"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },


            "order": [[0, "asc"]]
        });




        $('#datossoftv').dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bDestroy": true,
            "info": true,
            "stateSave": false,
            "lengthMenu": [[5], [5]],
            "ajax": {
                "url": "/BitacoraTecnico/ObtenListaBitacora/",
                "type": "POST",
                "data": { 'data': myJSONText }
            },
            "columns": [
                { "data": "NumeroPedido", "orderable": true },
                { "data": "Fecha", "orderable": false },
                { "data": "Destino", "orderable": false },
                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        
                        var estatus = "Entregado";
                        if (full.Estatus == "Cancelado") {
                            estatus = "Cancelado";
                        }
                        return estatus;

                    }
                },

                {
                    sortable: false,
                    "render": function (data, type, full, meta) {
                        var estatus = "Entregado";
                        if (full.Estatus == "Cancelado") {
                            estatus = "Cancelado";
                        }
                        

                        var renglon = "";
                        renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-default versoftv" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver bitácora"'
                            + '  onClick="VerSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-search"></i> ver bitácora</button>';
                        
                        renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-info pdfsoftv"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"'
                        + '  onClick="pdfSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar</button>';
                        return renglon;

                    }
                }

            ],

            language: {
                processing: "Procesando información...",
                search: "Buscar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ Elementos",
                info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
                infoEmpty: "No hay elemetos para mostrar",
                infoFiltered: "(filtrados _MAX_ )",
                infoPostFix: "",
                loadingRecords: "Búsqueda en curso...",
                zeroRecords: "No hay registros para mostrar",
                emptyTable: "No hay registros disponibles",
                paginate: {
                    first: "Primera",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultima"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },


            "order": [[0, "asc"]]
        });




}



function LlenaGrid2(pag, contrato) {

    var objRoles = {};
    objRoles.pag = contrato;
    objRoles.tecnico = $("#tecnico").val();
    objRoles.plaza = $("#almacen").val();
    objRoles.tipobitacora = $("#tipobitacora").val();

    var myJSONText = JSON.stringify(objRoles);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
        "ajax": {
            "url": "/BitacoraAlmacen/ObtenListaBitacora/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "fnInitComplete": function (oSettings, json) {

            AfterAjax();
        },
        "fnDrawCallback": function (oSettings) {

            AfterAjax();
        },


        "columns": [
            { "data": "NumeroPedido", "orderable": true },
            { "data": "Fecha", "orderable": false },
            { "data": "Destino", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    var estatus = (full.BitacoraLetra == 'C') ? 'CANCELADO' : 'Entregado';
                    console.log(full.BitacoraLetra);
                    return estatus;
                }
            },

         {
             sortable: false,
             "render": function (data, type, full, meta) {
                 var renglon = "";
                 renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-default versoftv" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver bitácora"'
                     + '  onClick="VerSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-search"></i> Ver bitácora</button>';

                 renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-primary pdfsoftv"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"'
                 + '  onClick="pdfSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar</button>';
                 return renglon;
             }
         },
         {
             sortable: false,
             "render": function (data, type, full, meta) {
                 if (full.BitacoraLetra != 'C') {
                     return '<button class="btn btn-danger btn-xs" onClick="cancela(\'' + full.Clave + '\')"><i class="fa fa-trash"></i> Cancelar</button>';
                 } else {
                     return "";
                 }

             }
         }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    });




    $('#datossoftv').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/BitacoraTecnico/ObtenListaBitacora/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "columns": [
            { "data": "NumeroPedido", "orderable": true },
            { "data": "Fecha", "orderable": false },
            { "data": "Destino", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {

                    var estatus = "Entregado";
                    if (full.Estatus == "Cancelado") {
                        estatus = "Cancelado";
                    }
                    return estatus;

                }
            },

            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    var estatus = "Entregado";
                    if (full.Estatus == "Cancelado") {
                        estatus = "Cancelado";
                    }


                    var renglon = "";
                    renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-default versoftv" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver bitácora"'
                        + '  onClick="VerSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-search"></i> ver bitácora</button>';

                    renglon += '<button type="button" class="btn ink-reaction  btn-xs btn-info pdfsoftv"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"'
                    + '  onClick="pdfSoftv(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar</button>';
                    return renglon;

                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    });




}

$('#contrato').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        $('#buscarcontrato').click();
        return false;
    }
});


//$('#buscarcontrato').click(function () {
//    var contrato = $('#contrato').val();
//    console.log(contrato);
//    if (contrato == "") {
//        toastr.warning("Atención"," debe colocar un contrato válido");
//    }
//    else {
//        LlenaGrid("",contrato);
//    }
    
//});


function VerSoftv(id) {

    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle bitácora");
    $("#framecanvas").attr("src", "/BitacoraTecnico/DetalleSofTV?pdf=2&s=" + id);

}

function pdfSoftv(id) {

    var elrel = $(this).attr('rel');
    window.location.href = "/BitacoraTecnico/DetalleSofTV?pdf=1&s=" + id;
}

function cancela(id) {
    var url = '/BitacoraAlmacen/CancelaBitacora/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'bitacoraClave': id },
        type: "POST",
        success: function (data) {
            if (data == "1") {
                toastr.success('La bitácora se ha cancelado correctamente');
                LlenaGrid(1, "");
            } else {
                toastr.error(data.Mensaje);
            }

        },
        error: function (request, error) {
            toastr.error('Sucedio un error de conexión /su sesión ha expirado');
        }
    });

    CancelaBitacora
}

function AfterAjax() {

    //BitacoraTecnicoDetalle();


    $(".ver").click(function () {
        var elrel = $(this).attr('rel');

        $('#repo').click();
        $('#titulocanvas').empty().append("Detalle de surtido distribuidor");
        $("#framecanvas").attr("src", "/BitacoraTecnico/Detalle?pdf=2&s=" + elrel);


        
        //$("#eliframe").attr("src", "/BitacoraTecnico/Detalle?pdf=2&s=" + elrel);
       // $('#ver').modal('show');
    });


    $(".pdf").click(function () {

        var elrel = $(this).attr('rel');
        window.location.href = "/BitacoraTecnico/Detalle?pdf=1&s=" + elrel;
    });  
   
}

