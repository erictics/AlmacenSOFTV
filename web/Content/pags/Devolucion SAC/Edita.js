﻿
var idEdiccion = null;
function Edita(id) {
    articulos = [];
    idEdiccion = id;
    DetalleDevolucion(id);
    $('#guardaDevolucion').hide();
    $('#EditaDevolucion').show();
}

function cuantosdevolucion(idDev, idArticulo) {
    var url = '/DevolucionSAC/cuantosdevolucion/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idDev': idDev, 'idArticulo': idArticulo },
        type: "POST",
        success: function (data) {
            $('#cantidad_anterior').val(data);
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

$('#EditaDevolucion').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Alerta,No puede realizar una devolución sin articulos");
    }

    else if (comment.length == 0) {
        toastr.warning("Alerta", "No puede realizar una devolución sin especificar el motivo de esta.");
    }
    else {

        var url = '/DevolucionSAC/EditaDevolucion/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'motivo': $('#motivo').val(), 'articulos': JSON.stringify(articulos), 'devolucion': idEdiccion },
            type: "POST",
            success: function (data) {
                if (data.idError == "-1") {
                    toastr.warning("Atención " + data.error);
                }
                else if (data.idError == "-2") {
                    toastr.warning("Atención " + data.error);
                }
                else if (data.idError == "-3") {
                    toastr.warning("Atención " + data.error);
                }
                else if (data.idError == "-4") {
                    toastr.warning("Atención " + data.error);
                }
                else if (data.idError == "-5") {
                    toastr.warning("Atención " + data.error);
                }
                else if (data.idError == "-6") {
                    toastr.warning("Atención " + data.error);
                }
                else if (data.idError == "-7") {
                    toastr.warning("Atención " + data.error);
                }
                else {
                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle Devolución de material");
                    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);                   
                    toastr.success("Correcto, Se ha generado correctamente");
                    $("#AgregaDevolucion").modal("hide");
                    ObtenerLista();
                }
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });
    }

});


