﻿$(document).ready(function () {
    ObtenerLista(0, 0);
});

function ObtenerLista(almacen, orden) {
    var Numero= (orden == "" || orden == undefined) ? 0 :orden;
    var Alm= (almacen == "" || almacen == undefined) ? 0:almacen;       
    var data = { 'data': "1", 'almacen': Alm, 'orden': Numero }
    var url = "/DevolucionSAC/ListaDevoluciones/";
    var columnas = [
                 { "data": "pedido", "orderable": false },
                 { "data": "fechaCorta", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var str;
                         if (full.estatus == 1) { str = "Por Autorizar" }
                         else if (full.estatus == 4) { str = "Recibido en almacen central" }
                         else { str = "Cancelada" }
                         return str;
                     }
                 },
                 { "data": "almacen", "orderable": false },
                  { "data": "almacenDestino", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var pdf = '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger" onClick="descargapdf(\'' + full.proceso + '\')"><i class="fa fa-download" aria-hidden="true" ></i></button>';
                         var verpdf = '<button class="btn ink-reaction btn-floating-action btn-xs btn-primary " onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search" aria-hidden="true" ></i></button>';
                        

                         var str = "";
                         str += pdf + verpdf;
                        
                         return str;
                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var str = "";
                         var detenvio = '<button class="btn ink-reaction btn-floating-action btn-xs btn-primary-bright" onclick="DetalleEnvio(\'' + full.proceso + '\');"><i class="fa fa-envelope-o" aria-hidden="true"></i></buttton>';
                         var envio = '<button class="btn ink-reaction btn-floating-action btn-xs btn-default-bright" onclick="Envio(\'' + full.proceso + '\');"><i class="fa fa-envelope-o" aria-hidden="true"></i></buttton>';
                         if (full.estatus == 1) {

                             if (full.tiene_envio == 1) {
                                 str += detenvio;
                             } else {
                                 str += envio;
                             }
                         }
                         return str;
                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         var cancelar = '<button class="btn btn-danger btn-xs" id="cancelar" onclick="Cancela(\'' + full.proceso + '\');"><i class="fa fa-trash-o" aria-hidden="true"></i>Cancelar</button>';
                         var editar = '<button class="btn btn-warning btn-xs" id="editar" onclick="Edita(\'' + full.proceso + '\');"><i class="fa fa-pencil" aria-hidden="true"></i>Editar</button>';

                         var str = "";
                         if (full.estatus == 1) {
                             str += editar + cancelar;
                         }
                         return str;
                     }
                 }];       

    $('#indice').dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "bDestroy": true,
            "info": true,
            "stateSave": true,
            "lengthMenu": [[5], [5]],
            "ajax": {
                "url": url,
                "type": "POST",
                "data": data
            },
            "columns": columnas,
            language:lenguaje,
            "order": [[0, "asc"]]
        }) 
    
}

function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle Devolución de material");
    $("#framecanvas").attr("src", "/Devolucion/Detalle?pdf=2&d=" + devolucion);

};

function descargapdf(devolucion) {
    window.location.href = "/Devolucion/Detalle?pdf=1&d=" + devolucion;
};



function Cancela(proceso) {
    $('#CancelaDev').modal('show');
    $('#id_procesoc').val(proceso);
}

$('#Cancelar-dev').click(function () {
    var proceso = $('#id_procesoc').val();
    var url = '/DevolucionSAC/Cancela/';
    $.ajax({
        url: url,
        data: { 'proceso': proceso },
        type: "POST",
        success: function (data) {
            if (data == "-1") {
                toastr.warning("Error, La devolución ya no se encuentra en el sistema, contacte al administrador del sistema");
            }
            else if (data == "-2") {
                toastr.warning("Error, La devolución ya se encuentra con estatus de cancelado, probablemente otro usuario ya ejecutó la cancelacíón");
            }
            else if (data == "-3") {
                toastr.warning("Error, La devolución ya se recepcionó en almacén principal, No se puede ser cancelada");
            }
            else {                
                toastr.success("Correcto, Se ha cancelado correctamente");               
                $('#CancelaDev').modal('hide');
                ObtenerLista();
            }
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
});

$('#buscar').click(function () {
    var val = $('#ord').val();
    ObtenerLista();
});
$('#falmacen').change(function () {
    var val = $('#falmacen').val();
    ObtenerLista()
});