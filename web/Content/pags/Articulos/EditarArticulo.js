﻿function edita(cual) {   
    $("#titulowin").html("Edita Artículo");
    $('#edita').modal('show');

    var url = '/Articulo/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {
            console.log(data);
            var oRol = eval(data);
            $("[type=checkbox]").removeProp('checked');
            $("[type=checkbox]").parent().removeClass('checked');

            $("#ed_existencias").prop('readonly', true);
            $("#ed_id").val(oRol.Clave);
            $("#ed_nombre").val(oRol.Descripcion);

            if (oRol.idServicios.length > 0) {

                for (a = 0; a < oRol.idServicios.length; a++) {
                    $("#" + oRol.idServicios[a]).prop('checked', 'checked');
                    $("#" + oRol.idServicios[a]).parent().addClass('checked');
                    
                }
            }
            if (oRol.EsPagare == "1") {
                $("#lblPrecio").html("Precio de Pagare");

                $("#ed_moneda option[value=1]").prop('selected', true);
                $("#ed_moneda").attr("disabled", true);

            }
            else {
                $("#lblPrecio").html("Precio de Venta");
                $("#ed_moneda").attr("disabled", false);
            }

            $("#ed_precio").val(oRol.PrecioPagare);
            $("#ed_moneda").val(oRol.catMonedaPagareClave);
            $("#ed_existencias").val(oRol.Existencias);
            $("#ed_tope").val(oRol.TopeMinimo);
            $("#ed_unidad").val(oRol.catUnidadClave);
            $("#ed_porcentaje").val(oRol.Porcentaje);

            $("#ed_unidad").val(oRol.catUnidadClave);
            $("#ed_clasificacionmaterial").val(oRol.catClasificacionMaterialClave);
            LlenaTipoMaterial();
            $("#ed_tipomaterial").val(oRol.catTipoArticuloClave);
            $("#ed_ubicacion").val(oRol.catUbicacionClave);
            $("#ed_proveedor1").val(oRol.proveedor1);
            $("#ed_precio1").val(oRol.precio1);
            $("#ed_moneda1").val(oRol.moneda1);
            $("#ed_proveedor2").val(oRol.proveedor2);
            $("#ed_precio2").val(oRol.precio2);
            $("#ed_moneda2").val(oRol.moneda2);
            $("#ed_proveedor3").val(oRol.proveedor3);
            $("#ed_precio3").val(oRol.precio3);
            $("#ed_moneda3").val(oRol.moneda3);
            $("#parte").val(oRol.parte);

            if (oRol.Activo === "1") {
                $("#ed_activa").prop('checked', 'checked');
                $("#ed_activa").parent().addClass('checked');
            }
            //LlenaGrid(_pag);

        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}