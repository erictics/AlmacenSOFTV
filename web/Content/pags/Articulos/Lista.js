﻿
$(document).ready(function () {
    llenaLista();

    $("#tip").change(function () {
        llenaLista();
    });

    $("#cla").change(function () {
        llenaLista();
    });

    $("#pagareventa").change(function () {
        llenaLista();
    });    


    $("#ed_clasificacionmaterial").change(function () {

        LlenaTipoMaterial();

    });

});




$("#cla").change(function () {

    $("#tip").val('');

    if ($("#temporal").length == 0) {
        var select = '';
        select += "<select id='temporal' style='display:none'>";

        $(".optiontipo").each(function () {
            select += "<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "'>" + $(this).text() + "</option>";
        });

        select += "</select>";
        $("#tip").after(select);
    }
    $("#tip").empty();
    $("#tip").append("<option value='' selected>Seleccione</option>");


    $("#temporal option").each(function () {
        if ($(this).attr("data-cla") == $("#cla").val()) {
            $("#tip").append("<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "'>" + $(this).text() + "</option>");
        }
    });

    $('#tip').val('');
});


function llenaLista() {

    var objRoles = {};
    objRoles.pag = 1;
    objRoles.tipo = $('#tip').val();
    objRoles.cla = $('#cla').val();
    objRoles.pagare = $("#pagareventa").val();

    var myJSONText = JSON.stringify(objRoles);    

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/Articulo/ObtenPorTipo/",
            "type": "POST",
            "data": {
                'data': myJSONText
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
             {
                 sortable: false,
                 "render": function (data, type, full, meta) {

                     if (full.Activo === "1") {
                         return full.Descripcion;
                     }
                     else {
                         return "<s>" + full.Descripcion + "</s>";
                     }


                 }

             },                  
               { "data": "Clasificacion", "orderable": false },
               { "data": "TipoArticulo", "orderable": false },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.EsPagare == "1") {
                              return "Pagare";
                          }
                          else {
                              return "Para Venta";
                          }
                      }

                  },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          
                          if (full.Servicios != null) {
                              if (full.Servicios.length > 0) {
                                  var string = "";
                                  for (var s = 0; s < full.Servicios.length; s++) {
                                      string += "<label class='btn btn-primary btn-xs' >"+full.Servicios[s]+"</label>";
                                  }
                                  return string;
                              }
                              else {
                                  return "<label class='btn btn-default-bright btn-xs'>Sin asignar</label>";
                              }
                          } else {

                              return "<label class='btn btn-default-bright btn-xs'>Sin asignar</label>";
                          }
                      }

                  },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.Activo === "1") {
                             return "<img src='/Content/assets/img/activo.png'>";
                          }
                          else {
                              return "<img src='/Content/assets/img/inactivo.png'>";
                          }
                      }

                  },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {
                          return  '<button type="button" class="btn btn-warning btn-xs edit" onClick="edita(\'' + full.Clave + '\')" ><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Editar</button>';
                          
                      }

                  },

                  


        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}


function LlenaTipoMaterial() {
    $("#ed_tipomaterial").val('');

    if ($("#temporal").length == 0) {
        var select = '';
        select += "<select id='temporal' style='display:none'>";

        $(".optiontipo").each(function () {
            select += "<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "' data-tipo='" + $(this).attr('data-tipo') + "'>" + $(this).text() + "</option>";
        });

        select += "</select>";
        $("#ed_tipomaterial").after(select);
    }
    $("#ed_tipomaterial").empty();
    $("#ed_tipomaterial").append("<option value='' selected>Seleccione</option>");

    $("#temporal option").each(function () {
        if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val()) {
            $("#ed_tipomaterial").append("<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "' data-tipo='" + $(this).attr('data-tipo') + "'>" + $(this).text() + "</option>");
        }
    });

    $('#ed_tipomaterial').val('');
}
