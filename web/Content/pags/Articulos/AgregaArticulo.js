﻿$("#add").click(function () {
    //$("#accionesedita").addClass('hide');
    //$("#accionesedita").hide();
    $("#tt1").click();


    $("#ed_existencias").removeAttr('readonly');

    $("#titulowin").html("Agregar Artículo");
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');
    $("#ed_precio").val('');

    $("#ed_ubicacion").val('');


    $("#ed_existencias").val('');
    $("#ed_tope").val('');

    $("#ed_unidad").val('');
    $("#ed_porcentaje").val('');

    $("#ed_unidad").val('');

    $("#ed_tipomaterial").val('');
    $("#ed_clasificacionmaterial").val('');


    $("#ed_proveedor1").val('');
    $("#ed_precio1").val('');
    $("#ed_moneda1").val('');

    $("#ed_proveedor2").val('');
    $("#ed_precio2").val('');
    $("#ed_moneda2").val('');

    $("#ed_proveedor3").val('');
    $("#ed_precio3").val('');
    $("#ed_moneda3").val('');

    $("[type=checkbox]").removeProp('checked');
    $("[type=checkbox]").parent().removeClass('checked');
    $("#ed_activa").prop('checked', 'checked');
    $("#ed_activa").parent().addClass('checked');

});



$("#formaedita").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (!valid) {
        toastr.error('debe de llenar todos los campos')
        return;
    }
    
    else {
        

        var url = '/Articulo/Guarda/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaedita').serialize(),
            type: "POST",
            success: function (data) {
                console.log(data);
                if (data == "-3") {
                    toastr.error('Seleccione al menos un servicio para el artículo');
                }
                else if(data == "1") {
                    $("#edita").modal('hide');
                    toastr.success('El Artículo se guardó correctamente');

                    llenaLista();
                }
                else {
                    toastr.success('El Artículo se guardó correctamente, pero el tipo de servicio no se guardó correctamente');
                }

                
            },
            error: function (request, error) {
                toastr.error('Sucedio un error de conexión /su sesión ha expirado');
            }
        });

    }

});