﻿

$(document).ready(function () {
   


    $("#lassalidas").on("click",".editarsalida",function () {

        objArticulos = new Array();

        $("#editadistribuidor").modal("show");


        var objRoles = {};
        objRoles.pedido = _orden;
        objRoles.salida = $(this).attr('rel');

        _salida = objRoles.salida;


        var myJSONText = JSON.stringify(objRoles);
        $("#selecciona").hide();

        $("#articulostabledistribuidor tbody").empty();

        var url = '/Pedido/Articulos/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {


                $("#botonesdistribuidor").removeClass('hide');
                $("#botonesdistribuidor").show();

                var info = "";
                var info2 = "";

                var arreglo = eval(data);
                console.log(arreglo);
                _articulospedidos = arreglo;

                _pedido = objRoles.pedido;

                for (var i = 0; i < arreglo.length; i++) {

                    var pendiente = parseInt(arreglo[i].Cantidad) - parseInt(arreglo[i].Surtida);

                    if (pendiente != 0) {
                        if (arreglo[i].catSeriesClaves.length > 0) {
                            arreglo[i].Surtida = arreglo[i].Seriales.length;
                        }

                        pendiente = parseInt(arreglo[i].Cantidad) - parseInt(arreglo[i].Surtida);
                    }
                        var maximo = pendiente;
                        if (maximo > parseInt(arreglo[i].Existencia)) {
                            maximo = parseInt(arreglo[i].Existencia);
                        }

                    

                        info += "<tr>" + "<td>" + arreglo[i].Nombre + "</td><td id='AlmacenEx" + i + "'>" + arreglo[i].Existencia + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td id='TotSur" + i + "'>" + arreglo[i].Surtida + "</td><td id='Pen" + i + "'>" + pendiente + "</td>";
 
                        if (arreglo[i].catSeriesClaves.length > 0) {
                            info += '<td><button type="button" class="btn btn-warning btn-xs detalle" rel="' + i + '">&nbsp;Seriales</button></td>';
                        } else {
                            info += '<td><input value="' + arreglo[i].Surtiendo + '" type="text" size=3 maxlength=4 name="c' + i + '" id="c' + i + '" rel="' + i + '" class="validate[required,custom[integer],min[0],max[' + maximo + ']] saliendo"></td>';
                        }

                        info += "</tr>";
                    


                    var objArticulo = {};

                    objArticulo.ArticuloClave = arreglo[i].ArticuloClave;
                    objArticulo.ArticuloTexto = arreglo[i].Nombre;
                    objArticulo.tieneseries = arreglo[i].TieneSeries;
                    objArticulo.Cantidad = arreglo[i].Cantidad;


                    objArticulo.Inventario = new Array();
                    for (var jj=0; jj  < arreglo[i].Seriales.length; jj++)
                    {
                        
                        objArticulo.Inventario.push(arreglo[i].Seriales[jj].InventarioClave);
                    }
                    
                    objArticulo.Saliendo = 0;
                    objArticulo.Existencia = arreglo[i].Existencia;

                    objArticulos.push(objArticulo);

                }


                $("#articulostabledistribuidor tbody").append(info);


                $("#tt2").click();

                AfterAjax(); SalidaMaterialDetalle();

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });



    jQuery('#formaeditadistribuidor').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false
    });

    $("#addeditadistribuidor").click(function () {

        $("#tt1").click();

        $("#titulowindistribuidor").html("Salida a Almacén");

        $("#editadistribuidor").modal("show");

    });


    $("#tt1").click(function () {
        $("#botonesdistribuidor").addClass('hide');
        $("#botonesdistribuidor").hide();

        objArticulos = new Array();
        _articulospedidos = null;
        _indiceArticulo = -1;

    });
   


    $("#formaeditadistribuidor").submit(function (e) {
        e.preventDefault();

        GuardaInfo();

    });

});

var pendiente_art = 0;

function AfterAjax2() {

   

    $(".seleccionarpedido").click(function () {
        
        _salida = '';
        objArticulos = new Array();

        $("#editadistribuidor").modal("show");


        var objRoles = {};
        objRoles.pedido = $(this).attr('rel');

        $("#ed_almacen").val($(this).attr("data-almacen"));
        $("#ed_pedido").val($(this).attr('rel'));
        $("#ed_solicitante").val($(this).attr("data-destino"))
        $("#ed_observaciones").val('');

        var myJSONText = JSON.stringify(objRoles);
        $("#selecciona").hide();

        $("#articulostabledistribuidor tbody").empty();

        var url = '/Pedido/Articulos/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {


                $("#botonesdistribuidor").removeClass('hide');
                $("#botonesdistribuidor").show();

                var info = "";
                var info2 = "";

                var arreglo = eval(data);
                _articulospedidos = arreglo;

                _pedido = objRoles.pedido;

                for (var i = 0; i < arreglo.length; i++) {

                    var pendiente = parseInt(arreglo[i].Cantidad) - parseInt(arreglo[i].Surtida);

                    var maximo = pendiente;
                    if (maximo > parseInt(arreglo[i].Existencia)) {
                        maximo = parseInt(arreglo[i].Existencia);
                    }

                    if (pendiente != 0) {
                        info += "<tr>" + "<td>" + arreglo[i].Nombre + "</td><td id='AlmacenEx" + i + "'>" + arreglo[i].Existencia + "</td>" + "<td id='TotPed" + i + "'>" + arreglo[i].Cantidad + "</td>" + "<td id='TotSur" + i + "'>" + arreglo[i].Surtida + "</td><td id='Pen" + i + "'>" + pendiente + "</td>";

                        if (arreglo[i].catSeriesClaves.length > 0) {
                            info += '<td><button type="button" class="btn btn-warning btn-xs detalle" rel="' + i + '">&nbsp;Seriales</button></td>';
                        } else {
                            info += '<td><input type="text" size=3 maxlength=4 name="c' + i + '" id="c' + i + '" rel="' + i + '" class="validate[required,custom[integer],min[0],max[' + maximo + ']] saliendo"></td>';
                        }

                        info += "</tr>";
                    }


                    var objArticulo = {};

                    objArticulo.ArticuloClave = arreglo[i].ArticuloClave;
                    objArticulo.ArticuloTexto = arreglo[i].Nombre;
                    objArticulo.tieneseries = arreglo[i].TieneSeries;
                    objArticulo.Cantidad = arreglo[i].Cantidad;
                    objArticulo.Inventario = new Array();
                    objArticulo.Saliendo = 0;
                    objArticulo.Existencia = arreglo[i].Existencia;

                    objArticulos.push(objArticulo);

                }


                $("#articulostabledistribuidor tbody").append(info);


                $("#tt2").click();

                AfterAjax(); SalidaMaterialDetalle();

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });



}



function GuardaInfo() {
    var objDatos = {};
    var valid = jQuery('#formaeditadistribuidor').validationEngine('validate');
    if (valid) {
       
        var error = false;
        $(".saliendo").each(function () {
            var cual = $(this).attr('rel');
            objArticulos[cual].Saliendo = $(this).val();

            if (objArticulos[cual].Saliendo > objArticulos[cual].Existencia)
            {
                error = true;
                toastr.error('Error', 'No hay existencias suficientes para: ' + objArticulos[cual].ArticuloTexto);
                return;
            }
        });

        if ($.grep(objArticulos, function (n, i) {
            return ((n.tieneseries == 0 && n.Saliendo > 0) ||
                (n.tieneseries > 0 && n.Inventario.length > 0));
        }).length == 0) {
            toastr.error('Error', 'No puede hacer una salida sin productos');
            return false;
        }
        
        //aqui
        objDatos.Pedido = $("#ed_pedido").val();
        
        objDatos.Almacen = $("#ed_almacen").val();
        objDatos.TipoSalida = $("#ed_tiposalida").val();
        objDatos.Tecnico = $("#ed_solicitante").val();
        objDatos.Observaciones = $("#ed_observaciones").val();

        if (_salida != '')
        {
            objDatos.ed_id = _salida;
        }

        // Se comenta esta linea para depurar arreglo con articulos
        //objDatos.Inventario = objArticulos;
        objDatos.Inventario = $.grep(objArticulos, function (n, i) {
            return ((n.tieneseries == 0 && n.Saliendo > 0) ||
                (n.tieneseries > 0 && n.Inventario.length > 0));
        });

        var myJSONText = JSON.stringify(objDatos);

        /*alert(myJSONText);*/

        $("#loader").show();

        var url = '/SalidaMaterial/Guarda/';
        $.ajax({
            url: url,
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                $("#editadistribuidor").modal('hide');
                $("#eliframe").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + data);
                $('#ver').modal('show');


                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }
    else {
        //Mensaje('Error', 'Debes de llenar los campos obligatorios');
    }
}