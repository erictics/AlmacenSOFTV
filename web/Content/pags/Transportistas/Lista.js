﻿//transportistas

$(document).ready(function () {
    llenaLista()
});


function edita(cual) {

    $("#titulowin").html("Edita Transportista");
    $('#edita').modal('show');

    var url = '/Transportista/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {

            var oRol = eval(data);

            $("#ed_id").val(oRol.Clave);
            $("#ed_nombre").val(oRol.Descripcion);

            $("#ed_url").val(oRol.URLSeguimiento);

            $("[type=checkbox]").removeProp('checked');
            $("[type=checkbox]").parent().removeClass('checked');

            if (oRol.Activo === "1") {
                $("#ed_activa").prop('checked', 'checked');
                $("#ed_activa").parent().addClass('checked');
            }

            //LlenaGrid(_pag);

        },
        error: function (request, error) {
            toastr.error('Sucedio un error de conexión /su sesión ha expirado');
        }
    });
}




$("#add").click(function () {
    $("#titulowin").html("Agregar Transportista");
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');


    $("#ed_url").val('');

    $("[type=checkbox]").removeProp('checked');
    $("[type=checkbox]").parent().removeClass('checked');
    $("#ed_activa").prop('checked', 'checked');
    $("#ed_activa").parent().addClass('checked');
});



$("#formaedita").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        $("#loader").show();

        var url = '/Transportista/Guarda/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaedita').serialize(),
            type: "POST",
            success: function (data) {

                $("#edita").modal('hide');                
                toastr.success('El Transportista se guardó correctamente');
                llenaLista();
            },
            error: function (request, error) {
                toastr.error('Sucedio un error de conexión /su sesión ha expirado');
            }
        });

    }

});






function llenaLista() {

    var objRoles = {};
    objRoles.pag = 1;
    var myJSONText = JSON.stringify(objRoles);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/Transportista/ObtenLista/",
            "type": "POST",
            "data": {
                'data': myJSONText
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [

               { "data": "Descripcion", "orderable": false },

                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.Activo === "1") {
                              return "<img src='/Content/assets/img/activo.png'>";
                          }
                          else {
                              return "<img src='/Content/assets/img/inactivo.png'>";
                          }
                      }

                  },

                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {
                          return '<button type="button" class="btn btn-warning btn-xs edit" onClick="edita(\'' + full.Clave + '\')" ><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Editar</button>';

                      }

                  },




        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}