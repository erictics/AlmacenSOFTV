﻿var _Metodo = 0;
var nTotOrd = 0;
var nTotSur = 0;
var nPend = 0;
var idx_art = 0;
var idx_serie = 0;
var vbExiste = false;
var Tabla;

$(document).ready(function () {

    $("#formaeditadistribuidor").submit(function (e) {
        e.preventDefault();
        GuardaInfo();
    });

  
    


    $("#btn-cierra_edDPS_IR").click(function () {
        var idx = 0;
        var array = objArticulos[_indiceArticulo].Inventario;

        for (var foo = 0; foo < array.length; foo++) {
            if ($.inArray($("#ed_id_IR").val(), array[foo]) >= 0) {
                idx = foo;
                break;
            }
        }

        var vArr_ns = $("#edDetProdSer_IR table tbody select").val() + "|";
        var seriales = new Array();

        $("#newSerie .seriales").each(function () {
            var clave = $(this).attr("id");
            var valor = $(this).val();

            var serieInv = {};
            serieInv.Clave = clave;
            serieInv.Valor = valor.toUpperCase();

            seriales.push(serieInv);

            if (vArr_ns.substring(vArr_ns.length, 1) != "|") {
                vArr_ns += "~";
            }
            vArr_ns += $(this).val();
        });

        ValidaSerie(seriales);

        if (vbExiste) {
            return false;
        }

        objArticulos[_indiceArticulo].Inventario[idx][2] = vArr_ns;
    });
});

function SalidaMaterialDetalle() {

    $(".detalle").click(function () {

        var elrel = $(this).attr('rel');

        // Esta Variable nos servira para actualizar la tabla de resumen de Articulos
        idx_art = elrel;

        nTotOrd = parseInt($("#TotPed" + elrel).html());
        nTotSur = parseInt($("#TotSur" + elrel).html());
        nPend = nTotOrd - nTotSur;

        if (_Metodo == 0 && (_recepcion != "" && objArticulos[idx_art].Inventario.length == 0 && nPend == 0)) {
            toastr.error("Error", "El proceso actual no tiene recepciones del articulo seleccionado.");
            return;
        }

        if (nPend == 0)
        {
            $("#_DetProdSer-Busca").prop("disabled", true);
        }
        else
        {
            $("#_DetProdSer-Busca").removeAttr("disabled");
        }

        $("#resumen tbody").empty();
        var tbody = "<tr><td id='cellTO'>" + nTotOrd + "</td><td id='cellTS'>" + nTotSur + "</td><td id='cellP'>" + nPend + "</td></tr>";
        $("#resumen tbody").append(tbody);

        var objArticuloDetalle = {};

        if (_Metodo == 0) {
            objArticuloDetalle.Articulo = objArticulos[parseInt(elrel)].ArticuloClave;
            objArticuloDetalle.Proceso = objArticulos[parseInt(elrel)].ProcesoClave;
            objArticuloDetalle.Recepcion = _recepcion;
        }
        else if (_Metodo == 1 || _Metodo == 2) {
            objArticuloDetalle.articulo = objArticulos[parseInt(elrel)].ArticuloClave;
            objArticuloDetalle.opcion = _opcion;
            objArticuloDetalle.devolucion = objArticulos[parseInt(elrel)].ProcesoClave;
        }

        _indiceArticulo = elrel;

        var myJSONText = JSON.stringify(objArticuloDetalle);

        var url = "";

        if (_Metodo == 0) {
            url = "/RecepcionDistribuidor/Articulo/";
        }
        else if (_Metodo == 1 || _Metodo == 2) {
            url = "/Devolucion/Articulo/";
        }

        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                console.log(data);
                var arreglo = eval(data);
                _articulospedidos = arreglo;

                IniciaDetalle();
                IniciaContenido();

                $("#_DetProdSer-Titulo").html("Recepción Pedido - " + objArticulos[parseInt(elrel)].ArticuloTexto);
                $("#lblBusqueda").html("Capturar " + _articulospedidos[0].catSeriesClaves[0].Nombre + ":");
                $("#_DetProdSer-Busca").attr("placeholder", _articulospedidos[0].catSeriesClaves[0].Nombre);
                $("#edDetProdSer").modal("show");

                $("#_DetProdSer-Busca").focus();
            },
            error: function (request, error) {
                toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });

        $("#_DetProdSer-Busca").bind("keypress keydown keyup", function (e) {
            var texto = $(this).val();
            if (e.which == 13) {
                e.preventDefault();

                if (texto.length == 0) {
                    e.stopPropagation();
                    return false;
                }

                var idx = 0;
                var chB;
                var vbEncontrado = false;
                var rows = $("#lasseries tbody tr");

                for (var foo = 0; foo < rows.length; foo++) {
                    if ($(rows[foo]).attr("data-rel").indexOf(texto.toUpperCase()) > -1) {
                        chB = $("input[type='checkbox'][name*='cb']")[foo].id;
                        if ($("#" + chB).prop("checked")) {
                            $("#_DetProdSer-Busca").val("");
                            $("#_DetProdSer-Busca").focus();
                            return false;
                        }
                        nTotSur++;
                        nPend--;
                        $("#" + chB).prop("checked", "checked");
                        $("#" + chB).parent().addClass("checked");
                        $("#_DetProdSer-Busca").val("");
                        if (_Metodo != 2) {
                            objArticulos[_indiceArticulo].Inventario.push($("#" + chB).attr("rel"));
                        }
                        else {
                            objArticulos[_indiceArticulo].Inventario.push(new Array($(this).attr('rel'), "7", ''));
                        }
                        ActRes();

                        if (_Metodo == 2) {
                            $("#cbx" + $("#" + chB).attr('rel')).removeAttr("disabled");
                        }

                        if (nTotOrd == nTotSur) {
                            $(".inventarios:not(:checked)").prop("disabled", true);
                            $("#_DetProdSer-Busca").prop("disabled", true);
                        }
                        else {
                            $("#_DetProdSer-Busca").focus();
                        }

                        break;
                    }
                }
                e.stopPropagation();
                return false;
            }
        });
    });

    $("#btn_cierraSelArt").click(function () {
        $("#TotPed" + idx_art).html(nTotOrd);
        $("#TotSur" + idx_art).html(nTotSur);
        $("#Pen" + idx_art).html(nPend);
    });
}

function IniciaDetalle()
{
    $("#lasseries thead").empty();
    $("#lasseries tbody").empty();

    if (_articulospedidos[0] != undefined || _articulospedidos[0] != null) {
        var tiposeries = _articulospedidos[0].catSeriesClaves;
        var tr = "";
        tr += "<tr>";
        tr += "<th><input type='checkbox' id='selAll' name='selAll' class='AllSel'>&nbsp;&nbsp;" + ((_Metodo == 2) ? "Sel." : "Seleccionar") + "</th>";
        for (var ii = 0; ii < tiposeries.length; ii++) {
            tr += "<th>" + tiposeries[ii].Nombre + "</th>";
        }
        if (_Metodo == 1 || _Metodo == 2)
            tr += "<th>Estatus</th></tr>";
        else
            tr += "</tr>";
        $("#lasseries thead").append(tr);
    }
    else {
        $("#lasseries thead").append('<tr><td>No ha artículos en almacen</td></tr>');
    }

    $("#selAll").on("ifChanged", function (event) {
        $("input[type='checkbox'][ce='1']:not('.switch,.AllSel')").each(function () {
            $(this).iCheck(($("#selAll").prop("checked") ? "" : "un") + "check");
        });
    });
}

function IniciaContenido()
{
    var tbody = "";
    var vCanEdit = true;
    var AllSel = false;
    for (var ii = 0; ii < _articulospedidos.length; ii++)
    {
        vCanEdit = _articulospedidos[ii].Seriales[0].EstatusClave != "0";
        var cadena = '';
        tbody += "<tr data-rel='_remplazar_' " + ((!vCanEdit) ? "class='btn-info'" : "") + ">";
        tbody += "<td><input type='checkbox' id='cb" + _articulospedidos[ii].Clave + "' name='cb" + _articulospedidos[ii].Clave + "' rel='" + _articulospedidos[ii].Clave + "' ce='" + ((!vCanEdit) ? "0' disabled" : "1'") + " class='inventarios' _checked_ /></td>";
        for (var ij = 0; ij < _articulospedidos[ii].Seriales[0].datos.length; ij++)
        {
            tbody += "<td>" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "</td>";
            cadena += "|" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "|";
        }
        if (_Metodo == 1) {
            tbody += "<td>" + _articulospedidos[ii].Seriales[0].EstatusTexto + "</td>";
        }
        else if (_Metodo == 2) {
            tbody += "<td>" + CreaCtrlExtra(_articulospedidos[ii].Clave, ii, ((objArticulos[_indiceArticulo].Inventario.length > 0) ? objArticulos[_indiceArticulo].Inventario : null)) + "</td>";
        }
            
        tbody = tbody.replace("_remplazar_", cadena);
        var array = objArticulos[_indiceArticulo].Inventario;
        var i = -1;
        if (_Metodo == 2) {
            for (var foo = 0; foo < array.length; foo++) {
                if ($.inArray(_articulospedidos[ii].Clave, array[foo]) >= 0) {
                    i = foo;
                    break;
                }
            }
        }
        else {
            i = array.indexOf(_articulospedidos[ii].Clave);
        }
        if (i != -1) {
            tbody = tbody.replace("_checked_", "checked");
        }
        else {
            tbody = tbody.replace("_checked_", "");
        }
        tbody += "</tr>";
    }

    $("#lasseries tbody").append(tbody);

    $('input[type="checkbox"]:not(".switch")').iCheck({
        checkboxClass: 'icheckbox_square-blue ',
        increaseArea: '20%'
    });

    var vSel = 0;
    $(".inventarios[checked]").each(function () { vSel++; });

    if (vSel == $(".inventarios").length) {
        $("#selAll").iCheck("check");
    }

    $('input[type="checkbox"]:not(".switch,.AllSel")').on('ifChecked', function (event) {

        var cantidad = nTotOrd;
        if (cantidad > nTotSur) {
            if (_Metodo == 2) {
                objArticulos[_indiceArticulo].Inventario.push(new Array($(this).attr('rel'),"7",''));
            }
            else {
                objArticulos[_indiceArticulo].Inventario.push($(this).attr('rel'));
            }
            nTotSur++;
            nPend--;
            ActRes();

            if (_Metodo == 2) {
                $("#cbx" + $(this).attr('rel')).val(7);
                $("#cbx" + $(this).attr('rel')).removeAttr("disabled");
            }
        }
        if (cantidad == nTotSur)
        {
            $("#_DetProdSer-Busca").prop("disabled", true);
            $(".inventarios:not(:checked)").iCheck('disable');
        }
        else {
            $("#_DetProdSer-Busca").focus();
        }
    });

    $('input[type="checkbox"]:not(".switch,.AllSel")').on('ifUnchecked', function (event) {

        var cantidad = nTotOrd;
        var array = objArticulos[_indiceArticulo].Inventario;

        var i = -1;
        if (_Metodo == 2) {
            for (var foo = 0; foo < array.length; foo++) {
                if ($.inArray($(this).attr('rel'), array[foo]) >= 0) {
                    i = foo;
                    break;
                }
            }
        }
        else {
            i = array.indexOf($(this).attr('rel'));
        }
        
        if (i != -1) {
            objArticulos[_indiceArticulo].Inventario.splice(i, 1);
            nTotSur--;
            nPend++;
            ActRes();

            if (_Metodo == 2) {
                $("#cbx" + $(this).attr('rel')).val("");
                $("#cbx" + $(this).attr('rel')).prop("disabled", true);
                $("#btn_ingrem" + $(this).attr('rel')).prop("disabled", true);
            }
        }
        if (cantidad > nTotSur) {
            $("#_DetProdSer-Busca").removeAttr("disabled");
            $(".inventarios[ce='1']").iCheck('enable');
            $("#_DetProdSer-Busca").focus();
        }
    });

    $("select[id*='cbx']").change(function () {
        var v_id = this.id.substring(3, this.id.length);

        if ($(this).val() == 14) {
            $("#btn_ingrem" + v_id).removeAttr("disabled");
        }
        else {
            $("#btn_ingrem" + v_id).prop("disabled", true);
        }

        var idx = 0;
        var array = objArticulos[_indiceArticulo].Inventario;

        for (var foo = 0; foo < array.length; foo++) {
            if ($.inArray(v_id, array[foo]) >= 0) {
                idx = foo;
                break;
            }
        }

        objArticulos[_indiceArticulo].Inventario[idx][1] = $(this).val();
    });

    $("button[id*='btn_ingrem']").click(function () {
        $("#ed_id_IR").val(this.id.replace("btn_ingrem", ""));
        idx_serie = $(this).attr("rel");
        $("#edDetProdSer_IR h4").html("Captura Equipo Reemplazo - " + objArticulos[_indiceArticulo].ArticuloTexto);
        $("#edDetProdSer_IR").modal("show");
        ns_creaCap();
    });
}

function CreaCtrlExtra(idx_inv, idx, arrInv) {
    var estatus = "";

    if (arrInv != null) {
        for (var foo = 0; foo < arrInv.length; foo++) {
            if ($.inArray(idx_inv, arrInv[foo]) >= 0) {
                estatus = arrInv[foo][1];
                break;
            }
        }
    }

    var extraCtrl = "<select id='cbx" + idx_inv + "' class='form-control' style='width:130px;float:left;padding:0px;'" + ((estatus == "") ? " disabled" : "") + ">";
    $("#tmp_estatus option").each(function () {
        extraCtrl += "<option value='" + $(this).val() + "'" + (($(this).val() == estatus) ? " selected" : "") + ">" + $(this).text() + "</option>";
    });
    extraCtrl += "</select>";

    extraCtrl += "&nbsp;<button id='btn_ingrem" + idx_inv + "' type='button' title='Ingresar Equipo de Reemplazo' class='btn btn-default' rel='" + idx + "'";
    extraCtrl += ((estatus == "14") ? "" : " disabled") + ">&#10012;</button></td>";

    return extraCtrl;
}

function ns_creaCap() {
    var arrDatosNS = new Array();

    var arrInv = objArticulos[_indiceArticulo].Inventario;

    if (arrInv != null && arrInv.length > 0) {
        for (var foo = 0; foo < arrInv.length; foo++) {
            if ($.inArray($("#ed_id_IR").val(), arrInv[foo]) >= 0) {
                arrDatosNS = arrInv[foo][2].split("|");
                break;
            }
        }
    }

    $("#newSerie thead").empty();
    $("#newSerie tbody").empty();

    var tiposeries = _articulospedidos[0].catSeriesClaves;
    var tr = "";
    tr += "<tr>";
    tr += "<th>Estatus</th>";
    for (var ii = 0; ii < tiposeries.length; ii++) {
        tr += "<th>" + tiposeries[ii].Nombre + "</th>";
    }
    tr += "</tr>";
    $("#newSerie thead").append(tr);

    var tbody = "<tr>";
    tbody += "<td>&#9781;&nbsp;&#9781;&nbsp;&#9781;&nbsp;&#9781;&nbsp;&#9781;&nbsp;&#9781;&nbsp;&#9781;</td>";
    for (var foo = 0; foo < tiposeries.length; foo++) {
        tbody += "<td>" + _articulospedidos[idx_serie].Seriales[0].datos[foo].Valor + "</td>";
    }
    tbody += "</tr>";
    $("#newSerie tbody").append(tbody);

    tbody = "<tr>";
    tbody += "<td><select class='form-control validate[required]' id='estatusserial'>";
    tbody += "<option value=1>Buen Estado</option><option value=2>Dañado</option><option value=3>CRC</option><option value=4>Reparación</option></select></td>";
    for (var ii = 0; ii < tiposeries.length; ii++) {
        var ultimo = "";
        if (ii == tiposeries.length - 1) {
            ultimo = "ultimo";
        }
        tbody += "<td><input type='text' style='text-transform:uppercase;' class='" + ultimo + " form-control validate[required";
        if (tiposeries[ii].Hexadecimal == 'True') {
            tbody += ",custom[hexa]";
        }
        tbody += ",minSize[" + tiposeries[ii].Min + "]";
        tbody += ",maxSize[" + tiposeries[ii].Max + "]";
        tbody += "] seriales' id='s" + tiposeries[ii].Clave + "'";

        if (arrDatosNS[0] != "") {
            tbody += " value='" + arrDatosNS[1].split("~")[ii] + "'";
        }

        tbody += "></td>";
    }
    tbody += "</tr>";
    $("#newSerie tbody").append(tbody);

    if (arrDatosNS[0] != "") {
        $("#estatusserial").val(arrDatosNS[0]);
    }
}

function ActRes()
{
    // Limpiamos contenido
    $("#cellTS").html("");
    $("#cellP").html("");

    // Actualizamos Resumen
    $("#cellTS").html(nTotSur);
    $("#cellP").html(nPend);
}

function ValidaSerie(pSeries) {
    vbExiste = false;
    ValidaSerieInmediata(pSeries);
    ValidaSerieRemota(pSeries);
}

function ValidaSerieInmediata(seriales) {

    if (!vbExiste) {
        for (var i = 0; i < seriales.length; i++) {
            for (var j = 0; j < seriales.length; j++) {
                if (i != j) {
                    if (seriales[i].Valor.toUpperCase() == seriales[j].Valor.toUpperCase()) {
                        vbExiste = true;
                        if (vbExiste) {
                            toastr.error('Error', 'Los seriales no se pueden repetir');
                        }
                    }
                }
            }
        }
    }
}

function ValidaSerieRemota(serie) {

    if (!vbExiste) {
        var objSerieVal = {};
        objSerieVal.serie = serie;
        var myJSONText = JSON.stringify(objSerieVal);

        var url = '/Inventario/Serie/';
        $.ajax({
            async: false,
            cache: false,
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {
                vbExiste = eval(data);

                if (vbExiste) {
                    toastr.error('Error', 'Los seriales existen previamente en el inventario');
                }
            },
            error: function (request, error) {
                toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    }
}