﻿

$(document).ready(function () {

    llenaLista();

});

$("#ed_distr").change(function () {
    ShowAlmacenes($(this).val(), $("#ed_id").val());
});

$("#dis").change(function () {
    $("#ed_distr").val($("#dis").val());
    llenaLista();

});

$("#obten").click(function () {
    llenaLista();

});

$("#add").click(function () {
    $("#titulowin").html("Agregar Técnico");
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');
    $("[type=checkbox]").removeProp('checked');
    $("[type=checkbox]").parent().removeClass('checked');
    $("#ed_activa").prop('checked', 'checked');
    $("#ed_activa").parent().addClass('checked');

    $("#ed_distr").prop("disabled", false);

    ShowAlmacenes($("#ed_distr").val(), null);

});



$("#formaedita").submit(function (e) {
    e.preventDefault();

    $("#ed_distr").prop("disabled", false);
    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        

        var url = '/Tecnico/Guarda/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaedita').serialize(),
            type: "POST",
            success: function (data) {

                if (data == "-1") {
                    toastr.error('Este nombre de técnico ya esta registrado');
                  

                } else {

                    $("#edita").modal('hide');
                    toastr.success('El técnico se guardó correctamente');                  

                    llenaLista();


                }


            },
            error: function (request, error) {
                toastr.error('la sesión ha expirado');
            }
        });

    }

});

$("#formaalmacen").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        $("#loader").show();

        var url = '/Tecnico/Almacenes/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaalmacen').serialize(),
            type: "POST",
            success: function (data) {

                $("#almacen").modal('hide');
                //LlenaGrid(_pag);

                toastr.success('El usuario se guardó correctamente');
                
            },
            error: function (request, error) {
                toastr.error('la sesión ha expirado');
            }
        });

    }

});




$(".almacen").click(function () {
    $('#almacen').modal('show');

    var distribuidor = $(this).attr("data-distribuidor");
    var usuario = $(this).attr("rel");

    ShowAlmacenes(distribuidor, usuario);

});

function edita (cual){
    
    $("#titulowin").html("Edita Técnico");
    $("#ed_distr").prop("disabled", true);
    $('#edita').modal('show');

    var url = '/Tecnico/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {

            var oRol = eval(data);

            $("#ed_id").val(oRol.Clave);
            $("#ed_nombre").val(oRol.Nombre);

            $("#ed_puesto").val(oRol.catPuestosTecnicosClave);
            $("#ed_distr").val((oRol.catDistribuidorClave === "") ? "-1" : oRol.catDistribuidorClave);

            $("[type=checkbox]").removeProp('checked');
            $("[type=checkbox]").parent().removeClass('checked');

            if (oRol.Activo === "1") {
                $("#ed_activa").prop('checked', 'checked');
                $("#ed_activa").parent().addClass('checked');
            }

            ShowAlmacenes($("#ed_distr").val(), oRol.Clave);
        },
        error: function (request, error) {
            toastr.error('La sesión ha expirado');
        }
    });

}






function llenaLista() {

    var objRoles = {};
    objRoles.pag = 1;
    objRoles.dis = $("#dis").val();

    var myJSONText = JSON.stringify(objRoles);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/Tecnico/ObtenLista/",
            "type": "POST",
            "data": {
                'data': myJSONText
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [

               {
                   sortable: false,
                   "render": function (data, type, full, meta) {

                       if (full.Activo == 1) {
                            return full.Nombre;
                       }
                       else {
                           return "<s>" + full.Nombre + "</s>";
                       }
                   }

               },
                { "data": "PuestoTecnicos", "orderable": false },

                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          if (full.Activo === "1") {
                              return "<img src='/Content/assets/img/activo.png'>";
                          }
                          else {
                              return "<img src='/Content/assets/img/inactivo.png'>";
                          }
                      }

                  },

                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {
                          return '<button type="button" class="btn btn-warning btn-xs edit" onClick="edita(\'' + full.Clave + '\')" ><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Editar</button>';

                      }

                  },




        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}



function ShowAlmacenes(distribuidor, usuario) {

    $("#distribuidoralmacen_id").val(distribuidor);
    $("#tecnicoalmacen_id").val(usuario);

    var url = '/Almacen/ObtenPorDistribuidorTecnico/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': distribuidor + "|" + usuario },
        type: "POST",
        success: function (data) {

            $("#almacenes").empty();

            var arreglo = eval(data);

            var html = '';
            for (var i = 0; i < arreglo.length; i++) {
                html += "<div class='col-md-12'>";
                html += "<input type='checkbox' ";

                if (arreglo[i].Activo == "1") {
                    html += "checked"
                }

                html += " class='checkbox' name='almacen" + arreglo[i].Clave + "' id='almacen" + arreglo[i].Clave + "' /> ";
                html += arreglo[i].Nombre;
                html += "</div>";
            }

            $("#almacenes").append(html);

            $('input[type="checkbox"]:not(".switch")').iCheck({
                checkboxClass: 'icheckbox_square-blue ',
                increaseArea: '20%' // optional
            });

            //$('.checkbox').iCheckbox();
            //LlenaGrid(_pag);

        },
        error: function (request, error) {
            toastr.error('La sesión ha expirado');
        }
    });
}