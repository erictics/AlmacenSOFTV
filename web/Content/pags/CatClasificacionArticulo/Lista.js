﻿
$(document).ready(function () {
    llenaLista();
});





function llenaLista() {

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/ClasificacionArticulo/ObtenLista/",
            "type": "POST",
            "data": {
                'data': 1
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "Descripcion", "orderable": false },                 
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         
                         if (full.Activo == 1) {
                            return "<img src='/Content/assets/img/activo.png'>";
                         }
                         else {                            
                           return "<img src='/Content/assets/img/inactivo.png'>";
                         }
                         

                     }

                 },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {
                          var accion = '<button type="button" class="btn btn-xs ink-reaction btn-warning edit" onClick="edita(\'' + full.Clave + '\')" ><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button>';
                         
                          return accion;

                      }

                  }
                

        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}




$("#add").click(function () {
    $("#titulowin").html("Agregar Clasificación Artículo");
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');
});

function edita(cual) {

    $("#titulowin").html("Edita Clasificación Artículo");
    $('#edita').modal('show');

    var url = '/ClasificacionArticulo/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {

            var oRol = eval(data);

            $("#ed_id").val(oRol.Clave);
            $("#ed_nombre").val(oRol.Descripcion);

            $("[type=checkbox]").removeProp('checked');
            $("[type=checkbox]").parent().removeClass('checked');
            if (oRol.Activo === "1") {
                $("#ed_activa").prop('checked', 'checked');
                $("#ed_activa").parent().addClass('checked');
            }


            //LlenaGrid(_pag);

        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}








$("#formaedita").submit(function (e) {
    e.preventDefault();

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
    

    var url = '/ClasificacionArticulo/Guarda/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: $('#formaedita').serialize(),
        type: "POST",
        success: function (data) {

            $("#edita").modal('hide');
            toastr.success('La Clasificación Artículo se guardó correctamente')
           

            llenaLista();
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

    });