﻿$(document).ready(function () {

    LlenaGrid(_pag);


    $("#formaeliminarecepcion").submit(function (e) {

        e.preventDefault();

        CancelaRecepcion();

    })

    /// Pantalla Inicial
    $("#add").on("click", function () {
        _recepcionClave = "";
        _pedidoClave = "";
        $("#ed_id").val('');
        LimpiaVariables();
        AgregaConPedido();
    });
    $("#addsin").on("click", function () {
        _recepcionClave = "";
        _pedidoClave = "";
        $("#ed_id").val('');
        LimpiaVariables();
        AgregaSinPedido();
    });


    //Filtros Grid
    $("#tiporecepcion").on("change", function () {
        LlenaGrid(1);
    });

    $("#buscaproveedor").on("change", function () {
        LlenaGrid(1);
    });

    $("#buscar").on("click", function () {

        LlenaGrid(1);
    });

    ///Botones GRID Pantalla Inicial
    $("#datos").on("click", ".recepcionmaterial", function () {

        var elrel = $(this).attr('rel');
        $('#repo').click();
        $('#titulocanvas').empty().append("Detalle de Recepcion");
        $("#framecanvas").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + elrel);


        
        //$("#eliframerm").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + elrel);
        //$('#recepcionmaterial').modal('show');
    });

    $("#datos").on("click", ".pdfrecepcionmaterial", function () {
        var elrel = $(this).attr('rel');
        window.location.href = "/RecepcionMaterial/Detalle?pdf=1&p=" + elrel;
    });



    $("#datos").on("click", ".cancelarecepcion", function () {
        var elrel = $(this).attr('rel');

        _recepcionClave = elrel;
       Cancela_Ventana(elrel);
    });


    $("#datos").on("click", ".editarecepcionsp", function () {
        LimpiaVariables();
        var elrel = $(this).attr('rel');

        _recepcionClave = elrel;
        SinPedido_Edita(elrel);
    });

    $("#datos").on("click", ".editarecepcion", function () {
        LimpiaVariables();
        $("#datos2pedido tbody").empty();

        var elrel = $(this).attr('rel');
        var data_pedido = $(this).attr('data-pedido');
        var pedidonumero = $(this).attr('data-numeropedido');

        _pedidoClave = data_pedido;
        _recepcionClave = elrel;

        ConPedido_Edita(elrel, data_pedido, pedidonumero);

    });

    $("#datos").on("click", ".pdf", function () {
        var elrel = $(this).attr('rel');
        window.location.href = "/Pedido/Detalle?pdf=1&p=" + elrel;

    });
    
    /// Pantalla Sin Pedido
    $("#articulostable").on("click", ".eliminasinpedido", function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');

        //Articulos completos eliminados
        _eliminaLineas.push(_objArticulos[_indiceArticulo]);
        _objArticulos.splice(_indiceArticulo, 1);

        SinPedido_EliminaArticulo();
    });

    $("#articulostable").on("click", ".editasinpedido", function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');
        SinPedido_EditaArticulo(elrel, _indiceArticulo);
    });


    $("#ed_clasificacionmaterial").change(function () {
        $("#ed_tipomaterial").val('');
        $(".optiontipo").each(function () {
            if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val()) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });

    //Selecciona el tipo de Material
    $("#ed_tipomaterial").change(function () {
        var tipo = $(this).val();
        SeleccionaTipoArticulo(tipo);
    });

    //Selecciona Material (Articulo)
    $("#ed_articulosinpedido").change(function () {
        SeleccionaMaterial();
    });

        //Agrega los seriales
        $("#addartsp").on("click", function () {
            AgregaSeriales();
            $("#lasseriessp .seriales:first").focus();
        });
   

        $("#lasseriessp").on("click", ".delserial", function () {

            var indice = $(this).attr('rel');

            if (_indiceArticulo != -1) {
                if (_objArticulos[_indiceArticulo].Eliminados == undefined || _objArticulos[_indiceArticulo].Eliminados == null) {
                    _objArticulos[_indiceArticulo].Eliminados = new Array();
                }
                _objArticulos[_indiceArticulo].Eliminados.push(_seriales[indice]);
            }
            _seriales.splice(indice, 1);
            IniciaDetalle(_conpedido);
        });


    //Agrega articulo a Recepción Sin Pedido
        $("#agregararticulo").on("click", function () {
           
                SinPedido_AgregaArticulo_Guarda();  
        });

        $("#cerrarArticulo").on("click", function () {
            
            $('#articulo').modal('hide');
            ActualizaLista();
        });


       



    //Agrega articulo a Recepción Sin Pedido carga masiva
    $("#agregararticulo_carga_masiva").on("click", function () {



        if ($("#mas_clasificacion").val() == "" || $("#mas_clasificacion").val() == null) {
            alert($("#mas_clasificacion").val());            
        }
       else if ($("#mas_tipo_articulo").val() == "" || $("#mas_tipo_articulo").val() == null) {
            toastr.error("Necesita seleccionar un tipo de artículo");          
        }
       else if ($("#mas_articulo").val() == "" || $("#mas_articulo").val() == null) {
            toastr.error("Necesita seleccionar un artículo");           
        }        
       else if ($("#mas_precio").val() == "" || $("#mas_precio").val() == null) {            
            toastr.error("Necesita asignar un precio");
        }
       else if ($("#mas_moneda").val() == "" || $("#mas_moneda").val() == null) {            
            toastr.error("Necesita seleccionar un tipo de moneda");            
        }
      else  if ($("#almacensinpedido").val() == "" || $("#almacensinpedido").val() == null) {
           
            toastr.error("Necesita seleccionar un almacén");
            
        }
      else  if ($("#proveedorsinpedido").val() == "" || $("#proveedorsinpedido").val() == null) {
           
            toastr.error("Necesita seleccionar un proveedor");
            
        }
      else  if ($("#tipocambiosinpedido").val() == "" || $("#tipocambiosinpedido").val() == null) {
           
            toastr.error("Necesita asignar un tipo de cambio");
            
        }
      else  if ($("#ivasinpedido").val() == "" || $("#ivasinpedido").val() == null) {
            
            toastr.error("Necesita asignar IVA");
           
        }
       else if ($("#descuentosinpedido").val() == "" || $("#descuentosinpedido").val() == null) {
            toastr.error("Necesita asignar IVA");
        }
      
       else if ($("#almacensinpedido").val() != "" && $("#proveedorsinpedido").val() != "" && $("#mas_clasificacion").val() != ""
            && $("#mas_tipo_articulo").val() != ""  && $("#mas_articulo").val() != ""
            && $("#mas_precio").val() != "" && $("#mas_moneda").val() != "") {
       
            if ($('#existen').val() == 'existe') {
                toastr.error("No puede agregar estas series, estas ya existen en el inventario");
            
            $('#cm').modal('hide');
        }
        else {
            SinPedido_AgregaArticulo_Guarda_carga_masiva();
        }

       }
       else {
           toastr.error("No se pudieron agregar las series");
       }
    });

    //Guarda Sin Pedido
    $("#guardasinpedido").on("click", function (e) {
        SinPedido_Guarda();
    });

   
    



    //Pantalla Con pedido
    $("#buscarproveedor").change(function () {
        BuscaPedidos();
    });

    $("#buscarpedido").click(function () {
        BuscaPedidos();
    });

    $("#datospedido").on("click",".seleccionarpedido",function () {
        _pedidoClave = $(this).attr('rel');
        var numeropedido = $(this).attr('data-numeropedido');
        ObtenPedido($(this).attr('rel'), -1, numeropedido);
    });
    
 
    //

    $("#agregararticulosinpedido").on("click", function () {
        SinPedido_AgregaArticulo();
    });

    $("#btnp").on("click", function () {
        SinPedido_AgregaArticulo_carga_masiva();
    });
    
   

   


    $("#datos2pedido").on("click", ".recibir", function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');

        var nombreproducto = $(this).attr('data-indice');

        ConPedido_Recibe(_indiceArticulo);
    });



    $("#datos2pedido").on("click", ".recibircm", function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');
        $('#ed_asurtircm').val('');
        $('#inputFile').val('');        
        $('#id_tbody_tabla_carga_masiva_con_orden').empty();
        var nombreproducto = $(this).attr('data-indice');

        ConPedido_Recibecm(_indiceArticulo);
    });
    
    $("#datos2pedido").on("keyup", ".recibegenerico", function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');

        ConPedido_RecibeGenerico(_indiceArticulo);
    });

    $("#ed_asurtir").keyup(function () {
        CalculaAbajo();
    });

    $("#detalle").on("click", function () {
        ConPedido_DetalleArticulos();
        Lectora();
    });

    //Agrega los seriales
    $("#lasseries").on("click", "#addart", function () {
        ConPedido_AgregaSeriales();
        $("#lasseries .seriales:first").focus();
    });

    $("#lasseries").on("click", ".delserial", function () {
        var indice = $(this).attr('rel');


        if (_objArticulos != null && _objArticulos != undefined) {

            console.log(_objArticulos);
            console.log(_indiceArticulo);
            console.log(indice);
            console.log(indice);

            if (_objArticulos[_indiceArticulo].Eliminados == undefined || _objArticulos[_indiceArticulo].Eliminados == null) {
                _objArticulos[_indiceArticulo].Eliminados = new Array();
            }
            _objArticulos[_indiceArticulo].Eliminados.push(_seriales[indice]);
            _seriales.splice(indice, 1);
            _objArticulos[_indiceArticulo].Series = _seriales.slice(0);
            _objArticulos[_indiceArticulo].Surtiendo = parseInt(_seriales.length);

            pendiente = parseInt(_objArticulos[_indiceArticulo].Cantidad) - parseInt(_objArticulos[_indiceArticulo].Surtida) - parseInt(_seriales.length);
            $("#surtiendo_" + _indiceArticulo).html(_seriales.length);
            $("#pendiente_" + _indiceArticulo).html(pendiente);
            var monto = _seriales.length * _objArticulos[_indiceArticulo].PrecioUnitario;
            $("#importe_" + _indiceArticulo).html(ToMoneda(monto));
        }

        CalculaAbajo();

        IniciaDetalle(_conpedido);
    });



    $("#datos2pedido").on("click", ".limpia", function () {
        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');
        ConPedido_Limpia(_indiceArticulo);
    });


    //Guarda Sin Pedido
    $("#guarda").on("click", function (e) {
        ConPedido_Guarda();
    });

});

function Lectora() {


    $('#lasseries').on('keypress', '.seriales', function (e) {
        if (e.keyCode == 13) {

            if ($(this).hasClass('ultimo')) {
                //e.preventDefault();

                ConPedido_AgregaSeriales();
                $("#lasseries .seriales:first").focus();

                return;
            }


            var $targets = $('#lasseries').find('.seriales'),
                steps = $targets.map(function () {
                    return $(this).attr('tabindex');
                }).get();


            var current = $.inArray($(this).attr('tabindex'), steps),
                next = steps[++current % steps.length];
            $targets.filter('[tabindex="' + next + '"]').focus();

        }
    });

    $('#lasseriessp').on('keypress', '.seriales', function (e) {
        if (e.keyCode == 13) {

            if ($(this).hasClass('ultimo')) {
                e.preventDefault();
                $("#addartsp").click();
                $("#lasseriessp .seriales:first").focus();
                return;
            }


            var $targets = $('#lasseriessp').find('.seriales'),
                steps = $targets.map(function () {
                    return $(this).attr('tabindex');
                }).get();


            var current = $.inArray($(this).attr('tabindex'), steps),
                next = steps[++current % steps.length];
            $targets.filter('[tabindex="' + next + '"]').focus();

            if ($("*:focus").attr('id') == 'addartsp') {
                $(this).click();
            }
            //
        }
    });

}