﻿

var _objRecepcion = new Array();
var _objRecepcionInicial = new Array();

var _objSurtidas = new Array();
var _objSurtiendo = new Array();

var _objArticulos = new Array();

var _articulospedidos = null;

var _pedido = null;

var _conpedido = true;

var _pedidoClave = '';
var _recepcionClave = '';


$(document).ready(function () {


   
      LlenaGrid(1);

    // 1.- funcion llenar grid LLENA LA TABLA CUANDO EL DOCUMENTO SE CARGA
    //2.- rm-funciones.js
    ////Pedidos en General
    //  function LlenaGrid(pag) {
    //3.- se obtiene la tabla con botones de opciones
    //4-funcion rm-botones.js 
    //function DamePedido(elid, numero) {
    //5.- funcion dame detalle( boton pdf)
    //6.-function DamePedido(boton ver orden )
    //7.- function DameDetalle(boton ver detalle,editar,cancelar,pdf)
    //8.-

     





    jQuery('#formaedita').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false
    });

    $("#establecelugar").click(function () {
        $("#at1").click();
    });


    $("#at1").click(function () {
        $("#establecelugar").hide();

        $("#selecciona").hide();
        $("#guarda").hide();

        _objRecepcion = new Array();
        _objRecepcionInicial = new Array();

        _objSurtidas = new Array();

        _objArticulos = new Array();

        _articulospedidos = null;

        _pedido = null;

    });

    $("#at0").click(function () {
        //$("#establecelugar").show();
    });

    $("#selecciona").click(function () {

        $("#selecciona").hide();
        $("#guarda").removeClass("hide");
        $("#guarda").show();



    });

    $(".disabled").click(function (e) {
        if ($(this).hasClass("disabled")) {

            Mensaje('Error', 'Debe seleccionar una Orden de Compra');

            e.preventDefault();
            return false;
        }
    });

    $("#buscarproveedor").change(function () {
        BuscaPedidos();
    });


    $("#buscarpedido").click(function () {
        BuscaPedidos();
    });


    $(".preciootrosgastos").blur(function () {
        Resumen();
    });

    
    $("#agregar").click(function () {
        $('#articulo').modal('show');

        
        $("#ed_clasificacionmaterial").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_articulo").val('');
        $("#ed_cantidad").val('');
        $("#ed_precio").val('');
        $("#ed_moneda").val('');

    });

    $(".optiontipo").hide();

    $("#ed_clasificacionmaterial").change(function () {

        $("#ed_tipomaterial").val('');

        $(".optiontipo").each(function () {
            if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val()) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });

   

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    //agrega una nueva recepcion
    //boton donde se recibe el pedido
    $("#add").click(function () {


        _recepcionClave = '';
        
        $("#at0").click();
        $("#at2").addClass('disabled');
        $("#titulowin").html("Recibir Material");

        $("#buscarproveedor").val('-1');

        $("#datospedido tbody").empty();
        

        $(".nav-tabs a").first().click();
        _objArticulos = new Array();
        $('#edita').modal('show');

        $("#t0").removeClass('active');


        
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Articulo/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {

                $("#elimina").modal('hide');
                Mensaje($("#titulowin").html(), 'El artículo se eliminó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    });





    $("#guarda").click(function (e) {
        e.preventDefault();

        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {

            $("#guarda").hide();
            var objRecepcionMaterial = {};

            var totalsurtido = 0;
            for (var i = 0; i < _objRecepcion.length; i++)
            {
                try
                {
                    totalsurtido += parseInt(_objRecepcion[i].length);
                }
                catch(e)
                {
                
                }
            }

            $(".recibegenerico").each(function () {
                try
                {
                    totalsurtido += parseInt($(this).val());
                }
                catch(e)
                {
                
                }
            });


            if (totalsurtido == 0)
            {
                Mensaje('Error', 'la recepción debe de tener al menos un artículo');
                return;
            }


            if ($("#ed_idsinpedido").val() != null && $("#ed_idsinpedido").val() != '') {
                objRecepcionMaterial.ed_id = $("#ed_idsinpedido").val();
            }

            if ($("#ed_id").val() != null && $("#ed_id").val() != '') {
                objRecepcionMaterial.ed_id = $("#ed_id").val();
            }


            //alert(_pedido);

            objRecepcionMaterial.Almacen = $("#almacen").val();
            objRecepcionMaterial.ProcesoClaveRespuesta = _pedidoClave;
            objRecepcionMaterial.Recepcion = _objRecepcion;
            objRecepcionMaterial.Articulos = _articulospedidos;

            var myJSONText = JSON.stringify(objRecepcionMaterial);

            $("#loader").show();

            var url = '/RecepcionMaterial/Guarda/';
            $.ajax({
                url: url,
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {
                    $("#eliframe").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + data);
                    $('#ver').modal('show');
                    $("#edita").modal('hide');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    Mensaje('Surgió un error', 'intente nuevamente más tarde');
                }
            });
        }
        else {
            Mensaje('Error', 'debe llenar los campos obligatorios');
        }

    });





});


//function Resumen() {

//    var suma = 0;
//    var tipocambio = 1;

//    for (var i = 0; i < _objArticulos.length; i++) {

//        if(_objArticulos[i].MonedaTexto.indexOf('Dólar')>-1)
//        {
//            tipocambio = parseFloat($("#ed_tipocambio").val());
//        }

//        var totalproducto = parseFloat(_objArticulos[i].PrecioUnitario) * parseInt(_objArticulos[i].Cantidad);
//            totalproducto = totalproducto * tipocambio;
//            suma += totalproducto;
//    }

//    if ($("#ed_preciooc1").val() != '') {
//        suma += parseFloat($("#ed_preciooc1").val());
//    }

//    if ($("#ed_preciooc2").val() != '') {
//        suma += parseFloat($("#ed_preciooc2").val());
//    }


//    var eliva = parseFloat($("#ed_iva").val());

//    var descuento = 0;

//    var descuento_po = 0;
//    try
//    {
//        if ($("#ed_descuento").val() != "") {
//            descuento_po = parseFloat($("#ed_descuento").val());
//            descuento = descuento_po * suma / 100;
//        }
//    }
//    catch (e)
//    { }

//    var subtotaldescuento = suma - descuento;
//    var iva = (subtotaldescuento * eliva / 100);
//    var total = subtotaldescuento + (subtotaldescuento * eliva / 100);


//    $("#to_subtotal").val(suma);
//    $("#to_descuento").val(descuento);
//    $("#to_iva").val(iva);
//    $("#to_total").val(total);


//    $("#reiva_po").html("(" + $("#ed_iva").val() + "%" + ")");
//    $("#reiva").html(ToMoneda(iva) + "%");

//    $("#resubtotal").html(ToMoneda(suma));
//    $("#redescuento").html(ToMoneda(descuento));
//    $("#redescuento_po").html("(" + descuento_po + "%"+")");
//    $("#retotal").html(ToMoneda(total));

//}


//function ActualizaLista()
//{
//    $("#articulostable tbody").empty();

//    for (var i = 0; i < _objArticulos.length; i++) {

//        var total = parseFloat(_objArticulos[i].PrecioUnitario) * parseInt(_objArticulos[i].Cantidad);

//        var renglon = "<tr><td>" + _objArticulos[i].ArticuloTexto + "</td><td>" + _objArticulos[i].Cantidad + "</td><td>" + ToMoneda(_objArticulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td><td>" + _objArticulos[i].MonedaTexto + "</td><td>" + DameAccionesArticulo(i) + "</td></tr>"
//        $("#articulostable tbody").append(renglon);
//    }

//    Resumen();
//    AfterAjax();
//}






function BuscaPedidos() {
    var objRoles = {};
    objRoles.pedido = $("#pedidoseleccionar").val();
    objRoles.proveedor = $("#buscarproveedor").val();
    objRoles.pag = -1;
    objRoles.act = "1";

    var myJSONText = JSON.stringify(objRoles);

    $("#datospedido").show();
    $("#datospedido tbody").empty();
    $("#datos2pedido tbody").empty();

    $("#selecciona").hide();

    var url = '/RecepcionMaterial/Pedidos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {


            var info = "";
            var info2 = "";

            var arreglo = eval(data);
            _articulospedidos = arreglo;


            _pedido = objRoles.pedido;

            for (var i = 0; i < arreglo.length; i++) {

                arreglo[i].Estatus = arreglo[i].Estatus.replace('Autorizado', 'Pendiente por surtir');

                var renglon = "<tr><td>" + arreglo[i].NumeroPedido + "</td><td>" + arreglo[i].Fecha + "</td><td>" + arreglo[i].Proveedor + "</td><td>" + arreglo[i].Estatus + "</td>";

                if (arreglo[i].Estatus == 'Por autorizar' || arreglo[i].Estatus == "Completo" || arreglo[i].Estatus == "Rechazado") {
                    renglon += "<td></td>";
                }
                else {
                    renglon += "<td>" + DetalleSelecciona(arreglo[i].Clave, i) + "</td>";
                }

                renglon += "</tr>"

                $("#datospedido tbody").append(renglon);
            }


            /*
            for (var i = 0; i < arreglo.length; i++) {
                info2 += "<tr>" + "<td>" + arreglo[i].Nombre + "</td>" + "<td>" + arreglo[i].Cantidad + "</td>" + "<td id='surtida_" + i + "'>" + arreglo[i].Surtida + "</td>" + "<td>" + DetalleRecepcion(arreglo[i].Clave, i) + "</td>" + "</tr>";

                _objRecepcion.push(null);
                _objRecepcion[i] = arreglo[i].Seriales;
                _objSurtidas.push(null);
                _objSurtidas[i] = arreglo[i].Seriales.length;
            }

            $("#datospedido tbody").append(info);
            $("#datos2pedido tbody").append(info2);
            */


            AfterAjax();

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}


var _pag = 1;
function LlenaGrid(pag)
{
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;

    var myJSONText = JSON.stringify(objRoles);

    $("#datos tbody").empty();
    $("#loader").show();
    var url = '/RecepcionMaterial/ObtenLista/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#loader").hide();

            var arreglo = eval(data);

            if (arreglo.length > 0) {
                $("#resultados").html("");
                $("#datos").show();
            }
            else {
                $("#resultados").html("No se encontraron resultados");
            }


            for (var i = 0; i < arreglo.length; i++) {
                var renglon = "<tr><td>" + arreglo[i].NumeroPedido + "</td><td>" + arreglo[i].Fecha + "</td><td>" + arreglo[i].Destino + "</td>"

                if (arreglo[i].ClavePedido != "") {
                    renglon += "<td>" + DamePedido(arreglo[i].ClavePedido, arreglo[i].Pedido) + "</td>";
                }
                else {
                    renglon += "<td></td>";
                }



                renglon += "<td>" + DameDetalle(arreglo[i].Clave, arreglo[i].ClavePedido) + "</td></tr>"
                $("#datos tbody").append(renglon);
            }

            AfterAjax();
        },
        error: function (request, error) {
            Mensaje('Surgió un error', ' intente nuevamente más tarde');
        }
    });


    url = '/RecepcionMaterial/ObtenPaginas/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            LlenaPaginacion(data);
        },
        error: function (request, error) {
            Mensaje('Surgió un error', ' intente nuevamente más tarde');
        }
    });
}

var _indiceArticulo = -1;
function AfterAjax() {

    

    $(".seleccionarpedido").click(function () {

        _pedidoClave = $(this).attr('rel');
        ObtenPedido($(this).attr('rel'),-1);
    });

    $(".limpia").click(function () {

        //alert("a");
        var serialeslongitud = 0;

        try {
            serialeslongitud = _objRecepcion[_indiceArticulo].length;
        }
        catch (e) {
        }


        var elrel = $(this).attr('rel');
        _indiceArticulo = $(this).attr('data-indice');

        _articulospedidos[_indiceArticulo].Surtiendo = 0;
        _objRecepcion[_indiceArticulo] = _objRecepcionInicial[_indiceArticulo].slice(0);

        $("#ed_surtida").val(_articulospedidos[_indiceArticulo].Surtiendo);
        var pendiente = parseInt(_articulospedidos[_indiceArticulo].Cantidad) - parseInt(_articulospedidos[_indiceArticulo].Surtida);
        $("#pendiente_" + _indiceArticulo).html(pendiente);

        $("#surtiendo_" + _indiceArticulo).html(_articulospedidos[_indiceArticulo].Surtiendo);

        var monto = _articulospedidos[_indiceArticulo].Surtiendo * _articulospedidos[_indiceArticulo].PrecioUnitario;
        $("#importe_" + _indiceArticulo).html(ToMoneda(monto));

    });


    $(".precio").click(function () {
        _indiceArticulo = $(this).attr('data-indice');
        $("#precio").modal('show');
        $("#ed_precio_edita").val(ToMoneda(_articulospedidos[_indiceArticulo].PrecioUnitario).replace('$', ''));
    });


    $("#guardaprecio").click(function () {
        _articulospedidos[_indiceArticulo].PrecioUnitario = $("#ed_precio_edita").val();
        _articulospedidos[_indiceArticulo].PrecioUnitarioTexto = ToMoneda(_articulospedidos[_indiceArticulo].PrecioUnitario);

        ActualizaProductosPedidos();
        $("#precio").modal('hide');
    });

    $(".recibir").click(function () {

        $("#camposagrega").show();

        var elrel = $(this).attr('rel');



        _indiceArticulo = $(this).attr('data-indice');

        var cantidad = _articulospedidos[_indiceArticulo].Cantidad;
        var surtida = _articulospedidos[_indiceArticulo].Surtida;
        var pendiente = cantidad - surtida;
        var clave = _articulospedidos[_indiceArticulo].Clave;

        $("#guardarsurtir").hide();
        $("#detalle").hide();
        $("#lasseries").hide();

        if (_articulospedidos[_indiceArticulo].catSeriesClaves == '') {
            $("#guardarsurtir").removeClass("hide");
            $("#guardarsurtir").show();
        }
        else {
            $("#detalle").removeClass("hide");
            $("#detalle").show();
            $("#lasseries").show();
        }


        $("#lasseries tbody").empty();
        $("#lasseries").hide();
        //alert(articuloclave);

        $("#ed_solicitada").val(cantidad);
        $("#ed_pendiente").val(pendiente);
        $("#ed_articulo").val(elrel.split('|')[1]);

        $("#ed_asurtir").val('');

        if (_objSurtidas[_indiceArticulo] != null) {
            $("#ed_surtida").val(_articulospedidos[_indiceArticulo].Surtiendo);
            //$("#lasseries").show();
            IniciaDetalle(true);
        }
        else {
            $("#ed_surtida").val(_articulospedidos[_indiceArticulo].Surtiendo);
        }


        if (_articulospedidos[_indiceArticulo].catSeriesClaves == '') {
        }
        else {
            $("#lasseries").show();
        }


        //$("#rechaza_id").val(elrel);
        $('#recibir').modal('show');




    });

    $(".rechaza").click(function () {
        var elrel = $(this).attr('rel');
        $("#rechaza_id").val(elrel);
        $('#rechaza').modal('show');
    });

    $(".autoriza").click(function () {
        var elrel = $(this).attr('rel');
        $("#autoriza_id").val(elrel);
        $('#autoriza').modal('show');
    });

    $(".ver").click(function () {

        var elrel = $(this).attr('rel');
        $("#eliframe").attr("src", "/Pedido/Detalle?pdf=2&p=" + elrel);

        $('#ver').modal('show');
    });

    $('#recepcionmaterial').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });

    $(".recepcionmaterial").click(function () {
        var elrel = $(this).attr('rel');
        $("#eliframerm").attr("src", "/RecepcionMaterial/Detalle?pdf=2&p=" + elrel);
        $('#recepcionmaterial').modal('show');
    });

    $(".pdf").click(function () {
        var elrel = $(this).attr('rel');
        window.location.href = "/Pedido/Detalle?pdf=1&p=" + elrel;
    });

    $(".pdfrecepcionmaterial").click(function () {
        var elrel = $(this).attr('rel');
        window.location.href = "/RecepcionMaterial/Detalle?pdf=1&p=" + elrel;
    });

    $(".delarticulo").click(function () {
        var cual = $(this).val();
        //alert(cual);
        _objArticulos.splice(index, 1);
        ActualizaLista();
    });

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Elimina artículo");
        $('#elimina').modal('show');

        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#del_id").val(oRol.Clave);
                $("#titulowindel").html("Elimina artículo " + oRol.Descripcion);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                Mensaje('Surgió un error', ' intente nuevamente más tarde');
            }
        });

    });


    $(".editarecepcionsp").click(function () {
        alert('Implementando');
    });

    //boton editar en recepcion de pedidos
    //
    //
    //
    //


    $(".editarecepcion").click(function () {
        // se asigna id recepcion 
        var recepcion = $(this).attr('rel');
        var pedido = $(this).attr('data-pedido');

        _recepcionClave = recepcion;
        _pedidoClave = pedido;

        $("#ed_id").val(_recepcionClave);

        ObtenPedido(pedido, recepcion)
        

        $('#edita').modal('show');

        $("#datos2pedido tbody").empty();

        $("#selecciona").hide();

        $("#at1").parent().hide();
        $("#at2").parent().show();
        $("#at0").parent().show();

        $(".active").removeClass('fade');
        $(".active").removeClass('active');


        $("#t2").removeClass('fade');
        $("#t2").addClass('active');
        $("#at2").removeClass('hide');
        $("#at2").removeClass('disabled');
        $("#at2").click();



        $("#guarda").removeClass('hide');
        $("#guarda").show();




    });

    $(".edit").click(function () {

        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Rol");
        $('#edita').modal('show');

        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oRol = eval(data);

                $("#ed_id").val(oRol.Clave);
                $("#ed_nombre").val(oRol.Descripcion);

                $("#ed_existencias").val(oRol.Existencias);
                $("#ed_tope").val(oRol.TopeMinimo);

                $("#ed_unidad").val(oRol.catUnidadClave);
                $("#ed_porcentaje").val(oRol.Porcentaje);

                $("#ed_unidad").val(oRol.catUnidadClave);

                $("#ed_tipomaterial").val(oRol.catTipoArticuloClave);
                $("#ed_clasificacionmaterial").val(oRol.catClasificacionMaterialClave);


                $("#ed_ubicacion").val(oRol.catUbicacionClave);

                $("#ed_proveedor1").val(oRol.proveedor1);
                $("#ed_precio1").val(oRol.precio1);
                $("#ed_moneda1").val(oRol.moneda1);

                $("#ed_proveedor2").val(oRol.proveedor2);
                $("#ed_precio2").val(oRol.precio2);
                $("#ed_moneda2").val(oRol.moneda2);

                $("#ed_proveedor3").val(oRol.proveedor3);
                $("#ed_precio3").val(oRol.precio3);
                $("#ed_moneda3").val(oRol.moneda3);

                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}

function ObtenPedido(idpedido,idrecepcion)
{
    var objRoles = {};
    objRoles.pedido = idpedido;
    objRoles.recepcion = idrecepcion;

    var myJSONText = JSON.stringify(objRoles);

    $("#datos2pedido tbody").empty();

    $("#selecciona").hide();

    $("#at1").parent().hide();
    $("#at2").parent().show();
    $("#at0").parent().show();

    $("#guarda").removeClass('hide');
    $("#guarda").show();

    $("#loaderseleccionarpedido").show();
    $(".seleccionarpedido").hide();

    var url = '/Pedido/Articulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#loaderseleccionarpedido").hide();

            if (data.indexOf('-2:') > -1) {
                Mensaje('Error', 'El pedido no ha sido autorizado');
                return;
            }

            if (data.indexOf('-3:') > -1) {
                Mensaje('Error', 'Error desconocido');
                return;
            }


            var arreglo = eval(data);
            _articulospedidos = arreglo;
            _pedido = objRoles.pedido;

            ActualizaProductosPedidos();

            $(".nav li").removeClass("active");
            $(".nav li a").removeClass("disabled");
            $("#at2").click();

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}

function DameAccionesArticulo(cual) {
    return "<button type='button' class='btn btn-danger btn-xs delarticulo' rel='" + cual + "'>&nbsp;Elimina</button>";
}

//ver detalles del pedido 
function DamePedido(elid,numero) {
    return "<button type='button' class='btn btn-default btn-xs ver' rel='" + elid + "'>&nbsp;Ver Orden (" + numero + ")</button><button type='button' class='btn btn-info btn-xs pdf' rel='" + elid + "'>&nbsp;PDF</button>";
}


// selecciona acciones de editar o cancelar pedidos

function DameDetalle(elid, pedido) {

    var regresa = "<button type='button' class='btn btn-default btn-xs recepcionmaterial' rel='" + elid + "'>&nbsp;Ver Detalle</button><button type='button' class='btn btn-info btn-xs pdfrecepcionmaterial' rel='" + elid + "'>&nbsp;PDF</button>";

    if (pedido == '' || pedido == null) {
        regresa += "<button type='button' class='btn btn-warning btn-xs editarecepcionsp' rel='" + elid + "' data-pedido='" + pedido + "'>&nbsp;Editar</button>";
    }
    else {
        regresa += "<button type='button' class='btn btn-warning btn-xs editarecepcion' rel='" + elid + "' data-pedido='" + pedido + "'>&nbsp;Editar</button>";
    }

    return regresa;
}

function DameAcciones(elid) {
    return "<button type='button' class='btn btn-info btn-xs autoriza' rel='" + elid + "'>&nbsp;Recibir Material</button>";
}


function DetalleSelecciona(elid, i) {
    return "<button type='button' data-indice='" + i + "' class='btn btn-info btn-xs seleccionarpedido' rel='" + elid + "'>&nbsp;Seleccionar Orden de Compra</button>"
    + "<img src='/Content/assets/images/ajax-loader.gif' style='margin-right:10px;display:none;float:right' id='loaderseleccionarpedido'>";
}
//
function DetalleRecepcion(tieneseries,elid,i,max) {
    var regresa = "";    
    if (tieneseries > 0) {
        regresa += "<button type='button' data-indice='" + i + "' class='btn btn-info btn-xs recibir' rel='" + elid + "'>&nbsp;Recibir Material</button>";
    }
    else {
        regresa += "<input type='' size=2 id='qty_"+ i +"' data-indice='" + i + "' rel='" + elid + "' class='recibegenerico validate[custom[integer],min[0],max[" + max + "]]'>";
    }

    return regresa;
}

function LimpiaRecepcion(tieneseries, elid, i)
{
    var regresa = '';
    regresa += "<button type='button' data-indice='" + i + "' class='btn btn-warning btn-xs limpia' rel='" + elid + "'>&nbsp;Limpia</button>";
    return regresa;
}

function ActualizaProductosPedidos() {

    $("#datos2pedido tbody").empty();

    var info = "";
    var info2 = "";


    for (var i = 0; i < _articulospedidos.length; i++) {

        var pendiente = _articulospedidos[i].Cantidad - _articulospedidos[i].Surtida - _articulospedidos[i].Surtiendo;

        info2 += "<tr>" + "<td>" + _articulospedidos[i].Nombre + "</td>" + "<td>" + _articulospedidos[i].Cantidad + "</td>"
            + "<td id='surtida_" + i + "'>" + _articulospedidos[i].Surtida + "</td>"
            + "<td id='surtiendo_" + i + "'>" + _articulospedidos[i].Surtiendo + "</td>"
            + "<td id='pendiente_" + i + "'>" + pendiente + "</td>"
            + "<td id='preciounitario_" + i + "' class='precio' data-indice='" + i + "'><button type='button' class='btn btn-default btn-xs'>&nbsp;" + _articulospedidos[i].PrecioUnitarioTexto + "</button></td>";

        var mo = _articulospedidos[i].Surtiendo * _articulospedidos[i].PrecioUnitario;
        info2 += "<td id='importe_" + i + "'>" + ToMoneda(mo) + "</td>";

        info2 += "<td>" + DetalleRecepcion(_articulospedidos[i].TieneSeries, _articulospedidos[i].Clave, i, pendiente) + "</td>";
        info2 += "<td>" + LimpiaRecepcion(_articulospedidos[i].TieneSeries, _articulospedidos[i].Clave, i) + "</td>";

        info2 += "</tr>";


        _objRecepcion.push(null);
        _objRecepcion[i] = _articulospedidos[i].Seriales.slice(0);

        _objRecepcionInicial.push(null);
        _objRecepcionInicial[i] = _articulospedidos[i].Seriales.slice(0);

        _objSurtidas.push(null);
        _objSurtidas[i] = _articulospedidos[i].Seriales.length;
    }

    $("#datos2pedido tbody").append(info2);


    
    for (var i = 0; i < _articulospedidos.length; i++) {
        $("#qty_" + i).val(_articulospedidos[i].Surtiendo);
    }

    AfterPedido();
    AfterAjax();

}