﻿var Almacenes = new Array();

$(document).ready(function () {
    var element = $(this).find('option:selected');
    var myTag = element.attr("id-almacen");
    llenaLista(myTag);

    $('#todosalm').on('ifChecked', function (event) {
        Almacenes = new Array();
        todos();       
        var element = $('#dis').find('option:selected');
        var myTag = element.attr("id-almacen");
        llenaLista(myTag);
        
    });

    $('#todosalm').on('ifUnchecked', function (event) {
        Almacenes = new Array();
        var element = $('#dis').find('option:selected');
        var myTag = element.attr("id-almacen");
        llenaLista(myTag);
    });
});

$('#dis').change(function () {
    var element = $(this).find('option:selected');
    var myTag = element.attr("id-almacen");    
    llenaLista(myTag);
    $('#todosalm').removeAttr("cheked")
});

function todos() {
   
    var url = '/Almacen/GetAlmacenByDistribuidor/';
    $.ajax({
        url: url,
        data: { 'distribuidor': $('#dis').val() },
        type: "POST",
        success: function (data) {
            for (var p = 0; p < data.length; p++) {
                Almacenes.push(data[p].IdAlmacen);
            }
        },
        error: function (request, error) {
            toastr.error("Surgió un error, intente nuevamente más tarde");
        }
    });
}



function llenaLista(iddistribuidor) {   
    console.log(Almacenes);

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[5, 10, 15], [5, 10, 15]],
        "ajax": {
            "url": "/Almacen/ObtenAlamcenes/",
            "type": "POST",
            "data": {
                'IdDistribuidor': iddistribuidor
            }
        },

        "columns": [
             {
                 sortable: false,
                 "render": function (data, type, full, meta) {

                     


                     if (existeTecnico(full.IdAlmacen)) {
                         return '<label class="checkbox-inline checkbox-styled checkbox-warning"><input onchange="Activa(\'' + full.IdAlmacen + '\')"  type="checkbox" checked value="' + full.IdAlmacen + '"><span></span></label>';
                     } else {
                         return '<label class="checkbox-inline checkbox-styled checkbox-warning"><input onchange="Activa(\'' + full.IdAlmacen + '\')"  type="checkbox"  value="' + full.IdAlmacen + '"><span></span></label>';
                     }

                 }
             },
               { "data": "Nombre", "orderable": false },

        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}

function Activa(id) {

    if (existeTecnico(id)) {
        removeItem(id);
    } else {
        Almacenes.push(id);
    }

    console.log(Almacenes);
}

function existeTecnico(id) {
    for (var i = 0; i < Almacenes.length; i++) {
        if (Almacenes[i] == id) {
            return true;
        }
    }
    return false;
}

function removeItem(item) {
    var i = Almacenes.indexOf(item);
    if (i !== -1) {
        Almacenes.splice(i, 1);
    }
}

$('#excel').click(function () {   
        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            var distribuidor = document.getElementById("dis").value;
            window.location.href = '/Reportes/ObtenSaldoExcel?d=' + distribuidor + "&almacenes=" + Almacenes;
        }
        else {
            toastr.error('Error', 'Debes de llenar los campos obligatorios');
        }
})

$('#pdf').click(function () {

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        var distribuidor = document.getElementById("dis").value;
        window.location.href = '/Reportes/ObtenSaldoPDF?d=' + distribuidor + "&almacenes=" + Almacenes;
    }
    else {
        toastr.error('Error', 'Debes de llenar los campos obligatorios');
    } 
        
});