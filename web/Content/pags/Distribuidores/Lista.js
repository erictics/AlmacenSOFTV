﻿//distribuidores


$(document).ready(function () {
    llenaLista();
});

function llenaLista() {

    var objRoles = {};
    objRoles.pag = 1;

    var myJSONText = JSON.stringify(objRoles);

   

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/Distribuidor/ObtenLista/",
            "type": "POST",
            "data": {
                'data': myJSONText
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [

               { "data": "Nombre", "orderable": false },
               {
                   sortable: false,
                   "render": function (data, type, full, meta) {

                       if (full.Activo == 1) {
                           //accion += "<a href='#' rel='" + elid + "' class='del'><img src='/Content/assets/images/activo.png'></a>";
                           return "<img src='/Content/assets/img/activo.png'>";
                       }
                       else {
                           //accion += "<a href='#' rel='" + elid + "' class='activar'><img src='/Content/assets/images/inactivo.png'></a>";
                           return "<img src='/Content/assets/img/inactivo.png'>";
                       }
                   }

               },

                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {
                          return '<button type="button" class="btn btn-warning btn-xs edit" onClick="edita(\'' + full.Clave + '\')" ><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Editar</button>';
                      }
                  }
        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elementos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            //loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay elementos para mostrar",
            emptyTable: "No hay elementos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}


$("#formaedita").submit(function (e) {
    e.preventDefault();

    validatelefono(1); validatelefono(2);

    var valid = jQuery('#formaedita').validationEngine('validate');
    if (valid) {
        $("#loader").show();

        var url = '/Distribuidor/Guarda/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaedita').serialize(),
            type: "POST",
            success: function (data) {

                $("#edita").modal('hide');
                toastr.error( 'La Distribuidor se guardó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });

    }

});


function edita (cual ) {
    
    $("#titulowin").html("Edita Distribuidor");
    $('#edita').modal('show');

    $("#ed_id").val('');
    $("#ed_nombre").val('');

    $("#ed_CalleNumero").val('');
    $("#ed_Colonia").val('');
    $("#ed_CodigoPostal").val('');
    $("#ed_Ciudad").val('');

    $("#ed_rfc").val('');

    $("#ed_nombrecontacto").val('');
    $("#ed_emailcontacto").val('');
    $("#ed_lada1contacto").val('');
    $("#ed_telefono1contacto").val('');
    $("#ed_lada2contacto").val('');
    $("#ed_telefono2contacto").val('');

    $("#ed_importepagare").val('');

    var url = '/Distribuidor/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {

            var oRol = eval(data);

            $("#ed_id").val(oRol.Clave);
            $("#ed_nombre").val(oRol.Nombre);

            $("#ed_importepagare").val(oRol.ImportePagare);

            $("#ed_CalleNumero").val(oRol.CalleNumero);
            $("#ed_Colonia").val(oRol.Colonia);
            $("#ed_CodigoPostal").val(oRol.CodigoPostal);
            $("#ed_Ciudad").val(oRol.Ciudad);

            $("#ed_rfc").val(oRol.RFC);

            $("#ed_nombrecontacto").val(oRol.NombreContacto);
            $("#ed_emailcontacto").val(oRol.EmailContacto);
            $("#ed_lada1contacto").val(oRol.Lada1Contacto);
            $("#ed_telefono1contacto").val(oRol.Telefono1Contacto);
            $("#ed_lada2contacto").val(oRol.Lada2Contacto);
            $("#ed_telefono2contacto").val(oRol.Telefono2Contacto);
            $("#ed_activo").val(oRol.Activo);
            if (oRol.Activo == "True" || oRol.Activo == "1") {
                $("#ed_activo").prop('checked', 'checked');
                $("#ed_activo").parent().addClass('checked');
            }
            else {
                $("#ed_activo").removeProp('checked');
                $("#ed_activo").parent().removeClass('checked');
            }
            //LlenaGrid(_pag);

        },
        error: function (request, error) {
            toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

};



$("#add").click(function () {
    $("#ed_id").val('');
    $("#ed_nombre").val('');

    $("#ed_importepagare").val('');

    $("#ed_CalleNumero").val('');
    $("#ed_Colonia").val('');
    $("#ed_CodigoPostal").val('');
    $("#ed_Ciudad").val('');

    $("#titulowin").html("Agregar Distribuidor");
    $('#edita').modal('show');
    $("#ed_id").val('');
    $("#ed_nombre").val('');
    $("#ed_CalleNumero").val('');
    $("#ed_Colonia").val('');
    $("#ed_CodigoPostal").val('');
    $("#ed_Ciudad").val('');


    $("#ed_nombrecontacto").val('');
    $("#ed_emailcontacto").val('');
    $("#ed_lada1contacto").val('');
    $("#ed_telefono1contacto").val('');
    $("#ed_lada2contacto").val('');
    $("#ed_telefono2contacto").val('');

    $("#ed_rfc").val('');

});
