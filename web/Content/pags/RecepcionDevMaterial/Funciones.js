﻿
function recepcion(devolucion) {
    $('#DetalleDevolucion').modal('show');
    $('#contenidoresumen').empty();
    $('#contenidoseries').empty();
    $('#iddev').val(devolucion);
    var url = '/RecepcionDevolucionMaterial/Detalledevolucion/';
    $.ajax({
        url: url,
        data: { 'devolucion':devolucion},
        type: "POST",
        success: function (data) {

            for (var a = 0; a < data.length; a++) {
                console.log(data[a].tieneseries);
                if (data[a].tieneseries==false) {
                    $('#contenidoresumen').append("<tr><td>" + data[a].articulo + "</td><td>" + data[a].Cantidad + "</td></tr>");
                }
                else {
                    $('#contenidoresumen').append("<tr><td>" + data[a].articulo + "</td><td>" + data[a].Cantidad + "</td><td><button class='btn btn-info btn-xs series' rel='" + devolucion + "' data-claveart='"+data[a].ClaveArticulo+"'>Ver series</button></td></tr>");
                }
                
                };
            
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });


};

$('#tabladetalle').on('click', '.series', function () {
    
    //$('#DetalleSerie').modal('show');
    $('#contenidoseries').empty();
    var devolucion = $(this).attr('rel');
    var articulo = $(this).attr('data-claveart');
    var url = '/RecepcionDevolucionMaterial/detalleSeries/';
    $.ajax({
        url: url,
        data: { 'devolucion': devolucion, 'articulo':articulo },
        type: "POST",
        success: function (data) {
            console.log(data);
            for (var a = 0; a < data.length; a++) {

                if (data[a].clv_estatus == 7) {
                    $('#contenidoseries').append("<tr><td><button class='btn btn-default-bright btn-xs'>" + data[a].valor + "</button></td><td><button class='btn btn-info btn-xs'>" + data[a].estatus + "</button></td></tr>");
                }
                else if (data[a].clv_estatus == 9) {
                    $('#contenidoseries').append("<tr><td><button class='btn btn-default-bright btn-xs'>" + data[a].valor + "</button></td><td><button class='btn btn-warning btn-xs' style='background-color:#e67c31;border-color:#e67c31; '>" + data[a].estatus + "</button></td></tr>");
                }
                else if (data[a].clv_estatus == 10) {
                    $('#contenidoseries').append("<tr><td><button class='btn btn-default-bright btn-xs'>" + data[a].valor + "</button></td><td><button class='btn btn-danger btn-xs'>" + data[a].estatus + "</button></td></tr>");
                }
                else {
                    $('#contenidoseries').append("<tr><td><button class='btn btn-default-bright btn-xs'>" + data[a].valor + "</button></td><td><button class='btn btn-default-bright btn-xs'>" + data[a].estatus + "</button></td></tr>");
                }
              
               
                
                
                
            }
            
        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });
});



$('#guardaRecepcion').click(function () {
       
    var devolucion = $('#iddev').val();
    var url = '/RecepcionDevolucionMaterial/guarda/';
    $.ajax({
        url: url,
        data: { 'devolucion': devolucion },
        type: "POST",
        success: function (data) {
            
            if (data == "-1") {
                toastr.warning("Atención, se evitó la recepción, Alguno de los aparatos ya fue recibido por el almacén principal.");
            }
            else if (data == "-2") {
                toastr.warning("Error, La devolución fue cancelada por el distribuidor");
            }
            else {

                $('#DetalleDevolucion').modal('hide');
                llenaLista();
                $('#repo').click();
                $('#titulocanvas').empty().append("Detalle recepción de material");
                $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);


               // $("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + data);
               //toastr.success("Correcto", "Se ha generado correctamente");
               // $('#ver').modal('show');
                
            }

        },
        error: function (request, error) {
            toastr.error('Error, Surgió un error, intente nuevamente más tarde');
        }
    });

});
