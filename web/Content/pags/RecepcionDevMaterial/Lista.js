﻿llenaLista();


function llenaLista(alm, orden) {
    if (alm == undefined ) {
        alm = "";
    }
    if(orden==undefined|| orden==""){
        orden = 0;
    }

    


    $('#indice').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[ 5], [5]],
        "ajax": {
            "url": "/RecepcionDevolucionMaterial/ListaDevoluciones/",
            "type": "POST",
            "data": {
                'data': "1",
                'almacen': alm,
                'orden':orden
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "pedido", "orderable": false },
                 { "data": "fecha", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         console.log(full.estatus);
                         var str;
                         if (full.estatus == 1) { str = "Por Autorizar" }
                         else if (full.estatus == 4) { str = "Recibido en almacen central" }
                         else { str = "Cancelada" }
                         return str;
                     }
                 },
                 { "data": "almacen", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         return '<button class="btn ink-reaction  btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title=""  data-original-title="Descarga PDF de devolución"'
                             + 'onClick="descargapdf(\'' + full.proceso + '\')"><i class="md-file-download" aria-hidden="true"></i>Descargar</button> '
                             + '<button class="btn ink-reaction btn-xs btn-success" data-toggle="tooltip" data-placement="right" title=""  data-original-title="Ver detalle de devolución"'
                             + ' onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search" aria-hidden="true"></i> Detalle</button>';


                     }
                 },
        {
        sortable: false,
        "render": function (data, type, full, meta) {

            if (full.estatus == 1) {
                return '<button class="btn btn-primary btn-xs  " onClick="recepcion(\'' + full.proceso + '\')"> Recibir material</button>';
            }


            if (full.estatus == 4) {
                return '<button class="btn ink-reaction btn-xs btn-warning" data-toggle="tooltip" data-placement="right" title=""  data-original-title="Ver detalle de recepción por devolución"'
                    + ' onClick="verrecepcion(\'' + full.Recepcion + '\')"><i class="fa fa-search"></i></button> '

                    + '<button class="btn ink-reaction btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title=""  data-original-title="Descarga PDF de recepción por devolución" '
                    + ' onClick="descargarecepcion(\'' + full.Recepcion + '\')"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>';
            }
        }
        }
        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay aparatos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay aparatps para mostrar",
            emptyTable: "No hay aparatos disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}


function verpdf(devolucion) {

    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción de material");
    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + devolucion);


    //$("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + devolucion);
    //$("#titulo_ver").html($("body div[class='page-title']").html().trim());
    //$('#ver').modal('show');
};


function descargapdf(devolucion) {
   
   // AlertaDescarga();
    window.location.href = "/DevolucionAlmacenCentral/Detalle?pdf=1&d=" + devolucion;
};


function verrecepcion(recepcion) {

    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle recepción de material");
    $("#framecanvas").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + recepcion);


   // $("#eliframe").attr("src", "/DevolucionAlmacenCentral/Detalle?pdf=2&d=" + recepcion);
   // $("#titulo_ver").html($("body div[class='page-title']").html().trim());
   // $('#ver').modal('show');

}

function descargarecepcion(recepcion) {

    //AlertaDescarga();
    window.location.href = "/DevolucionAlmacenCentral/Detalle?pdf=1&d=" + recepcion;
}


$('#buscar').click(function () {
    var orden = $('#orden').val();
    llenaLista("",orden);
});

$('#falmac').change(function () {
    var alm = $('#falmac').val();
    llenaLista(alm,0);
   
});

$('#Distribuidor').change(function () {
    $('#falmac')
  .empty()
  .append('<option selected="selected" disabled value="x">Selecciona</option>')
    ;
    var url = '/RecepcionDevolucionMaterial/ObtenAlamacenes/';
    $.ajax({
        url: url,
        data: { 'id': $('#Distribuidor').val() },
        type: "POST",
        success: function (data) {
            $('#falmac').show();
            for (var a = 0; a < data.length; a++) {
                $('#falmac').append("<option value="+data[a].Clave+">"+data[a].Nombre+"</option>" )
            }
            
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });

});