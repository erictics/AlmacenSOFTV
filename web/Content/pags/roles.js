﻿

$(document).ready(function () {

    LlenaGrid(1);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $("#titulowin").html("Agregar Rol");
        $('#edita').modal('show');
        $("#ed_nombre").val('');
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Roles/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {

                $("#elimina").modal('hide');
                toastr.success( 'El rol se eliminó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Error, surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#formaedita").submit(function (e) {
        e.preventDefault();

        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var url = '/Roles/Guarda/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formaedita').serialize(),
                type: "POST",
                success: function (data) {

                    $("#edita").modal('hide');
                    toastr.success( 'El rol se guardó correctamente');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });
        }

    });

});


var _pag = 1;
function LlenaGrid(pag)
{
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;

    var myJSONText = JSON.stringify(objRoles);

    $("#datos tbody").empty();
    $("#loader").show();
    var url = '/Roles/ObtenLista/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            console.log(data);
            $("#loader").hide();

            var arreglo = eval(data);
            if (arreglo.length > 0) {
                $("#datos").show();
            }
            else {
                $("#resultados").html("No se encontraron resultados");
            }


            for (var i = 0; i < arreglo.length; i++) {
                var renglon = "<tr><td>" + (i + 1) + "</td><td>" + arreglo[i].Nombre + "</td><td>" + DameAcciones(arreglo[i].Id, arreglo[i].tieneusuarios) + "</td></tr>"
                $("#datos tbody").append(renglon);
            }

            AfterAjax();
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });


    url = '/Roles/ObtenPaginas/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            LlenaPaginacion(data);
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });
}

function AfterAjax() {

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Elimina Rol");
        $('#elimina').modal('show');

        var url = '/Roles/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#del_id").val(oRol.Id);
                $("#titulowindel").html("Elimina Rol " + oRol.Nombre);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    });

    $(".edit").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Rol");
        $('#edita').modal('show');

        var url = '/Roles/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oRol = eval(data);

                $("#ed_id").val(oRol.Id);
                $("#ed_nombre").val(oRol.Nombre);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}

function DameAcciones(elid,usuarios)
{
    console.log(usuarios);
    if (usuarios == true) {
        return "<button type='button' class='btn btn-default btn-xs edit' rel='" + elid + "'>&nbsp;Editar</button><button type='button' class='btn btn-danger btn-xs del' rel='" + elid + "' disabled='disabled' >&nbsp;Elimina</button>";
    }
    else
    {
        return "<button type='button' class='btn btn-default btn-xs edit' rel='" + elid + "'>&nbsp;Editar</button><button type='button' class='btn btn-danger btn-xs del' rel='" + elid + "' >&nbsp;Elimina</button>";
    }
   
}