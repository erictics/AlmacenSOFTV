﻿$(document).ready(function () {
    ObtenerLista($('#almacenb').val(), 0);
});

$('#almacenb').change(function () {
    ObtenerLista($('#almacenb').val(), 0);
});

function ObtenerLista(almacen, orden) {
    var Numero = (orden == "" || orden == undefined)?0:orden;    
    var Alm= (almacen == "" || almacen == undefined) ?0:almacen;     
    var data = { 'data': "1", 'almacen': Alm, 'orden': Numero }
    var url = "/HabilitacionEquipos/ListaEquipos/";
    var columnas = [
                  { "data": "pedido", "orderable": false },
                 { "data": "fecha", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var str;
                         if (full.estatus == 1) { str = "Por Autorizar" }
                         else if (full.estatus == 4) { str = "Ejecutado" }
                         else { str = "Cancelada" }
                         return str;
                     }
                 },
                 { "data": "almacen", "orderable": false },

                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         console.log(full.tipo);
                         return "<button class='btn ink-reaction btn-flat btn-xs btn-primary'>" + full.tipo + "</button>"

                     }
                 },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var pdf = '<button class="btn ink-reaction btn-floating-action btn-xs btn-danger" onClick="descargapdf(\'' + full.proceso + '\')"><i class="fa fa-download" aria-hidden="true"></i> </button>';
                         var verpdf = '<button class="btn ink-reaction btn-floating-action btn-xs btn-primary" onClick="verpdf(\'' + full.proceso + '\')"><i class="fa fa-search" aria-hidden="true"></i> </button>';


                         var str = "";
                         str += pdf + verpdf;
                         if (full.estatus == 1) {

                             if (full.tiene_envio == 1) {
                                 str += detenvio;
                             } else {
                                 str += envio;
                             }
                         }
                         return str;


                     }
                 }];


    $('#indice').dataTable({
        "processing": true,
        "responsive": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": url,
            "type": "POST",
            "data": data
        },

        "columns": columnas,

        language: lenguaje,
        "order": [[0, "asc"]]
    })



}


function verpdf(devolucion) {
    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle proceso");
    $("#framecanvas").attr("src", "/DevolucionSAC/Detalle?pdf=2&d=" + devolucion);
}


function descargapdf(devolucion) {
    window.location.href = "/DevolucionSAC/Detalle?pdf=1&d=" + devolucion;
};