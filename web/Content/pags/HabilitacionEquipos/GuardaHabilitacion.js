﻿$('#guardaHabilitar').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Atención, No puede realizar una habilitación sin artículos");
    }
    else if (comment.length == 0) {
        toastr.warning("Atención, No puede realizar una habilitación sin especificar el motivo de esta.");
    }
    else {
        var url = '/HabilitacionEquipos/Habilita/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'almacen': $('#almacen').val(), 'motivo': $('#motivo').val(), 'articulos': JSON.stringify(articulos), 'status': $('#status').val() },
            type: "POST",
            success: function (data) {

                if (data == "0") {
                    toastr.error('El proceso no pudo realizarse');
                }
                else if (data == "-1") {
                    toastr.error('Alguno de los aparatos no puede ser procesado, no está en estatus de revisión o dañado y/o no esta disponible en el inventario del almacén seleccionado ');
                }
                else if (data == "-2") {
                    toastr.error('Alguno de los aparatos no puede ser procesado, no está en estatus de revisión o dañado y/o no esta disponible en el inventario del almacén seleccionado ');
                }
                else if (data == "-3") {
                    toastr.error('la cantidad de artículos en revision/dañados es mayor a la cantidad en el inventario');
                }
                else if (data == "-3") {
                    toastr.error('Ocurrió un error de Identificacíon de inventarios');
                }

                else {
                    ObtenerLista(0, 0);
                    $('#AgregaDevolucion').modal('hide');
                    toastr.success('El proceso se realizó correctamente');
                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle habilitación de material");
                    $("#framecanvas").attr("src", "/DevolucionSAC/Detalle?pdf=2&d=" + data);
                }               
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });
        

    }

});


$('#guardaBaja').click(function () {
    var comment = $.trim($('#motivo').val());
    if (articulos.length == 0) {
        toastr.warning("Atención, No puede realizar una baja sin artículos");
    }

    else if (comment.length == 0) {
        toastr.warning("Atención, No puede realizar una baja sin especificar el motivo de esta.");
    }
    else {
        var url = '/HabilitacionEquipos/Baja/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'almacen': $('#almacen').val(), 'motivo': $('#motivo').val(), 'articulos': JSON.stringify(articulos),'status': $('#status').val() },
            type: "POST",
            success: function (data) {
                if (data == "0") {
                    toastr.error('El proceso no pudo realizarse');
                }
                else if (data == "-1") {
                    toastr.error('Alguno de los aparatos no puede ser procesado, no está en estatus de revisión o dañado y/o no esta disponible en el inventario del almacén seleccionado ');
                }
                else if (data == "-2") {
                    toastr.error('Alguno de los aparatos no puede ser procesado, no está en estatus de revisión o dañado y/o no esta disponible en el inventario del almacén seleccionado ');
                }
                else if (data == "-3") {
                    toastr.error('la cantidad de artículos en revision/dañados es mayor a la cantidad en el inventario');
                }

                else {

                    $('#repo').click();
                    $('#titulocanvas').empty().append("Detalle baja de material");
                    $("#framecanvas").attr("src", "/DevolucionSAC/Detalle?pdf=2&d=" + data);                    

                    ObtenerLista(0, 0);
                    $('#AgregaDevolucion').modal('hide');
                    toastr.success('El proceso se realizó correctamente');
                }
            },
            error: function (request, error) {
                toastr.error('Error,Surgió un error, intente nuevamente más tarde');
            }
        });


    }

});




