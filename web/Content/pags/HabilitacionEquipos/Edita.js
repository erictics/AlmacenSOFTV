﻿var idEdiccion = null;
function EditaArticulo(idArticulo, cantidad) {
    $('#AgregaArticulo').modal('show');

    var url = '/SalidaMaterialTecnico/DetalleArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idarticulo': idArticulo },
        type: "POST",
        success: function (data) {

            $('#clasificacion').val(data.idClasificacion);
            obtentipomat(data.idClasificacion, data.idTipoArticulo);
            var valor = AsignaArticulo(data.idArticulo, data.idTipoArticulo);

        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });

}



function GuardaAccesorios() {
    if ($('#cantidad').val() <= 0) {
        toastr.warning("Error, Necesita colocar una cantidad mayor a 0 para realizar la devolución");
    }
    else if ($('#cantidad').val() % 1 != 0) {
        toastr.warning("Error, Necesita colocar un número entero");
    }
    else if ($('#enalmacen').val() == 0) {
        toastr.warning("Error, Este almacen no cuenta con piezas de este artículo para realizar la devolución");
    }
    else if (parseInt($('#enalmacen').val()) < parseInt($('#cantidad').val())) {
        toastr.warning("Error, La cantidad solicitada supera la cantidad en almacén");
    }
    else if (parseInt($('#cantidad_anterior').val()) + parseInt($('#enalmacen').val()) < parseInt($('#cantidad').val())) {
        toastr.warning("Error, La cantidad solicitada es mayor a la cantidad anterior y la cantidad en almacén");
    }

    else {

        if (existe_en_arreglo($('#articulo').val()) == true) {
            EditaArreglo($('#articulo').val(), $('#cantidad').val());
            ActualizaDetalle();
            $('#AgregaArticulo').modal('hide');
        } else {
            var Serie = {};
            Serie.InventarioClave = $("#InventarioClave").val();
            Serie.catSerieClave = "";
            Serie.Valor = "";
            Serie.ArticuloClave = $('#articulo').val();
            Serie.Nombre = $("#articulo option:selected").text();
            Serie.Cantidad = $('#cantidad').val();
            Serie.status = 7;
            Serie.Statustxt = "";
            articulos.push(Serie);
            ActualizaDetalle();
            $('#AgregaArticulo').modal('hide');
        }
    }
};