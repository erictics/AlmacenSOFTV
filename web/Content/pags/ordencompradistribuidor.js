﻿var objArticulos = new Array();
var _orden = '';
var _salida = '';

$(document).ready(function () {


    $("#lasseries").on("click", ".inventarios",function () {
        //alert('aaa');
    });
   
    $("#buscar").click(function () {

        var valid = jQuery('#formbusca').validationEngine('validate');
        if (valid) {
            LlenaGrid(1);
        }
    });

    $("#buscastatus,#buscadistribuidor,#buscapagvta").change(function () {

        var valid = jQuery('#formbusca').validationEngine('validate');
        if (valid) {
            LlenaGrid(1);
        }
    });

    $("#formadetalle .btn-danger").click(function () {
        var valid = jQuery('#formarechaza').validationEngine('validate');
        if (valid) {
            var objRoles = {};
            objRoles.comentario = $("#comentario").val();
            objRoles.accion = 'rechaza';
            objRoles.clave = $("#autoriza_id").val();
            var myJSONText = JSON.stringify(objRoles);

            var url = '/OrdenCompra/Autoriza/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#ver").modal('hide');
                    toastr.success('Rechazo', 'la orden de compra se rechazó correctamente');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Error', 'surgió un error, intente nuevamente más tarde');
                }
            });
        }
    });

    $("#formadetalle .btn-success").click(function () {
        var valid = jQuery('#formaautoriza').validationEngine('validate');
        if (valid) {
            var objRoles = {};
            objRoles.comentario = $("#comentario").val();
            objRoles.accion = 'autoriza';
            objRoles.clave = $("#autoriza_id").val();
            var myJSONText = JSON.stringify(objRoles);

            var url = '/OrdenCompra/Autoriza/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#ver").modal('hide');
                    toastr.success('Autorización', 'la orden de compra se autorizó correctamente');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });
        }
    });

    $("#formacancelar").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/SalidaMaterial/CancelaTraspaso/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formacancelar').serialize(),
            type: "POST",
            success: function (data) {

                if (data == "-1")
                {
                    toastr.error("Error", 'la salida a cancelar contiene artículos no disponibles en el almacén, revise su inventario');
                }

                $("#cancelar").modal('hide');
                toastr.success($("#titulowincancelar").html(), 'La salida se canceló correctamente');

                ShowSalidas(_orden);
                LlenaGrid(_pag);

                //LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $(".preciootrosgastos").blur(function () {
        Resumen();
    });

    $("#agregar").click(function () {
        $('#articulo').modal('show');

        
        $("#ed_clasificacionmaterial").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_articulo").val('');
        $("#ed_cantidad").val('');
        $("#ed_precio").val('');
        $("#ed_moneda").val('');

    });

    $(".optiontipo").hide();

    $("#ed_clasificacionmaterial").change(function () {

        $("#ed_tipomaterial").val('');

        $(".optiontipo").each(function () {
            if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val()) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });

    LlenaGrid(1);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $("#titulowin").html("Agregar Orden de Compra");

        objArticulos = new Array();

        $('#edita').modal('show');
        $("#ed_id").val('');
        $("#ed_nombre").val('');

        $("#ed_existencias").val('');
        $("#ed_tope").val('');

        $("#ed_unidad").val('');
        $("#ed_porcentaje").val('');

        $("#ed_unidad").val('');

        $("#ed_tipomaterial").val('');
        $("#ed_clasificacionmaterial").val('');

        $("#ed_proveedor1").val('');
        $("#ed_precio1").val('');
        $("#ed_moneda1").val('');

        $("#ed_proveedor2").val('');
        $("#ed_precio2").val('');
        $("#ed_moneda2").val('');

        $("#ed_proveedor3").val('');
        $("#ed_precio3").val('');
        $("#ed_moneda3").val('');
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Articulo/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {

                $("#elimina").modal('hide');
                toastr.success($("#titulowin").html(), 'El artículo se eliminó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#agregararticulo").click(function () {

        var valid = jQuery('#formaarticulo').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var objArticulo = {};
            objArticulo.ArticuloClave = $("#ed_articulo").val();
            objArticulo.ArticuloTexto = $("#ed_articulo option:selected").text();
            objArticulo.Cantidad = $("#ed_cantidad").val();
            objArticulo.PrecioUnitario = $("#ed_precio").val();
            objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
            objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();
            
            objArticulos.push(objArticulo);

            ActualizaLista();

            $('#articulo').modal('hide');

        }
    });

    $("#formaedita").submit(function (e) {
        e.preventDefault();

        var objRoles = {};
        objRoles.catProveedorClave = $("#ed_proveedor").val();
        objRoles.EntidadClaveSolicitante = $("#ed_almacen").val();
        objRoles.UsuarioClaveAutorizacion = $("#ed_autorizador").val();
        objRoles.Observaciones = $("#ed_observaciones").val();
        objRoles.TipoCambio = $("#ed_tipocambio").val();
        objRoles.Iva = $("#ed_iva").val();
        objRoles.Descuento = $("#ed_descuento").val();
        objRoles.DiasCredito = $("#ed_diascredito").val();

        var objOtroGasto1 = {};
        objOtroGasto1.Concepto = $("#ed_conceptooc1").val();
        objOtroGasto1.Precio = $("#ed_preciooc1").val();
        objOtroGasto1.Moneda = $("#ed_monedaoc1").val();

        var objOtroGasto2 = {};
        objOtroGasto2.Concepto = $("#ed_conceptooc2").val();
        objOtroGasto2.Precio = $("#ed_preciooc2").val();
        objOtroGasto2.Moneda = $("#ed_monedaoc2").val();

        var objOtrosGastos = new Array();
        objOtrosGastos.push(objOtroGasto1);
        objOtrosGastos.push(objOtroGasto2);

        objRoles.OtrosGastos = objOtrosGastos;
        objRoles.Articulos = objArticulos;
        objRoles.ToSubtotal = $("#to_subtotal").val();
        objRoles.ToDescuento = $("#to_descuento").val();
        objRoles.ToIVA = $("#to_iva").val();
        objRoles.Total = $("#to_total").val();

        var myJSONText = JSON.stringify(objRoles);

            $("#loader").show();

            var url = '/Pedido/Guarda/';
            $.ajax({
                url: url,
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#edita").modal('hide');
                    $("#eliframe").attr("src", "/SalidaMaterial/Detalle?pdf=2&p=" + data);
                    $('#ver').modal('show');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });
    });

    $("#ed_tipomaterial").change(function () {

        var tipo = $(this).val();

        var objRoles = {};
        objRoles.tipo = tipo;

        $("#ed_articulo").empty();
        $("#ed_articulo").append("<option value=''>Seleccione</option>");

        var myJSONText = JSON.stringify(objRoles);
        var url = '/Articulo/ObtenPorTipo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                var arreglo = eval(data);
                for (var i = 0; i < arreglo.length; i++) {
                    $("#ed_articulo").append("<option value='" + arreglo[i].Clave + "'>" + arreglo[i].Descripcion + "</option>");
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });
});

function Resumen() {

    var suma = 0;
    var tipocambio = 1;

    for (var i = 0; i < objArticulos.length; i++) {

        if(objArticulos[i].MonedaTexto.indexOf('Dólar')>-1)
        {
            tipocambio = parseFloat($("#ed_tipocambio").val());
        }

        var totalproducto = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);
            totalproducto = totalproducto * tipocambio;
            suma += totalproducto;
    }

    if ($("#ed_preciooc1").val() != '') {
        suma += parseFloat($("#ed_preciooc1").val());
    }

    if ($("#ed_preciooc2").val() != '') {
        suma += parseFloat($("#ed_preciooc2").val());
    }

    var eliva = parseFloat($("#ed_iva").val());

    var descuento = 0;

    var descuento_po = 0;
    try
    {
        if ($("#ed_descuento").val() != "") {
            descuento_po = parseFloat($("#ed_descuento").val());
            descuento = descuento_po * suma / 100;
        }
    }
    catch (e)
    { }

    var subtotaldescuento = suma - descuento;
    var iva = (subtotaldescuento * eliva / 100);
    var total = subtotaldescuento + (subtotaldescuento * eliva / 100);

    $("#to_subtotal").val(suma);
    $("#to_descuento").val(descuento);
    $("#to_iva").val(iva);
    $("#to_total").val(total);

    $("#reiva_po").html("(" + $("#ed_iva").val() + "%" + ")");
    $("#reiva").html(ToMoneda(iva) + "%");

    $("#resubtotal").html(ToMoneda(suma));
    $("#redescuento").html(ToMoneda(descuento));
    $("#redescuento_po").html("(" + descuento_po + "%"+")");
    $("#retotal").html(ToMoneda(total));
}


function ActualizaListaDistribuidor() {
    $("#articulostable tbody").empty();

    for (var i = 0; i < objArticulos.length; i++) {

        var total = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);

        var renglon = "<tr><td>" + objArticulos[i].ArticuloTexto + "</td><td>" + objArticulos[i].Cantidad + "</td><td>" + ToMoneda(objArticulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td><td>" + objArticulos[i].MonedaTexto + "</td><td>" + DameAccionesArticulo(i) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }

    Resumen();
    AfterAjax();
}

function ActualizaLista()
{
    alert('actualiza');

    $("#articulostable tbody").empty();

    for (var i = 0; i < objArticulos.length; i++) {

        var total = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);

        var renglon = "<tr><td>" + objArticulos[i].ArticuloTexto + "</td><td>" + objArticulos[i].Cantidad + "</td><td>" + ToMoneda(objArticulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td><td>" + objArticulos[i].MonedaTexto + "</td><td>" + DameAccionesArticulo(i) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }

    Resumen();
    AfterAjax();
}

var _pag = 1;
//function LlenaGrid(pag)
//{
//    _pag = pag;

//    var objOrdenCompraDistribuidor = {};
//    objOrdenCompraDistribuidor.pag = pag;
//    objOrdenCompraDistribuidor.estatus = $("#buscastatus").val();
//    objOrdenCompraDistribuidor.distribuidor = $("#buscadistribuidor").val();
//    objOrdenCompraDistribuidor.pedido = $("#abuscar").val();
//    objOrdenCompraDistribuidor.esPagare = $("#buscapagvta").val();
//    objOrdenCompraDistribuidor.pagadas = 1;

//    var myJSONText = JSON.stringify(objOrdenCompraDistribuidor);

//    $("#datos tbody").empty();
//    $("#loader").show();
//    var url = '/Pedido/ObtenLista/';
//    $.ajax({
//        url: url,
//        dataType: 'json',
//        data: { 'data': myJSONText },
//        type: "POST",
//        success: function (data) {

//            $("#loader").hide();

//            var arreglo = eval(data);
//            if (arreglo.length > 0) {
//                $("#resultados").html("");
//                $("#datos").show();
//            }
//            else {
//                $("#resultados").html("No se encontraron resultados");
//            }

//            for (var i = 0; i < arreglo.length; i++) {
//                var renglon = "<tr><td>" + arreglo[i].NumeroPedido + "</td><td>" + arreglo[i].Destino + "</td><td>" + arreglo[i].Fecha + "</td><td>" + arreglo[i].Total + "</td><td>" + arreglo[i].Estatus.replace('Autorizado', 'Pendiente') + "</td>";

//                if (arreglo[i].EsPagare=="1") {
//                    renglon += "<td>Pagare</td>";
//                }
//                else {
//                    renglon += "<td>Venta</td>";
//                }


//                renglon += "<td>" + DameSalidas(arreglo[i].Clave, arreglo[i].Estatus.substring(0, 3)) + "&nbsp;";
//                renglon += DameAcciones(arreglo[i].Clave, arreglo[i].EntidadClaveAfectada, arreglo[i].EntidadClaveSolicitante, arreglo[i].Estatus.substring(0, 3)) + "</td>";
//                renglon += "</tr>"
//                $("#datos tbody").append(renglon);
//            }

//            AfterAjax();
//            AfterAjax2();
//        },
//        error: function (request, error) {
//            
//        }
//    });

//    url = '/Articulo/ObtenPaginas/';
//    $.ajax({
//        url: url,
//        dataType: 'json',
//        data: { 'data': myJSONText },
//        type: "POST",
//        success: function (data) {
//            LlenaPaginacion(data);
//        },
//        error: function (request, error) {
//           
//        }
//    });
//}


function LlenaGrid(pag)
{
    _pag = pag;

    var objOrdenCompraDistribuidor = {};
    objOrdenCompraDistribuidor.pag = pag;
    objOrdenCompraDistribuidor.estatus = $("#buscastatus").val();
    objOrdenCompraDistribuidor.distribuidor = $("#buscadistribuidor").val();
    objOrdenCompraDistribuidor.pedido = $("#abuscar").val();
    objOrdenCompraDistribuidor.esPagare = $("#buscapagvta").val();
    objOrdenCompraDistribuidor.pagadas = 1;

    var myJSONText = JSON.stringify(objOrdenCompraDistribuidor);

    
    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
        "ajax": {
            "url": "/Pedido/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText },
        },
        "fnInitComplete": function (oSettings, json) {
            AfterAjax();
           // AfterAjax2();

        },
        "fnDrawCallback": function (oSettings) {
            AfterAjax();
            AfterAjax2();

        },
        "columns": [
            { "data": "NumeroPedido", "orderable": true },
            { "data": "Destino", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Total", "orderable": false },
            { "data": "Estatus", "orderable": false },


            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    if (full.EsPagare == "1") {
                       return "Pagare"
                    }
                    else {
                     return "Venta"
                    }
                }
            },
            {
                sortable: false,
                "render": function (data, type, full, meta) {                    
                    return DameSalidas(full.Clave, full.Estatus.substring(0, 3)) + "&nbsp;" + DameAcciones(full.Clave, full.EntidadClaveAfectada, full.EntidadClaveSolicitante, full.Estatus.substring(0, 3));
                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}














function AfterAjax() {

    $(".ver").click(function () {

        var elrel = $(this).attr('rel');
        //alert(elrel);
       
        $("#comentario").val('');
        $("#autoriza_id").val(elrel);
        $("#eliframe").attr("src", "/Pedido/Detalle?pdf=2&p=" + elrel);

        $('#ver').modal('show');
    });

    $(".pdf").click(function () {

        var elrel = $(this).attr('rel');
        window.location.href = "/Pedido/Detalle?pdf=1&p=" + elrel;
    });

    $(".delarticulo").click(function () {
        var cual = $(this).val();
        alert(cual);
        objArticulos.splice(index, 1);
        ActualizaLista();
    });

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Elimina Artículo");
        $('#elimina').modal('show');

        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#del_id").val(oRol.Clave);
                $("#titulowindel").html("Elimina Artículo " + oRol.Descripcion);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });

    $(".versalidas").click(function () {
       
        var cual = $(this).attr('rel');
        _orden = cual;
        ShowSalidas(cual);
    });



    $(".edit").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Rol");
        $('#edita').modal('show');

        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oRol = eval(data);

                $("#ed_id").val(oRol.Clave);
                $("#ed_nombre").val(oRol.Descripcion);

                $("#ed_existencias").val(oRol.Existencias);
                $("#ed_tope").val(oRol.TopeMinimo);

                $("#ed_unidad").val(oRol.catUnidadClave);
                $("#ed_porcentaje").val(oRol.Porcentaje);

                $("#ed_unidad").val(oRol.catUnidadClave);

                $("#ed_tipomaterial").val(oRol.catTipoArticuloClave);
                $("#ed_clasificacionmaterial").val(oRol.catClasificacionMaterialClave);


                $("#ed_ubicacion").val(oRol.catUbicacionClave);

                $("#ed_proveedor1").val(oRol.proveedor1);
                $("#ed_precio1").val(oRol.precio1);
                $("#ed_moneda1").val(oRol.moneda1);

                $("#ed_proveedor2").val(oRol.proveedor2);
                $("#ed_precio2").val(oRol.precio2);
                $("#ed_moneda2").val(oRol.moneda2);

                $("#ed_proveedor3").val(oRol.proveedor3);
                $("#ed_precio3").val(oRol.precio3);
                $("#ed_moneda3").val(oRol.moneda3);

                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}

function ShowSalidas(cual)
{
    $("#titulosalidasdetalle").html("Ver salidas");
    $('#salidasdetalle').modal('show');

    var objRoles = {};
    objRoles.pag = 1;
    objRoles.pedido = cual;
    objRoles.tipoentidad = 1;

    var myJSONText = JSON.stringify(objRoles);

    var url = '/SalidaMaterial/ObtenLista2/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#lassalidas tbody").empty();

            var arreglo = eval(data);
            var tbody = '';

            for (var i = 0; i < arreglo.length; i++) {
                tbody += "<tr>";
                tbody += "<td>" + arreglo[i].NumeroPedido + "</td>";
                tbody += "<td>" + arreglo[i].Fecha + "</td>";

                arreglo[i].Estatus.replace('Por autorizar', 'Por recepcionar');

                tbody += "<td>" + arreglo[i].Estatus + "</td>";

                tbody += "<td><button type='button' class='btn btn-info btn-xs versalida' rel='" + arreglo[i].Clave + "'>&nbsp;Ver salida</button>";

                if (arreglo[i].Estatus != "Cancelado") {
                    tbody += "&nbsp;<button type='button' class='btn btn-default btn-xs envio' rel='" + arreglo[i].Clave + "' data-send='" + arreglo[i].Total_USD + "'>&#9993;&nbsp;Datos de Envio</button>";
                }

                tbody += "&nbsp;&nbsp;";

                //tbody += "<td>";
                if (arreglo[i].Estatus != "Cancelado") {


                    tbody += "<button type='button' class='btn btn-warning btn-xs editarsalida' style='display:none;' data-pedido='" + arreglo[i].NumeroPedido + "' rel='" + arreglo[i].Clave + "'>&nbsp;Editar</button>";

                    if (arreglo[i].Estatus != "Completo" && arreglo[i].Estatus != "Parcial") {
                        tbody += "<button type='button' style='display:none' class='btn btn-danger btn-xs cancelarsalida' rel='" + arreglo[i].Clave + "'>&nbsp;Cancelar</button>";
                    }
                }
                tbody += "</td>";
                tbody += "</tr>";
            }

            $("#lassalidas tbody").append(tbody);

            $(".versalida").click(function () {
                var elrel = $(this).attr('rel');
                $("#eliframesalida").attr("src", "/SalidaMaterial/Detalle?pdf=2&s=" + elrel);
                $('#versalida').modal('show');
            });

            $(".envio").click(function () {
                _sub = 1;
                Envio($(this).attr("rel"), $(this).attr("data-send"));
            });

            $(".cancelarsalida").click(function () {

                var cual = $(this).attr('rel');

                $("#cancelar_id").val(cual);
                $("#cancelar").modal('show');

            });

        }
    });
}

function DameAccionesArticulo(cual)
{
    return "<button type='button' class='btn btn-danger btn-xs delarticulo' rel='" + cual + "'>&nbsp;Elimina</button>";
}

function DameDetalle(elid) {
    return "<button type='button' class='btn btn-default btn-xs ver' rel='" + elid + "'>&nbsp;Ver orden de compra</button><button type='button' class='btn btn-info btn-xs pdf' rel='" + elid + "'>&nbsp;PDF</button>";
}

function DameSalidas(elid, estatus) {
    return "<button type='button' class='btn btn-default btn-xs versalidas' rel='" + elid + "' " + ((estatus == "Aut" || estatus == "Can") ? "disabled" : "") + ">&nbsp;Ver Salidas</button>";
}

function DameAcciones(elid, retirarde, poneren, estatus)
{
    console.log()
    return "<button type='button' class='btn btn-default btn-xs ver' rel='" + elid + "'>&nbsp;Ver orden de compra</button>&nbsp;"
        + "<button type='button' class='btn btn-info btn-xs seleccionarpedido' rel='" + elid + "' data-almacen='" + retirarde + "' data-destino='" + poneren + "' " + ((estatus == "Com" || estatus == "Can") ? "disabled" : "") + ">&nbsp;Salida</button>";
}