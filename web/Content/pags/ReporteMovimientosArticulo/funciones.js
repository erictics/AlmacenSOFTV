﻿ObtenerAlamcenes($('#dis').val());

$('#dis').change(function () {
    ObtenerAlamcenes($('#dis').val());
});



$('#cla').change(function () {
    ObtenerTiposArt($('#cla').val())
});


$('#tip').change(function () {

    ObtenerArticulos($('#tip').val())
})


$(document).ready(function () {

    $('#todosalm').on('ifChecked', function (event) {
        $('#almacen').empty().append('<option value="0">Todos los almacenes</option>')
    });

    $('#todosalm').on('ifUnchecked', function (event) {
        ObtenerAlamcenes($('#dis').val());
    });    

    $('#todosart').on('ifChecked', function (event) {
        $('#tip').empty().append("<option value='0'>Todos los artículos</option>");
        $('#articulos').empty().append("<option value='0'>Todos los artículos</option>");


    });

    $('#todosart').on('ifUnchecked', function (event) {
        //$('#cla').val(0);
        $('#tip').empty().append("<option value='0'>Tipos de artículo</option>");
        $('#articulos').empty().append("<option value='0'>Artículos</option>");
        ObtenerTiposArt($('#cla').val());
    });


});
function ObtenerAlamcenes(iddistribuidor) {
    $('#almacen').empty();
    var url = '/MovimientoTecnicos/AlmacenesDistribuidor/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'IdDistribuidor': iddistribuidor },
        type: "POST",
        success: function (data) {
            for (var s = 0; s < data.length; s++) {
                $('#almacen').append("<option value=" + data[s].EntidadClave + ">" + data[s].Nombre + "</option>");
            }

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}






function ObtenerTiposArt(idclasif) {

    $('#tip').empty();
    var url = '/MovimientoTecnicos/TiposArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idclas': idclasif },
        type: "POST",
        success: function (data) {
            $('#tip').append("<option value='0'>Todos los tipos</option>")
            for (var s = 0; s < data.length; s++) {
                $('#tip').append("<option value=" + data[s].catTipoArticuloClave + ">" + data[s].Descripcion + "</option>")
            }

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });

}


function ObtenerArticulos(idtip) {

    $('#articulos').empty();
    var url = '/MovimientoTecnicos/Articulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idtip': idtip },
        type: "POST",
        success: function (data) {

            $('#articulos').append("<option value='0'>Todos los artículos</option>")
            for (var s = 0; s < data.length; s++) {
                $('#articulos').append("<option value=" + data[s].ArticuloClave + ">" + data[s].Nombre + "</option>")
            }

        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
}

$('#pdf').click(function () {
    var distribuidor = $('#dis').val();
    var almacen = $('#almacen').val();
    var proceso = $('#proceso').val();
    var clasificacion = $('#cla').val();
    var tipo = $('#tip').val();
    var articulo = $('#articulos').val();
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();

    if (fechainicio == "" || fechafin == "") {
        toastr.warning("Coloca un rango de fechas");
        return;
    }

    window.location.href = "/MovimientoArticulos/Detalle?distribuidor=" + distribuidor +
        "&almacen=" + almacen + "&clasificacion=" + clasificacion + "&tipo="
        + tipo + "&articulo=" + articulo + "&fechainicio=" + fechainicio + "&fechafin=" + fechafin + "&proceso=" + proceso;
});


$('#excel').click(function () {
    var distribuidor = $('#dis').val();
    var almacen = $('#almacen').val();   
    var proceso = $('#proceso').val();
    var clasificacion = $('#cla').val();
    var tipo = $('#tip').val();
    var articulo = $('#articulos').val();
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();

    if (fechainicio == "" || fechafin == "") {
        toastr.warning("Coloca un rango de fechas");
        return;
    }

    window.location.href = "/MovimientoArticulos/Excel?distribuidor=" + distribuidor +
        "&almacen=" + almacen + "&clasificacion=" + clasificacion + "&tipo="
        + tipo + "&articulo=" + articulo + "&fechainicio=" + fechainicio + "&fechafin=" + fechafin + "&proceso="+proceso;
});