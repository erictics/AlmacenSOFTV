﻿
var soy_distribuidor = false;
var _objPedido = {};
var _productoSeleccionado = -1;
var _pagare = false;

$(document).ready(function () {
    ActualizaTipos();
    obtenerUsuario();
    LlenaGrid(1);

    $("#buscastatus").change(function () {
        LlenaGrid(1);
    });
    $("#buscaproveedor").change(function () {
        LlenaGrid(1);
    });
    $("#buscar").click(function () {

        var valid = jQuery('#formbusca').validationEngine('validate');
        if (valid) {
            LlenaGrid(1);
        }
    });

    jQuery('#formaedita').validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition: false
    });
    $(".preciootrosgastos").blur(function () {
        Resumen();
    });

    function obtenerUsuario() {

        var url = '/Usuarios/queusuariosoy/';
        $.ajax({
            url: url,
            dataType: 'json',
            type: "GET",
            success: function (data) {
                if (data == "ADM") {
                    soy_distribuidor = false;
                    $('#pservicio').hide();
                }
                else if (data == "AD") {
                    soy_distribuidor = true;
                    $('#pservicio').show();
                }
                else {
                    toastr.warning("Error, no se ha iniciado sesión ");
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    }

    $("#agregar").click(function () {
        if (soy_distribuidor) {
            if ($('#ed_almacen').val() == "" || $('#ed_almacen').val() == null || $('#ed_almacen').val() == undefined || $('#selectServicio').val() == null || $('#selectServicio').val() == undefined || $('#selectServicio').val() == "") {

                toastr.warning("Atención, Selecciona el almacén y el servicio");

            } else {

                $('#articulo').modal('show');
                $("#ed_clasificacionmaterial").removeAttr('disabled');
                $("#ed_tipomaterial").removeAttr('disabled');
                $("#ed_articulo").removeAttr('disabled');
                if (_pagare) {
                    $('#titulowinarticulo').html('Agregar Artículo (No Venta)');
                    $("#div_pdisponible").show();
                    $("#div_tmoneda").hide();
                    $("#c_importedolares").hide();
                }
                else {
                    $('#titulowinarticulo').html('Agregar Artículo (Venta)');
                    $("#div_pdisponible").hide();
                    $("#div_tmoneda").show();
                    $("#c_importedolares").show();
                }
                _productoSeleccionado = -1;
                $("#ed_clasificacionmaterial").val('');
                $("#ed_tipomaterial").val('');
                $("#ed_articulo").val('');
                $("#ed_cantidad").val('');
                $("#ed_precio").val('');
                $("#ed_moneda").val('');
                $("#ed_disponible").val('');
                $("#ed_precio").val('');
            }
        }
        else {

            $('#articulo').modal('show');
            $("#ed_clasificacionmaterial").removeAttr('disabled');
            $("#ed_tipomaterial").removeAttr('disabled');
            $("#ed_articulo").removeAttr('disabled');
            if (_pagare) {
                $('#titulowinarticulo').html('Agregar Artículo (No Venta)');
                $("#div_pdisponible").show();
                $("#div_tmoneda").hide();
                $("#c_importedolares").hide();
            }
            else {
                $('#titulowinarticulo').html('Agregar Artículo (Venta)');
                $("#div_pdisponible").hide();
                $("#div_tmoneda").show();
                $("#c_importedolares").show();
            }
            _productoSeleccionado = -1;

            $("#ed_clasificacionmaterial").val('');
            $("#ed_tipomaterial").val('');
            $("#ed_articulo").val('');
            $("#ed_cantidad").val('');
            $("#ed_precio").val('');
            $("#ed_moneda").val('');
            $("#ed_disponible").val('');
            $("#ed_precio").val('');
        }
    });


    $("#ed_articulo").change(function () {
        if (!_principal) {
            if (_pagare) {
                var disponibleenpagare = $("#ed_articulo option:selected").attr('data-pagare');
                $("#ed_disponible").val(disponibleenpagare);
                var preciopagare = $("#ed_articulo option:selected").attr('data-preciopagare');
                $("#ed_precio").val(preciopagare);
                var moneda = $("#ed_articulo option:selected").attr('data-moneda');
                $("#ed_moneda").val(moneda);
                $("#ed_cantidad").removeClass();
                $("#ed_cantidad").addClass('form-control validate[required,custom[integer],max[' + disponibleenpagare + ']]');
            }
            else {
                var preciopagare = $("#ed_articulo option:selected").attr('data-preciopagare');
                $("#ed_precio").val(preciopagare);
                var moneda = $("#ed_articulo option:selected").attr('data-moneda');
                $("#ed_moneda").val(moneda);
                $("#ed_cantidad").removeClass();
                $("#ed_cantidad").addClass('form-control validate[required,custom[integer]]');
                $("#ed_disponible").val('');
            }

            $("#ed_moneda").prop('disabled', true);
        }
        else {
            $("#ed_moneda").removeAttr('disabled');
        }
    });

    $(".optiontipo").hide();
    $("#ed_clasificacionmaterial").change(function () {
        $("#ed_precio").val('');
        $("#ed_disponible").val('');
        ActualizaTipos();
    });

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $('#selectServicio').attr('disabled', false);
        $('#ed_almacen').attr('disabled', false);
        if (_pagare) {
            $('#titulowin').html('Agregar Orden de Compra (Pagaré)');
            $("#c_importedolares").hide();
        }
        else {
            $('#titulowin').html('Agregar Orden de Compra (Venta)');
            $("#c_importedolares").show();
        }
        _objPedido = {};
        _objPedido.Articulos = new Array();
        _productoSeleccionado = -1;
        $('#edita').modal('show');
        $("#ed_id").val('');
        $("#ed_nombre").val('');
        $("#ed_proveedor").val('');
        $("#ed_almacen").val('');
        $("#ed_autorizador").val('');
        $("#ed_observaciones").val('');
        $("#ed_conceptooc1").val('');
        $("#ed_preciooc1").val('');
        $("#ed_monedaoc1").val('');
        $("#ed_conceptooc2").val('');
        $("#ed_preciooc2").val('');
        $("#ed_monedaoc2").val('');
        $("#ed_clasificacionmaterial").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_articulo").val('');
        $("#ed_cantidad").val('');
        $("#ed_precio").val('');
        $("#ed_moneda").val('');
        ActualizaLista();
        $("#tt1").click();
    });


    $("#adddistribuidor").click(function () {
        $('#pagare').modal('show');
        $("#ed_id").val('');
        $("#titulowinpagare").html('Seleccione tipo de Orden de Compra');
    });

    $("#artscompra_si").click(function () {
        _pagare = false;
        $("#add").click();
        $('#pagare').modal('hide');
    });

    $("#artscompra_no").click(function () {
        _pagare = true;
        $("#add").click();
        $('#pagare').modal('hide');
    });

    $("#formacancela").submit(function (e) {
        e.preventDefault();
        var url = '/Pedido/Cancela/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formacancela').serialize(),
            type: "POST",
            success: function (data) {
                $("#cancela").modal('hide');
                toastr.success('El pedido se canceló correctamente');
                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();
        var url = '/Articulo/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {
                $("#elimina").modal('hide');
                toastr.success('El artículo se eliminó correctamente');
                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#agregararticulo").click(function () {
        var valid = jQuery('#formaarticulo').validationEngine('validate');
        if (valid) {
            var objArticulo = {};
            objArticulo.ArticuloClasificacionClave = $("#ed_clasificacionmaterial").val();
            objArticulo.ArticuloTipoClave = $("#ed_tipomaterial").val();
            objArticulo.ArticuloClave = $("#ed_articulo").val();
            objArticulo.ArticuloTexto = $("#ed_articulo option:selected").text();
            objArticulo.Cantidad = $("#ed_cantidad").val();
            objArticulo.CantidadEntregada = 0;
            objArticulo.PrecioUnitario = $("#ed_precio").val().replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
            objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();
            if (_productoSeleccionado == -1) {
                _objPedido.Articulos.push(objArticulo);
            }
            else {
                _objPedido.Articulos[_productoSeleccionado] = objArticulo;
            }
            ActualizaLista();
            $('#articulo').modal('hide');
            Resumen();
        }
    });

    $("#formaedita").submit(function (e) {
        e.preventDefault();
        if (_objPedido.Articulos.length == 0) {
            toastr.error('Faltan artículos en el pedido, debe agregar al menos un artículo para realizar un pedido');
            return;
        }
        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            Resumen();
            _objPedido.Servicio = $("#selectServicio").val();
            _objPedido.catProveedorClave = $("#ed_proveedor").val();
            _objPedido.EntidadClaveSolicitante = $("#ed_almacen").val();
            _objPedido.UsuarioClaveAutorizacion = $("#ed_autorizador").val();
            _objPedido.Observaciones = $("#ed_observaciones").val();
            _objPedido.TipoCambio = $("#ed_tipocambio").val();
            _objPedido.Iva = $("#ed_iva").val();
            _objPedido.Descuento = $("#ed_descuento").val();
            _objPedido.DiasCredito = $("#ed_diascredito").val();
            var objOtroGasto1 = {};
            objOtroGasto1.Concepto = $("#ed_conceptooc1").val();
            objOtroGasto1.Precio = $("#ed_preciooc1").val();
            objOtroGasto1.Moneda = $("#ed_monedaoc1").val();
            var objOtroGasto2 = {};
            objOtroGasto2.Concepto = $("#ed_conceptooc2").val();
            objOtroGasto2.Precio = $("#ed_preciooc2").val();
            objOtroGasto2.Moneda = $("#ed_monedaoc2").val();
            var objOtrosGastos = new Array();
            objOtrosGastos.push(objOtroGasto1);
            objOtrosGastos.push(objOtroGasto2);
            _objPedido.OtrosGastos = objOtrosGastos;
            _objPedido.ToSubtotal = $("#to_subtotal").val();
            _objPedido.ToDescuento = $("#to_descuento").val();
            _objPedido.ToIVA = $("#to_iva").val();
            _objPedido.Total = $("#to_total").val();
            _objPedido.ToSubtotal_usd = $("#to_subtotal_usd").val();
            _objPedido.ToDescuento_usd = $("#to_descuento_usd").val();
            _objPedido.ToIVA_usd = $("#to_iva_usd").val();
            _objPedido.Total_usd = $("#to_total_usd").val();
            if (_pagare) {
                _objPedido.EsPagare = 1;
            }
            else {
                _objPedido.EsPagare = 0;
            }
            var myJSONText = JSON.stringify(_objPedido);
            var url = '/Pedido/Guarda/';
            $.ajax({
                url: url,
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {
                    $("#framecanvas").attr("src", "/Pedido/Detalle?pdf=2&p=" + data);
                    $("#titulocanvas").empty().append("Orden de compra");
                    $('#repo').click();
                    $("#edita").modal('hide');
                    toastr.success('Hecho, el pedido se guardó correctamente');
                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Error, surgió un error, intente nuevamente más tarde');
                }
            });
        }
        else {
            toastr.error('Error, debe llenar los campos obligatorios');
        }
    });

    $("#ed_tipomaterial").change(function () {
        $("#ed_precio").val('');
        $("#ed_disponible").val('');
        ActualizaMateriales();
    });

});

function Llena() {
    $("#ed_proveedor").val(_objPedido.catProveedorClave);
    $("#ed_almacen").val(_objPedido.EntidadClaveSolicitante);
    obtenServicios(_objPedido.EntidadClaveSolicitante, _objPedido.soyinternet);
    $("#ed_autorizador").val(_objPedido.UsuarioClaveAutorizacion);
    $("#ed_observaciones").val(_objPedido.Observaciones);
    $("#ed_tipocambio").val(_objPedido.TipoCambio);
    $("#ed_iva").val(_objPedido.Iva);
    $("#ed_descuento").val(_objPedido.Descuento);
    $("#ed_diascredito").val(_objPedido.DiasCredito);
    try {
        var objOtroGasto1 = _objPedido.OtrosGastos[0];
        $("#ed_conceptooc1").val(objOtroGasto1.Concepto);
        $("#ed_preciooc1").val(objOtroGasto1.Precio);
        $("#ed_monedaoc1").val(objOtroGasto1.Moneda);
    }
    catch (e) { }

    try {
        var objOtroGasto2 = _objPedido.OtrosGastos[1];
        $("#ed_conceptooc2").val(objOtroGasto2.Concepto);
        $("#ed_preciooc2").val(objOtroGasto2.Precio);
        $("#ed_monedaoc2").val(objOtroGasto2.Moneda);
    }
    catch (e) { }
    ActualizaLista();
    Resumen();
}

function Resumen() {
    var suma = 0;
    var suma_usd = 0;
    var tipocambio = 1;
    for (var i = 0; i < _objPedido.Articulos.length; i++) {
        if (_objPedido.Articulos[i].MonedaTexto.toLowerCase().indexOf('pesos') > -1) {
            var totalproducto = parseFloat(_objPedido.Articulos[i].PrecioUnitario) * parseInt(_objPedido.Articulos[i].Cantidad);
            suma += totalproducto;
        }
        else if (_objPedido.Articulos[i].MonedaTexto.toLowerCase().indexOf('dólar') > -1 || _objPedido.Articulos[i].MonedaTexto.toLowerCase().indexOf('dolar') > -1) {
            var totalproducto = parseFloat(_objPedido.Articulos[i].PrecioUnitario) * parseInt(_objPedido.Articulos[i].Cantidad);
            suma_usd += totalproducto;
        } else {
            var totalproducto = parseFloat(_objPedido.Articulos[i].PrecioUnitario) * parseInt(_objPedido.Articulos[i].Cantidad);
            suma += totalproducto;
        }
    }
    if ($("#ed_preciooc1").val() != '') {
        if ($("#ed_monedaoc1 option:selected").text().toLowerCase().indexOf('pesos') > -1) {
            suma += parseFloat($("#ed_preciooc1").val());
        }
        else if ($("#ed_monedaoc1 option:selected").text().toLowerCase().indexOf('dólar') > -1 || $("#ed_monedaoc1 option:selected").text().toLowerCase().indexOf('dolar') > -1) {
            suma_usd += parseFloat($("#ed_preciooc1").val());
        } else {
            suma += parseFloat($("#ed_preciooc1").val());
        }
    }
    if ($("#ed_preciooc2").val() != '') {
        if ($("#ed_monedaoc2 option:selected").text().toLowerCase().indexOf('pesos') > -1) {
            suma += parseFloat($("#ed_preciooc2").val());
        }
        else if ($("#ed_monedaoc2 option:selected").text().toLowerCase().indexOf('dólar') > -1 || $("#ed_monedaoc2 option:selected").text().toLowerCase().indexOf('dolar') > -1) {
            suma_usd += parseFloat($("#ed_preciooc2").val());
        } else {
            suma += parseFloat($("#ed_preciooc2").val());
        }
    }
    var eliva = 0;
    eliva = parseFloat($("#ed_iva").val());
    var descuento = 0;
    var descuento_po = 0;
    var descuento_usd = 0;
    var descuento_po_usd = 0;
    try {
        if ($("#ed_descuento").val() != "") {
            descuento_po = parseFloat($("#ed_descuento").val());
            descuento = descuento_po * suma / 100;
            descuento_po_usd = parseFloat($("#ed_descuento").val());
            descuento_usd = descuento_po_usd * suma_usd / 100;
        }
    }
    catch (e)
    { }

    var subtotaldescuento = 0;
    var subtotaldescuento_usd = 0;
    subtotaldescuento = suma - descuento;
    subtotaldescuento_usd = suma_usd - descuento_usd;
    var iva = 0;
    var total = 0;
    var iva_usd = 0;
    var total_usd = 0;
    iva = (subtotaldescuento * eliva / 100);
    total = subtotaldescuento + (subtotaldescuento * eliva / 100);
    iva_usd = (subtotaldescuento_usd * eliva / 100);
    total_usd = subtotaldescuento_usd + (subtotaldescuento_usd * eliva / 100);
    $("#to_subtotal").val(suma);
    $("#to_descuento").val(descuento);
    $("#to_iva").val(iva);
    $("#to_total").val(total);
    $("#to_subtotal_usd").val(suma_usd);
    $("#to_descuento_usd").val(descuento_usd);
    $("#to_iva_usd").val(iva_usd);
    $("#to_total_usd").val(total_usd);
    $("#reiva_po").html("(" + $("#ed_iva").val() + "%" + ")");
    $("#reiva").html(ToMoneda(iva) + "%");
    $("#reiva_po_usd").html("(" + $("#ed_iva_usd").val() + "%" + ")");
    $("#reiva_usd").html(ToMoneda(iva_usd) + "%");
    $("#resubtotal").html(ToMoneda(suma));
    $("#redescuento").html(ToMoneda(descuento));
    $("#redescuento_po").html("(" + descuento_po + "%" + ")");
    $("#retotal").html(ToMoneda(total));
    $("#resubtotal_usd").html(ToMoneda(suma_usd));
    $("#redescuento_usd").html(ToMoneda(descuento_usd));
    $("#redescuento_po_usd").html("(" + descuento_po_usd + "%" + ")");
    $("#retotal_usd").html(ToMoneda(total_usd));

}

function ActualizaLista() {
    $("#articulostable tbody").empty();
    for (var i = 0; i < _objPedido.Articulos.length; i++) {
        var total = 0;
        var total_usd = 0;
        if (_objPedido.Articulos[i].MonedaTexto.toLowerCase() == "pesos") {
            total = parseFloat(_objPedido.Articulos[i].PrecioUnitario) * parseInt(_objPedido.Articulos[i].Cantidad);
        }
        else if (_objPedido.Articulos[i].MonedaTexto.toLowerCase().indexOf("dólar") > -1 || _objPedido.Articulos[i].MonedaTexto.toLowerCase().indexOf("dolar") > -1) {
            total_usd = parseFloat(_objPedido.Articulos[i].PrecioUnitario) * parseInt(_objPedido.Articulos[i].Cantidad);
        } else {
            total = parseFloat(_objPedido.Articulos[i].PrecioUnitario) * parseInt(_objPedido.Articulos[i].Cantidad);
        }
        var renglon = "<tr><td>" + _objPedido.Articulos[i].ArticuloTexto + "</td><td>" + _objPedido.Articulos[i].Cantidad + "</td><td>" + ToMoneda(_objPedido.Articulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td>";

        if (!_pagare) {
            renglon += "<td>" + ToMoneda(total_usd) + "</td>";
        }
        renglon += "<td>" + DameAccionesArticulo(i) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }
    AfterAjax();
    Resumen();

}
var _pag = 1;
function LlenaGrid(pag) {
    AfterAjax();
    var objRoles = {};
    objRoles.pag = pag;
    objRoles.estatus = $("#buscastatus").val();
    objRoles.proveedor = $("#buscaproveedor").val();
    objRoles.pedido = $("#abuscar").val();
    var myJSONText = JSON.stringify(objRoles);
    $('#datos').dataTable({
        "processing": true,
        "responsive": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
        "ajax": {
            "url": "/Pedido/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "fnInitComplete": function (oSettings, json) {


        },
        "fnDrawCallback": function (oSettings) {

            AfterAjax();
        },
        "columns": [
        { "data": "NumeroPedido", "orderable": false },
            { "data": "Destino", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Total", "orderable": false },
            { "data": "Total_USD", "orderable": false },
            { "data": "Estatus", "orderable": false },
            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    return '<button type="button" rel=' + full.Clave + ' class="btn ink-reaction btn-floating-action btn-xs btn-danger btn-xs pdf" onClick="Imprimepdf(\'' + full.Clave + '\')" data-toggle="tooltip" data-placement="right" title="" data-original-title="Descarga PDF"> <i class="md md-file-download" aria-hidden="true"></i></button><button class="btn ink-reaction btn-floating-action btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="" data-original-title="Ver orden"   onClick="Ver(\'' + full.Clave + '\')"  ><i class="fa fa-search" ></i></button>';
                }
            },

            {
                sortable: false,
                "render": function (data, type, full, meta) {
                    var renglon = "";
                    if (_principal == false) {
                        renglon = DameFacturacion(full.Clave, full.Estatus, full.FacturacionClave, full.EsPagare);
                    }
                    if (full.Estatus == 'Por autorizar' || full.Estatus == 'Por surtir' || full.Estatus == 'Rechazado' || full.Estatus == 'Autorizado (Por Surtir)' || full.Estatus == 'Por pagar') {

                        if (full.EntidadClaveSolicitante != '00000000-0000-0000-0000-000000000001' && full.EsPagare == "0") {
                            if (full.Estatus == 'Por autorizar' || full.Estatus == 'Por pagar') {
                                renglon += '<td><button type="button" class="btn btn-warning ink-reaction btn-xs edita"  onClick="EditaOrden(\'' + full.Clave + '\')" rel=' + full.Clave + '><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edita</button></td>';
                                renglon += '<td><button type="button" class="btn btn-danger ink-reaction btn-xs cancela" onClick="CancelaOrden(\'' + full.Clave + '\')" rel="' + full.Clave + '"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Cancela</button></td>';
                            }
                        }
                        else {
                            renglon += '<td><button type="button" class="btn btn-warning btn-xs edita"  onClick="EditaOrden(\'' + full.Clave + '\')" rel="' + full.Clave + '"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edita</button></td>';
                            renglon += '<td><button type="button" class="btn btn-danger ink-reaction btn-xs cancela" onClick="CancelaOrden(\'' + full.Clave + '\')"   rel="' + full.Clave + '"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Cancela</button></td>';
                        }
                    }
                    return renglon;
                }
            }

        ],

        language: {
            processing: "",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "order": [[0, "asc"]]
    })
}


function Ver(elrel) {

    $('#repo').click();
    $('#titulocanvas').empty().append("Detalle de orden de compra");
    $("#framecanvas").attr("src", "/Pedido/Detalle?pdf=2&p=" + elrel);
}
function Imprimepdf(elrel) {
    window.location.href = "/Pedido/Detalle?pdf=1&p=" + elrel;
}
function EditaOrden(cual) {
    _productoSeleccionado = -1;
    $("#titulowin").html("Editar Pedido");
    $('#edita').modal('show');
    $("#tt1").click();

    var url = '/Pedido/ObtenUI/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {
            $('#selectServicio').attr('disabled', true);
            $('#ed_almacen').attr('disabled', true);
            _objPedido = eval(data);
            if (_objPedido.EsPagare == "1") {
                _pagare = true;
            }
            else {
                _pagare = false;
            }
            Llena();
        }
    });
}
function CancelaOrden(cual) {
    _productoSeleccionado = -1;
    $("#titulowincancela").html("Cancela Pedido");
    $('#cancela').modal('show');
    $("#tt1").click();
    $("#cancela_id").val(cual);
    var url = '/Pedido/Obten/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': cual },
        type: "POST",
        success: function (data) {
            var pedido = eval(data);
        }
    });

}

function AfterAjax() {

    $(".delarticulo").click(function () {
        var cual = $(this).attr('rel');
        _objPedido.Articulos.splice(cual, 1);
        ActualizaLista();
    });
    $(".editaarticulo").click(function () {
        $("#articulo").modal("show");
        var cual = $(this).attr('rel');
        _productoSeleccionado = cual;
        $("#ed_clasificacionmaterial").val(_objPedido.Articulos[cual].ArticuloClasificacionClave);
        ActualizaTipos();
        $("#ed_tipomaterial").val(_objPedido.Articulos[cual].ArticuloTipoClave);
        ActualizaMateriales();
        $("#ed_cantidad").val(_objPedido.Articulos[cual].Cantidad);
        $("#ed_precio").val(_objPedido.Articulos[cual].PrecioUnitario);
        $("#ed_moneda").val(_objPedido.Articulos[cual].catTipoMonedaClave);
        if (_pagare) {
            $('#titulowinarticulo').html('Agregar Artículo (No Venta)');
            $("#div_pdisponible").show();
            $("#div_tmoneda").hide();
        }
        else {
            $('#titulowinarticulo').html('Agregar Artículo (Venta)');
            $("#div_pdisponible").hide();
            $("#div_tmoneda").show();
        }
        $("#ed_clasificacionmaterial").prop('disabled', true);
        $("#ed_tipomaterial").prop('disabled', true);
        $("#ed_articulo").prop('disabled', true);
    });

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Elimina Artículo");
        $('#elimina').modal('show');
        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                var oRol = eval(data);
                $("#del_id").val(oRol.Clave);
                $("#titulowindel").html("Elimina artículo " + oRol.Descripcion);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });



    //$(".cancelarfactura").click(function () {
    //    _productoSeleccionado = -1;
    //    var cual = $(this).attr('rel');
    //    $("#cancela_id").val(cual);
    //    var url = '/Factura/Cancela/';
    //    $.ajax({
    //        url: url,
    //        dataType: 'json',
    //        data: { 'data': cual },
    //        type: "POST",
    //        success: function (data) {
    //            LlenaGrid(_pag);
    //        }
    //    });
    //});

    //$(".factura").click(function () {
    //    _productoSeleccionado = -1;
    //    var cual = $(this).attr('rel');
    //    $("#cancela_id").val(cual);
    //    var url = '/Factura/Solicita/';
    //    $.ajax({
    //        url: url,
    //        dataType: 'json',
    //        data: { 'data': cual },
    //        type: "POST",
    //        success: function (data) {
    //            LlenaGrid(_pag);
    //        }
    //    });
    //});





}


function DameAccionesArticulo(cual) {
    return "<button type='button' class='btn btn-danger ink-reaction btn-xs delarticulo' rel='" + cual + "'><i class='fa fa-trash'></i> Elimina</button>"
            + "<button type='button' class='btn btn-warning  ink-reaction btn-xs editaarticulo' rel='" + cual + "'><i class='fa fa-pencil'></i>Edita</button>";
}

function DameFacturacion(elid, estado, estadofacturacion, espagare) {
    return "";
}
//****************ORDEN DE COMPRA 
function DameOpciones(elid, estado, espagare, entidadsolicitante) {
    var elregresa = "";
    return elregresa;
}

function DameAcciones(elid) {
    return "<button type='button' class='btn btn-default btn-xs edit' rel='" + elid + "'>&nbsp;Editar</button><button type='button' class='btn btn-danger btn-xs del' rel='" + elid + "'>&nbsp;Elimina</button>";
}

var opciones = new Array();
function ActualizaTipos() {
    $("#ed_tipomaterial").val('');
    if ($("#temporal").length == 0) {
        var select = '';
        select += "<select id='temporal' style='display:none'>";

        $(".optiontipo").each(function () {
            select += "<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "' data-pagare='" + $(this).attr('data-pagare') + "'>" + $(this).text() + "</option>";
        });

        select += "</select>";
        $("#ed_tipomaterial").after(select);
    }
    $("#ed_tipomaterial").empty();
    $("#ed_tipomaterial").append("<option value='' selected>Seleccione</option>");

    var datapagare = 0;
    if (_pagare) {
        datapagare = 1;
    }
    else {
        datapagare = 0;
    }

    $("#temporal option").each(function () {
        if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val() && (_principal || datapagare == $(this).attr('data-pagare'))) {
            $("#ed_tipomaterial").append("<option value='" + $(this).val() + "' data-cla='" + $(this).attr('data-cla') + "' data-pagare='" + $(this).attr('data-pagare') + "'>" + $(this).text() + "</option>");
        }
    });
    $("#ed_tipomaterial").val('');
}

function ActualizaMateriales() {

    var tipo = $("#ed_tipomaterial").val();
    var objRoles = {};
    objRoles.tipo = tipo;
    objRoles.Pedido = true;
    objRoles.Almacen = $('#ed_almacen').val();
    objRoles.Servicio = $('#selectServicio').val();
    $("#ed_articulo").empty();
    $("#ed_articulo").append("<option value=''>Seleccione</option>");
    var myJSONText = JSON.stringify(objRoles);
    var url = '/Articulo/ObtenPorTipo2/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            var arreglo = eval(data);
            for (var i = 0; i < arreglo.length; i++) {
                var yaesta = false;
                if (arreglo[i].Activo == "1") {
                    var selected = '';
                    if (_productoSeleccionado != -1) {
                        if (_objPedido.Articulos[_productoSeleccionado].ArticuloClave == arreglo[i].Clave) {
                            selected = "selected";
                            if (_objPedido.ProcesoClave == undefined || _objPedido.ProcesoClave == null) {
                                $("#ed_disponible").val(arreglo[i].Pagare);
                                if (arreglo[i].Pagare == "1") {
                                    $("#ed_cantidad").removeClass();
                                    $("#ed_cantidad").addClass('form-control validate[required,custom[integer],max[' + $("#ed_disponible").val() + ']]');
                                }
                            }
                            else {
                                $("#ed_disponible").val(parseInt(arreglo[i].Pagare));

                                if (arreglo[i].Pagare == "1") {
                                    $("#ed_cantidad").removeClass();
                                    $("#ed_cantidad").addClass('form-control validate[required,custom[integer],max[' + $("#ed_disponible").val() + ']]');
                                }
                            }
                        }
                    }
                    else {
                        for (var j = 0; j < _objPedido.Articulos.length; j++) {
                            if (_objPedido.Articulos[j].ArticuloClave == arreglo[i].Clave) {
                                yaesta = true;
                            }
                        }
                    }
                    if (!yaesta) {
                        $("#ed_articulo").append("<option value='" + arreglo[i].Clave + "' " + selected + " data-pagare='" + arreglo[i].Pagare + "' data-preciopagare='" + arreglo[i].PrecioPagare + "' data-moneda='" + arreglo[i].catMonedaPagareClave + "'>" + arreglo[i].Descripcion + "</option>");
                    }
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function obtenServicios(idalmacen, internet) {
    $('#selectServicio').empty();
    url = '/Almacen/GetServiciosAlmacenGuid/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'guidAlmacen': $('#ed_almacen').val() },
        type: "POST",
        success: function (data) {

            if (data == "") {
                $('#selectServicio').append("<option value=''>No hay servicios asignados</option>");

            } else {

                for (var s = 0; s < data.length; s++) {
                    if (internet == true && parseInt(data[s].idServicio) == 2) {
                        $('#selectServicio').append("<option selected value=" + data[s].idServicio + ">" + data[s].Nombre + "</option>");
                    }
                    else if (internet == false && parseInt(data[s].idServicio) == 1) {
                        $('#selectServicio').append("<option selected value=" + data[s].idServicio + ">" + data[s].Nombre + "</option>");
                    } else {
                        $('#selectServicio').append("<option value=" + data[s].idServicio + ">" + data[s].Nombre + "</option>");
                    }
                }
            }
        },
        error: function (request, error) {
            toastr.error('Surgió un error, intente nuevamente más tarde');
        }
    });
}
$('#ed_almacen').change(function () {
    obtenServicios($('#ed_almacen').val(), false);
});

