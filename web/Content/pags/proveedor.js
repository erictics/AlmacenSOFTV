﻿

$(document).ready(function () {


    $("#ed_lada1contacto").keyup(function () {
        validatelefono(1);
    });


    $("#ed_lada2contacto").keyup(function () {
        validatelefono(2);
    });


    jQuery('#formaedita').validationEngine();

    LlenaGrid(1);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {

        $("#ed_id").val('');
        $("#ed_nombre").val('');

        $("#ed_CalleNumero").val('');
        $("#ed_Colonia").val('');
        $("#ed_CodigoPostal").val('');
        $("#ed_Ciudad").val('');

        $("#titulowin").html("Agregar Proveedor");
        $('#edita').modal('show');
        $("#ed_id").val('');
        $("#ed_nombre").val('');
        $("#ed_CalleNumero").val('');
        $("#ed_Colonia").val('');
        $("#ed_CodigoPostal").val('');
        $("#ed_Ciudad").val('');

        $("#ed_nombrecontacto").val('');
        $("#ed_emailcontacto").val('');
        $("#ed_lada1contacto").val('');
        $("#ed_telefono1contacto").val('');
        $("#ed_lada2contacto").val('');
        $("#ed_telefono2contacto").val('');

        $("#ed_rfc").val('');
    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Proveedor/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {

                $("#elimina").modal('hide');
                Mensaje($("#titulowindel").html(), 'El proveedor se desactivó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                Mensaje('Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#formaactiva").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Proveedor/Activa/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaactiva').serialize(),
            type: "POST",
            success: function (data) {

                $("#activa").modal('hide');
                Mensaje($("#titulowinactiva").html(), 'El proveedor se activó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });
    });

    $("#formaedita").submit(function (e) {
        e.preventDefault();

        validatelefono(1); validatelefono(2);

        var valid = jQuery('#formaedita').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var url = '/Proveedor/Guarda/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formaedita').serialize(),
                type: "POST",
                success: function (data) {

                    $("#edita").modal('hide');
                    Mensaje($("#titulowin").html(), 'El proveedor se guardó correctamente');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
                }
            });

        }

    });

});


function validatelefono(cual) {
    if ($("#ed_lada" + cual + "contacto").val().length == 3) {
        $("#ed_telefono" + cual + "contacto").removeClass();
        $("#ed_telefono" + cual + "contacto").addClass("form-control validate[required,custom[integer],maxSize[7]]");
    } else if ($("#ed_lada" + cual + "contacto").val().length == 2) {
        $("#ed_telefono" + cual + "contacto").removeClass();
        $("#ed_telefono" + cual + "contacto").addClass("form-control validate[required,custom[integer],maxSize[8]]");
    }
}



var _pag = 1;
function LlenaGrid(pag)
{
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;

    var myJSONText = JSON.stringify(objRoles);

    $("#datos tbody").empty();
    $("#loader").show();
    var url = '/Proveedor/ObtenLista/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {

            $("#loader").hide();

            var arreglo = eval(data);
            if (arreglo.length > 0) {
                $("#datos").show();
            }
            else {
                $("#resultados").html("No se encontraron resultados");
            }


            for (var i = 0; i < arreglo.length; i++) {
                var renglon = "<tr><td>" + (i + 1) + "</td><td>";

                if (arreglo[i].Activo == 1) {
                    renglon += arreglo[i].Nombre;
                }
                else {
                    renglon += "<s>" + arreglo[i].Nombre + "</s>";
                }

                renglon += "</td>";
                renglon += "<td>" + DameActivar(arreglo[i].Clave, arreglo[i].Activo) + "</td>";
                renglon += "<td>" + DameAcciones(arreglo[i].Clave, arreglo[i].Activo) + "</td>";
                renglon += "</tr>"
                $("#datos tbody").append(renglon);
            }

            AfterAjax();
        },
        error: function (request, error) {
            Mensaje('Surgió un error, intente nuevamente más tarde');
        }
    });


    url = '/Proveedor/ObtenPaginas/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'data': myJSONText },
        type: "POST",
        success: function (data) {
            LlenaPaginacion(data);
        },
        error: function (request, error) {
            Mensaje('Surgió un error, intente nuevamente más tarde');
        }
    });
}

function AfterAjax() {

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Desactivar Proveedor");
        $('#elimina').modal('show');

        var url = '/Proveedor/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#del_id").val(oRol.Clave);
                $("#titulowindel").html("Desactivar proveedor: " + oRol.Nombre);



                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                Mensaje('Surgió un error', 'intente nuevamente más tarde');
            }
        });

    });


    $(".activar").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowinactiva").html("Habilíta Proveedor");
        $('#activa').modal('show');

        var url = '/Proveedor/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#activa_id").val(oRol.Clave);
                $("#titulowinactiva").html("Activar Proveedor: " + oRol.Nombre);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                Mensaje('Surgió un error', 'intente nuevamente más tarde');
            }
        });

    });

    $(".edit").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Proveedor");
        $('#edita').modal('show');


        $("#ed_id").val('');
        $("#ed_nombre").val('');

        $("#ed_CalleNumero").val('');
        $("#ed_Colonia").val('');
        $("#ed_CodigoPostal").val('');
        $("#ed_Ciudad").val('');

        $("#ed_rfc").val('');

        $("#ed_nombrecontacto").val('');
        $("#ed_emailcontacto").val('');
        $("#ed_lada1contacto").val('');
        $("#ed_telefono1contacto").val('');
        $("#ed_lada2contacto").val('');
        $("#ed_telefono2contacto").val('');

        var url = '/Proveedor/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oRol = eval(data);

                $("#ed_id").val(oRol.Clave);
                $("#ed_nombre").val(oRol.Nombre);

                $("#ed_CalleNumero").val(oRol.CalleNumero);
                $("#ed_Colonia").val(oRol.Colonia);
                $("#ed_CodigoPostal").val(oRol.CodigoPostal);
                $("#ed_Ciudad").val(oRol.Ciudad);

                $("#ed_rfc").val(oRol.RFC);

                $("#ed_nombrecontacto").val(oRol.NombreContacto);
                $("#ed_emailcontacto").val(oRol.EmailContacto);
                $("#ed_lada1contacto").val(oRol.Lada1Contacto);
                $("#ed_telefono1contacto").val(oRol.Telefono1Contacto);
                $("#ed_lada2contacto").val(oRol.Lada2Contacto);
                $("#ed_telefono2contacto").val(oRol.Telefono2Contacto);
                $("#ed_activo").val(oRol.Activo);
                if (oRol.Activo == "True" || oRol.Activo == "1") {
                    $("#ed_activo").prop('checked', 'checked');
                    $("#ed_activo").parent().addClass('checked');
                }
                else {
                    $("#ed_activo").removeProp('checked');
                    $("#ed_activo").parent().removeClass('checked');
                }

                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                Mensaje('Surgió un error', 'intente nuevamente más tarde');
            }
        });

    });
}

function DameActivar(elid, activo) {
    var accion = "";

    if (activo == 1) {
        accion += "<img src='/Content/assets/images/activo.png'>";
    }
    else {
        accion += "<img src='/Content/assets/images/inactivo.png'>";
    }

    return accion;
}

function DameAcciones(elid, activo) {
    var accion = "<button type='button' class='btn btn-default btn-xs edit' rel='" + elid + "'>&nbsp;Editar</button>";

    return accion;
}