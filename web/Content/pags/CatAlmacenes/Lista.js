﻿
llenaLista($('#busca_distribuidor').val());

var servicios = [];

var idAlmacen = "";

$('#busca_distribuidor').change(function () {

    llenaLista($('#busca_distribuidor').val())

});

function llenaLista(IdDistribuidor) {

    $('#datos').dataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": false,
        "lengthMenu": [[5], [5]],
        "ajax": {
            "url": "/Almacen/ObtenAlamcenes/",
            "type": "POST",
            "data": {
                'IdDistribuidor': IdDistribuidor
            }
        },
        "fnInitComplete": function (oSettings, json) {

        },
        "fnDrawCallback": function (oSettings) {

        },

        "columns": [
                 { "data": "IdAlmacen", "orderable": false },
                 { "data": "Nombre", "orderable": false },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {

                         var str;
                         if (full.Activo == 1) { str = "<button class='btn btn-info btn-xs'>Activo</button>" }
                         else { str = str = "<button class='btn btn-default-bright btn-xs'>Deshabilitado</button>" }
                         return str;


                     }

                 },
                  {
                      sortable: false,
                      "render": function (data, type, full, meta) {

                          var str = "";
                          if (full.servicios.length > 0) {
                              for (var a = 0; a < full.servicios.length; a++) {
                                  str += "<button class='btn btn-primary btn-xs'>" + full.servicios[a].Nombre + "</button>";

                              }
                              return str;
                          } else {
                              str += "<button class='btn btn-default-bright btn-xs'>Sin asignar</button>";
                              return str;
                          }

                      }

                  },
                 {
                     sortable: false,
                     "render": function (data, type, full, meta) {
                         var str = "";

                         str += "<button class='btn btn-warning btn-xs' onclick=EditaAlmacen(" + full.IdAlmacen + ")><i class='fa fa-pencil' aria-hidden='true'></i> Editar</button>"


                         return str;

                     }

                 },

        ],

        language: {

            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ orden",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay almacenes para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay almacenes para mostrar",
            emptyTable: "No hay almacenes disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })

}




function EditaAlmacen(id) {
    servicios = [];
    idAlmacen = id;
    $('#tblser').empty();
    $('#edita').modal('show');
    url = '/Almacen/obtenGeneralServ/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idDis': id },
        type: "POST",
        success: function (data) {



            for (var a = 0; a < data.length; a++) {
                if (data[a].Asignado == true) {
                    activa(data[a].idServicio, data[a].Nombre, data[a].Activo, null)

                    $('#tblser').append('<tr><td><button class="btn btn-info btn-xs" onClick="activa(\'' + data[a].idServicio + '\',\'' + data[a].Nombre + '\',\'' + data[a].Activo + '\',this)" style="width: 25px !important; height: 25px !important;padding:5px;"><i class="fa fa-check" aria-hidden="true"></i></button></td><td>' + data[a].Nombre + '</td></tr>');
                } else {
                    $('#tblser').append('<tr><td><button class="btn btn-default btn-xs"  onClick="activa(\'' + data[a].idServicio + '\',\'' + data[a].Nombre + '\',\'' + data[a].Activo + '\',this)" style="width:25px;height:25px; padding:5px;"><i class="fa fa-check" aria-hidden="true" style="color:#FFFFFF;"></i></button></td><td>' + data[a].Nombre + '</td></tr>');
                }
            }
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });


}

function activa(idserv, nombre, status, elem) {

    Servicio = {};
    Servicio.idServicio = idserv;
    Servicio.Nombre = nombre;
    Servicio.Activo = status;

    if (existe_en_arreglo(Servicio.idServicio)) {
        EliminarDeArreglo(servicios, Servicio.idServicio)
        if (elem != null) {
            $(elem).attr('class', 'btn btn-default btn-xs');
        }


    } else {
        servicios.push(Servicio);
        if (elem != null) {
            $(elem).attr('class', 'btn btn-info btn-xs');
        }


    }


}



function existe_en_arreglo(idServicio) {
    var result = $.grep(servicios, function (obj) { return obj.idServicio == idServicio; });
    if (result.length == 0) {
        return false;
    } else {
        return true;
    }
}


function EliminarDeArreglo(arr, value) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].idServicio == value) {
            arr.splice(i, 1);
            break;
        }
    }
}






$('#guarda').click(function () {

    var url = '/Almacen/GuardaAsignaciones/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'idAlmacen': idAlmacen, 'servicios': JSON.stringify(servicios) },
        type: "POST",
        success: function (data) {

            if (data == "1") {
                toastr.success("Correcto,Se asignaron los servicios correctamente");

            }
            else {
                toastr.warning("Error,Los servicios no se pudieron asignar");
            }



            llenaLista($('#busca_distribuidor').val());



            $('#edita').modal('hide');
        },
        error: function (request, error) {
            toastr.error('Error,Surgió un error, intente nuevamente más tarde');
        }
    });

});
