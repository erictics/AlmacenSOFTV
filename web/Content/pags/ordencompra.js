﻿

var objArticulos = new Array();

$(document).ready(function () {
    //cambio de status en AUTORIZACION DE ORDEN DE COMPRA
    $("#buscastatus").change(function () {
        LlenaGrid(1);
    });



    $('#close').click(function () {

        $('#autorizacion').hide();
    })

    // Cambio de select   AUTORIZACION DE ORDEN DE COMPRA
    $("#buscaproveedor").change(function () {
        LlenaGrid(1);
    });
    //Cuando se da click a buscar numero de autorizacion en AUTORIZACION DE ORDEN DE COMPRA
    $("#buscar").click(function () {

        var valid = jQuery('#formbusca').validationEngine('validate');
        if (valid) {
            LlenaGrid(1);
        }
    });

    // si se da cancelar a 
    $("#formadetalle .btn-danger").click(function () {
        var valid = jQuery('#formarechaza').validationEngine('validate');
        if (valid) {
            var objRoles = {};
            //objRoles.comentario = $("#comentario").val();
            objRoles.accion = 'rechaza';
            objRoles.clave = $("#autoriza_id").val();
            var myJSONText = JSON.stringify(objRoles);

            var url = '/OrdenCompra/Autoriza/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#ver").modal('hide');
                    toastr.success('La orden de compra se rechazó correctamente');
                    

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });
        }
    });

    $("#formadetalle .btn-success").click(function () {
        var valid = jQuery('#formaautoriza').validationEngine('validate');
        if (valid) {
            var objRoles = {};
            objRoles.comentario = $("#comentario").val();
            objRoles.accion = 'autoriza';
            objRoles.clave = $("#autoriza_id").val();
            var myJSONText = JSON.stringify(objRoles);

            var url = '/OrdenCompra/Autoriza/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#ver").modal('hide');

                    toastr.success('La orden de compra se autorizó correctamente');
                   

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });

        }
    });


    $(".preciootrosgastos").blur(function () {
        Resumen();
    });

    
    $("#agregar").click(function () {
        $('#articulo').modal('show');

        
        $("#ed_clasificacionmaterial").val('');
        $("#ed_tipomaterial").val('');
        $("#ed_articulo").val('');
        $("#ed_cantidad").val('');
        $("#ed_precio").val('');
        $("#ed_moneda").val('');

    });

    $(".optiontipo").hide();

    $("#ed_clasificacionmaterial").change(function () {

        $("#ed_tipomaterial").val('');

        $(".optiontipo").each(function () {
            if ($(this).attr("data-cla") == $("#ed_clasificacionmaterial").val()) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });

    LlenaGrid(1);

    $("#obten").click(function () {
        LlenaGrid(1);
    });

    $("#add").click(function () {
        $("#titulowin").html("Agregar Pedido");

        objArticulos = new Array();

        $('#edita').modal('show');
        $("#ed_id").val('');
        $("#ed_nombre").val('');



        $("#ed_existencias").val('');
        $("#ed_tope").val('');

        $("#ed_unidad").val('');
        $("#ed_porcentaje").val('');

        $("#ed_unidad").val('');

        $("#ed_tipomaterial").val('');
        $("#ed_clasificacionmaterial").val('');


        $("#ed_proveedor1").val('');
        $("#ed_precio1").val('');
        $("#ed_moneda1").val('');

        $("#ed_proveedor2").val('');
        $("#ed_precio2").val('');
        $("#ed_moneda2").val('');

        $("#ed_proveedor3").val('');
        $("#ed_precio3").val('');
        $("#ed_moneda3").val('');

    });

    $("#formaelimina").submit(function (e) {
        e.preventDefault();

        $("#loader").show();

        var url = '/Articulo/Elimina/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: $('#formaelimina').serialize(),
            type: "POST",
            success: function (data) {

                $("#elimina").modal('hide');
                toastr.warning($("#titulowin").html(), 'El artículo se eliminó correctamente');

                LlenaGrid(_pag);
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });




    $("#agregararticulo").click(function () {

        var valid = jQuery('#formaarticulo').validationEngine('validate');
        if (valid) {
            $("#loader").show();

            var objArticulo = {};
            objArticulo.ArticuloClave = $("#ed_articulo").val();
            objArticulo.ArticuloTexto = $("#ed_articulo option:selected").text();
            objArticulo.Cantidad = $("#ed_cantidad").val();
            objArticulo.PrecioUnitario = $("#ed_precio").val();
            objArticulo.catTipoMonedaClave = $("#ed_moneda").val();
            objArticulo.MonedaTexto = $("#ed_moneda option:selected").text();

            objArticulos.push(objArticulo);

            ActualizaLista();

            $('#articulo').modal('hide');

        }
    });


    $("#formaedita").submit(function (e) {
        e.preventDefault();


        var objRoles = {};
        objRoles.catProveedorClave = $("#ed_proveedor").val();
        objRoles.EntidadClaveSolicitante = $("#ed_almacen").val();
        objRoles.UsuarioClaveAutorizacion = $("#ed_autorizador").val();
        objRoles.Observaciones = $("#ed_observaciones").val();

        objRoles.TipoCambio = $("#ed_tipocambio").val();

        //alert($("#ed_iva").val());

        objRoles.Iva = $("#ed_iva").val();
        objRoles.Descuento = $("#ed_descuento").val();
        objRoles.DiasCredito = $("#ed_diascredito").val();

        var objOtroGasto1 = {};
        objOtroGasto1.Concepto = $("#ed_conceptooc1").val();
        objOtroGasto1.Precio = $("#ed_preciooc1").val();
        objOtroGasto1.Moneda = $("#ed_monedaoc1").val();

        var objOtroGasto2 = {};
        objOtroGasto2.Concepto = $("#ed_conceptooc2").val();
        objOtroGasto2.Precio = $("#ed_preciooc2").val();
        objOtroGasto2.Moneda = $("#ed_monedaoc2").val();

        var objOtrosGastos = new Array();
        objOtrosGastos.push(objOtroGasto1);
        objOtrosGastos.push(objOtroGasto2);

        objRoles.OtrosGastos = objOtrosGastos;
        objRoles.Articulos = objArticulos;


        //alert($("#to_subtotal").val());

        objRoles.ToSubtotal = $("#to_subtotal").val();
        objRoles.ToDescuento = $("#to_descuento").val();
        objRoles.ToIVA = $("#to_iva").val();
        objRoles.Total = $("#to_total").val();


        var myJSONText = JSON.stringify(objRoles);

        //alert(myJSONText);

            $("#loader").show();

            var url = '/Pedido/Guarda/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: { 'data': myJSONText },
                type: "POST",
                success: function (data) {

                    $("#edita").modal('hide');
                    toastr.success($("#titulowin").html(), 'El artículo se guardó correctamente');

                    LlenaGrid(_pag);
                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                }
            });

    });

    $("#ed_tipomaterial").change(function () {


        var tipo = $(this).val();

        var objRoles = {};
        objRoles.tipo = tipo;

        $("#ed_articulo").empty();
        $("#ed_articulo").append("<option value=''>Seleccione</option>");

        var myJSONText = JSON.stringify(objRoles);
        var url = '/Articulo/ObtenPorTipo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                var arreglo = eval(data);
                for (var i = 0; i < arreglo.length; i++) {
                    $("#ed_articulo").append("<option value='" + arreglo[i].Clave + "'>" + arreglo[i].Descripcion + "</option>");
                }
            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });
    });

});


function Resumen() {

    var suma = 0;
    var tipocambio = 1;

    for (var i = 0; i < objArticulos.length; i++) {

        if(objArticulos[i].MonedaTexto.indexOf('Dólar')>-1)
        {
            tipocambio = parseFloat($("#ed_tipocambio").val());
        }

        var totalproducto = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);
            totalproducto = totalproducto * tipocambio;
            suma += totalproducto;
    }

    if ($("#ed_preciooc1").val() != '') {
        suma += parseFloat($("#ed_preciooc1").val());
    }

    if ($("#ed_preciooc2").val() != '') {
        suma += parseFloat($("#ed_preciooc2").val());
    }


    var eliva = parseFloat($("#ed_iva").val());

    var descuento = 0;

    var descuento_po = 0;
    try
    {
        if ($("#ed_descuento").val() != "") {
            descuento_po = parseFloat($("#ed_descuento").val());
            descuento = descuento_po * suma / 100;
        }
    }
    catch (e)
    { }

    var subtotaldescuento = suma - descuento;
    var iva = (subtotaldescuento * eliva / 100);
    var total = subtotaldescuento + (subtotaldescuento * eliva / 100);


    $("#to_subtotal").val(suma);
    $("#to_descuento").val(descuento);
    $("#to_iva").val(iva);
    $("#to_total").val(total);


    $("#reiva_po").html("(" + $("#ed_iva").val() + "%" + ")");
    $("#reiva").html(ToMoneda(iva) + "%");

    $("#resubtotal").html(ToMoneda(suma));
    $("#redescuento").html(ToMoneda(descuento));
    $("#redescuento_po").html("(" + descuento_po + "%"+")");
    $("#retotal").html(ToMoneda(total));

}


function ActualizaLista()
{
    $("#articulostable tbody").empty();

    for (var i = 0; i < objArticulos.length; i++) {

        var total = parseFloat(objArticulos[i].PrecioUnitario) * parseInt(objArticulos[i].Cantidad);

        var renglon = "<tr><td>" + objArticulos[i].ArticuloTexto + "</td><td>" + objArticulos[i].Cantidad + "</td><td>" + ToMoneda(objArticulos[i].PrecioUnitario) + "</td><td>" + ToMoneda(total) + "</td><td>" + objArticulos[i].MonedaTexto + "</td><td>" + DameAccionesArticulo(i) + "</td></tr>"
        $("#articulostable tbody").append(renglon);
    }

    Resumen();
    AfterAjax();
}

//*************  Llena la tabla #datos 
var _pag = 1;




function LlenaGrid(pag) {
    _pag = pag;

    var objRoles = {};
    objRoles.pag = pag;
    objRoles.autoriza = "autoriza";
    objRoles.estatus = $("#buscastatus").val();
    objRoles.proveedor = $("#buscaproveedor").val();
    objRoles.pedido = $("#abuscar").val();

    var myJSONText = JSON.stringify(objRoles);


    $('#datos').dataTable({
        "processing": true,
        "responsive": true,
        "serverSide": true,
        "bFilter": false,
        "bDestroy": true,
        "info": true,
        "stateSave": true,
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "/Pedido/ObtenLista/",
            "type": "POST",
            "data": { 'data': myJSONText }
        },
        "fnInitComplete": function (oSettings, json) {

            AfterAjax();
        },
        "fnDrawCallback": function (oSettings) {

            AfterAjax();
        },
        "columns": [
            { "data": "NumeroPedido", "orderable": false },
            { "data": "Destino", "orderable": false },
            { "data": "Fecha", "orderable": false },
            { "data": "Total", "orderable": false },
            { "data": "Total_USD", "orderable": false },
            { "data": "Estatus", "orderable": false },


            {
                sortable: false,
                "render": function (data, type, full, meta) {
                                
                    return DameAcciones(full.Clave, full.Estatus);

                }
            }

        ],

        language: {
            processing: "Procesando información...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ Elementos",
            info: "Mostrando   _START_ de _END_ Total _TOTAL_ elementos",
            infoEmpty: "No hay elemetos para mostrar",
            infoFiltered: "(filtrados _MAX_ )",
            infoPostFix: "",
            loadingRecords: "Búsqueda en curso...",
            zeroRecords: "No hay registros para mostrar",
            emptyTable: "No hay registros disponibles",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },


        "order": [[0, "asc"]]
    })


}


function verOrden(id) {
   
    $('#btnaut').click();
    $('#titulocanvas-aut').empty().append("Autorización de pedido");
    $("#frameaut").attr("src", "/Pedido/Detalle?pdf=2&p=" + id);
    $("#comentario").val('');
    $("#autoriza_id").val(id);
    $('#formadetalle').show();

}

function solover(id) {

    $('#btnaut').click();
    $('#titulocanvas-aut').empty().append("Autorización de pedido");
    $("#frameaut").attr("src", "/Pedido/Detalle?pdf=2&p=" + id);
    $('#formadetalle').hide();
}


function AfterAjax() {

    






    $(".pdf").click(function () {

        var elrel = $(this).attr('rel');
        window.location.href = "/Pedido/Detalle?pdf=1&p=" + elrel;
    });

    $(".delarticulo").click(function () {
        var cual = $(this).val();
        alert(cual);
        objArticulos.splice(index, 1);
        ActualizaLista();
    });

    $(".del").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowindel").html("Elimina Artículo");
        $('#elimina').modal('show');

        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {

                var oRol = eval(data);

                $("#del_id").val(oRol.Clave);
                $("#titulowindel").html("Elimina Artículo " + oRol.Descripcion);


                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });

    $(".edit").click(function () {
        var cual = $(this).attr('rel');
        $("#titulowin").html("Edita Rol");
        $('#edita').modal('show');

        var url = '/Articulo/Obten/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': cual },
            type: "POST",
            success: function (data) {
                
                var oRol = eval(data);

                $("#ed_id").val(oRol.Clave);
                $("#ed_nombre").val(oRol.Descripcion);

                $("#ed_existencias").val(oRol.Existencias);
                $("#ed_tope").val(oRol.TopeMinimo);

                $("#ed_unidad").val(oRol.catUnidadClave);
                $("#ed_porcentaje").val(oRol.Porcentaje);

                $("#ed_unidad").val(oRol.catUnidadClave);

                $("#ed_tipomaterial").val(oRol.catTipoArticuloClave);
                $("#ed_clasificacionmaterial").val(oRol.catClasificacionMaterialClave);


                $("#ed_ubicacion").val(oRol.catUbicacionClave);

                $("#ed_proveedor1").val(oRol.proveedor1);
                $("#ed_precio1").val(oRol.precio1);
                $("#ed_moneda1").val(oRol.moneda1);

                $("#ed_proveedor2").val(oRol.proveedor2);
                $("#ed_precio2").val(oRol.precio2);
                $("#ed_moneda2").val(oRol.moneda2);

                $("#ed_proveedor3").val(oRol.proveedor3);
                $("#ed_precio3").val(oRol.precio3);
                $("#ed_moneda3").val(oRol.moneda3);

                //LlenaGrid(_pag);

            },
            error: function (request, error) {
                toastr.error('Surgió un error, intente nuevamente más tarde');
            }
        });

    });
}

function DameAccionesArticulo(cual)
{
    return "<button type='button' class='btn btn-danger btn-xs delarticulo' rel='" + cual + "'>&nbsp;Elimina</button>";
}

function DameDetalle(elid) {
    var opt="";
    opt += '<button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-success solover" data-toggle="tooltip" data-placement="right title="" data-original-title="Ver orden de compra"'
        + 'onClick="solover(\'' + elid + '\')"><i class="fa fa-search" aria-hidden="true"></i></button>';
        
        
      opt+="<button type='button' class='btn btn-info btn-xs pdf' rel='" + elid + "'>&nbsp;PDF</button>";
}

function DameAcciones(elid, estatus)
{
    if (estatus.toLowerCase().indexOf('autorizado') > -1 || estatus.toLowerCase().indexOf('rechazado') > -1 || estatus.toLowerCase().indexOf('parcial') > -1 || estatus.toLowerCase().indexOf('completo') > -1) {
        return '<button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-success solover" data-toggle="tooltip" data-placement="right title="" data-original-title="Ver orden de compra" '
            + ' onClick="solover(\'' + elid + '\')"><i class="fa fa-search" aria-hidden="true"></i></button>';
    }
    //if ( estatus.toLowerCase().indexOf('rechazado') > -1 ) {
    //    return "<button type='button' class='btn btn-default btn-xs solover' rel='" + elid + "'>&nbsp;Ver Orden de Compra</button>";
    //}
  
    else {
        if (estatus.toLowerCase().indexOf('cancelado') > -1) {
            return "<button class='btn btn-warning btn-xs'><i class='fa fa-ban' aria-hidden='true'></i>&nbsp;sin acciones</button>";
        }
        else {
            return '<button type="button" class="btn btn-success btn-xs ver" onClick="verOrden(\'' + elid + '\')" rel=' + elid + '><i class="fa fa-check-square" aria-hidden="true"></i>&nbsp;Autorizar o Rechazar</button>';
        }
       
    }
    //return "<button type='button' class='btn btn-success btn-xs autoriza' rel='" + elid + "'>&nbsp;Autoriza</button><button type='button' class='btn btn-danger btn-xs rechaza' rel='" + elid + "'>&nbsp;Rechaza</button>";
}