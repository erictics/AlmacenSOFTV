﻿





$('#distribuidor').change(function () {
    $('#almacen').empty();

    console.log($('#distribuidor').val());
    if ($('#distribuidor').val() == 0) {

        $('#almacen').append("<option value='0'>Todos los almacenes</option>");

    
    } else {

        var url = '/ReporteSaldoTecnico/getAlamcen/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'id_distribuidor': $('#distribuidor').val() },
            type: "POST",
            success: function (data) {
                $('#almacen').append("<option value='0'>Todos los almacenes</option>");
                for (var a = 0; a < data.length; a++)
                    $('#almacen').append("<option value=" + data[a].EntidadClave + ">" + data[a].Nombre + "</option>");
            },
            error: function (request, error) {
                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });

    }    
});


$('#clasificacion').change(function () {
    $('#tiparticulos').empty().append("<option value='0' selected>Todas los tipos</option>");
    var url = '/ReporteSurtidoDistribuidor/OntenerTipoArticulo/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': $('#clasificacion').val() },
        type: "POST",
        success: function (data) {
            console.log(data);
            for (var a = 0; a < data.length; a++) {
                $('#tiparticulos').append("<option value=" + data[a].catTipoArticuloClave + ">" + data[a].Descripcion + "</option>");
            }
        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
});


$('#tiparticulos').change(function () {
    $('#articulos').empty().append("<option>Selecciona</option>");
    var url = '/ReporteSurtidoDistribuidor/obtenerArticulos/';
    $.ajax({
        url: url,
        dataType: 'json',
        data: { 'id': $('#tiparticulos').val() },
        type: "POST",
        success: function (data) {
            console.log(data);
            for (var a = 0; a < data.length; a++) {
                $('#articulos').append("<option value=" + data[a].ArticuloClave + ">" + data[a].Nombre + "</option>");
            }
        },
        error: function (request, error) {
            Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
        }
    });
});



$("#pdf").click(function () {

   
    var distribuidor = document.getElementById("distribuidor").value;
    var almacen = document.getElementById("almacen").value;
    var clasificacion = document.getElementById("clasificacion").value;
    var tipo = document.getElementById("tiparticulos").value;
    var arti = document.getElementById("articulos").value;


    window.location.href = '/AparatosRobados/Reporte?pdf=1&distribuidor=' + distribuidor + '&almacen=' + almacen
                        + '&clasificacion=' + clasificacion + '&tipo=' + tipo + '&arti=' + arti;

    //$("#eliframe").attr('src', '/AparatosRobados/Reporte?pdf=1&distribuidor=' + distribuidor + '&almacen=' + almacen
    //                    + '&clasificacion=' + clasificacion + '&tipo=' + tipo + '&arti=' + arti);

});








