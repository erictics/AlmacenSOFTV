﻿
$(document).ready(function () {
    $("#modificapassword").click(function () {
        $("#password").modal("show");
    });

    $("#olvidepassword").click(function () {
        $("#passwordolvide").modal("show");
    });

    $("#setpassword").click(function () {

        var valid = jQuery('#formapassword').validationEngine('validate');
        if (valid) {

            //alert($('#formapassword').serialize());

            var url = '/Acceso/CambiaPassword/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formapassword').serialize(),
                type: "POST",
                success: function (data) {

                    if (data == "1") {
                        $("#password").modal('hide');
                        toastr.success('La contraseña se guardó correctamente');

                    }
                    else {
                        $("#password").modal('hide');
                        toastr.warning( 'La contraseña actual es incorrecta');

                    }


                },
                error: function (request, error) {
                    toastr.error('Surgió un error, intente nuevamente más tarde');
                    $("#password").modal('hide');
                }
            });

        }

    });

    $("#getpassword").click(function () {

        var valid = jQuery('#formapassword').validationEngine('validate');
        if (valid) {

            //alert($('#formapassword').serialize());

            var url = '/Acceso/ObtenPassword/';
            $.ajax({
                url: url,
                dataType: 'json',
                data: $('#formapassword').serialize(),
                type: "POST",
                success: function (data) {

                    if (data == "1") {
                        $("#passwordolvide").modal('hide');
                        Mensaje("Obtener contraseña", 'la contraseña se envió correctamente');

                    }
                    else {
                        $("#passwordolvide").modal('hide');
                        Mensaje("Obtener contraseña", 'la contraseña no se envió. Posiblemente no existe el correo electrónico');

                    }


                },
                error: function (request, error) {
                    Mensaje('Surgió un error, intente nuevamente más tarde');
                    $("#passwordolvide").modal('hide');
                }
            });

        }

    });
});



