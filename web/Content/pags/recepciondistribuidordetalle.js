﻿function SalidaMaterialDetalle() {
    $(".detalle").click(function () {

        var elrel = $(this).attr('rel');

        var objArticuloDetalle = {};
        objArticuloDetalle.Articulo = objArticulos[parseInt(elrel)].ArticuloClave;
        objArticuloDetalle.Proceso = objArticulos[parseInt(elrel)].ProcesoClave;

        _indiceArticulo = elrel;

        var myJSONText = JSON.stringify(objArticuloDetalle);

        //alert(myJSONText);
        var url = '/RecepcionDistribuidor/Articulo/';
        $.ajax({
            url: url,
            dataType: 'json',
            data: { 'data': myJSONText },
            type: "POST",
            success: function (data) {

                var arreglo = eval(data);
                _articulospedidos = arreglo;

                IniciaDetalle();
                IniciaContenido();
              
            },
            error: function (request, error) {
                Mensaje('Error', 'Surgió un error, intente nuevamente más tarde');
            }
        });

        $("#titulowindetalle").html("Recepción Pedido - " + objArticulos[parseInt(elrel)].ArticuloTexto);
        $("#editadetalle").modal("show");
    });
}

function IniciaDetalle()
{
    $("#lasseries thead").empty();
    $("#lasseries tbody").empty();
    var tiposeries = _articulospedidos[0].catSeriesClaves;
    var tr = "";
    tr += "<tr>";
    tr += "<th>Seleccionar</th>";
    for (var ii = 0; ii < tiposeries.length; ii++) {
        tr += "<th>" + tiposeries[ii].Nombre + "</th>";
    }
    tr += "</tr>";
    $("#lasseries thead").append(tr);
}

function IniciaContenido()
{
    var tbody = "";
    for (var ii = 0; ii < _articulospedidos.length; ii++)
    {
        var cadena = '';
        tbody += "<tr data-rel='_remplazar_'>";
        tbody += "<td><input type='checkbox' id='cb" + _articulospedidos[ii].Clave + "' name='cb" + _articulospedidos[ii].Clave + "' rel='" + _articulospedidos[ii].Clave + "' class='inventarios' _checked_ /></td>";
        for (var ij = 0; ij < _articulospedidos[ii].Seriales[0].datos.length; ij++)
        {
            tbody += "<td>" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "</td>";
            cadena += "|" + _articulospedidos[ii].Seriales[0].datos[ij].Valor + "|";
        }
        tbody = tbody.replace("_remplazar_", cadena);
        var array = objArticulos[_indiceArticulo].Inventario;
        var i = array.indexOf(_articulospedidos[ii].Clave);
        if (i != -1) {
            tbody = tbody.replace("_checked_", "checked");
        }
        else {
            tbody = tbody.replace("_checked_", "");
        }
        tbody += "</tr>";
    }
    $("#lasseries tbody").append(tbody);
    $('input[type="checkbox"]:not(".switch")').iCheck({
        checkboxClass: 'icheckbox_square-blue ',
        increaseArea: '20%'
    });
    $('input[type="checkbox"]:not(".switch")').on('ifChecked', function (event) {

        var cantidad = objArticulos[_indiceArticulo].Cantidad;
        if (cantidad > objArticulos[_indiceArticulo].Inventario.length) {
            objArticulos[_indiceArticulo].Inventario.push($(this).attr('rel'));
        }
        if (cantidad == objArticulos[_indiceArticulo].Inventario.length)
        {
            $(".inventarios:not(:checked)").prop("disabled", true);
        }
    });

    $('input[type="checkbox"]:not(".switch")').on('ifUnchecked', function (event) {

        var cantidad = objArticulos[_indiceArticulo].Cantidad;
        var array = objArticulos[_indiceArticulo].Inventario;
        var i = array.indexOf($(this).attr('rel'));
        if (i != -1) {
            objArticulos[_indiceArticulo].Inventario.splice(i, 1);
        }
        if (cantidad > objArticulos[_indiceArticulo].Inventario.length) {
            $(".inventarios").removeAttr("disabled");
        }
    });
}

$(document).ready(function () {
    $("#ed_buscar").keyup(function (e) {
        var texto = $(this).val();
        $("#lasseries tbody tr").each(function () {
            if ($(this).attr('data-rel').indexOf(texto) > -1)
            {
                $(this).show();
            }
            else
            {
                $(this).hide();
            }
        });
    });

    $("#formaeditadistribuidor").submit(function (e) {
        e.preventDefault();
        GuardaInfo();
    });
});